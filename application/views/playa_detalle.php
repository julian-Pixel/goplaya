<?php
if( $_SESSION['idioma']   == 'es' ){
  $data['title']=$info[0]->meta_title_en;
}else if( $_SESSION['idioma']   == 'en' ){
  $data['title']=$info[0]->meta_title_es;
}


$data['title_en']=$info[0]->meta_title_en;
$data['title_es']=$info[0]->meta_title_es;

$data['desc_es']=$info[0]->meta_description_es;
$data['desc_en']=$info[0]->meta_description_en;
$this->load->view('commons/header_playa',$data);

?>

<style>
    .flex-viewport {
        height: 470px;
    }

   .bs {
    background: white;
    color: #064b75;
    border-radius: 0;
    min-width: 90%;
    text-align: left;
    border: solid 1px rgba(128, 128, 128, 0.18);
    position: relative;
    height: 53px;
    line-height: 53px;
    padding: 0 5px;
    position: relative;
    font-size: 30px;
    font-weight: 700;
       margin-bottom: 0rem;
}

    .bs:after {
        content: '\f078';
        font-family: FontAwesome;
        position: absolute;
        right: 10px;
    }

    #regionc  h1{
    display: none;
}
</style>

<?php

     $lat =  $info[0]->latitud;



      $long = $info[0]->longitud;


 ?>
<div class="">


<!-- Ads posicion 1 -->
<div class="flexslider">
<ul class="slides">
<?php foreach($banners_playa as $b){?>

<?php if ($b->posicion == 1) {  ?>
<li>
  <a href="<?php echo $b->url; ?>" target="_blank">
<img src="<?php echo base_url()?>/archivos/<?php echo $b->banner; ?>" alt="">
</a>
</li>
<? } ?>
<?php }?>
</ul>
</div>
<!-- Fin Ads posicion 1 -->

<div class="row playa_header" style="background:linear-gradient(rgba(255, 255, 255, 0.11)68%,rgb(255, 255, 255)100%),url( <?php echo base_url().'archivos/'.$galeria[0]->url; ?>)">




<?php if( $_SESSION['idioma']   == 'es' ){ ?>

<h2><span>Playa</span> <?php echo $info[0]->nombre; ?></h2>
<? }else if( $_SESSION['idioma']   == 'en' ){?>

<h2> <?php echo $info[0]->nombre; ?> <span>Beach</span></h2>
<?php }?>



<?php if(isset($this->session->userdata['logged_in'])){ ?>
<div class="stars2">
<div class="stars">

<?php

$votos = $estrellas[0]->total ;
$registros = $estrellas[0]->registros;


$star = ($votos/$registros);

if($star > 0){


for($i=1 ; $i<=$star; $i++){
?>

<i class="fa fa-star" aria-hidden="true" onclick="vote(<?php echo $i;?>,'<?php echo $slug; ?>');"></i>
<?php

}
for($i=$star ; $i<5; $i++){
?>

<i class="fa fa-star-o" aria-hidden="true" onclick="vote(<?php echo $i+1;?>,'<?php echo $slug; ?>');"></i>
<?php

}

}else{
?>
<i class="fa fa-star" aria-hidden="true" onclick="vote(1,'<?php echo $slug; ?>');"></i>
<i class="fa fa-star" aria-hidden="true" onclick="vote(2,'<?php echo $slug; ?>');"></i>
<i class="fa fa-star" aria-hidden="true" onclick="vote(3,'<?php echo $slug; ?>');"></i>
<i class="fa fa-star" aria-hidden="true" onclick="vote(4,'<?php echo $slug; ?>');"></i>
<i class="fa fa-star" aria-hidden="true" onclick="vote(5,'<?php echo $slug; ?>');"></i>

<?php

}
?>



</div>
<span class="vote"><?php $parte = explode(".", $star);  echo $parte[0]; ?> <i class="fa fa-star"></i> / <?php echo $registros; ?> <?php echo $GLOBALS['votos'];?></span>
</div>

<?php }else{

?>
<div class="stars2">
<div class="stars">

<?php

$votos = $estrellas[0]->total ;
$registros = $estrellas[0]->registros;


$star = ($votos/$registros);

?>
<i class="fa fa-star login-action" aria-hidden="true"></i>
<i class="fa fa-star login-action" aria-hidden="true"></i>
<i class="fa fa-star login-action" aria-hidden="true"></i>
<i class="fa fa-star login-action" aria-hidden="true"></i>
<i class="fa fa-star login-action" aria-hidden="true"></i>





</div>
<span class="vote"><?php $parte = explode(".", $star);  echo $parte[0]; ?> <i class="fa fa-star"></i> / <?php echo $registros; ?> <?php echo $GLOBALS['votos'];?></span>
</div>

<?php

} ?>
<a href="#galery" class="gal-btn anchor"><i class="fa fa-camera-retro" aria-hidden="true"></i></a>
<a target="_blank" style="background: #064b75;right: 224px;" href="waze://?q= <?php echo $info[0]->latitud; ?>, <?php echo $info[0]->longitud; ?>" class="gal-btn"><img style="width: 38px;margin-top: -4px;" src="<?php echo base_url();?>theme/img/waze.svg" alt=""></a>
<a target="_blank" style="background: #064b75;right: 167px;" href="https://maps.google.com/maps?ll=<?php echo $info[0]->latitud; ?>,<?php echo $info[0]->longitud; ?>&q=<?php echo $info[0]->latitud; ?>,<?php echo $info[0]->longitud; ?>&hl=en&t=h&z=18" class="gal-btn">

<img style="width: 28px;margin-top: -4px;" src="<?php echo base_url();?>theme/img/google-maps.svg" alt="">
</a>
<a href="http://hoteles.goplaya.cr/searchresults.html?aid=1333737&latitude=<?php echo $lat; ?>&longitude=<?php echo $long; ?>&radius=20" target="_blank" style="right:110px;    background: white;
    font-size: 32px;"  class="gal-btn"><i class="icon-hoteles"></i></a>
</div>







<section class="container">


	<?php $full2 = false; ?>

<!-- Ads posicion 5 -->
<div class="col-sm-3 flexslider">
<ul class="slides">
<?php foreach($banners_playa as $b){?>

<?php if ($b->posicion == 4) {


	 $full2 = true;
?>


<li>
    <a href="<?php echo $b->url; ?>" target="_blank">
<img src="<?php echo base_url()?>/archivos/<?php echo $b->banner; ?>" alt="">
</a>
</li>
<? } ?>
<?php }?>
</ul>
</div>
<!-- Fin Ads posicion 5 -->



<div class="<?php if($full2){echo 'col-sm-9'; } else {echo 'col-sm-12';} ?> ">

<?php if( $_SESSION['idioma']   == 'es' ){ ?>

<?php echo $info[0]->descripcion; ?>

<? }else if( $_SESSION['idioma']   == 'en' ){?>

<?php echo $info[0]->descripcion_en; ?>
<?php }?>

</div>
</section>


<!-- Ads posicion 2 -->




<div class="flexslider">
<ul class="slides">
<?php foreach($banners_playa as $b){?>

<?php if ($b->posicion == 20) {  ?>
<li>
  <a href="<?php echo $b->url; ?>" target="_blank">
<img src="<?php echo base_url()?>/archivos/<?php echo $b->banner; ?>" alt="">
</a>
</li>
<? } ?>
<?php }?>
</ul>
</div>
<!-- Fin Ads posicion 2 -->
<br>

<section class="txt-center">
<h2><?php echo $GLOBALS['caracteristicas']; ?> </h2>
</section>
<div class="container ">

<ul class="iconos">
<?php foreach($actividades as $actividad){ ?>

<?php if( $_SESSION['idioma']   == 'es' ){ ?>

<li class="<?php if (in_array($actividad->idactividad, $c_datos)) { echo 'popup'; }  ?>"
data-order="<?php if (in_array($actividad->idactividad, $c_datos)) { echo '1'; }else{ echo '2'; }  ?>" onclick="<?php if (in_array($actividad->idactividad, $c_datos)) { ?> det(<?php echo $actividad->idactividad; ?>,<?php echo $id; ?>) <?php } ?>">

<i class="<?php echo $actividad->icono ; ?>"></i> <span><?php echo $actividad->detalle ; ?> </span></li>



<? }else if( $_SESSION['idioma']   == 'en' ){?>

<!--<li><i class="<?php echo $actividad->icono ; ?>"></i> <span><?php echo $actividad->detalle_en ; ?></span></li>
-->


<li class="<?php if (in_array($actividad->idactividad, $c_datos)) { echo 'popup'; }  ?>"
data-order="<?php if (in_array($actividad->idactividad, $c_datos)) { echo '1'; }else{ echo '2'; }  ?>" onclick="<?php if (in_array($actividad->idactividad, $c_datos)) { ?> det(<?php echo $actividad->idactividad; ?>,<?php echo $id; ?>) <?php } ?>">

<i class="<?php echo $actividad->icono ; ?>"></i> <span><?php echo $actividad->detalle_en ; ?></span></li>


<?php }?>

<?php
}
?>
</ul>

</div>





<section class="txtcenter" style="margin-bottom: -10px;">

<ul id="galery" class="row is-flex">
<?php


foreach($galeria as $item){?>
<li class="col-xs-12 col-md-4" style="padding:0">



<?php if($item->tipo == 2){
$video_code = explode("=", $item->url);

$icono = 'fa fa-youtube-play';
$url = 'https://img.youtube.com/vi/'.$video_code[1].'/0.jpg';
$url2 = $item->url;


} else if($item->tipo == 1){

$icono = 'fa fa-picture-o';
$url = base_url().'archivos/'.$item->url;
$url2 = base_url().'archivos/'.$item->url;

}


?>

<a href="<?php echo $url2; ?>" rel="gallery-1" class="swipebox" title="<?php echo $item->caption; ?>"><img class="img-responsive"  src="<?php echo $url; ?>" alt="">

<div class="overlay">

<i class="<?php echo $icono; ?>" aria-hidden="true"></i>
</div>

</a>

</li>
<?php } ?>
</ul>
</section>





<div class="container" style="text-align: justify;padding-top: 3%;padding-bottom: 3%;">

<?php $full = false; ?>

<!-- Ads posicion 5 -->
<div class="col-sm-3 flexslider">
<ul class="slides">
<?php foreach($banners_playa as $b){?>

<?php if ($b->posicion == 5) {


	 $full = true;
?>


<li>
    <a href="<?php echo $b->url; ?>" target="_blank">
<img src="<?php echo base_url()?>/archivos/<?php echo $b->banner; ?>" alt="">
</a>
</li>
<? } ?>
<?php }?>
</ul>
</div>
<!-- Fin Ads posicion 5 -->


<div class="<?php if($full){echo 'col-sm-9'; } else {echo 'col-sm-12';} ?> " style="text-align:center">

<?php if( $_SESSION['idioma']   == 'es' ){ ?>
<button type="button" class="bs " data-toggle="collapse" data-target="#regionc">¿Qué debe saber de la región?</button>

<div id="regionc" class="collapse" style="text-align:justify">
<?php echo $region[0]->detalle; ?>
    </div>
<? }else if( $_SESSION['idioma']   == 'en' ){?>
<button type="button" class="bs " data-toggle="collapse" data-target="#regionc">Regional information</button>
<div id="regionc" class="collapse" style="text-align:justify">
<?php echo $region[0]->detalle_en; ?>
</div>
<?php }?>

</div>

</div>
<!-- Ads posicion 3 -->
<div class="flexslider">
<ul class="slides">
<?php foreach($banners_playa as $b){?>

<?php if ($b->posicion == 3) {  ?>
<li>
    <a href="<?php echo $b->url; ?>" target="_blank">
<img src="<?php echo base_url()?>/archivos/<?php echo $b->banner; ?>" alt="">
</a>
</li>
<? } ?>
<?php }?>
</ul>
</div>
<!-- Fin Ads posicion 3 -->




<!-- TOURS DESTACADOS  -->

<?php if (count($tours) >= 1) { ?>
<section class="col-xs-12">

<!-- <h2> <span>Tours</span> disponibles </h2> -->
<h2 style="text-align:center;" > <span>Tours</span> cercanos </h2>


</section>
<?php } ?>

<div class="container">


<div class="row">
  <?php foreach($tours as $tour){?>
   <div class="touritem col-xs-12 col-md-4">


<a href="<?php echo base_url();?>tours/tour/<?php echo $tour->idtour; ?>">

      <div class="contenedor-tour">

       <div class="imagen">
       <?php if($tour->imagen != ''){
       $imagen = base_url().'archivos/'.$tour->imagen;
       }else{
       $imagen = 'https://i.ytimg.com/vi/-ZEDp5JhhIc/maxresdefault.jpg';

       }?>

       <img src="<?php echo $imagen; ?>" class="img-responsive" alt="">

       </div>


       <div class="info-wrap clearfix">
           <div class="info-left">
               <h3 class="title"><a class="theme-color-hover" href="<?php echo base_url();?>tours/tour/<?php echo $tour->idtour; ?>">


                 <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                     <?php echo $tour->nombre; ?>
               <? }else if( $_SESSION['idioma']   == 'en' ){?>

                   <?php echo $tour->nombre_eng; ?>
               <?php }?>



               </a></h3>
               <div class="post-meta">
                   <ul>
                       <li class="destinations"> <i class="fa fa-map-marker"> </i> Amsterdam </li>
                   </ul>
               </div>
           </div>
           <div class="tour-price-vote">
             <?php
               if($tour->porcetaje_descuento>0){
             ?>
             <span class="old-price">$ <?php echo $tour->precio; ?></span>
             <?php } ?>

             <div class="wc">


              <span class="price-tour theme-bg">$ <?php

             if($tour->porcetaje_descuento>0){
             echo ($tour->precio)-(($tour->precio)*($tour->porcetaje_descuento/100));
             }else{
               echo ($tour->precio);
             }


               ?></span>

</div>


           </div>
       </div>

       <div class="overlay">
         <a href="<?php echo base_url();?>tours/tour/<?php echo $tour->idtour; ?>">


<div class="alg-c">

         <div class="col-xs-12">

           <?php
             if($tour->porcetaje_descuento>0){
           ?>
           <span class="old-price">$ <?php echo $tour->precio; ?></span>
           <?php } ?>
       <span class="precio">
           <?php if($tour->porcetaje_descuento>0){
             echo '$';
           echo ($tour->precio)-(($tour->precio)*($tour->porcetaje_descuento/100));
           }else{
               echo '$';
             echo ($tour->precio);
           }  ?>

       </span>
         </div>

         <div class="col-xs-12">
               <br>
           <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                 <h3> <strong> <?php echo $tour->nombre; ?>
         <? }else if( $_SESSION['idioma']   == 'en' ){?> </strong></h3>

             <h3> <strong>  <?php echo $tour->nombre_eng; ?> </strong></h3>
         <?php }?>

         </div>

          <div class="col-xs-12 txt-justify">

            <!-- <strong>Descripción:</strong> -->

            <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                   <?php if (strlen($tour->descripcion)> 200) {?>
                     <p ><?php echo substr($tour->descripcion,0,200); ?>...</p>
                   <?php } else { ?>
                     <p ><?php echo $tour->descripcion; ?></p>
                   <?php } ?>

          <? }else if( $_SESSION['idioma']   == 'en' ){?> </h4>



              <?php if (strlen($tour->descripcion_eng)> 200) {?>
                <p ><?php echo substr($tour->descripcion_eng,0,200); ?>...</p>
              <?php } else { ?>
                <p ><?php $tour->descripcion_eng; ?>...</p>
              <?php } ?>

          <?php }?>



         </div>

</div>

</a>
       </div>

       </div>




</a>
<br><br>
   </div>

   <?php } ?>
</div>

</div>


<!--  FIN TOURS DESTACADOS -->





<div class="row">

<style>
    .banner_hoteles {
        background: linear-gradient(rgba(8, 8, 8, 0.26), rgba(31, 31, 31, 0)), url(<?php echo base_url().'archivos/'.$galeria[0]->url;
        ?>);
        height: 230px;
        background-size: cover;
        background-position: center center;
    }

    .bh_cp {
        width: 50%;
        overflow: hidden;
        text-align: center;
        color: white;
        top: 50%;
        position: relative;
        transform: translateY(-50%);
    }

    .bh_cp h2 {
        font-size: 28px;
        font-family: 'Amarillo';
    }

   .bh_cp h3 {
    margin: 0;
    font-family: 'Raleway', sans-serif;
    font-weight: 700;
}
    .bh_cp p {
        font-size: 20px;
        font-weight: 100;
        margin: 0 !important;
            font-family: 'Raleway', sans-serif;
    }

    .bh_cp a {
        text-transform: uppercase;
        color: white;
        background: #8BC34A;
        height: 40px;
        display: inline-block;
        line-height: 40px;
        margin-top: 2rem;
        padding: 0 15px;
        border-radius: 30px;
        font-size: 17px;
    }



    @media (max-width: 580px) {
        .bh_cp {
            width: 100%;
        }

        .bs {
    font-size: 3vw;
}
    }
</style>
    <div class="banner_hoteles">
       <?php

            $lat =  $info[0]->latitud;



             $long = $info[0]->longitud;


        ?>

       <?php if( $_SESSION['idioma']   == 'es' ){ ?>



 <div class="bh_cp" id="hoteles">
            <p>Encontrá </p>
            <h3>alojamientos cercanos a</h3>
            <h2><?php echo $info[0]->nombre; ?></h2>

            <a href="http://hoteles.goplaya.cr/searchresults.html?aid=1333737&latitude=<?php echo $lat; ?>&longitude=<?php echo $long; ?>&radius=20" target="_blank">Ver Hoteles</a>
        </div>



<? }else if( $_SESSION['idioma']   == 'en' ){?>

 <div class="bh_cp" id="hoteles">
            <p>Find</p>
            <h3>accommodations near</h3>
            <h2><?php echo $info[0]->nombre; ?> Beach</h2>

            <a href="http://hoteles.goplaya.cr/searchresults.html?aid=1333737&latitude=<?php echo $lat; ?>&longitude=<?php echo $long; ?>&radius=20" target="_blank">View Hotels</a>
        </div>
<?php }?>






    </div>








</div>


<?php

$this->load->view('commons/footer');

?>
</div>


<script>

function det(caracteristica, playa){
$.ajax({
url: '<?php echo base_url(); ?>provincias/detalles_item', //This is the current doc
type: "POST"
, data: ({c:caracteristica,p:playa})
,dataType: 'json'
, success: function (data) {


<?php if( $_SESSION['idioma']   == 'es' ){ ?>

$('#det_carac').html(data[0].detalle);
<? }else if( $_SESSION['idioma']   == 'en' ){?>

$('#det_carac').html(data[0].detalle_en);
<?php }?>


$('#detModal').modal('toggle');

}
});

}
$('.bs').html($('#regionc > h1').text());


</script>


<!-- Modal -->
<div class="modal fade" id="detModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog"  role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel"></h4>
</div>
<div id="det_carac" class="modal-body">

</div>

</div>
</div>
</div>
