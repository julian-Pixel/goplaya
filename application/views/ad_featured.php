<?php
  $this->load->view('commons/header');
?>




<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">

           <h2>Featured</h2>

      </div>
      </div>

    </div>
    <div class="row ctool">

        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
    </div>

</div>
<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">

<h4>Enlaces home</h4>
   <div class="table-responsive">
 <table class="table table-striped">
     <thead>
         <tr>
             <th>Titulo</th>
             <th>Acciones</th>
         </tr>
     </thead>

<tbody>
   <?php foreach($featured as $f){ ?>
   <tr>
        <td><?php echo $f->titulo_es ?></td>
        <td><a href="<?php echo base_url()?>dashboard/editar_featured/<?php echo $f->id ?>">Editar</a></td>
    </tr>


<?php } ?>

</tbody>
 </table>


 <h4>Playas del mes</h4>


<form class="" action="<?php echo base_url()?>index.php/dashboard/add_featured_pm" method="post" style="margin-bottom:3rem;">

  <select class="form-control" name="playa">
    <?php foreach ($playas as $playa) {
    ?>
      <option value="<?php echo $playa->idplaya;?>"><?php echo $playa->nombre;?></option>
    <?php
      }
    ?>

</select>

<input type="text" name="zona" class="form-control" placeholder="detalle" value="">

<input type="submit" class="btn btn-default" value="Agregar">
</form>


    <div class="table-responsive">
  <table class="table table-striped">
      <thead>
          <tr>
              <th>Titulo</th>
              <th>Acciones</th>
          </tr>
      </thead>

 <tbody>
    <?php foreach($delmes as $f){ ?>
    <tr>
         <td><?php echo $f->nombre ?></td>
         <td><a href="<?php echo base_url()?>dashboard/eliminar_playa_mes/<?php echo $f->id ?>">Eliminar</a></td>
     </tr>


 <?php } ?>

 </tbody>
  </table>


  <h4>Playas más populares</h4>


 <form class="" action="<?php echo base_url()?>index.php/dashboard/add_featured_pp" method="post" style="margin-bottom:3rem;">

   <select class="form-control" name="playa">
     <?php foreach ($playas as $playa) {
     ?>
       <option value="<?php echo $playa->idplaya;?>"><?php echo $playa->nombre;?></option>
     <?php
       }
     ?>

 </select>

 <input type="text" name="zona" class="form-control" placeholder="detalle" value="">

 <input type="submit" class="btn btn-default" value="Agregar">
 </form>


     <div class="table-responsive">
   <table class="table table-striped">
       <thead>
           <tr>
               <th>Titulo</th>
               <th>Acciones</th>
           </tr>
       </thead>

  <tbody>
     <?php foreach($populares as $f){ ?>
     <tr>
          <td><?php echo $f->nombre ?></td>
          <td><a href="<?php echo base_url()?>dashboard/eliminar_playa_popular/<?php echo $f->id ?>">Eliminar</a></td>
      </tr>


  <?php } ?>

  </tbody>
   </table>

</div>
</div>
<div class="container-fluid">

<?php
  $this->load->view('commons/footer');
?>
</div>
