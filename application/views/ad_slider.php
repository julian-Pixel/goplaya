<?php
  $this->load->view('commons/header');
?>
<div class="save_ajax">
   <img src="<?php echo base_url();?>/theme/img/clock.gif" alt="">
    
</div>



<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">
          
           <h2>Editar slider</h2>
         
      </div>
      </div>
        
    </div>   
    <div class="row ctool">
        
        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
    </div>
    
</div>
<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">

<div class="row flexslider">
  <ul class="slides">
   <?php foreach($slides as $slide){?>
   <li>
      <img src="<?php echo base_url();?>archivos/<?php echo $slide->url; ?>" />
    </li>
   <?php } ?>
    
    
  </ul>
  
  
</div>
<div class="row slides">
    
      <div class="col-xs-12 tool-bar">
      <a data-toggle="modal" class="btn-default-gp" data-target="#slideModal" href="#">Agregar slide</a>
    </div>
     <div class="col-xs-12">
      <h3>Slides</h3>
    <ul class="sortable">
        <?php foreach($slides as $slide){?>
   <li id="item_<?php echo $slide->id; ?>">
      <img class="img-responsive" src="<?php echo base_url();?>archivos/<?php echo $slide->url; ?>" />
      <a href="<?php echo base_url();?>slider/eliminar_slide/<?php echo $slide->id; ?>"><i class="fa fa-trash" style="right: 50px;" aria-hidden="true"></i></a>
      <i class="fa fa-arrows" aria-hidden="true"></i>
    </li>
   <?php } ?>
    </ul>
      
    </div>
   
  
</div>
 
</div>


<!-- Modal -->
<div class="modal fade" id="slideModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Agregar slide</h4>
      </div>
      <div class="modal-body clearfix">
       <form action="<?php echo base_url(); ?>slider/agregar_slide" method="post" enctype="multipart/form-data"> 
	  	
	  		<div class="col-md-12">
				<input class="filestyle" type="file"  data-buttonText="Buscar imagen" name="uploadedimages[]" data-icon="false" multiple>
				<br/>
				<div id="vpp"></div>
   
	  		</div>
 <input type="submit" value="Agregar" class="btn btn-theme  btn-default-gp-v">
     
          
          </form>
      </div>
     
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="container-fluid">



<?php
  $this->load->view('commons/footer');
?>
</div>

