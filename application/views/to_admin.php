<?php
  $this->load->view('commons/header');
?>
<?php if(!$have_operadora){?>

    <div class="setup-modal">
<a href="#" class="close-setm"><i class="fa fa-times"></i></a>

<form action="<?php echo base_url(); ?>index.php/tour_operadora/configurar" method="post" data-toggle="validator" role="form">
<div class="step row s-active" id="step1">
       <div class="title-wizard">
            <h2><span class="number">1</span> Información básica</h2>
        </div>
     <div class="step-body">

   <div class="col-xs-6">
      <div class="form-group">
           <label for="nombre" class="control-label" >Nombre</label>
            <input type="text" class="form-control" name="nombre" required>
        </div>
        <div class="form-group">
           <label for="email" class="control-label" >Email</label>
            <input type="email" class="form-control" name="email" required>
        </div>
   </div>
   <div class="col-xs-6">
        <div class="form-group">
           <label for="telefono1" class="control-label" >Teléfono 1</label>
            <input type="tel" class="form-control" name="telefono1" required>
        </div>
        <div class="form-group">
           <label for="telefono2" class="control-label" >Teléfono 2</label>
            <input type="tel" class="form-control" name="telefono2">
        </div>
   </div>

     </div>

    <ul class="step-arrows">
        <li><a href="#step2">Siguiente <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
    </ul>
</div>
<div class="step row" id="step2">
       <div class="title-wizard">
            <h2><span class="number">2</span> Datos de contacto</h2>
        </div>
      <div class="step-body">

   <div class="col-xs-6">
      <div class="form-group">
           <label for="fax" class="control-label" >Fax</label>
            <input type="tel" class="form-control" name="fax">
        </div>
   </div>
     <div class="col-xs-6">
        <div class="form-group">
           <label for="website" class="control-label" >Sitio Web</label>
            <input type="website" class="form-control" name="website">
        </div>
   </div>
   <div class="col-xs-12">
        <div class="form-group">
           <label for="direccion" class="control-label" >Dirección</label>
            <textarea  class="form-control" name="direccion" required></textarea>
        </div>
   </div>
   <div col-xs-12>
       <input type="submit" class="btn-default-gp" value="Finalizar">
   </div>


    </div>
    <ul class="step-arrows">
        <li><a class="back-arrow" href="#step1"><i class="fa fa-chevron-left" aria-hidden="true"></i>Anterior</a></li>
    </ul>

</div>
</form>


</div>

<?php } ?>



<div class="container-fluid" style="background: #f5f5f5;">
<?php if(!$have_operadora){?>
<div class="row to_setupc">
   <div class="col-xs-12">
       <img class="img-responsive" style="display: inline-block;" src="<?php echo base_url(); ?>theme/img/tour_setup.gif" alt="">
   </div>
  <div class="col-xs-12 txt-center" style="padding-left: 10%;padding-right: 10%;">
     <h2>No has configurado aún los datos de la  <span>Tour operadora</span></h2>
     <p style="font-size: 15px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dignissim dui id scelerisque convallis. Duis efficitur, velit nec viverra pellentesque, mi nisl placerat magna, quis feugiat tortor odio ut elit. </p>
      <a href="#" class="btn-default-gp setup-btn">Configurar tour operadora</a>
    </div>
</div>

<?php }else{?>


<div class="row to-cintillo">

    <div class="col-xs-8">
      <div class="profile">
          <img src="<?php if(isset($foto_perfil)){ echo $foto_perfil; } ?>" class="img-responsive" alt="">
           <h2>Bienvenido de nuevo, <span><?php echo $datos_operadora[0]->nombre; ?></span> </h2>

      </div>


    </div>

    <div class="col-xs-2 codigo_comercio">
          <p>Código de comercio: <span style="font-size: 21px;color: rgba(245, 245, 245, 0.67);"><?php echo $datos_operadora[0]->codigo; ?></span></p>
    </div>


</div>

<div class="container" id="panel_reservaciones">

    <div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?php echo $GLOBALS['reserva_to']; ?></a></li>
    <li role="presentation" ><a href="#video" aria-controls="video" role="tab" data-toggle="tab">¿Cómo subir un tour?</a></li>

  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">


      <table class="table table-striped">
        <thead>
          <tr>
            <th><?php echo $GLOBALS['tour_to']; ?></th>
            <th><?php echo $GLOBALS['fechat_to'];  ?></th>
            <th><?php echo $GLOBALS['fechar_to']; ?></th>
            <th><?php echo $GLOBALS['cliente_to'];  ?></th>
            <th><?php echo $GLOBALS['estado_to'];  ?></th>
              <th><?php echo $GLOBALS['acciones_to'];  ?></th>
          </tr>
        </thead>

        <tbody>

          <?php foreach ($mis_reservas as $reserva) {?>

              <?php   $date=date_create($reserva->fecha);
                      $date_tour=date_create($reserva->fecha_tour); ?>
          <tr>
            <td><?php echo $reserva->tour_nombre; ?></td>



            <td><?php echo date_format($date_tour, "d-m-Y"); ?> <?php echo $reserva->hora; ?></td>
            <td><?php echo date_format($date,"d-m-Y"); ?></td>
            <td><?php echo $reserva->nombre; ?> <?php echo $reserva->apellidos; ?>,  <?php echo $reserva->email; ?></td>
            <td>
              <?php if($reserva->estado == 1){ ?>

                 <span class="label label-success"><?php echo $GLOBALS['status_to']; ?></span>
              <?php }elseif($reserva->estado == 0){ ?>

                 <span class="label label-danger"><?php echo $GLOBALS['status1_to']; ?></span>
              <?php } ?>

            </td>
            <td> <a href="<?php echo base_url(); ?>tours/ver_reserva/<?php echo base64_encode($reserva->idreservacion);?>" style="margin-top: 11px;float:right;" class="btn-default-gp">Ver</a></td>
          </tr>

          <?php
          } ?>
        </tbody>
      </table>




    </div>
    <div role="tabpanel" class="tab-pane" id="video">
        <div class="row">
          <div class="col-xs-12 col-md-12">


            <?php if( $_SESSION['idioma']   == 'es' ){ ?>

              <iframe width="100%" height="415" src="https://www.youtube.com/embed/<?php echo $videos_tours[0]->video; ?>"
                frameborder="0"
                allow="autoplay; encrypted-media"
                allowfullscreen>
              </iframe>



          <? }else if( $_SESSION['idioma']   == 'en' ){?>
            <iframe width="100%" height="415" src="https://www.youtube.com/embed/<?php echo $videos_tours[0]->video_eng; ?>"
              frameborder="0"
              allow="autoplay; encrypted-media"
              allowfullscreen>
            </iframe>
          <?php }?>



          </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="messages">...</div>
    <div role="tabpanel" class="tab-pane" id="settings">...</div>
  </div>

</div>



</div>

<section class="row" style="background:white" id="mis_tours">

    <div class="section-header">
      <h2><?php echo $GLOBALS['mis_tours'] ;?></h2>
      <ul class="tool-section-bar">
          <li><a data-open="modal" data-target="#agregar-tour"><?php echo $GLOBALS['add_tour_es'] ?><i class="fa fa-plus"></i></a></li>
      </ul>
    </div>

    <?php foreach($mis_tours as $tour){?>

     <div class="col-md-4 col-xs-6 tour-item tour-item2">
       <div class="info-basica">
           <h2><?php echo $tour->nombre; ?></h2>
           <span class="activo" style="background:<?php if($tour->estado == '1'){ echo '#689F38'; }else{echo '#C62828';}?>">

              <?php if($tour->estado == '1'){ ?>
               <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                <?php }else{ ?>
                <i class="fa fa-ban" aria-hidden="true"></i>
                <?php }?>
           </span>
           <p class="precio"><span>$</span> <?php echo $tour->precio; ?></p>
       </div>
        <div class="overlay">
           <div class="center2 clearfix">

                <a class="btn-default-gp btn-default-gp-v" href="<?php echo base_url();?>tours/tour/<?php echo $tour->idtour; ?>" target="_blank">
                <?php echo $GLOBALS['ver_tour'] ?></a>

                <?php if( $_SESSION['idioma']   == 'es' ){ ?>
                <a class="btn-default-gp btn-default-gp-v" href="<?php echo base_url();?>tour_operadora/configurar_tour/<?php echo $tour->idtour; ?>?idioma=es"><i class="fa fa-cog" aria-hidden="true"></i>  <?php echo $GLOBALS['editar_tour'] ?></a>
              <?php }else{ ?>
                <a class="btn-default-gp btn-default-gp-v" href="<?php echo base_url();?>tour_operadora/configurar_tour/<?php echo $tour->idtour; ?>?idioma=en"><i class="fa fa-cog" aria-hidden="true"></i>  <?php echo $GLOBALS['editar_tour'] ?></a>

              <?php } ?>
           </div>

        </div>


      <?php if ($tour->imagen != null){ ?>
          <img class="img-responsive" src="<?php echo base_url();?>archivos/<?php echo $tour->imagen; ?>" alt="">
      <?php }else{ ?>

        <img class="img-responsive" src="<?php echo base_url();?>archivos/placeholder-image.jpg" alt="">
      <?php } ?>


    </div>

    <?php } ?>




</section>




<?php }?>

<?php
  $this->load->view('commons/footer');
?>
</div>




  <div class="setup-modal" id="agregar-tour">
<a href="#" class="close-setm"><i class="fa fa-times"></i></a>
 <div class="title-wizard">
            <h2> <?php echo $GLOBALS['add_tour_es'] ?></h2>
        </div>
        <form action="<?php echo base_url()?>tour_operadora/agregar_tour" method="post" data-toggle="validator" role="form">
 <div class="step-body active" data-step="1">



         <div class="form-group col-xs-12">
             <label for="nombre_es" class="control-label" > <?php echo $GLOBALS['nombre_del_tour_es'] ?></label>
             <input type="text" class="form-control" name="nombre_es" required>

         </div>

         <div class="form-group col-xs-12">
             <label for="desc_es" class="control-label" ><?php echo $GLOBALS['descripcion_del_tour_es'] ?></label>
            <textarea class="form-control" name="desc_es" required cols="30" rows="5"></textarea>

         </div>

         <a class="next-step btn btn-default" data-gostep="2"><?php echo $GLOBALS['btn_siguiente']?></a>

 </div>
      <div class="step-body" data-step="2">
           <div class="form-group col-xs-12">
             <label for="nombre_en" class="control-label" ><?php echo $GLOBALS['nombre_del_tour_en'] ?></label>
             <input type="text" class="form-control" name="nombre_en" required>

         </div>


          <div class="form-group col-xs-12">
             <label for="desc_en" class="control-label" ><?php echo $GLOBALS['descripcion_del_tour_en']?></label>

             <textarea class="form-control" name="desc_en" required cols="30" rows="5"></textarea>

         </div>
         <a class="prev-step btn btn-default" data-gostep="1"><?php echo $GLOBALS['btn_anterior']?></a>
         <a class="next-step btn btn-default" data-gostep="3"><?php echo $GLOBALS['btn_siguiente']?></a>
      </div>
      <div class="step-body" data-step="3">

          <div class="form-group col-xs-12">
             <!-- <label for="precio" class="control-label" >Ingrese la fecha de su tour</label> -->
             <input type="hidden" class="form-control date" name="fecha" value="10-10-2018">

         </div>
         <div class="form-group col-xs-12">
             <label for="zona" class="control-label" ><?php echo $GLOBALS['su_tour_se_realizar_cerca_de_una_playa'] ?> </label>
             <select name="zona" id="" class="form-control">
                <?php foreach($playas as $playa){?>
                    <option value="<?php echo $playa->idplaya; ?>"><?php echo $playa->nombre; ?></option>
                <?php } ?>

             </select>

         </div>

             <a class="prev-step btn btn-default" data-gostep="2"><?php echo $GLOBALS['btn_anterior']?></a>
         <a class="next-step btn btn-default" data-gostep="4"><?php echo $GLOBALS['btn_siguiente']?></a>

      </div>
      <div class="step-body" data-step="4">
          <div class="form-group col-xs-12">
             <label for="precio" class="control-label" > <?php echo $GLOBALS['precio_base_del_tour'] ?></label>
             <input type="text" class="form-control" name="precio" required>

         </div>
             <input type="hidden" class="form-control" name="maximo" required>



           <a class="prev-step btn btn-default" data-gostep="3"><?php echo $GLOBALS['btn_anterior']?></a>
         <a class="next-step btn btn-default" data-gostep="5"><?php echo $GLOBALS['btn_siguiente']?></a>
      </div>
      <div class="step-body" data-step="5">

          <!-- <div class="form-group col-xs-12">
           <label for="estado">Estado</label>
            <select name="estado" id="" class="form-control">

                    <option value="2">Inactivo</option>
                    <option value="1">Activo</option>


             </select>

         </div> -->

         <div class="form-group col-xs-12">
             <input type="submit" class="btn btn-success" value="<?php echo $GLOBALS['add_tour_btn']; ?>">

         </div>
           <a class="prev-step btn btn-default" data-gostep="4"><?php echo $GLOBALS['btn_anterior']?></a>

      </div>

      </form>
</div>


<script>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        },
        responsive: true,

// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container

maintainAspectRatio: false,

    }
});
</script>
