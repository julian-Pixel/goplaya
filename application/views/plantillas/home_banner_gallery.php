<section class="article row">
    
    <h2><?php echo $nombre; ?> </h2>
    
    <div class="col-xs-12 col-md-8 galeria-article">
       
       <ul class="gallery-article">
           <li class="item1"><img class="img-responsive" src="http://placehold.it/300x300" alt=""></li>
           <li class="item2" ><img class="img-responsive" src="http://placehold.it/300x300" alt=""></li>
           <li class="item3" ><img class="img-responsive" src="http://placehold.it/300x300" alt=""></li>
           <li class="item4" ><img class="img-responsive" src="http://placehold.it/300x300" alt=""></li>
       </ul>
        
    </div>
    <div class="col-xs-12 col-md-4 banner-article-c">
       
       <img class="img-responsive" src="<?php echo $imagen; ?>" alt="">
        
    </div>
    <div class="col-xs-12 center-article">
        
        <p>
            <?php echo $extracto; ?> 
        </p>
        <a href=" <?php echo base_url()?>pagina/detalle/<?php echo $slug;?>" class="btn-default-gp btnf btn-corto">Ver más</a>
       
    </div>
    
</section>