<?php
  $this->load->view('commons/header');
?>




<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">
          
           <h2>Editar secciones del home</h2>
         
      </div>
      </div>
        
    </div>   
    <div class="row ctool">
         <a href="<?php echo base_url();?>" target="_blank"  style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><i class="fa fa-eye" aria-hidden="true"></i> Vista previa</a>
        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
    </div>
    
</div>

<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">
 
 <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Secciones Activas</h3>
  </div>
  <div class="panel-body">
  <div class="row slides">
   <ul class="sortable2">
<?php foreach($secciones as $seccion){ ?>

<li id="item_<?php echo $seccion->id; ?>"><span>Sección: </span><?php echo $seccion->modulo; ?>  <i class="fa fa-arrows" aria-hidden="true"></i></li>
<?php } ?>
    </ul>
</div>

  </div>
</div>
 
 
  
</div>

<div class="container-fluid">
   
   <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Agregar secciones</h3>
  </div>
  <div class="panel-body">
       <form action="<?php echo base_url();?>modulos/agregar_home" method="post">
        
        <div class="form-group col-xs-12">
           <label for="seccion">Secciones disponibles</label>
            <select name="seccion" class="form-control" id="">
                <?php foreach($disponibles as $disp){?>
                <option value="<?php echo $disp->id;?>"><?php echo $disp->modulo;?></option>
                <?php } ?>
            </select>
        </div>
        
         <div class="form-group col-xs-12">
          <input type="submit" value="Agregar">
        </div>
        
    </form>
  </div>
</div>
   
</div>
<div class="container-fluid">

<?php
  $this->load->view('commons/footer');
?>
</div>

