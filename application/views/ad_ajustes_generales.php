<?php
  $this->load->view('commons/header');
?>
<div class="save_ajax">
   <img src="<?php echo base_url();?>/theme/img/clock.gif" alt="">
    
</div>



<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">
          
           <h2>Ajustes generales</h2>
         
      </div>
      </div>
        
    </div>   
    <div class="row ctool">
        
        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
    </div>
    
</div>
<div class="container" style="padding-top: 5%;padding-bottom: 5%;">

<form action="<?php echo base_url(); ?>/dashboard/aag" method="post">
     <h4>Datos de contacto</h4>
    <div class="form-group">
        <input type="email" name="email_contacto" class="form-control" placeholder="Email de contacto" value="<?php echo $datos_generales[0]->email_contacto; ?>">
    </div>
    <div class="form-group">
        <input type="text" name="telefono" class="form-control" placeholder="Teléfono" value="<?php echo $datos_generales[0]->telefono; ?>">
    </div> 
    <div class="form-group">
        <textarea name="direccion" class="form-control" id="" cols="30" rows="10" placeholder="Dirección"><?php echo $datos_generales[0]->direccion; ?></textarea>
    </div>
    
     <h4>Redes sociales</h4>
     
     <div class="form-group">
        <input name="facebook" type="text" class="form-control" placeholder="Facebook" value="<?php echo $datos_generales[0]->facebook; ?>">
    </div> 
    
    <div class="form-group">
        <input name="youtube" type="text" class="form-control" placeholder="Youtube" value="<?php echo $datos_generales[0]->youtube; ?>">
    </div>  
    <div class="form-group">
        <input name="instagram" type="text" class="form-control" placeholder="Instagram" value="<?php echo $datos_generales[0]->instagram; ?>">
    </div> 
    
    
    
    <h4>Servidor SMTP</h4>
    
    <div class="form-group">
        <input name="host" type="text" class="form-control" placeholder="SMTP Host" value="<?php echo $datos_generales[0]->smtp_host; ?>">
    </div> 
    <div class="form-group">
        <input name="port" type="text" class="form-control" placeholder="SMTP Port" value="<?php echo $datos_generales[0]->smtp_port; ?>">
    </div>
     <div class="form-group">
        <input name="user" type="text" class="form-control" placeholder="SMTP User" value="<?php echo $datos_generales[0]->smtp_user; ?>">
    </div>
    <div class="form-group">
        <input name="password" type="password" class="form-control" placeholder="SMTP Contraseña" value="<?php echo $datos_generales[0]->smtp_pass; ?>">
    </div>
    
    <div class="form-group">
        <input type="submit" class="btn" value="Actualizar">
    </div>
    
</form>
 
</div>



<div class="container-fluid">



<?php
  $this->load->view('commons/footer');
?>
</div>

