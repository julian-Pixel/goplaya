<?php
  $this->load->view('commons/header');
?>




<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">
          
           <h2>Crear página</h2>
         
      </div>
      </div>
        
    </div>   
    <div class="row ctool">
         <a href=""  style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><i class="fa fa-eye" aria-hidden="true"></i> Vista previa</a>
        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
    </div>
    
</div>
<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">
<div class="col-md-10 col-xs-12 panel-card">
    <form action="<?php echo base_url(); ?>index.php/pagina/agregar_pagina" method="post">
   
  
    
    
    <div class="form-group">
    <label for="nombre">Nombre</label>
    <input type="text" name="nombre" class="form-control" value="" id="titulo" required>    
    </div>
    
   <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><i class="fa fa-link" aria-hidden="true"></i></div>
      <input type="text" name="permalink" class="form-control" id="permalink" required>
      
    </div>
  </div>
    
    
     <!--<div class="form-group">
      <label for="descripcion">Descripción</label>
       <textarea name="descripcion" class="editor" id="" cols="30" rows="10" required></textarea>
   </div>
-->
    <div class="tool-bar">
       <span><i class="fa fa-hand-pointer-o" aria-hidden="true"></i> Editor visual</span>
        <a href="#" id="salvar"><i class="fa fa-floppy-o" aria-hidden="true"></i> Actualizar cambios</a>
    </div>
    
    
    
    
    
    
    
    
    
     <h4>Español</h4>
    
   <div id="myGrid">
       
   </div>
    
    <textarea name="contenido" id="test" cols="30" rows="10"><div class="row"><div class="col-md-4 col-sm-4 col-xs-4 column"><div class="ge-content ge-content-type-ckeditor" data-ge-content-type="ckeditor"><p style="text-align: justify;">Lorem initius...</p></div></div><div class="col-md-4 col-sm-4 col-xs-4 column"><div class="ge-content ge-content-type-ckeditor" data-ge-content-type="ckeditor"><p style="text-align: justify;">Lorem initius...</p></div></div><div class="col-md-4 col-sm-4 col-xs-4 column">
<div class="ge-content ge-content-type-ckeditor" data-ge-content-type="ckeditor"><p style="text-align: justify;">Lorem initius...</p>
</div></div></div></textarea>
  
  <h4>Ingles</h4>
  
  <div id="myGrid2">
       
   </div>
    
    <textarea name="contenido_en" id="test2" cols="30" rows="10"><div class="row"><div class="col-md-4 col-sm-4 col-xs-4 column"><div class="ge-content ge-content-type-ckeditor" data-ge-content-type="ckeditor"><p style="text-align: justify;">Lorem initius...</p></div></div><div class="col-md-4 col-sm-4 col-xs-4 column"><div class="ge-content ge-content-type-ckeditor" data-ge-content-type="ckeditor"><p style="text-align: justify;">Lorem initius...</p></div></div><div class="col-md-4 col-sm-4 col-xs-4 column">
<div class="ge-content ge-content-type-ckeditor" data-ge-content-type="ckeditor"><p style="text-align: justify;">Lorem initius...</p>
</div></div></div></textarea>
   
    

</div>
<div class="col-md-2 col-xs-12 pside">
  <div class="form-group">
  <label for="">Publicar como</label>
  
   <select class="form-control" name="estado" id="">
         
          <option value="1">Borrador</option>
          <option value="2">Publicado</option>
       
      </select>
      <input type="submit" value="Publicar"  class="btn-default-gp-v">
    </div>
    
    <div class="form-group">
  <label for="">Categoría</label>
  
   <select class="form-control" name="categoria" id="">
         
          <option value="1">Artículo</option>
          <option value="2">Página</option>
       
      </select>
    
    </div>
  <div class="form-group">
    
      <label for="descripcion">Plantilla</label>
      
      <select class="form-control" name="plantilla" id="">
          <?php foreach($plantillas as $plantilla){?>
          <option value="<?php echo $plantilla; ?>"><?php echo $plantilla; ?></option>
          <?php } ?>
      </select>
   </div>
    
</div>


</form>
 
</div>
<div class="container-fluid">

<?php
  $this->load->view('commons/footer');
?>
</div>

