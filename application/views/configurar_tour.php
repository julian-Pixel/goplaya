<?php
  $this->load->view('commons/header');
?>




<div class="container-fluid" style="background: #f5f5f5;">


<div class="row to-cintillo">

    <div class="col-xs-8">
      <div class="profile">
          <img src="<?php if(isset($foto_perfil)){ echo $foto_perfil; } ?>" class="img-responsive" alt="">
           <h2><?php echo $GLOBALS['Bienvenido de nuevo']; ?>, <span><?php echo $datos_operadora[0]->nombre; ?></span> </h2>

      </div>


    </div>

    <div class="col-xs-2 codigo_comercio">
          <p>Código de comercio: <span style="font-size: 21px;color: rgba(245, 245, 245, 0.67);"><?php echo $datos_operadora[0]->codigo; ?></span></p>
    </div>


</div>
<div class="row ctool">

    <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><?php echo $GLOBALS['volver_al_dashboard'];?></a>
</div>

<div class="col-xs-12 col-md-9">


    <div class="col-xs-12">

        <div class="panel panel-default">

  <div class="panel-body config-tour">
    <form id="formupdate" >

      <input type="hidden" name="id" value="<?php echo $tour[0]->idtour; ?>">

      <div>

  <!-- Nav tabs -->


  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="es">
<!-- Español-->

   <?php if ($rol_user[0]->rol  == 1) { ?>

        <div class="form-group">
            <label  class="config-label"for=""><?php echo $GLOBALS['estado'] ?></label>
            <select class="form-control" name="estado_tour" id="estado_tour">
              <?php if($tour[0]->habilitado == 1){ ?>
                <option value="1">Activado</option>
                <option value="0">Desactivado</option>
              <?php }else{ ?>
                <option value="0">Desactivado</option>
                <option value="1">Activado</option>
                  <?php } ?>
            </select>
        </div>

        <div class="form-group">
            <label  class="config-label"for=""><?php echo $GLOBALS['destacar_tour'] ?></label>
            <select class="form-control" name="destacado_tour" id="destacado_tour">
              <?php if($tour[0]->destacado == 1){ ?>
                <option value="1"><?php echo $GLOBALS['si'] ; ?></option>
                <option value="0"><?php echo $GLOBALS['no'] ; ?></option>
              <?php }else{ ?>
                <option value="0"><?php echo $GLOBALS['no'] ; ?></option>
                <option value="1"><?php echo $GLOBALS['si'] ; ?></option>
                  <?php } ?>
            </select>
        </div>

      <?php } ?>
        <div class="form-group">
           <label class="config-label" for=""><?php echo $GLOBALS['nombre_del_tour_es']; ?></label>
           <input type="text" class="form-control" name="nombre" id="nombre" placeholder="" value="<?php echo $tour[0]->nombre; ?>">
       </div>

       <div class="form-group">
                  <label class="config-label" for=""><?php echo $GLOBALS['nombre_del_tour_en'] ?></label>
                  <input type="text" class="form-control" name="nombreen" id="nombreen" placeholder="" value="<?php echo $tour[0]->nombre_eng; ?>">
        </div>



        <div class="form-group">
             <label class="config-label" for=""><?php echo $GLOBALS['duracion_es'] ?> </label>
             <input type="text" class="form-control" name="duracion" id="duracion" placeholder="" value="<?php echo $tour[0]->duracion; ?>">
         </div>

         <div class="form-group">
              <label class="config-label" for=""><?php echo $GLOBALS['duracion_en'] ?> </label>
              <input type="text" class="form-control" name="duracion_eng" id="duracion_eng" placeholder="" value="<?php echo $tour[0]->duracion_eng; ?>">
          </div>


              <div class="form-check">

                <?php if($tour[0]->reviews == 1){ ?>
                      <input type="checkbox" class="form-check-input" value="0" name="reviews" id="reviews" checked>
                <?php } else { ?>
                      <input type="checkbox" class="form-check-input" value="0" name="reviews" id="reviews">
                <?php } ?>


                <label  class="form-check-label" for="exampleCheck1"><?php echo $GLOBALS['habilitarestrellas'] ?></label>
              </div>

              <div class="form-check">

                <?php if($tour[0]->opinion == 1){ ?>
                      <input type="checkbox" class="form-check-input" value="0" name="opinion" id="opinion" checked>
                <?php } else { ?>
                      <input type="checkbox" class="form-check-input" value="0" name="opinion" id="opinion">
                <?php } ?>


                <label class="form-check-label" for="exampleCheck2"><?php echo $GLOBALS['habilitaropinionesclientes'] ?></label>
              </div>

              <br>

          <div class="form-group">
           <label for=""><?php echo $GLOBALS['descripcion_del_tour_es'] ?></label>
           <textarea name="descripcion" class="editor4" rows="8" cols="80"><?php echo $tour[0]->descripcion; ?></textarea>
       </div>



       <div class="form-group">
            <label for=""><?php echo $GLOBALS['itinerario_es'] ?></label>
             <textarea name="itinerario" class="editor3" rows="8" cols="80"><?php echo $tour[0]->itinerario_es; ?></textarea>
        </div>

        <div class="form-group">
             <label for=""><?php echo $GLOBALS['detalles_tour_es'] ?></label>
              <textarea name="detalle" class="editor7" rows="8" cols="80"><?php echo $tour[0]->detalle; ?></textarea>
         </div>

         <div class="form-group">
              <label for=""><?php echo $GLOBALS['cancelacion_es']?></label>
               <textarea name="cancelacion" class="editor9" rows="8" cols="80"><?php echo $tour[0]->cancelacion; ?></textarea>
          </div>


        <div class="form-group">
         <label for=""><?php echo $GLOBALS['descripcion_del_tour_en'] ?></label>
          <textarea name="descripcionen" class="editor2" rows="8" cols="80"><?php echo $tour[0]->descripcion_eng; ?></textarea>
     </div>

     <div class="form-group">
          <label for=""><?php echo $GLOBALS['itinerario_en']  ?></label>
           <textarea name="itinerarioeng" class="editor" rows="8" cols="80"><?php echo $tour[0]->itinerario_en; ?></textarea>
      </div>

      <div class="form-group">
           <label for=""><?php echo $GLOBALS['detalles_tour_en'] ?></label>
            <textarea name="detalle_eng" class="editor8" rows="8" cols="80"><?php echo $tour[0]->detalle_eng; ?></textarea>
       </div>

       <div class="form-group">
            <label for=""><?php echo $GLOBALS['cancelacion_en']?></label>
             <textarea name="cancelacion_eng" class="editor10" rows="8" cols="80"><?php echo $tour[0]->cancelacion_eng; ?></textarea>
        </div>

<!-- Fin español-->


    </div>

  </div>

  <div class="col-xs-12">
        <label for=""><?php echo $GLOBALS['caracteristicas_tour']?></label>
  </div>

<div class="form-group col-xs-12 col-md-6">

           <select class="form-control" name="categoria" id="categoria">
              <?php
                foreach($actividades as $actividad){
               ?>
               <option value="<?php echo $actividad->idactividad; ?>" <?php if($actividad->idactividad==$tour[0]->idcategoria){echo'selected';}?>><?php echo $actividad->detalle; ?></option>

               <?php
                }
               ?>
           </select>
       </div>

       <div class="form-group col-xs-12 col-md-6">

                  <select class="form-control" name="categoria2" id="categoria2">
                     <?php
                       foreach($actividades as $actividad){
                      ?>
                      <option value="<?php echo $actividad->idactividad; ?>" <?php if($actividad->idactividad==$tour[0]->idcategoria){echo'selected';}?>><?php echo $actividad->detalle; ?></option>

                      <?php
                       }
                      ?>
                  </select>
              </div>

       <div class="form-group col-xs-12">
           <label for=""><?php echo $GLOBALS['su_tour_se_realizar_cerca_de_una_playa'] ?></label>
           <select class="form-control" name="playa" id="playa">
              <?php
                foreach($playas as $playa){
               ?>
               <option value="<?php echo $playa->idplaya; ?>"  <?php if($playa->idplaya==$tour[0]->id_playa){echo'selected';}?> ><?php echo $playa->nombre; ?></option>

               <?php
                }
               ?>
           </select>
       </div>
        <!-- <div class="form-group col-xs-12 col-md-6">
           <label for="">Fecha del tour</label>
           <input type="text" class="form-control date-range" name="fecha" id="" placeholder="" value="<?php echo $tour[0]->fecha_tour; ?> - <?php echo $tour[0]->fecha_final_tour; ?>">
       </div> -->
        <!-- <div class="form-group col-xs-12 col-md-6">
           <label for="">Capacidad</label>
           <input type="text" class="form-control" name="maximo" id="" placeholder="" value="<?php echo $tour[0]->maximo_personas; ?>">
       </div> -->
       <div class="form-group col-xs-12 col-md-12" >
           <label for=""> <?php echo $GLOBALS['precio_base_del_tour'] ?></label>
           <input type="text" class="form-control" name="precio" id="precio" placeholder="" value="<?php echo $tour[0]->precio; ?>">
       </div>

         <div class="form-group col-xs-12 col-md-4">
           <!-- <label for=""> <?php echo $GLOBALS['descuento_tour'] ?></label> -->
           <input type="hidden" class="form-control" name="descuento" id="descuento" placeholder="" value="<?php echo $tour[0]->porcetaje_descuento * 100; ?>">
       </div>

       <!-- <div class="form-group col-xs-12 col-md-4">
        <label for="">Estado</label>
           <select name="estado" class="form-control" id="estado">
               <option value="1" <?php if($tour[0]->estado=='1'){echo'selected';}?></optio>Activo</option>
               <option value="2" <?php if($tour[0]->estado=='2'){echo'selected';}?>>Inactivo</option>
           </select>
 </div> -->



</div>
<div class="col-xs-12">


<h3><?php echo $GLOBALS['Punto_de_encuentro'] ?></h3>
  </div>
<div class="form-group col-md-6 col-xs-12">



<label for="latitud"><?php echo $GLOBALS['latitud']?></label>
<input type="text" id="latitudfrm" class="form-control numero2" name="latitud" value="<?php if(!empty($tour[0]->lat)){ echo $tour[0]->lat; }else{ echo '9.72268';} ?>">
</div>
<div class="form-group col-md-6 col-xs-12">
<label for="longitud"><?php echo $GLOBALS['longitud']?></label>
<input type="text" id="longitud" class="form-control numero2" name="longitud" value="<?php if(!empty($tour[0]->lng)){ echo $tour[0]->lng; }else{ echo '-84.6524';} ?>">
</div>
<div class="col-xs-12" style="position:relative;">

<input id="pac-input" class="controls" type="text" placeholder="Search Box">



<div id="map" class="clearfix"></div>





</div>


<div class="col-xs-12">
  <br>
  <div class="form-group">
   <label for=""> <?php echo $GLOBALS['Punto_de_encuentro_direccion'] ?></label>
    <textarea name="direccion_exacta" class="editor5" rows="5" cols="80"><?php echo $tour[0]->direccion_exacta; ?></textarea>
  </div>
</div>


<div class="col-xs-12">
  <br>
  <div class="form-group">
   <label for=""><?php echo $GLOBALS['Punto_de_encuentro_direccion_en'] ?></label>
    <textarea name="direccion_exacta_eng" class="editor6" rows="5" cols="80"><?php echo $tour[0]->direccion_exacta_eng; ?></textarea>
  </div>
</div>









   </form>
  </div>
</div>



    </div>

    <div class="col-xs-12" id="cupo">
           <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo $GLOBALS['Tour disponible '] ?></h3>
    <ul class="tool-section-bar">
          <li><a class="btn-config" href="#" data-toggle="modal" data-target="#modaldescuentos"><?php echo $GLOBALS['agregar_disponible'] ?><i class="fa fa-plus"></i></a></li>
      </ul>
  </div>
  <div class="panel-body">


    <table id="tablecupos" class="table table-striped">

        <thead>
            <tr>
                <th><?php echo $GLOBALS['fecha'] ?></th>
                <th><?php echo $GLOBALS['hora'] ?></th>
                <th><?php echo $GLOBALS['disponibilidad'] ?></th>
                <th><?php echo $GLOBALS['acciones'] ?></th>
            </tr>
        </thead>
        <tbody>
          <?php foreach ($cupos as $cupo) { ?>

          <tr id="<?php echo $cupo->id; ?>">
              <?php $date=date_create($cupo->fecha);  ?>
              <td><?php echo date_format($date,"d-m-Y"); ?></td>
              <td><?php echo $cupo->hora; ?></td>
              <td><?php echo $cupo->cupos; ?></td>
                <td> <a class="borrarcupo" href="javascript:void(0)" data-value1="<?php echo $cupo->id; ?>" data-value2="<?php echo  $tour[0]->idtour; ?>">Eliminar</a> </td>
              <!-- <td> <a href="<?php echo base_url(); ?>index.php/tour_operadora/eliminar_cupo?id=<?php echo $cupo->id; ?>&tour=<?php echo  $tour[0]->idtour; ?>">Eliminar</a> </td> -->

          </tr>



        <?php } ?>

        </tbody>

    </table>

    </div>

</div>
</div>  <div class="col-xs-12" id="precios">
           <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo $GLOBALS['precio_por_tipo_de_persona'] ?></h3>
    <ul class="tool-section-bar">
          <li><a class="btn-config" href="#" data-toggle="modal" data-target="#modalprecios"><?php echo $GLOBALS['agregar_precio'] ?><i class="fa fa-plus"></i></a></li>
      </ul>
  </div>
  <div class="panel-body">


    <table id="tableprecios" class="table table-striped">

        <thead>
            <tr>
                <th><?php echo $GLOBALS['tipo_cliente'] ?></th>
                <th><?php echo $GLOBALS['precio_base']  ?></th>
                <th><?php echo $GLOBALS['descuento'] ?></th>
                <th><?php echo $GLOBALS['estado'] ?></th>
                <th><?php echo $GLOBALS['acciones'] ?></th>
            </tr>
        </thead>
        <tbody>

          <?php  foreach ($precios as $precio) {


            if($precio->tipo_cliente =='1'){
              $name = 'Adultos';

            }elseif($precio->tipo_cliente =='2'){
              $name = 'Estudiantes';
            }elseif($precio->tipo_cliente =='3'){
              $name = 'Niños';
            }elseif($precio->tipo_cliente =='4'){
              $name = 'Menores';
            }else{
              $name = 'Otro';

            }



             ?>

           <tr id="<?php echo $precio->id; ?>">
               <td><?php echo   $name; ?></td>
               <td><?php echo $tour[0]->precio; ?></td>
               <td><?php echo round( $precio->descuento * 100 ), '%' ; ?> </td>
               <td><?php echo $precio->estado; ?></td>
               <!-- <td><a href="<?php echo base_url(); ?>index.php/tour_operadora/eliminar_precio_tour?id=<?php echo $precio->id; ?>&tour=<?php echo  $tour[0]->idtour; ?>">Eliminar</a></td> -->
               <td> <a class="borrarprecio" href="javascript:void(0)" data-value1="<?php echo $precio->id; ?>" data-value2="<?php echo  $tour[0]->idtour; ?>">Eliminar</a> </td>


           </tr>
        <?php } ?>
        </tbody>

    </table>

    </div>

</div>
    </div>



  <form id="seo">
    <div class="col-xs-12">
           <div class="panel panel-default">
                 <?php if ($rol_user[0]->rol  == 1) { ?>
  <div class="panel-heading">
    <h3 class="panel-title">SEO</h3>

  </div>
  <div class="panel-body">


      <input type="hidden" name="id" value="<?php echo $tour[0]->idtour; ?>">
      <div class="form-group">
        <label for=""><?php echo $GLOBALS['Meta_title_es']?></label>
        <input type="text" name="meta_title_es" value="<?php echo $tour[0]->meta_title_es; ?>" class="form-control">
      </div>
      <div class="form-group">
        <label for=""><?php echo $GLOBALS['Meta_title_en'] ?></label>
          <input type="text" name="meta_title_en" value="<?php echo $tour[0]->meta_title_en; ?>" class="form-control">
      </div>





      <div class="form-group">
        <label for=""><?php echo $GLOBALS['Meta_description_es'] ?></label>
          <textarea name="meta_desc_es" rows="4" class="form-control"><?php echo $tour[0]->meta_desc_es; ?></textarea>
      </div>
      <div class="form-group">
        <label for=""><?php echo $GLOBALS['Meta_description_en'] ?></label>
        <textarea name="meta_desc_en" rows="4" class="form-control"><?php echo $tour[0]->meta_desc_en; ?></textarea>
      </div>
      <div class="form-group">
        <label for=""><?php echo $GLOBALS['keywords_es'] ?></label>
        <textarea name="keywords_es" rows="4" class="form-control"><?php echo $tour[0]->keywords_es; ?></textarea>
      </div>

      <div class="form-group">
        <label for=""><?php echo $GLOBALS['keywords_en'] ?></label>
        <textarea name="keywords_en" rows="4" class="form-control"><?php echo $tour[0]->keywords_en; ?></textarea>
      </div>




    </div>
        <?php } ?>
  </form>






</div>
    </div>

    <div class="col-xs-12">
      <br>
    <input id="btnupdate" type="button" value="<?php echo $GLOBALS['guardar'] ?>" class="btn btn-default">
    </div>

</div>

<div class="col-xs-12 col-md-3">

     <div class="galery-header clearfix">
          <h4><?php echo $GLOBALS['fotografia_destacada'] ?></h4>

      </div>
   <div class="img-destacada" id="img-destacada" data-toggle="modal"  data-target="#fotoModal">
    <img id="imgd"  class="img-thumbnail" style="width: 100%;" src="<?php echo base_url();?>archivos/<?php echo $tour[0]->imagen; ?>" alt="">
    <div class="overlay"><i class="fa fa-camera-retro"></i></div>
    </div>

    <div class="galery-header clearfix">
          <h4><?php echo $GLOBALS['galeria'] ?></h4>

      </div>
      <a href="#" class="btn-default-gp  btn-default-gp-v" data-toggle="modal"  data-target="#galeriaModal" ><?php echo $GLOBALS['anadir_item'] ?></a>

<section class="galeria-playa">
      <ul class="sortable4">
      <?php foreach($galeria as $item){?>

      <li id="item_<?php echo $item->id; ?>">

      <?php
      $icono = 'fa fa-picture-o';
      $url = base_url().'archivos/'.$item->url;

      ?>

      <img src="<?php echo $url; ?>" alt="">
      <div class="overlay">
      <a href="#" class="ecf" data-toggle="modal" data-id="<?php echo $item->id; ?>" data-target="#efModal" style="top: 36px;"><i class="fa fa-pencil fonc"></i></a>
      <a href="<?php echo base_url()?>index.php/provincias/eliminar_item_galeria_tour_tour?id=<?php echo $item->id; ?>&playa=<?php echo  $tour[0]->idtour; ?>"><i class="fa fa-times fonc"></i></a>
      <i class="<?php echo $icono; ?>" aria-hidden="true"></i>
      </div>
      </li>

      <?php } ?>

      </ul>
    </section>

</div>

<div class="container" id="panel_reservaciones">





</div>


<div class="row" style="background:white;padding-top:5%;padding-bottom:5%">


    <div class="container">



    </div>

</div>




<!-- Modal -->
<div class="modal fade" id="fotoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" style="width:80%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo  $GLOBALS['agregar_fotografia_destacada'] ?></h4>
      </div>
      <div class="modal-body clearfix">
       <input type="hidden" class="id" id="photoid" value="<?php echo $tour[0]->idtour; ?>">
        <div class="col-md-12 text-center">
				<div id="upload-demo2" style="width:100%"></div>
	  		</div>

	  		<div class="col-md-12">
				<input class="filestyle" type="file" id="upload2" data-buttonText="<?php echo $GLOBALS['buscar_imagen'] ?>" data-icon="false">
				<br/>
				<div id="vpp"></div>
     <button class="btn btn-theme upload-result-tour" ><?php echo $GLOBALS['subir_imagen'] ?></button>
	  		</div>

      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="modaldescuentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" style="width:80%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $GLOBALS['agregar'] ?></h4>
      </div>
      <div class="modal-body clearfix">

        <form id="formcupos" class="" action="<?php echo base_url();?>index.php/tour_operadora/agregar_cupo" method="post">
          <input type="hidden" name="id" value="<?php echo $tour[0]->idtour; ?>">
          <input type="hidden" name="idcupo" value="<?php echo $precio->id; ?>">
          <div class="form-group">
            <label for=""><?php echo $GLOBALS['fecha'] ?></label>
            <input type="text" name="fecha" class="datetimepicker form-control" value="">
          </div>
          <div class="form-group">
            <label for=""><?php echo $GLOBALS['hora'] ?></label>
            <input type="text" name="time" class="datetimepicker2 form-control" value="">
          </div>

          <div class="form-group">
            <label for="">Cupos</label>
            <input type="text" name="cupos" class="time form-control" value="">
          </div>

          <div class="form-group">

            <input id="btncupos" type="button" class="btn btn-success" value="<?php echo $GLOBALS['agregar'] ?>">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalprecios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" style="width:80%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $GLOBALS['agregar_precio']  ?></h4>
      </div>
      <div class="modal-body clearfix">

        <form id="formprecios" class="" action="<?php echo base_url();?>index.php/tour_operadora/agregar_precio" method="post">
          <input type="hidden" name="id" value="<?php echo $tour[0]->idtour; ?>">
          <div class="form-group">
            <label for=""><?php echo $GLOBALS['tipo_cliente'] ?></label>
            <select class="form-control" name="cliente">

              <option value="1">Adultos</option>
              <option value="2">Estudiantes</option>
              <option value="3">Niños</option>
              <option value="4">Menores</option>

            </select>
          </div>
              <input type="hidden" name="precio" value="<?php echo $tour[0]->precio; ?>">
          <div class="form-group">
            <label for=""><?php echo $GLOBALS['Porcentaje de descuento']; ?></label>
            <select class="form-control" name="descuento">

              <option value="0">0%</option>
              <option value="0.05">5%</option>
              <option value="0.10">10%</option>
              <option value="0.15">15%</option>
              <option value="0.20">20%</option>
              <option value="0.25">25%</option>
              <option value="0.30">30%</option>
              <option value="0.35">35%</option>
              <option value="0.40">40%</option>
              <option value="0.45">45%</option>
              <option value="0.50">50%</option>
              <option value="0.55">55%</option>
              <option value="0.60">60%</option>
              <option value="0.65">65%</option>
              <option value="0.70">70%</option>
              <option value="0.75">75%</option>
              <option value="0.80">80%</option>
              <option value="0.85">85%</option>
              <option value="0.90">90%</option>
              <option value="0.95">95%</option>
              <option value="1">100%</option>

            </select>
          </div>

          <div class="form-group">
            <label for=""><?php echo $GLOBALS['estado'] ?></label>
            <select class="form-control" name="estado">

              <option value="1"><?php echo  $GLOBALS['Activo']; ?></option>
              <option value="2"><?php echo  $GLOBALS['Inactivo']; ?></option>

            </select>
          </div>

          <div class="form-group">

            <input id="btnprecios" type="button" class="btn btn-success" value="<?php echo $GLOBALS['agregar'] ?>">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="galeriaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel"><?php echo $GLOBALS['agregar_foto_galeria'] ?></h4>
</div>
<div class="modal-body clearfix">
  <div class="col-md-12 text-center">
  <div id="upload-demo3" style="width:100%;display:none"></div>
  </div>


<form enctype="multipart/form-data" accept-charset="utf-8" id="formgalerias" method="post" >
<input type="hidden" class="id" name="tourid" id="playaid" value="<?php echo $tour[0]->idtour; ?>">
<div class="form-group">

<h4><?php echo $GLOBALS['Tipo_de_item']?></h4>


<div class="galery-type col-xs-12 gsp">
<div class="form-group txtcenter">

<input type="radio" value="1" class="tipo_item" name="tipo_item" id="imageg" checked />
<label for="imageg" ></label>
</div>
</div>





</div>
<div class="col-md-12 galeria-foto">
<input class="filestyle" type="file" id="txtfileimage" data-buttonText="<?php echo $GLOBALS['buscar_imagen'] ?>" name="uploadedimages[]" data-icon="false" multiple>
	<!-- <input class="filestyle" type="file" id="upload3" data-buttonText="<?php echo $GLOBALS['buscar_imagen'] ?>" data-icon="false"> -->
<br/>


</div>


<div id="vppcaption1"></div>
<input type="button" value="<?php echo $GLOBALS['agregar'] ?>" class="btn btn-theme  btn-default-gp-v" id="btn-galerias">


</form>

</div>
</div>
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="efModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Editar foto</h4>
</div>
<div class="modal-body">
<form action="" id="upcaption">

<input type="hidden" name="id" id="elementid">

<input type="text" name="caption" class="form-control" id="fcaption" placeholder="Caption">
<div id="vppcaption"></div>
<button type="button" id="upcaption_tour">Actualizar</button>
</form>
</div>

</div>
</div>
</div>




<?php
  $this->load->view('commons/footer');
?>
</div>


<!--Mapa -->

<script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initAutocomplete() {
var latf = parseFloat($('#latitudfrm').val());
var lnf = parseFloat($('#longitud').val());


var myLatLng = {lat: latf, lng: lnf };
var map = new google.maps.Map(document.getElementById('map'), {

center: myLatLng,
zoom: 13,
mapTypeId: google.maps.MapTypeId.ROADMAP
});

var marker = new google.maps.Marker({
position: myLatLng,
map: map,
draggable:true,
title: 'Hello World!'
});

// Create the search box and link it to the UI element.
var input = document.getElementById('pac-input');
var searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

// Bias the SearchBox results towards current map's viewport.
map.addListener('bounds_changed', function() {
searchBox.setBounds(map.getBounds());
});

var markers = [];
// [START region_getplaces]
// Listen for the event fired when the user selects a prediction and retrieve
// more details for that place.
searchBox.addListener('places_changed', function() {
var places = searchBox.getPlaces();

if (places.length == 0) {
return;
}

// Clear out the old markers.
markers.forEach(function(marker) {
marker.setMap(null);
});
markers = [];

// For each place, get the icon, name and location.
var bounds = new google.maps.LatLngBounds();
places.forEach(function(place) {
var icon = {
url: place.icon,
size: new google.maps.Size(71, 71),
origin: new google.maps.Point(0, 0),
anchor: new google.maps.Point(17, 34),
scaledSize: new google.maps.Size(25, 25)
};

// Create a marker for each place.
markers.push(new google.maps.Marker({
map: map,
icon: icon,
title: place.name,
draggable:true,
position: place.geometry.location
}));

$('#latitudfrm').val(place.geometry.location.lat());
$('#longitud').val(place.geometry.location.lng());

if (place.geometry.viewport) {
// Only geocodes have viewport.
bounds.union(place.geometry.viewport);
} else {
bounds.extend(place.geometry.location);
}
});
map.fitBounds(bounds);



});


google.maps.event.addListener(marker, 'dragend', function()
{


$('#latitudfrm').val(marker.position.lat());
$('#longitud').val(marker.position.lng());
});






}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5742avOaS0L-PGIhGGYe4soqYExhlB-g&libraries=places&callback=initAutocomplete" async defer></script>
<!--Fin mapa -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>



<script type="text/javascript">
      $(function () {
          $('.datetimepicker').datetimepicker({

              format: 'DD-MM-YYYY'
          });

          $('.datetimepicker2').datetimepicker({

              format: 'HH:mm:ss'
          });
      });
  </script>
