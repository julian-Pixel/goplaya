<div class="container-fluid">
  <div class="row footer">
<!-- <ul>
<?php  foreach($menu_home as $menu){?>
<?php if( $_SESSION['idioma']   == 'es' ){ ?>
<li><a href="<?php echo $menu->enlace; ?>"><?php echo $menu->detalle; ?></a></li>
<? }else if( $_SESSION['idioma']   == 'en' ){?>
<li><a href="<?php echo $menu->enlace; ?>"><?php echo $menu->detalle_en; ?></a></li>
<?php }?>
<?php } ?>
</ul> -->


<ul>




  <?php if( $_SESSION['idioma']   == 'es' ){ ?>
    <li><a href="http://dev.goplaya.cr/search_controller/buscar">Buscador de playas</a></li>
    <!-- <li><a href="http://http://dev.goplaya.cr/index.php/tours">Tours</a></li> -->
    <li><a href="https://www.booking.com/country/cr.html?aid=1333737">Hoteles</a></li>
    <li><a href="http://dev.goplaya.cr/rentacar">Rent a car</a></li>
    <li><a href="https://goplaya.cr/crea_tu_ruta">Crea tu ruta</a></li>
    <li><a href="https://goplaya.cr/blog/es">Blog</a></li>
    <li><a href="http://dev.goplaya.cr/index.php/pagina/detalle/conozcanos">Conózcanos</a></li>
    <li><a href="http://dev.goplaya.cr/tour_operadora/registrarme#step-1">Trabajemos juntos</a></li>
    <li><a href="#" class="login-action">Iniciar Sesión</a></li>
  <? }else if( $_SESSION['idioma']   == 'en' ){?>
    <li><a href="http://dev.goplaya.cr/search_controller/buscar">Beach explorer</a></li>
    <li><a href="http://dev.goplaya.cr/index.php/tours">Tours</a></li>
    <li><a href="https://www.booking.com/country/cr.html?aid=1333737">Hotels</a></li>
    <li><a href="http://dev.goplaya.cr/rentacar">Rent a car</a></li>
    <li><a href="https://goplaya.cr/crea_tu_ruta">Create your route</a></li>
    <li><a href="https://goplaya.cr/blog/es">Blog</a></li>
    <li><a href="http://dev.goplaya.cr/index.php/pagina/detalle/conozcanos">About us</a></li>
    <li><a href="http://dev.goplaya.cr/tour_operadora/registrarme#step-1">Let´s work together</a></li>
      <li><a href="#" class="login-action">Log in</a></li>
    <?php }?>

</ul>


<ul>
<?php if( $_SESSION['idioma']   == 'es' ){ ?>
      <li><a href="<?php echo base_url();?>pagina/detalle/terminos-y-condiciones">Términos y condiciones</a></li>
      <li><a href="<?php echo base_url();?>pagina/detalle/politicas-de-privacidad">Políticas de privacidad</a></li>
<? }else if( $_SESSION['idioma']   == 'en' ){?>
<li><a href="<?php echo base_url();?>pagina/detalle/terminos-y-condiciones">Terms and Conditions</a></li>
         <li><a href="<?php echo base_url();?>pagina/detalle/politicas-de-privacidad">Privacy policies</a></li>
  <?php }?>
 <li><a href="mailto:info@goplaya.cr">info@goplaya.cr</a></li>
   </ul>
   <ul>
       <li><a target="_blank" href="<?php echo $datos_generales[0]->facebook; ?>"><i class="fa fa-facebook"></i></a></li>
       <li><a target="_blank" href="<?php echo $datos_generales[0]->instagram; ?>"><i class="fa fa-instagram"></i></a></li>
       <li><a target="_blank" href="<?php echo $datos_generales[0]->youtube; ?>"><i class="fa fa-youtube-play"></i></a></li>
  </ul>
<a class="creditos" target="_blank" href="http://www.pixelcr.com">Diseño por &nbsp; &nbsp;<img style="width: 80px;" src="<?php echo base_url(); ?>theme/img/logo-pixel.png" alt=""></a>
</div>
</div>

</div>
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>theme/img/favicons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>theme/img/favicons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>theme/img/favicons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>theme/img/favicons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>theme/img/favicons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>theme/img/favicons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>theme/img/favicons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>theme/img/favicons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>theme/img/favicons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>theme/img/favicons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>theme/img/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>theme/img/favicons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>theme/img/favicons/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>theme/img/favicons/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<!-- Bootstrap -->
<link href="<?php echo base_url(); ?>theme/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700|Roboto" rel="stylesheet">
<link href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/css/jquery.fonticonpicker.min.css" />
<link href="<?php echo base_url(); ?>theme/css/jquery.fonticonpicker.darkgrey.css" rel="stylesheet">
<!-- Font -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/css/icomoon/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/css/ni/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/fonts/hotel/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/fonts/rentacar/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/fonts/hotel-sept/style.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/css/simditor.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/css/croppie.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/css/easydropdown.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/css/skins/flat/blue.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/css/swipebox.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/css/grideditor.css" />
<link href="<?php echo base_url(); ?>theme/css/grideditor-font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link href="<?php echo base_url(); ?>theme/css/calendar.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>theme/css/custom.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>theme/css/mobile.css" rel="stylesheet">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/jquery.flexslider-min.js"></script>
<link href="<?php echo base_url(); ?>theme/css/flexslider.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>theme/css/daterangepicker.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>theme/css/jquery-entropizer.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>theme/css/ion.rangeSlider.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>theme/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>theme/css/introjs.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>theme/js/stelar.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/module.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/hotkeys.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/uploader.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/simditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/croppie.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/bootstrap-filestyle.js"></script>
<script src="<?php echo base_url(); ?>theme/js/validator.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js"></script>
<script src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
<link href="<?php echo base_url(); ?>theme/css/smart_wizard.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/jquery.smartWizard.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/jquery.fonticonpicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/jquery.easydropdown.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/icheck.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/jquery.swipebox.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/slugger.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/jquery.grideditor.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/underscore-min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/entropizer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/jquery-entropizer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/ion.rangeSlider.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/intro.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/wow.min.js"></script>
 <!-- Ckeditor js -->
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="slick/slick.min.js"></script>
<link href="<?php echo base_url(); ?>theme/css/wickedpicker.min.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/wickedpicker.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/functions.js"></script>


<script>
 new WOW().init();
</script>


<script>

	   $('.flexslider').flexslider({
      animation: "slide"
    });

var token = '5607590505.1a2cee7.55b5f908bdd84408ba848e0e7b73da67',
    num_photos = 8;

$.ajax({
	url: 'https://api.instagram.com/v1/users/self/media/recent',
	dataType: 'jsonp',
	type: 'GET',
	data: {access_token: token, count: num_photos},
	success: function(data){
 		console.log(data);
		for( x in data.data ){
			$('#rudr_instafeed').append('<li><div class="over-ins"><a href="https://www.instagram.com/goplaya/"><i class="fa fa-instagram"></i></a></div><img src="'+data.data[x].images.standard_resolution.url+'"></li>');
		}
	},
	error: function(data){
		console.log(data);
	}
});

</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-101657854-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
$('.featured').slick({
dots: false,
 arrows : false,
infinite: false,
speed: 300,
slidesToShow: 4,
slidesToScroll: 4,
margin: '4px', // perhaps % values as well?
responsive: [
  {
    breakpoint: 1024,
    settings: {
      dots: false,
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      dots: true
    }
  },
  {
    breakpoint: 600,
    settings: {
      dots: false,
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      dots: false,
      slidesToShow: 2,
      slidesToScroll: 2
    }
  }

]
});
</script>
</body>

</html>
