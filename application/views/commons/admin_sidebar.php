<div class="side_bar_menu">
             <ul>
                 <li><a href="#" class="active"> <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li>
                 <li><a href="#"> <i class="fa fa-globe" aria-hidden="true"></i> Lugares</a></li>
                 <li><a href="#"> <i class="fa fa-pagelines" aria-hidden="true"></i> Playas</a></li>
                 <li><a href="#"> <i class="fa fa-sun-o" aria-hidden="true"></i> Tours</a></li>
                 <li><a href="#"> <i class="fa fa-bullhorn" aria-hidden="true"></i> Anuncios</a></li>
                 <li><a href="#"> <i class="fa fa-money" aria-hidden="true"></i> Facturas</a></li>
                 <li><a href="#"> <i class="fa fa-user" aria-hidden="true"></i> Usuarios <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                     
                     <ul class="sub-menu-admin">
                         <li><a href="#">Roles de usuarios</a></li>
                     </ul>
                 
                 </li>
             </ul>
         </div>