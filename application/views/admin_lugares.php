<?php
  $this->load->view('commons/header_dashboard');
?>

   
   <?php

    if(isset($_GET['e'])){
        
        
        if($_GET['e']==1){
            
           $msjt = 'Acción completada';
           $msj = 'Se ha completado correctamen la accioón en el sistema';
            
        }else{
            $msjt = 'Acción no completada';
            $msj = 'Ocurrio un problema, intente de nuevo mas adelante.';
            
        }
        ?>
        
        
        <div class="notificacion">
           <div class="icono">
               <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
           </div>
           <div class="contenido">
            <h2><?php echo $msjt?></h2>
            <p><?php echo $msj?></p>
            </div>
        </div>
        
        
        <?php
        
    }

?>
   
   
   
    <style>
        body {
            background: #f4f4f4;
        }
    </style>
    <div class="row">
        <div class="col-md-2" id="side_bar">
            <div class="user_info"> <img src="http://steelcoders.com/alpha/v1.2/assets/images/profile-image.png" alt="..." class="img-circle img-responsive">
                <div class="name"> <strong>Usuario</strong>
                    <br> <strong>Email</strong>
                    <br> <a href="" class="user_options-btn"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i></a> </div>
            </div>
            <?php
  $this->load->view('commons/admin_sidebar');
?>
        </div>
        <div class="col-md-10" id="contenido_admin">
            <div class="panel-flat clearfix">
                <div class="header-panel">
                    <h2>Provincias</h2> 
                    
                    <ul>
                        <li><a href="#" data-toggle="modal" data-target="#modalprovincias">Agregar nueva provincia</a></li>
                    </ul>
                    </div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Provincia</th>
                                <th>Latitud</th>
                                <th>Longitud</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($provincias as $provincia){?>
                                <tr>
                                    <td>
                                        <?php echo $provincia->nombre; ?>
                                    </td>
                                    <td>
                                        <?php echo $provincia->latitud; ?>
                                    </td>
                                    <td>
                                        <?php echo $provincia->longitud; ?>
                                    </td>
                                    <td> <a class="actions-btn" href="<?php echo base_url()?>index.php/provincias/eliminar_provincia/<?php echo $provincia->idprovincia; ?>"><i class="fa fa-trash"></i></a> 
                                    <a class="actions-btn" href="<?php echo $provincia->idprovincia; ?>"><i class="fa fa-pencil"></i></a> </td>
                                </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-flat clearfix">
                <div class="header-panel">
                    <h2>Regiones</h2> 
                     <ul>
                        <li><a href="#" data-toggle="modal" data-target="#modalregiones">Agregar nueva región</a></li>
                    </ul> 
                </div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Región</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($regiones as $region){?>
                                <tr>
                                    <td>
                                        <?php echo $region->nombre; ?>
                                    </td>
                                    <td> <a class="actions-btn" href="<?php echo base_url()?>index.php/provincias/eliminar_region/<?php echo $region->id_region; ?>"><i class="fa fa-trash"></i></a>  
                                    <a class="actions-btn" href="<?php echo $region->id_region; ?>"><i class="fa fa-pencil"></i></a> </td>
                                </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-flat clearfix">
                <div class="header-panel">
                    <h2>Playas</h2> 
                     <ul>
                        <li><a href="#" data-toggle="modal" data-target="#modalplayas">Agregar playa</a></li>
                    </ul> 
                </div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Playa</th>
                                <th>Descripción</th>
                                <th>Latitud</th>
                                <th>Longitud</th>
                                <th>Provincia</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($playas as $playa){?>
                                <tr>
                                    <td>
                                        <?php echo $playa->nombre; ?>
                                    </td>
                                    <td>
                                        <?php echo $playa->descripcion; ?>
                                    </td>
                                    <td>
                                        <?php echo $playa->latitud; ?>
                                    </td>
                                    <td>
                                        <?php echo $playa->longitud; ?>
                                    </td>
                                    <td>
                                        <?php echo $playa->idprovincia; ?>
                                    </td>
                                    <td> <a class="actions-btn" href="<?php echo base_url()?>index.php/provincias/eliminar_playa/<?php echo $playa->idplaya; ?>"><i class="fa fa-trash"></i></a> <a class="actions-btn" href="<?php echo $playa->idplaya; ?>"><i class="fa fa-pencil"></i></a> </td>
                                </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    <!--Modales-->
    
    <!-- Agregar Provincia -->
    
  <div class="modal fade" id="modalprovincias" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar Provincia</h4>
        </div>
        <div class="modal-body">
         <form action="<?php echo base_url() ?>index.php/provincias/insertar_provincia" method="post" data-toggle="validator" role="form">
             <div class="form-group">
                <label for="nombre_provincia" class="control-label">Nombre</label>
                 <input type="text" name="nombre_provincia" class="form-control" required>
             </div>
             
             <div class="form-group">
                <label for="latitud_provincia" class="control-label">Latitud</label>
                 <input type="text" name="latitud_provincia" class="form-control numero2" required>
             </div>
             <div class="form-group">
                <label for="longitud_provincia" class="control-label">Longitud</label>
                 <input type="text" name="longitud_provincia" class="form-control numero2" required>
             </div>
               <div class="form-group">
               <input type="submit" value="Agregar" class="btn btn-default">
             </div>
         </form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      
    </div>
  </div>
  
  <!-- Agregar Región -->
    
  <div class="modal fade" id="modalregiones" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar Región</h4>
        </div>
        <div class="modal-body">
         <form action="<?php echo base_url() ?>index.php/provincias/insertar_region" method="post" data-toggle="validator" role="form">
             <div class="form-group">
                <label for="nombre_region" class="control-label">Nombre</label>
                 <input type="text" name="nombre_region" class="form-control" required>
             </div>
               <div class="form-group">
               <input type="submit" value="Agregar" class="btn btn-default">
             </div>
         </form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      
    </div>
  </div>
    
    
    <!--Fin Modales-->
    
    <?php
  $this->load->view('commons/footer');
?>