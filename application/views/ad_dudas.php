<?php
  $this->load->view('commons/header');
?>

<div class="save_ajax">
   <img src="<?php echo base_url();?>/theme/img/clock.gif" alt="">

</div>

<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">

           <h2><?php echo   $GLOBALS['agregar_video_explicativo'];?></h2>

      </div>
      </div>

    </div>
    <div class="row ctool">

        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><?php echo $GLOBALS['volver_al_dashboard'];?></a>
    </div>

</div>



<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">


<div class="row">

      <div class="col-xs-12 tool-bar">
        <a data-toggle="modal" class="btn-default-gp" data-target="#addModal" href="#"><?php echo $GLOBALS['agregar_video_explicativo'];?></a>
      </div>


    <table class="table table-striped">
        <thead>
            <tr>
                <th><?php echo $GLOBALS['Nombre'];?></th>
                <th><?php echo $GLOBALS['name_en'] ;?></th>
                <th><?php echo $GLOBALS['descripcion'];?></th>
                <th><?php echo $GLOBALS['descripcion_en'];?></th>
                  <th>Video</th>
                <th><?php echo $GLOBALS['habilitado'] ;?></th>
                <th><?php echo $GLOBALS['acciones_to'];?></th>

            </tr>
        </thead>
        <tbody>
            <?php foreach($dudas as $duda){ ?>
            <tr>
                <td><?php echo $duda->nombre; ?></td>
                <td><?php echo $duda->nombre_eng; ?></td>
                  <td><?php echo $duda->comentario; ?></td>
                    <td><?php echo $duda->comentario_eng; ?></td>
                  <td><?php echo $duda->video; ?></td>
                <td>
                <?php  if($duda->estado == '1'){ echo 'Si'; }else if($duda->estado == '0'){ echo 'No'; } ?>
                </td>
                <td>
                  <a href="<?php echo base_url(); ?>tours/eliminar_dudas/<?php echo $duda->id; ?>"><?php echo $GLOBALS['eliminar'];?></a>
                  <a class="dudas-modal" data-toggle="modal" data-id="<?php echo $duda->id; ?>" href="javascript:void(0)"><?php echo $GLOBALS['editar'];?></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>



</div>

</div>


<!-- Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo   $GLOBALS['agregar_video_explicativo'];?></h4>
      </div>
      <div class="modal-body clearfix">
       <form action="<?php echo base_url(); ?>tours/agregar_dudas_de_tour" method="post" enctype="multipart/form-data">




        <input type="text" name="nombre" class="form-control" placeholder="<?php echo $GLOBALS['Nombre'];?>">
          <input type="text" name="nombre_eng" class="form-control" placeholder="<?php echo $GLOBALS['name_en'] ;?>">
      <input type="text" name="comentario"  class="form-control" placeholder="<?php echo $GLOBALS['comentario_es'] ;?>">
        <input type="text" name="comentario_eng" class="form-control" placeholder="<?php echo$GLOBALS['comentario_en'];?>">
        <input type="text" name="video" class="form-control" placeholder="<?php echo $GLOBALS['linkvideo_es'] ;?>">
        <input type="text" name="video_eng" class="form-control" placeholder="<?php echo $GLOBALS['linkvideo_en'];?>">
        <div class="form-group">
  <label for=""><?php echo $GLOBALS['habilitar_video'] ;?></label>
                   <select class="form-control" name="estado">


                      <option value=1 ><?php echo $GLOBALS['Si'] ;?></option>
                      <option value=0 ><?php echo $GLOBALS['No'] ;?></option>


                   </select>
               </div>





 <input type="submit" value="<?php echo $GLOBALS['agregar'];?>" class="btn btn-theme  btn-default-gp-v">


          </form>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Editar video explicativo</h4>
      </div>
      <div class="modal-body clearfix">
       <form action="<?php echo base_url(); ?>tours/editar_dudas_de_tour" method="post" enctype="multipart/form-data">



<input type="hidden" name="id"  id="id" class="form-control" placeholder="Nombre del video">
        <input type="text" name="nombre"  id="nombre" class="form-control" placeholder="Nombre del video">
        <input type="text" name="nombre_eng" id="nombre_eng" class="form-control" placeholder="Nombre del video en inglés">
        <input type="text" name="comentario" id="comentario" class="form-control" placeholder="Comentario en español">
        <input type="text" name="comentario_eng" id="comentario_eng" class="form-control" placeholder="Comentario en inglés">
        <input type="text" name="video" id="video" class="form-control" placeholder="Link del video en español">
        <input type="text" name="video_eng" id="video_eng" class="form-control" placeholder="Link del video en inglés">
        <div class="form-group">
  <label for="">Habilitar video</label>
                   <select class="form-control" name="estado" id="estado">
                      <option value=1>Si</option>
                       <option value=0>No</option>


                   </select>
               </div>





 <input type="submit" value="Editar" class="btn btn-theme  btn-default-gp-v">


          </form>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="container-fluid">



<?php
  $this->load->view('commons/footer');
?>
</div>
