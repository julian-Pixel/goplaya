<?php
  $this->load->view('commons/header');
?>




<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-12 col-md-8">
      <div class="profile">
          <img src="<?php if(isset($foto_perfil)){ echo $foto_perfil; } ?>" class="img-responsive" alt="">
           <h2>Bienvenido de nuevo, <span></span> </h2>

      </div>

    </div>

</div>
<div class="row admin-bar">
    <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#ubicaciones" aria-controls="home" role="tab" data-toggle="tab"> <i class="fa fa-globe"></i><?php echo $GLOBALS['ubicaciones'];?></a></li>
    <li role="presentation"><a href="#playas" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-pagelines" aria-hidden="true"></i> <?php echo   $GLOBALS['caracteristicas_playas'] ;?></a></li>
     <li role="presentation"><a href="#entradas" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-pencil" aria-hidden="true"></i> <?php echo  $GLOBALS['entradas'] ;?></a></li>
     <li role="presentation"><a href="<?php echo base_url();?>menu"><i class="fa fa-bars" aria-hidden="true"></i><?php echo $GLOBALS['menu'];?></a></li>
     <li role="presentation"><a href="<?php echo base_url();?>usuarios"><i class="fa fa-users" aria-hidden="true"></i><?php echo $GLOBALS['usuarios'];?></a></li>
     <li role="presentation"><a href="<?php echo base_url();?>slider" ><i class="fa fa-spinner" aria-hidden="true"></i> Slider</a></li>
     <!--<li role="presentation"><a href="#medios" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-file-image-o" aria-hidden="true"></i> Biblioteca de medios</a></li>-->
     <li role="presentation"><a href="<?php echo base_url();?>publicidad" ><i class="fa fa-bullhorn" aria-hidden="true"></i> <?php echo $GLOBALS['publicidad'];?></a></li>

     <li role="presentation"><a href="<?php echo base_url();?>modulos" ><i class="fa fa-puzzle-piece" aria-hidden="true"></i>  <?php echo   $GLOBALS['modulos'];?></a></li>
    <li role="presentation"><a href="<?php echo base_url();?>index.php/dashboard/featured" ><i class="fa fa-star" aria-hidden="true"></i> <?php echo    $GLOBALS['Featured_Home'] ;?></a></li>
    <li role="presentation"><a href="<?php echo base_url();?>index.php/dashboard/aprobar_tours" ><i class="fa fa-globe" aria-hidden="true"></i> <?php echo $GLOBALS['aprobar_tours'];?></a></li>
    <li role="presentation"><a href="<?php echo base_url();?>publicidad_tours" ><i class="fa fa-bullhorn" aria-hidden="true"></i>  <?php echo $GLOBALS['publicidad_tours'] ;?></a></li>
    <li role="presentation"><a href="<?php echo base_url();?>tours/tipos_de_tour" ><i class="fa fa-code-fork " aria-hidden="true"></i>  <?php echo $GLOBALS['tipos_tours'];?></a></li>
    <li role="presentation"><a href="<?php echo base_url();?>tours/dudas_de_tours" ><i class="fa fa-video-camera " aria-hidden="true"></i>  <?php echo   $GLOBALS['dudas_tours'];?></a></li>

    <li role="presentation"><a href="<?php echo base_url();?>tours/iconos_tours" ><i class="fa fa-info-circle " aria-hidden="true"></i>   <?php echo     $GLOBALS['iconos_tours'];?></a></li>

    <li role="presentation"><a href="<?php echo base_url();?>tours/como_subir_tours" ><i class="fa fa-video-camera " aria-hidden="true"></i>  <?php echo $GLOBALS['comossubiunr_tour'];?></a></li>
    <li role="presentation"><a href="<?php echo base_url();?>tours/opiniones" ><i class="fa fa-comments " aria-hidden="true"></i>  <?php echo $GLOBALS['opiniones'];?></a></li>
    <li role="presentation"><a href="<?php echo base_url();?>tours/ad_tour_operadores" ><i class="fa fa-handshake-o" aria-hidden="true"></i><?php echo $GLOBALS['tour_operadora']; ?></a></li>


  </ul>


</div>
<div class="container" style="padding-top: 5%;padding-bottom: 5%;">



     <!-- Tab panes -->
  <div class="tab-content">

  <!--Tab ubicaciones-->

    <div role="tabpanel" class="tab-pane active" id="ubicaciones">

    <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">  <?php echo  $GLOBALS['regiones'];?></h3>
     <ul class="tool-section-bar">
          <li><a class="btn-tour-ad" href="#" data-toggle="modal" data-target="#modalregiones"> <?php echo  $GLOBALS['agregar_region'] ;?><i class="fa fa-plus"></i></a></li>
      </ul>
  </div>
  <div class="panel-body">
  <div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th> <?php echo $GLOBALS['region'];?></th>
        <th></th>
      </tr>
    </thead>
    <tbody>

     <?php foreach($regiones as $region){?>
     <tr>
        <td><?php echo $region->nombre; ?></td>
        <td>
            <a href="<?php echo base_url();?>index.php/provincias/eliminar_region/<?php echo $region->id_region; ?>" style="margin-top: 11px;float:right;" class="btn-default-gp"><i class="fa fa-trash"></i><?php echo $GLOBALS['eliminar'];?></a>
            <a href="<?php echo base_url();?>index.php/provincias/editar_region/<?php echo $region->id_region; ?>" style="margin-top: 11px;float:right;" class="btn-default-gp  btn-default-gp-v"><i class="fa fa-pencil"></i><?php echo $GLOBALS['editar'];?></a>
        </td>
      </tr>
     <?php } ?>

    </tbody>
  </table>

  </div>
  </div>
</div>


 <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo $GLOBALS['provincias']  ;?></h3>
     <ul class="tool-section-bar">
          <li><a class="btn-tour-ad" href="#" data-toggle="modal" data-target="#modalprovincias"> <?php echo  $GLOBALS['agregar_provincia'] ;?><i class="fa fa-plus"></i></a></li>
      </ul>
  </div>
  <div class="panel-body">
  <div class="table-responsive">
   <table class="table table-striped">
    <thead>
      <tr>
        <th> <?php echo  $GLOBALS['provincia']  ;?></th>
        <th></th>
      </tr>
    </thead>
    <tbody>

     <?php foreach($provincias as $provincia){?>
     <tr>
        <td><?php echo $provincia->nombre; ?></td>
        <td>
            <a href="<?php echo base_url();?>index.php/provincias/eliminar_provincia/<?php echo $provincia->idprovincia; ?>" style="margin-top: 11px;float:right;" class="btn-default-gp"><i class="fa fa-trash"></i><?php echo $GLOBALS['eliminar'];?></a>
            <a href="#" style="margin-top: 11px;float:right;" class="btn-default-gp  btn-default-gp-v"><i class="fa fa-pencil"></i> <?php echo $GLOBALS['editar'];?></a>
        </td>
      </tr>
     <?php } ?>

    </tbody>
  </table>

  </div>
  </div>
</div>



 <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo $GLOBALS['playas'] ;?></h3>
     <ul class="tool-section-bar">
          <li><a class="btn-tour-ad" href="#" data-toggle="modal" data-target="#modalplayas"> <?php echo $GLOBALS['agregar_playa'] ;?><i class="fa fa-plus"></i></a></li>
      </ul>
  </div>
  <div class="panel-body">
  <div class="table-responsive">
   <table class="table table-striped">
    <thead>
      <tr>
        <th><?php echo  $GLOBALS['playa']  ;?></th>
        <th><?php echo $GLOBALS['region']   ;?></th>
        <th><?php echo  $GLOBALS['provincia']  ;?></th>
        <th></th>
      </tr>
    </thead>
    <tbody>

     <?php foreach($playas as $playas){?>
     <tr>
        <td><?php echo $playas->nombre; ?></td>
        <td><?php echo $playas->region; ?></td>
        <td><?php echo $playas->provincia; ?></td>
        <td>
            <a href="<?php echo base_url();?>index.php/provincias/eliminar_playa/<?php echo $playas->idplaya; ?>" style="margin-top: 11px;float:right;" class="btn-default-gp"><i class="fa fa-trash"></i> <?php echo $GLOBALS['eliminar'];?></a>
            <a href="<?php echo base_url()?>index.php/dashboard/editar_playa/<?php echo $playas->idplaya; ?>" style="margin-top: 11px;float:right;" class="btn-default-gp  btn-default-gp-v"><i class="fa fa-pencil"></i><?php echo $GLOBALS['editar'];?></a>
        </td>
      </tr>
     <?php } ?>

    </tbody>
  </table>
  </div>
  </div>
</div>




    </div>
     <!--Fin tab ubicaciones-->

      <!--Tab caracteristicas-->
<div role="tabpanel" class="tab-pane" id="playas">



     <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Características padre</h3>
     <ul class="tool-section-bar">
          <li><a href="#" data-toggle="modal" data-target="#modalcaracteristicaspadres">Agregar característica<i class="fa fa-plus"></i></a></li>
      </ul>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
   <table class="table table-striped">
    <thead>
      <tr>
        <th>Características Padre</th>
        <th></th>
      </tr>
    </thead>
    <tbody>

     <?php foreach($cat_padre as $caracteristica){
        ?>
     <tr>
        <td><?php echo $caracteristica->detalle; ?></td>
           <td>
            <a href="<?php echo base_url();?>provincias/eliminar_caracteristica_padre/<?php echo $caracteristica->id; ?>" style="margin-top: 11px;float:right;" class="btn-default-gp"><i class="fa fa-trash"></i><?php echo $GLOBALS['eliminar'];?></a>
            <a href="<?php echo base_url();?>dashboard/editar_caracteristica_padre/<?php echo $caracteristica->id; ?>" style="margin-top: 11px;float:right;" class="btn-default-gp  btn-default-gp-v"><i class="fa fa-pencil"></i> <?php echo $GLOBALS['editar'];?></a>
        </td>
      </tr>
     <?php } ?>

    </tbody>
  </table>

  </div>
  </div>
</div>




    <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Características</h3>
     <ul class="tool-section-bar">
          <li><a href="#" data-toggle="modal" data-target="#modalcaracteristicas">Agregar característica<i class="fa fa-plus"></i></a></li>
      </ul>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
   <table class="table table-striped">
    <thead>
      <tr>
        <th>Característica</th>
        <th>Característica padre</th>
        <th>Icono</th>
        <th>Filtro simple</th>
        <th></th>
      </tr>
    </thead>
    <tbody>

     <?php foreach($caracteristicas as $caracteristica){
        ?>
     <tr>
        <td><?php echo $caracteristica->detalle; ?></td>
        <td class="td-center"><?php echo $caracteristica->padre; ?></td>
        <td class="td-center"><i class="<?php echo $caracteristica->icono; ?>"></i></td>
        <td class="td-center"><?php if($caracteristica->simple == 0){ echo 'No';}elseif($caracteristica->simple == 1){echo 'Si';} ?></td>
        <td>
            <a href="<?php echo base_url();?>index.php/provincias/eliminar_caracteristica/<?php echo $caracteristica->idactividad; ?>" style="margin-top: 11px;float:right;" class="btn-default-gp"><i class="fa fa-trash"></i> <?php echo $GLOBALS['eliminar'];?></a>
            <a href="<?php echo base_url();?>dashboard/editar_caracteristica/<?php echo $caracteristica->idactividad; ?>" style="margin-top: 11px;float:right;" class="btn-default-gp  btn-default-gp-v"><i class="fa fa-pencil"></i> <?php echo $GLOBALS['editar'];?></a>
        </td>
      </tr>
     <?php } ?>

    </tbody>
  </table>

  </div>
  </div>
</div>


</div>
    <!--Fin tab caracteristicas-->

     <!--Tab entradas-->
<div role="tabpanel" class="tab-pane" id="entradas">

    <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Entradas</h3>
     <ul class="tool-section-bar">
          <li><a href="<?php echo base_url();?>pagina/crear_pagina">Agregar entrada<i class="fa fa-plus"></i></a></li>
      </ul>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
   <table class="table table-striped">
    <thead>
      <tr>
        <th>Entrada</th>
        <th>Tipo</th>
        <th>Estado</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
     <tr>
         <td>Pagina de inicio</td>
         <td>Página</td>
         <td>Activa</td>
         <td><a href="<?php echo base_url();?>pagina//editar_home" style="margin-top: 11px;float:right;" class="btn-default-gp  btn-default-gp-v"><i class="fa fa-pencil"></i> Editar</a></td>
     </tr>
     <?php foreach($paginas as $pagina){
        ?>
     <tr>
        <td><?php echo $pagina->nombre; ?></td>
        <td class="td-center"><?php if($pagina->categoria == 1){ echo 'Artículo'; }else{ echo 'Página'; }; ?></td>
        <td class="td-center"><?php if($pagina->estado == 1){ echo 'Borrador'; }else{ echo 'Publicada'; }; ?></td>
        <td>
            <a href="<?php echo base_url();?>pagina/eliminar_pagina/<?php echo $pagina->id; ?>" style="margin-top: 11px;float:right;" class="btn-default-gp"><i class="fa fa-trash"></i> <?php echo $GLOBALS['eliminar'];?></a>
            <a href="<?php echo base_url();?>pagina/editar/<?php echo $pagina->slug; ?>" style="margin-top: 11px;float:right;" class="btn-default-gp  btn-default-gp-v"><i class="fa fa-pencil"></i> <?php echo $GLOBALS['editar'];?></a>
        </td>
      </tr>
     <?php } ?>

    </tbody>
  </table>

  </div>
  </div>
</div>


</div>
    <!--Fin tab entradas-->



       <!--Tab caracteristicas-->
<div role="tabpanel" class="tab-pane" id="medios">

    <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Archivos</h3>
     <ul class="tool-section-bar">

      </ul>
  </div>
  <div class="panel-body">
   <!--<ul id="medios">

   </ul>-->

  </div>
</div>


</div>
    <!--Fin tab caracteristicas-->



    <div role="tabpanel" class="tab-pane" id="messages">...</div>
    <div role="tabpanel" class="tab-pane" id="settings">...</div>



</div>
</div>




    <!--Modales-->

    <!-- Agregar Provincia -->

  <div class="modal fade" id="modalprovincias" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo  $GLOBALS['agregar_provincia'] ;?></h4>
        </div>
        <div class="modal-body">
         <form action="<?php echo base_url() ?>index.php/provincias/insertar_provincia" method="post" data-toggle="validator" role="form">
             <div class="form-group">
                <label for="nombre_provincia" class="control-label"><?php echo  $GLOBALS['Nombre'] ;?></label>
                 <input type="text" name="nombre_provincia" class="form-control" required>
             </div>

             <div class="form-group">
                <label for="latitud_provincia" class="control-label"><?php echo  $GLOBALS['latitud'] ;?></label>
                 <input type="text" name="latitud_provincia" class="form-control numero2" required>
             </div>
             <div class="form-group">
                <label for="longitud_provincia" class="control-label"><?php echo  $GLOBALS['longitud'];?></label>
                 <input type="text" name="longitud_provincia" class="form-control numero2" required>
             </div>
               <div class="form-group">
               <input type="submit" value="<?php echo $GLOBALS['agregar'];?>" class="btn btn-default">
             </div>
         </form>
        </div>
        <div class="modal-footer">

        </div>
      </div>

    </div>
  </div>

  <!-- Agregar Región -->

  <div class="modal fade" id="modalregiones" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo  $GLOBALS['agregar_region'] ;?></h4>
        </div>
        <div class="modal-body">
         <form action="<?php echo base_url() ?>index.php/provincias/insertar_region" method="post" data-toggle="validator" role="form">
             <div class="form-group">
                <label for="nombre_region" class="control-label"><?php echo  $GLOBALS['Nombre'] ;?></label>
                 <input type="text" name="nombre_region" class="form-control" required>
             </div>
               <div class="form-group">
               <input type="submit" value="<?php echo $GLOBALS['agregar'];?>" class="btn btn-default">
             </div>
         </form>
        </div>
        <div class="modal-footer">

        </div>
      </div>

    </div>
  </div>

   <!-- Agregar caracteristicas -->

  <div class="modal fade" id="modalcaracteristicas" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar Característica</h4>
        </div>
        <div class="modal-body">
         <form action="<?php echo base_url() ?>index.php/provincias/insertar_caracteristica" method="post" data-toggle="validator" role="form">
             <div class="form-group col-xs-12">
                <label for="padre" class="control-label">Categoría padre</label>
                 <select name="padre" class="form-control" id="">
                     <?php foreach($cat_padre as $cp){?>
                     <option value="<?php echo $cp->id; ?>"><?php echo $cp->detalle; ?></option>
                     <?php } ?>
                 </select>
             </div>
              <div class="form-group col-xs-12">
                <label for="nombre_caracteristica" class="control-label">Detalle</label>
                 <input type="text" name="nombre_caracteristica" class="form-control" required>
             </div>
             <div class="col-xs-12">
                  <select id="e2_element" name="icono">

                     <option value="">No icon</option>

  <option value="icon-area-conservacion">icon-area-conservacion</option>
  <option value="icon-arena">icon-arena</option>
  <option value="icon-BALLENAS-Y-DELFINES">icon-BALLENAS-Y-DELFINES</option>
  <option value="icon-ESTADO-DEL-CAMINO">icon-ESTADO-DEL-CAMINO</option>
  <option value="icon-MIRADOR">icon-MIRADOR</option>
  <option value="icon-OLEAJE">icon-OLEAJE</option>
  <option value="icon-RUTA-DE-ACCESO">icon-RUTA-DE-ACCESO</option>
  <option value="icon-BUCEO">icon-BUCEO</option>
  <option value="icon-PIEDRA">icon-PIEDRA</option>
  <option value="icon-PLAYA-VIRGEN">icon-PLAYA-VIRGEN</option>
  <option value="icon-SNORKELING">icon-SNORKELING</option>
  <option value="icon-SURF">icon-SURF</option>
  <option value="icon-TORTUGA">icon-TORTUGA</option>
  <option value="icon-VEGETACION">icon-VEGETACION</option>

                 </select>
             </div>
              <div class="form-group col-xs-12">
                <label for="simple" class="control-label">Simple</label>
                 <select name="simple" class="form-control" id="">

                     <option value="0">No</option>
                     <option value="1">Si</option>

                 </select>
             </div>

               <div class="form-group ">
               <input type="submit"  value="<?php echo $GLOBALS['agregar'];?>" class="btn btn-default">
             </div>
         </form>
        </div>
        <div class="modal-footer">

        </div>
      </div>

    </div>
  </div>

    <!-- Agregar caracteristicas -->

  <div class="modal fade" id="modalcaracteristicaspadres" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar Característica</h4>
        </div>
        <div class="modal-body">
         <form action="<?php echo base_url() ?>index.php/provincias/insertar_caracteristica_padre" method="post" data-toggle="validator" role="form">

              <div class="form-group col-xs-12">
                <label for="nombre_caracteristica" class="control-label">Detalle</label>
                 <input type="text" name="nombre_caracteristica" class="form-control" required>
             </div>


       <div class="form-group">
                <label for="simple" class="control-label">Oculto</label>
                 <select name="oculto" class="form-control" id="">

                     <option value="0" >No</option>
                     <option value="1" >Si</option>

                 </select>
             </div>

               <div class="form-group ">
               <input type="submit"  value="<?php echo $GLOBALS['agregar'];?>" class="btn btn-default">
             </div>
         </form>
        </div>
        <div class="modal-footer">

        </div>
      </div>

    </div>
  </div>


    <!-- Agregar playa -->

  <div class="modal fade" id="modalplayas" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> <?php echo $GLOBALS['agregar_playa'] ;?></h4>
        </div>
        <div class="modal-body">
         <form action="<?php echo base_url() ?>index.php/provincias/insertar_playa" method="post" data-toggle="validator" role="form">
             <div class="form-group">
                <label for="nombre_playa" class="control-label"><?php echo  $GLOBALS['Nombre'] ;?></label>
                 <input type="text" name="nombre_playa" class="form-control" required>
             </div>
             <div class="form-group">
                <label for="descripcion_playa" class="control-label"><?php echo  $GLOBALS['descripcion'] ;?></label>
                 <input type="text" name="descripcion_playa" class="form-control" required>
             </div>
             <div class="form-group">
                <label for="latitud_playa" class="control-label"><?php echo  $GLOBALS['latitud'] ;?></label>
                 <input type="text" name="latitud_playa" class="form-control numero2" required>
             </div>
             <div class="form-group">
                <label for="longitud_playa" class="control-label"><?php echo  $GLOBALS['longitud'];?></label>
                 <input type="text" name="longitud_playa" class="form-control numero2" required>
             </div>
              <div class="form-group">
                <label for="region_playa" class="control-label"><?php echo  $GLOBALS['agregar_region'] ;?></label>
                 <select name="region_playa" class="form-control" required>
                     <?php foreach($regiones as $region){ ?>
                     <option value="<?php echo $region->id_region; ?>"><?php echo $region->nombre; ?></option>
                     <?php } ?>
                 </select>
             </div>
             <div class="form-group">
                <label for="provincia_playa" class="control-label"><?php echo  $GLOBALS['provincia']  ;?></label>
                 <select name="provincia_playa" class="form-control" required>
                      <?php foreach($provincias as $provincia){?>
                     <option value="<?php echo $provincia->idprovincia; ?>"><?php echo $provincia->nombre; ?></option>
                     <?php } ?>
                 </select>
             </div>
            <div class="form-group">
               <input type="submit"  value="<?php echo $GLOBALS['agregar'];?>" class="btn btn-default">
             </div>
         </form>
        </div>
        <div class="modal-footer">

        </div>
      </div>

    </div>
  </div>

    <!--Fin Modales-->




<?php
  $this->load->view('commons/footer');
?>
</div>
