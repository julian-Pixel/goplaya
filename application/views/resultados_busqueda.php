<?php
  $this->load->view('commons/header');

?>

<?php
  $this->load->view('3rd/Mobile_Detect');

$detect = new Mobile_Detect;

?>

<style>
.dropdown {
    width: 48%;
    float: left;
}</style>




<div class="container-fluid">
    <form action="<?php echo base_url(); ?>index.php/search_controller/buscar#sa" method="get" data-step="1" data-intro="<?php echo $GLOBALS['step-1b']; ?>">

  <!--  <div class="filters_bar row">

       <div class="col-xs-12 col-md-4">
           <a href="#" class="btn-geolocate " onclick="geolocate();"><i class="fa fa-location-arrow" aria-hidden="true"></i> <?php echo $GLOBALS['btn_detectar_ubicacion']; ?></a>
       </div>


    </div>-->

    <section class="col-xs-12 col-md-6 txt-center">


     <input type="hidden" name="latitud" value="<?php echo $latitud; ?>" id="latitudsf">
     <input type="hidden" name="longitud" value="<?php echo $longitud; ?>" id="longitudsf">
      <h2 style="font-size: 25px;"><?php echo $GLOBALS['opciones']; ?></h2>

          <h4 class="vc" data-step="2" data-intro="<?php echo $GLOBALS['step-2b']; ?>" >1. <?php echo $GLOBALS['zonas']; ?></h4>

  <ul class="zonas_list clearfix">
      <li>

            <input type="checkbox" value="todas" id="btodos" name="busqueda" class="icheck"


            <?php
                   if (isset($_GET['busqueda'])) {

                    if ($_GET['busqueda']=='todas') {
                    echo "checked";
                }
                   }else{
                       echo "checked";
                   }

            ?>
            >
            <label for="btodos"><?php echo $GLOBALS['todas']; ?></label>
      </li>

      <?php foreach($regiones as $region){?>

      <li>

            <input type="checkbox" value="<?php echo $region->nombre; ?>" id="<?php echo $region->nombre; ?>" class="icheck" name="busqueda[]"




             <?php

             if (in_array($region->nombre, $_GET['busqueda'])) {
                    echo "checked";
                }


            ?>



            >
            <label for="<?php echo $region->nombre; ?>"><?php echo $region->nombre; ?></label>
      </li>

      <?php } ?>

  </ul>

      <hr>


          <h4 class="vc" id="<?php if($detect->isMobile()){ echo ''; }else{ echo 'sa'; } ?>" data-step="3" data-intro="<?php echo $GLOBALS['step-3b']; ?>" >2. <?php echo $GLOBALS['opciones_playas']; ?></h4>

             <ul class="zonas_list clearfix">


      <?php foreach($caracteristicas as $c){?>

      <li>

            <input type="radio"  id="<?php echo $c->detalle; ?>" value="<?php echo $c->idactividad; ?>" id="btodos" class="icheck"  name="filtros[]"

             <?php

             if (in_array($c->idactividad, $_GET['filtros'])) {
                    echo "checked";
                }


            ?>



                  >
            <label for="<?php echo $c->detalle; ?>">


                   <?php if( $_SESSION['idioma']   == 'es' ){ ?>
                    <?php echo $c->detalle; ?>
                    <? }else if( $_SESSION['idioma']   == 'en' ){?>
                      <?php echo $c->detalle_en; ?>
                    <?php }?>






            </label>
      </li>

      <?php } ?>

  </ul>
       <hr>

        <h4 class="vc" data-step="4" data-intro="<?php echo $GLOBALS['step-4b']; ?>">3. <?php echo $GLOBALS['definir_radio']; ?></h4>



         <div class="col-xs-12">
            <input type="text" name="radio" class="range">
       </div>

           <hr>

         <div class="col-xs-12">
              <a id="mfa2"  class="btn-default-gp  btn-default-gp-v" style="float: initial;display: inline-block;margin: 0;margin-bottom: 3rem;margin-top: 3rem;width: 100%;
    border-radius: 6px;" data-toggle="modal" data-target="#myModal064" data-step="5" data-intro="<?php echo $GLOBALS['step-5b']; ?>"><?php echo $GLOBALS['mostrar_filtros']; ?></a>
         </div>



       <!-- Modal -->
<div class="modal fade" id="myModal064" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="text-align: left;" id="myModalLabel">Búsqueda avanzada</h4>
      </div>
      <div class="modal-body clearfix">


         <?php foreach($filtros_avanzados as $fa => $value){?>

       <div class="col-xs-12 col-md-6 af" style="text-align: left;">
           <h3><?php echo $fa; ?> <a class="checkAll fha" data-target=".<?php echo str_replace(" ","_",$fa);?>"><i class="fa fa-check" aria-hidden="true"></i></a></h3>


        <?php foreach($value[0] as $v => $code){

    ?>
    <div class="form-group">
    <label for="<?php echo $value[1][$v]; ?>"> <?php echo $value[1][$v]; ?> </label>
     <input type="radio" class="icheck <?php echo str_replace(" ","_",$fa);?>" value="<?php echo $code; ?>" id="<?php echo $value[1][$v]; ?>" name="filtros[]"


     <?php

             if (in_array($code, $_GET['filtros'])) {
                    echo "checked";
                }


            ?>


     >


</div>

    <?php

}
    ?>
 </div>


       <?php } ?>

       <div class="form-group">

            <button  type="submit" class="btn-default-gp" style="min-width: 220px;float: initial;margin: 0;margin-bottom: 2rem;"><?php echo $GLOBALS['buscar_playas']; ?></button>

       </div>

      </div>

    </div>
  </div>
</div>



     <!--
     <div class="col-xs-12 filterbg">


       <?php foreach($filtros_avanzados as $fa => $value){?>

       <div class="col-xs-12 col-md-6 af" style="text-align: left;">
           <h3><?php echo $fa; ?> <a class="checkAll fha" data-target=".<?php echo str_replace(" ","_",$fa);?>"><i class="fa fa-check" aria-hidden="true"></i></a></h3>


        <?php foreach($value[0] as $v => $code){

    ?>
    <div class="form-group">
    <label> <?php echo $value[1][$v]; ?> </label>
     <input type="checkbox" class="icheck <?php echo str_replace(" ","_",$fa);?>" value="<?php echo $code; ?>" id="btodos" name="filtros[]">


</div>

    <?php

}
    ?>
 </div>


       <?php } ?>

      </div> -->

     <div class="col-xs-12 others">
         <button data-step="6" data-intro="<?php echo $GLOBALS['step-6b']; ?>" type="submit" class="btn-default-gp" style="min-width: 220px;float: initial;margin: 0;margin-bottom: 2rem;"><?php echo $GLOBALS['buscar_playas']; ?></button>

    </div>
   </form>




    </section>

    </form>
    <section class="col-xs-12 col-md-6" style="margin-bottom:0px;">
        <section class="row" id="<?php if($detect->isMobile()){ echo 'sa'; }else{ echo ''; } ?>" style="position:relative;margin-bottom:0px;">

     <div id="map2"></div>
</section>
    </section>
</div>

<div class="container-fluid">
<div class="row">

        <?php if(isset($playas)){ ?>



        <section style="padding-left: 16px;">
            <h2><?php echo $GLOBALS['resultados_busqueda']; ?></h2>
        </section>
<div class="" id="resultados_s_playas ">
   <div id="rbctr" class="row is-flex" style="float: left;  min-height: 270px;    margin-right: 0;
    margin-left: 0;">

   <?php

      //var_dump($playas);

      foreach($playas as $playa){ ?>


      <div class="col-xs-12 col-md-3 playa_item">

   <div class="img-ctr">



      <a href="<?php echo base_url();?>playa/detalle/<?php if(empty($playa->slug)){ echo $playa->idplaya; }else{ echo $playa->slug; } ?>">
       <img src="<?php echo base_url()?>archivos/<?php echo $playa->imagen_destacada; ?>" class="img-responsive" alt="">
       </a>

       <a href="<?php echo base_url();?>playa/detalle/<?php if(empty($playa->slug)){ echo $playa->idplaya; }else{ echo $playa->slug; } ?>" class="vp-ctr">




                   <?php if( $_SESSION['idioma']   == 'es' ){ ?>
                        Ver playa
                    <? }else if( $_SESSION['idioma']   == 'en' ){?>
                    View beach
                    <?php }?>


       </a>
       <div class="overlay-m"></div>
   </div>




   <div class="gl-feat-item-details"><h3> <a target="_blank" href="https://maps.google.com/maps?ll=<?php echo $playa->latitud; ?>,<?php echo $playa->longitud; ?>&q=<?php echo $playa->latitud; ?>,<?php echo $playa->longitud; ?>&hl=en&t=h&z=18"><span><img src="<?php echo base_url(); ?>theme/img/google-maps.svg" alt=""></i></span></a> <?php echo $playa->nombre; ?></h3></div>


</div>


   <?php } ?>


   </div>
</div>



  <?php } ?>


</div>

<?php
  $this->load->view('commons/footer');
?>

</div>



 <?php if(isset($playas)){ ?>

<!--Mapa -->

 <script>

var map;
function initMap() {
    var marcadores = [

         <?php foreach($playas as $playa){ ?>

        ['<?php echo $playa->nombre; ?>', <?php echo $playa->latitud; ?>, <?php echo $playa->longitud; ?>,'<?php echo base_url(); ?>archivos/<?php echo $playa->imagen_destacada; ?>'],
        <?php } ?>
      ];
    console.log(marcadores);
  map = new google.maps.Map(document.getElementById('map2'), {
    center: {lat: <?php echo $latitud; ?>, lng: <?php echo $longitud; ?>},
    zoom: 7
  });
     var infowindow = new google.maps.InfoWindow();
      var marker, i;
      for (i = 0; i < marcadores.length; i++) {
        marker = new google.maps.Marker({
          icon: '<?php echo base_url(); ?>theme/img/icon-map.png',
          position: new google.maps.LatLng(marcadores[i][1], marcadores[i][2]),
          map: map
        });
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent( '<div class="listing-map-popover"><div class="imagen"><img src="'+marcadores[i][3]+'"></div><div class="info-compact"><h3>'+marcadores[i][0]+'</h3></div></div>');
            infowindow.open(map, marker);
          }
        })(marker, i));
      }

    if (navigator.geolocation) {

         function sucess(position) {
            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            // Create a marker and center map on user location
            marker = new google.maps.Marker({
                position: pos,
                draggable: true,
                animation: google.maps.Animation.DROP,
                map: map
            });
            map.setCenter(pos);
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);
        }

        function error(){

              var pos = new google.maps.LatLng(<?php echo $latitud; ?>,  <?php echo $longitud; ?>);
            // Create a marker and center map on user location
            marker = new google.maps.Marker({
                position: pos,
                draggable: true,
                animation: google.maps.Animation.DROP,
                map: map
            });
            map.setCenter(pos);
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);


            }

                  navigator.geolocation.getCurrentPosition(sucess,error);


    }


}



function geolocate() {

        if (navigator.geolocation) {

         function sucess(position) {
            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            // Create a marker and center map on user location
            marker = new google.maps.Marker({
                position: pos,
                draggable: true,
                animation: google.maps.Animation.DROP,
                map: map
            });
            map.setCenter(pos);
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);
        }

        function error(){

              var pos = new google.maps.LatLng(9.935607,-84.1833856);
            // Create a marker and center map on user location
            marker = new google.maps.Marker({
                position: pos,
                draggable: true,
                animation: google.maps.Animation.DROP,
                map: map
            });
            map.setCenter(pos);
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);


            }

                  navigator.geolocation.getCurrentPosition(sucess,error);


    }

}



function handleEvent(event) {
    document.getElementById('latitudsf').value = event.latLng.lat();
    document.getElementById('longitudsf').value = event.latLng.lng();
}

    </script>
     <?php }else{ ?>



 <script>

var map;
function initMap() {

  map = new google.maps.Map(document.getElementById('map2'), {
    center: {lat: <?php echo $latitud; ?>, lng: <?php echo $longitud; ?>},
    zoom: 7
  });


    if (navigator.geolocation) {

         function sucess(position) {
            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            // Create a marker and center map on user location
            marker = new google.maps.Marker({
                position: pos,
                draggable: true,
                animation: google.maps.Animation.DROP,
                map: map
            });
            map.setCenter(pos);
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);
        }

        function error(){

              var pos = new google.maps.LatLng(9.935607,-84.1833856);
            // Create a marker and center map on user location
            marker = new google.maps.Marker({
                position: pos,
                draggable: true,
                animation: google.maps.Animation.DROP,
                map: map
            });
            map.setCenter(pos);
            //marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);


            }

                  navigator.geolocation.getCurrentPosition(sucess,error);


    }






}
function handleEvent(event) {
    document.getElementById('latitudsf').value = event.latLng.lat();
    document.getElementById('longitudsf').value = event.latLng.lng();
    map.setZoom(7);
    map.panTo(marker.position);
}
    </script>
     <script>


</script>


      <?php } ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtH7lp3f5hbOz_SzGOJ2EBgXx36X7s8CM&callback=initMap"
        async defer></script>
        <style>

            label {
    font-size: 13px;
}
</style>
<!--Fin mapa -->
