<?php
  $this->load->view('commons/header');
?>




<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">

           <h2>Menus</h2>

      </div>
      </div>

    </div>
    <div class="row ctool">

        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
        <a href="#"  data-toggle="modal" data-target="#menu_modal" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Agregar Item</a>
    </div>

</div>
<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">

 <div>

 </div>

 <ul class="sortable3 menu_menu">
     <?php foreach($menus as $menu){ ?>



   <li id="item_<?php echo $menu->id; ?>" class="clearfix">
        <form action="<?php echo base_url(); ?>menu/editar_item" method="post">
         <input type="hidden" name="idmenu" value="<?php echo $idmenu;?>">
       <div class="form-group">

    <div class="col-sm-11">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-align-left" aria-hidden="true"></i></span>
         <input type="hidden" name="id" class="form-control" value="<?php echo $menu->id ?>">
         <input type="text" name="detalle" placeholder="Nombre en español" class="form-control" value="<?php echo $menu->detalle ?>">
         <input type="text" name="detalle_en" placeholder="Nombre en inglés" class="form-control" value="<?php echo $menu->detalle_en ?>">
      </div>

    </div>
       <div class="col-sm-1">
       <a class="dbtn" href="<?php echo base_url(); ?>menu/eliminar_item/<?php echo $menu->id; ?>"><i class="fa fa-trash-o"></i></a>
        </div>
  </div>

      <div class="form-group">

    <div class="col-sm-11">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-link"></i></span>
         <input type="text" name="enlace_es" class="form-control" value="<?php echo $menu->enlace ?>">
         <input type="text" name="enlace_en" class="form-control" value="<?php echo $menu->enlace_en ?>">
      </div>

    </div>
  </div>

    <input type="submit" value="Actualizar" style="margin-top: 2rem;float: right;background: #064b75;color: white;">
    </form>
    </li>


<?php } ?>
 </ul>

</div>
<div class="container-fluid">



<!-- Modal -->
<div id="menu_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar item menu</h4>
      </div>
      <div class="modal-body" style="background: #efefef;">

        <form action="<?php echo base_url(); ?>menu/agregar_item" method="post">
         <input type="hidden" name="id" value="<?php echo $idmenu;?>">
              <!-- Ingles Español TABS -->

            <div>

              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#esc" role="tab" data-toggle="tab">Español</a></li>
                <li role="presentation"><a href="#enc" role="tab" data-toggle="tab">Ingles</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="esc">
                    <input type="text" name="detalle_es" class="form-control" placeholder="Detalle">
                    <input type="text" name="enlace_es" class="form-control" placeholder="Enlace">


                </div>
                <div role="tabpanel" class="tab-pane" id="enc">
                    <input type="text" name="detalle_en" class="form-control" placeholder="Detalle">
                    <input type="text" name="enlace_en" class="form-control" placeholder="Enlace">

                </div>
              </div>


            </div>
            <!-- FIN Ingles Español TABS -->




           <input type="submit" value="Agregar">






       </form>


      </div>
    </div>

  </div>
</div>

<?php
  $this->load->view('commons/footer');
?>
</div>
