<style >
.slick-slide {
    height: auto !important;
}


.slick-slide p{
        color: grey;
        font-weight:100;
}
</style>

<section  class="container sb-m" >



  <div class="txt-center margin-bottom">



  <?php if( $_SESSION['idioma']   == 'es' ){ ?>

        <h2><span>¿Qué estás </span> buscando?</h2>
  <? }else if( $_SESSION['idioma']   == 'en' ){?>

    <h2><span>What  are you   </span> looking for?</h2>
  <?php }?>

  </div>



<?php foreach ($featured as $f) {?>

  <div class="col-xs-4 featured-tem">
    <a href=" <?php echo $f->link; ?>">
    <i class="  <?php echo $f->icon; ?>"></i>


    <?php if( $_SESSION['idioma']   == 'es' ){ ?>

      <h5>  <?php echo $f->titulo_es; ?></h5>
      <p class="hidden-sm hidden-xs">  <?php echo $f->descrip_es; ?></p>
<? }else if( $_SESSION['idioma']   == 'en' ){?>

  <h5>  <?php echo $f->titulo_en; ?></h5>
  <p class="hidden-sm hidden-xs">  <?php echo $f->descrp_en; ?></p>
<?php }?>

</a>
  </div>




<?php } ?>



</section>









<!-- pruebaaa -->

<section  class="container" >
  <div class="txt-center margin-bottom">


    <?php if( $_SESSION['idioma']   == 'es' ){ ?>

        <h2><span>Tours </span> destacados</h2>
    <? }else if( $_SESSION['idioma']   == 'en' ){?>

    <h2><span>Must-see </span> tours</h2>
    <?php }?>
    <!-- <p>En pareja, con tu familia o viajando sólo. En Go Playa encontrarás el tour perfecto!</p> -->
    <span style="color:#064b75"><?php echo $GLOBALS['descripcion_destacados']; ?></span>
  </div>


  <div class="featured">
    <?php foreach ($tours as $tour) { ?>


      <?php if($tour->imagen != ''){
      $imagen = base_url().'archivos/'.$tour->imagen;
      }else{
      $imagen = 'https://i.ytimg.com/vi/-ZEDp5JhhIc/maxresdefault.jpg';

      }?>
    <div>
<a href="<?php echo base_url();?>tours/tour/<?php echo $tour->idtour; ?>">
<img class="img-responsive" src="<?php echo $imagen; ?> " alt="" style="height: 265px;">
</a>




    <?php if( $_SESSION['idioma']   == 'es' ){ ?>

      <h2 style="font-size: 20px;"><?php echo $tour->nombre; ?> </h2>
    <? }else if( $_SESSION['idioma']   == 'en' ){?>

      <h2 style="font-size: 20px;"><?php echo $tour->nombre_eng; ?> </h2>
    <?php }?>


<div class="stars3" style="width:100%">
  <?php
    if($tour->porcetaje_descuento>0){
  ?>
  <span class="old-price">$ <?php echo $tour->precio; ?></span>
  <?php } ?>
<span class="precio">
  <?php if($tour->porcetaje_descuento>0){
    echo '$';
  echo ($tour->precio)-(($tour->precio)*($tour->porcetaje_descuento));
  }else{
      echo '$';
    echo ($tour->precio);
  }  ?>

</span>
</div>

<p> <?php echo $tour->provincia; ?> </p>
    </div>
  <?php } ?>
  </div>








</section>




<section  class="container" >
  <div class="txt-center margin-bottom">

    <?php if( $_SESSION['idioma']   == 'es' ){ ?>

          <h2><span>Playas </span> del mes</h2>
    <? }else if( $_SESSION['idioma']   == 'en' ){?>

        <h2><span>Best beaches </span> of the month</h2>
    <?php }?>
  </div>


  <div class="featured">
    <?php foreach ($delmes as $py) { ?>

    <div>
<a href="<?php echo base_url();?>/playa/detalle/<?php echo $py['slug']; ?>">
<img class="img-responsive" src="<?php echo base_url(); ?>archivos/<?php echo $py['imagen_destacada']; ?> " alt="">
</a>




    <?php if( $_SESSION['idioma']   == 'es' ){ ?>

      <h2 style="font-size: 20px;"><span>Playa </span><?php echo $py["nombre"]; ?> </h2>
    <? }else if( $_SESSION['idioma']   == 'en' ){?>

      <h2 style="font-size: 20px;"><span><?php echo $py["nombre"]; ?> </span> Beach </h2>
    <?php }?>


<div class="stars3">
  <?php
  $votos = $py['total'] ;
  $registros = $py['registros'];

  if(empty($votos)){
    $star=0;
  }else{
    $star = ($votos/$registros);
  }


  if($star > 0){


  for($i=1 ; $i<=$star; $i++){
  ?>

  <i class="fa fa-star" aria-hidden="true"></i>
  <?php

  }
  for($i=$star ; $i<5; $i++){
  ?>

  <i class="fa fa-star-o" aria-hidden="true"></i>
  <?php

  }

  }else{
  ?>
  <i class="fa fa-star-o" aria-hidden="true"></i>
  <i class="fa fa-star-o" aria-hidden="true"></i>
  <i class="fa fa-star-o" aria-hidden="true"></i>
  <i class="fa fa-star-o" aria-hidden="true"></i>
  <i class="fa fa-star-o" aria-hidden="true"></i>

  <?php

  }
  ?>
</div>

<p> <?php echo $py['zona_es']; ?> </p>
    </div>
  <?php } ?>
  </div>








</section>

<section  class="container" >
  <div class="txt-center margin-bottom">

    <?php if( $_SESSION['idioma']   == 'es' ){ ?>

          <h2><span>Playas </span> más populares</h2>
    <? }else if( $_SESSION['idioma']   == 'en' ){?>

        <h2><span>Most   </span> popular beaches</h2>
    <?php }?>
  </div>


  <div class="featured">
    <?php foreach ($populares as $py) { ?>

    <div>
<a href="<?php echo base_url();?>/playa/detalle/<?php echo $py['slug']; ?>">
<img class="img-responsive" src="<?php echo base_url(); ?>archivos/<?php echo $py['imagen_destacada']; ?> " alt="">
</a>
<?php if( $_SESSION['idioma']   == 'es' ){ ?>

  <h2 style="font-size: 20px;"><span>Playa </span><?php echo $py["nombre"]; ?> </h2>
<? }else if( $_SESSION['idioma']   == 'en' ){?>

  <h2 style="font-size: 20px;"><span><?php echo $py["nombre"]; ?> </span> Beach </h2>
<?php }?>

<div class="stars3">
  <?php
  $votos = $py['total'] ;
  $registros = $py['registros'];

  if(empty($votos)){
    $star=0;
  }else{
    $star = ($votos/$registros);
  }


  if($star > 0){


  for($i=1 ; $i<=$star; $i++){
  ?>

  <i class="fa fa-star" aria-hidden="true"></i>
  <?php

  }
  for($i=$star ; $i<5; $i++){
  ?>

  <i class="fa fa-star-o" aria-hidden="true"></i>
  <?php

  }

  }else{
  ?>
  <i class="fa fa-star-o" aria-hidden="true"></i>
  <i class="fa fa-star-o" aria-hidden="true"></i>
  <i class="fa fa-star-o" aria-hidden="true"></i>
  <i class="fa fa-star-o" aria-hidden="true"></i>
  <i class="fa fa-star-o" aria-hidden="true"></i>

  <?php

  }
  ?>
</div>

<p> <?php echo $py['zona_es']; ?> </p>
    </div>
  <?php } ?>
  </div>








</section>
