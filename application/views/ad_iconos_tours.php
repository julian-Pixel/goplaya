<?php
  $this->load->view('commons/header');
?>

<div class="save_ajax">
   <img src="<?php echo base_url();?>/theme/img/clock.gif" alt="">

</div>

<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">

           <h2><?php echo  $GLOBALS['agregar_iconos'];?></h2>

      </div>
      </div>

    </div>
    <div class="row ctool">

        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><?php echo $GLOBALS['volver_al_dashboard'];?></a>
    </div>

</div>



<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">


<div class="row">

      <div class="col-xs-12 tool-bar">
      <a data-toggle="modal" class="btn-default-gp" data-target="#slideModal" href="#"><?php echo  $GLOBALS['agregar_icono'];?></a>
    </div>


    <table class="table table-striped">


        <thead>
            <tr>
                <th><?php echo  $GLOBALS['icono'];?></th>
                <th><?php echo  $GLOBALS['titulo_en_es'];?></th>
                <th><?php echo  $GLOBALS['titulo_en_en'];?></th>
                <th><?php echo  $GLOBALS['descripcion_es'];?></th>
                <th><?php echo  $GLOBALS['descripcion_en'];?></th>
                <th><?php echo  $GLOBALS['url_es'];?></th>
                <th><?php echo  $GLOBALS['url_en'] ;?></th>
                <th><?php echo  $GLOBALS['acciones_to'];?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($iconos_tours as $icono){ ?>
            <tr>
                <td><img style="width: 100px;" src="<?php echo base_url()?>archivos/<?php echo $icono->icono; ?>" alt=""></td>
                <td><?php echo $icono->descripcion_esp; ?></td>
                <td><?php echo $icono->descripcion_eng; ?></td>
                <td><?php echo $icono->texto_esp; ?></td>
                <td><?php echo $icono->texto_eng; ?></td>
                <td><?php echo $icono->url_esp; ?></td>
                <td><?php echo $icono->url_eng; ?></td>
                <td><a href="<?php echo base_url(); ?>tours/eliminar_iconos_tours/<?php echo $icono->id; ?>"><?php echo  $GLOBALS['eliminar'];?></a>
                    <a class="iconos-modal" data-toggle="modal" data-id="<?php echo $icono->id; ?>" href="javascript:void(0)"><?php echo  $GLOBALS['editar'];?></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>


    </table>



</div>

</div>


<!-- Modal -->
<div class="modal fade" id="slideModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo  $GLOBALS['agregar_icono'];?></h4>
      </div>
      <div class="modal-body clearfix">
       <form action="<?php echo base_url(); ?>tours/agregar_iconos" method="post" enctype="multipart/form-data">

	  		<div class="col-md-12">
				<input class="filestyle" type="file"  data-buttonText="<?php echo    $GLOBALS['buscar_icono'];?>" name="uploadedimages[]" data-icon="false" multiple>
				<br/>
				<div id="vpp"></div>

	  		</div>


        <!-- <select class="form-control" name="playa">

<option value="">No aparecen en una playa</option>
          <?php foreach ($playas as $playa) {
          ?>
            <option value="<?php echo $playa->idplaya;?>"><?php echo $playa->nombre;?></option>
          <?php
            }
          ?>

        </select> -->



        <input type="text" name="descripcion_esp"  class="form-control" placeholder="<?php echo  $GLOBALS['titulo_en_es'];?>">
        <input type="text" name="descripcion_eng"  class="form-control" placeholder="<?php echo  $GLOBALS['titulo_en_en'];?>">
        <input type="text" name="texto_esp" class="form-control" placeholder="<?php echo  $GLOBALS['descripcion_es'];?>">
        <input type="text" name="texto_eng"  class="form-control" placeholder="<?php echo  $GLOBALS['descripcion_en'];?>">
        <input type="text" name="url_esp" class="form-control" placeholder="<?php echo  $GLOBALS['url_es'];?>">
        <input type="text" name="url_eng"  class="form-control" placeholder="<?php echo  $GLOBALS['url_en'] ;?>">

 <input type="submit" value="<?php echo $GLOBALS['agregar'];?>" class="btn btn-theme  btn-default-gp-v">


          </form>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $GLOBALS['editar_icono'];?></h4>
      </div>
      <div class="modal-body clearfix">
       <form action="<?php echo base_url(); ?>tours/editar_iconos_tour" method="post" enctype="multipart/form-data">
           <input type="hidden" name="id" id="id" class="form-control" placeholder="Descripción en español">
           <input type="hidden" name="nombreicono" id="nombreicono" class="form-control" placeholder="Descripción en español">
	  		<div class="col-md-12">
				<input class="filestyle" type="file"  data-buttonText="<?php echo    $GLOBALS['buscar_icono'];?>" name="uploadedimages[]" id="imagen" data-icon="false" multiple>
				<br/>
				<div id="vpp"></div>

	  		</div>


        <!-- <select class="form-control" name="playa">

<option value="">No aparecen en una playa</option>
          <?php foreach ($playas as $playa) {
          ?>
            <option value="<?php echo $playa->idplaya;?>"><?php echo $playa->nombre;?></option>
          <?php
            }
          ?>

        </select> -->
        <input type="text" name="descripcion_esp"  class="form-control" placeholder="<?php echo  $GLOBALS['titulo_en_es'];?>">
        <input type="text" name="descripcion_eng"  class="form-control" placeholder="<?php echo  $GLOBALS['titulo_en_en'];?>">
        <input type="text" name="texto_esp" class="form-control" placeholder="<?php echo  $GLOBALS['descripcion_es'];?>">
        <input type="text" name="texto_eng"  class="form-control" placeholder="<?php echo  $GLOBALS['descripcion_en'];?>">
        <input type="text" name="url_esp" class="form-control" placeholder="<?php echo  $GLOBALS['url_es'];?>">
        <input type="text" name="url_eng"  class="form-control" placeholder="<?php echo  $GLOBALS['url_en'] ;?>">


 <input type="submit" value="<?php echo  $GLOBALS['editar'];?>" class="btn btn-theme  btn-default-gp-v">


          </form>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="container-fluid">



<?php
  $this->load->view('commons/footer');
?>
</div>
