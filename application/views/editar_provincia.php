<?php
  $this->load->view('commons/header');
?>

<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">
          
           <h2>Editar provincia</h2>
         
      </div>
      </div>
        
    </div>   
    <div class="row ctool">
        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
    </div>
    
</div>

<div class="container-fluid">
  
  
  <div class="panel panel-default" style="margin-top:2rem;">
  <div class="panel-body">
     <form action="<?php echo base_url()?>index.php/dashboard/update_prov" method="post">
      <input type="hidden" name="id" value="<?php echo $region[0]->idprovincia; ?>">
       
       <div class="form-group">
                <label for="nombre_provincia" class="control-label">Nombre</label>
                 <input type="text" name="nombre_provincia" class="form-control"  value="<?php echo $region[0]->nombre; ?>" required>
             </div>
             
             <div class="form-group">
                <label for="latitud_provincia" class="control-label">Latitud</label>
                 <input type="text" name="latitud_provincia" class="form-control numero2" value="<?php echo $region[0]->latitud; ?>" required>
             </div>
             <div class="form-group">
                <label for="longitud_provincia" class="control-label">Longitud</label>
                 <input type="text" name="longitud_provincia" class="form-control numero2" value="<?php echo $region[0]->longitud; ?>" required>
             </div>
       
       
       
       
             
    <input type="submit" class="btn btn-default" value="Actualizar">
   </form>
  </div>
</div>
   
  
   
   
</div>


<div class="container-fluid">

<?php
  $this->load->view('commons/footer');
?>
</div>

