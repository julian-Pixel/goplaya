<?php
  $this->load->view('commons/header');
?>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">


<style media="screen">
.wizard {
    padding: 0 !Important;
    background: white !Important;
}
  .wizard .nav-tabs {
      position: relative;
      margin: 40px auto;
      margin-bottom: 0;
      border-bottom-color: #e0e0e0;
  }

  .wizard > div.wizard-inner {
    position: relative;
    background: #f7f7f7;
    padding-bottom: 32px;
}

  .connecting-line {
      height: 2px;
      background: #e0e0e0;
      position: absolute;
      width: 70%;
      margin: 0 auto;
      left: 0;
      right: 0;
      top: 35%;
      z-index: 1;
  }

.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
  color: #555555;
  cursor: default;
  border: 0;
  border-bottom-color: transparent;
}

.tab-content{
    padding: 2%;
}

span.round-tab {
  width: 70px;
  height: 70px;
  line-height: 70px;
  display: inline-block;
  border-radius: 100px;
  background: #fff;
  border: 2px solid #e0e0e0;
  z-index: 2;
  position: absolute;
  left: 0;
  text-align: center;
  font-size: 25px;
}
span.round-tab i{
  color:#555555;
}
.wizard li.active span.round-tab {
  background: #fff;
  border: 2px solid #5bc0de;
      margin-top: -10px;

}
.wizard li.active span.round-tab i{
  color: #5bc0de;
}

span.round-tab:hover {
  color: #333;
  border: 2px solid #333;
}

.wizard .nav-tabs > li {
    width: 33%;
    text-align: center;
}
.wizard li:after {
  content: " ";
  position: absolute;
  left: 46%;
  opacity: 0;
  margin: 0 auto;
  bottom: 0px;
  border: 5px solid transparent;
  border-bottom-color: #5bc0de;
  transition: 0.1s ease-in-out;
}



.wizard .nav-tabs > li a {
  width: 70px;
  height: 70px;
  margin: 20px auto;
  border-radius: 100%;
  padding: 0;
}

  .wizard .nav-tabs > li a:hover {
      background: transparent;
  }

.wizard .tab-pane {
  position: relative;
  padding-top: 50px;
}

.wizard h3 {
  margin-top: 0;
}

.nav-tabs>li>a {
  background: white  !important;
}

.o-cart {
    display: block;
    border-top: 0;
}

.o-cart-items {
    display: block;
}
.o-cart-items__item {
    padding: 3.125vw;
    border-bottom: .15625vw solid #e0e0e0;
    font-size: 0;
    background-color: #f7f7f7;
    overflow: hidden;
}
.o-cart-items__item-wrapper {
    display: flex;
    position:relative;
}

.o-cart-items__item__delete {
    position: absolute;
    top: 0;
    right: 0;
    z-index: 10;
}

.o-cart-items__item__image {
    position: relative;
    width: 31%;
    max-width: 31%;
    min-width: 31%;
    flex: 1 auto;
    height: auto;
}

.o-cart-items__item__image img {
    min-width: 100%;
    max-width: 100%;
    max-height: 100%;
    width: auto;
    height: auto;
}

.o-cart-items__item__details {
    min-height: 0;
    margin-top: -.375rem;
    padding: 0 2.125rem 2.0625rem 1.25rem;
    flex: 1 auto;
    width: 69%;
    max-width: 69%;
    min-width: 69%;
        position:relative;
}
.o-cart-items__item__details__title {
    margin-bottom: 2.34375vw;
    font-weight: 600;
    font-size: 25px;
    color: #064b74;
    line-height: 1.2;
    margin-bottom: 0;
}

.o-cart-items__item__details__cancel, .o-cart-items__item__details__date, .o-cart-items__item__details__from, .o-cart-items__item__details__persons, .o-cart-items__item__details__persons-extended, .o-cart-items__item__details__persons-extended p, .o-cart-items__item__details__to, .o-cart-items__item__details__type {
    margin-bottom: .5rem;
    font-size: 1.5rem;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    padding-right: 0;
    margin-bottom: 0;
}

.o-cart-items__item__details__cancel {
    position: absolute;
    bottom: 0;
    margin-bottom: 0;
    color: #690;
    line-height: 1.5;
}

.o-cart-items__item__details__price {
    margin-bottom: 0;
    font-weight: 700;
    font-size: 4vw;
    color: #064B74;
    line-height: 1;
    text-transform: capitalize;
    position: absolute;
    bottom: 0;
    right: 0;
}

.o-cart-items__item__delete__icon {
    display: inline-table;
    vertical-align: bottom;
    line-height: 0;
    text-indent: -9999px;
}

.borrar-tour {
    position: absolute;
    right: 30px;
    color: grey;
    font-size: 26px;
    top: 10px;
}
.total{
    background: #f7f7f7;
    padding: 0 !Important;
}

.total h3{
    background: white;
    padding: 10px;
    text-align: center;
    font-size: 39px;
    color: #064b73;
    border: solid 1px #e0e0e06b;
}

.total h3 span{
    font-size: 18px;
    color: #c5c5c5;
    /* float: left; */
    /* line-height: 44px; */
}
@media( max-width : 585px ) {

  .wizard {
      width: 90%;
      height: auto !important;
  }

  span.round-tab {
      font-size: 16px;
      width: 50px;
      height: 50px;
      line-height: 50px;
  }

  .wizard .nav-tabs > li a {
      width: 50px;
      height: 50px;
      line-height: 50px;
  }

  .wizard li.active:after {
      content: " ";
      position: absolute;
      left: 35%;
  }
}
</style>
<div class="container-fluid">


	<div class="row">
		<section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" >
                            <span class="round-tab">
                                <i class="fa fa-shopping-bag" aria-hidden="true"></i>

                            </span>
                        </a>
                          <strong><?php echo $GLOBALS['Revisa_tu_perdido'] ?></strong>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" >
                            <span class="round-tab">
                                <i class="fa fa-id-card-o" aria-hidden="true"></i>
                            </span>

                        </a>
                          <strong><?php echo $GLOBALS['Datos_personales']?></strong>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" >
                            <span class="round-tab">
                                <i class="fa fa-credit-card" aria-hidden="true"></i>
                            </span>
                        </a>
                        <strong><?php echo $GLOBALS['Metodo_de_pago'] ?></strong>
                    </li>


                </ul>
            </div>

          <form  action="<?php echo base_url();?>index.php/tours/procesar_reserva" method="post">
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">


                        <div class="col-xs-12 col-md-8">

                          <a href="#" class="borrar-tour"> <i class="fa fa-close"></i> </a>

                          <div class="o-cart">

    <li class="o-cart-items__item " data-type="activity">

                <div class="o-cart-items__item-wrapper ">



            <div class="o-cart-items__item__image">
                <picture>
                    <img src="<?php echo base_url(); ?>archivos/<?php echo $tour[0]->imagen; ?>" alt="Excursión" class="img-reserve">
                </picture>
            </div>
            <div class="o-cart-items__item__details">

              <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                  <p class="o-cart-items__item__details__title"><?php echo $tour[0]->nombre; ?></p>
            <? }else if( $_SESSION['idioma']   == 'en' ){?>
                  <p class="o-cart-items__item__details__title"><?php echo $tour[0]->nombre_eng; ?></p>
            <?php }?>



                <br>
                <p class="o-cart-items__item__details__date u-hide--mobile"><?php echo $GLOBALS['La fecha del tour'] ?> <?php

                $date=date_create($fecha);
                echo date_format($date,"d-m-Y");

                 ?>


                 <?php echo $GLOBALS['a_las'] ?> <?php echo $hora; ?> h</p>

                <p class="o-cart-items__item__details__persons u-show--mobile">
                                                                   </p>
                <div class="o-cart-items__item__details__persons-extended u-hide--mobile">


                  <?php
$descuento = $tour[0]->precio * $tour[0]->porcetaje_descuento;


                      foreach ($precios as $precio) {



                          $p = $tour[0]->precio - $descuento;
                          $d = $p * $precio->descuento  ;


                          if($precio->tipo_cliente == 1){
                            if($_SESSION['compra_actual']["adultos"] > 0)
                            {
                            ?>

                            <!-- <p> <?php echo $_SESSION['compra_actual']["adultos"];?> Adultos x <?php echo ($p-$d);?>$</p> -->
                                <p> <?php echo $_SESSION['compra_actual']["adultos"];?><?php echo  $GLOBALS['Adultos'] ?> </p>
                            <?php
                            }

                          }elseif($precio->tipo_cliente == 2){
                            if($_SESSION['compra_actual']["estudiantes"] > 0)
                            {
                              ?>
                              <p> <?php echo $_SESSION['compra_actual']["estudiantes"];?> <?php echo  $GLOBALS['Estudiantes'] ?>  </p>
                              <?php
                            }

                          }elseif($precio->tipo_cliente == 3){
                            if($_SESSION['compra_actual']["ninos"] > 0)
                            {
                            ?>
                            <p> <?php echo $_SESSION['compra_actual']["ninos"];?> <?php echo  $GLOBALS['ninos'] ?> </p>
                            <?php
                            }

                          }elseif($precio->tipo_cliente == 4){
                              if($_SESSION['compra_actual']["menores"] > 0)
                              {

                                ?>
                                <p> <?php echo $_SESSION['compra_actual']["menores"];?><?php echo  $GLOBALS['Menores'] ?>  </p>
                                <?php

                              }

                          }




                      }


                   ?>

                                                                                        </div>
                                <!-- <p class="o-cart-items__item__details__price"><?php



                                                            echo $tour[0]->precio - $descuento;


                                                            ?> $</p> -->
                                </div>
        </div>
    </li>
                                            </ul>
                </div>

                        </div>

                        <div class="col-xs-12 col-md-4 total">

                          <!-- <?php

                          $total_pagar =0;

                              foreach ($precios as $precio) {
                                $p = $tour[0]->precio - $descuento;
                                $d = $p * $precio->descuento  ;

                                if($precio->tipo_cliente == 1){

                                      $st = ($p-$d) *  $_SESSION['compra_actual']["adultos"];
                                      $total_pagar = $total_pagar + $st;



                                }elseif($precio->tipo_cliente == 2){


                                  $st = ($p-$d) *  $_SESSION['compra_actual']["estudiantes"];
                                  $total_pagar = $total_pagar + $st;

                                }elseif($precio->tipo_cliente == 3){


                                  $st = ($p-$d) *   $_SESSION['compra_actual']["ninos"];
                                  $total_pagar = $total_pagar + $st;

                                }elseif($precio->tipo_cliente == 4){


                                  $st = ($p-$d) *  $  $_SESSION['compra_actual']["menores"];
                                  $total_pagar = $total_pagar + $st;

                                }



                                $descuento = $tour[0]->precio * $tour[0]->porcetaje_descuento;

                                $p = $tour[0]->precio - $descuento;
                                $d = $p * $precio->descuento  ;


                              }


                           ?> -->
                            <h3> <span>Total: </span>$<?php echo   $total_pagar; ?> </h3>
                            <input type="hidden" name="xyu7" value="<?php echo   $total_pagar; ?>">
                        <button type="button" class="btn btn-success next-step"><?php echo $GLOBALS['Proceder_con_la_compra'] ?></button>

                        </div>

                    </div>




                    <div class="tab-pane" role="tabpanel" id="step2">



                      <div class="col-xs-12">
                      <h3><?php echo $GLOBALS['Datos_de_la_reserva'] ?></h3>
                      <br>
                      </div>


                        <div class="form-group col-xs-12 col-md-6">
                          <input type="text" name="nombre" class="form-control nombre" placeholder="<?php  echo $GLOBALS['Nombre'] ?>" value="<?php if(isset($datos_user)){ echo $datos_user[0]->nombre; } ?>" required>
                        </div>
                        <div class="form-group col-xs-12 col-md-6">
                          <input type="text" name="apellidos" class="form-control apellidos" placeholder="<?php  echo $GLOBALS['Apellidos'] ?>" value="<?php if(isset($datos_user)){ echo $datos_user[0]->apellidos; } ?>" required>
                        </div>
                        <div class="form-group col-xs-12  col-md-6">
                          <input type="text" name="email" class="form-control correo" placeholder="<?php  echo $GLOBALS['Email'] ?>" value="<?php if(isset($datos_user)){ echo $datos_user[0]->email; } ?>" required>
                        </div>
                        <!-- <div class="form-group col-xs-12 col-md-6">
                          <select class="form-control" name="pais">
                                <option value="CR">COSTA RICA</option>
                          </select>
                        </div> -->
                        <div class="form-group col-xs-12 col-md-6">
                          <input type="text" id="telefonoreserva" name="telefono" class="form-control tel" placeholder="<?php  echo $GLOBALS['Telefono'] ?>" value="<?php if(isset($datos_user)){ echo $datos_user[0]->telefono; } ?>" required>
                        </div>
                        <div class="form-group col-xs-12 col-md-6">
                          <input type="hidden" name="monto" class="form-control"  value="<?php echo base64_encode($total_pagar); ?>" required>
                          <input type="hidden" name="id" class="form-control"  value="<?php echo $_GET['id']; ?>" required>
                          <input type="hidden" name="a" class="form-control"  value="<?php echo $_GET['a']; ?>" required>
                          <input type="hidden" name="e" class="form-control"  value="<?php echo $_GET['e']; ?>" required>
                          <input type="hidden" name="n" class="form-control"  value="<?php echo $_GET['n']; ?>" required>
                          <input type="hidden" name="m" class="form-control"  value="<?php echo $_GET['m']; ?>" required>
                          <input type="hidden" name="fecha" class="form-control"  value="<?php echo $_GET['fecha']; ?>" required>
                          <input type="hidden" name="time" class="form-control"  value="<?php echo $_GET['time']; ?>" required>
                        </div>

                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-primary btn-next"><?php echo $GLOBALS['Continuar'] ?></button></li>
                        </ul>



                    </div>
                    <div class="tab-pane" role="tabpanel" id="step3">
                        <h3><?php echo $GLOBALS['Elija_un_metodo_de_pago'] ?></h3>

<input class="metodo-pago" type="radio" name="pago" value="1" checked> Pay-me
<br><br>
                        <ul class="list-inline pull-right">

                            <li style="margin-bottom: 15px;"><button type="button" class="btn btn-default prev-step"><?php echo $GLOBALS['Anterior'] ?></button></li>
                            <li style="margin-bottom: 15px;"><button type="submit" class="btn btn-success btn-info-full next-step"><?php echo $GLOBALS['Proceder_con_la_compra'] ?></button></li>
                        </ul>

                      </form>

                    </div>

                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </section>
   </div>




</div>
<div class="container-fluid">

<?php
  $this->load->view('commons/footer');
?>
</div>

<script type="text/javascript">
$(document).ready(function () {


  var url = window.location.href.split("?")[0];

  if(url = "http://queleparece.com/goplaya/tours/reservar_tour")
  {
    $(".idioma").hide();
  }
  //Initialize tooltips
  $('.nav-tabs > li a[title]').tooltip();

  //Wizard
  $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

      var $target = $(e.target);

      if ($target.parent().hasClass('disabled')) {
          return false;
      }
  });

  $(".next-step").click(function (e) {

      var $active = $('.wizard .nav-tabs li.active');
      $active.next().removeClass('disabled');
      nextTab($active);

  });

  $(".btn-next").click(function (e) {

      if(!$('.nombre').val() || !$('.apellidos').val() || !$('.correo').val() || !$('.tel').val()){
        var html ='<div class="alerta">'
          +'<div class="icono">'
              +'<i class="fa fa-check" aria-hidden="true"></i>'
            +'</div>'
            +'<div class="contenido">'
                +'<h3>Verificar datos</h3>'
              +'<p>Formulario incompleto</p>'
            +'</div>'

        +'</div>';

        $('body').append(html);

        return false;
      }
      else {
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);
      }

  });
  $(".prev-step").click(function (e) {

      var $active = $('.wizard .nav-tabs li.active');
      prevTab($active);

  });
});

function nextTab(elem) {
  $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
  $(elem).prev().find('a[data-toggle="tab"]').click();
}




</script>
