<?php
  $this->load->view('commons/header');
?>

<div class="save_ajax">
   <img src="<?php echo base_url();?>/theme/img/clock.gif" alt="">

</div>

<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">

           <h2><?php echo $GLOBALS['videoexplicativocomosubirtour'];?></h2>

      </div>
      </div>

    </div>
    <div class="row ctool">

        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><?php echo $GLOBALS['volver_al_dashboard'];?></a>
    </div>

</div>



<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">


<div class="row">


    <?php if ($rol_user[0]->rol ==1) { ?>

    <table class="table table-striped">


        <thead>
            <tr>
              <th><?php echo $GLOBALS['Nombre'] ;?></th>
                <th><?php echo $GLOBALS['video_es'];?></th>
                <th><?php echo $GLOBALS['video_en'];?></th>
                <th><?php echo $GLOBALS['accion'];?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($videos_tours as $video){ ?>
            <tr>
                <td><?php echo $video->nombre; ?></td>
                <td><?php echo $video->video; ?></td>
                <td><?php echo $video->video_eng; ?></td>
                <td>
                    <a class="video-modal" data-toggle="modal" data-id="<?php echo $video->id; ?>" href="javascript:void(0)"><?php echo $GLOBALS['editar'];?></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>


    </table>
  <?php } elseif ($rol_user[0]->rol ==2){?>
    <div class="col-xs-12 col-md-12">


      <?php if( $_SESSION['idioma']   == 'es' ){ ?>

        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $videos_tours[0]->video; ?>"
          frameborder="0"
          allow="autoplay; encrypted-media"
          allowfullscreen>
        </iframe>



    <? }else if( $_SESSION['idioma']   == 'en' ){?>
      <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $videos_tours[0]->video_eng; ?>"
        frameborder="0"
        allow="autoplay; encrypted-media"
        allowfullscreen>
      </iframe>
    <?php }?>



    </div>

<?php   } ?>



</div>

</div>


<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $GLOBALS['editar_icono'];?></h4>
      </div>
      <div class="modal-body clearfix">
       <form action="<?php echo base_url(); ?>tours/editar_como_subir_video" method="post" enctype="multipart/form-data">
         <input type="hidden" name="id" id="id" class="form-control" >


        <input type="text" name="video" id="video" class="form-control" placeholder="<?php echo $GLOBALS['video_es'];?>">
        <input type="text" name="video_eng" id="video_eng" class="form-control" placeholder="<?php echo $GLOBALS['video_en'];?>">

 <input type="submit" value="<?php echo $GLOBALS['editar'];?>" class="btn btn-theme  btn-default-gp-v">


          </form>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="container-fluid">



<?php
  $this->load->view('commons/footer');
?>
</div>
