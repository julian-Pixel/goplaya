<?php
  $this->load->view('commons/header');
?>

<div class="save_ajax">
   <img src="<?php echo base_url();?>/theme/img/clock.gif" alt="">

</div>

<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">

           <h2><?php echo $GLOBALS['opiniones'];?></h2>

      </div>
      </div>

    </div>
    <div class="row ctool">

        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><?php echo $GLOBALS['volver_al_dashboard'];?></a>
    </div>

</div>



<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">


<div class="row">


    <?php if ($rol_user[0]->rol ==1) { ?>

    <table class="table table-striped">


        <thead>
            <tr>
              <th>Nombre</th>
                <th>Correo</th>
                <th>Opinión</th>
                <th>Tour</th>
                <th><?php echo $GLOBALS['habilitado'];?></th>
                <th>Fecha</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
          <?php foreach($opiniones as $opinion){ ?>
            <tr>
                <td><?php echo $opinion->nombre; ?></td>
                <td><?php echo $opinion->correo; ?></td>
                <td><?php echo $opinion->opinion; ?></td>
                <td><?php echo $opinion->nombre_tour; ?></td>

                     <?php if($opinion->habilitado =='1'){ ?>
                  <td>Si</td>
                  <?php }else{ ?>
                                    <td>No</td>
                  <?php } ?>
                <td><?php echo $opinion->fecha; ?></td>
                <td>
                     <?php if($opinion->habilitado =='1'){ ?>
                    <a class="btn btn-default" href="<?php echo base_url();?>tours/aprobar_opiniones?id=<?php echo $opinion->id; ?>&e=2"><?php echo $GLOBALS['rechazar'] ;?></a>

                  <?php }else{ ?>
                    <a class="btn btn-default" href="<?php echo base_url();?>tours/aprobar_opiniones?id=<?php echo $opinion->id; ?>&e=1"><?php echo $GLOBALS['aprobar'] ;?></a>

                  <?php } ?>
                </td>
            </tr>
            <?php } ?>
        </tbody>


    </table>
  <?php } ?>



    </div>





</div>

</div>

<div class="container-fluid">



<?php
  $this->load->view('commons/footer');
?>
</div>
