<?php
$this->load->view('commons/header');

?>




<div class="container">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title txt-center-mobile">
    			<h1><?php echo $GLOBALS['Reservacion']; ?></h2>
            <div class="pull-right invoice-title txt-center-mobile" style="text-align: right;">
            <h3># <?php echo  $reserva[0]->idreservacion; ?></h3>

            <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                <span><?php echo  $info[0]->nombre; ?></span>
            <? }else if( $_SESSION['idioma']   == 'en' ){?>
                <span><?php echo  $info[0]->nombre_eng; ?></span>
            <?php }?>



            </div>
    		</div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-12 col-md-6 txt-center-mobile">
    				<address>
    				<strong><?php  echo $GLOBALS['Facturado a']; ?></strong><br>
    					<?php echo  $reserva[0]->nombre; ?> <?php echo  $reserva[0]->apellidos; ?><br>
    					<?php echo  $reserva[0]->pais; ?><br>
    					<?php echo  $reserva[0]->telefono; ?><br>
    					<?php echo  $reserva[0]->email; ?>
    				</address>
    			</div>

    		</div>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong><?php echo  $GLOBALS['Metodo_de_pago'] ; ?>:</strong><br>


              <?php if($reserva[0]->estado == '0'){ ?>

                <span style="margin-top:10px;display: inline-block;" class="label label-danger" >Pendiente de pago</span>
              <?php }else{ ?>
                Visa ending **** 4242<br>
      					jsmith@email.com
      				</address>
              <?php } ?>

    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
    					<strong> <i class="fa fa-calendar-check-o"></i> <?php echo $GLOBALS['fechar_to']; ?></strong><br>
    					<?php echo  $reserva[0]->fecha; ?><br><br>
              <strong> <i class="fa fa-calendar"></i> <?php echo $GLOBALS['fechat_to']; ?></strong><br>
    					<?php echo  $reserva[0]->fecha_tour; ?><br><br>
              <strong> <i class="fa fa-clock-o"></i> <?php echo $GLOBALS['hora_tour']; ?></strong><br>
              <?php echo  $reserva[0]->hora_tour; ?><br><br>
    				</address>

    			</div>
    		</div>
    	</div>
    </div>
<div class="row">
<h3><?php echo $GLOBALS['Personas']; ?></h3>
<center>


<div class="personas fin-reserva prs">

  <?php  if($reserva[0]->adultos >0){?>
  <li>
    <br>
    <span><?php echo $GLOBALS['Adultos']; ?></span>

    <span class="cantidad circle-reserva"><?php echo $reserva[0]->adultos; ?></span>

   </li>
<?php  }?>
  <?php  if($reserva[0]->estudiantes >0){?>
  <li>
    <br>
    <span ><?php echo $GLOBALS['Estudiantes']; ?></span>
       <span class="cantidad circle-reserva"><?php echo $reserva[0]->estudiantes; ?></span> </li>
<?php  }?>
<?php  if($reserva[0]->ninos >0){?>
  <li>
    <br>
    <span><?php echo $GLOBALS['ninos']; ?></span>
       <span class="cantidad circle-reserva"><?php echo $reserva[0]->ninos; ?></span> </li>

  </li>
<?php  }?>
  <?php
  if($reserva[0]->menores > 0){?>

  <li >   <span><?php echo $GLOBALS['Menores']; ?></span>
      <br>
       <span class="cantidad circle-reserva"><?php echo $reserva[0]->menores; ?></span>

     </li>
<?php } ?>
</div>
</center>

</div>



<div class="row">
<div class="col-xs-12">
  <br>
  <h3 class="txt-center-mobile"><?php echo $GLOBALS['Detalles_del_tour']; ?></h3>
  <br>
</div>
  <div class="col-xs-6">
<img src="<?php echo base_url();?>archivos/<?php echo  $info[0]->imagen; ?>" class="img-responsive" alt="">
  </div>

  <div class="col-xs-6" style="text-align: justify;">

    <?php if( $_SESSION['idioma']   == 'es' ){ ?>

        <?php echo  $info[0]->descripcion; ?>
    <? }else if( $_SESSION['idioma']   == 'en' ){?>
        <span><?php echo  $info[0]->descripcion_eng; ?></span>
    <?php }?>
  </div>

  <div class="col-xs-12">
<br><br>
    <h3>Itinerario</h3>
    <?php if( $_SESSION['idioma']   == 'es' ){ ?>

        <?php echo  $info[0]->itinerario_es; ?>
    <? }else if( $_SESSION['idioma']   == 'en' ){?>
        <?php echo  $info[0]->itinerario_en; ?>
    <?php }?>

  <br><br>
  </div>



</div>



</div>
<?php
$this->load->view('commons/footer');

?>


<script type="text/javascript">
$( window ).load(function() {
  var table = $('table').DataTable();

    table.destroy();
});
</script>
