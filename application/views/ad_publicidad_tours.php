<?php
  $this->load->view('commons/header');
?>

<div class="save_ajax">
   <img src="<?php echo base_url();?>/theme/img/clock.gif" alt="">

</div>

<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">

           <h2><?php echo $GLOBALS['AGREGAR_PUBLICIDAD_A_LOS_TOURS'];?></h2>

      </div>
      </div>

    </div>
    <div class="row ctool">

        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><?php echo $GLOBALS['volver_al_dashboard'];?></a>
    </div>

</div>



<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">


<div class="row">

      <div class="col-xs-12 tool-bar">
      <a data-toggle="modal" class="btn-default-gp" data-target="#slideModal" href="#"><?php echo $GLOBALS['agregar_banner'] ;?></a>
    </div>


    <table class="table table-striped">


        <thead>
            <tr>
                <th><?php echo $GLOBALS['banner'] ;?></th>
                <th><?php echo $GLOBALS['posicion'];?></th>
                <th><?php echo $GLOBALS['acciones_to'];?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($banners as $banner){ ?>
            <tr>
                <td><img style="width: 100px;" src="<?php echo base_url()?>archivos/<?php echo $banner->banner; ?>" alt=""></td>
                <td>



                <?php  if($banner->posicion == '1'){ echo 'Home: Superior'; }else if($banner->posicion == '2'){ echo 'Home: Medio'; }else if($banner->posicion == '3'){ echo 'Home: Final'; }else if($banner->posicion == '4'){ echo 'Home: Buscador'; } ?>
                </td>
                <td><a href="<?php echo base_url(); ?>publicidad_tours/eliminar_item/<?php echo $banner->id; ?>"><?php echo $GLOBALS['eliminar'];?></a></td>
            </tr>
            <?php } ?>
        </tbody>


    </table>



</div>

</div>


<!-- Modal -->
<div class="modal fade" id="slideModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $GLOBALS['agregar_banner'];?></h4>
      </div>
      <div class="modal-body clearfix">
       <form action="<?php echo base_url(); ?>publicidad_tours/agregar_items" method="post" enctype="multipart/form-data">

	  		<div class="col-md-12">
				<input class="filestyle" type="file"  data-buttonText="<?php echo   $GLOBALS['buscar_img_es'];?>" name="uploadedimages[]" data-icon="false" multiple>
				<br/>
				<div id="vpp"></div>

	  		</div>



        <div class="col-md-12">
        <input class="filestyle" type="file"  data-buttonText="<?php echo   $GLOBALS['buscar_img_en'];?>" name="uploadedimagesing[]" data-icon="false" multiple>
        <br/>
        <div id="vpping"></div>

        </div>
        <!-- <select class="form-control" name="playa">

<option value="">No aparecen en una playa</option>
          <?php foreach ($playas as $playa) {
          ?>
            <option value="<?php echo $playa->idplaya;?>"><?php echo $playa->nombre;?></option>
          <?php
            }
          ?>

        </select> -->
        <input type="text" name="url" class="form-control" placeholder="<?php echo $GLOBALS['url_pag_es'];?>">

        <input type="text" name="url_ingles" class="form-control" placeholder="<?php echo $GLOBALS['url_pag_en'];?>">
	  		<select name="posicion" class="form-control" id="">
           <option value="20"><?php echo $GLOBALS['noapareceeltour'];?></option>
	  		    <option value="1">Tour: Superior</option>
	  		    <option value="2">Tour: Medio</option>
	  		    <option value="3">Tour: Final</option>
	  		</select>
 <input type="submit" value="<?php echo $GLOBALS['agregar'];?>" class="btn btn-theme  btn-default-gp-v">


          </form>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="container-fluid">



<?php
  $this->load->view('commons/footer');
?>
</div>
