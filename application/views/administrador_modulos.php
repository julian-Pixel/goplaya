<?php
  $this->load->view('commons/header');
?>




<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">
          
           <h2>Modulos</h2>
         
      </div>
      </div>
        
    </div>   
    <div class="row ctool">
        
        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
    </div>
    
</div>
<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">
 
 <div>
     
 </div>
 
 
 <table class="table table-striped">
     <thead>
         <tr>
             <th>Modulo</th>
             <th>Estado</th>
         </tr>
     </thead>

<tbody>
   <?php foreach($disponibles as $disponible){ ?>
   <tr>
        <td><?php echo $disponible ?></td>
        <td>Inactivo</td>
        <td><a href="<?php echo base_url();?>modulos/activar_modulo?m=<?php echo $disponible ?>">Activar</a></td>
    </tr>


<?php } ?>
   
   <?php foreach($secciones as $seccion){ ?>
   <tr>
        <td><?php echo $seccion->modulo; ?></td>
        <td>Activo</td>
    </tr>


<?php } ?>
<tr>
    <td><a href="<?php echo base_url(); ?>dashboard/suscriptores">Suscriptores</a></td>
    <td>Activo</td>
</tr> 
<tr>
    <td><a href="<?php echo base_url(); ?>dashboard/ajustes_generales">Ajustes del sitio</a></td>
    <td>Activo</td>
</tr>
<tr>
    <td><a href="<?php echo base_url(); ?>dashboard/rutas_recomendadas">Rutas recomendadas</a></td>
    <td>Activo</td>
</tr>
</tbody>
 </table>



</div>
<div class="container-fluid">

<?php
  $this->load->view('commons/footer');
?>
</div>

