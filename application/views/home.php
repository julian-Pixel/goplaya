<?php
  $this->load->view('commons/header');
?>


<!-- lang('global.titulo')
anchor('search_controller/buscar', lang('global.titulo')) -->

<div class="social-media">

    <ul>
        <li><a href="<?php echo $datos_generales[0]->facebook; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
        <li><a href="<?php echo $datos_generales[0]->instagram; ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
        <li><a href="<?php echo $datos_generales[0]->youtube; ?>" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
    </ul>


</div>





<?php

    foreach($secciones as $seccion){

        $this->load->view('modulos/'.$seccion->modulo);

    }

?>

<div class="container-fluid">
<?php
  $this->load->view('commons/footer');
?>

</div>
