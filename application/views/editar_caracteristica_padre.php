<?php
  $this->load->view('commons/header');
?>

<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">
          
           <h2>Editar caracteristica</h2>
         
      </div>
      </div>
        
    </div>   
    <div class="row ctool">
        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
    </div>
    
</div>

<div class="container-fluid">
  
  
  <div class="panel panel-default" style="margin-top:2rem;">
  <div class="panel-body">
     <form action="<?php echo base_url()?>index.php/dashboard/update_catpad" method="post">
      <input type="hidden" name="id" value="<?php echo $caracteristica[0]->id; ?>">
       <div class="form-group">
          <label for="">Detalle Español</label>
           <input type="text" class="form-control" name="detalle" value="<?php echo $caracteristica[0]->detalle; ?>">
       </div>
       
       <div class="form-group">
          <label for="">Detalle Inglés</label>
           <input type="text" class="form-control" name="detalle_en" value="<?php echo $caracteristica[0]->detalle_en; ?>">
       </div>
       
       
       
       <div class="form-group">
                <label for="simple" class="control-label">Oculto</label>
                 <select name="oculto" class="form-control" id="">
                    
                     <option value="0" <?php if($caracteristica[0]->oculto== 0){echo 'selected';} ?>>No</option>
                     <option value="1" <?php if($caracteristica[0]->oculto == 1){echo 'selected';} ?>>Si</option>
                    
                 </select>
             </div>
             
             <input type="submit" class="btn btn-default" value="Actualizar">
   </form>
  </div>
</div>
   
  
   
   
</div>


<div class="container-fluid">

<?php
  $this->load->view('commons/footer');
?>
</div>

