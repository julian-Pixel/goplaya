<?php
  $this->load->view('commons/header');
?>


<div class="container-fluid">
   <div class="row">
    <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31439.958727521353!2d-84.11772307383785!3d9.934386550803326!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8b0f4f71ee0eade5!2sParque+Central!5e0!3m2!1ses-419!2ses!4v1495490007956" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe> -->
    </div>
    <div class="row" style="padding-top:3%;padding-bottom:3%;">
        <section class="col-xs-12 col-md-6">
           <h2><?php echo $GLOBALS['titulo_ic']; ?></h2>

           <ul class="contact-list">
               <li><i class="fa fa-map-marker"></i> <a href="#"><?php echo $datos_generales[0]->direccion; ?></a></li>
               <li><i class="fa fa-envelope-o"></i> <a href="mailto:<?php echo $datos_generales[0]->email_contacto; ?>"><?php echo $datos_generales[0]->email_contacto; ?></a></li>
               <li><i class="fa fa-phone"></i> <a href="tel:<?php echo $datos_generales[0]->telefono; ?>"><?php echo $datos_generales[0]->telefono; ?></a></li>
           </ul>

        </section>
        <div class="col-xs-12 col-md-6">
            <form action="<?php echo base_url();?>contacto/enviar" method="post">

                <div class="col-xs-12 col-md-6">
                    <input type="text" name="nombre" placeholder="<?php echo $GLOBALS['nombre']; ?>" class="form-control">
                </div>
                <div class="col-xs-12 col-md-6">
                    <input type="email" name="email" placeholder="<?php echo $GLOBALS['email']; ?>" class="form-control">
                </div>
                 <div class="col-xs-12">
                    <input type="text" name="asunto" placeholder="<?php echo $GLOBALS['asunto']; ?>" class="form-control">
                </div>
                   <div class="col-xs-12">
                    <textarea name="mensaje" id="" class="form-control" cols="30" placeholder="<?php echo $GLOBALS['mensaje']; ?>" rows="10"></textarea>
                </div>
                <input class="btn-default-gp btnf2" style="float:right;" type="submit" value="<?php echo $GLOBALS['enviar']; ?>">

            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
<?php
  $this->load->view('commons/footer');
?>


</div>
