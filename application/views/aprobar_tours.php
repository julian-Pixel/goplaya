<?php
  $this->load->view('commons/header');
?>




<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">

           <h2><?php echo $GLOBALS['tours'];?></h2>

      </div>
      </div>

    </div>
    <div class="row ctool">

        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><?php echo $GLOBALS['volver_al_dashboard'] ;?></a>
        <a data-open="modal" data-target="#agregar-tour" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><?php echo $GLOBALS['add_tour_es'];?></a>
    </div>

</div>
<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">

<h3><?php echo $GLOBALS['tours'];?></h3>
   <div class="table-responsive">
 <table class="table table-striped">
     <thead>
         <tr>
           <th><?php echo   $GLOBALS['tour_operadora'] ;?></th>
           <th><?php echo   $GLOBALS['codigo'] ;?></th>
           <th><?php echo $GLOBALS['tour_to'];?></th>
          <th><?php echo $GLOBALS['estado_to'];?></th>
          <th><?php echo $GLOBALS['habilitado'];?></th>
          <th><?php echo $GLOBALS['acciones'];?></th>

         </tr>
     </thead>

<tbody>
   <?php foreach($tours as $t){ ?>
   <tr>
      <td><?php echo $t->nombre_touroperadora; ?></td>
     <td><?php echo $t->codigo; ?></td>
       <td><?php echo $t->nombre_tour; ?></td>
       <td><?php if($t->estado =='1'){ echo $GLOBALS['aprobado']; }else{ echo $GLOBALS['pendiente']; }?></td>
       <td><?php if($t->habilitado =='1'){ echo 'Si'; }else{ echo 'No'; }?></td>
       <td>


         <?php if($t->estado =='1'){ ?>

      <a class="btn btn-default" href="<?php echo base_url();?>index.php/dashboard/estadotour?id=<?php echo $t->idtour; ?>&e=2"><?php echo $GLOBALS['rechazar'] ;?></a>

          <?php }else{ ?>
     <a class="btn btn-default" href="<?php echo base_url();?>index.php/dashboard/estadotour?id=<?php echo $t->idtour; ?>&e=1"><?php echo $GLOBALS['aprobar'] ;?></a>


          <?php } ?>

   <a class="btn btn-default" href="<?php echo base_url();?>tours/tour/<?php echo $t->idtour; ?>"><?php echo  $GLOBALS['ver_tour'];?></a>
   <a class="btn btn-default" href="<?php echo base_url();?>index.php/tour_operadora/configurar_tour/<?php echo $t->idtour; ?>"><?php echo $GLOBALS['editar_tour'];?></a>

       </td>
   </tr>


<?php } ?>

</tbody>
 </table>


<h3><?php echo $GLOBALS['Comisiones_pendientes_de_pagar']  ;?></h3>
       <table class="table table-striped">
         <thead>
           <tr>
             <th><?php echo $GLOBALS['tour'] ;?></th>
             <th><?php echo $GLOBALS['#reservacion'] ;?></th>
             <th><?php echo $GLOBALS['fecha_del_tour']  ;?></th>
             <th><?php echo $GLOBALS['operadora'] ;?></th>
             <th><?php echo $GLOBALS['monto'];?></th>
             <th><?php echo $GLOBALS['reserva'] ;?></th>
             <th><?php echo $GLOBALS['acciones_to'];?></th>

           </tr>
         </thead>

         <tbody>

           <?php foreach ($mis_reservas as $reserva) {?>
           <tr>
             <td><?php echo $reserva->tour_nombre; ?></td>
             <td><?php echo $reserva->idreservacion; ?></td>
             <td><?php echo date("d-m-Y", strtotime($reserva->fecha_tour)); ?> </td>
             <td><?php echo $reserva->codt; ?> | <?php echo $reserva->nomt; ?> </td>
             <td>$ <?php echo $reserva->monto; ?>  </td>
             <td>
               <?php if($reserva->estado == 1){ ?>

                  <span class="label label-success">Procesada</span>
               <?php }elseif($reserva->estado == 0){ ?>

                  <span class="label label-danger">Pendiente de pago</span>
               <?php } ?>

             </td>
             <td> <a href="<?php echo base_url(); ?>tour_operadora/pagado?id=<?php echo $reserva->id;?>&e=1" style="margin-top: 11px;float:right;font-size: 10px;" class="btn-default-gp"><?php echo $GLOBALS['marcapaga']  ;?></a></td>
           </tr>

           <?php
           } ?>
         </tbody>
       </table>



       <h3><?php echo $GLOBALS['Comisiones_ya_pagadas']   ;?></h3>
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th><?php echo $GLOBALS['tour'] ;?></th>
                      <th><?php echo $GLOBALS['#reservacion'] ;?></th>
                      <th><?php echo $GLOBALS['fecha_del_tour']  ;?></th>
                      <th><?php echo $GLOBALS['operadora'] ;?></th>
                      <th><?php echo $GLOBALS['monto'];?></th>
                      <th><?php echo $GLOBALS['reserva'] ;?></th>
                      <th><?php echo $GLOBALS['acciones_to'];?></th>

                  </tr>
                </thead>

                <tbody>

                  <?php foreach ($mis_reservas2 as $reserva) {?>
                  <tr>
                    <td><?php echo $reserva->tour_nombre; ?></td>
                    <td><?php echo $reserva->idreservacion; ?></td>
                    <td><?php echo date("d-m-Y", strtotime($reserva->fecha_tour)); ?> </td>
                    <td><?php echo $reserva->codt; ?> | <?php echo $reserva->nomt; ?> </td>
                    <td>$ <?php echo $reserva->monto; ?>  </td>
                    <td>
                      <?php if($reserva->estado == 1){ ?>

                         <span class="label label-success">Procesada</span>
                      <?php }elseif($reserva->estado == 0){ ?>

                         <span class="label label-danger">Pendiente de pago</span>
                      <?php } ?>

                    </td>
                    <td> <a href="<?php echo base_url(); ?>tour_operadora/pagado?id=<?php echo $reserva->id;?>&e=0" style="margin-top: 11px;float:right;font-size: 10px;" class="btn-default-gp"><?php echo $GLOBALS['marcanopaga']  ;?></a></td>
                  </tr>

                  <?php
                  } ?>
                </tbody>
              </table>




</div>
</div>








<div class="setup-modal" id="agregar-tour">
<a href="#" class="close-setm"><i class="fa fa-times"></i></a>
<div class="title-wizard">
          <h2> <?php echo $GLOBALS['agregar_tour'] ;?></h2>
      </div>
      <form action="<?php echo base_url()?>tour_operadora/agregar_tour" method="post" data-toggle="validator" role="form">
<div class="step-body active" data-step="1">


  <div class="form-group col-xs-12">
     <label for="nombre_es" class="control-label" ><?php echo   $GLOBALS['tour_operadora'] ;?></label>
             <select class="form-control" name="id_usuario" id="id_usuario">
                <?php
                  foreach($tour_operadora as $operadora){
                 ?>
                 <option value="<?php echo $operadora->idtour_operadora; ?>"><?php echo $operadora->nombre; ?></option>

                 <?php
                  }
                 ?>
             </select>
         </div>



       <div class="form-group col-xs-12">
           <label for="nombre_es" class="control-label" ><?php echo   $GLOBALS['nombre_del_tour_es'] ;?></label>
           <input type="text" class="form-control" name="nombre_es" required>

       </div>

       <div class="form-group col-xs-12">
           <label for="desc_es" class="control-label" ><?php echo   $GLOBALS['descripcion_del_tour_es'] ;?></label>
          <textarea class="form-control" name="desc_es" required cols="30" rows="5"></textarea>

       </div>

       <a class="next-step btn btn-default" data-gostep="2"><?php echo   $GLOBALS['btn_siguiente'] ;?></a>

</div>
    <div class="step-body" data-step="2">
         <div class="form-group col-xs-12">
           <label for="nombre_en" class="control-label" ><?php echo   $GLOBALS['nombre_del_tour_en'] ;?></label>
           <input type="text" class="form-control" name="nombre_en" required>

       </div>


        <div class="form-group col-xs-12">
           <label for="desc_en" class="control-label" ><?php echo   $GLOBALS['descripcion_del_tour_en'] ;?></label>

           <textarea class="form-control" name="desc_en" required cols="30" rows="5"></textarea>

       </div>
       <a class="prev-step btn btn-default" data-gostep="1"><?php echo   $GLOBALS['btn_anterior'] ;?></a>
       <a class="next-step btn btn-default" data-gostep="3"><?php echo   $GLOBALS['btn_siguiente'] ;?></a>
    </div>
    <div class="step-body" data-step="3">

        <div class="form-group col-xs-12">
           <label for="precio" class="control-label" ><?php echo   $GLOBALS['ingrese_fecha_de_su_tour'];?></label>
           <input type="text" class="form-control date" name="fecha" required>

       </div>
       <div class="form-group col-xs-12">
           <label for="zona" class="control-label" ><?php echo   $GLOBALS['su_tour_se_realizar_cerca_de_una_playa'];?></label>
           <select name="zona" id="" class="form-control">
              <?php foreach($playas as $playa){?>
                  <option value="<?php echo $playa->idplaya; ?>"><?php echo $playa->nombre; ?></option>
              <?php } ?>

           </select>

       </div>

           <a class="prev-step btn btn-default" data-gostep="2"><?php echo   $GLOBALS['btn_anterior'];?></a>
       <a class="next-step btn btn-default" data-gostep="4"><?php echo   $GLOBALS['btn_siguiente'] ;?></a>

    </div>
    <div class="step-body" data-step="4">
        <div class="form-group col-xs-12">
           <label for="precio" class="control-label" ><?php echo   $GLOBALS['precio_base_del_tour'] ;?></label>
           <input type="text" class="form-control" name="precio" required>

       </div>
           <input type="hidden" class="form-control" name="maximo" required>



         <a class="prev-step btn btn-default" data-gostep="3"><?php echo   $GLOBALS['btn_anterior'];?></a>
       <a class="next-step btn btn-default" data-gostep="5"><?php echo   $GLOBALS['btn_siguiente'] ;?></a>
    </div>
    <div class="step-body" data-step="5">

        <!-- <div class="form-group col-xs-12">
         <label for="estado">Estado</label>
          <select name="estado" id="" class="form-control">

                  <option value="2">Inactivo</option>
                  <option value="1">Activo</option>


           </select>

       </div> -->

       <div class="form-group col-xs-12">
           <input type="submit" class="btn btn-success" value="Agregar el tour">

       </div>
         <a class="prev-step btn btn-default" data-gostep="4"><?php echo   $GLOBALS['btn_anterior'];?></a>

    </div>

    </form>
</div>


<div class="container-fluid">

<?php
  $this->load->view('commons/footer');
?>
</div>
