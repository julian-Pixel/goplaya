<?php

$GLOBALS['btn_detectar_ubicacion'] = 'Detectar ubicación';
$GLOBALS['nombre'] = 'Nombre';
$GLOBALS['apellidos'] = 'Apellidos';
$GLOBALS['email'] = 'Email';
$GLOBALS['asunto'] = 'Asunto';
$GLOBALS['mensaje'] = 'Mensaje';
$GLOBALS['enviar'] = 'Enviar';
$GLOBALS['votos'] = 'Votos';
$GLOBALS['ver_mas'] = 'Ver más';
$GLOBALS['btn_siguiente']='Siguiente';
$GLOBALS['btn_anterior']='Anterior';
$GLOBALS['filtros']='Escoja el tipo de playa que desea visitar';
$GLOBALS['todos']='Todos';
$GLOBALS['radio']='Radio (en km) que desea recorrer (desde la playa que escogió)';
$GLOBALS['btn_actualizar']='Actualizar';
$GLOBALS['apellidos']='Apellidos';
$GLOBALS['telefono']='Teléfono';
$GLOBALS['ajustes']='AJUSTES DE MI CUENTA';
$GLOBALS['ajustes_generales']='Ajustes generales';
$GLOBALS['caracteristicas']='Características';
$GLOBALS['email']='Email'; 
$GLOBALS['contrasena']='Contraseña'; 
$GLOBALS['ingresar']='Ingresar'; 
$GLOBALS['ingresar_facebook']='Ingresar con  Facebook'; 
$GLOBALS['registrarse']='Registrarse'; 
$GLOBALS['ruta_enviada']="GOPlaya ha enviado su ruta de playas a su correo electrónico";

?>
