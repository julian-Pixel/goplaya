<?php
//HOME
    $GLOBALS['step-intro'] ='<h4>Bienvenido a GO Playa</h4> <p>Para facilitarte la búsqueda, te daremos un rápido recorrido.</p>';
    $GLOBALS['step-1h'] ='Elija el tipo de playa que busca.<br>Ejemplo: Arena blanca. Luego de elegir, toque el botón “Buscar playas” para que salgan los resultados de búsqueda.';
    $GLOBALS['step-2h'] ='Puede buscar de dos maneras: A. Coloque el nombre de la playa y selecciónela, así irá directo a ella. El sistema le sugerirá playas con cada letra que ponga. B. Combine: Coloque el nombre completo de la provincia en la barra de búsqueda y elija el tipo de playa que busca (como en el paso 2). ';
    $GLOBALS['step-3h'] ="Toque el botón “Buscar playas” o presione enter cuando decida combinar. Si busca la playa por su nombre en la barra de búsqueda, solo selecciónela.";
//Buscador
    $GLOBALS['step-1b'] ='<h4>Bienvenido al Buscador de playas</h4><p>Para facilitarte la búsqueda, te daremos un rápido recorrido.</p>';
    $GLOBALS['step-2b'] ='Escoja la provincia que desea visitar. Puede elegir más de una provincia al mismo tiempo.';
    $GLOBALS['step-3b'] ='Escoja el tipo de playa que le gustaría visitar. Ejemplo: Arena blanca.';
    $GLOBALS['step-4b'] ='Elija el radio de búsqueda desde el punto rojo señalado el mapa, o desde el lugar donde se encuentra según el GPS de su celular.';
    $GLOBALS['step-5b'] ='Puede elegir más características en búsqueda avanzada. Es opcional.';
    $GLOBALS['step-6b'] ='Siempre que va realizar una búsqueda, debe darle click a este botón';
//Crea tu ruta
    $GLOBALS['step-1c'] ='<h4>Bienvenido a Crea tu Ruta</h4> <p>Para facilitarte la búsqueda, te daremos un rápido recorrido.</p>';
    $GLOBALS['step-2c'] ='Coloque la playa en donde desea iniciar su ruta. Ejemplo: Tamarindo. Al elegirla, el punto rojo en el mapa se localizará sobre la playa.';
    $GLOBALS['step-3c'] ='Elija las características de las playas que desea visitar en su ruta.';
   
    $GLOBALS['step-4c'] ='Elija los Km que está dispuesto a recorrer desde la playa que escogió. ';
    $GLOBALS['step-5c'] ='Presione “Buscar” para que aparezcan las playas que puede agregar a su ruta. Cada playa trae el botón verde de “Agregar a mi ruta”, presione click en la playa que desee y revise el mapa. ';
 $GLOBALS['step-6c'] ='Importante: Cada vez que toque el botón verde de “Agregar a mi ruta”, acerque el mapa para que vea con claridad la ruta que está creando.';

?>