<?php

    $GLOBALS['titulo_tours'] ='Mi lugar feliz';
    $GLOBALS['beneficios'] ='Beneficios';
  //  $GLOBALS['titulo_tours'] ='Nuestros Tours';
    $GLOBALS['subtitulo_tours'] ='¿Qué tour estás buscando?';
    $GLOBALS['itinerario'] ='Itinerario';
  //  $GLOBALS['subtitulo_tours'] ='¿Qué experiencia estás buscando?';
    $GLOBALS['btn_tipotours'] ='Tipos de tours';
    $GLOBALS['su_numero_id'] ='Este es su código de proveedor:';
    $GLOBALS['ingrese_al_sistema'] ='Ingrese al sistema con su usuario y contraseña';
    $GLOBALS['lbl1'] ='Goplaya, ganador del premio internacional Pasaporte Abierto en la categoría innovación';
    $GLOBALS['lbl2'] ='Afiliado a la Cámara Nacional de Turismo (Canatur)';
    $GLOBALS['lbl3'] ='Miembro de la Organización Mundial de Periodismo Turístico';
    $GLOBALS['gracias'] ='Gracias por Registrarse';

    $GLOBALS['tab1'] ='Datos generales<br /><small>Datos del dueño del tour operador</small>';
    $GLOBALS['tab2'] ='Tour operador<br /><small>Detalles de la compañía tour operador</small>';
    $GLOBALS['tab3'] ='Recepción de pagos <br /><small>Detalles para la transferencia del dinero al tour operador</small>';

    $GLOBALS['marca'] ='Nombre de la empresa (marca)';
    $GLOBALS['razon'] ='Razón social';
    $GLOBALS['website'] ='Dirección del sitio web de la empresa';
    $GLOBALS['direccion_empresa'] ='Dirección de su empresa:';
    $GLOBALS['ciudad'] ='Ciudad:';
    $GLOBALS['provincia'] ='Provincia';
    $GLOBALS['organizacion'] ='¿Está afiliado a una organización turística?';
    $GLOBALS['numero_afiliado'] ='Número de afiliado a dicha cámara:';

    $GLOBALS['propietario'] ='Nombre del propietario de la cuenta bancaria:';
    $GLOBALS['cedula'] ='Cédula del propietario de la cuenta bancaria:';
    $GLOBALS['cuenta'] ='Cuenta bancaria:';
    $GLOBALS['cuenta_normal'] ='Número de Cuenta:';
    $GLOBALS['cuenta_sinpe'] ='Número de SINPE:';

    $GLOBALS['cuenta_iban'] ='Número de IBAN:';
    $GLOBALS['elija_uno'] ='Elija una opción';
    $GLOBALS['actualizar_contrasena'] ='Actualizar Contraseña';


    $GLOBALS["siguiente"]='Siguiente';
    $GLOBALS["anterior"]='Anterior';
    $GLOBALS['lbl6'] ='*Cuenta bancaria debe ser en dólares. <br>
    *GOPlaya, ganador del premio internacional “Pasaporte abierto” en originalidad. <br>
    *GOPlaya está afiliado a CANATUR y CANAECO.';


    $GLOBALS['descripcion_destacados'] = 'Elija su tour de playa favorito y viaje con los expertos en playas';
    $GLOBALS['precio_persona'] = 'Precio por persona';
    $GLOBALS['horario_tour'] = 'Paso 2: Escoja horario';
    $GLOBALS['fecha_tour'] = 'Paso 1: Elija la fecha del tour';
    $GLOBALS['fecha_del_tour'] = 'Fecha del tour';


    // placeholder registro tour operadora

    $GLOBALS['nombre_operador']='Nombre del dueño';

    $GLOBALS['tipo_cedula'] ='Tipo de Cédula';
    $GLOBALS['num_cedula'] ='Número de Cédula';
    $GLOBALS['seleccionar_banco'] = 'Seleccione su banco';


    // tabla tour operador admin
    $GLOBALS['reserva_to']='Últimas reservaciones';
    $GLOBALS['tour_to']='Tour';
    $GLOBALS['hora_tour']='Hora del tour';
    $GLOBALS['fechat_to']='Fecha Tour';
    $GLOBALS['fechar_to']='Fecha Reserva';
    $GLOBALS['cliente_to']='Cliente';
    $GLOBALS['estado_to']='Estado';
    $GLOBALS['acciones_to']='Acciones';
    $GLOBALS['status_to']='Procesada';
    $GLOBALS['status1_to']='Pendiente de pago';
    $GLOBALS['destacar_tour']='¿Es un tour destacado?';
    $GLOBALS['si'] = 'Si';
    $GLOBALS['no'] = 'No';




     $GLOBALS['nombre_del_tour_es']='Nombre del tour en español';
     $GLOBALS['nombre_del_tour_en']='Nombre del tour en inglés';
     $GLOBALS['descripcion_del_tour_es']='Descripción del tour en español';
     $GLOBALS['descripcion_del_tour_en']='Descripción del tour en inglés';
     $GLOBALS['add_tour_es']='Agregar Tour';
     $GLOBALS['add_tour_btn']='Agregar el Tour';
     $GLOBALS['btn_siguiente']='Siguiente';
     $GLOBALS['btn_anterior']='Anterior';
     $GLOBALS['mis_tours']='Mis Tours';
     $GLOBALS['ingrese_fecha_de_su_tour']='Ingrese la fecha de su tour';
     $GLOBALS['su_tour_se_realizar_cerca_de_una_playa']='¿Su tour se realiza en alguna playa en especial?';
     $GLOBALS['precio_base_del_tour']='Precio base del tour en dólares estadounidenses (sin el signo $)';
     $GLOBALS['ver_tour']='Ver Tour';
     $GLOBALS['editar_tour']='Editar tour';
     $GLOBALS['duracion_es']='Duración del tour en español';
     $GLOBALS['duracion_en']='Duración del tour en inglés';
     $GLOBALS['habilitarestrellas']='Habilitar estrellas';
     $GLOBALS['habilitaropinionesclientes']='Habilitar opiniones de clientes';
     $GLOBALS['itinerario_es']='Itinerario del tour en español';
     $GLOBALS['itinerario_en']='Itinerario del tour en Inglés';
     $GLOBALS['detalles_tour_es']='Detalle del tour en español';
     $GLOBALS['detalles_tour_en']='Detalle del tour en Inglés';
     $GLOBALS['cancelacion_en']='Cancelación en Inglés';
     $GLOBALS['cancelacion_es']='Cancelación en español';
     $GLOBALS['caracteristicas_tour']='Características de su tour';
     $GLOBALS['descuento_tour']='Porcentaje de descuento del tour (sin el signo %)';
     $GLOBALS['longitud']='Longitud';
     $GLOBALS['latitud']='Latitud';
     $GLOBALS['Punto_de_encuentro']='Punto de encuentro del tour';
     $GLOBALS['Punto_de_encuentro_direccion']='Dirección del punto de encuentro';
     $GLOBALS['Punto_de_encuentro_direccion_en']='Dirección del punto de encuentro en inglés';
     $GLOBALS['agregar_fotografia_destacada']='AGREGAR FOTOGRAFÍA DESTACADA';
     $GLOBALS['fotografia_destacada']='Imágen Destacada';
     $GLOBALS['galeria']='Galería';
     $GLOBALS['anadir_item']='Agregar Item';
     $GLOBALS['buscar_imagen']='Buscar Imágen';
     $GLOBALS['Tipo_de_item']='Tipo de item';
     $GLOBALS['agregar']='Agregar';
     $GLOBALS['subir_imagen']='Subir Imágen';
     $GLOBALS['agregar_foto_galeria']= 'Agregar Item a la Galería';

     $GLOBALS['Tour disponible ']='Espacios disponibles por tour';
     $GLOBALS['fecha']='Fecha';
     $GLOBALS['hora']='Hora';
     $GLOBALS['disponibilidad']='Espacios';
     $GLOBALS['acciones']='Acciones';
     $GLOBALS['agregar_disponible']='Agregar espacio disponible';

     $GLOBALS['precio_por_tipo_de_persona']='Precios por tipo de turista';
     $GLOBALS['agregar_precio']= 'Agregar Precio';
     $GLOBALS['tipo_cliente']=   'Tipo de cliente';
     $GLOBALS['precio_base']=   'Precio base';
     $GLOBALS['descuento']=   'Descuento';
     $GLOBALS['estado']=   'Estado';

     $GLOBALS['Meta_title_es']=   'Meta Title Español';
     $GLOBALS['Meta_title_en']=   'Meta Title Inglés';
     $GLOBALS['Meta_description_en']=   'Meta description Inglés';
     $GLOBALS['Meta_description_es']=   'Meta description Español';
     $GLOBALS['keywords_es']=   'Keywords Español';
     $GLOBALS['keywords_en']=   'Keywords Inglés';
     $GLOBALS['guardar']=   'Guardar';


      $GLOBALS['he_leido'] = 'He leído';
      $GLOBALS['y acepto los Términos y Condiciones'] = 'y acepto los Términos y Condiciones';
      $GLOBALS['Porcentaje de descuento'] = 'Porcentaje de descuento';
      $GLOBALS['Activo'] = 'Activo';
      $GLOBALS['Inactivo'] = 'Inactivo';

      $GLOBALS['Bienvenido de nuevo'] = 'Bienvenido de nuevo';
      $GLOBALS['donde_vamos'] = '¿Dónde Vamos?';

      $GLOBALS['Top_destinos'] = 'Top destinos';
      $GLOBALS['Ver_todos_los_destino'] = 'Ver todos los destino';
      $GLOBALS['Expertos_en_playa'] = 'Expertos en playa';
      $GLOBALS['Destino_virgen'] = 'Destino virgen';
      $GLOBALS['Sin_sobreprecios'] = 'Sin sobreprecios';
      $GLOBALS['Pago_seguro'] = 'Pago seguro';
      $GLOBALS['Guías_locales'] = 'Guías locales';
      $GLOBALS['Tours_a_la medida'] = 'Tours a la medida';
      $GLOBALS['Tours_destacados'] = 'Tours destacados';


      $GLOBALS['Precio'] = 'Precio';
      $GLOBALS['Adultos'] = 'Adultos';
      $GLOBALS['Menores'] = 'Menores';
      $GLOBALS['Estudiantes'] = 'Estudiantes';
      $GLOBALS['ninos'] = 'Niños';

      $GLOBALS['Cupos'] = 'Cupos Disponibles:';
      $GLOBALS['Detalles_del_tour'] = 'Detalles del tour';
      $GLOBALS['Punto_de_Encuentro'] = 'Punto de Encuentro';
      $GLOBALS['Cancelaciones'] = 'Cancelaciones';
      $GLOBALS['Dudas'] = 'Dudas';
      $GLOBALS['Personas'] = 'Personas';

      $GLOBALS['Reservar'] = 'Reservar';

      $GLOBALS['Revisa_tu_perdido'] = 'Revisa tu perdido';
      $GLOBALS['Datos_personales'] = 'Datos personales';
      $GLOBALS['Metodo_de_pago'] = 'Método de pago';
      $GLOBALS['Proceder_con_la_compra'] = 'Proceder con la compra';

        $GLOBALS['Nombre'] = 'Nombre';
        $GLOBALS['Apellidos'] = 'Apellidos';
        $GLOBALS['Email'] = 'Email';
        $GLOBALS['Telefono'] = 'Teléfono';
        $GLOBALS['Continuar'] = 'Continuar';
        $GLOBALS['Anterior'] = 'Anterior';
        $GLOBALS['Elija_un_metodo_de_pago'] = 'Elija un método de pago';
        $GLOBALS['La_fecha_del_tour'] = "La fecha del tour es el ";
        $GLOBALS['a_las'] = "a las";
        $GLOBALS['Opinion'] = "Opinión";

          $GLOBALS['Datos_de_la_reserva'] = 'Datos de la reserva';

          $GLOBALS['fisica'] = "Fisíca";
          $GLOBALS['juridica'] = "Jurídica";

          $GLOBALS['No_estoy_afiliado'] = "No estoy afiliado";
          $GLOBALS['Otra'] = "Otra";
          $GLOBALS['Enviar'] = "Enviar";

          //Dashboard
            $GLOBALS['menu'] = " Menú";
            $GLOBALS['usuarios'] = " Usuarios";


//No hay traduccion de parte de jose
            $GLOBALS['ubicaciones'] = "Ubicaciones";
            $GLOBALS['caracteristicas_playas'] = "Caracteristicas Playas";
            $GLOBALS['entradas'] = "Entradas";
            $GLOBALS['publicidad'] = "Publicidad";
            $GLOBALS['modulos'] = "Módulos";
            $GLOBALS['Featured_Home'] = "Featured Home";
            $GLOBALS['aprobar_tours'] = "Aprobar Tours";
            $GLOBALS['publicidad_tours'] = "Publicidad Tours";
            $GLOBALS['tipos_tours'] = "Tipos de Tours";
            $GLOBALS['dudas_tours'] = "Dudas de Tours";
            $GLOBALS['iconos_tours'] = "Iconos  Tours";
            $GLOBALS['comossubiunr_tour'] = "¿Comó subir un tour?";
            $GLOBALS['opiniones'] = "Opiniones de Clientes";
            $GLOBALS['regiones'] = "Regiones";
            $GLOBALS['agregar_region'] = "Agregar Región";
            $GLOBALS['region'] = "Región";
            $GLOBALS['editar'] = " Editar";
            $GLOBALS['eliminar'] = "Eliminar";

            $GLOBALS['agregar_provincia'] = "Agregar Provincia";
            $GLOBALS['agregar_playa'] = "Agregar Playa";
            $GLOBALS['provincias'] ='Provincias';
            $GLOBALS['playas'] ='Playas';
            $GLOBALS['playa'] ='Playa';
            $GLOBALS['descripcion'] ='Descripción';
            $GLOBALS['sitio_web'] = 'Sitio Web';
            $GLOBALS['propietario_de_cuenta'] = 'Propietario de la cuenta';
            $GLOBALS['cedula_cuenta'] = 'Cédula Cuenta';



            $GLOBALS['admin_tour_operadores'] ='Administración de Tour Operadores';



                          //Aproebar tours

                            $GLOBALS['tours'] ='Tours';
                            $GLOBALS['volver_al_dashboard'] ='Volver al Dashboard';
                            $GLOBALS['habilitado'] ='Habilitado';
                            $GLOBALS['rechazar'] ='Rechazar';
                            $GLOBALS['aprobar'] ='Aprobar';
                            $GLOBALS['tour'] ='Tour';
                            $GLOBALS['#reservacion'] ='#Reservacion';
                            $GLOBALS['Reservacion'] ='Reservación';
                            $GLOBALS['Facturado a'] ='Facturado a:';
                            $GLOBALS['operadora'] ='Operadora';
                            $GLOBALS['monto']  ='Monto';
                            $GLOBALS['reserva']  ='Reserva';
                            $GLOBALS['Comisiones_pendientes_de_pagar']  ='Comisiones pendientes de pagar';
                            $GLOBALS['Comisiones_ya_pagadas']  ='Comisiones ya pagadas';
                            $GLOBALS['marcanopaga']  ='Marca No Paga';
                            $GLOBALS['marcapaga']  ='Marca Paga';
                            $GLOBALS['tour_operadora']  ='Tour Operadora';
                            $GLOBALS['codigo']  ='Código';
                            $GLOBALS['aprobado']  ='Aprobado';
                            $GLOBALS['pendiente']  ='Pendiente';
                            $GLOBALS['agregar_tour']  ='Agregar Tour';
                            $GLOBALS['agregar_tour_operador'] = "Agregar Tour Operador";
                            $GLOBALS['actualizar_tour_operador'] = "Actualizar Tour Operador";


                            //Publicidad tours_all

                            $GLOBALS['AGREGAR_PUBLICIDAD_A_LOS_TOURS'] ='AGREGAR PUBLICIDAD A LOS TOURS';
                            $GLOBALS['agregar_banner']  ='Agregar Banner';
                            $GLOBALS['banner']  ='Banner';
                            $GLOBALS['posicion']  ='Posicion';
                            $GLOBALS['buscar_img_es']  ='Buscar Imagen en español';
                            $GLOBALS['buscar_img_en']  ='Buscar Imagen en Inglés';
                            $GLOBALS['url_pag_es'] ='URL Página en español';
                            $GLOBALS['url_pag_en']  ='URL Página en inglés';
                            $GLOBALS['noapareceeltour']  ='No aparece el tour';


                            //Tipos de tours_all

                            $GLOBALS['agregar_tipos_tour']  ='Agregar Tipos de Tours';
                            $GLOBALS['detalle']  ='Detalle';
                            $GLOBALS['detalleingles']  ='Detalle Inglés';
                            $GLOBALS['agregar_tipo_de_tour']  ='Agregar Tipo de Tour';
                            $GLOBALS['editar_tipo_de_tour']  ='Editar Tipo de Tour';

                            //Agregar video explicativo dudas de tour

                            $GLOBALS['agregar_video_explicativo']  ='AGREGAR VIDEO EXPLICATIVO';
                            $GLOBALS['video_name']  ='Nombre del video';
                            $GLOBALS['video_name_en'] ='Nombre del video en inglés';
                            $GLOBALS['comentario_en'] ='Comentario en inglés';
                            $GLOBALS['comentario_es'] ='Comentario en español';
                            $GLOBALS['linkvideo_en']  ='Link del video en inglés';
                            $GLOBALS['linkvideo_es']  ='Link del video en español';
                            $GLOBALS['habilitar_video']  ='Habilitar Video';
                            $GLOBALS['name_en']  ='Nombre en Inglés';
                            $GLOBALS['descripcion_en'] ='Descripción en Inglés';
                            $GLOBALS['Si'] ='Si';
                            $GLOBALS['No'] ='No';

                            //Icons tours

                            $GLOBALS['agregar_iconos']  ='Agregar Iconos';
                            $GLOBALS['agregar_icono']  ='Agregar Icono';
                            $GLOBALS['titulo_en_es'] ='Titulo en español';
                            $GLOBALS['titulo_en_en'] ='Titulo en inglés';
                            $GLOBALS['descripcion_es'] ='Descripción en español';
                            $GLOBALS['url_es'] ='URL en español';
                            $GLOBALS['url_en']  ='URL en inglés';
                            $GLOBALS['buscar_icono']  ='Buscar Icono';
                            $GLOBALS['icono']  ='Icono';
                            $GLOBALS['editar_icono']  ='Editar Icono';

                            //video explicativo de como subir el tour
                            $GLOBALS['videoexplicativocomosubirtour']  ='VIDEO EXPLICATIVO DE COMO SUBIR UN TOUR';
                            $GLOBALS['video_es']  ='Video en español';
                            $GLOBALS['video_en']  ='Video en Inglés';
                            $GLOBALS['accion']='Acción';

                            $GLOBALS['msjerror']='Porfavor complete este campo!';




?>
