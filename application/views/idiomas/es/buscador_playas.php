<?php

//Home
$GLOBALS['titulo_bp'] = '<span>#</span>CompartimosCostaRica';
$GLOBALS['subtitulo_bp'] = 'Suba su foto aquí con el hashtag #CompartimosCostaRica';
$GLOBALS['btn_bp'] = 'Buscador de playas';

//Page

$GLOBALS['opciones'] = '<span>Seleccione</span> el tipo de  playa que le gustaría visitar';
$GLOBALS['opciones_playas'] ='Opciones de playas';
$GLOBALS['definir_radio'] ='Definir radio de la búsqueda';
$GLOBALS['zonas'] = 'Zonas';
$GLOBALS['mostrar_filtros'] = 'Búsqueda avanzada';
$GLOBALS['buscar_playas'] = 'Buscar playas';
$GLOBALS['todas'] = 'Todas';
$GLOBALS['busqueda_avanzada'] = '<span>Búsqueda</span> avanzada de playas';
$GLOBALS['resultados_busqueda'] = '<span>Resultados</span> de búsqueda';

?>