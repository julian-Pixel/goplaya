<?php

$GLOBALS['titulo_ctr'] = '<span>Crea tu</span> Ruta';

//Page

$GLOBALS['punto_partida']='Elija la playa desde donde desea iniciar la ruta';
$GLOBALS['placeholder-ctr']='Elige tú punto de partida';
$GLOBALS['waze']='Ir con waze';
$GLOBALS['gmap']='Ir con google maps';
$GLOBALS['ruta_predisenada']='<span>Rutas</span> recomendadas';
$GLOBALS['lblAdd']='Agregar a mi ruta';
$GLOBALS['lblRemove']='Eliminar de mi ruta';
$GLOBALS['mensaje_ruta']='Envía tu ruta a tu correo o a la cuenta de un amigo';


?>
