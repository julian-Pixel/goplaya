<?php

$GLOBALS['titulo'] = 'Costa Rica';
$GLOBALS['subtitulo'] = '¿Qué tipo de playa te gustaría explorar?';
$GLOBALS['placeholder'] = 'Playa o provincia';
$GLOBALS['btn_filtro'] = '¿QUÉ TIPO DE PLAYA BUSCAS?';
$GLOBALS['btn_buscar'] = '<span>Buscar</span> playas';
$GLOBALS['frase_final'] = 'Más de 200 playas <br> Más de 2000 hoteles';
$GLOBALS['foto_por'] = 'Fotografía:';

?>