<?php
//HOME
    $GLOBALS['step-intro'] ='<h4>Welcome to GO Playa</h4> <p>For an easier search, we´ll give you a quick walkthrough tutorial.</p>';
    $GLOBALS['step-1h'] ='Choose the type of beach you are looking for. Example: white sand. After you make your choice, hit the “Explore beaches” button to get the search results';
    $GLOBALS['step-2h'] ='You can search by two ways: A. Input the beach name and select it, that way you will get its info. Our system will suggest beaches names depending on what you are writing. B. Combine: Input the province name on the search bar and then choose the type of beach you are looking for (as step 2).';
    $GLOBALS['step-3h'] ="Hit the “Explore beaches” button or just hit enter when you have decided what to combine. If you search a beach by its name on the search bar, just select it.";
//Buscador
    $GLOBALS['step-1b'] ='<h4>Welcome to your Beach explorer</h4><p>For an easier search, we´ll give you a quick walkthrough tutorial.</p>';
    $GLOBALS['step-2b'] ='Choose the province you want to visit. You can choose more than one province at a time.';
    $GLOBALS['step-3b'] ='Select what type of beach you want to visit. Example: White sand.';
    $GLOBALS['step-4b'] ='Choose search range from the red localization icon on the map, or your current location (as per your mobile GPS).';
    $GLOBALS['step-5b'] ='More features can be selected on the advanced beach search. This step is optional.';
    $GLOBALS['step-6b'] ='Every time you want to make a search, you must hit this button.';
//Crea tu ruta
    $GLOBALS['step-1c'] ='<h4>Welcome to Create your route</h4> <p>For an easier search, we´ll give you a quick walkthrough tutorial.</p>';
    $GLOBALS['step-2c'] ='Input the beach name where you want to start your route. Example: Tamarindo. When you select it, the red localization icon on the map will point out that beach.';
    $GLOBALS['step-3c'] ='Choose the beach features that you want to have on your route.';
   
    $GLOBALS['step-4c'] ='Choose how many Km you are willing to drive from the beach you selected as starting point. ';
    $GLOBALS['step-5c'] ='Hit the “Search” button and select the beaches you want in your route. A green “Add to route” button will appear on each beach. Select the ones you want to visit and check how your route map looks like.';
 $GLOBALS['step-6c'] ='Important: Every time you hit the green “Add to route” button, zoom in the map so you can see clearly your route map.';

?>