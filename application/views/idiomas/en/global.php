<?php

$GLOBALS['btn_detectar_ubicacion'] = 'Detect my location';
$GLOBALS['nombre'] = 'Name';
$GLOBALS['apellidos'] = 'Last name';
$GLOBALS['email'] = 'Email';
$GLOBALS['asunto'] = 'Subject';
$GLOBALS['mensaje'] = 'Message';
$GLOBALS['enviar'] = 'Submit';
$GLOBALS['votos'] = 'Votes';
$GLOBALS['ver_mas'] = 'View more';
$GLOBALS['btn_siguiente']='Next';
$GLOBALS['btn_anterior']='Previous';
$GLOBALS['filtros']='1.	Select type of beach you want to visit';
$GLOBALS['todos']='All';
$GLOBALS['radio']='Range (in Km) you are willing to drive (from the beach you selected)';
$GLOBALS['btn_actualizar']='Update';
$GLOBALS['apellidos']='Last name';
$GLOBALS['telefono']='Phone';
$GLOBALS['ajustes']='SETTINGS';
$GLOBALS['ajustes_generales']='General settings';
$GLOBALS['seguridad']='Security';
$GLOBALS['caracteristicas']='Features'; 
$GLOBALS['email']='Email'; 
$GLOBALS['contrasena']='Password'; 
$GLOBALS['ingresar']='Log in'; 
$GLOBALS['ingresar_facebook']='Log in with Facebook'; 
$GLOBALS['registrarse']='Sign up'; 
$GLOBALS['ruta_enviada']="GOPlaya has sent out your beach route to your e-mail";
?>
