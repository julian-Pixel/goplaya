  <?php
    $GLOBALS['titulo_tours'] ='My happy place';
    $GLOBALS['beneficios'] ='Benefits';
    //$GLOBALS['titulo_tours'] ='Our Tours';
    $GLOBALS['subtitulo_tours'] ='What tour are you looking for?';
    //$GLOBALS['subtitulo_tours'] ='¿Qué experiencia estás buscando?';
    $GLOBALS['btn_tipotours'] ='Type of tour';
    $GLOBALS['itinerario'] ='Itinerary';
    $GLOBALS['su_numero_id'] ='This is your supplier number:';

    $GLOBALS['descripcion_destacados'] = 'Choose your favorite beach tour and travel with beach experts';
    $GLOBALS['precio_persona'] = 'Price per person';
    $GLOBALS['fecha_tour'] = 'Step 1: Choose tour date';
    $GLOBALS['fecha_del_tour'] = 'Tour date';
    $GLOBALS['horario_tour'] = 'Step 2: Chose schedule';

    $GLOBALS['tab1'] ='General information<br /><small>Tour operator owner</small>';
    $GLOBALS['tab2'] ='Tour Operator<br /><small>Details about tour operator company</small>';
    $GLOBALS['tab3'] ='Payment information <br /><small>Details about payment method.</small>';


    $GLOBALS['lbl6'] ='*Must be dollar account.  <br>
    *GOPlaya, awarded with the “Pasaporte Abierto” prize on originality.  <br>
    *GOPlaya is affiliated to CANATUR and CANAECO.';

    $GLOBALS["siguiente"]='Next';
    $GLOBALS["anterior"]='Previous';
    $GLOBALS['actualizar_contrasena'] ='Update Password';

    // placeholder registro tour operadora

    $GLOBALS['nombre_operador']='Tour Operator Owner´s name';

    $GLOBALS['marca'] ='Tour operator Company name';
    $GLOBALS['razon'] ='Legal name';
    $GLOBALS['website'] ='Website';
    $GLOBALS['direccion_empresa'] ='Company address';
    $GLOBALS['ciudad'] ='City';
    $GLOBALS['provincia'] ='Province';
    $GLOBALS['organizacion'] ='Is the tour operator affiliated to a Costa Rican Tourism Organization?';
    $GLOBALS['numero_afiliado'] ='Organization affiliate number';
    $GLOBALS['tipo_cedula'] ='ID Type';
    $GLOBALS['num_cedula'] ='ID Number';
    $GLOBALS['seleccionar_banco'] = 'Choose your bank';

    $GLOBALS['propietario'] ='Name of the bank account owner:';
    $GLOBALS['cedula'] ='ID of the the bank account owner:';
    $GLOBALS['cuenta'] ='Bank Account:';
    $GLOBALS['cuenta_normal'] ='Bank account number:';
    $GLOBALS['cuenta_sinpe'] ='Bank account SINPE number:';
    $GLOBALS['cuenta_iban'] ='Bank account IBAN number:';


      $GLOBALS['ingrese_al_sistema'] ='Login with your username and password';



    // tabla tour operador admin
    $GLOBALS['reserva_to']='Recent reservations';
    $GLOBALS['tour_to']='Tour';
    $GLOBALS['fechat_to']='Tour date';
    $GLOBALS['fechar_to']='Reservation date';
    $GLOBALS['hora_tour']='Tour hour';
    $GLOBALS['cliente_to']='Client';
    $GLOBALS['estado_to']='Status';
    $GLOBALS['acciones_to']='Actions';
    $GLOBALS['status_to']='Check';
    $GLOBALS['status1_to']='Pending payment';
    $GLOBALS['destacar_tour']='Is it a featured tour?';
    $GLOBALS['si'] = 'Yes';
    $GLOBALS['no'] = 'No';


     $GLOBALS['nombre_del_tour_es']='Tour name in Spanish';
     $GLOBALS['nombre_del_tour_en']='Tour name in English';
     $GLOBALS['descripcion_del_tour_es']='Tour Description in Spanish';
     $GLOBALS['descripcion_del_tour_en']='Tour Description in English';
     $GLOBALS['add_tour_es']='Add Tour';
     $GLOBALS['add_tour_btn']='Add Tour';
     $GLOBALS['btn_siguiente']='Next';
     $GLOBALS['btn_anterior']='Previous';
     $GLOBALS['mis_tours']='My Tours';
     $GLOBALS['ingrese_fecha_de_su_tour']='Enter the tour date';
     $GLOBALS['su_tour_se_realizar_cerca_de_una_playa']='Is your tour near a beach?';
     $GLOBALS['precio_base_del_tour']='Tour base Price in US dollars (without the $ sign)';
     $GLOBALS['ver_tour']='Check tour';
     $GLOBALS['editar_tour']='Edit tour';
     $GLOBALS['duracion_es']='Duration in spanish';
     $GLOBALS['duracion_en']='Duration in english';
     $GLOBALS['habilitarestrellas']='Enable stars';
     $GLOBALS['habilitaropinionesclientes']='Enable client reviews';
     $GLOBALS['itinerario_es']='Tour itinerary in Spanish';
     $GLOBALS['itinerario_en']='Tour itinerary in English';
     $GLOBALS['detalles_tour_es']='Tour Details in Spanish';
     $GLOBALS['detalles_tour_en']='Tour Details in English';
     $GLOBALS['cancelacion_en']='Tour Cancellation in English';
     $GLOBALS['cancelacion_es']='Tour Cancellation in Spanish';
     $GLOBALS['caracteristicas_tour']='Tour features';
     $GLOBALS['descuento_tour']='Tour discount (without the % sign)';
     $GLOBALS['longitud']='Longitude';
     $GLOBALS['latitud']='Latitude';
     $GLOBALS['Punto_de_encuentro']='Departure point';
     $GLOBALS['Punto_de_encuentro_direccion']='Departure point address';
     $GLOBALS['Punto_de_encuentro_direccion_en']='Departure point address in english';
     $GLOBALS['agregar_fotografia_destacada']='ADD COVER PHOTO';
     $GLOBALS['fotografia_destacada']='Cover Photo';
     $GLOBALS['galeria']='Gallery';
     $GLOBALS['anadir_item']='Add Photo';
     $GLOBALS['buscar_imagen']='Search Photo';
     $GLOBALS['Tipo_de_item']='Picture format';
     $GLOBALS['agregar']='Add';
     $GLOBALS['subir_imagen']='Upload Image';
     $GLOBALS['Tour disponible ']='Tour availability';
     $GLOBALS['fecha']='Date';
     $GLOBALS['hora']='Time';
     $GLOBALS['disponibilidad']='Availability';
     $GLOBALS['acciones']='Actions';
     $GLOBALS['agregar_disponible']='Add availability';

     $GLOBALS['precio_por_tipo_de_persona']='Price by tourist type';
     $GLOBALS['agregar_precio']= 'Add Price';
     $GLOBALS['tipo_cliente']=   'Client type';
     $GLOBALS['precio_base']=   'Base Price';
     $GLOBALS['descuento']=   'Discount';
     $GLOBALS['estado']=   'Status';

     $GLOBALS['Meta_title_es']=   'Meta Title Spanish';
     $GLOBALS['Meta_title_en']=   'Meta Title English';
     $GLOBALS['Meta_description_en']=   'Meta description English';
     $GLOBALS['Meta_description_es']=   'Meta description Spanish';
     $GLOBALS['keywords_es']=   'Keywords Spanish';
     $GLOBALS['keywords_en']=   'Keywords English';
     $GLOBALS['guardar']=   'Save';
     $GLOBALS['he_leido'] = 'I have read';
       $GLOBALS['y acepto los Términos y Condiciones'] = 'and accept the terms and conditions';
       $GLOBALS['Porcentaje de descuento'] = 'Discount rate';

       $GLOBALS['Activo'] = 'Enable';
       $GLOBALS['Inactivo'] = 'Disable';
       $GLOBALS['Bienvenido de nuevo'] = 'Welcome';
       $GLOBALS['donde_vamos'] = 'Where are we going?';

       $GLOBALS['Top_destinos'] = 'Top destinations';
       $GLOBALS['Ver_todos_los_destino'] = 'Show all destinations';
       $GLOBALS['Expertos_en_playa'] = 'Beach experts';
       $GLOBALS['Destino_virgen'] = 'Unspoiled destinations';
       $GLOBALS['Sin_sobreprecios'] = 'No surcharges';
       $GLOBALS['Pago_seguro'] = 'Secure payment';
       $GLOBALS['Guías_locales'] = 'Local guides';
       $GLOBALS['Tours_a_la_medida'] = 'Tailored tours';
       $GLOBALS['Tours_destacados'] = 'Must-see tours';

       $GLOBALS['Precio'] = 'Price';
       $GLOBALS['Adultos'] = ' Adults';
       $GLOBALS['Menores'] = ' Underage';
       $GLOBALS['Estudiantes'] = ' Students';
       $GLOBALS['ninos'] = ' Children';

       $GLOBALS['Cupos'] = 'Space Available:';
       $GLOBALS['Detalles_del_tour'] = 'Tour details';
       $GLOBALS['Punto_de_Encuentro'] = 'Meeting point';
       $GLOBALS['Cancelaciones'] = 'Cancelations';
       $GLOBALS['Dudas'] = 'Questions';
       $GLOBALS['Personas'] = 'People';

       $GLOBALS['Reservar'] = 'Reserve';

       $GLOBALS['Revisa_tu_perdido'] = 'Check your order';
       $GLOBALS['Datos_personales'] = 'Personal details';
       $GLOBALS['Metodo_de_pago'] = 'Payment method';
       $GLOBALS['Proceder_con_la_compra'] = 'Secure checkout';

        $GLOBALS['Datos_de_la_reserva'] = 'Contact info';
       $GLOBALS['Nombre'] = 'Name';
       $GLOBALS['Apellidos'] = 'Last name';
       $GLOBALS['Email'] = 'Email';
       $GLOBALS['Telefono'] = 'Phone';
        $GLOBALS['sitio_web'] = 'Web Site';
        $GLOBALS['propietario_de_cuenta'] = 'Owner Account';
        $GLOBALS['cedula_cuenta'] = 'Owner ID';


       $GLOBALS['Continuar'] = 'Continue';
       $GLOBALS['Anterior'] = 'Back';
       $GLOBALS['Elija_un_metodo_de_pago'] = 'Choose a payment method';
       $GLOBALS['La fecha del tour'] = "Tour's Date";
       $GLOBALS['a_las'] = "at";
        $GLOBALS['fisica'] = "Physical person";
        $GLOBALS['juridica'] = "Legal entity";
        $GLOBALS['elija_uno'] = "Choose one";

        $GLOBALS['juridica'] = "Legal entity";
        $GLOBALS['elija_uno'] = "Choose one";
        $GLOBALS['No_estoy_afiliado'] = "Not affiliated";
        $GLOBALS['Otra'] = "Choose one";
        $GLOBALS['Enviar'] = "Submit";
        //dashboard

        $GLOBALS['usuarios'] = " Users";
        $GLOBALS['menu'] = " Menu";


//No hay traduccion de parte de jose
        $GLOBALS['ubicaciones'] = "Locations";
        $GLOBALS['caracteristicas_playas'] = "Beaches Features";
        $GLOBALS['entradas'] = "Entrances";
        $GLOBALS['publicidad'] = "Publicity";
        $GLOBALS['modulos'] = "Modules";
        $GLOBALS['Featured_Home'] = "Featured Home";
        $GLOBALS['aprobar_tours'] = "Approve Tours";
        $GLOBALS['publicidad_tours'] = "Tours Ads";
        $GLOBALS['tipos_tours'] = "Types of tours";
        $GLOBALS['dudas_tours'] = "Doubts about Tours";
        $GLOBALS['iconos_tours'] = "Icons Tour";
        $GLOBALS['comossubiunr_tour'] = "How to upload a tour?";
        $GLOBALS['opiniones'] = "Reviews";

        $GLOBALS['regiones'] = "Regions";
        $GLOBALS['agregar_region'] = "Add Region";
        $GLOBALS['region'] = "Region";
        $GLOBALS['editar'] = " Edit";
        $GLOBALS['eliminar'] = " Delete";
        $GLOBALS['agregar_provincia'] = "Add Province";
        $GLOBALS['agregar_playa'] = "Add beach";
        $GLOBALS['agregar_tour_operador'] = "Add Tour Operator";
        $GLOBALS['actualizar_tour_operador'] = "Update Tour Operator";
        $GLOBALS['Opinion'] = "Review";



        $GLOBALS['provincias'] ='Provinces';
        $GLOBALS['playas'] ='Beaches';
        $GLOBALS['playa'] ='Beach';
        $GLOBALS['descripcion'] ='Description';

        $GLOBALS['admin_tour_operadores'] ='Tour Operators Managament';




        //Aproebar tours

          $GLOBALS['tours'] ='Tours';
          $GLOBALS['volver_al_dashboard'] ='Back To Dashboard';
          $GLOBALS['habilitado'] ='Available';
          $GLOBALS['rechazar'] ='Discard';
          $GLOBALS['aprobar'] ='Approve';
          $GLOBALS['tour'] ='Tour';
          $GLOBALS['#reservacion'] ='#Reservation';
          $GLOBALS['Reservacion'] ='Reservation';
          $GLOBALS['Facturado a'] ='Billed to:';
          $GLOBALS['operadora'] ='Operator';
          $GLOBALS['monto']  ='Amount';
          $GLOBALS['reserva']  ='Reservation';
          $GLOBALS['Comisiones_pendientes_de_pagar']  ='Fees pending payment';
          $GLOBALS['Comisiones_ya_pagadas']  ='Fees already paid';
          $GLOBALS['marcanopaga']  ='Mark do not pay';
          $GLOBALS['marcapaga']  ='Mark do pay';
          $GLOBALS['tour_operadora']  ='Tour Operator';
          $GLOBALS['codigo']  ='Code';
          $GLOBALS['aprobado']  ='Approved';
          $GLOBALS['pendiente']  ='Pending';
          $GLOBALS['agregar_tour']  ='Add Tour';


          //Publicidad tours_all

          $GLOBALS['AGREGAR_PUBLICIDAD_A_LOS_TOURS'] ='Add Ads in Tours';
          $GLOBALS['agregar_banner']  ='Add Banner';
          $GLOBALS['banner']  ='Banner';
          $GLOBALS['posicion']  ='Position';
          $GLOBALS['buscar_img_es']  ='Search Image in Spanish';
          $GLOBALS['buscar_img_en']  ='Search Image in English';
          $GLOBALS['url_pag_es'] ='Spanish Page URL';
          $GLOBALS['url_pag_en']  ='English Page URL';
          $GLOBALS['noapareceeltour']  ='The tour does not appear';

            //Tipos de tours_all

          $GLOBALS['agregar_tipos_tour']  ='Add types of tours';
          $GLOBALS['detalle']  ='Detail';
          $GLOBALS['detalleingles']  ='English Detail';
          $GLOBALS['agregar_tipo_de_tour']  ='Add type of tour';
          $GLOBALS['editar_tipo_de_tour']  ='Edit type of tour';

            //Agregar video explicativo dudas de tour

          $GLOBALS['agregar_video_explicativo']  ='Add Explanatory video';
          $GLOBALS['video_name']  ='Video Name';
          $GLOBALS['video_name_en'] ='Video Name in english';
          $GLOBALS['comentario_en'] ='Comment in english';
          $GLOBALS['comentario_es'] =' Comment in spanish';
          $GLOBALS['linkvideo_en']  ='Link video in english';
          $GLOBALS['linkvideo_es']  ='Link video in spanish';
          $GLOBALS['habilitar_video']  ='Enable Video';
          $GLOBALS['name_en']  ='English Name';
          $GLOBALS['descripcion_en'] ='English Description';
          $GLOBALS['Si'] ='Yes';
          $GLOBALS['No'] ='No';

          //Icons tours

        $GLOBALS['agregar_iconos']  ='Add Icons';
        $GLOBALS['agregar_icono']  ='Add Icon';
        $GLOBALS['titulo_en_es'] ='Spanish Title';
        $GLOBALS['titulo_en_en'] ='English Title';
        $GLOBALS['descripcion_es'] ='Spanish Description';
        $GLOBALS['url_es'] ='Spanish Page URL';
        $GLOBALS['url_en']  ='English Page URL';
        $GLOBALS['buscar_icono']  ='Search icon';
        $GLOBALS['icono']  ='Icon';
        $GLOBALS['editar_icono']  ='Edit Icon';



            //video explicativo de como subir el tour
        $GLOBALS['videoexplicativocomosubirtour']  ='"How to upload a video " video tutorial';
        $GLOBALS['video_es']  ='Spanish Video';
        $GLOBALS['video_en']  ='English Video';
        $GLOBALS['accion']='Action';

        //mensjaes de error


        $GLOBALS['msjerror']='Please complete this field';







































?>
