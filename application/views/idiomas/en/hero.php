<?php

$GLOBALS['titulo'] = 'Costa Rica';
$GLOBALS['subtitulo'] = 'What unspoiled beach do you want to discover?';
$GLOBALS['placeholder'] = 'Beach name, province';
$GLOBALS['btn_filtro'] = 'TYPE OF BEACH';
$GLOBALS['btn_buscar'] = '<span>Explore</span> beaches';
$GLOBALS['frase_final'] = 'More than 200 beaches <br> More than 2000 hotels';
$GLOBALS['foto_por'] = 'Picture:';


?>