

<?php

function url_origin($s, $use_forwarded_host=false) {

  $ssl = ( ! empty($s['HTTPS']) && $s['HTTPS'] == 'on' ) ? true:false;
  $sp = strtolower( $s['SERVER_PROTOCOL'] );
  $protocol = substr( $sp, 0, strpos( $sp, '/'  )) . ( ( $ssl ) ? 's' : '' );

  $port = $s['SERVER_PORT'];
  $port = ( ( ! $ssl && $port == '80' ) || ( $ssl && $port=='443' ) ) ? '' : ':' . $port;

  $host = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
  $host = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;

  return $protocol . '://' . $host;

}

function full_url( $s, $use_forwarded_host=false ) {
  return url_origin( $s, $use_forwarded_host ) . $s['REQUEST_URI'];
}

$absolute_url = full_url( $_SERVER );


$absolute_url = explode("?",$absolute_url);
$absolute_url = $absolute_url[0];



if(!isset($_SESSION['idioma'])){
    if(!isset($_SESSION))
    {
        session_start();
    }
    $_SESSION['idioma']   = 'es';

}else{

if(isset($_GET['idioma'])){

   if($_GET['idioma']!='en' && $_GET['idioma']!='es') {

         $_SESSION['idioma']   = 'en';
   }else{
    $_SESSION['idioma']   = $_GET['idioma'];

   }

}


}

$this->load->view('idiomas/'.$_SESSION['idioma'].'/'.$_SESSION['idioma']);

//var_dump($_SESSION['idioma']);

// FIN IDIOMA

$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="Plan your vacation time in Costa Rica. Explore among +200 beaches and setup your beach tour | Primer buscador de playas de Costa Rica. Más de 200 playas por explorar. Planee su tour de playas en Costa Rica" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="robots" content="NOODP">
<meta name="robots" content="nosnippet">
    <title>GO Playa</title>

 <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!--  Notificaciones-->

<?php
    $this->load->view('notificaciones');


?>




<!--  Fin notificaciones-->






   <div class="modal-bg"></div>
   <div class="modal-overlay">
   <div id="login-modal" class="login-modal">
      <a href="#" class="close-modal"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a>
       <i class="fa fa-lock" aria-hidden="true"></i>
          <form action="<?php echo base_url()?>index.php/dashboard/login" method="post">
          <input type="hidden" value="<?php echo $actual_link; ?>" name="actual_url">
          <input type="email" name="username" placeholder="<?php echo $GLOBALS['email']; ?>" class="form-control">
          <input type="password" name="password" placeholder="<?php echo $GLOBALS['contrasena'];  ?>" class="form-control">
          <input type="submit" class="btn-login" value="<?php echo $GLOBALS['ingresar'];  ?>">
      </form>
        <hr class="separator">
       <a class="btn btn-login-face" id="loginbtn" href="#"><i class="fa fa-facebook"></i>   <?php echo $GLOBALS['ingresar_facebook']; ?></a>
       <hr class="separator">

       <a href="#" class="register-btn" data-toggle="modal" data-target="#myModal"> <?php echo $GLOBALS['registrarse']; ?></a>
   </div>
   </div>

    <!--<div class="loader">
        <img src="<?php echo base_url(); ?>theme/img/loading.gif" alt="">
    </div>-->
    <div class="container-fluid header">

        <div class="logo-container" data-intro="<?php echo $GLOBALS['step-intro']; ?>">
         <a href="<?php echo base_url(); ?>"><img class="img-responsive" src="<?php echo base_url(); ?>theme/img/logo.png" alt=""></a></div>
         <style>
             .active {
    display: block;
    height: 32px;
    line-height: 21px;
}
        </style>
          <nav class="main-menu">
                       <ul>
                       <?php  foreach($menu_home as $menu){?>



                       <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                         <li><a href="<?php echo $menu->enlace; ?>" class="<?php if($absolute_url == $menu->enlace){ echo 'active'; } ?>"   ><?php echo $menu->detalle; ?></a></li>
                        <? }else if( $_SESSION['idioma']   == 'en' ){

                          if(empty($menu->enlace_en)){

                            ?>

                              <li><a href="<?php echo $menu->enlace; ?>"><?php echo $menu->detalle_en; ?></a></li>

                            <?php

                          }else{


                            ?>

                              <li><a href="<?php echo $menu->enlace_en; ?>"><?php echo $menu->detalle_en; ?></a></li>

                            <?php

                          }



                          ?>


                        <?php }?>


                        <?php } ?>




                    </ul>
                    </nav>
                    <div class="login-menu">
                    <ul>
                       <?php if( $_SESSION['idioma']   == 'es' ){ ?>
                        <li><a style="cursor:pointer;" onClick="help()"><i style="color: #adadad;" class="fa fa-question-circle" aria-hidden="true"></i> <span class="hm">Ayuda</span></a></li>
                        <? }else if( $_SESSION['idioma']   == 'en' ){?>
                        <li><a style="cursor:pointer;" onClick="help()"><i style="color: #adadad;" class="fa fa-question-circle" aria-hidden="true"></i> <span class="hm">Help</span></a></li>
                        <?php }?>
                       <?php if(!isset($this->session->userdata['logged_in'])){?>
                        <li><a class="login-action" href="#"><i class="fa fa-user-o" aria-hidden="true"></i> <span class="hm"><?php echo $GLOBALS['ingresar']; ?></span></a></li>
                        <?php }else{?>
                        <li><a href="#" class="btn_perfil"><img src="<?php if(isset($foto_perfil)){ echo $foto_perfil; } ?>" class="img-responsive img-topbar" alt="">
                        <?php echo $GLOBALS['mi_perfil']; ?>

                        </a>

                        </li>
                        <?php }?>
                        <?php


                        $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                         if( $_SESSION['idioma']   == 'es' ){
                             $lang = '?idioma=en';
                             $lan2 = 'EN';
                         }else if( $_SESSION['idioma']   == 'en' ){
                              $lang = '?idioma=es';
                             $lan2 = 'ES';
                         }


                        $nl = explode("?",$actual_link);





                        $nl2 = $nl[0].$lang;


                        ?>



                        <li><a href="<?php echo  $nl2; ?>"><?php echo $lan2; ?> <i class="fa fa-chevron-down" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="mobile-menu">
                                <div id="nav-icon2" class="open-mm">
  <span></span>
  <span></span>
  <span></span>
  <span></span>
  <span></span>
  <span></span>
</div>

                </div>

                <div class="mp_submenu">

                   <div class="hp">
                       <img src="<?php if(isset($foto_perfil)){ echo $foto_perfil; } ?>" class="img-responsive" alt="">
                   </div>
                   <p><span><?php echo $GLOBALS['bienvenido']; ?>,</span> <?php echo $info_user[0]->nombre; ?></p>
                   <p style="background: #31708f;"><i class="fa fa-user-o"></i> <?php echo $rol_user[0]->nombre; ?></p>
                    <ul>
                        <div class="close-sm btn_perfil"><i class="fa fa-times" aria-hidden="true"></i></div>
                        <li><a href="#" data-toggle="modal" data-target="#admcModal" ><i class="fa fa-cog" aria-hidden="true"></i> <?php echo $GLOBALS['configuracion']; ?></a></li>
                       <!-- <li><a href="#"><i class="fa fa-suitcase" aria-hidden="true"></i> Mis próximos viajes</a></li>
                        <li><a href="#"><i class="fa fa-map-signs" aria-hidden="true"></i> Mis rutas</a></li>-->
                        <?php if($admin){?>
                        <li><a href="<?php echo base_url(); ?>index.php/dashboard"><i class="fa fa-user-secret" aria-hidden="true"></i> <?php echo $GLOBALS['administracion']; ?></a></li>
                         <?php }?>
                        <li><a href="<?php echo base_url(); ?>index.php/dashboard/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> <?php echo $GLOBALS['salir']; ?></a></li>
                    </ul>
                </div>
                <!--<div class="col-xs-12 logo-container"><img class="img-responsive" src="<?php echo base_url(); ?>theme/img/logo.png" alt=""></div>
                <div class="col-xs-12">
                    <nav class="main-menu">
                       <ul>
                        <li><a href="#">Inicio</a></li>
                        <li><a href="#">Servicios</a></li>
                        <li><a href="#">Empresa</a></li>
                        <li><a href="#">Contáctenos</a></li>
                    </ul>
                    </nav>


                     <div class="login-menu">
                    <ul>
                        <li><a class="login-action" href="#">Ingresar / Registrarse</a></li>
                        <li><a href="#">ENG</a></li>
                    </ul>
                </div>
                </div>
               -->


    </div>







 <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $GLOBALS['registrarse']; ?></h4>
      </div>
      <div class="modal-body">

        <form action="<?php echo base_url();?>dashboard/registrarse" method="post">

          <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                <input type="text" name="nombre" placeholder="<?php echo $GLOBALS['nombre']; ?>" class="form-control">
            </div>
        </div>

           <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                  <input type="text" name="apellidos" placeholder="<?php echo $GLOBALS['apellidos']; ?> " class="form-control">
            </div>
        </div>
           <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                 <input type="email" name="email" placeholder="<?php echo $GLOBALS['email']; ?>" class="form-control">
            </div>
        </div>

            <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-unlock-alt" aria-hidden="true"></i></div>
                <input type="text" name="pass" placeholder="<?php echo $GLOBALS['contrasena']; ?>" class="form-control">
            </div>
        </div>


            <input type="submit" value="<?php echo $GLOBALS['registrarse']; ?>">

        </form>


      </div>

    </div>

  </div>
</div>






<!-- Modal -->
<div id="admcModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $GLOBALS['ajustes']; ?></h4>
      </div>
      <div class="modal-body clearfix infop-tab">


     <div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#dp" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-address-card" aria-hidden="true"></i> <?php echo $GLOBALS['ajustes_generales']; ?></a></li>
    <li role="presentation"><a href="#cs" role="tab" data-toggle="tab"><i class="fa fa-key" aria-hidden="true"></i> <?php echo $GLOBALS['seguridad']; ?></a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="dp">

        <form action="<?php base_url(); ?>dashboard/update_info" method="post">

            <input type="text" class="form-control" name="nombre" placeholder="<?php echo $GLOBALS['nombre']; ?>" value="<?php echo $info_user[0]->nombre; ?>">
            <input type="text" class="form-control" name="apellidos" placeholder="<?php echo $GLOBALS['apellidos']; ?>" value="<?php echo $info_user[0]->apellidos; ?>">
            <input type="text" class="form-control" name="telefono" placeholder="<?php echo $GLOBALS['telefono']; ?>" value="<?php echo $info_user[0]->telefono; ?>">
            <input type="email" class="form-control" name="email" placeholder="<?php echo $GLOBALS['email']; ?>" value="<?php echo $info_user[0]->email; ?>">
            <input type="submit" value="<?php echo $GLOBALS['btn_actualizar']; ?>">

        </form>



    </div>
    <div role="tabpanel" class="tab-pane" id="cs">

          <form action="<?php base_url(); ?>dashboard/update_pass" method="post">

            <input type="password" class="form-control" name="contrasena" >
            <input type="submit" value="<?php echo $GLOBALS['btn_actualizar']; ?>">

        </form>

    </div>
  </div>

</div>




      </div>

    </div>

  </div>
</div>
