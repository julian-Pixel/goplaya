<?php
  $this->load->view('commons/header');
?>


    <div class="container-fluid container-tours-home" >


          <div class="hero_scene row hero_tours hero_mobile" style="background:url('<?php echo base_url();?>theme/img/CHICA.jpg');background-position: bottom center;">
           <p class="ch"><?php //echo $GLOBALS['foto_por']?> </p>
            <!--<ul class="slides">
               <?php foreach($slides as $slide){?>
             <li>
                <img src="<?php echo base_url();?>archivos/<?php echo $slide->url; ?>" />
              </li>
             <?php } ?>




            </ul>-->
            <div class="over-slidermodulo"></div>
            <div class="formulario-busqueda">
              <div class="txt-center">
                   <h2><?php echo $GLOBALS['titulo_tours']; ?></h2>
                   <p><?php echo $GLOBALS['subtitulo_tours']; ?></p>

              </div>


               <form action="<?php echo base_url(); ?>index.php/tours" method="get">



                 <div class="col-xs-4 sin-padding "  data-step="2" data-intro="<?php echo $GLOBALS['step-1h']; ?>" >

                     <ul class="filtros-list sin-padding ">
                         <li class="filtros-btn">

                             <span class="oculto_mobile"><?php echo $GLOBALS['btn_tipotours']; ?></span>
                             <span class="oculto_desktop"><i class="fa fa-filter" aria-hidden="true"></i></span>
                             <i class="fa fa-chevron-down icono" aria-hidden="true"></i>


                         </li>
                          <ul class="sub-list">
                                <?php foreach($caracteristicas as $c){ ?>

                                 <li class="clearfix">

                                    <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                                    <input type='radio' id="<?php echo $c->detalle; ?>" class="icheck auto" name='filtros[]' value='<?php echo $c->idactividad; ?>'/>

                                    <label for="<?php echo $c->detalle; ?>" class="lbl-bs"> <?php echo $c->detalle; ?></label>
                                   <? }else if( $_SESSION['idioma']   == 'en' ){?>
                                   <input type='radio' class="icheck auto" id="<?php echo $c->idactividad; ?>" name='filtros[]' value='<?php echo $c->idactividad; ?>'/>

                                   <label for="<?php echo $c->idactividad; ?>" class="lbl-bs"> <?php echo $c->detalle_en; ?></label>
                                   <?php }?>



                                 </li>
                                     <?php } ?>
                                     <li class="btnvmf"><a href="<?php echo base_url();?>search_controller/buscar"><?php echo $GLOBALS['ver_mas']; ?></a></li>
                             </ul>


                     </ul>


                 </div>
                 <div class="col-xs-8 sin-padding " data-step="3" data-intro="<?php echo $GLOBALS['step-2h']; ?>" >



                 <div class="input-icon" >
                     <i class="fa fa-search" aria-hidden="true"></i>
                      <input type="text" id="playas-generatortours" name="busqueda[]" placeholder="<?php echo $GLOBALS['donde_vamos']  ?>" class="form-control"></div>
                 </div>



                 <!-- <div class="txt-center" >
                 <button class="btn-search" type="submit" data-step="4" data-intro="<?php echo $GLOBALS['step-3h']; ?>"  > <span>Buscar</span> tours </button>
                 </div> -->
             </form>

             <div class="playas-destacadas wow fadeIn"  id="btours" >
               <div class="txt-center">
                  <h3> <strong><?php echo $GLOBALS['Top_destinos'] ?></strong> </h3>
                  <br>
               </div>

               <div class="row tours">

               </div>

               <div class="txtcenter">
                 <br>
                 <a class="a-button-main" href="<?php echo base_url() ?>/tours"><?php echo $GLOBALS['Ver_todos_los_destino'] ?></a>
               </div>

             </div>


            </div>

          </div>


          <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                        <section>

                          <div class="flexslider">

                              <ul class="slides">
                                  <?php foreach($banners_superior as $banner){?>
                                      <li>
                                        <a href="<?php echo $banner->url; ?>" target="_blank">

                                         <img src="<?php echo base_url()?>archivos/<?php echo $banner->banner; ?>" alt=""> </li>

                                         </a>
                                      <?php }?>
                              </ul>
                          </div>

                        </section>

          <? }else if( $_SESSION['idioma']   == 'en' ){?>

            <section>

              <div class="flexslider">

                  <ul class="slides">
                      <?php foreach($banners_superior as $banner){?>
                          <li>
                            <a href="<?php echo $banner->url_ingles; ?>" target="_blank">

                             <img src="<?php echo base_url()?>/archivos/<?php echo $banner->banner_ingles; ?>" alt=""> </li>

                             </a>
                          <?php }?>
                  </ul>
              </div>

            </section>


          <?php } ?>

          <div class="txt-center bnf">
               <h2> <?php echo $GLOBALS['beneficios']; ?> </h2>
          </div>

            <div class="container iconmobile featured-tours">





              <?php foreach($iconos_tours as $iconos){ ?>


                  <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                <div class="col-xs-4 featured-tem">
                  <a href="<?php echo $iconos->url_esp; ?>">
                    <img class="icon-tour" src="<?php echo base_url();?>archivos/<?php echo $iconos->icono; ?>" alt="">
                    <h5><?php echo $iconos->descripcion_esp; ?></h5>
                    <p class="hidden-sm hidden-xs"><?php echo $iconos->texto_esp; ?></p>
                  </a>

                </div>

                  <? }else if( $_SESSION['idioma']   == 'en' ){?>

                    <div class="col-xs-4 featured-tem">
                      <a href="<?php echo $iconos->url_eng; ?>">
                        <img class="icon-tour" src="<?php echo base_url();?>archivos/<?php echo $iconos->icono; ?>" alt="">
                        <h5><?php echo $iconos->descripcion_eng; ?></h5>
                        <p class="hidden-sm hidden-xs"><?php echo $iconos->texto_eng; ?></p>
                      </a>
                    </div>

                      <?php }?>
                <!-- <div class="col-xs-6 col-md-3">
                    <img class="icon-tour" style="  width: 68px;" src="<?php echo base_url();?>theme/img/i4.png" alt="">
                  <p class="p-tour">Sin sobreprecios ni costos ocultos</p>
                </div>
                <div class="col-xs-6 col-md-3">
                    <img class="icon-tour" style="width: 68px;" src="<?php echo base_url();?>theme/img/i2.png" alt="">
                  <p class="p-tour">Garantía de pago seguro</p>
                </div>

                <div class="col-xs-6 col-md-3">
                    <img class="icon-tour" src="<?php echo base_url();?>theme/img/i3.png" alt="">
                  <p class="p-tour">Agencias locales seleccionadas</p>
                </div> -->

              <?php } ?>

            </div>
<center>
<section class="col-xs-12">

<!-- <h2> <span>Tours</span> disponibles </h2> -->
<h2> <span><?php echo $GLOBALS['Tours_destacados']; ?></span>  </h2>
<!-- <p>En pareja, con tu familia o viajando sólo. En Go Playa encontrarás el tour perfecto!</p> -->
<p style="color:#064b75"><?php echo $GLOBALS['descripcion_destacados']; ?></p>

</section>
</center>

<div class="container-fluid">

  <div class="row">

    <div class="col-xs-12" id="res">



      <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                    <section>

                      <div class="flexslider">

                          <ul class="slides">
                              <?php foreach($banners_medio as $banner){?>
                                  <li>
                                    <a href="<?php echo $banner->url; ?>" target="_blank">

                                     <img src="<?php echo base_url()?>/archivos/<?php echo $banner->banner; ?>" alt=""> </li>

                                     </a>
                                  <?php }?>
                          </ul>
                      </div>

                    </section>

      <? }else if( $_SESSION['idioma']   == 'en' ){?>

        <section>

          <div class="flexslider">

              <ul class="slides">
                  <?php foreach($banners_medio as $banner){?>
                      <li>
                        <a href="<?php echo $banner->url_ingles; ?>" target="_blank">

                         <img src="<?php echo base_url()?>/archivos/<?php echo $banner->banner; ?>" alt=""> </li>

                         </a>
                      <?php }?>
              </ul>
          </div>

        </section>


      <?php } ?>




       <?php foreach($tours as $tour){?>

         <?php if ($tour->habilitado == 1) {?>

        <div class="touritem ctours col-xs-12 col-md-4">


  <a href="<?php echo base_url();?>tours/tour/<?php echo $tour->idtour; ?>">

           <div class="contenedor-tour">

            <div class="imagen">
            <?php if($tour->imagen != ''){
            $imagen = base_url().'archivos/'.$tour->imagen;
            }else{
            $imagen = 'https://i.ytimg.com/vi/-ZEDp5JhhIc/maxresdefault.jpg';

            }?>

            <img src="<?php echo $imagen; ?>" class="img-responsive" alt="">

            </div>


            <div class="info-wrap clearfix">
                <div class="info-left">
                    <h4 class="title"><a class="theme-color-hover" href="<?php echo base_url();?>tours/tour/<?php echo $tour->idtour; ?>">


                      <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                          <?php echo $tour->nombre; ?>

                    <? }else if( $_SESSION['idioma']   == 'en' ){?>

                        <?php echo $tour->nombre_eng; ?>

                    <?php }?>



                  </a></h4>
                    <p class="provincia-p">  <?php echo $tour->provincia; ?></p>
                    <div class="post-meta">
                        <ul>
                            <li class="destinations"> <i class="fa fa-map-marker"> </i> Amsterdam </li>
                        </ul>
                    </div>
                </div>
                <div class="tour-price-vote">
                  <?php

                  foreach ($precios as $precio) {

                      if($precio->tipo_cliente =='1'){
                        $precio_final = ($tour[0]->precio)-($descuento);
                        $descuento2 = $precio_final*$precio->descuento;
                        $precio_final2 = $precio_final-$descuento2;
                        $descuento_adulto = $precio->descuento;

                      }
                  }




                  //  if($tour->porcetaje_descuento>0){
                  if($descuento_adulto>0){
                  ?>
                  <span class="old-price">$ <?php echo $tour->precio; ?></span>
                  <?php } ?>

                  <div class="wc">


                   <span class="price-tour theme-bg">$ <?php

                  if($descuento_adulto>0){
                //  echo ($tour->precio)-(($tour->precio)*($tour->porcetaje_descuento));
                echo $precio_final2;
                  }else{
                    echo ($tour->precio);
                  }


                    ?></span>

  </div>


                </div>
            </div>
  <a href="<?php echo base_url();?>tours/tour/<?php echo $tour->idtour; ?>">
            <div class="overlay">



  <div class="alg-c">

              <div class="col-xs-12">

                <?php
                  if($tour->porcetaje_descuento>0){
                ?>
                <span class="old-price">$ <?php echo $tour->precio; ?></span>
                <?php } ?>
            <span class="precio">
                <?php if($tour->porcetaje_descuento>0){
                  echo '$';
                echo ($tour->precio)-(($tour->precio)*($tour->porcetaje_descuento));
                }else{
                    echo '$';
                  echo ($tour->precio);
                }  ?>

            </span>
              </div>

              <div class="col-xs-12">
                    <br>
                <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                      <h3>  <?php echo $tour->nombre; ?>
              <? }else if( $_SESSION['idioma']   == 'en' ){?> </h3>

                  <h3>   <?php echo $tour->nombre_eng; ?> </h3>
              <?php }?>

              </div>

               <div class="col-xs-12 txt-justify">

                 <!-- <strong>Descripción:</strong> -->

                 <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                        <?php if (strlen($tour->descripcion)> 200) {?>
                          <p ><?php echo substr($tour->descripcion,0,200); ?>...</p>
                        <?php } else { ?>
                          <p ><?php echo $tour->descripcion; ?></p>
                        <?php } ?>

               <? }else if( $_SESSION['idioma']   == 'en' ){?> </h4>



                   <?php if (strlen($tour->descripcion_eng)> 200) {?>
                     <p ><?php echo substr($tour->descripcion_eng,0,200); ?>...</p>
                   <?php } else { ?>
                     <p ><?php $tour->descripcion_eng; ?>...</p>
                   <?php } ?>

               <?php }?>



              </div>

  </div>


            </div>
  </a>
            </div>




  </a>
        </div>

      <?php   } ?>
        <?php } ?>
    </div>

  </div>

</div>






        </div>
    </div>

<?php if( $_SESSION['idioma']   == 'es' ){ ?>

              <section>

                <div class="flexslider">

                    <ul class="slides">
                        <?php foreach($banners_final as $banner){?>
                            <li>
                              <a href="<?php echo $banner->url; ?>" target="_blank">

                               <img src="<?php echo base_url()?>/archivos/<?php echo $banner->banner; ?>" alt=""> </li>

                               </a>
                            <?php }?>
                    </ul>
                </div>

              </section>

<? }else if( $_SESSION['idioma']   == 'en' ){?>

  <section>

    <div class="flexslider">

        <ul class="slides">
            <?php foreach($banners_final as $banner){?>
                <li>
                  <a href="<?php echo $banner->url_ingles; ?>" target="_blank">

                   <img src="<?php echo base_url()?>/archivos/<?php echo $banner->banner; ?>" alt=""> </li>

                   </a>
                <?php }?>
        </ul>
    </div>

  </section>


<?php } ?>

    <div class="container-fluid">
        <?php
  $this->load->view('commons/footer');
?>
    </div>


    <?php


             if(isset($_GET['filtros'])){ ?>
                <script>


                 $( window ).on( "load", function() {
$('html,body').animate({scrollTop: $('#res').offset().top},'slow');
   });


   </script>
<?php
             }elseif(isset($_GET['busqueda'])){

               ?>
               <script>


                $( window ).on( "load", function() {
     $('html,body').animate({scrollTop: $('#res').offset().top},'slow');
  });


  </script>

         <?php
       }else{


       }

    ?>
