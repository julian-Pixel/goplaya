
<form class="" action="<?php echo base_url()?>import/d" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <input type="file" name="file" class="file">
    <div class="input-group col-xs-12">
      <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
      <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
      <span class="input-group-btn">
        <button class="browse btn btn-primary input-lg" type="button"><i class="glyphicon glyphicon-search"></i> Buscar</button>
      </span>
    </div>
  </div>

<!--
  <input type="file" name="file" value=""> -->

  <label>Escribe el nombre de la tabla.</label><br />
    <input type="text" name="table" /><br /><br />

    <label>Escribe, separados por una coma, los campos de tu tabla excepto los autoincrementales, ejemplo id etc.</label><br />
    <input type="text" name="fields" style="width:600px" />

  <input type="submit" name="Importar" >

</form>


<style media="screen">
.file {
visibility: hidden;
position: absolute;
}


</style>
<script
  src="http://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script type="text/javascript">

$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});

</script>
