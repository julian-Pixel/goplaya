
<?php

$this->load->view('commons/header');

?>
<?php
  $this->load->view('3rd/Mobile_Detect');

$detect = new Mobile_Detect;

?>

<style>
  .filtros-btn {
    background: #ffffff !important;
    color: #636363 !important;
    width: 100%;
    display: inline-block;
    height: 47px;
    transition: all 500ms ease;
    font-size: 18px;
    text-transform: uppercase;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-radius: 10px !important;
    border: solid 1px #c5c5c5;
    position: relative;
}

    .filtros-btn .icono {
        position: absolute;
        right: 10px;
        top: 20px;
    }

    .filtros-btn span {
        line-height: 50px;
        padding-left: 10px;
        padding-right: 10px;
    }
</style>
<div class="container-fluid pom1">
  <form action="<?php echo base_url(); ?>index.php/crea_tu_ruta#sa" id="frmfiltros" data-step="1" data-intro="<?php echo $GLOBALS['step-1c']; ?>">



    <input type="hidden" id="lat" value="<?php if(isset($_GET['lat'])){ echo $_GET['lat']; }else{ echo '9.935607'; }?>" name="lat">
    <input type="hidden" id="lng" value="<?php if(isset($_GET['lng'])){ echo $_GET['lng']; }else{ echo '-84.1833856'; }?>" name="lng">


    <div style="display:none">

               <input type="radio" class="sg-replace-icons" id="q1" name="mode" value="DRIVING" checked>

           <label for="q1" class="rad"><i class="fa fa-car" aria-hidden="true"></i></label>

              <input type="radio"  class="sg-replace-icons" name="mode" id="q2" value="WALKING">
          <label for="q2" class="rad"><i class="fa fa-child"></i></label>

          </div>


    <div class="col-xs-12 col-md-4">

        <div class="lbl_tuto">1. <?php echo $GLOBALS['punto_partida']; ?></div>
        <input data-step="2" data-intro="<?php echo $GLOBALS['step-2c']; ?>" type="text" id="playas-generator2" name="busqueda" placeholder="<?php echo $GLOBALS['placeholder-ctr']?>" class="form-control" value="<?php if(isset($_GET['busqueda'])){ echo $_GET['busqueda']; }?>">
        <input type="hidden" class="form-control" name="puntopartida" id="pac-input2" placeholder="<?php echo $GLOBALS['placeholder-ctr']; ?>">

    </div>
    <div class="col-xs-12 col-md-3">
       <div class="lbl_tuto">2. <?php echo $GLOBALS['filtros']; ?></div>


       <ul class="filtros-list sin-padding ">
               <li class="filtros-btn" data-step="3" data-intro="<?php echo $GLOBALS['step-3c']; ?>" >

                   <span><?php echo $GLOBALS['todos']; ?></span>
                  <!-- <span class="oculto_desktop"><i class="fa fa-filter" aria-hidden="true"></i></span>-->
                   <i class="fa fa-chevron-down icono" aria-hidden="true"></i>


               </li>
               <ul class="sub-list" style="margin-top: 28px;">
                      <?php foreach($caracteristicas as $c){ ?>

                       <li class="clearfix">

                          <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                          <input type='checkbox' class="icheck" name='filtros[]' value='<?php echo $c->idactividad; ?>'/> <label> <?php echo $c->detalle; ?></label>
                         <? }else if( $_SESSION['idioma']   == 'en' ){?>
                         <input type='checkbox' class="icheck" name='filtros[]' value='<?php echo $c->idactividad; ?>'/> <label> <?php echo $c->detalle_en; ?></label>
                         <?php }?>



                       </li>
                           <?php } ?>

                   </ul>


           </ul>



    </div>
    <div class="col-xs-12 col-md-4" data-step="4" data-intro="<?php echo $GLOBALS['step-4c']; ?>">
       <div class="lbl_tuto" >3. <?php echo $GLOBALS['radio']; ?></div>
        <input type="text"  class="range" value="" name="range" />

    </div>
    <div class="col-xs-12 col-md-1">
       <button style="    width: 100%;
    min-width: auto;
    background: #064b75;
    color: white;
    float: right;
    margin-top: 26px;"  data-step="5" data-intro="<?php echo $GLOBALS['step-5c']; ?>" type="submit">Buscar</button>
       <!-- <a class="btn-default-gp btn-cuadrado" id="bpc">Buscar</a>-->
    </div>

    </form>

<!--

    <form action="" id="frmfiltros">
<div class="wizard row">
  <input type="hidden" id="lat" value="9.935607" name="lat">
    <input type="hidden" id="lng" value="-84.1833856" name="lng">

   <div class="step active" data-step="1">

       <div class="col-xs-10">
          <div class="col-xs-2">
           <div class="paso">1</div>

           </div>
          <div class="col-xs-2">

               <input type="radio" class="sg-replace-icons" id="q1" name="mode" value="DRIVING" checked>

           <label for="q1" class="rad"><i class="fa fa-car" aria-hidden="true"></i></label>

              <input type="radio"  class="sg-replace-icons" name="mode" id="q2" value="WALKING">
          <label for="q2" class="rad"><i class="fa fa-child"></i></label>

          </div>
          <div class="col-xs-8">


      <input type="text" class="form-control" name="puntopartida" id="pac-input2" placeholder="<?php echo $GLOBALS['placeholder-ctr']; ?>">


<a class="dlo"><i class="fa fa-compass" aria-hidden="true"></i></a>

          </div>









       </div>
    <div class="col-xs-2">

        <a href="#" data-gostep="2" class="btnstep"><?php echo $GLOBALS['btn_siguiente']; ?></a>
    </div>

   </div>

   <div class="step" data-step="2">
       <div class="col-xs-10">
          <div class="col-xs-2">
           <div class="paso">2</div>
          <?php echo $GLOBALS['filtros']; ?>
           </div>
          <div class="col-xs-5">
             <ul class="filtros-list sin-padding ">
               <li class="filtros-btn">

                   <span class="oculto_mobile"><?php echo $GLOBALS['todos']; ?></span>
                   <span class="oculto_desktop"><i class="fa fa-filter" aria-hidden="true"></i></span>
                   <i class="fa fa-chevron-down icono" aria-hidden="true"></i>


               </li>
               <ul class="sub-list">
                      <?php foreach($caracteristicas as $c){ ?>

                       <li class="clearfix">

                          <?php if( $_SESSION['idioma']   == 'es' ){ ?>

                          <input type='checkbox' class="icheck" name='filtros[]' value='<?php echo $c->idactividad; ?>'/> <label> <?php echo $c->detalle; ?></label>
                         <? }else if( $_SESSION['idioma']   == 'en' ){?>
                         <input type='checkbox' class="icheck" name='filtros[]' value='<?php echo $c->idactividad; ?>'/> <label> <?php echo $c->detalle_en; ?></label>
                         <?php }?>



                       </li>
                           <?php } ?>
                           <li class="btnvmf"><a href="<?php echo base_url();?>search_controller/buscar"><?php echo $GLOBALS['ver_mas']; ?></a></li>
                   </ul>


           </ul>


          </div>
          <div class="col-xs-5">

              <div class="col-xs-4"> <label for=""><?php echo $GLOBALS['radio']; ?></label></div>
        <div class="col-xs-8"> <input type="text" class="range" value="" name="range" /></div>



          </div>



       </div>
    <div class="col-xs-2">

      <a href="#" class="btnstep" data-gostep="1"><?php echo $GLOBALS['btn_anterior']; ?></a>
        <a class="btn-default-gp btn-cuadrado" id="bpc"><i class="fa fa-search" aria-hidden="true"></i></a>

    </div>

   </div>





</div>


    </form>

-->


<div class="filters_bar2 row">
    <form action="" id="frmfiltros">
    <input type="hidden" id="lat" value="10.6267399" name="lat">
    <input type="hidden" id="lng" value="-85.44367060000002" name="lng">
     <!--<div class="col-xs-12 col-md-1 p70">



      <input type="radio" class="sg-replace-icons" id="q1" name="mode" value="DRIVING" checked>

   <label for="q1" class="rad"><i class="fa fa-car" aria-hidden="true"></i></label>

      <input type="radio"  class="sg-replace-icons" name="mode" id="q2" value="WALKING">
  <label for="q2" class="rad"><i class="fa fa-child"></i></label>

        <select class="form-control" id="mode">
          <option value="DRIVING" selected>Conduciendo</option>
          <option value="WALKING">Caminando</option>
        </select>

        </div>-->
    <!--<div class="col-xs-12 col-md-4 p70">


      <input type="text" class="form-control" name="puntopartida" id="pac-input2" placeholder="Eljie tú punto de partida">


<a class="dlo"><i class="fa fa-compass" aria-hidden="true"></i></a>


    </div>-->
      <!--<div class="col-xs-12 col-md-2" style="padding:0;padding-top:8px">
          <ul class="filtros-list sin-padding ">
               <li class="filtros-btn">

                   <span class="oculto_mobile">Filtros</span>
                   <span class="oculto_desktop"><i class="fa fa-filter" aria-hidden="true"></i></span>
                   <i class="fa fa-chevron-down icono" aria-hidden="true"></i>


               </li>
                <ul class="sub-list">
                      <?php foreach($caracteristicas as $c){ ?>

                       <li class="clearfix">
                           <input type='checkbox' class="icheck" name='filtros[]' value='<?php echo $c->idactividad; ?>'/> <label> <?php echo $c->detalle; ?></label>


                       </li>
                           <?php } ?>
                   </ul>


           </ul>

      </div>-->

   <!--    <div class="col-xs-12 col-md-4 p70">

        <div class="col-xs-4"> <label for="">Radio en Km</label></div>
        <div class="col-xs-8"> <input type="text" class="range" value="" name="range" /></div>


    </div>-->
      <!-- <div class="col-xs-12 col-md-1 p70">
           <a class="btn-default-gp btn-cuadrado" id="bpc"><i class="fa fa-search" aria-hidden="true"></i></a>

       </div>-->
     </form>
</div>
    </div>

    <div class="container-fluid">
       <div class="row">

         <div id="<?php if($detect->isMobile()){ echo 'sa'; }else{ echo ''; } ?>" class="col-sm-12 col-md-6 col-md-push-6 sin-padding">
             <div id="map" class="mapl" data-step="6" data-intro="<?php echo $GLOBALS['step-6c']; ?>"></div>
             <div class="mapbar">
                 <a href="#"  data-toggle="modal" data-target="#wazeModal" class="btn-default-gp btn-default-gp-v btnf"><?php echo $GLOBALS['waze']; ?></a>
                 <a href="#"  data-toggle="modal" data-target="#gmModal" class="btn-default-gp btn-default-gp-v btnf"><?php echo $GLOBALS['gmap']; ?></a>


                 <?php if(isset($this->session->userdata['logged_in'])){ ?>
                 <a href="#"  data-toggle="modal" data-target="#correoModal" class="btn-default-gp btn-default-gp-v btnf"><?php echo $GLOBALS['email']; ?>
                 </a>

                 <?php }else{ ?>

                 <a href="#"class="btn-default-gp btn-default-gp-v btnf login-action"><?php echo $GLOBALS['email']; ?>
                 </a>

                 <?php } ?>

             </div>
            <!-- <div id="right-panel"></div>
             <div class="wazepanel">


             </div>
              <div class="gmpanel">


             </div>-->
         </div>

        <div class="col-sm-12 col-md-6 col-md-pull-6">

        <select class="form-control" multiple id="waypoints">
         <!-- <option value="9.5821173,-84.6201077">Playa Hemrmosa, Puntarenas</option>
          <option value="9.6519543,-84.6634667">Playa Herradura</option>
          <option value="9.6119253,-84.6308727">Playa Jaco</option>
          <option value="9.6931353,-85.2072357">Playa Manzanillo</option>-->
        </select>
        <ul id="rbctr">



              <?php if(isset($playas)){ ?>

           <?php foreach($playas as $playa){ ?>
<li class="col-xs-12 col-md-6 playa_item"  data-order="<?php if($_GET['busqueda'] == $playa->nombre ){ echo '1'; }else{ echo '0'; } ?>" >


  <?php if($_GET['busqueda'] != $playa->nombre ){ ?>
  <a href="#sa"  onclick="alerta('<?php echo $playa->latitud; ?>','<?php echo $playa->longitud; ?>','<?php echo $playa->nombre; ?>')" data-pos="<?php echo $playa->latitud; ?>,<?php echo $playa->longitud; ?>" data-labeladd="<?php echo $GLOBALS['lblAdd']; ?>" data-labelremove="<?php echo $GLOBALS['lblRemove']; ?>" class="add_route"><?php echo $GLOBALS['lblAdd']; ?></a>

  <?php } ?>
   <div class="img-ctr">
       <img src="<?php echo base_url()?>archivos/<?php echo $playa->imagen_destacada; ?>" class="img-responsive" alt="">
   </div>
    <div class="gl-feat-item-details"><a href="#" class="view-ip" data-id="<?php echo $playa->idplaya; ?>" data-toggle="modal" data-target="#infoplayaModal"><h3><span style="font-size: 20px;line-height: 40px;"><i class="fa fa-eye" aria-hidden="true"></i></span> <?php echo $playa->nombre; ?></h3></a></div>


</li>


<?php } ?>

              <?php } ?>






        </ul>

        </div>


</div>
</div>




<div class="anuncio-patrocinado clearfix">
   <div class="col-xs-12 col-md-4">
       <img class="img-responsive" src="https://goplaya.cr/theme/img/logo.png" alt="">
   </div>
   <div class="col-xs-12 col-md-8">
       <a class="cap" href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
       <p>Recuerde que le espera un largo camino, respete los límites de velocidad, colóquese el cinturón y no tome mientras maneje. No eche a perder sus vacaciones por una imprudencia.</p>
   </div>

</div>

<div class="container-fluid">
    <?php

$this->load->view('commons/footer');

?>
</div>



<!--Modal-->





<div class="modal fade" id="wazeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Waze</h4>
      </div>
      <div class="modal-body wazepanel">

      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="gmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Google maps</h4>
      </div>
      <div class="modal-body gmpanel">

      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="correoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $GLOBALS['email'];?></h4>
      </div>
      <div class="modal-body">
        <form id="enviar_ruta_email">
        <p><?php echo $GLOBALS['mensaje_ruta']; ?></p>
        <input type="hidden" id="text-success" value="<?php echo $GLOBALS['ruta_enviada'];?>">
        <textarea name="rutaemail" id="ruta_email" cols="30" rows="10" style="display:none"></textarea>
        <input type="email" name="email" class="form-control" value="<?php echo $info_user[0]->email; ?>" placeholder="Email">
        <button id="ruta_emai_btn" type="button"><?php echo $GLOBALS['enviar'];?></button>
        </form>

      </div>
    </div>
  </div>
</div>


<!--Fin Modal-->




 <script>

     $(function () {
            $("#playas-generator2").autocomplete({


                 select:function(event, ui){



                     var latituda = parseFloat(ui.item.latitud);
                     var longituda = parseFloat(ui.item.longitud);



 $('#lat').val(latituda);
    $('#lng').val(longituda);

    /*$('#waypoints').empty();
    $('#waypoints').append('<option value="' +latituda+','+longituda+'" selected>'+ui.label+'</option>');  */

                     initMap();

      },


                source: function (request, response) {
                    $.ajax({
                        url: "https://goplaya.cr/index.php/Search_controller/generar_playas/"
                        , type: "POST"
                        , dataType: "json"
                        , data: {
                            term: request.term
                        }
                        , success: function (data) {
                            console.log(data);
                            response($.map(data, function (item) {
                                return {
                                    label: item.nombre
                                    , value: item.nombre
                                    ,latitud: item.latitud
                                    ,longitud: item.longitud
                                };
                            }))
                        }
                    });
                }
            });
        });





function initMap() {

var directionsDisplay = new google.maps.DirectionsRenderer({
    preserveViewport: true
  });
  var directionsService = new google.maps.DirectionsService;


  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 7,
    center: {lat: 9.935607, lng: -84.1833856}
  });


directionsDisplay.setMap(map);
directionsDisplay.setPanel(document.getElementById('right-panel'));

  calculateAndDisplayRoute(directionsService, directionsDisplay);


   $('input[type=radio][name=mode]').change(function() {

       calculateAndDisplayRoute(directionsService, directionsDisplay);
    });



    var input = document.getElementById('pac-input2');
    var searchBox = new google.maps.places.SearchBox(input);


    // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });



    searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }


    places.forEach(function(place) {

        console.log(place.geometry.location);
    $('#lat').val(place.geometry.location.lat);
    $('#lng').val(place.geometry.location.lng);
    });

        calculateAndDisplayRoute(directionsService, directionsDisplay);
  });


}


     function alerta(lat,lng,name){



        if(getCookie('anuncio_crearuta') == '1'){



           }else{

               setCookie('anuncio_crearuta','1',1);
                $('.anuncio-patrocinado').slideToggle();
                setTimeout(function(){
                     $('.anuncio-patrocinado').slideToggle();
                }, 10000);
           }

         console.log(lat);
         console.log(lng);
     var target = $(event.target);
    console.log(target.attr("data-labeladd"));
    console.log(target.attr("data-labelremove"));



         if(target.hasClass('selected_tem')){
             //target.parent().removeClass('selected_tem');
             target.removeClass('selected_tem');

            $('#waypoints option[value="' + lat + ','+lng+'"]').remove();
              //target.parent().html('<i class="fa fa-plus" aria-hidden="true"></i>');
              target.html(target.attr("data-labeladd"));
              target.removeClass('elmr');

         }else{
            //target.parent().addClass('selected_tem');
            target.addClass('selected_tem');

            $('#waypoints').append('<option value="' + lat + ','+lng+'" selected>'+name+'</option>');
             //target.parent().html('<i class="fa fa-minus" aria-hidden="true"></i>');
             target.html(target.attr("data-labelremove"));
              target.addClass('elmr');

         }
       initMap();
}



function rp(puntos,name){

    var puntos2 = puntos.split(';');
  puntos2.forEach(function(item) {


       var target = $(event.target);
    console.log(target);

         if(target.hasClass('selected_tem')){
             target.removeClass('selected_tem');

            $('#waypoints option[value="' +item+'"]').remove();
              target.html('Agregar a mi ruta');

         }else{
            target.addClass('selected_tem');

            $('#waypoints').append('<option value="' + item+'" selected>'+name+'</option>');
             target.parent().html('Eliminar de mi ruta');

         }
       initMap();



});

}


function calculateAndDisplayRoute(directionsService, directionsDisplay) {

     var selectedMode = $('input[name=mode]:checked').val();
  //var selectedMode = document.getElementById('mode').value;
   $('.wazepanel').html('');
   $('.gmpanel').html('');
    var waypts = [];
    var array = [];
  var checkboxArray = document.getElementById('waypoints');
  for (var i = 0; i < checkboxArray.length; i++) {
    if (checkboxArray.options[i].selected) {
      waypts.push({
        location: checkboxArray[i].value,
        stopover: true
      });

        $('.wazepanel').append('<a target="_blank" href="waze://?q='+checkboxArray[i].value+'">'+checkboxArray[i].innerHTML+'</a>');
        $('.gmpanel').append('<a  target="_blank" href="https://maps.google.com/maps?ll='+checkboxArray[i].value+'&q='+checkboxArray[i].value+'&hl=en&t=h&z=18">'+checkboxArray[i].innerHTML+'</a>');




      /*  var txt = $.trim('<a target="_blank" href="waze://?q='+checkboxArray[i].value+'">'+'Waze: '+checkboxArray[i].innerHTML+'</a><br> ');
    var box = $("#ruta_email");
    box.val(box.val() + txt);*/


        var txt = $.trim('<a style="color:white;"  target="_blank" href="https://maps.google.com/maps?ll='+checkboxArray[i].value+'&q='+checkboxArray[i].value+'&hl=en&t=h&z=18">'+'Google maps: '+checkboxArray[i].innerHTML+'</a><br> ');
    var box = $("#ruta_email");
    box.val(box.val() + txt);


    }
  }

    var last_element = waypts[waypts.length - 1];

    console.log(last_element);


    if (last_element == null) {
       var lat = document.getElementById('lat').value;
        var lng =document.getElementById('lng').value;
        console.log('nulo');
}else{

    var ubicacion = last_element.location;

    var res = ubicacion.split(",")
    var lat = res[0];
    var lng = res[1];

}





    directionsService.route({

    origin: {lat : parseFloat(document.getElementById('lat').value), lng: parseFloat(document.getElementById('lng').value)},  // Haight.
    destination: {lat : parseFloat(lat), lng: parseFloat(lng)},  // Ocean Beach.
    waypoints: waypts,
    optimizeWaypoints: true,
    travelMode: google.maps.TravelMode[selectedMode]
  }, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    } else {
      //window.alert('Directions request failed due to ' + status);
    }
  });
}

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtH7lp3f5hbOz_SzGOJ2EBgXx36X7s8CM&libraries=places&callback=initMap"
        async defer></script>
<style>
.ui-widget {
    padding: 0;
    list-style: none;
    width: 31% ;
    z-index: 1;
    overflow: hidden;
    margin-top: 2px !Important;
    padding-top: 0;
    min-height: 115px;
    z-index: 0;
    z-index: 9;
    background: rgba(245, 245, 245, 0.94);
    border-bottom-left-radius: 35px;
    border-bottom-right-radius: 35px;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    z-index: 9999;
}

</style>





<!-- Modal -->
<div class="modal fade" id="infoplayaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

      </div>
      <div class="modal-body playam-detalle clearfix">

      </div>

    </div>
  </div>
</div>


<script>

    $('.view-ip').click(function () {
            var num = $(this).attr("data-id");

            $.ajax({
                url: '<? echo base_url();?>playa/info_basica', //This is the current doc
                type: "POST"
                 , data: { id: num}
                , success: function (data) {
                    $('.playam-detalle').html(data);
                }
            });
        });
</script>


<style>
    .anuncio-patrocinado{
        z-index: 999999999;
    }

</style>

<script>
    $('.logo-container').data('intro',20); //setter
</script>
