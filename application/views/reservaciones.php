 <?php
  $this->load->view('commons/header');
?>




<div class="container-fluid" style="background: #f5f5f5;">
<?php if(!$have_operadora){?>
<div class="row to_setupc">
   <div class="col-xs-12">
       <img class="img-responsive" style="display: inline-block;" src="<?php echo base_url(); ?>theme/img/tour_setup.gif" alt="">
   </div>
  <div class="col-xs-12 txt-center" style="padding-left: 10%;padding-right: 10%;">
     <h2>No has configurado aún los datos de la  <span>Tour operadora</span></h2>  
     <p style="font-size: 15px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dignissim dui id scelerisque convallis. Duis efficitur, velit nec viverra pellentesque, mi nisl placerat magna, quis feugiat tortor odio ut elit. </p>
      <a href="#" class="btn-default-gp setup-btn">Configurar tour operadora</a>  
    </div>
</div>

<?php }else{?>


<div class="row to-cintillo">
    
    <div class="col-xs-8">
      <div class="profile">
          <img src="<?php if(isset($foto_perfil)){ echo $foto_perfil; } ?>" class="img-responsive" alt="">
           <h2>Bienvenido de nuevo, <span><?php echo $datos_operadora[0]->nombre; ?></span> </h2>
         
      </div>
      
        
    </div>   
    
    <div class="col-xs-2 codigo_comercio">
          <p>Código de comercio: <span style="font-size: 21px;color: rgba(245, 245, 245, 0.67);"><?php echo $datos_operadora[0]->codigo; ?></span></p>
    </div>
    
    
</div>

<div class="container" id="panel_reservaciones">
    
<div class="row">
			<div class="page-header">
				<h3></h3>
				<div class="form-inline">
					<div class="btn-group">
						<button class="btn btn-primary" data-calendar-nav="prev"><< Anterior</button>
						<button class="btn" data-calendar-nav="today">Hoy</button>
						<button class="btn btn-primary" data-calendar-nav="next">Siguiente >></button>
					</div>
					<div class="btn-group">
						<button class="btn btn-warning" data-calendar-view="year">Año</button>
						<button class="btn btn-warning active" data-calendar-view="month">Mes</button>
						<button class="btn btn-warning" data-calendar-view="week">Semana</button>
						<button class="btn btn-warning" data-calendar-view="day">Día</button>
					</div>
				</div>
			</div>	
				
		</div>
		<div class="row">
			<div id="calendar"></div>
		</div>
    
</div>




<?php }?>

<?php
  $this->load->view('commons/footer');
?>
</div>

