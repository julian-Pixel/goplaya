<?php
  $this->load->view('commons/header');
?>




<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">
          
           <h2>Editar página</h2>
         
      </div>
      </div>
        
    </div>   
    <div class="row ctool">
         <a target="playa_<?php echo $datos[0]->slug; ?>" href="<?php echo base_url();?>index.php/pagina/detalle/<?php echo $datos[0]->slug; ?>"  style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><i class="fa fa-eye" aria-hidden="true"></i> Vista previa</a>
        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
    </div>
    
</div>
<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">
<div class="col-md-9 col-xs-12 panel-card">
    <form action="<?php echo base_url(); ?>index.php/pagina/editar_pagina" method="post">
   
  
    
    <input type="hidden" name="id" value="<?php echo $datos[0]->id; ?>" required>
    <div class="form-group">
    <label for="nombre">Nombre</label>
    <input type="text" name="nombre" class="form-control" value="<?php echo $datos[0]->nombre; ?>" id="titulo" required>    
    </div>
    
   <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><i class="fa fa-link" aria-hidden="true"></i> <?php echo base_url(); ?>pagina/detalle/</div>
      <input type="text" name="permalink" class="form-control" id="permalink" value="<?php echo $datos[0]->slug; ?>" required>
      
    </div>
  </div>
    <div class="tool-bar">
       <span><i class="fa fa-hand-pointer-o" aria-hidden="true"></i> Editor visual</span>
        <a href="#" id="salvar"><i class="fa fa-floppy-o" aria-hidden="true"></i> Actualizar cambios</a>
    </div>
    
     <!--<div class="form-group">
      <label for="descripcion">Descripción</label>
       <textarea name="descripcion" class="editor" id="" cols="30" rows="10" required></textarea>
   </div>
-->
   <h4>Español</h4>
   <div id="myGrid">
       
   </div>
    
    <textarea name="contenido" id="test" cols="30" rows="10"><?php echo $datos[0]->contenido; ?></textarea>
    
    <h4>Inglés</h4>
    
    
    
    
    <div id="myGrid2">
       
   </div>
    
    <textarea name="contenido_en" id="test2" cols="30" rows="10"><?php echo $datos[0]->contenido_en; ?></textarea>
    
    
    
   <div class="tool-bar" style="margin-top:10px;">
       <span><i class="fa fa-camera-retro" aria-hidden="true"></i> Extracto</span>
    </div>
    <div class="col-xs-12" style="padding:0;">
       <div class="form-group">
           <textarea name="extracto"  style="border-radius: 0;margin-top: 10px;" class="form-control" id="extracto" cols="30" rows="10"><?php echo $datos[0]->extracto; ?></textarea>
       </div>
    </div>
    
    
    
     <div class="tool-bar" style="margin-top:10px;">
       <span><i class="fa fa-camera-retro" aria-hidden="true"></i> Galería</span>
        <a href="#" data-toggle="modal" data-target="#galeriaModal">Agregar item</a>
    </div>

</div>
<div class="col-md-3 col-xs-12 pside">
  <div class="form-group">
  <label for="">Publicar como</label>
  
   <select class="form-control" name="estado" id="">
         
          <option value="1" <?php if($datos[0]->estado== 1){ echo 'selected'; }?>>Borrador</option>
          <option value="2" <?php if($datos[0]->estado== 2){ echo 'selected'; }?> >Publicado</option>
       
      </select>
      <input type="submit" value="Actualizar"  class="btn-default-gp-v">
    </div>
    
    <div class="form-group">
  <label for="">Tipo de entrada</label>
  
   <select class="form-control" name="categoria" id="">
         
          <option value="1" <?php if($datos[0]->categoria== 1){ echo 'selected'; }?> >Artículo</option>
          <option value="2" <?php if($datos[0]->categoria== 2){ echo 'selected'; }?> >Página</option>
       
      </select>
    
    </div>
    
    
    <div class="form-group clearfix">
    <div class="col-xs-12">
        <label for="">Mostrar en home</label>
    </div>
       
       <?php if($datos[0]->categoria== 1){?>
        <div class="col-xs-6 pside2">
       <input type="radio"  name="home" value="1" <?php if($datos[0]->mostrar_home == 1){ echo 'checked'; }?> > <label for="">Si</label> 
        </div>
        <div class="col-xs-6 pside2">
       <input type="radio"  name="home" value="0" <?php if($datos[0]->mostrar_home == 0){ echo 'checked'; }?> > <label for="">No</label>
        </div>
        <?php }?>
    </div>
  <div class="form-group" id="plantilla_select">
    
      <label for="descripcion">Plantilla</label>
      
      <select class="form-control" name="plantilla" id="">
          <?php foreach($plantillas as $plantilla){?>
          <option value="<?php echo $plantilla; ?>"   <?php if($datos[0]->plantilla == $plantilla){ echo 'selected'; }?>    ><?php echo $plantilla; ?></option>
          <?php } ?>
      </select>
      <a href="#" data-toggle="modal" data-target="#bannerModal" >Agregar banner</a>
   </div>
    <?php if($datos[0]->banner !=' '){ ?>
   <div class="form-group">
       <label>Banner</label>
      
       <img src="<?php echo base_url();?>archivos/<?php echo $datos[0]->banner; ?>" class="img-responsive" alt="">
      <a target="_blank" href="<?php echo base_url(); ?>dashboard/preview?b=<?php echo $datos[0]->plantilla; ?>&i=<?php echo base_url();?>archivos/<?php echo $datos[0]->banner; ?>">Vista previa banner</a>
       
   </div>
    <?php }?> 
</div>


</form>
 
</div>

<!-- Modal -->
<div class="modal fade" id="galeriaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar item a la galería</h4>
      </div>
      <div class="modal-body clearfix">
      
      <form action="<?php echo base_url();?>index.php/provincias/agregar_item_galeria_pagina" method="post" enctype="multipart/form-data">
       <input type="hidden" class="id" name="paginaid" id="paginaid" value="<?php echo $datos[0]->id; ?>">
	  	<div class="form-group">
	  	   
	  	   <h4>Tipo de item</h4>
	  	  
	  	    
	  	    <div class="galery-type col-xs-6 gsp">
              <div class="form-group txtcenter">
                  
                   <input type="radio" value="1" class="tipo_item" name="tipo_item" id="imageg" checked />
                  <label for="imageg" ></label>
              </div>
              </div>
              
            
	  	    <div class="galery-type2 col-xs-6 gsp">
              <div class="form-group txtcenter">
                  
                   <input type="radio" value="2" class="tipo_item" name="tipo_item" id="videog" />
                  <label for="videog" ></label>
              </div>
              </div>
               
	  	    
	  	</div>
	  		<div class="col-md-12 galeria-foto">
				<input class="filestyle" type="file"  data-buttonText="Buscar imagen" name="uploadedimages[]" data-icon="false" multiple>
				<br/>
				<div id="vpp"></div>
   
	  		</div>
      <div class="col-md-12 video-yotube">
          <div class="form-group">
              <label for="youtube">URL Youtube</label>
              <input type="text" class="form-control" name="youtube">
             
          </div>
      </div>
 <input type="submit" value="Agregar" class="btn btn-theme  btn-default-gp-v">
     
          
          </form>
     
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="bannerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar banner</h4>
      </div>
      <div class="modal-body clearfix">
      
      <form action="<?php echo base_url();?>pagina/agregar_banner_articulo" method="post" enctype="multipart/form-data">
       <input type="hidden" class="id" name="paginaid" id="paginaid" value="<?php echo $datos[0]->id; ?>">
       <input type="hidden" name="permalink" class="form-control" id="permalink" value="<?php echo $datos[0]->slug; ?>" required>
	  	
	  		<div class="col-md-12">
				<input class="filestyle" type="file"  data-buttonText="Buscar imagen" name="uploadedimages[]" data-icon="false" multiple>
				<br/>
				<div id="vpp"></div>
   
	  		</div>
 <input type="submit" value="Agregar" class="btn btn-theme  btn-default-gp-v">
     
          
          </form>
     
      </div>
    </div>
  </div>
</div>






<div class="container-fluid">



<?php
  $this->load->view('commons/footer');
?>
</div>

