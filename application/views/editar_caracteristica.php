<?php
  $this->load->view('commons/header');
?>

<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">
          
           <h2>Editar caracteristica</h2>
         
      </div>
      </div>
        
    </div>   
    <div class="row ctool">
        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
    </div>
    
</div>

<div class="container-fluid">
  
  
  <div class="panel panel-default" style="margin-top:2rem;">
  <div class="panel-body">
     <form action="<?php echo base_url()?>index.php/dashboard/update_cat" method="post">
      <input type="hidden" name="id" value="<?php echo $caracteristica[0]->idactividad; ?>">
       <div class="form-group">
          <label for="">Detalle español</label>
           <input type="text" class="form-control" name="detalle" value="<?php echo $caracteristica[0]->detalle; ?>">
       </div>
        <div class="form-group">
          <label for="">Detalle Inglés</label>
           <input type="text" class="form-control" name="detalle_en" value="<?php echo $caracteristica[0]->detalle_en; ?>">
       </div>
       
       <div class="form-group">
           <label for="catep">Categoría padre</label>
           <select name="catpadre" id="" class="form-control">
               <?php foreach($cat_padre as $cp){?>
               <option <?php if($cp->id == $caracteristica[0]->actividad_padre){echo 'selected';} ?> value="<?php echo $cp->id; ?>"><?php echo $cp->detalle; ?></option>
               <?php } ?>
           </select>
       </div>
       
        <div class="col-xs-12">
                 <select id="e2_element" name="icono">
                     
                     <option value="">No icon</option>
                     
  
                    
<option value="icon-Area-de-conservacion">icon-Area-de-conservacion</option>
<option value="icon-Arena">icon-Arena</option>
<option value="icon-Ballenas">icon-Ballenas</option>
<option value="icon-Buceo">icon-Buceo</option>
<option value="icon-Estado-del-camino">icon-Estado-del-camino</option>
<option value="icon-Mirador">icon-Mirador</option>
<option value="icon-Oleaje">icon-Oleaje</option>
<option value="icon-ROCAS">icon-ROCAS</option>
<option value="icon-Playa-virgen">icon-Playa-virgen</option>
<option value="icon-Ruta-de-acceso">icon-Ruta-de-acceso</option>
<option value="icon-Snorkeling">icon-Snorkeling</option>
<option value="icon-Surf">icon-Surf</option>
<option value="icon-Tortuga">icon-Tortuga</option>
<option value="icon-Vegetacion">icon-Vegetacion</option>
<option value="icon-Vida-nocturna">icon-Vida-nocturna</option>             
                     
                 </select>
             </div>
       
       <div class="form-group">
                <label for="simple" class="control-label">Simple</label>
                 <select name="simple" class="form-control" id="">
                    
                     <option value="0" <?php if($caracteristica[0]->simple == 0){echo 'selected';} ?>>No</option>
                     <option value="1" <?php if($caracteristica[0]->simple == 1){echo 'selected';} ?>>Si</option>
                    
                 </select>
             </div>
             
             <input type="submit" class="btn btn-default" value="Actualizar">
   </form>
  </div>
</div>
   
  
   
   
</div>


<div class="container-fluid">

<?php
  $this->load->view('commons/footer');
?>
</div>

