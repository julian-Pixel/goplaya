<?php
  $this->load->view('commons/header');
?>

<div class="save_ajax">
   <img src="<?php echo base_url();?>/theme/img/clock.gif" alt="">

</div>

<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">

           <h2><?php echo $GLOBALS['agregar_tipos_tour'] ;?></h2>

      </div>
      </div>

    </div>
    <div class="row ctool">

        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><?php echo $GLOBALS['volver_al_dashboard'];?></a>
    </div>

</div>




<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">


<div class="row">

      <div class="col-xs-12 tool-bar">
      <a data-toggle="modal" class="btn-default-gp" data-target="#addModal" href="#"><?php echo $GLOBALS['agregar_tipos_tour']  ;?></a>
    </div>


    <table class="table table-striped">


        <thead>
            <tr>
                <th><?php echo $GLOBALS['detalle']  ;?></th>
                <th><?php echo $GLOBALS['detalleingles'] ;?></th>
                <th><?php echo $GLOBALS['acciones_to'];?></th>

            </tr>
        </thead>
        <tbody>
            <?php foreach($tipos as $tipo){ ?>
            <tr>
                <td><?php echo $tipo->detalle; ?></td>
                <td><?php echo $tipo->detalle_en; ?></td>
                  <td>
                    <a href="<?php echo base_url(); ?>tours/eliminar_tipos_tours/<?php echo $tipo->idactividad; ?>"><?php echo $GLOBALS['eliminar']  ;?></a>
                    <a class="tipos-modal" data-toggle="modal" data-id="<?php echo $tipo->idactividad; ?>" href="javascript:void(0)"><?php echo $GLOBALS['editar'] ;?></a>
                  </td>

            </tr>
            <?php } ?>
        </tbody>


    </table>



</div>

</div>



<!-- Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $GLOBALS['agregar_tipo_de_tour']  ;?></h4>
      </div>
      <div class="modal-body clearfix">
       <form action="<?php echo base_url(); ?>tours/agregar_tipo_de_tour" method="post" enctype="multipart/form-data">




        <input type="text" name="detalle" class="form-control" placeholder="<?php echo $GLOBALS['detalle']  ;?>">
        <input type="text" name="detalle_en" class="form-control" placeholder="<?php echo $GLOBALS['detalleingles'] ;?>">

 <input type="submit" value="<?php echo $GLOBALS['agregar'];?>" class="btn btn-theme  btn-default-gp-v">


          </form>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->







<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?php echo $GLOBALS['editar_tipo_de_tour'] ;?></h4>
      </div>
      <div class="modal-body clearfix">
       <form action="<?php echo base_url(); ?>tours/editar_tipo_de_tour" method="post" enctype="multipart/form-data">



         <input type="hidden" name="id" id="id" class="form-control" placeholder="Detalle" >
        <input type="text" name="detalle" id="detalle"  class="form-control" placeholder="Detalle">
        <input type="text" name="detalle_en" id="detalle_en" class="form-control" placeholder="Detalle en inglés">

 <input type="submit" value="<?php echo $GLOBALS['editar'] ;?>" class="btn btn-theme  btn-default-gp-v">


          </form>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal hide" id="addBookDialog">
    <div class="modal-body">
        <input type="hidden" name="bookId" id="bookId" value=""/>
    </div>
</div>


<div class="container-fluid">



<?php
  $this->load->view('commons/footer');
?>
</div>
