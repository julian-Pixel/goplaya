<?php
  $this->load->view('commons/header');
?>

<style >

.iframe {
position: absolute;
width: 880px;
height: 400px;
border: 0;
left: 0;
right: 0;
margin: 0 auto;
text-align: center;
top: 50% !Important;
transform: translateY(-49%) !Important;
}
.full-width{
  position: relative;
}
.full-width img{
    width: 100%;
}

.content-hide{
    margin: 0 auto;
    display:none;
    max-width: 900px;
    text-align: left;
    margin-bottom: 1em;

}

.content {
margin: 0 auto;
max-width: 900px;
text-align: left;
margin-top: 2em;

}


.h-m{
  display: block;
}
.s-m{
    display: none;
}

.vermas {
color: white;
background-color: #064B75;
text-align: center;
cursor: pointer;
transition: all 0.3s;
display: table;
margin: 0 auto;
margin-top: 3em;
margin-bottom: 2em;
padding: 6px 25px;
border-radius: 25px;
text-transform: uppercase;
font-size: 12px;
}

.vermas:hover{
    background-color: black;

}

h1, h2 {
font-family: 'ralewayregular';
color: #064B75;
text-align: center;
font-weight: normal;
font-size: 22px;
margin-top: 1.5em;
margin-bottom: 1.5em;
}

h3{
font-family: 'ralewaybold';
color:#064B75;
text-align: left;
font-weight: normal;
font-size: 16px;
}

strong, b{
font-family: 'ralewayblack';
color:#064B75;
}

.azul{
color:#064B75;
}

ul {
margin: 0;
padding: 1em;
}

ul li{

}
.iframe-m{
display:none;
}
@media screen and (max-width:768px){

.iframe {
display:none;
}

.iframe-m {
    display: block;
      width: 200px;
    top: 0 !Important;
    transform: none !important;
    left: 0;
    position: relative;
}

.iframe-m .Miniframe {
    width: 100% !important;

}

.h-m{
  display: none;
}
.s-m{
    display: block;
}


} /*1390px*/
</style>

<div class="container-fluid">



<div class="full-width row">

    <iframe src="https://adobecar.cr/miniframe.aspx?id=GOPLY&logo=1" class="iframe hidden-sm-down">
    </iframe>


  <img class="h-m" src="<?php echo base_url()?>theme/img/banner.jpg">
  <img class="s-m" src="https://adobecar.cr/img/LogoMiniframeFB.jpg">

  <iframe src="https://adobecar.cr/miniframe.aspx?id=GOPLY&logo=0" class="iframe iframe-m">
  </iframe>
  </div>


  <div class="content">

  <?php if( $_SESSION['idioma']   == 'es' ){ ?>


      <p>
  ¡Evite sorpresas desagradables cuando renta un vehículo! Aquí el precio final es en verdad el precio final que vas a pagar. Además, podrás pasear con toda tranquilidad y comodidad por los lugares más increíbles de Costa Rica.
  </p>
      <p>GOPlaya te ayuda en esta travesía con excelentes ofertas que necesitas considerar. Gracias a nuestra alianza con Adobe Rent a Car, ponemos a tu disposición una gran cantidad de beneficios exclusivos que te permitirán ahorrar mucho dinero. </p>
      <p>
        Nuestra prioridad es facilitarte la mejor experiencia, es por esto que hemos logrado los siguientes beneficios:

      </p>


<ol>
  <li>10% descuento desde el primer día de renta</li>
  <li>Dos conductores completamente gratis (cambia de conductor las veces que quieras)</li>
  <li>Depósito $1,000 para todas las categorías en vez de $2,000 </li>
  <li>Depósito $500 para nacionales con tarjeta de crédito o débito</li>
  <li>100% cobertura daños a terceros con la protección básica</li>
  <li>Entrega y devolución totalmente GRATIS en hoteles cercanos </li>
  <li>Totalmente GRATIS la entrega y devolución de las oficinas en San José al aeropuerto y viceversa </li>
  <li>Teléfono celular GRATIS si renta un vehículo por más de 4 días</li>

</ol>



<h3>¿Qué necesitas saber?</h3>
<h4>Opciones de Cobertura</h43>
<p>Adobe Rent a Car te ofrece el plan de coberturas más completo, para una mejor experiencia en Costa Rica:</p>
<br>
<strong>Protección a Terceros (PLI):</strong>

<p>
Seguro obligatorio con una cobertura a terceros limitada:</p>
<p>Daños a la propiedad de terceros: US$20,000. Deducible de 20% o un mínimo de US$250.</p>
<p>Lesión y muerte de terceras personas que no estén dentro del vehículo de alquiler: US$100,000 por accidente. Sin deducible.</p>
<p>No cubre daños al vehículo rentado.</p>

<br>
<strong>Protección del Carro (LDW):</strong>
<p>Libera la responsabilidad financiera en casos de daños al automóvil de renta, producidos por un accidente, colisión o vuelco. También protege las pérdidas financieras causadas por un robo total o parcial del automóvil.</p>
<p>No cubre daños por vandalismo, costo de grúas ni asistencia en carretera.</p>
<p>La protección del carro tiene un deducible de US$1,000.</p>
<p>No cubre daños a terceros.</p>
<br>
<strong>Protección Extendida (SPP):</strong>
<p>Libera de la responsabilidad financiera del deducible de la Protección a terceros (20%) & Protección del carro ($1,000).</p>
<p>Extiende el límite a una cobertura a $4,000,000 por evento.</p>
<p>Cubre vandalismo, vidrios, llantas.</p>
<p>Cubre costo de asistencia en carretera (24/7) y grúa.</p>
<p>Depósito de garantía requerido de US$1,000 con tarjeta de crédito</p>
<p>Ver más detalles aquí: <a href="https://www.adobecar.com/servicios-y-condiciones/#cobertura"> Adobe Rent a Car</a></p>





<? }else if( $_SESSION['idioma']   == 'en' ){?>


  <p>
Avoid annoying surprises when you rent a car! Here final price is truly the final price you are going to pay. Moreover, you will be able to explore the most amazing places in Costa Rica totally safe and comfortable.

</p>
  <p>GOPlaya helps you on this journey with great deals you need to check out. Thanks to our partnership with Adobe Rent a Car, we offer many exclusive benefits that will help you to save a lot of money.
 </p>
  <p>
  Our priority is to facilitate the best experience to you, that is why we have achieved the following great benefits:


  </p>


<ol>
<li>10% off from the very first day of rental</li>
<li>Two FREE drivers (switch driver anytime you want)</li>
<li>$1,000 deposit for all cars instead of $2,000 deposit</li>
<li>100% cover to third parties’ damages just taking the basic cover</li>
<li>FREE drop off & pick up to and from nearby hotels</li>
<li>FREE shuttle pick up & drop off to airport from office in San José or the other way around</li>
<li>FREE cell phone when you rent a car for more than 4 days</li>


</ol>



<h3>What you need to know:</h3>
<h4>Coverage options</h43>
<p>Adobe Rent a Car offers you the most comprehensive insurance plan, for a better experience in Costa Rica:
</p>
<br>
<strong>Liability Protection (PLI):</strong>
<p>Mandatory insurance with a limited coverage of liability.</p>

<p>Damages to third parties´ property: US$20,000. 20% deductible or a minimum of US$250.</p>
<p>Injury or death of third persons not travelling in the rental car: US$100,000 per accident. No deductible.</p>
<p>Does not cover damages to the rental car.</p>


<br>
<strong>Car Protection (LDW):</strong>
<p>Waiver of financial responsibility in case of damages to the rental car, produced by a car accident, collision or roll over. It also protects the financial losses in case of a total or partial theft of the automobile.</p>
<p>Does not cover vandalism damages, cost of tow truck and road assistance.</p>
<p>The car protection has a deductible (excess) of US$1,000.</p>
<p>Does not cover third party damages.</p>

<br>
<strong>Extended Protection (SPP):</strong>
<p>Waiver of the deductible (excess) of the Liability Protection (20%) & Car Protection ($1,000).</p>
<p>Extends the limit of the coverage up to US$4,000,000 per event.</p>
<p>Covers vandalism damages, windows, tires.</p>
<p>Covers cost of road assistance cost (24/7) and tow truck.</p>
<p>Guarantee deposit of US$1,000 will be required with mayor credit card.</p>

<p>See more details here: <a href="https://www.adobecar.com/en/services-and-conditions/#coverage"> Adobe Rent a Car</a></p>





   <?php }?>








<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

   <script>

       jQuery(document).ready(function(){

           jQuery(".vermas").click(function(){
               jQuery('.content-hide').fadeIn();
               jQuery('.vermas').hide();

               jQuery('html,body').animate({
      scrollTop: $(".content-hide").offset().top},
      'slow');

           });



       });

  </script>


</div>

<div class="container-fluid">
<?php
  $this->load->view('commons/footer');
?>


</div>
