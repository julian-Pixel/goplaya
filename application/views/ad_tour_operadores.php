<?php
  $this->load->view('commons/header');
?>
<div class="save_ajax">
   <img src="<?php echo base_url();?>/theme/img/clock.gif" alt="">

</div>


<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">

           <h2><?php echo  $GLOBALS['admin_tour_operadores'];?></h2>

      </div>
      </div>

    </div>
    <div class="row ctool">

        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><?php echo $GLOBALS['volver_al_dashboard'];?></a>
    </div>

</div>

<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">
    <div class="row">

        <div class="col-xs-12 tool-bar">
          <a data-toggle="modal" class="btn-default-gp" data-target="#slideModal" href="#"><?php echo  $GLOBALS['agregar_tour_operador'];?></a>
        </div>


        <table class="table-lg display nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th><?php echo  $GLOBALS['codigo'];?></th>
                    <th><?php echo  $GLOBALS['nombre'];?></th>
                    <th>Cédula</th>
                    <th><?php echo  $GLOBALS['razon'];?></th>
                    <th><?php echo  $GLOBALS['direccion_empresa'];?></th>
                    <th><?php echo  $GLOBALS['Telefono'] ;?> 1</th>
                    <th><?php echo  $GLOBALS['Telefono'];?> 2</th>
                    <th>Fax</th>
                    <th><?php echo  $GLOBALS['email'];?></th>
                    <th><?php echo  $GLOBALS['sitio_web'];?></th>
                    <th><?php echo  $GLOBALS['propietario_de_cuenta'];?></th>
                    <th><?php echo  $GLOBALS['cedula'];?></th>
                    <th><?php echo  $GLOBALS['cuenta'];?></th>
                    <th>SINPE</th>
                    <th><?php echo  $GLOBALS['estado'];?></th>
                      <th><?php echo  $GLOBALS['accion'];?></th>
                </tr>
            </thead>
            <tbody>
              <?php foreach($tour_operadoras as $operador){ ?>
                <tr>
                    <td><?php echo $operador->idtour_operadora; ?></td>
                    <td><?php echo $operador->codigo; ?></td>
                    <td><?php echo $operador->nombre; ?></td>
                    <td><?php echo $operador->cedula; ?></td>
                    <td><?php echo $operador->razon; ?></td>
                    <td><?php echo $operador->direccion; ?></td>
                    <td><?php echo $operador->telefono1; ?></td>
                    <td><?php echo $operador->telefono2; ?></td>
                    <td><?php echo $operador->fax; ?></td>
                    <td><?php echo $operador->email; ?></td>
                    <td><?php echo $operador->sitio_web; ?></td>
                    <td><?php echo $operador->propietario_cuenta; ?></td>
                    <td><?php echo $operador->cedula_cuenta; ?></td>
                    <td><?php echo $operador->cuenta; ?></td>
                    <td><?php echo $operador->sinpe; ?></td>
                    <td><?php
                       if (($operador->estado) == '1' ) {
                         echo "Activo";
                       }
                       else {
                         echo "Inactivo";
                       }
                    ?></td>

                    <td>
                      <a class="to-modal" data-toggle="modal" data-target="#updateModal" data-id="<?php echo $operador->idtour_operadora; ?>" href=""><?php echo  $GLOBALS['editar'];?></a>
                      <a class="pass-modal" data-toggle="modal" data-target="#passModal" data-id_pass="<?php echo $operador->id_usuarioa; ?>" href="">| <?php echo  $GLOBALS['editar'];?> Contraseña</a>

                    </td>
                </tr>
              <?php } ?>

            </tbody>
        </table>

    </div>
</div>


<!-- Modal Añadir -->
<div class="modal fade" id="slideModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo  $GLOBALS['agregar_tour_operador'];?></h4>
      </div>
      <div class="modal-body clearfix">
       <form id="formregistrar" action="<?php echo base_url(); ?>tour_operadora/registrar" method="post" enctype="multipart/form-data">






        <!--------------------------------->
        <!----------- SECCIÓN 1 ----------->
        <!--------------------------------->
        <div id="step-1">
            <div id="form-step-0" role="form" data-toggle="validator">


                <div class="form-group">
                  <input type="text" name="nombre" placeholder="<?php echo $GLOBALS['nombre_operador']; ?>" class="form-control" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">

                  <input id="teltour" type="text" name="telefono" placeholder="<?php echo $GLOBALS['telefono']; ?>" class="form-control" pattern=".{5,10}" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">

                    <input type="email" name="email2" class="form-control" placeholder="<?php echo $GLOBALS['email']; ?>"  required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">

                    <input type="password" name="pass" placeholder="<?php echo $GLOBALS['contrasena']; ?>" class="form-control" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>


        <!--------------------------------->
        <!----------- SECCIÓN 2 ----------->
        <!--------------------------------->
        <div id="step-2">
            <div id="form-step-1" role="form" data-toggle="validator">
                <div class="form-group">
                  <label for="marca"><?php echo $GLOBALS['marca']; ?></label>
                  <input type="text" name="marca" class="form-control" value="" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label for=""><?php echo $GLOBALS['tipo_cedula']; ?> </label>
                  <select id="tipo_cedula" name="tipo_cedula" class="form-control" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                    <option value=""><?php echo  $GLOBALS['elija_uno']; ?></option>
                    <option value="1"><?php echo  $GLOBALS['fisica']; ?></option>
                    <option value="2"><?php echo  $GLOBALS['juridica']; ?></option>
                  </select>
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label for="razon"><?php echo $GLOBALS['num_cedula']; ?></label>
                  <input id="num_cedula" type="text" name="cedula" class="form-control" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label for="razon"><?php echo $GLOBALS['razon']; ?></label>
                  <input type="text" name="razon" class="form-control" value="" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label for="site"><?php echo $GLOBALS['website']; ?></label>
                  <input type="text" name="site" class="form-control" value="" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label for="direccion"><?php echo   $GLOBALS['direccion_empresa']; ?></label>
                  <textarea name="direccion" rows="8" class="form-control" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')"></textarea>
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label for="estado"><?php echo $GLOBALS['provincia']; ?></label>
                  <select tname="estado" id="provincias" class="form-control" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')"></select>
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label for="ciudad"><?php echo $GLOBALS['ciudad']; ?></label>
                  <select type="coudad" name="estado" class="form-control" id="cantones" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')"></select>
                  <div class="help-block with-errors" ></div>
                </div>

                <div class="form-group">
                  <label for=""><?php echo $GLOBALS['organizacion']; ?> </label>
                  <select name="" class="form-control" required>
                    <option value=""><?php echo  $GLOBALS['elija_uno']; ?></option>
                    <option value="">CANATUR</option>
                    <option value="">CATURGUA</option>
                    <option value="">ICT</option>
                    <option value="">CANAECO</option>
                    <option value=""><?php echo $GLOBALS['Otra'] ?></option>
                    <option value=""><?php echo $GLOBALS['No_estoy_afiliado'] ?></option>
                  </select>
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label for=""><?php echo $GLOBALS['numero_afiliado']; ?></label>
                  <input type="text" name="" class="form-control" value="" >
                  <div class="help-block with-errors"></div>
                </div>


            </div>
        </div>

        <!--------------------------------->
        <!----------- SECCIÓN 3 ----------->
        <!--------------------------------->
        <div id="step-3">
            <div id="form-step-2" role="form" data-toggle="validator">
              <select class="form-control" name="banco">
                <option value=""><?php echo  $GLOBALS['seleccionar_banco']; ?></option>
                <option value="Banco de Costa Rica">Banco de Costa Rica</option>
                <option value="Banco Nacional de Costa Rica">Banco Nacional Costa Rica</option>
                <option value="Banco Hipotecario de la Vivienda">Banco Hipotecario de la Vivienda</option>
                <option value="Banco Popular y de Desarrollo Comunal">Banco Popular y de Desarrollo Comunal</option>
                <option value="Banco BAC San José S.A.">Banco BAC San José S.A.</option>
                <option value="Banco BCT S.A.">Banco BCT S.A.</option>
                <option value="Banco Cathay de Costa Rica S.A.">Banco Cathay de Costa Rica S.A.</option>
                <option value="Banco CMB (Costa Rica) S.A.">Banco CMB (Costa Rica) S.A.</option>
                <option value="Banco Davivienda (Costa Rica) S.A.">Banco Davivienda (Costa Rica) S.A.</option>
                <option value="Banco General (Costa Rica) S.A.">Banco General (Costa Rica) S.A.</option>
                <option value="Banco Improsa S.A.">Banco Improsa S.A.</option>
                <option value="Banco Lafise S.A.">Banco Lafise S.A.</option>
                <option value="Banco Promérica de Costa Rica S.A.">Banco Promérica de Costa Rica S.A.</option>
                <option value="Prival Bank (Costa Rica) S.A.">Prival Bank (Costa Rica) S.A.</option>
                <option value="Scotiabank de Costa Rica S.A.">Scotiabank de Costa Rica S.A.</option>
              </select>

              <label for=""><?php echo $GLOBALS['propietario']?></label>
              <input type="text" name="propietario_cuenta" class="form-control" value="">
              <div class="form-group">
                <label for=""><?php echo $GLOBALS['tipo_cedula']; ?> </label>
                <select id="tipo_cedula_cuenta" name="" class="form-control" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                  <option value=""><?php echo  $GLOBALS['elija_uno']; ?></option>
                  <option value="1"><?php echo  $GLOBALS['fisica']; ?></option>
                  <option value="2"><?php echo  $GLOBALS['juridica']; ?></option>
                </select>
              </div>

              <label for=""><?php echo $GLOBALS['cedula']?></label>
              <input type="text" id="cedula_cuenta" name="cedula_cuenta" class="form-control" value="">

              <label for=""><?php echo $GLOBALS['cuenta']?></label>
              <input type="text" id="num_cuenta" name="cuenta" class="form-control" placeholder="<?php echo $GLOBALS['cuenta_normal']?>" value="">

              <input type="text" name="sinpe" placeholder="<?php echo $GLOBALS['cuenta_sinpe']?>"  class="form-control" value="">

              <input type="text" id="num_cuenta_iban" name="num_cuenta_iban" class="form-control" placeholder="<?php echo $GLOBALS['cuenta_iban']?>" value="">



              <input id="ad_btnenviar" type="button" class="btn btn-theme  btn-default-gp-v" value="<?php  echo $GLOBALS['guardar'] ?>">


            </div>
        </div>

        </form>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<!-- Modal Actualizar -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo  $GLOBALS['actualizar_tour_operador'];?></h4>
      </div>
      <div class="modal-body clearfix">
       <form id="formactualizar" action="<?php echo base_url(); ?>tour_operadora/registrar" method="post" enctype="multipart/form-data">


        <!--------------------------------->
        <!----------- SECCIÓN 2 ----------->
        <!--------------------------------->
        <div id="step-2">
            <div id="form-step-1" role="form" data-toggle="validator">
                <div class="form-group">
                  <input type="hidden" name="id_update" id="id_update" class="form-control" >
                  <label for="nombre_update"><?php echo $GLOBALS['marca']; ?></label>
                  <input type="text" name="nombre_update" id="nombre_update" class="form-control" value="" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">

                  <input  type="text" name="telefono1_update"  id="telefono1_update" value="" placeholder="<?php echo $GLOBALS['telefono']; ?> 1" class="form-control" required>
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">

                  <input  type="text" name="telefono2_update" id="telefono2_update" placeholder="<?php echo $GLOBALS['telefono']; ?> 2" class="form-control"  required >
                  <div class="help-block with-errors"></div>
                </div>


                <div class="form-group">
                  <label for="razon"><?php echo $GLOBALS['num_cedula']; ?></label>
                  <input id="cedula_update" type="text" name="cedula_update" class="form-control" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label for="razon"><?php echo $GLOBALS['razon']; ?></label>
                  <input type="text" name="razon_update" id="razon_update" class="form-control" value="" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label for="site"><?php echo $GLOBALS['website']; ?></label>
                  <input type="text" name="sitio_web_update" id="sitio_web_update" class="form-control" value="" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label for="direccion"><?php echo   $GLOBALS['direccion_empresa']; ?></label>
                  <textarea name="direccion_update" id="direccion_update" rows="8" class="form-control" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')"></textarea>
                  <div class="help-block with-errors"></div>
                </div>

                <!-- <div class="form-group">
                  <label for="estado"><?php echo $GLOBALS['provincia']; ?></label>
                  <select tname="estado" id="provincias" class="form-control" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')"></select>
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label for="ciudad"><?php echo $GLOBALS['ciudad']; ?></label>
                  <select type="coudad" name="estado" class="form-control" id="cantones" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')"></select>
                  <div class="help-block with-errors" ></div>
                </div> -->

                <!-- <div class="form-group">
                  <label for=""><?php echo $GLOBALS['organizacion']; ?> </label>
                  <select name="" class="form-control" required>
                    <option value=""><?php echo  $GLOBALS['elija_uno']; ?></option>
                    <option value="">CANATUR</option>
                    <option value="">CATURGUA</option>
                    <option value="">ICT</option>
                    <option value="">CANAECO</option>
                    <option value=""><?php echo $GLOBALS['Otra'] ?></option>
                    <option value=""><?php echo $GLOBALS['No_estoy_afiliado'] ?></option>
                  </select>
                  <div class="help-block with-errors"></div>
                </div> -->

                <!-- <div class="form-group">
                  <label for=""><?php echo $GLOBALS['numero_afiliado']; ?></label>
                  <input type="text" name="" class="form-control" value="" >
                  <div class="help-block with-errors"></div>
                </div> -->


            </div>
        </div>

        <!--------------------------------->
        <!----------- SECCIÓN 3 ----------->
        <!--------------------------------->
        <div id="step-3">
            <div id="form-step-2" role="form" data-toggle="validator">
              <!-- <select class="form-control" name="banco">
                <option value=""><?php echo  $GLOBALS['seleccionar_banco']; ?></option>
                <option value="Banco de Costa Rica">Banco de Costa Rica</option>
                <option value="Banco Nacional de Costa Rica">Banco Nacional Costa Rica</option>
                <option value="Banco Hipotecario de la Vivienda">Banco Hipotecario de la Vivienda</option>
                <option value="Banco Popular y de Desarrollo Comunal">Banco Popular y de Desarrollo Comunal</option>
                <option value="Banco BAC San José S.A.">Banco BAC San José S.A.</option>
                <option value="Banco BCT S.A.">Banco BCT S.A.</option>
                <option value="Banco Cathay de Costa Rica S.A.">Banco Cathay de Costa Rica S.A.</option>
                <option value="Banco CMB (Costa Rica) S.A.">Banco CMB (Costa Rica) S.A.</option>
                <option value="Banco Davivienda (Costa Rica) S.A.">Banco Davivienda (Costa Rica) S.A.</option>
                <option value="Banco General (Costa Rica) S.A.">Banco General (Costa Rica) S.A.</option>
                <option value="Banco Improsa S.A.">Banco Improsa S.A.</option>
                <option value="Banco Lafise S.A.">Banco Lafise S.A.</option>
                <option value="Banco Promérica de Costa Rica S.A.">Banco Promérica de Costa Rica S.A.</option>
                <option value="Prival Bank (Costa Rica) S.A.">Prival Bank (Costa Rica) S.A.</option>
                <option value="Scotiabank de Costa Rica S.A.">Scotiabank de Costa Rica S.A.</option>
              </select> -->

              <label for=""><?php echo $GLOBALS['propietario']?></label>
              <input type="text" name="propietario_cuenta_update" id="propietario_cuenta_update" class="form-control" value="">


              <label for=""><?php echo $GLOBALS['cedula']?></label>
              <input type="text" id="cedula_cuenta_update" name="cedula_cuenta_update" class="form-control" value="">

              <label for=""><?php echo $GLOBALS['cuenta']?></label>
              <input type="text" id="cuenta_update" name="cuenta_update" class="form-control" placeholder="<?php echo $GLOBALS['cuenta_normal']?>" value="">
              <label for="">SINPE</label>

              <input type="text" name="sinpe_update" id="sinpe_update" placeholder="<?php echo $GLOBALS['cuenta_sinpe']?>"  class="form-control" value="">
              <label for="">Estado</label>
              <select id="estado_update"  name="estado_update" class="form-control" required oninvalid="setCustomValidity('<?php echo $GLOBALS['msjerror']; ?>')">
                <option value="2">Inactivo</option>
                <option value="1">Activo</option>

              </select>

              <!-- <input type="text" id="num_cuenta_iban" name="num_cuenta_iban" class="form-control" placeholder="<?php echo $GLOBALS['cuenta_iban']?>" value=""> -->



              <input id="ad_btnupdate" type="button" class="btn btn-theme  btn-default-gp-v" value="<?php  echo $GLOBALS['guardar'] ?>">


            </div>
        </div>

        </form>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- Modal -->
<div id="passModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $GLOBALS['actualizar_contrasena']; ?></h4>
      </div>
      <div class="modal-body clearfix infop-tab">


     <div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">

    <li role="presentation" class="active"><a href="#cs" role="tab" data-toggle="tab"><i class="fa fa-key" aria-hidden="true"></i> <?php echo $GLOBALS['seguridad']; ?></a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">

    <div role="tabpanel" class="tab-pane active" id="cs">

          <form id="formactualizar_pass"  method="post">
            <input type="hidden" name="id_update_pass" id="id_update_pass" class="form-control" >
            <input type="password" class="form-control" name="contrasena" >
            <input id="ad_btnupdate_pass" type="button" class="btn btn-theme  btn-default-gp-v"  value="<?php echo $GLOBALS['btn_actualizar']; ?>">


        </form>

    </div>
  </div>

</div>




      </div>

    </div>

  </div>
</div>



<?php
  $this->load->view('commons/footer');
?>
