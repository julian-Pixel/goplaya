<?php
$this->load->view('commons/header');
?>




<div style="background: #f5f5f5;">

<div class="row to-cintillo">
<div class="col-xs-8">
<div class="profile">

<h2>Editar playa: <span><?php echo $datos_playa[0]->nombre; ?></span> </h2>

</div>
</div>

</div>
<div class="row ctool">

<a href="

<?php echo base_url();?>index.php/playa/detalle/<?php if(empty($datos_playa[0]->slug)){ echo $datos_playa[0]->idplaya; }else{ echo $datos_playa[0]->slug; } ?>

" target="playa_<?php echo $datos_playa[0]->idplaya; ?>" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v"><i class="fa fa-eye" aria-hidden="true"></i> Vista previa</a>
<a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
</div>

</div>
<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">
<div class="col-md-8 col-xs-12 panel-card">
<form action="<?php echo base_url(); ?>index.php/dashboard/actualizar_playa" method="post" novalidate>


<input type="hidden" name="id" value="<?php echo $datos_playa[0]->idplaya; ?>">

<div class="form-group">
<label for="nombre">Nombre</label>
<input type="text" name="nombre" id="titulo" class="form-control" value="<?php echo $datos_playa[0]->nombre; ?>" required>
</div>
<div class="form-group">
<div class="input-group">
<div class="input-group-addon"><i class="fa fa-link" aria-hidden="true"></i><?php echo base_url();?>playa/detalle/</div>
<input type="text" name="permalink" class="form-control" id="permalink" value="<?php echo $datos_playa[0]->slug; ?>" required="">

</div>
</div>

<div class="form-group">
<label for="descripcion">Descripción</label>
<textarea name="descripcion" class="editor" id="" cols="30" rows="10" required><?php echo $datos_playa[0]->descripcion; ?></textarea>
</div>

<div class="form-group">
<label for="descripcion_en">Descripción Inglés</label>
<textarea name="descripcion_en" class="editor2" id="" cols="30" rows="10" required><?php echo $datos_playa[0]->descripcion_en; ?></textarea>
</div>

<div>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
<li role="presentation" class="active"><a href="#latitud" aria-controls="profile" role="tab" data-toggle="tab">Latitud y longitud</a>
</li>

<li role="presentation" ><a href="#provincia" aria-controls="home" role="tab" data-toggle="tab">Provincia y Región</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
<div role="tabpanel" class="tab-pane" id="provincia">

<div class="form-group col-md-6 col-xs-12">
<label for="nombre">Región</label>

<select class="form-control" name="region" id="" required>
<?php foreach($regiones as $region){ ?>
<option value="<?php echo $region->id_region; ?>" <?php if($datos_playa[0]->idregion ==  $region->id_region){echo 'selected';}?>>
<?php echo $region->nombre; ?>
</option>
<?php } ?>
</select>

</div>
<div class="form-group col-md-6 col-xs-12">
<label for="nombre">Provincia</label>

<select class="form-control" name="provincia" id="" required>
<?php foreach($provincias as $provincia){ ?>
<option value="<?php echo $provincia->idprovincia; ?>" <?php if($datos_playa[0]->idprovincia ==  $provincia->idprovincia){echo 'selected';}?>>
<?php echo $provincia->nombre; ?>
</option>
<?php } ?>
</select>

</div>

</div>
<div role="tabpanel" class="tab-pane active clearfix" id="latitud">

<div class="form-group col-md-6 col-xs-12">
<label for="latitud">Latitud</label>
<input type="text" id="latitudfrm" class="form-control numero2" name="latitud" value="<?php echo $datos_playa[0]->latitud; ?>">
</div>
<div class="form-group col-md-6 col-xs-12">
<label for="longitud">Longitud</label>
<input type="text" id="longitud" class="form-control numero2" name="longitud" value="<?php echo $datos_playa[0]->longitud; ?>">
</div>
<div>

<input id="pac-input" class="controls" type="text" placeholder="Search Box">



<div id="map" class="clearfix"></div>


</div>


</div>
<div class="clearfix"> <input type="submit" value="Actualizar"  class="btn-default-gp-v"></div>
</div>

</div>


</form>
</div>
<div class="col-md-4 col-xs-12">
<div class="galery-header clearfix">
<h4>Imagen destacada</h4>

</div>
<div class="img-destacada" data-toggle="modal"  data-target="#fotoModal">
<img   class="img-thumbnail" style="width: 100%;" src="<?php echo base_url();?>archivos/<?php echo $datos_playa[0]->imagen_destacada; ?>" alt="">
<div class="overlay"><i class="fa fa-camera-retro"></i></div>
</div>


<section class="galeria-playa">
<div class="galery-header clearfix">
<h4>Galería</h4>
<a href="#" class="btn-default-gp  btn-default-gp-v" data-toggle="modal"  data-target="#galeriaModal" >Agregar item</a>
</div>

<ul class="sortable4">
<?php foreach($galeria as $item){?>

<li id="item_<?php echo $item->id; ?>">

<?php

if($item->tipo == 2) {

$video_code = explode("=", $item->url);
$icono = 'fa fa-youtube-play';
$url = 'http://img.youtube.com/vi/'.$video_code[1].'/0.jpg';


} else if( $item->tipo == 1 ) {

$icono = 'fa fa-picture-o';
$url = base_url().'archivos/'.$item->url;

}


?>

<img src="<?php echo $url; ?>" alt="">
<div class="overlay">
<a href="#" class="ecf" data-toggle="modal" data-id="<?php echo $item->id; ?>" data-target="#efModal" style="top: 36px;"><i class="fa fa-pencil fonc"></i></a>
<a href="<?php echo base_url()?>index.php/provincias/eliminar_item_galeria?id=<?php echo $item->id; ?>&playa=<?php echo $datos_playa[0]->idplaya; ?>"><i class="fa fa-times fonc"></i></a>
<i class="<?php echo $icono; ?>" aria-hidden="true"></i>
</div>
</li>

<?php } ?>

</ul>
</section>

<section class="galeria-playa">
<div class="galery-header clearfix">
<h4>Banners</h4>
<a href="#" class="btn-default-gp  btn-default-gp-v" data-toggle="modal"  data-target="#adsModal" >Agregar item</a>
</div>

<ul class="sortable4">
<?php foreach($ads as $item){?>

<li id="item_<?php echo $item->id; ?>">


<?php

$icono = 'fa fa-picture-o';
$url = base_url().'archivos/'.$item->url;


?>

<img src="<?php echo $url; ?>" alt="">
<div class="overlay">
<a href="#" class="ecf" data-toggle="modal" data-position="<?php echo $item->posicion; ?>" data-id="<?php echo $item->id; ?>" data-target="#positionModal" style="top: 36px;"><i class="fa fa-pencil fonc"></i></a>
<a href="<?php echo base_url()?>index.php/provincias/eliminar_item_ads?id=<?php echo $item->id; ?>&playa=<?php echo $datos_playa[0]->idplaya; ?>"><i class="fa fa-times fonc"></i></a>
<i class="<?php echo $icono; ?>" aria-hidden="true"></i>

</div>
</li>

<?php } ?>

</ul>
</section>


<section class="caracteristicas">
<div class="galery-header clearfix">
<h4>Características</h4>
<a href="#" class="btn-default-gp  btn-default-gp-v" data-toggle="modal"  data-target="#caracteristicaModal" >Agregar item</a>
</div>
<form action="<?php echo base_url(); ?>">

</form>
<ul>
<?php foreach($actividades as $a){ ?>
<li>
<a href="<?php echo base_url()?>index.php/provincias/eliminar_item_caracteristica?id=<?php echo $a->idcaracteristica_playa; ?>&playa=<?php echo $a->id_playa; ?>"><i class="fa fa-times fonc"></i></a>

<a style="position: relative;
margin-left: 10px;
font-size: 10px;" onclick="descp_carac('<?php echo $a->idactividad; ?>','<?php echo $datos_playa[0]->idplaya; ?>')"><i style="font-size: 16px;" class="fa fa-pencil"></i></a>

<i class="<?php echo $a->icono; ?>"></i> <?php echo $a->detalle; ?>
</li>
<?php } ?>
</ul>
</section>

</div>
<?php //var_dump($datos_playa);?>



</div>
<div class="container-fluid">

<?php
$this->load->view('commons/footer');
?>
</div>

<!-- Modal -->
<div class="modal fade" id="fotoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Agregar fotografía destacada</h4>
</div>
<div class="modal-body clearfix">
<input type="hidden" class="id" id="photoid" value="<?php echo $datos_playa[0]->idplaya; ?>">
<div class="col-md-12 text-center">
<div id="upload-demo" style="width:100%"></div>
</div>

<div class="col-md-12">
<input class="filestyle" type="file" id="upload" data-buttonText="Buscar imagen" data-icon="false">
<br/>
<div id="vpp"></div>
<button class="btn btn-theme upload-result" >Subir imagen</button>
</div>

</div>
</div>
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="galeriaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Agregar item a la galería</h4>
</div>
<div class="modal-body clearfix">

<form action="<?php echo base_url();?>index.php/provincias/agregar_item_galeria" method="post" enctype="multipart/form-data">
<input type="hidden" class="id" name="playaid" id="playaid" value="<?php echo $datos_playa[0]->idplaya; ?>">
<div class="form-group">

<h4>Tipo de item</h4>


<div class="galery-type col-xs-6 gsp">
<div class="form-group txtcenter">

<input type="radio" value="1" class="tipo_item" name="tipo_item" id="imageg" checked />
<label for="imageg" ></label>
</div>
</div>


<div class="galery-type2 col-xs-6 gsp">
<div class="form-group txtcenter">

<input type="radio" value="2" class="tipo_item" name="tipo_item" id="videog" />
<label for="videog" ></label>
</div>
</div>


</div>
<div class="col-md-12 galeria-foto">
<input class="filestyle" type="file"  data-buttonText="Buscar imagen" name="uploadedimages[]" data-icon="false" multiple>
<br/>
<div id="vpp"></div>

</div>
<div class="col-md-12 video-yotube">
<div class="form-group">
<label for="youtube">URL Youtube</label>
<input type="text" class="form-control" name="youtube">

</div>
</div>
<input type="submit" value="Agregar" class="btn btn-theme  btn-default-gp-v">


</form>

</div>
</div>
</div>
</div>



<!-- Modal Ads -->
<div class="modal fade" id="adsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Agregar item a los Banners</h4>
</div>
<div class="modal-body clearfix">

<form action="<?php echo base_url();?>index.php/provincias/agregar_item_ads" method="post" enctype="multipart/form-data">
<input type="hidden" class="id" name="playaid" id="playaid" value="<?php echo $datos_playa[0]->idplaya; ?>">


<div class="form-group">

<h4>Tipo de item</h4>


<div class="galery-type col-xs-12 gsp">
<div class="form-group txtcenter">

<input type="radio" value="1" class="tipo_item" name="tipo_item" id="imageg" checked />
<label for="imageg" ></label>





</div>
</div>




</div>
<div class="col-md-12 galeria-foto">
<input class="filestyle" type="file"  data-buttonText="Buscar imagen" name="uploadedimages[]" data-icon="false" multiple>
<br/>
<div id="vpp"></div>




</div>

<input type="text"  name="url" class="form-control" placeholder="url" value="">
<div class="col-md-12 text-center">
<select  name="posicion">
<option value="1">Banner Horizontal Superior</option>
<option value="2">Banner Horizontal Medio</option>
<option value="3">Banner Horizontal Inferior</option>
<option value="4">Banner Vertical Superior</option>
<option value="5">Banner Vertical Inferior</option>
</select>
</div>

<input type="submit" value="Agregar" class="btn btn-theme  btn-default-gp-v">


</form>

</div>
</div>
</div>
</div>




<!-- Modal -->
<div class="modal fade" id="caracteristicaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Agregar carcaterísticas</h4>
</div>
<div class="modal-body clearfix">

<form action="<?php echo base_url(); ?>index.php/provincias/agregar_item_actividad" method="post">
<input type="hidden" class="id" name="playaid" id="playaid" value="<?php echo $datos_playa[0]->idplaya; ?>">
<div class="form-group">
<!-- <select name="caracteristica" class="dropdown" id="">
<?php foreach($caracteristicas as $c){?>
<option value="<?php echo $c->idactividad; ?>"><?php echo $c->detalle; ?></option>
<?php } ?>
</select>-->


<select name="caracteristica[]" id="" class="form-control" multiple style="height: 400px;">



<?php foreach($filtros_avanzados as $fa => $value){?>


<optgroup label="<?php echo $fa; ?>">






<?php foreach($value[0] as $v => $code){

?>



<option value="<?php echo $code; ?>"><?php echo $value[1][$v]; ?></option>

<?php

}
?>
</optgroup>


<?php } ?>




</select>

</div>
<div class="form-group">
<input type="submit" value="Agregar" class="btn btn-default">
</div>

</form>

</div>
</div>
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="efModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Editar foto</h4>
</div>
<div class="modal-body">
<form action="" id="upcaption">
<div id="#rbctr"></div>
<input type="hidden" name="id" id="elementid">
<input type="text" name="caption" class="form-control" id="fcaption" placeholder="Caption">
<button type="button" id="upcaption2">Actualizar</button>
</form>
</div>

</div>
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="positionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Editar posición</h4>
</div>
<div class="modal-body">
<form action="" id="upcaption3">
<div id="#rbctr"></div>

<input type="hidden" name="id" id="elementid2">

<select id="upselectposition" name="position">
<option value="1">Banner Horizontal Superior</option>
<option value="2">Banner Horizontal Medio</option>
<option value="3">Banner Horizontal Inferior</option>
<option value="4">Banner Vertical Superior</option>
<option value="5">Banner Vertical Inferior</option>
</select>

<button type="button" id="upposition">Actualizar</button>
</form>
</div>

</div>
</div>
</div>





<!--Mapa -->

<script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initAutocomplete() {
var latf = parseFloat($('#latitudfrm').val());
var lnf = parseFloat($('#longitud').val());


var myLatLng = {lat: latf, lng: lnf };
var map = new google.maps.Map(document.getElementById('map'), {

center: myLatLng,
zoom: 13,
mapTypeId: google.maps.MapTypeId.ROADMAP
});

var marker = new google.maps.Marker({
position: myLatLng,
map: map,
draggable:true,
title: 'Hello World!'
});

// Create the search box and link it to the UI element.
var input = document.getElementById('pac-input');
var searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

// Bias the SearchBox results towards current map's viewport.
map.addListener('bounds_changed', function() {
searchBox.setBounds(map.getBounds());
});

var markers = [];
// [START region_getplaces]
// Listen for the event fired when the user selects a prediction and retrieve
// more details for that place.
searchBox.addListener('places_changed', function() {
var places = searchBox.getPlaces();

if (places.length == 0) {
return;
}

// Clear out the old markers.
markers.forEach(function(marker) {
marker.setMap(null);
});
markers = [];

// For each place, get the icon, name and location.
var bounds = new google.maps.LatLngBounds();
places.forEach(function(place) {
var icon = {
url: place.icon,
size: new google.maps.Size(71, 71),
origin: new google.maps.Point(0, 0),
anchor: new google.maps.Point(17, 34),
scaledSize: new google.maps.Size(25, 25)
};

// Create a marker for each place.
markers.push(new google.maps.Marker({
map: map,
icon: icon,
title: place.name,
draggable:true,
position: place.geometry.location
}));

$('#latitudfrm').val(place.geometry.location.lat());
$('#longitud').val(place.geometry.location.lng());

if (place.geometry.viewport) {
// Only geocodes have viewport.
bounds.union(place.geometry.viewport);
} else {
bounds.extend(place.geometry.location);
}
});
map.fitBounds(bounds);



});


google.maps.event.addListener(marker, 'dragend', function()
{


$('#latitudfrm').val(marker.position.lat());
$('#longitud').val(marker.position.lng());
});






}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5742avOaS0L-PGIhGGYe4soqYExhlB-g&libraries=places&callback=initAutocomplete" async defer></script>
<!--Fin mapa -->
<script>




function descp_carac(caracteristica, playa){

var editor3 = new Simditor({
textarea: $('.editor3')
, upload: false
, toolbar: [
'title'
, 'bold'
, 'italic'
, 'underline'
, 'strikethrough'
, 'fontScale'
, 'color'
, 'ol'
, 'ul'
, 'image'
, 'blockquote'
, 'table'
, 'link'
, 'hr'
, 'indent'
, 'outdent'
, 'alignment'
, ]
//optional options
});

var editor4 = new Simditor({
textarea: $('.editor4')
, upload: false
, toolbar: [
'title'
, 'bold'
, 'italic'
, 'underline'
, 'strikethrough'
, 'fontScale'
, 'color'
, 'ol'
, 'ul'
, 'image'
, 'blockquote'
, 'table'
, 'link'
, 'hr'
, 'indent'
, 'outdent'
, 'alignment'
, ]
//optional options
});
$.ajax({
url: '<?php echo base_url(); ?>provincias/descp_carac', //This is the current doc
type: "POST"
, data: ({c:caracteristica,p:playa})
,dataType: 'json'
, success: function (data) {

if(data == '0'){

$('#detalle_carac').val('');
$('#detalle_carac_en').val('');
$('#ecplaya').val(playa);
$('#ecc').val(caracteristica);
$("#btndeletec").hide();
$("#frmdc").attr("data-action","insert");
editor3.setValue($('#detalle_carac').val());
editor4.setValue($('#detalle_carac_en').val());
$('#edicaModal').modal('toggle');
}else{
$('#detalle_carac').val(data[0].detalle);
$('#detalle_carac_en').val(data[0].detalle_en);
$('#ecplaya').val(playa);
$('#ecc').val(caracteristica);
$("#btndeletec").show();
$("#frmdc").attr("data-action","update");


editor3.setValue($('#detalle_carac').val());
editor4.setValue($('#detalle_carac_en').val());


$('#edicaModal').modal('toggle');

}


}
});

}




</script>

<style>

.active {
height: auto !Important;
}

</style>




<!-- Modal -->
<div class="modal fade" id="edicaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">Detalles</h4>
</div>
<div class="modal-body">
<form id="frmdc" action="" data-action="">
<input type="hidden" name="playa" id="ecplaya">
<input type="hidden" name="caracteristica" id="ecc">
<label for="">Español</label>
<textarea name="detalle_carac" class="form-control editor3" id="detalle_carac" cols="30" rows="10"></textarea>
<label for="">Inglés</label>
<textarea name="detalle_carac_en" class="form-control editor4" id="detalle_carac_en" cols="30" rows="10"></textarea>
<button id="btndc" class="btn" type="button">Agregar</button>
<button id="btndeletec" class="btn" type="button">Eliminar detalles</button>
</form>


</div>
</div>
</div>
</div>
