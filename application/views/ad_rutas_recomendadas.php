<?php
  $this->load->view('commons/header');
?>
<div class="save_ajax">
   <img src="<?php echo base_url();?>/theme/img/clock.gif" alt="">
    
</div>



<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">
          
           <h2>Rutas</h2>
         
      </div>
      </div>
        
    </div>   
    <div class="row ctool">
        
        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
        <a href="#" style="color:white;border-color:white;float: right;" data-toggle="modal" data-target="#arModal"  class="btn-default-gp  btn-default-gp-v">Agregar Ruta</a>
    </div>
    
</div>
<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">


<table class="table table-striped">
    
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
       <?php foreach($rutas_predisenadas as $ruta){ ?>
        <tr>
            <td><?php echo $ruta->ruta; ?></td>
            <td><a href="<?php echo base_url();?>dashboard/editar_ruta_recomendada/<?php echo $ruta->id; ?>">Editar</a> 
            <a href="<?php echo base_url();?>dashboard/delete_ruta/<?php echo $ruta->id; ?>">Eliminar</a></td>
        </tr>
        <?php } ?>
    </tbody>
    
</table>

 
</div>

<!-- Modal -->
<div class="modal fade" id="arModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar ruta</h4>
      </div>
      <div class="modal-body">
       <form action="<?php echo base_url()?>dashboard/agregar_ruta_recomendada" method="post">
           
           <div class="form-group">
               <input type="text" name="ruta" class="form-control" placeholder="Nombre de la ruta">
           </div>
            <div class="form-group">
               <input type="text" name="puntos" class="form-control" placeholder="Puntos de la ruta">
           </div>
           <input type="submit" value="Agregar">
       </form>
      </div>
     
    </div>
  </div>
</div>


<div class="container-fluid">



<?php
  $this->load->view('commons/footer');
?>
</div>

