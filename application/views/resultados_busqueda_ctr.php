<?php foreach($playas as $playa){ ?>
<div class="col-xs-12 col-md-6 playa_item">
  
  <a href="#"  onclick="alerta('<?php echo $playa->latitud; ?>','<?php echo $playa->longitud; ?>','<?php echo $playa->nombre; ?>')" data-pos="<?php echo $playa->latitud; ?>,<?php echo $playa->longitud; ?>" class="add_route">Agregar a mi ruta</a>
   <div class="img-ctr">
       <img src="<?php echo base_url()?>archivos/<?php echo $playa->imagen_destacada; ?>" class="img-responsive" alt="">
   </div>
    <div class="gl-feat-item-details"><a href="#" class="view-ip" data-id="<?php echo $playa->idplaya; ?>" data-toggle="modal" data-target="#infoplayaModal"><h3><span style="font-size: 20px;line-height: 40px;"><i class="fa fa-eye" aria-hidden="true"></i></span> <?php echo $playa->nombre; ?></h3></a></div>
    
    
</div>


<?php } ?>






<!-- Modal -->
<div class="modal fade" id="infoplayaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body playam-detalle clearfix">
        
      </div>
      
    </div>
  </div>
</div>


<script>

    $('.view-ip').click(function () {
            var num = $(this).attr("data-id");
      
            $.ajax({
                url: '<? echo base_url();?>playa/info_basica', //This is the current doc
                type: "POST"
                 , data: { id: num} 
                , success: function (data) {
                    $('.playam-detalle').html(data);
                }
            });
        });
</script>