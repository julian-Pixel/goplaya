<?php
  $this->load->view('commons/header');
?>




<div class="container-fluid" style="background: #f5f5f5;">

<div class="row to-cintillo">
    <div class="col-xs-8">
      <div class="profile">
          
           <h2>Usuarios</h2>
         
      </div>
      </div>
        
    </div>   
    <div class="row ctool">
        
        <a href="<?php echo base_url();?>index.php/dashboard" style="color:white;border-color:white;float: right;" class="btn-default-gp  btn-default-gp-v">Volver al dashboard</a>
    </div>
    
</div>
<div class="container-fluid" style="padding-top: 5%;padding-bottom: 5%;">
 
 <div>
     
 </div>
 <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Usuarios</h3>
  </div>
  <div class="panel-body">
        <div class="table-responsive">
       <table class="table table-striped">
     <thead>
         <tr>
             <th>Usuario</th>
             <th>Email</th>
             <th>Fecha de registro</th>
             <th>Rol</th>
             <th>Acciones</th>
         </tr>
     </thead>

<tbody>
   <?php foreach($usuarios as $usuario){ ?>
   <tr>
        <td><?php echo $usuario->nombre; ?> <?php echo $usuario->apellidos; ?></td>
        <td><?php echo $usuario->email; ?></td>
        <td><?php echo $usuario->fecha; ?></td>
        <td><?php echo $usuario->rol; ?></td>
        <td>
        
        <?php if($usuario->estado == 1){?>
        <a href="<?php echo base_url();?>usuarios/bloquear_usuario?usuario=<?php echo $usuario->idusuario;?>" class="btn-default-gp"><i class="fa fa-ban" aria-hidden="true"></i> Bloquear</a>
        <?php }elseif($usuario->estado == 2){?>
        <a href="<?php echo base_url();?>usuarios/desbloquear_usuario?usuario=<?php echo $usuario->idusuario;?>" class="btn-default-gp"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Desbloquear</a>
        <?php }?>
        </td>
    </tr>


<?php } ?>
    
</tbody>
 </table>
  </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Administradores</h3>
  </div>
  <div class="panel-body">
   <form action="<?php echo base_url();?>usuarios/agregar_admin" method="post">
       <select name="usuario" class="form-control" id="">
            <?php foreach($usuarios as $usuario){ ?>
            <option value="<?php echo $usuario->idusuario; ?>"><?php echo $usuario->email; ?></option>

   <?php }?> 
           
       </select>
       <input type="submit" value="Agregar">
   </form>
   
   <br>
     <div class="table-responsive">
    <table class="table table-striped">
     <thead>
         <tr>
             <th>Usuario</th>
             <th>Email</th>
             <th>Fecha de registro</th>
             <th>Rol</th>
             <th>Acciones</th>
         </tr>
     </thead>

<tbody>
   <?php foreach($usuarios2 as $usuario){ ?>
   <tr>
        <td><?php echo $usuario->nombre; ?> <?php echo $usuario->apellidos; ?></td>
        <td><?php echo $usuario->email; ?></td>
        <td><?php echo $usuario->fecha; ?></td>
        <td><?php echo $usuario->rol; ?></td>
        <td>
        <a href="<?php echo base_url();?>usuarios/eliminar_admin/<?php echo $usuario->idusuario; ?>">Eliminar</a>
        </td>
    </tr>


<?php } ?>
    
</tbody>
 </table>
   
  </div>
  </div>
</div>
 
 



</div>
<div class="container-fluid">

<?php
  $this->load->view('commons/footer');
?>
</div>

