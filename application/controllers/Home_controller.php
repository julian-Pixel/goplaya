<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_controller extends CI_Controller {


    public function __construct() {
        parent::__construct();
        // $this->load->helper('language');
        //     $this->lang->load('generales');
        //     $this->load->helper("url");

        $this->load->model('login_model');
        $this->load->model('provincias_model');
        $this->load->model('playa_model');
        $this->load->model('pagina_model');
        $this->load->model('slider_model');
        $this->load->model('modulos_model');
        $this->load->model('menu_model');
        $this->load->model('publicidad_model');
        $this->load->model('email_model');
				$this->load->model('featured_model');
        $this->load->model('tour_model');
    }


	public function index()
	{
        //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales

        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }

        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');


        //Fin menu
        //Lugares
                $data['provincias'] = $this->provincias_model->todas_las_provincias();
                $data['regiones'] = $this->provincias_model->todas_las_regiones();
                $data['playas'] = $this->provincias_model->todas_las_playas();
                $data['caracteristicas'] = $this->provincias_model->todas_las_caracteristicas_simples();
                $data['tours'] = $this->tour_model->tours_disponibles_top();
        // Fin lugares

         //Articulos
            $data['articulos'] = $this->pagina_model->articulos_home();
        //Fin articulos
        //Slides

                $data['slides'] = $this->slider_model->slides();

            //Fin slides


    //Secciones home_template
        $data['secciones'] = $this->modulos_model->modulos_home();

     //Fin secciones home

    //crea tu ruta


     $data['crea_tu_ruta'] = $this->pagina_model->contenido_pagina('crea-tu-ruta');

    //Fin crea tu ruta



    //ADS

        $data['banners_superior'] = $this->publicidad_model->banners_posicion('1');
        $data['banners_medio'] = $this->publicidad_model->banners_posicion('2');
        $data['banners_final'] = $this->publicidad_model->banners_posicion('3');
        $data['banners_buscador'] = $this->publicidad_model->banners_posicion('4');

    //FIN ADS

		//Featured


$data['featured'] = $this->featured_model->fetured();
$delmes = $this->featured_model->delmes();
$populares = $this->featured_model->populares();
$delmes2 = array();
$populares2 = array();

foreach ($delmes as  $pf) {
  $estrella = $this->playa_model->estrellas($pf->idplaya);
  $d = array(

    'nombre'=>$pf->nombre,
    'slug'=>$pf->slug,
    'imagen_destacada'=>$pf->imagen_destacada,
    'total'=> $estrella[0]->total,
    'zona_es'=> $pf->zona_es,
    'registros'=>$estrella[0]->registros
  );
  array_push($delmes2 , $d);
}


foreach ($populares as  $pf) {
  $estrella = $this->playa_model->estrellas($pf->idplaya);
  $d = array(

    'nombre'=>$pf->nombre,
    'slug'=>$pf->slug,
    'imagen_destacada'=>$pf->imagen_destacada,
    'total'=> $estrella[0]->total,
    'zona_es'=> $pf->zona_es,
    'registros'=>$estrella[0]->registros
  );
  array_push($populares2 , $d);
}

$data['delmes']=$delmes2 ;
$data['populares']=$populares2 ;


        	if(isset($this->session->userdata['logged_in'])){
                $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
                $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }


            }


		$this->load->view('home',$data);
	}
}
