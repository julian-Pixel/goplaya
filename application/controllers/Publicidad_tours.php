<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publicidad_tours extends CI_Controller {


    public function __construct() {
        parent::__construct();

          $this->load->model('publicidad_tours_model');
          $this->load->model('pagina_model');
          $this->load->model('login_model');
          $this->load->model('menu_model');
          $this->load->model('email_model');
          $this->load->model('playa_model');


    }


    public function index(){

        //Datos Generales
        $data['datos_generales'] = $this->email_model->datos_generales();

        //Datos del login
        if(isset($this->session->userdata['logged_in'])){
                        $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
              $data['admin'] = $admin;
        }

        //Menu
        $data['menu_home'] = $this->menu_model->menu_items('1');





          if(isset($this->session->userdata['logged_in'])){

               //check administrador
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);

              //Datos Usuario
               $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
              $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                  if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                      $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                  }else{

                       $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                  }
                //Fin datos Usuario

                  //Slides
                  $data['banners'] = $this->publicidad_tours_model->banners();
  								$data['playas'] = $this->playa_model->get_all();



               $data['plantillas'] = $this->pagina_model->plantillas();

              	       $this->load->view('ad_publicidad_tours',$data);

            }else{


                }



      }






      public function agregar_items(){
          //Datos Generales
        $data['datos_generales'] = $this->email_model->datos_generales();
      //Fin Datos Generales

          if(isset($this->session->userdata['logged_in'])){
                $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
      $data['admin'] = $admin;
          }
          //Menu
              $data['menu_home'] = $this->menu_model->menu_items('1');
          //Fin menu
  		if(isset($this->session->userdata['logged_in'])){





                 $this->load->helper('form');

                  // retrieve the number of images uploaded;
                  $number_of_files = sizeof($_FILES['uploadedimages']['tmp_name']);
                  // considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
                  $files = $_FILES['uploadedimages'];
                  $errors = array();

                   // first make sure that there is no error in uploading the files
                    for($i=0;$i<$number_of_files;$i++)
                    {
                      if($_FILES['uploadedimages']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimages']['name'][$i];
                    }

                  if(sizeof($errors)==0){

                      // now, taking into account that there can be more than one file, for each file we will have to do the upload
                      // we first load the upload library
                      $this->load->library('upload');
                      // next we pass the upload path for the images
                      $config['upload_path'] = FCPATH . 'archivos/';
                      // also, we make sure we allow only certain type of images
                      $config['allowed_types'] = 'gif|jpg|jpeg|png';
                      for ($i = 0; $i < $number_of_files; $i++) {
                        $_FILES['uploadedimage']['name'] = $files['name'][$i];
                        $_FILES['uploadedimage']['type'] = $files['type'][$i];
                        $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
                        $_FILES['uploadedimage']['error'] = $files['error'][$i];
                        $_FILES['uploadedimage']['size'] = $files['size'][$i];
                        //now we initialize the upload library
                        $this->upload->initialize($config);
                        // we retrieve the number of files that were uploaded
                        if ($this->upload->do_upload('uploadedimage'))
                        {

                             echo 'si';
                          $data['uploads'][$i] = $this->upload->data();

                           //var_dump( $data['uploads'][$i]["file_name"]);

                            $url = $data['uploads'][$i]["file_name"];



                        }
                        else
                        {
                          $data['upload_errors'][$i] = $this->upload->display_errors();
                             var_dump($data['upload_errors'][$i]);
                        }
                      }

                    }
              else
              {
                print_r($errors);
              }





              // retrieve the number of images uploaded;
              $number_of_files = sizeof($_FILES['uploadedimagesing']['tmp_name']);
              // considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
              $files = $_FILES['uploadedimagesing'];
              $errors = array();

               // first make sure that there is no error in uploading the files
                for($i=0;$i<$number_of_files;$i++)
                {
                  if($_FILES['uploadedimagesing']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimagesing']['name'][$i];
                }

              if(sizeof($errors)==0){

                  // now, taking into account that there can be more than one file, for each file we will have to do the upload
                  // we first load the upload library
                  $this->load->library('upload');
                  // next we pass the upload path for the images
                  $config['upload_path'] = FCPATH . 'archivos/';
                  // also, we make sure we allow only certain type of images
                  $config['allowed_types'] = 'gif|jpg|jpeg|png';
                  for ($i = 0; $i < $number_of_files; $i++) {
                    $_FILES['uploadedimage']['name'] = $files['name'][$i];
                    $_FILES['uploadedimage']['type'] = $files['type'][$i];
                    $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
                    $_FILES['uploadedimage']['error'] = $files['error'][$i];
                    $_FILES['uploadedimage']['size'] = $files['size'][$i];
                    //now we initialize the upload library
                    $this->upload->initialize($config);
                    // we retrieve the number of files that were uploaded
                    if ($this->upload->do_upload('uploadedimage'))
                    {

                         echo 'si';
                      $data['uploads'][$i] = $this->upload->data();

                       //var_dump( $data['uploads'][$i]["file_name"]);

                        $url_ing = $data['uploads'][$i]["file_name"];



                    }
                    else
                    {
                      $data['upload_errors'][$i] = $this->upload->display_errors();
                         var_dump($data['upload_errors'][$i]);
                    }
                  }

                }
          else
          {
            print_r($errors);
          }



              $data = array(
                'banner'=>$url,
                'banner_ingles'=>$url_ing,
                'posicion'=>$_POST['posicion'],
                'url'=>$_POST['url'],
                'url_ingles'=>$_POST['url_ingles'],
                'idplaya'=>$_POST['playa']
            );


             $insert_p = $this->publicidad_tours_model->insertar_item($data);



    	if($insert_p){

             echo '<script>window.location="'.base_url().'publicidad_tours/?e=1"</script>';

          }else{

             echo '<script>window.location="'.base_url().'publicidad_tours/?e=2"</script>';

        }


          }


  }



  public function eliminar_item($id){

      //Menu
          $data['menu_home'] = $this->menu_model->menu_items('1');

       if(isset($this->session->userdata['logged_in'])){


             $delete = $this->publicidad_tours_model->eliminar_slide($id);
             if($delete){
                 echo '<script>window.location="'.base_url().'publicidad_tours/?e=1"</script>';
             }else{

               echo '<script>window.location="'.base_url().'publicidad_tours/?e=2"</script>';
             }

  }else{


      }

  }



}
