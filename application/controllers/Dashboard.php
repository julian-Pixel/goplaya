<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


public function __construct() {
parent::__construct();


$this->load->model('login_model');
$this->load->model('provincias_model');
$this->load->model('pagina_model');
$this->load->model('menu_model');
$this->load->model('email_model');
$this->load->model('playa_model');
$this->load->model('featured_model');
$this->load->model('tour_model');
}




public function index()
{
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu
if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
$data['admin'] = $admin;

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario
//Lugares
$data['provincias'] = $this->provincias_model->todas_las_provincias();
$data['regiones'] = $this->provincias_model->todas_las_regiones();
$data['playas'] = $this->provincias_model->todas_las_playas();
$data['caracteristicas'] = $this->provincias_model->todas_las_caracteristicas();
$data['cpadre'] = $this->provincias_model->caracteristicas_padre();
// Fin lugares

//Paginas / Entradas
$data['paginas'] = $this->pagina_model->todas_las_paginas();


//Fin paginas / Entradas

//Categorias_padre

$data['cat_padre'] = $this->provincias_model->caracteristicas_padre_all();
$data['cat_padre2'] = $this->provincias_model->caracteristicas_padre_all2();

//Fin categorias padre

if($admin){

//Vista
$this->load->view('ad_dashboard',$data);
//Fin Vista

}elseif($data['rol_user'][0]->rol==2){

echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{

echo '<script>window.location="'.base_url().''.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}



public function featured()
{
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu
if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
$data['admin'] = $admin;

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario
//Lugares
			$data['playas'] = $this->playa_model->get_all();
$data['featured'] = $this->featured_model->fetured();
$data['delmes'] = $this->featured_model->delmes();
$data['populares'] = $this->featured_model->populares();


if($admin){

//Vista
$this->load->view('ad_featured',$data);
//Fin Vista

}elseif($data['rol_user'][0]->rol==2){

echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{

echo '<script>window.location="'.base_url().''.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}




public function aprobar_tours()
{
			//Datos Generales
			$data['datos_generales'] = $this->email_model->datos_generales();
			//Menu
			$data['menu_home'] = $this->menu_model->menu_items('1');
			//Fin menu
			if(isset($this->session->userdata['logged_in'])){

			//check administrador

			$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
			//fin check adminsitrador
			//Datos Usuario
			$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
			$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
			$data['admin'] = $admin;

			if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

			$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

			}else{

			$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
			}
			//Fin datos Usuario
			//Lugares

			$data['tours'] = $this->tour_model->tours_all_join();

			if($admin){

				$data['mis_reservas'] = $this->tour_model->reservaciones_admin($this->session->userdata['logged_in']['id']);
				$data['mis_reservas2'] = $this->tour_model->reservacionesyap_admin($this->session->userdata['logged_in']['id']);

			}else{

				$data['mis_reservas'] = $this->tour_model->reservaciones($this->session->userdata['logged_in']['id']);
				$data['mis_reservas2'] = $this->tour_model->reservacionesyap($this->session->userdata['logged_in']['id']);

			}


			if($admin){

				//Selecciona todas las tour operadoras activas
				$data['tour_operadora'] = $this->tour_model->todas_las_operadoras();

				//Selecciona todas las playas
				$data['playas'] = $this->provincias_model->todas_las_playas();

			//Vista
			$this->load->view('aprobar_tours',$data);
			//Fin Vista

			}elseif($data['rol_user'][0]->rol==2){

			echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
			}else{

			echo '<script>window.location="'.base_url().''.'"</script>';

			}


			}else{
			echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
			}
}




public function estadotour()
{
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu
if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
$data['admin'] = $admin;

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario
//Lugares

$data['tours'] = $this->tour_model->tours_all();



if($admin){


$id = $_GET['id'];

if ($_GET['e'] == 1) {
	$habilitado = 1;
}
else {
	$habilitado = 0;
}

$data = array(
	'estado'=> $_GET['e'],
	'habilitado'=> $habilitado,
);





$update = $this->tour_model->actualizar_info_tour($id,$data);

if($update){
	echo '<script>window.location="'.base_url().'index.php/dashboard/aprobar_tours?e=1'.'"</script>';
}else{
	echo '<script>window.location="'.base_url().'index.php/dashboard/aprobar_tours?e=2'.'"</script>';
}



}elseif($data['rol_user'][0]->rol==2){

echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{

echo '<script>window.location="'.base_url().''.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}



public function editar_featured($id)
{
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu
if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
$data['admin'] = $admin;
$data['id'] = $id;

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario
//Lugares
$data['featured'] = $this->featured_model->fetured_by_id($id);
// $data['delmes'] = $this->featured_model->delmes();
// $data['populares'] = $this->featured_model->populares();


if($admin){

//Vista
$this->load->view('ad_featurededit',$data);
//Fin Vista

}elseif($data['rol_user'][0]->rol==2){

echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{

echo '<script>window.location="'.base_url().''.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}


public function update_featured()
{
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu
if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
$data['admin'] = $admin;

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario
//Lugares
$data['featured'] = $this->featured_model->fetured_by_id($id);
// $data['delmes'] = $this->featured_model->delmes();
// $data['populares'] = $this->featured_model->populares();

$id = $_POST['id'];
if($admin){

  $data = array(

    'icon'=>$_POST['icon'],
    'titulo_es'=>$_POST['titulo_es'],
    'titulo_en'=>$_POST['titulo_en'],
    'descrip_es'=>$_POST['des_es'],
    'descrp_en'=>$_POST['des_en'],
    'link'=>$_POST['link'],
    'icon'=>$_POST['icono'],
    'orden'=>$_POST['orden']

  );

  $update = $this->featured_model->update_featured($id,$data);

  if($update){
    echo '<script>window.location="'.base_url().'index.php/dashboard/editar_featured/'.$id.'?e=1'.'"</script>';
  }else{
    //  echo '<script>window.location="'.base_url().'index.php/dashboard/editar_featured/'.$id.'?e=2'.'"</script>';

  }

}elseif($data['rol_user'][0]->rol==2){

echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{

echo '<script>window.location="'.base_url().''.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}




//Login Actions

public function login() {
$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

if ($this->form_validation->run() == FALSE) {
if(isset($this->session->userdata['logged_in'])){
echo '<script>window.location="'.base_url().'index.php/dashboard'.'"</script>';
}else{
//echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
} else {
$data = array(
'username' => $this->input->post('username'),
'password' => md5($this->input->post('password'))
);
$result = $this->login_model->login($data);
if ($result == TRUE) {

$username = $this->input->post('username');
$result = $this->login_model->read_user_information($username);
if ($result != false) {
$session_data = array(
'id' => $result[0]->idusuario,
'email' => $result[0]->email,
'rol' => $result[0]->rol,
'estado' => $result[0]->estado
);
// Add user data in session
$this->session->set_userdata('logged_in', $session_data);

if($result[0]->rol == 2){
echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{
	echo '<script>window.location="'.$_POST['actual_url'].'"</script>';
}

}
} else {
$data = array(
'error_message' => 'Invalid Username or Password'
);

echo '<script>window.location="'.base_url().'?e=2'.'"</script>';
}
}
}



public function facebook_login(){


$email = $_POST['email'];

$fbp = $this->input->post('picture');
$data = array(
'email' => $this->input->post('email'),
'nombre' => $this->input->post('nombre'),
'contrasena' => md5($_POST['email'].''.$_POST['nombre']),
'fecha_registro' => date('Y-m-d h:m:s'),
'rol' => '1',
'estado' => '1'
);

$result = $this->login_model->facebook_login($data);

if($result){


$result = $this->login_model->read_user_information($this->input->post('email'));
if ($result != false) {
$session_data = array(
'id' => $result[0]->idusuario,
'email' => $result[0]->email,
'rol' => $result[0]->rol,
'estado' => $result[0]->estado,
'fbp' => $fbp
);
// Add user data in session
$this->session->set_userdata('logged_in', $session_data);

}



echo 'Usuario registrado';

}else{


$result = $this->login_model->read_user_information($this->input->post('email'));
if ($result != false) {
$session_data = array(
'id' => $result[0]->idusuario,
'email' => $result[0]->email,
'rol' => $result[0]->rol,
'estado' => $result[0]->estado,
'fbp' => $fbp
);
// Add user data in session
$this->session->set_userdata('logged_in', $session_data);

}


}


}

public function ingresar(){
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
$this->load->view('login_form',$data);

}


public function logout() {

// Removing session data
$sess_array = array(
'username' => ''
);
$this->session->unset_userdata('logged_in', $sess_array);
echo '<script>window.location="'.base_url().''.'"</script>';


}

public function editar_playa($id){
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu
if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario

//Lugares
$data['provincias'] = $this->provincias_model->todas_las_provincias();
$data['regiones'] = $this->provincias_model->todas_las_regiones();
$data['playas'] = $this->provincias_model->todas_las_playas();
$data['caracteristicas'] = $this->provincias_model->todas_las_caracteristicas();
$data['datos_playa'] = $this->provincias_model->playa_por_id($id);
// Fin lugares

//Galeria
$data['galeria'] = $this->provincias_model->galeria_playa($id);
// Fin galeria

$data['ads'] = $this->provincias_model->ads_playa($id);

//Actividades
$data['actividades'] = $this->provincias_model->actividades_por_playa($id);
//Fin actividades



//Filtros avanzados
$caracteristicas_padre = $this->provincias_model->caracteristicas_padre_all();
$fp = array();
foreach($caracteristicas_padre as $cp){

$ch = $this->provincias_model->todas_las_caracteristicas_por_padre($cp->id);

foreach($ch as $c){

$stack[] =  $c->detalle;
$id_c[] = $c->idactividad;
}


$fp[$cp->detalle] = array($id_c,$stack);

unset($stack);
$stack = array();
unset($id_c);
$id_c = array();



}


$data['filtros_avanzados'] = $fp;

//Fin Filtros avanzados

if($admin){

//Vista
$this->load->view('ad_editar_playa',$data);
//Fin Vista

}elseif($data['rol_user'][0]->rol==2){

echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{

echo '<script>window.location="'.base_url().''.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}



}

public function actualizar_playa(){

$id = $_POST['id'];

$data = array(
'nombre'=> $_POST['nombre'],
'descripcion' => $_POST['descripcion'],
'descripcion_en' => $_POST['descripcion_en'],
'idregion' => $_POST['region'],
'idprovincia' => $_POST['provincia'],
'latitud' => $_POST['latitud'],
'longitud' => $_POST['longitud'],
'slug' => $_POST['permalink']
);


$actualizar = $this->provincias_model->actualizar_playa($id,$data);

if($actualizar){

echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$id.'?e=1"</script>';

}else{

echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$id.'?e=2"</script>';

}

}


public function update_imagen_playa(){

var_dump($_POST);
if(isset($this->session->userdata['logged_in'])){


$data_img = $_POST['image'];
$uploadData['id'] = $_POST['xc'];
list($type, $data_img) = explode(';', $data_img);
list(, $data_img)      = explode(',', $data_img);

$data_img = base64_decode($data_img);
$imageName = time().'.jpg';
file_put_contents('archivos/'.$imageName, $data_img);


$uploadData['foto'] = $imageName;


$insert = $this->provincias_model->update_playa_image($uploadData);

var_dump($insert);
if($insert){
echo 'si';
}else{
echo 'no';
}



}else{


echo '<script>window.location.replace("'.base_url().'");</script>';


}

}


public function preview(){
$data['imagen'] = $_GET['i'];
$data['plantilla']= $_GET['b'];

$this->load->view('preview',$data);

}


public function eliminar_fichero($name){

$url = './archivos/'.$name;
$delete = unlink($url);
if($delete){
echo '<script>window.location.replace("'.base_url().'dashboard?e=1");</script>';
}else{
echo '<script>window.location.replace("'.base_url().'dashboard?e=2");</script>';
}

}
public function editar_caracteristica($id){
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu
if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario
//Lugares
$data['provincias'] = $this->provincias_model->todas_las_provincias();
$data['regiones'] = $this->provincias_model->todas_las_regiones();
$data['playas'] = $this->provincias_model->todas_las_playas();
$data['caracteristicas'] = $this->provincias_model->todas_las_caracteristicas();
$data['cpadre'] = $this->provincias_model->caracteristicas_padre();
// Fin lugares

//Paginas / Entradas
$data['paginas'] = $this->pagina_model->todas_las_paginas();


//Fin paginas / Entradas

//Categorias_padre

$data['cat_padre'] = $this->provincias_model->caracteristicas_padre_all();
$data['cat_padre2'] = $this->provincias_model->caracteristicas_padre_all2();

$data['caracteristica'] = $this->provincias_model->caracteristica_por_id($id);
//Fin categorias padre

if($admin){

$this->load->view('editar_caracteristica',$data);
}elseif($data['rol_user'][0]->rol==2){

echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{

echo '<script>window.location="'.base_url().''.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}


}



public function editar_caracteristica_padre($id){
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu
if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario
//Lugares
$data['provincias'] = $this->provincias_model->todas_las_provincias();
$data['regiones'] = $this->provincias_model->todas_las_regiones();
$data['playas'] = $this->provincias_model->todas_las_playas();
$data['caracteristicas'] = $this->provincias_model->todas_las_caracteristicas();
$data['cpadre'] = $this->provincias_model->caracteristicas_padre();
// Fin lugares

//Paginas / Entradas
$data['paginas'] = $this->pagina_model->todas_las_paginas();


//Fin paginas / Entradas

//Categorias_padre

$data['cat_padre'] = $this->provincias_model->caracteristicas_padre_all();
$data['cat_padre2'] = $this->provincias_model->caracteristicas_padre_all2();

$data['caracteristica'] = $this->provincias_model->caracteristica_padre_por_id($id);
//Fin categorias padre

if($admin){

$this->load->view('editar_caracteristica_padre',$data);
}elseif($data['rol_user'][0]->rol==2){

echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{

echo '<script>window.location="'.base_url().''.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}


}




public function update_cat(){



if(isset($this->session->userdata['logged_in'])){

//var_dump($_POST);

$id = $_POST['id'];
$data = array(
'detalle'=>$_POST['detalle'],
'detalle_en'=>$_POST['detalle_en'],
'actividad_padre'=>$_POST['catpadre'],
'simple'=>$_POST['simple'],
'icono'=>$_POST['icono'],

);

$insert = $this->provincias_model->actualizar_categoria($id,$data);

if($insert){
echo '<script>window.location="'.base_url().'dashboard/editar_caracteristica/'.$id.'?e=1"</script>';
}else{
echo '<script>window.location="'.base_url().'dashboard/editar_caracteristica/'.$id.'?e=2"</script>';
}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}
public function update_ruta(){



if(isset($this->session->userdata['logged_in'])){

//var_dump($_POST);

$id = $_POST['id'];
$data = array(
'ruta'=>$_POST['ruta'],
'puntos'=>$_POST['puntos']

);

$insert = $this->provincias_model->update_ruta($id,$data);

if($insert){
echo '<script>window.location="'.base_url().'dashboard/editar_ruta_recomendada/'.$id.'?e=1"</script>';
}else{
echo '<script>window.location="'.base_url().'dashboard/editar_ruta_recomendada/'.$id.'?e=2"</script>';
}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}

public function agregar_ruta_recomendada(){



if(isset($this->session->userdata['logged_in'])){


$data = array(
'ruta'=>$_POST['ruta'],
'puntos'=>$_POST['puntos']

);

$insert = $this->provincias_model->insert_ruta($data);

if($insert){
echo '<script>window.location="'.base_url().'dashboard/rutas_recomendadas/?e=1"</script>';
}else{
echo '<script>window.location="'.base_url().'dashboard/rutas_recomendadas/?e=2"</script>';
}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}

public function delete_ruta($id){



if(isset($this->session->userdata['logged_in'])){



$insert = $this->provincias_model->delete_ruta($id);

if($insert){
echo '<script>window.location="'.base_url().'dashboard/rutas_recomendada?e=1"</script>';
}else{
echo '<script>window.location="'.base_url().'dashboard/rutas_recomendada?e=2"</script>';
}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}


public function eliminar_playa_mes($id){



if(isset($this->session->userdata['logged_in'])){



$insert = $this->featured_model->eliminar_playa_mes($id);

if($insert){
echo '<script>window.location="'.base_url().'dashboard/featured?e=1"</script>';
}else{
echo '<script>window.location="'.base_url().'dashboard/featured?e=2"</script>';
}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}


public function eliminar_playa_popular($id){



if(isset($this->session->userdata['logged_in'])){



$insert = $this->featured_model->eliminar_playa_popular($id);

if($insert){
echo '<script>window.location="'.base_url().'dashboard/featured?e=1"</script>';
}else{
echo '<script>window.location="'.base_url().'dashboard/featured?e=2"</script>';
}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}



public function add_featured_pm(){



if(isset($this->session->userdata['logged_in'])){

  $data = array(
    'id_playa'=> $_POST['playa'],
    'zona_es'=> $_POST['zona']
  );

$insert = $this->featured_model->add_playa_mes($data);

if($insert){
echo '<script>window.location="'.base_url().'dashboard/featured?e=1"</script>';
}else{
echo '<script>window.location="'.base_url().'dashboard/featured?e=2"</script>';
}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}


public function add_featured_pp(){



if(isset($this->session->userdata['logged_in'])){

  $data = array(
    'id_playa'=> $_POST['playa'],
    'zona_es'=> $_POST['zona']
  );

$insert = $this->featured_model->add_playa_popular($data);

if($insert){
echo '<script>window.location="'.base_url().'dashboard/featured?e=1"</script>';
}else{
echo '<script>window.location="'.base_url().'dashboard/featured?e=2"</script>';
}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}

public function update_catpad(){



if(isset($this->session->userdata['logged_in'])){

//var_dump($_POST);

$id = $_POST['id'];
$data = array(
'detalle'=>$_POST['detalle'],
'detalle_en'=>$_POST['detalle_en'],
'oculto'=>$_POST['oculto'],

);

$insert = $this->provincias_model->actualizar_categoriapad($id,$data);

if($insert){
echo '<script>window.location="'.base_url().'dashboard/editar_caracteristica_padre/'.$id.'?e=1"</script>';
}else{
echo '<script>window.location="'.base_url().'dashboard/editar_caracteristica_padre/'.$id.'?e=2"</script>';
}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}

public function update_regi(){

if(isset($this->session->userdata['logged_in'])){

//var_dump($_POST);

$id = $_POST['id'];
$data = array(
'nombre'=>$_POST['detalle'],
'detalle'=>$_POST['detalle2'],
'detalle_en'=>$_POST['detalle2_en']

);

$insert = $this->provincias_model->actualizar_region($id,$data);
if($insert){
echo '<script>window.location="'.base_url().'provincias/editar_region/'.$id.'?e=1"</script>';
}else{
echo '<script>window.location="'.base_url().'provincias/editar_region/'.$id.'?e=2"</script>';
}



}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}

public function update_prov(){

if(isset($this->session->userdata['logged_in'])){

//var_dump($_POST);

$id = $_POST['id'];
$data = array(
'nombre'=>$_POST['nombre_provincia'],
'latitud'=>$_POST['latitud_provincia'],
'longitud'=>$_POST['longitud_provincia'],

);

$insert = $this->provincias_model->actualizar_prov($id,$data);
if($insert){
echo '<script>window.location="'.base_url().'provincias/editar_provincia/'.$id.'?e=1"</script>';
}else{
echo '<script>window.location="'.base_url().'provincias/editar_provincia/'.$id.'?e=2"</script>';
}



}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}




public function registrarse() {

$nombre = $_POST['nombre'];
$apellidos = $_POST['apellidos'];
$email = $_POST['email'];
$pass = $_POST['pass'];
$rol = '1';
$estado = '1';



$data = array(
'nombre' => $nombre,
'apellidos' => $apellidos,
'email' => $email,
'rol' => $rol,
'contrasena' => md5($pass),
'fecha_registro' => date("Y/m/d :h:m:s"),
'estado' => $estado
);

$result = $this->login_model->registration_insert($data);

if ($result == TRUE) {

echo '<script>window.location="'.base_url().'?e=1"</script>';

} else {

echo '<script>window.location="'.base_url().'?e=2"</script>';



}

}

public function aag() {

$data = array(
'email_contacto' =>  $_POST['email_contacto'],
'telefono' =>  $_POST['telefono'],
'direccion' =>  $_POST['direccion'],
'facebook' =>  $_POST['facebook'],
'youtube' =>  $_POST['youtube'],
'instagram' =>  $_POST['instagram'],
'smtp_host' =>  $_POST['host'],
'smtp_port' =>  $_POST['port'],
'smtp_user' =>  $_POST['user'],
'smtp_pass' =>  $_POST['password']
);


var_dump($data);

$result = $this->email_model->agg($data);

if ($result == TRUE) {

echo '<script>window.location="'.base_url().'dashboard/ajustes_generales?e=1"</script>';

} else {

echo '<script>window.location="'.base_url().'dashboard/ajustes_generales?e=2"</script>';



}

}


public function suscriptores()
{
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu
if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
$data['admin'] = $admin;

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario

$data['suscriptores'] = $this->email_model->suscriptores();



if($admin){

//Vista
$this->load->view('ad_suscriptores',$data);
//Fin Vista

}elseif($data['rol_user'][0]->rol==2){

echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{

echo '<script>window.location="'.base_url().''.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}


public function votar()
{

if(isset($this->session->userdata['logged_in'])){


$slug = $_POST['playa'];

$playa_id = $this->playa_model->informacion_general2($slug);

$id = $playa_id[0]->idplaya;

$data = array(
'id_playa' => $id,
'id_usuario' => $this->session->userdata['logged_in']['id'],
'calificacion' => $_POST['voto']
);


$insert = $this->playa_model->agregar_voto($data);

if($insert){

echo 'si';

}else{

echo 'no';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}


public function update_info()
{

if(isset($this->session->userdata['logged_in'])){



$data = array(
'nombre' => $_POST['nombre'],
'apellidos' => $_POST['apellidos'],
'telefono' => $_POST['telefono'],
'email' => $_POST['email']
);

$insert = $this->login_model->update_info($this->session->userdata['logged_in']['id'],$data);

if($insert){

echo '<script>window.location="'.base_url().'?e=1'.'"</script>';

}else{

// echo '<script>window.location="'.base_url().'?e=2'.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}



public function update_pass()
{

if(isset($this->session->userdata['logged_in'])){



$data = array(
'contrasena' =>md5($_POST['contrasena'])
);

$insert = $this->login_model->update_pass($this->session->userdata['logged_in']['id'],$data);

if($insert){

echo '<script>window.location="'.base_url().'?e=1'.'"</script>';

}else{

// echo '<script>window.location="'.base_url().'?e=2'.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}


public function update_pass_tour_operador()
{

if(isset($this->session->userdata['logged_in'])){


$id_user = $_POST['id_update_pass'];


$data = array(
'contrasena' =>md5($_POST['contrasena'])
);

$insert = $this->login_model->update_pass($id_user,$data);

if($insert){

echo '<script>window.location="'.base_url().'?e=1'.'"</script>';

}else{

// echo '<script>window.location="'.base_url().'?e=2'.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}

public function ajustes_generales()
{
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu
if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
$data['admin'] = $admin;

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario

$data['datos_generales'] = $this->email_model->datos_generales();



if($admin){

//Vista
$this->load->view('ad_ajustes_generales',$data);
//Fin Vista

}elseif($data['rol_user'][0]->rol==2){

echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{

echo '<script>window.location="'.base_url().''.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}



public function rutas_recomendadas()
{
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu
if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
$data['admin'] = $admin;

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil'] = $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil'] = 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario

$data['datos_generales'] = $this->email_model->datos_generales();
$data['rutas_predisenadas'] = $this->provincias_model->rutas_predisenadas();



if($admin){

//Vista
$this->load->view('ad_rutas_recomendadas',$data);
//Fin Vista

}elseif($data['rol_user'][0]->rol==2){

echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{

echo '<script>window.location="'.base_url().''.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}



public function editar_ruta_recomendada($id)
{
//Datos Generales
$data['datos_generales'] = $this->email_model->datos_generales();
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu
if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
$data['admin'] = $admin;

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario

$data['datos_generales'] = $this->email_model->datos_generales();
$data['ruta_predisenada'] = $this->provincias_model->ruta_predisenada($id);



if($admin){

//Vista
$this->load->view('editar_ruta_recomendada',$data);
//Fin Vista

}elseif($data['rol_user'][0]->rol==2){

echo '<script>window.location="'.base_url().'index.php/tour_operadora'.'"</script>';
}else{

echo '<script>window.location="'.base_url().''.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}


public function enviar_ruta($id)
{
if(isset($this->session->userdata['logged_in'])){

$data['mensaje'] = $_POST['rutaemail'];

$to = $_POST['email'];
$asunto = "Ruta recomendada";
$mensaje = $data;
$plantilla = "enviar_ruta";


echo $_POST['rutaemail'];

$enviar_email = $this->email_model->enviar_email2($to,$asunto,$mensaje,$plantilla);

var_dump($enviar_email);

if($enviar_email){
echo 'si';
}else{
echo 'no';
}

}else{
echo 'no';
}
}






}
