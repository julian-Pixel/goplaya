<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagina extends CI_Controller {

	  
    public function __construct() {
        parent::__construct();


        $this->load->model('pagina_model');
        $this->load->model('login_model');
        $this->load->model('modulos_model');
        $this->load->model('menu_model');
        $this->load->model('email_model');
    }

    
	public function index()
	{
        
     
    }
    
    public function detalle($slug){
          //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        
          $data['datos'] = $this->pagina_model->datos_pagina($slug);
        if(isset($this->session->userdata['logged_in'])){
            
             //check administrador
            
            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            //fin check adminsitrador
                //Datos Usuario
             $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
            
                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){
                    
                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];
                   
                }else{
                    
                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
              //Fin datos Usuario  
            
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            
     
            if($data['datos'][0]->estado == 1){
                    
                if($admin){
                   $borrador = true; 
                }else{
                     $borrador = false;
                }
                    
                    
                }else{
                    $borrador =true ;
                    
                }
                
        
            
       
        }else{
            
      
            if($data['datos'][0]->estado == 1){
                
                $borrador = false;
                
            }else{
                 $borrador = true;
            }
            
            
            
        }
        
       
        if($borrador){
            $this->load->view('detalle_pagina',$data);
        }else{
            
             $this->load->view('404',$data);
            
        }
        
          
            	
        
        
    }
    
    public function crear_pagina(){
          //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
        
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        if(isset($this->session->userdata['logged_in'])){
            
             //check administrador
            
            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            //fin check adminsitrador
                //Datos Usuario
             $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
            
                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){
                    
                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];
                   
                }else{
                    
                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
              //Fin datos Usuario  
            
            
             $data['plantillas'] = $this->pagina_model->plantillas();
      
            	$this->load->view('crear_pagina',$data);
            
    }else{
            
            
        }
           
           
	
	}
    
      public function editar_home(){
            //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        
        if(isset($this->session->userdata['logged_in'])){
            
             //check administrador
            
            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            //fin check adminsitrador
                //Datos Usuario
             $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
            
             $data['secciones'] = $this->modulos_model->modulos_home();
            $data['disponibles'] = $this->modulos_model->modulos_sin_indexar();

            
                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){
                    
                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];
                   
                }else{
                    
                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
              //Fin datos Usuario  
            
            
             $data['plantillas'] = $this->pagina_model->plantillas();
      
            	$this->load->view('editar_home',$data);
            
    }else{
            
            
        }
           
           
	
	}
    
    
     public function editar($slug){
           //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        
        if(isset($this->session->userdata['logged_in'])){
            
             //check administrador
            
            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            //fin check adminsitrador
                //Datos Usuario
             $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
            
                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){
                    
                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];
                   
                }else{
                    
                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
              //Fin datos Usuario  
            
            
             $data['plantillas'] = $this->pagina_model->plantillas();
             $data['datos'] = $this->pagina_model->datos_pagina($slug);
      
            	$this->load->view('editar_pagina',$data);
            
    }else{
            
            
        }
           
           
	
	}
     
     public function agregar_pagina(){
           //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        
        if(isset($this->session->userdata['logged_in'])){
          
            
            $data = array(
            
                'nombre'=>$_POST['nombre'],
                'contenido'=>$_POST['contenido'],
                'contenido_en'=>$_POST['contenido_en'],
                'plantilla'=>$_POST['plantilla'],
                'categoria'=>$_POST['categoria'],
                'estado'=>$_POST['estado'],
                'slug'=>$_POST['permalink']
            
            );
            
           
            $insert = $this->pagina_model->crear_pagina($data);
            
            if($insert){
                
                 echo '<script>window.location="'.base_url().'index.php/pagina/editar/'.$_POST['permalink'].'"</script>';
                
            }else{
                echo'error';
            }
            
    }else{
            
            
        }
           
           
	
	}
    
    public function editar_pagina(){
          //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
        
     //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        if(isset($this->session->userdata['logged_in'])){
          
            $id = $_POST['id'];
            $data = array(
            
                'nombre'=>$_POST['nombre'],
                'contenido'=>$_POST['contenido'],
                'contenido_en'=>$_POST['contenido_en'],
                'plantilla'=>$_POST['plantilla'],
                'categoria'=>$_POST['categoria'],
                'estado'=>$_POST['estado'],
                'slug'=>$_POST['permalink'],
                'mostrar_home'=>$_POST['home'],
                'extracto'=>$_POST['extracto']
            
            );
            
           
            $insert = $this->pagina_model->editar_pagina($id,$data);
            
            if($insert){
                
                 echo '<script>window.location="'.base_url().'index.php/pagina/editar/'.$_POST['permalink'].'"</script>';
                
            }else{
                echo'error';
            }
            
    }else{
            
            
        }
           
           
	
	}
    
 
    
    
    public function agregar_banner_articulo(){
          //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
		if(isset($this->session->userdata['logged_in'])){
            
           
        // var_dump($_POST);
            
        
               $this->load->helper('form');
 
                // retrieve the number of images uploaded;
                $number_of_files = sizeof($_FILES['uploadedimages']['tmp_name']);
                // considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
                $files = $_FILES['uploadedimages'];
                $errors = array();

           // first make sure that there is no error in uploading the files
            for($i=0;$i<$number_of_files;$i++)
            {
              if($_FILES['uploadedimages']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimages']['name'][$i];
            }
                
                if(sizeof($errors)==0){
      // now, taking into account that there can be more than one file, for each file we will have to do the upload
      // we first load the upload library
      $this->load->library('upload');
      // next we pass the upload path for the images
      $config['upload_path'] = FCPATH . 'archivos/';
      // also, we make sure we allow only certain type of images
      $config['allowed_types'] = 'gif|jpg|png';
      for ($i = 0; $i < $number_of_files; $i++) {
        $_FILES['uploadedimage']['name'] = $files['name'][$i];
        $_FILES['uploadedimage']['type'] = $files['type'][$i];
        $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
        $_FILES['uploadedimage']['error'] = $files['error'][$i];
        $_FILES['uploadedimage']['size'] = $files['size'][$i];
        //now we initialize the upload library
        $this->upload->initialize($config);
        // we retrieve the number of files that were uploaded
        if ($this->upload->do_upload('uploadedimage'))
        {
          $data['uploads'][$i] = $this->upload->data();
            
           //var_dump( $data['uploads'][$i]["file_name"]);
            
            $url = $data['uploads'][$i]["file_name"];
            $id = $_POST['paginaid'];
            
              $data = array(
                'banner'=>$url
            );
            
          
             $update = $this->pagina_model->insertar_banner($id,$data);
        }
        else
        {
          $data['upload_errors'][$i] = $this->upload->display_errors();
        }
      }
    }
    else
    {
      print_r($errors);
    }
                
                
      
	if($update){
        
         echo '<script>window.location="'.base_url().'index.php/pagina/editar/'.$_POST['permalink'].'?e=1"</script>';
        
    }else{
        
        // echo '<script>window.location="'.base_url().'index.php/pagina/editar/'.$_POST['permalink'].'?e=2"</script>';
        
    }
        
        
        }
    
       
}
    
    
}
        
      

