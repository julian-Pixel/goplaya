<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tours extends CI_Controller {


    public function __construct() {
        parent::__construct();

        $this->load->model('login_model');
        $this->load->model('provincias_model');
        $this->load->model('pagina_model');
        $this->load->model('slider_model');
        $this->load->model('modulos_model');
        $this->load->model('menu_model');
        $this->load->model('tour_model');
        $this->load->model('email_model');
        $this->load->model('publicidad_tours_model');
        $this->load->model('opiniones_model');
    }


public function cantones($provincia = 1){


$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://ubicaciones.paginasweb.cr/provincia/".$provincia."/cantones.json",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Postman-Token: 75d47bb7-b04b-4c7d-8185-54c163c0087f",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}

}


public function provincias(){


$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://ubicaciones.paginasweb.cr/provincias.json",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Postman-Token: 75d47bb7-b04b-4c7d-8185-54c163c0087f",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}

}



        public function index(){

          if(isset($_GET['filtros'])){

              $data['tours'] = $this->tour_model->tours_disponibles_tipo($_GET['filtros'][0]);



          }elseif(isset($_GET['busqueda'])){
              $data['tours'] = $this->tour_model->tours_disponibles_like($_GET['busqueda']);

          }else{

            $data['tours'] = $this->tour_model->tours_disponibles();

          }


             //Menu
                $data['menu_home'] = $this->menu_model->menu_items('1');
                $data['menu_footer'] = $this->menu_model->menu_items('2');

            //Fin menu
            //Lugares
                $data['provincias'] = $this->provincias_model->todas_las_provincias();
                $data['regiones'] = $this->provincias_model->todas_las_regiones();
                $data['playas'] = $this->provincias_model->todas_las_playas();
                $data['caracteristicas'] = $this->tour_model->todas_los_tipos_de_tour();
            // Fin lugares

             //Articulos
                $data['articulos'] = $this->pagina_model->articulos_home();
            //Fin articulos
            //Slides

                    $data['slides'] = $this->slider_model->slides();

                //Fin slides


                //ADS
                $data['banners_superior'] = $this->publicidad_tours_model->banners_posicion('1');
                $data['banners_medio'] = $this->publicidad_tours_model->banners_posicion('2');
                $data['banners_final'] = $this->publicidad_tours_model->banners_posicion('3');



        //Secciones home_template
            $data['secciones'] = $this->modulos_model->modulos_home();



            //Carga iconos de tours
                $data['iconos_tours'] = $this->tour_model->todos_los_iconos();

                //Carga precios
                $data['precios'] = $this->tour_model->precios_tour($id);

        //Fin secciones home

            	if(isset($this->session->userdata['logged_in'])){
                    $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
                    $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                    if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                        $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                    }else{

                         $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                    }

                }


             $this->load->view('tours.php',$data);





        }


    public function tipos_de_tour(){
      //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();

      //Datos del login
      if(isset($this->session->userdata['logged_in'])){
                      $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            $data['admin'] = $admin;


      }

      //Menu
      $data['menu_home'] = $this->menu_model->menu_items('1');


      if(isset($this->session->userdata['logged_in'])){

           //check administrador
          $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);

          //Datos Usuario
           $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
          $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

              if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                  $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

              }else{

                   $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
              }
            //Fin datos Usuario

            //tipos de tours
              $data['tipos'] = $this->tour_model->todas_los_tipos_de_tour();





           $data['plantillas'] = $this->pagina_model->plantillas();

                   $this->load->view('ad_tipos_de_tours',$data);

        }else{


            }




    }





    public function removeCache()
 {
 $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
 $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
 $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
 $this->output->set_header('Pragma: no-cache');
 }

    public function agregar_tipo_de_tour(){

      //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
      //Fin Datos Generales

      if(isset($this->session->userdata['logged_in'])){
            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            $data['admin'] = $admin;
      }

      //Menu
      $data['menu_home'] = $this->menu_model->menu_items('1');


      if(isset($this->session->userdata['logged_in'])){


                 $this->load->helper('form');

                 $data = array(
                   'detalle'=>$_POST['detalle'],
                   'detalle_en'=>$_POST['detalle_en'],
                   'icono'=>'',
                   'actividad_padre'=>0,
                   'simple'=>0
               );


                $insert_p = $this->tour_model->insertar_tipo_tour($data);


            	if($insert_p){

                     echo '<script>window.location="'.base_url().'tours/tipos_de_tour/?e=1"</script>';

                  }else{

                     echo '<script>window.location="'.base_url().'tours/tipos_de_tour/?e=2"</script>';
                }
          }
    }





    public function editar_tipo_de_tour(){

      //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
      //Fin Datos Generales

      if(isset($this->session->userdata['logged_in'])){
            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            $data['admin'] = $admin;
      }

      //Menu
      $data['menu_home'] = $this->menu_model->menu_items('1');


      if(isset($this->session->userdata['logged_in'])){


                 $this->load->helper('form');

                 $id = $_POST['id'];
                 $data = array(
                   'detalle'=>$_POST['detalle'],
                   'detalle_en'=>$_POST['detalle_en'],
                   'icono'=>'',
                   'actividad_padre'=>0,
                   'simple'=>0
               );


                $update_p = $this->tour_model->editar_tipo_tour($id,$data);


            	if($update_p){

                     echo '<script>window.location="'.base_url().'tours/tipos_de_tour/?e=1"</script>';

                  }else{

                     echo '<script>window.location="'.base_url().'tours/tipos_de_tour/?e=2"</script>';
                }
          }
    }


    public function editar_como_subir_video(){

      //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
      //Fin Datos Generales

      if(isset($this->session->userdata['logged_in'])){
            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            $data['admin'] = $admin;
      }

      //Menu
      $data['menu_home'] = $this->menu_model->menu_items('1');


      if(isset($this->session->userdata['logged_in'])){


                 $this->load->helper('form');

                 $id = $_POST['id'];
                 $data = array(
                   'video'=>$_POST['video'],
                   'video_eng'=>$_POST['video_eng'],
               );


                $update_p = $this->tour_model->editar_como_subir_video($id,$data);


            	if($update_p){

                     echo '<script>window.location="'.base_url().'tours/como_subir_tours/?e=1"</script>';

                  }else{

                     echo '<script>window.location="'.base_url().'tours/como_subir_tours/?e=2"</script>';
                }
          }
    }



    public function editar_dudas_de_tour(){

      //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
      //Fin Datos Generales

      if(isset($this->session->userdata['logged_in'])){
            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            $data['admin'] = $admin;
      }

      //Menu
      $data['menu_home'] = $this->menu_model->menu_items('1');


      if(isset($this->session->userdata['logged_in'])){


                 $this->load->helper('form');

                 $id = $_POST['id'];
                 $data = array(
                   'nombre'=>$_POST['nombre'],
                   'nombre_eng'=>$_POST['nombre_eng'],
                   'comentario'=>$_POST['comentario'],
                   'comentario_eng'=>$_POST['comentario_eng'],
                   'video'=>$_POST['video'],
                   'video_eng'=>$_POST['video_eng'],
                   'estado'=>$_POST['estado'],
               );


                $update_p = $this->tour_model->editar_dudas_tour($id,$data);


            	if($update_p){

                     echo '<script>window.location="'.base_url().'tours/dudas_de_tours/?e=1"</script>';

                  }else{

                     echo '<script>window.location="'.base_url().'tours/dudas_de_tours/?e=2"</script>';
                }
          }
    }




    public function editar_iconos_tour(){

      //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
      //Fin Datos Generales

      if(isset($this->session->userdata['logged_in'])){
            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            $data['admin'] = $admin;
      }

      //Menu
      $data['menu_home'] = $this->menu_model->menu_items('1');


      if(isset($this->session->userdata['logged_in'])){


                 $this->load->helper('form');





                 // retrieve the number of images uploaded;
                 $number_of_files = sizeof($_FILES['uploadedimages']['tmp_name']);
                 // considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
                 $files = $_FILES['uploadedimages'];
                 $errors = array();

            // first make sure that there is no error in uploading the files
             for($i=0;$i<$number_of_files;$i++)
             {
               if($_FILES['uploadedimages']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimages']['name'][$i];
             }

             // if(sizeof($errors)==0){

   // now, taking into account that there can be more than one file, for each file we will have to do the upload
   // we first load the upload library
   $this->load->library('upload');
   // next we pass the upload path for the images
   $config['upload_path'] = FCPATH . 'archivos/';
   // also, we make sure we allow only certain type of images
   $config['allowed_types'] = 'gif|jpg|jpeg|png';
   for ($i = 0; $i < $number_of_files; $i++) {
     $_FILES['uploadedimage']['name'] = $files['name'][$i];
     $_FILES['uploadedimage']['type'] = $files['type'][$i];
     $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
     $_FILES['uploadedimage']['error'] = $files['error'][$i];
     $_FILES['uploadedimage']['size'] = $files['size'][$i];
     //now we initialize the upload library
     $this->upload->initialize($config);
     // we retrieve the number of files that were uploaded
     // if ($this->upload->do_upload('uploadedimage'))
     // {

       $this->upload->do_upload('uploadedimage');

       $data['uploads'][$i] = $this->upload->data();

        //var_dump( $data['uploads'][$i]["file_name"]);

$id = $_POST['id'];

         $url = $data['uploads'][$i]["file_name"];

         if ($url == null || $url ==  "") {
            $url = $_POST['nombreicono'];
         }


           $data = array(
             'icono'=>$url,
             'descripcion_esp'=>$_POST['descripcion_esp'],
             'descripcion_eng'=>$_POST['descripcion_eng'],
             'texto_esp'=>$_POST['texto_esp'],
             'texto_eng'=>$_POST['texto_eng'],
             'url_esp'=>$_POST['url_esp'],
             'url_eng'=>$_POST['url_eng'],
         );


        $update_p = $this->tour_model->editar_iconos_tour($id,$data);

        if($update_p){

               echo '<script>window.location="'.base_url().'tours/iconos_tours/?e=1"</script>';

            }else{

               echo '<script>window.location="'.base_url().'tours/iconos_tours/?e=2"</script>';
          }
     // }
     // else
     // {
     //   $data['upload_errors'][$i] = $this->upload->display_errors();
     //     echo '<script>window.location="'.base_url().'tours/iconos_tours/?e=2"</script>';
     // }
   }
 // }
 // else
 // {
 //   print_r($errors);
 //      echo '<script>window.location="'.base_url().'tours/iconos_tours/?e=2"</script>';
 // }










          }
    }



    public function dudas_de_tours(){
      //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();

      //Datos del login
      if(isset($this->session->userdata['logged_in'])){
                      $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            $data['admin'] = $admin;


      }

      //Menu
      $data['menu_home'] = $this->menu_model->menu_items('1');


      if(isset($this->session->userdata['logged_in'])){

           //check administrador
          $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);

          //Datos Usuario
           $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
          $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

              if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                  $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

              }else{

                   $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
              }
            //Fin datos Usuario

            //tipos de tours
              $data['dudas'] = $this->tour_model->todas_las_dudas();





           $data['plantillas'] = $this->pagina_model->plantillas();

                   $this->load->view('ad_dudas',$data);

        }else{


            }




    }



    public function agregar_dudas_de_tour(){

      //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
      //Fin Datos Generales

      if(isset($this->session->userdata['logged_in'])){
            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            $data['admin'] = $admin;
      }

      //Menu
      $data['menu_home'] = $this->menu_model->menu_items('1');


      if(isset($this->session->userdata['logged_in'])){


                 $this->load->helper('form');

                 $data = array(
                   'nombre'=>$_POST['nombre'],
                   'nombre_eng'=>$_POST['nombre_eng'],
                   'comentario'=>$_POST['comentario'],
                   'comentario_eng'=>$_POST['comentario_eng'],
                   'video'=>$_POST['video'],
                   'video_eng'=>$_POST['video_eng'],
                   'estado'=>$_POST['estado'],
               );


                $insert_p = $this->tour_model->insertar_dudas_tour($data);


              if($insert_p){

                     echo '<script>window.location="'.base_url().'tours/dudas_de_tours/?e=1"</script>';

                  }else{

                     echo '<script>window.location="'.base_url().'tours/dudas_de_tours/?e=2"</script>';
                }
          }
    }


    public function eliminar_dudas($id){

        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');

         if(isset($this->session->userdata['logged_in'])){


               $delete = $this->tour_model->eliminar_dudas($id);
               if($delete){
                   echo '<script>window.location="'.base_url().'tours/dudas_de_tours/?e=1"</script>';
               }else{

                 echo '<script>window.location="'.base_url().'tours/dudas_de_tours/?e=2"</script>';
               }

    }else{


        }

    }



    public function iconos_tours(){
      //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();

      //Datos del login
      if(isset($this->session->userdata['logged_in'])){
                      $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            $data['admin'] = $admin;


      }

      //Menu
      $data['menu_home'] = $this->menu_model->menu_items('1');


      if(isset($this->session->userdata['logged_in'])){

           //check administrador
          $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);

          //Datos Usuario
           $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
          $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

              if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                  $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

              }else{

                   $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
              }
            //Fin datos Usuario

            //tipos de tours
              $data['iconos_tours'] = $this->tour_model->todos_los_iconos();





           $data['plantillas'] = $this->pagina_model->plantillas();

                   $this->load->view('ad_iconos_tours',$data);

        }else{


            }




    }








    public function agregar_iconos(){
        //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales

        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
    if(isset($this->session->userdata['logged_in'])){





               $this->load->helper('form');

                // retrieve the number of images uploaded;
                $number_of_files = sizeof($_FILES['uploadedimages']['tmp_name']);
                // considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
                $files = $_FILES['uploadedimages'];
                $errors = array();

           // first make sure that there is no error in uploading the files
            for($i=0;$i<$number_of_files;$i++)
            {
              if($_FILES['uploadedimages']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimages']['name'][$i];
            }

                if(sizeof($errors)==0){

      // now, taking into account that there can be more than one file, for each file we will have to do the upload
      // we first load the upload library
      $this->load->library('upload');
      // next we pass the upload path for the images
      $config['upload_path'] = FCPATH . 'archivos/';
      // also, we make sure we allow only certain type of images
      $config['allowed_types'] = 'gif|jpg|jpeg|png';
      for ($i = 0; $i < $number_of_files; $i++) {
        $_FILES['uploadedimage']['name'] = $files['name'][$i];
        $_FILES['uploadedimage']['type'] = $files['type'][$i];
        $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
        $_FILES['uploadedimage']['error'] = $files['error'][$i];
        $_FILES['uploadedimage']['size'] = $files['size'][$i];
        //now we initialize the upload library
        $this->upload->initialize($config);
        // we retrieve the number of files that were uploaded
        if ($this->upload->do_upload('uploadedimage'))
        {

             echo 'si';
          $data['uploads'][$i] = $this->upload->data();

           //var_dump( $data['uploads'][$i]["file_name"]);

            $url = $data['uploads'][$i]["file_name"];


              $data = array(
                'icono'=>$url,
                'descripcion_esp'=>$_POST['descripcion_esp'],
                'descripcion_eng'=>$_POST['descripcion_eng'],
                'texto_esp'=>$_POST['texto_esp'],
                'texto_eng'=>$_POST['texto_eng'],
                'url_esp'=>$_POST['url_esp'],
                'url_eng'=>$_POST['url_eng'],
            );


             $insert_p = $this->tour_model->insertar_icono_tour($data);
        }
        else
        {
          $data['upload_errors'][$i] = $this->upload->display_errors();
             var_dump($data['upload_errors'][$i]);
        }
      }
    }
    else
    {
      print_r($errors);
    }



  if($insert_p){

         echo '<script>window.location="'.base_url().'tours/iconos_tours/?e=1"</script>';

    }else{

         echo '<script>window.location="'.base_url().'tours/iconos_tours/?e=2"</script>';

    }


        }


}



public function eliminar_iconos_tours($id){

    //Menu
        $data['menu_home'] = $this->menu_model->menu_items('1');

     if(isset($this->session->userdata['logged_in'])){


           $delete = $this->tour_model->eliminar_icono_tour($id);
           if($delete){
               echo '<script>window.location="'.base_url().'tours/iconos_tours/?e=1"</script>';
           }else{

             echo '<script>window.location="'.base_url().'tours/iconos_tours/?e=2"</script>';
           }

}else{


    }

}


public function tipos_tours_id(){

    //Menu
        $data['menu_home'] = $this->menu_model->menu_items('1');

     if(isset($this->session->userdata['logged_in'])){

           $id  = $_POST['id'];

           $delete = $this->tour_model->tipos_de_tour_id($id);
           if($delete){
              echo json_encode($delete);
           }else{

             echo '<script>window.location="'.base_url().'tours/tipos_de_tour/?e=2"</script>';
           }

}else{


    }

  }


  public function dudas_tours_id(){

      //Menu
          $data['menu_home'] = $this->menu_model->menu_items('1');

       if(isset($this->session->userdata['logged_in'])){

             $id  = $_POST['id'];

             $select = $this->tour_model->dudas_de_tour_id($id);
             if($select){
                echo json_encode($select);
             }else{

               echo '<script>window.location="'.base_url().'tours/tipos_de_tour/?e=2"</script>';
             }

  }else{


      }

    }


    public function como_subir_tours(){
      //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();

      //Datos del login
      if(isset($this->session->userdata['logged_in'])){
                      $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            $data['admin'] = $admin;


      }

      //Menu
      $data['menu_home'] = $this->menu_model->menu_items('1');


      if(isset($this->session->userdata['logged_in'])){

           //check administrador
          $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);

          //Datos Usuario
           $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
          $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

              if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                  $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

              }else{

                   $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
              }
            //Fin datos Usuario

            //tipos de tours
              $data['videos_tours'] = $this->tour_model->todos_los_videos();





           $data['plantillas'] = $this->pagina_model->plantillas();

                   $this->load->view('ad_como_subir_tour',$data);

        }else{


            }




    }


    public function opiniones(){
      //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();

      //Datos del login
      if(isset($this->session->userdata['logged_in'])){
                      $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            $data['admin'] = $admin;


      }

      //Menu
      $data['menu_home'] = $this->menu_model->menu_items('1');


      if(isset($this->session->userdata['logged_in'])){

           //check administrador
          $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);

          //Datos Usuario
           $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
          $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

              if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                  $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

              }else{

                   $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
              }
            //Fin datos Usuario

            //seleccionar todas las opiniones
            $data['opiniones'] = $this->opiniones_model->todas_las_opiniones();




            $this->load->view('ad_opiniones',$data);

        }else{


            }
    }



    public function aprobar_opiniones()
    {
        //Datos Generales
        $data['datos_generales'] = $this->email_model->datos_generales();
        //Menu
        $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        if(isset($this->session->userdata['logged_in'])){

        //check administrador

        $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
        //fin check adminsitrador
        //Datos Usuario
        $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
        $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
        $data['admin'] = $admin;

        if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

        $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

        }else{

        $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
        }
        //Fin datos Usuario


        if($admin){

          //seleccionar todas las opiniones
          $data['opiniones'] = $this->opiniones_model->todas_las_opiniones();


            $id = $_GET['id'];

            $data = array(
            	'habilitado'=> $_GET['e'],
            );

            $update = $this->opiniones_model->actualizar_opinion($id,$data);

            if($update){
            	echo '<script>window.location="'.base_url().'tours/opiniones?e=1'.'"</script>';
            }else{
            	echo '<script>window.location="'.base_url().'tours/opiniones?e=2'.'"</script>';
            }

        }

      }else{
        echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
      }
    }



    public function iconos_tours_id(){

        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');

         if(isset($this->session->userdata['logged_in'])){

               $id  = $_POST['id'];

               $select = $this->tour_model->iconos_de_tour_id($id);
               if($select){
                  echo json_encode($select);
               }else{

                 echo '<script>window.location="'.base_url().'tours/iconos_tours/?e=2"</script>';
               }

    }else{


        }

      }


      public function como_subir_videos_tours_id(){

          //Menu
              $data['menu_home'] = $this->menu_model->menu_items('1');

           if(isset($this->session->userdata['logged_in'])){

                 $id  = $_POST['id'];

                 $select = $this->tour_model->como_subir_videos_tours_id($id);
                 if($select){
                    echo json_encode($select);
                 }else{

                   echo '<script>window.location="'.base_url().'tours/iconos_tours/?e=2"</script>';
                 }

      }else{


          }

        }




  // CRUD Tour Operadores

  public function Ad_Tour_Operadores(){
    //Datos Generales
    $data['datos_generales'] = $this->email_model->datos_generales();

    //Datos del login
    if(isset($this->session->userdata['logged_in'])){
                    $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
          $data['admin'] = $admin;


    }

    //Menu
    $data['menu_home'] = $this->menu_model->menu_items('1');


    if(isset($this->session->userdata['logged_in'])){

         //check administrador
        $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);

        //Datos Usuario
         $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
        $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

            if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

            }else{

                 $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
            }
          //Fin datos Usuario

          //tipos de tours
            $data['tour_operadoras'] = $this->tour_model->todas_las_tour_operadoras();





         $data['plantillas'] = $this->pagina_model->plantillas();

                 $this->load->view('ad_tour_operadores',$data);

      }else{


          }

  }


      public function tour_operadora_id(){

          //Menu
              $data['menu_home'] = $this->menu_model->menu_items('1');

           if(isset($this->session->userdata['logged_in'])){

                 $id  = $_POST['id_update'];

                 $select = $this->tour_model->tour_operadora_id($id);
                 if($select){
                    echo json_encode($select);
                 }else{

                   echo '<script>window.location="'.base_url().'tours/ad_tour_operadores/?e=2"</script>';
                 }

      }else{


          }

        }


        public function update_pass_tour_operador()
        {

        if(isset($this->session->userdata['logged_in'])){


        $id_user = $_POST['id_update_pass'];


        $data = array(
        'contrasena' =>md5($_POST['contrasena'])
        );

        $update = $this->login_model->update_pass($id_user,$data);



        if($update){


              $datos = array(
                  'status' => '1',
                  'codigo' => '',
                  'titulo' => "Completado",
                  'mensaje' => "Contraseña Modificada",

              );

        }else{

          $datos = array(
              'status' => '1',
              'codigo' => '',
              'titulo' => "No Completado",
              'mensaje' => "Contraseña NO Modificada",

          );

        }


        }else{
        echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
        }


            echo json_encode($datos);


        }


  // Fin CRUD Tour Operadores




    public function generar_playas(){


        $s = $_POST['term'];

        $playas = $this->tour_model->generar_playas($s);

        if($playas){
             echo json_encode($playas);
        }else{

  $string = '
  [{"idplaya":"1","nombre":"No se encontraron resultados","provincia":""}]
  ';

        $obj = json_decode($string);
  echo json_encode($obj);
        }



    }


    public function generar_playas_all(){


        $s = $_POST['term'];

        $playas = $this->tour_model->generar_playas_all();

        if($playas){
             echo json_encode($playas);
        }else{

  $string = '
  [{"idplaya":"1","nombre":"No se encontraron resultados","provincia":""}]
  ';

        $obj = json_decode($string);
  echo json_encode($obj);
        }



    }



    public function tours_disponibles_destacados(){


        $s = $_POST['term'];

        $tours = $this->tour_model->tours_disponibles_destacados();

        if($tours){

             echo json_encode($tours);
        }else{

  $string = '
  [{"idplaya":"1","nombre":"No se encontraron resultados","provincia":""}]
  ';

        $obj = json_decode($string);
  echo json_encode($obj);
        }



    }

    public function tour($id){
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu



        	if(isset($this->session->userdata['logged_in'])){
                $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
                $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }


            }

             $data['playas'] = $this->provincias_model->todas_las_playas();
             $data['tour'] = $this->tour_model->tours_por_id($id);
             $data['fechas_disponibles'] = $this->tour_model->fechas_disponibles_tour($id);
             $data['precios'] = $this->tour_model->precios_tour($id);
             $data['galeria'] = $this->tour_model->galeria_tour($id);
             $data['comentarios'] = $this->opiniones_model->todas_las_opiniones_where($id);
               $data['dudas'] = $this->tour_model->todas_las_dudas();


           // }elseif($data['rol_user'][0]->rol==2){
           // }else{
           // }

if($data['tour'][0]->estado == 2){

  if($data['rol_user'][0]->rol==1 || $data['rol_user'][0]->rol==2){
    $this->load->view('tour_detail.php',$data);
  }else{
      echo 'No disponible';
  }

}else{
  $this->load->view('tour_detail.php',$data);
}



         }




    public function actualizar_tour(){

    if(isset($this->session->userdata['logged_in'])){

        $id = $_POST['id'];
        $fecha = explode('-',$_POST['fecha']);
        $fi =$fecha[0];
        $ff =$fecha[1];

        $descuento  = number_format(($_POST['descuento'] / 100),1);


        if($_POST[reviews] == null) $_POST[reviews] = 0;

        $data = array(
            'nombre'=>$_POST['nombre'],
            'nombre_eng'=>$_POST['nombreen'],
            'descripcion'=>$_POST['descripcion'],
            'descripcion_eng'=>$_POST['descripcionen'],
            'idcategoria'=>$_POST['categoria'],
            'itinerario_es'=>$_POST['itinerario'],
            'itinerario_en'=>$_POST['itinerarioeng'],
            'id_playa'=>$_POST['playa'],
            'precio'=>$_POST['precio'],
            'fecha_tour'=>$fi,
            'fecha_final_tour'=>$ff,
            'porcetaje_descuento'=>$descuento,
            'maximo_personas'=>$_POST['maximo'],
            'lat'=>$_POST['latitud'],
            'lng'=>$_POST['longitud'],
            'direccion_exacta'=>$_POST['direccion_exacta'],
            'direccion_exacta_eng'=>$_POST['direccion_exacta_eng'],
            'cate1'=>$_POST['categoria'],
            'cate2'=>$_POST['categoria2'],
            'reviews'=>$_POST['reviews'],
            'opinion'=>$_POST['opinion'],
            'detalle'=>$_POST['detalle'],
            'detalle_eng'=>$_POST['detalle_eng'],
            'cancelacion'=>$_POST['cancelacion'],
            'cancelacion_eng'=>$_POST['cancelacion_eng'],
            'duracion'=>$_POST['duracion'],
            'duracion_eng'=>$_POST['duracion_eng'],
            'habilitado'=>number_format($_POST['estado_tour'],1),
            'destacado'=>number_format($_POST['destacado_tour'],1)

        );



        $update = $this->tour_model->actualizar_info_tour($id,$data);

        if($update){


            if( $_SESSION['idioma']   == 'es' ){

              $response = array(
                'status'=>'1',
                'titulo'=>'Gracias por agregar un tour en GOPlaya.',
                'mensaje'=>'Nuestro equipo lo revisará lo antes posible.'
              );

              echo json_encode($response);


            }else if( $_SESSION['idioma']   == 'en' ){

              $response = array(
                'status'=>'1',
                'titulo'=>'Thanks for adding a tour at GOPlaya.',
                'mensaje'=>'Our team will check it out as soon as possible.'
              );

              echo json_encode($response);

            }




        }else{




          if( $_SESSION['idioma']   == 'es' ){

            $response = array(
              'status'=>'2',
              'titulo'=>'No completado',
              'mensaje'=>'Accion no completada'
            );

            echo json_encode($response);



          }else if( $_SESSION['idioma']   == 'en' ){

            $response = array(
              'status'=>'2',
              'titulo'=>'Not completed',
              'mensaje'=>'Action not completed'
            );

            echo json_encode($response);

          }


        }

            } else{

            echo 'window.location="'.base_url().'index.php/dashboard/ingresar'.'"';

        }

    }






    public function agregar_opiniones(){

    if(isset($this->session->userdata['logged_in'])){



        $data = array(
            'nombre'=>$_POST['nombre'],
            'correo'=>$_POST['correo'],
            'opinion'=>$_POST['opinion'],
            'id_tour'=>$_POST['id'],
            'habilitado'=>2,
            'fecha' => date("Y-m-d"),
        );



        $insert = $this->opiniones_model->agregar_opinion($data);

        if($insert){


            if( $_SESSION['idioma']   == 'es' ){

              $response = array(
                'status'=>'1',
                'titulo'=>'Gracias por agregar una opinión.',
                'mensaje'=>'Nuestro equipo lo revisará lo antes posible.'
              );

              echo json_encode($response);


            }else if( $_SESSION['idioma']   == 'en' ){

              $response = array(
                'status'=>'1',
                'titulo'=>'Thanks for adding a comment.',
                'mensaje'=>'Our team will check it out as soon as possible.'
              );

              echo json_encode($response);

            }




        }else{




          if( $_SESSION['idioma']   == 'es' ){

            $response = array(
              'status'=>'2',
              'titulo'=>'No completado',
              'mensaje'=>'Accion no completada'
            );

            echo json_encode($response);



          }else if( $_SESSION['idioma']   == 'en' ){

            $response = array(
              'status'=>'2',
              'titulo'=>'Not completed',
              'mensaje'=>'Action not completed'
            );

            echo json_encode($response);

          }


        }

            } else{

            echo 'window.location="'.base_url().'index.php/dashboard/ingresar'.'"';

        }

    }




    public function actualizar_tour_seo(){

    if(isset($this->session->userdata['logged_in'])){

        $id = $_POST['id'];
        $fecha = explode('-',$_POST['fecha']);
        $fi =$fecha[0];
        $ff =$fecha[1];

        $descuento  = number_format(($_POST['descuento'] / 100),1);


        if($_POST[reviews] == null) $_POST[reviews] = 0;

        $data = array(
          'meta_title_es'=> $_POST['meta_title_es'],
          'meta_title_en'=> $_POST['meta_title_en'],
          'meta_desc_es'=> $_POST['meta_desc_es'],
          'meta_desc_en'=> $_POST['meta_desc_en'],
          'keywords_es'=> $_POST['keywords_es'],
          'keywords_en'=> $_POST['keywords_en']
        );


        $update = $this->tour_model->actualizar_info_tour($id,$data);

        if($update){
            echo '1';

        }else{

            echo '2';
        }

            } else{

            echo 'window.location="'.base_url().'index.php/dashboard/ingresar'.'"';

        }

    }

		public function verificar_tour(){

			$tour = $_POST['tour'];
			$date = $_POST['fecha'];
			$_POST['time'];


			if(isset($_POST['adultos'])){
						$_POST['adultos'];
			}else{
						$_POST['adultos']='0';
			}


			if(isset($_POST['estudiantes'])){
						$_POST['estudiantes'];
			}else{
						$_POST['estudiantes']='0';
			}
			if(isset($_POST['ninos'])){
						$_POST['ninos'];
			}else{
						$_POST['ninos']='0';
			}
			if(isset($_POST['menores'])){
						$_POST['menores'];
			}else{
						$_POST['menores']='0';
			}

			$cantidad_personas = $_POST['adultos']+$_POST['estudiantes']+$_POST['ninos']+$_POST['menores'];

			$consultar_disponibilidad = $this->tour_model->verificar_tour($tour,$date);
			if(!empty($consultar_disponibilidad)){


				if($consultar_disponibilidad[0]->cupos >= $cantidad_personas){

					$data= array(
							'response'=>'1',
							'code'=>'Realizar reserva',
					);

					echo json_encode($data);

				}else{


						$data= array(
								'response'=>'2',
								'code'=>'No hay cupo para '.$cantidad_personas.' personas en la fecha seleccionada',
						);

							echo json_encode($data);
				}

			}

		}

		public function verificar_horas_tour(){

			$tour = $_POST['tour'];
			$date = $_POST['fecha'];
			$consultar_disponibilidad = $this->tour_model->verificar_horas_tour($tour,$date);
			echo json_encode($consultar_disponibilidad);

		}

		public function verificar_cupo_horas_tour(){

			$tour = $_POST['tour'];
			$date = $_POST['fecha'];
			$hora = $_POST['hora'];

			// 	$tour = 1;
			// $date = '2018-01-11';
			// $hora = '12:30:00';

			$consultar_disponibilidad = $this->tour_model->verificar_cupo_horas_tour($tour,$date,$hora);
			echo json_encode($consultar_disponibilidad);

		}
		public function ver_reserva($id){
		$id = base64_decode($id);

		//Menu
				$data['menu_home'] = $this->menu_model->menu_items('1');
		//Fin menu




			if(isset($this->session->userdata['logged_in'])){
						$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
						$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

						if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

								$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

						}else{

								 $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
						}


				}
		$data['reserva'] = $this->tour_model->ver_reserva($id);
		$data['info'] = $this->tour_model->tours_por_id($data['reserva'][0]->id_tour);

		$this->load->view('detalles_reserva',$data);

		}

		public function reservar_tour(){
      if(isset($this->session->userdata['logged_in'])){
        $data['datos_user'] = $this->login_model->read_user_information($this->session->userdata['logged_in']['email']);
      }




			$tour = base64_decode ($_GET['id']);
			$adultos = base64_decode ($_GET['a']);
			$ninos = base64_decode ($_GET['n']);
			$estudiantes = base64_decode ($_GET['e']);


			$menores = base64_decode ($_GET['m']);

			if($adultos!=="undefined"){
						$adultos = base64_decode ($_GET['a']);
			}else{
						$adultos = "0";
			}
			if($estudiantes!=="undefined"){
						$estudiantes = base64_decode ($_GET['e']);
			}else{
						$estudiantes = "0";
			}
			if($menores!=="undefined"){
						$menores = base64_decode ($_GET['m']);
			}else{
						$menores = "0";
			}

			if($ninos!=="undefined"){

						$ninos = base64_decode ($_GET['n']);

			}else{
						$ninos = "0";
			}
			$date = base64_decode ($_GET['fecha']);
			$hora = base64_decode ($_GET['time']);
			session_start();
			$consultar_disponibilidad = $this->tour_model->verificar_cupo_horas_tour($tour,$date,$hora);
			$cantidad_a_restar = ($adultos + $ninos + $estudiantes + $menores);

			$_SESSION['compra_actual'] = array(
				'tour_id'=>$tour,
				'fecha'=>$date,
				'hora'=>$hora,
				'adultos'=>$adultos,
				'ninos'=>$ninos,
				'estudiantes'=>$estudiantes,
				'menores'=>$menores,
			);




	$data['precios'] =  $this->tour_model->precios_tour($_SESSION['compra_actual']["tour_id"]);


	//Menu
			$data['menu_home'] = $this->menu_model->menu_items('1');
	//Fin menu
$data['tour']= $this->tour_model->tours_por_id($_SESSION['compra_actual']['tour_id']);
$data['fecha']= $_SESSION['compra_actual']['fecha'];
$data['hora']= $_SESSION['compra_actual']['hora'];

// var_dump($data['tour']);
$this->load->view('reservaciones_form',$data);


}


public function procesar_reserva(){
//var_dump($_POST);
  $tour = base64_decode ($_POST['id']);
  $adultos = base64_decode ($_POST['a']);
  $ninos = base64_decode ($_POST['n']);
  $estudiantes = base64_decode ($_POST['e']);
  $menores = base64_decode ($_POST['m']);
  $monto = base64_decode ($_POST['monto']);

  if($adultos!=="undefined"){
        $adultos = base64_decode ($_POST['a']);
  }else{
        $adultos = "0";
  }
  if($estudiantes!=="undefined"){
        $estudiantes = base64_decode ($_POST['e']);
  }else{
        $estudiantes = "0";
  }
  if($menores!=="undefined"){
        $menores = base64_decode ($_POST['m']);
  }else{
        $menores = "0";
  }

  if($ninos!=="undefined"){

        $ninos = base64_decode ($_POST['n']);

  }else{
        $ninos = "0";
  }
  $date = base64_decode ($_POST['fecha']);
  $hora = base64_decode ($_POST['time']);
  session_start();
  $consultar_disponibilidad = $this->tour_model->verificar_cupo_horas_tour($tour,$date,$hora);
  $cantidad_a_restar = ($adultos + $ninos + $estudiantes + $menores);

  $_SESSION['compra_actual'] = array(
    'tour_id'=>$tour,
    'fecha'=>$date,
    'hora'=>$hora,
    'adultos'=>$adultos,
    'ninos'=>$ninos,
    'estudiantes'=>$estudiantes,
    'menores'=>$menores,
  );

  if($consultar_disponibilidad[0]->cupos >=$cantidad_a_restar){
    $data = array(
        'cupos'=>($consultar_disponibilidad[0]->cupos - $cantidad_a_restar)
    );

     $update_cupo = $this->tour_model->update_cantidad_tour($consultar_disponibilidad[0]->id,$data);

     if($update_cupo){
       $idreserva = uniqid("GP-");
       $data = array(
        'id_tour'=> $tour,
        'idreservacion'=>$idreserva,
        'id_usuario_reg'=>'0',
        'adultos'=>$adultos,
        'ninos'=>$ninos,
        'estudiantes'=>$estudiantes,
        'menores'=>$menores,
        'fecha'=>date("Y-m-d"),
        'fecha_tour'=>date($date),
        'hora_tour'=>$hora,
        'estado'=>'0',
        'nombre'=>$_POST['nombre'],
        'apellidos'=>$_POST['apellidos'],
        'email'=>$_POST['email'],
        'pais'=>'CRC',
        'telefono'=>$_POST['telefono'],
        'monto'=>$monto,
      );

      $t = $this->tour_model->tours_por_id($tour);
      $op = $this->tour_model->datos_operadora($t[0]->id_usuario);

      $reservar = $this->tour_model->reservar($data);




          // $mensaje='';
          // $mensaje.= '<p align="left" style="display:inline-block;">'.'Tour:'.'</p><p align="right" style="display:inline-block;float: right;">'.$t[0]->nombre.'</p><br>';
          // $mensaje.= '<p align="left" style="display:inline-block">'.'Tour operadora: '.'</p><p align="right" style="display:inline-block;float:right;">'.$op[0]->nombre.'</p><br>';
          // $mensaje.= '<p align="left" style="display:inline-block">'.'Código de reserva: '.'</p><p align="right" style="display:inline-block;float:right;">'.$idreserva.'</p><br>';
          // $mensaje.= '<center><h3><strong>'.'Datos del tour:'.'</strong></h3></center><br>';
          //
          // if ($adultos != 0) {
          //     $mensaje.= '<p align="left" style="display:inline-block">'.'Adultos:'.'</p><p align="right" style="display:inline-block;float:right;">'.$adultos.'</p><br>';
          // }
          //
          // if ($ninos != 0) {
          //       $mensaje.= '<p align="left" style="display:inline-block">'.'Niños:'.'</p><p align="right" style="display:inline-block;float:right;">'.$ninos.'</p><br>';
          // }
          //
          // if ($estudiantes != 0) {
          //       $mensaje.= '<p align="left" style="display:inline-block">'.'Niños:'.'</p><p align="right" style="display:inline-block;float:right;">'.$estudiantes.'</p><br>';
          // }
          //
          // if ($menores != 0) {
          //       $mensaje.= '<p align="left" style="display:inline-block">'.'Niños:'.'</p><p align="right" style="display:inline-block;float:right;">'.$menores.'</p><br>';
          // }
          //
          // $mensaje.= '<p align="left" style="display:inline-block">'.'Fecha de reserva:'.'</p><p align="right" style="display:inline-block;float:right;">'.date("d-m-Y").'</p><br>';
          // $mensaje.= '<p align="left" style="display:inline-block">'.'Fecha y hora del tour:'.'</p><p align="right" style="display:inline-block;float:right;">'.$date.' '.$hora.'</p><br>';
          // $mensaje.= '<center><h3><strong>'.'Datos del cliente'.'</strong></h3></center><br>';
          // $mensaje.= '<p align="left" style="display:inline-block">'.'Nombre:'.'</p><p align="right" style="display:inline-block;float:right;">'.$_POST['nombre'].','.$_POST['apellidos'].'</p><br>';
          // $mensaje.= '<p align="left" style="display:inline-block">'.'Email:'.'</p><p align="right" style="display:inline-block;float:right;">'.$_POST['email'].'</p><br>';
          // $mensaje.= '<p align="left" style="display:inline-block">'.'País:'.'</p><p align="right" style="display:inline-block;float:right;">'.$_POST['pais'].'</p><br>';
          // $mensaje.= '<p align="left" style="display:inline-block"><strong>'.'Total de la factura: </p><p align="right" style="display:inline-block;float:right;">$'.$monto.'</p></strong>';



        $data['nombre_to'] = $t[0]->nombre_to;
        $data['imagen_tour'] = $t[0]->imagen;
        $data['telefono_tour'] = $_POST['telefono'];




        $to = 'info@goplaya.cr,'.$_POST['email'].', '.$op[0]->email;
        //$to = 'julian@pixelcr.com';
        $asunto = "Reservación tour";
        $mensaje = $data;




        if( $_SESSION['idioma']   == 'es' ){
              $data['nombre_tour'] = $t[0]->nombre;
              $plantilla = "reservacion2";
        }else if( $_SESSION['idioma']   == 'en' ){
              $data['nombre_tour'] = $t[0]->nombre_eng;
              $plantilla = "reservacion3";
        }


        // var_dump($_SESSION['idioma']);
        // var_dump($plantilla);
        // exit;



        $enviar_email = $this->email_model->enviar_email2($to,$asunto,$mensaje,$plantilla);







      if($reservar){



       echo '<script>window.location="'.base_url().'tours/ver_reserva/'.base64_encode ($idreserva).'"</script>';
    //  var_dump($mensaje);
    // echo 'reservado';
      }
      else{

        //redireccionar email
        echo 'no reservado';

      }

     }else{
       //redireccionar email
      echo 'no reservado';
     }

}else{

}



}


    public function update_imagen_tour(){


      if(isset($this->session->userdata['logged_in'])){


        $data_img = $_POST['image'];
        $uploadData['id'] = $_POST['xc'];
        list($type, $data_img) = explode(';', $data_img);
				list(, $data_img)      = explode(',', $data_img);

				$data_img = base64_decode($data_img);
				$imageName = time().'.jpg';
				file_put_contents('archivos/'.$imageName, $data_img);

       $uploadData['foto'] = $imageName;


        $insert = $this->tour_model->update_tour_image($uploadData);


            if($insert){
              $response = array(
                'status'=>'1',
                'row'=>$uploadData
              );
            }else{
              $response = array(
                'status'=>'2',
                'row'=>$uploadData
              );
            }

            echo json_encode($response);



            }else{


                echo '<script>window.location.replace("'.base_url().'");</script>';


            }




    }


        // public function agregar_item_galeria(){
        // if(isset($this->session->userdata['logged_in'])){
        //
        //
        //   $data_img = $_POST['image'];
        //
        //     $uploadData['idtour'] = $_POST['tourid'];
        //   list($type, $data_img) = explode(';', $data_img);
        //   list(, $data_img)      = explode(',', $data_img);
        //
        //   $data_img = base64_decode($data_img);
        //   $imageName = time().'.jpg';
        //   file_put_contents('archivos/'.$imageName, $data_img);
        //
        //  $uploadData['foto'] = $imageName;
        //
        //  var_dump($uploadData);
        //
        //   $insert = $this->tour_model->insertar_item_galeria($uploadData);
        //
        //
        //       if($insert){
        //         $response = array(
        //           'status'=>'1',
        //           'row'=>$uploadData
        //         );
        //       }else{
        //         $response = array(
        //           'status'=>'2',
        //           'row'=>$uploadData
        //         );
        //       }
        //
        //       echo json_encode($response);
        //
        //
        //
        //
        // }else{
        // echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
        // }
        // }






    public function agregar_item_galeria(){
    if(isset($this->session->userdata['logged_in'])){


      // var_dump($_FILES['file']);
      //    var_dump($_POST);

    $tipo = 1;
    $playa = $_POST['tourid'];

    if($tipo == 1){
    $this->load->helper('form');

    // retrieve the number of images uploaded;
    $number_of_files = sizeof($_FILES['file']['tmp_name']);
    // considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
    $files = $_FILES['file'];
    $errors = array();
  // var_dump($_FILES);

  $pathto= './archivos/';
$this->load->library('upload');

  for ($i = 0; $i < $number_of_files; $i++) {


      $config['upload_path'] = FCPATH . 'archivos/';
      // also, we make sure we allow only certain type of images
      $config['allowed_types'] = 'gif|jpg|png|jpeg';
  $_FILES['file']['name'] = $files['name'];
  $_FILES['file']['type'] = $files['type'];
  $_FILES['file']['tmp_name'] = $files['tmp_name'];
  $_FILES['file']['error'] = $files['error'];
  $_FILES['file']['size'] = $files['size'];

$this->upload->initialize($config);



$image_gal = uniqid().'_'.$_FILES['file']['name'];
$up_image_gal = $pathto.$image_gal;



   move_uploaded_file( $_FILES['file']['tmp_name'],$up_image_gal) or
            die( "Could not copy file!");

                   $data = array(
                   'idtour'=>$playa,
                   'url'=>$image_gal ,
                   );
                   $insert = $this->tour_model->insertar_item_galeria($data);
}

if($insert){
  if( $_SESSION['idioma']   == 'es' ){

    $response = array(
      'status'=>'1',
      'titulo'=>'Completado.',
      'mensaje'=>'Imagen agregada correctamente.',
      'url_img'=>$image_gal
    );

    echo json_encode($response);


  }else if( $_SESSION['idioma']   == 'en' ){

    $response = array(
      'status'=>'1',
      'titulo'=>'Completed.',
      'mensaje'=>'Image added .',
          'url_img'=>$image_gal
    );

    echo json_encode($response);

  }
}
// var_dump($errors);
    // first make sure that there is no error in uploading the files
    // for($i=0;$i<$number_of_files;$i++)
    // {
    // if($_FILES['file']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['file']['name'][$i];
    // }

    // if(sizeof($errors)==0){
    // // now, taking into account that there can be more than one file, for each file we will have to do the upload
    // // we first load the upload library
    // $this->load->library('upload');
    // // next we pass the upload path for the images
    // $config['upload_path'] = FCPATH . 'archivos/';
    // // also, we make sure we allow only certain type of images
    // $config['allowed_types'] = 'gif|jpg|png|jpeg';
    // for ($i = 0; $i < $number_of_files; $i++) {
    // $_FILES['file']['name'] = $files['name'][$i];
    // $_FILES['file']['type'] = $files['type'][$i];
    // $_FILES['file']['tmp_name'] = $files['tmp_name'][$i];
    // $_FILES['file']['error'] = $files['error'][$i];
    // $_FILES['file']['size'] = $files['size'][$i];
    // //now we initialize the upload library
    // $this->upload->initialize($config);
    //
    //
    //    $path=$_FILES['name'];
    //
    //    var_dump($_FILES);
    //    $pathto=FCPATH . 'archivos/';
    //
    //    move_uploaded_file( $_FILES[$key]['tmp_name'],$pathto) or
    //             die( "Could not copy file!");
    //  $value = base_url().$pathto;
    //        // $data['meta_value'] = $value;
    //        // $update = $this->core_model->update_meta($_POST['id'],$key,$data);
    //
    //        $data = array(
    //        'idtour'=>$playa,
    //        'url'=>$value ,
    //        );
    //        $insert = $this->tour_model->insertar_item_galeria($data);
    //
    // }
    // }
//     else
//     {
//     print_r($errors);
//     }
//
//
//     }else if($tipo ==2){
//
//     $url = $_POST['youtube'];
//     }else{
//
//     echo '<script>window.location="'.base_url().'index.php/tour_operadora/configurar_tour/'.$playa.'?e=2'.'"</script>';
//     }
//
//
//
//
//     if($tipo == 1){
//
//
//
//
//
//     }else if($tipo ==2){
//
//     $data = array(
//     'idplaya'=>$playa,
//     'url'=>$url ,
//     'tipo'=>$tipo,
//     );
//
//     $insert = $this->provincias_model->insertar_item_galeria($data);
//
//     }else{
//
//     echo '<script>window.location="'.base_url().'index.php/tour_operadora/configurar_tour/'.$playa.'?e=2'.'"</script>';
//     }
//
//
//
//     if($insert){
//
//     // echo '<script>window.location="'.base_url().'index.php/tour_operadora/configurar_tour/'.$playa.'?e=1'.'"</script>';
//      echo "1";
//     var_dump($data);
//
//     }else{
//     // echo '<script>window.location="'.base_url().'index.php/tour_operadora/configurar_tour/'.$playa.'?e=2'.'"</script>';
//      echo "2";
// var_dump($data);
//
//     }


    }else{
    echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
    }

    }





}


public function update_caption_tour(){
if(isset($this->session->userdata['logged_in'])){
$id = $_POST['id'];

$data = array(
'caption' => $_POST['caption']
);

$update= $this->tour_model->update_caption_tour($id,$data);



if($update){

echo 'si';
}else{
echo 'no';

}

}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}



}
