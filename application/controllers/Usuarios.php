<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	  
    public function __construct() {
        parent::__construct();


        $this->load->model('login_model');
        $this->load->model('provincias_model');
        $this->load->model('pagina_model');
        $this->load->model('slider_model');
        $this->load->model('modulos_model');
        $this->load->model('usuarios_model');
        $this->load->model('menu_model');
        $this->load->model('email_model');
    }

    
	public function index()
	{
           //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales
        
        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
    if(isset($this->session->userdata['logged_in'])){
        
          //Datos Usuario
             $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
            
                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){
                    
                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];
                   
                }else{
                    
                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
              //Fin datos Usuario  
        $data['usuarios'] = $this->usuarios_model->usuarios();   
        $data['usuarios2'] = $this->usuarios_model->usuarios2();   
        $this->load->view('ad_usuarios',$data);
        
    }else{
        
    } 
	
	}
    
    
    public function bloquear_usuario(){
           //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales
        
        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        if(isset($this->session->userdata['logged_in'])){

            $id = $_GET['usuario'];
            
            $data = array(
                'estado' => '2'
            );
            
            $update = $this->usuarios_model->bloquear_usuario($id,$data);
            
            if($update){
                echo '<script>window.location="'.base_url().'usuarios/?e=1"</script>';
            }else{
                echo '<script>window.location="'.base_url().'usuarios/?e=2"</script>';
            }

        }else{

        } 
	
	}
    
     public function desbloquear_usuario(){
            //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales
        
        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
         //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        if(isset($this->session->userdata['logged_in'])){

            $id = $_GET['usuario'];
            
            $data = array(
                'estado' => '1'
            );
            
            $update = $this->usuarios_model->desbloquear_usuario($id,$data);
            
            if($update){
                echo '<script>window.location="'.base_url().'usuarios/?e=1"</script>';
            }else{
                echo '<script>window.location="'.base_url().'usuarios/?e=2"</script>';
            }

        }else{

        } 
	
	}
    
    
    
     public function agregar_admin(){
           //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales
        
        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        if(isset($this->session->userdata['logged_in'])){
            if( $data['admin']){
                
                var_dump($_POST);
            $id = $_POST['usuario'];
            
            $data = array(
                'idusuario' => $id
            );
            
            $insert = $this->usuarios_model->agregar_admin($data);
            
            if($insert){
                echo '<script>window.location="'.base_url().'usuarios/?e=1"</script>';
            }else{
                echo '<script>window.location="'.base_url().'usuarios/?e=2"</script>';
            }

                
                
            }

        }else{

        } 
	
	}
    
    
    
       public function eliminar_admin($id){
           //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales
        
        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        if(isset($this->session->userdata['logged_in'])){
            if( $data['admin']){
                
             
            
            
            $insert = $this->usuarios_model->eliminar_admin($id);
            
            if($insert){
                echo '<script>window.location="'.base_url().'usuarios/?e=1"</script>';
            }else{
                echo '<script>window.location="'.base_url().'usuarios/?e=2"</script>';
            }

                
                
            }

        }else{

        } 
	
	}
    
    
}
