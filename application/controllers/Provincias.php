<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provincias extends CI_Controller {


public function __construct() {
parent::__construct();


$this->load->model('login_model');
$this->load->model('provincias_model');
$this->load->model('menu_model');
}




public function index(){
if(isset($this->session->userdata['logged_in'])){



}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}

public function descp_carac(){


$caracteristica = $_POST['c'];
$playa = $_POST['p'];


$consulta = $this->provincias_model->descp_carac($caracteristica,$playa);

if($consulta){

$datos = $this->provincias_model->descp_carac2($caracteristica,$playa);

echo json_encode($datos);
}else{

echo '0';

}
}

public function detalles_item(){


$caracteristica = $_POST['c'];
$playa = $_POST['p'];


$datos = $this->provincias_model->descp_carac2($caracteristica,$playa);


echo json_encode($datos);
}

public function comprobar_permalink(){

$s = $_POST['slug'];

$slug = $this->provincias_model->comporobar_permalink($s);

if($slug){
echo '1';

}else{
echo '0';
}

}

public function descp_carac_add(){

$data = array(
'idplaya'=>$_POST['playa'],
'idcaracteristica'=>$_POST['caracteristica'],
'detalle'=>$_POST['detalle_carac'],
'detalle_en'=>$_POST['detalle_carac_en']
);


$insert = $this->provincias_model->descp_carac_add($data);

if($insert){

echo  '1';
}else{

echo '0';

}


}

public function descp_carac_update(){

$data = array(
'detalle'=>$_POST['detalle_carac'],
'detalle_en'=>$_POST['detalle_carac_en']
);


$insert = $this->provincias_model->descp_carac_update($_POST['playa'],$_POST['caracteristica'],$data);

if($insert){

echo  '1';
}else{

echo '0';

}


}


public function descp_carac_delete(){
$insert = $this->provincias_model->descp_carac_delete($_POST['playa'],$_POST['caracteristica']);

if($insert){

echo  '1';
}else{

echo '0';

}


}


public function insertar_provincia(){
if(isset($this->session->userdata['logged_in'])){

$data = array(
'nombre' => $this->input->post('nombre_provincia'),
'latitud' =>  $this->input->post('latitud_provincia'),
'longitud' =>  $this->input->post('longitud_provincia')
);

$insert = $this->provincias_model->insertar_provincia($data);

if($insert){

echo '<script>window.location="'.base_url().'index.php/dashboard?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard?e=2'.'"</script>';

}



}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}

public function eliminar_provincia($id){

if(isset($this->session->userdata['logged_in'])){



$delete = $this->provincias_model->eliminar_provincia($id);

if($delete){

echo '<script>window.location="'.base_url().'index.php/dashboard?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard?e=2'.'"</script>';

}



}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}



public function editar_region($id){
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu

if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario



$data['region'] = $this->provincias_model->region_por_id($id);

$this->load->view('editar_region',$data);


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}


public function editar_provincia($id){
//Menu
$data['menu_home'] = $this->menu_model->menu_items('1');
//Fin menu

if(isset($this->session->userdata['logged_in'])){

//check administrador

$admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
//fin check adminsitrador
//Datos Usuario
$data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
$data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

$data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

}else{

$data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
}
//Fin datos Usuario



$data['region'] = $this->provincias_model->provincia_por_id($id);

$this->load->view('editar_provincia',$data);


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}





public function insertar_region(){
if(isset($this->session->userdata['logged_in'])){

$data = array(
'nombre' => $this->input->post('nombre_region')
);

$insert = $this->provincias_model->insertar_region($data);

if($insert){

echo '<script>window.location="'.base_url().'index.php/dashboard?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard?e=2'.'"</script>';

}



}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}



public function eliminar_region($id){

if(isset($this->session->userdata['logged_in'])){



$delete = $this->provincias_model->eliminar_region($id);

if($delete){

echo '<script>window.location="'.base_url().'index.php/dashboard?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard?e=2'.'"</script>';

}



}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}


public function insertar_playa(){
if(isset($this->session->userdata['logged_in'])){

$data = array(
'nombre' => $this->input->post('nombre_playa'),
'descripcion' => $this->input->post('descripcion_playa'),
'latitud' =>  $this->input->post('latitud_playa'),
'longitud' =>  $this->input->post('longitud_playa'),
'idprovincia' =>  $this->input->post('provincia_playa'),
'idregion' =>  $this->input->post('region_playa')
);

$insert = $this->provincias_model->insertar_playa($data);

if($insert){

echo '<script>window.location="'.base_url().'index.php/dashboard?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard?e=2'.'"</script>';

}



}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}


public function agregar_item_galeria(){
if(isset($this->session->userdata['logged_in'])){


$tipo = $_POST['tipo_item'];
$playa = $_POST['playaid'];

if($tipo == 1){
$this->load->helper('form');

// retrieve the number of images uploaded;
$number_of_files = sizeof($_FILES['uploadedimages']['tmp_name']);
// considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
$files = $_FILES['uploadedimages'];
$errors = array();

// first make sure that there is no error in uploading the files
for($i=0;$i<$number_of_files;$i++)
{
if($_FILES['uploadedimages']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimages']['name'][$i];
}

if(sizeof($errors)==0){
// now, taking into account that there can be more than one file, for each file we will have to do the upload
// we first load the upload library
$this->load->library('upload');
// next we pass the upload path for the images
$config['upload_path'] = FCPATH . 'archivos/';
// also, we make sure we allow only certain type of images
$config['allowed_types'] = 'gif|jpg|png';
for ($i = 0; $i < $number_of_files; $i++) {
$_FILES['uploadedimage']['name'] = $files['name'][$i];
$_FILES['uploadedimage']['type'] = $files['type'][$i];
$_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
$_FILES['uploadedimage']['error'] = $files['error'][$i];
$_FILES['uploadedimage']['size'] = $files['size'][$i];
//now we initialize the upload library
$this->upload->initialize($config);
// we retrieve the number of files that were uploaded
if ($this->upload->do_upload('uploadedimage'))
{
$data['uploads'][$i] = $this->upload->data();

$url = $_FILES['uploadedimage']['name'];

$data = array(
'idplaya'=>$playa,
'url'=>$url ,
'tipo'=>$tipo,
);
$insert = $this->provincias_model->insertar_item_galeria($data);
}
else
{
$data['upload_errors'][$i] = $this->upload->display_errors();
}
}
}
else
{
print_r($errors);
}


}else if($tipo ==2){

$url = $_POST['youtube'];
}else{

echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=2'.'"</script>';
}




if($tipo == 1){





}else if($tipo ==2){

$data = array(
'idplaya'=>$playa,
'url'=>$url ,
'tipo'=>$tipo,
);

$insert = $this->provincias_model->insertar_item_galeria($data);

}else{

echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=2'.'"</script>';
}



if($insert){

echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=2'.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}


public function agregar_item_ads(){

if(isset($this->session->userdata['logged_in'])){



$tipo = $_POST['tipo_item'];
$playa = $_POST['playaid'];
$url2 = $_POST['url'];
$posicion = $_POST['posicion'];


if($tipo == 1){
$this->load->helper('form');

// retrieve the number of images uploaded;
$number_of_files = sizeof($_FILES['uploadedimages']['tmp_name']);
// considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
$files = $_FILES['uploadedimages'];
$errors = array();



// first make sure that there is no error in uploading the files
for($i=0;$i<$number_of_files;$i++)
{
if($_FILES['uploadedimages']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimages']['name'][$i];
}

if(sizeof($errors)==0){
// now, taking into account that there can be more than one file, for each file we will have to do the upload
// we first load the upload library
$this->load->library('upload');
// next we pass the upload path for the images
$config['upload_path'] = FCPATH . 'archivos/';
// also, we make sure we allow only certain type of images
$config['allowed_types'] = 'gif|jpg|png';

for ($i = 0; $i < $number_of_files; $i++) {
$_FILES['uploadedimage']['name'] = $files['name'][$i];
$_FILES['uploadedimage']['type'] = $files['type'][$i];
$_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
$_FILES['uploadedimage']['error'] = $files['error'][$i];
$_FILES['uploadedimage']['size'] = $files['size'][$i];
//now we initialize the upload library
$this->upload->initialize($config);
// we retrieve the number of files that were uploaded
if ($this->upload->do_upload('uploadedimage'))
{
$data['uploads'][$i] = $this->upload->data();


$url = $_FILES['uploadedimage']['name'];

$data = array(
'idplaya'=>$playa,
'url'=>$url2 ,
'banner'=>$url ,
'tipo'=>$tipo,
'posicion'=>$posicion,
);

$insert = $this->provincias_model->insertar_item_ads($data);

} else {

$data['upload_errors'][$i] = $this->upload->display_errors();

}
}
}
else
{
print_r($errors);

}


}else{

echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=2'.'"</script>';
}




if($tipo == 1){





}else if($tipo ==2){

$data = array(
'idplaya'=>$playa,
'url'=>$url ,
'tipo'=>$tipo,
);

$insert = $this->provincias_model->insertar_item_ads($data);

}else{

echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=2'.'"</script>';
}



if($insert){

echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=2'.'"</script>';

}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}




public function eliminar_item_ads(){
$id = $_GET['id'];
$playa = $_GET['playa'];

if(isset($this->session->userdata['logged_in'])){



$insert = $this->provincias_model->eliminar_item_ads($id);

if($insert){

echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=2'.'"</script>';



}

}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}




public function eliminar_item_galeria(){
$id = $_GET['id'];
$playa = $_GET['playa'];

if(isset($this->session->userdata['logged_in'])){



$insert = $this->provincias_model->eliminar_item_galeria($id);

if($insert){

echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=2'.'"</script>';



}

}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}


public function eliminar_item_galeria_tour_tour(){
$id = $_GET['id'];
$playa = $_GET['playa'];
if(isset($this->session->userdata['logged_in'])){



$insert = $this->provincias_model->eliminar_item_galeria_tour($id);

if($insert){

echo '<script>window.location="'.base_url().'index.php/tour_operadora/configurar_tour/'.$playa.'?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/tour_operadora/configurar_tour/'.$playa.'?e=2'.'"</script>';



}

}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}


public function insertar_caracteristica(){
if(isset($this->session->userdata['logged_in'])){

$data = array(
'detalle' => $this->input->post('nombre_caracteristica'),
'actividad_padre' => $this->input->post('padre'),
'icono'=>$_POST['icono'],
'simple' => $this->input->post('simple')
);

$insert = $this->provincias_model->insertar_caracteristica($data);

if($insert){

echo '<script>window.location="'.base_url().'index.php/dashboard?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard?e=2'.'"</script>';



var_dump($_POST);

}

}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}

public function update_caption(){
if(isset($this->session->userdata['logged_in'])){
$id = $_POST['id'];

$data = array(
'caption' => $_POST['caption']
);

$update= $this->provincias_model->update_caption($id,$data);



if($update){

echo 'si';
}else{
echo 'no';

}

}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}



public function update_position(){
if(isset($this->session->userdata['logged_in'])){
$id = $_POST['id'];

$data = array(
'posicion' => $_POST['position']
);


var_dump($id);

$update= $this->provincias_model->update_position($id,$data);



if($update){

echo 'si';
}else{
echo 'no';

}

}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}





public function insertar_caracteristica_padre(){
if(isset($this->session->userdata['logged_in'])){

$data = array(
'detalle' => $this->input->post('nombre_caracteristica'),
'oculto' => $this->input->post('oculto')
);

$insert = $this->provincias_model->insertar_caracteristica_padre($data);

if($insert){

echo '<script>window.location="'.base_url().'index.php/dashboard?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard?e=2'.'"</script>';



var_dump($_POST);

}

}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}


public function eliminar_playa($id){

if(isset($this->session->userdata['logged_in'])){



$delete = $this->provincias_model->eliminar_playa($id);

if($delete){

echo '<script>window.location="'.base_url().'index.php/dashboard?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard?e=2'.'"</script>';

}



}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}


public function eliminar_caracteristica($id){

if(isset($this->session->userdata['logged_in'])){



$delete = $this->provincias_model->eliminar_caracteristica($id);

if($delete){

echo '<script>window.location="'.base_url().'index.php/dashboard?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard?e=2'.'"</script>';

}



}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}

public function eliminar_caracteristica_padre($id){

if(isset($this->session->userdata['logged_in'])){



$delete = $this->provincias_model->eliminar_caracteristica_padre($id);

if($delete){

echo '<script>window.location="'.base_url().'index.php/dashboard?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard?e=2'.'"</script>';

}



}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}




public function subir_imagenes_galeria(){
$data = array();

if($this->input->post())
{

$number_of_files = sizeof($_FILES['uploadedimages']['tmp_name']);

$files = $_FILES['uploadedimages'];
$errors = array();


for($i=0;$i<$number_of_files;$i++)
{
if($_FILES['uploadedimages']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimages']['name'][$i];
}
if(sizeof($errors)==0)
{

$this->load->library('upload');

$config['upload_path'] = FCPATH . 'upload/';
$config['allowed_types'] = 'gif|jpg|png';
for ($i = 0; $i < $number_of_files; $i++) {
$_FILES['uploadedimage']['name'] = $files['name'][$i];
$_FILES['uploadedimage']['type'] = $files['type'][$i];
$_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
$_FILES['uploadedimage']['error'] = $files['error'][$i];
$_FILES['uploadedimage']['size'] = $files['size'][$i];
$this->upload->initialize($config);

if ($this->upload->do_upload('uploadedimage'))
{
$data['uploads'][$i] = $this->upload->data();
}
else
{
$data['upload_errors'][$i] = $this->upload->display_errors();
}
}
}
else
{
print_r($errors);
}
echo '<pre>';
print_r($data);
echo '</pre>';
}
else
{
$this->load->view('upload_form', $data);
}
}


public function agregar_item_actividad(){
if(isset($this->session->userdata['logged_in'])){



foreach($_POST['caracteristica'] as $car){



$data = array(
'id_playa'=> $_POST['playaid'],
'id_caracteristica'=> $car
);
$insert = $this->provincias_model->agregar_item_actividad($data);


}





if($insert){

echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$_POST['playaid'].'?e=1'.'"</script>';

}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$_POST['playaid'].'?e=2'.'"</script>';
}


}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}
}


public function eliminar_item_caracteristica(){
$id = $_GET['id'];
$playa = $_GET['playa'];

if(isset($this->session->userdata['logged_in'])){



$insert = $this->provincias_model->eliminar_item_caracteristica($id);

if($insert){

echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=1'.'"</script>';
}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/editar_playa/'.$playa.'?e=2'.'"</script>';



}

}else{
echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
}

}




}
