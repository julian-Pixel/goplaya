<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Playa extends CI_Controller {


    public function __construct() {
        parent::__construct();


        $this->load->model('login_model');
        $this->load->model('provincias_model');
        $this->load->model('playa_model');
        $this->load->model('menu_model');
          $this->load->model('tour_model');

		$this->load->model('publicidad_model');
    }


	public function index()
	{

	}

    public function detalle($slug){

     $in = is_numeric($slug);



        if($in){

            $id = $slug;

        }else{
            $playa_id = $this->playa_model->informacion_general2($slug);


            $id = $playa_id[0]->idplaya;

        }



          $data['actividades'] = $this->provincias_model->actividades_por_playa($id);

         //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        //Lugares
                $data['provincias'] = $this->provincias_model->todas_las_provincias();
                $data['regiones'] = $this->provincias_model->todas_las_regiones();
                $data['playas'] = $this->provincias_model->todas_las_playas();
                $data['caracteristicas'] = $this->provincias_model->todas_las_caracteristicas_simples();
            // Fin lugares
        	if(isset($this->session->userdata['logged_in'])){
       $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
       $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }


            }
           $data['slug'] = $slug;
           $data['id'] = $id;
        //Detalles playa
        $data['galeria'] = $this->playa_model->galeria($id);
				// $data['banners_playa'] = $this->playa_model->ads($id);

        $data['estrellas'] = $this->playa_model->estrellas($id);
        $data['info'] = $this->playa_model->informacion_general($id);


        //Información de los tours relacionados a esa playa
        $data['tours'] = $this->tour_model->tours_disponibles_por_playa($id);


        $data['region'] = $this->provincias_model->info_region($data['info'][0]->idregion);
        $c_datos = $this->provincias_model->c_datos($id);

        $pila = array();
        foreach($c_datos as $c){

            array_push($pila, $c->idcaracteristica);
        }


	//ADS



	//	var_dump($data['banners_playa'] );
					 //  exit;
	$data['banners_playa'] = $this->publicidad_model->banners_id($id);



	//FIN ADS



        $data['portada'] = $this->playa_model->portada($id);
        $data['c_datos'] = $pila;


        $this->load->view('playa_detalle',$data);
    }


      function ordenar_item(){
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
       if(isset($this->session->userdata['logged_in'])){

         $items = $_POST['item'];
        $orden = 0;

           foreach($items as $item){

               $data=array(
               'id'=> $item,
                'orden'=>$orden
               );

               $insert = $this->playa_model->ordenar_items($data);
               if($insert){
                   echo 'item actualizado';
               }else{

                   'problema';
               }

              $orden ++;
           }


        }else{

        }

}
 function info_basica(){


   $slug = $_POST['id'];


      $in = is_numeric($slug);

        if($in){

            $id = $slug;

        }else{
            $playa_id = $this->playa_model->informacion_general2($slug);

            $id = $playa_id[0]->idplaya;

        }



          $data['actividades'] = $this->provincias_model->actividades_por_playa($id);


        //Lugares
                $data['provincias'] = $this->provincias_model->todas_las_provincias();
                $data['regiones'] = $this->provincias_model->todas_las_regiones();
                $data['playas'] = $this->provincias_model->todas_las_playas();
                $data['caracteristicas'] = $this->provincias_model->todas_las_caracteristicas_simples();
            // Fin lugares
        	if(isset($this->session->userdata['logged_in'])){
       $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
       $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }


            }
           $data['slug'] = $slug;
        //Detalles playa
        $data['galeria'] = $this->playa_model->galeria($id);
        $data['estrellas'] = $this->playa_model->estrellas($id);
        $data['info'] = $this->playa_model->informacion_general($id);
        $data['region'] = $this->provincias_model->info_region($data['info'][0]->idregion);
        $data['portada'] = $this->playa_model->portada($id);


        $this->load->view('playa_detalle2',$data);






 }

}
