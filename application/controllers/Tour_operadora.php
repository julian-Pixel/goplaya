<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tour_operadora extends CI_Controller {


    public function __construct() {
        parent::__construct();


        $this->load->model('login_model');
        $this->load->model('provincias_model');
        $this->load->model('tour_model');
        $this->load->model('menu_model');
        $this->load->model('email_model');
    }


	public function index()
	{


    if(isset($this->session->userdata['logged_in'])){
          $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
$data['admin'] = $admin;
    }

        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu



        $data['']='';
        if(isset($this->session->userdata['logged_in'])){
       $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
       $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }



            if($data['rol_user'][0]->rol == 2){
             $data['have_operadora'] = ($this->tour_model->check_operadora($this->session->userdata['logged_in']['id']));
             $data['datos_operadora'] = ($this->tour_model->datos_operadora($this->session->userdata['logged_in']['id']));
             $data['playas'] = $this->provincias_model->todas_las_playas();
             $data['mis_tours'] = $this->tour_model->mis_tours($this->session->userdata['logged_in']['id']);
             $data['mis_reservas'] = $this->tour_model->reservaciones_tour($this->session->userdata['logged_in']['id']);
               $data['videos_tours'] = $this->tour_model->todos_los_videos();
             $this->load->view('to_admin.php',$data);

         }else{

            echo '<script>window.location="'.base_url().''.'"</script>';

         }


            } else{

            echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';

        }




	}

  public function ingresar($id){

    //Menu
    $data['menu_home'] = $this->menu_model->menu_items('1');
    //Fin menu

    $data['']='';
    $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
    $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

    if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){
        $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];
    }else{
        $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
    }

    $id = base64_decode($id);

    $data['usr'] = $this->tour_model->datos_operadora($id);


    $this->load->view('to_success',$data);
  }

public function pagado(){


  if(isset($this->session->userdata['logged_in'])){

    $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
    $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
if($data['rol_user'][0]->rol == 1){


  $id = $_GET['id'];
  $estado = $_GET['e'];


    $data = array(

      'estado'=> $estado,
      'comision'=> $estado


    );



  $update = $this->tour_model->comision($id,$data);


  if($update){
  echo '<script>window.location="'.base_url().'index.php/dashboard/aprobar_tours?e=1'.'"</script>';
  }else{
  echo '<script>window.location="'.base_url().'index.php/dashboard/aprobar_tours?e=2'.'"</script>';
  }


}else{

var_dump($data['rol_user'][0]->rol);

}

  }




}
  public function registrar(){

    $data = array(
      'email'=>$_POST['email2'],
       'contrasena'=>md5($_POST['pass']),
      // 'contrasena'=>$_POST['pass'],
      'nombre'=>$_POST['nombre'],
      'fecha_registro'=>date('Y-m-d H:m:s'),
      'rol'=>'2',
      'estado'=>'1',
    );
    $result = $this->login_model->registration_insert($data);


    $usuario = $this->login_model->read_user_information($_POST['email2']);
    if ($result == TRUE) {
        $data = array(
          'nombre'=>$_POST['marca'],
          'razon'=>$_POST['razon'],
          'cedula'=>$_POST['cedula'],
          'telefono1'=>$_POST['telefono'],
          'cedula'=>$_POST['cedula'],
          'tipo_cedula'=>$_POST['tipo_cedula'],
          'codigo'=>uniqid(),
          'sitio_web'=>$_POST['site'],
          'direccion'=>$_POST['direccion'],
          'email'=>$_POST['email2'],
          'estado'=>'1',
          'id_usuario'=>$usuario[0]->idusuario,
          'propietario_cuenta'=>$_POST['propietario_cuenta'],
          'cedula_cuenta'=>$_POST['cedula_cuenta'],
          'cuenta'=>$_POST['cuenta'],
          'sinpe'=>$_POST['sinpe'],
        );


          $result = $this->tour_model->touroperadora_insert($data);

            if ($result){

              $u = base64_encode($usuario[0]->idusuario);

              $daticos = array(
                  'status' => '1',
                  'codigo' => $u,
                  'titulo' => "Completado",
                  'mensaje' => "Usuario registrado",

              );


            } else {

              $daticos = array(
                  'status' => '2',
                  'codigo' => '',
                  'titulo' => "No Completado",
                  'mensaje' => "Usuario no registrado",

              );

            }

    } else {

      $daticos = array(
          'status' => '3',
          'codigo' => '',
          'titulo' => "No Completado",
          'mensaje' => "Usuario no registrado",

      );


    }

      echo json_encode($daticos);

  }





  public function modificar(){

    // $data = array(
    //   'email'=>$_POST['email2'],
    //   'contrasena'=>md5($_POST['pass']),
    //   // 'contrasena'=>$_POST['pass'],
    //   'nombre'=>$_POST['nombre'],
    //   'fecha_registro'=>date('Y-m-d H:m:s'),
    //   'rol'=>'2',
    //   'estado'=>'1',
    // );
    // $result = $this->login_model->registration_insert($data);


    // $usuario = $this->login_model->read_user_information($_POST['email2']);


        $id  = $_POST['id_update'];
        $data = array(
          'nombre'=>$_POST['nombre_update'],
          'cedula'=>$_POST['cedula_update'],
          'telefono1'=>$_POST['telefono1_update'],
          'telefono2'=>$_POST['telefono2_update'],

          'razon'=>$_POST['razon_update'],
          'sitio_web'=>$_POST['sitio_web_update'],
          'direccion'=>$_POST['direccion_update'],
          'estado'=>$_POST['estado_update'],
          'propietario_cuenta'=>$_POST['propietario_cuenta_update'],
          'cedula_cuenta'=>$_POST['cedula_cuenta_update'],
          'cuenta'=>$_POST['cuenta_update'],
          'sinpe'=>$_POST['sinpe_update'],
        );


          $result = $this->tour_model->touroperadora_update($data,$id);

            if ($result){

              // $u = base64_encode($usuario[0]->idusuario);

              $datos = array(
                  'status' => '1',
                  'codigo' => '',
                  'titulo' => "Completado",
                  'mensaje' => "Usuario modificado",

              );


            } else {

              $datos = array(
                  'status' => '2',
                  'codigo' => $id,
                  'titulo' => "No Completado",
                  'mensaje' => 'error',

              );

            }



      echo json_encode($datos);

  }




    public function configurar(){
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        $data['']='';
        	if(isset($this->session->userdata['logged_in'])){
       $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
       $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }


            }

         if($data['rol_user'][0]->rol == 2){


              $data = array(
                'nombre'=>$_POST['nombre'],
                'email'=>$_POST['email'],
                'telefono1'=>$_POST['telefono1'],
                'telefono2'=>$_POST['telefono2'],
                'fax'=>$_POST['fax'],
                'sitio_web'=>$_POST['website'],
                'direccion'=>$_POST['direccion'],
                'id_usuario'=>$this->session->userdata['logged_in']['id'],
                'estado'=>'1'

              );

                 $insert =  $this->tour_model->configurar_operadora($data);

             if($insert){
                  echo '<script>window.location="'.base_url().'index.php/tour_operadora/?e=1'.'"</script>';
             }else{
                  echo '<script>window.location="'.base_url().'index.php/tour_operadora/?e=2'.'"</script>';
             }

         }else{



         }

    }

    public function registrarme(){

      if(isset($this->session->userdata['logged_in'])){
          $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
$data['admin'] = $admin;
    }
    //Datos Generales
  $data['datos_generales'] = $this->email_model->datos_generales();
  $data['rutas_predisenadas'] = $this->provincias_model->rutas_predisenadas();

//Fin Datos Generales
    //Menu
        $data['menu_home'] = $this->menu_model->menu_items('1');
    //Fin menu
    $data['caracteristicas'] = $this->provincias_model->todas_las_caracteristicas_simples();
    if(isset($this->session->userdata['logged_in'])){
            $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

            if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

            }else{

                 $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
            }


        }


              $data['videos_tours'] = $this->tour_model->todos_los_videos();

        $this->load->view('registro_tour',$data);


    }


    public function agregar_tour(){
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu

          $data['']='';
        	if(isset($this->session->userdata['logged_in'])){
       $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
       $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }


            }




         if($data['rol_user'][0]->rol == 2){


              $data = array(
                'nombre'=>$_POST['nombre_es'],
                'nombre_eng'=>$_POST['nombre_en'],
                'descripcion'=>$_POST['desc_es'],
                'descripcion_eng'=>$_POST['desc_en'],
                'fecha_tour'=>$_POST['fecha'],
                'id_playa'=>$_POST['zona'],
                'precio'=>$_POST['precio'],
                'maximo_personas'=>$_POST['maximo'],
                'id_usuario'=>$this->session->userdata['logged_in']['id'],
                'estado'=>'2'

              );



                 $insert =  $this->tour_model->agregar_tour($data);

             if($insert){
                  echo '<script>window.location="'.base_url().'index.php/tour_operadora/?e=1'.'"</script>';
             }else{
                  //echo '<script>window.location="'.base_url().'index.php/tour_operadora/?e=2'.'"</script>';
             }

         }elseif($data['rol_user'][0]->rol == 1){

           $data = array(
             'nombre'=>$_POST['nombre_es'],
             'nombre_eng'=>$_POST['nombre_en'],
             'descripcion'=>$_POST['desc_es'],
             'descripcion_eng'=>$_POST['desc_en'],
             'fecha_tour'=>$_POST['fecha'],
             'id_playa'=>$_POST['zona'],
             'precio'=>$_POST['precio'],
             'maximo_personas'=>$_POST['maximo'],
             'id_usuario'=>$this->session->userdata['logged_in']['id'],
             'estado'=>'2'

           );



              $insert =  $this->tour_model->agregar_tour($data);




          if($insert){
               echo '<script>window.location="'.base_url().'index.php/dashboard/aprobar_tours?e=1'.'"</script>';
          }else{
              echo '<script>window.location="'.base_url().'index.php/dashboard/aprobar_tours?e=2'.'"</script>';
          }

        }else{

        }



    }

    public function eliminar_cupo(){

     if(isset($this->session->userdata['logged_in'])){

       $id = $_POST['id'];
       $playa = $_POST['tour'];
       if(isset($this->session->userdata['logged_in'])){

       $insert = $this->provincias_model->eliminar_cupo_tour($id);

       if($insert){
         // echo '<script>window.location="'.base_url().'index.php/tour_operadora/configurar_tour/'.$playa.'?e=1#cupo'.'"</script>';
         // var_dump($insert);
         if( $_SESSION['idioma']   == 'es' ){

           $response = array(
             'status'=>'1',
             'titulo'=>'Completado.',
             'mensaje'=>'Acción completada.'
           );

           echo json_encode($response);


         }else if( $_SESSION['idioma']   == 'en' ){

           $response = array(
             'status'=>'1',
             'titulo'=>'Completed.',
             'mensaje'=>'Action completed.'
           );

           echo json_encode($response);

         }
       }else{
         // echo '<script>window.location="'.base_url().'index.php/tour_operadora/configurar_tour/'.$playa.'?e=2#cupo'.'"</script>';
         //  var_dump($insert);
         if( $_SESSION['idioma']   == 'es' ){

           $response = array(
             'status'=>'1',
             'titulo'=>'No Completado.',
             'mensaje'=>'Acción no completada.'
           );

           echo json_encode($response);


         }else if( $_SESSION['idioma']   == 'en' ){

           $response = array(
             'status'=>'1',
             'titulo'=>'Incomplete.',
             'mensaje'=>'Action incomplete.'
           );

           echo json_encode($response);

         }
        }



}





  } else{

            echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';

        }
}
    public function eliminar_precio_tour(){

     if(isset($this->session->userdata['logged_in'])){

       $id = $_POST['id'];
       $playa = $_POST['tour'];
       if(isset($this->session->userdata['logged_in'])){

       $insert = $this->provincias_model->eliminar_precio_tour($id);




       // if($insert){
       //   echo '<script>window.location="'.base_url().'index.php/tour_operadora/configurar_tour/'.$playa.'?e=1#precios'.'"</script>';
       // }else{
       //   echo '<script>window.location="'.base_url().'index.php/tour_operadora/configurar_tour/'.$playa.'?e=2#precios'.'"</script>';
       //  }



       if($insert){
         // echo '<script>window.location="'.base_url().'index.php/tour_operadora/configurar_tour/'.$playa.'?e=1#cupo'.'"</script>';
         // var_dump($insert);



         $data4 = array(
           'porcetaje_descuento'=>0
         );

         $update= $this->tour_model->actualizar_info_tour($playa,$data4);


         if( $_SESSION['idioma']   == 'es' ){

           $response = array(
             'status'=>'1',
             'titulo'=>'Completado.',
             'mensaje'=>'Acción completada.'
           );

           echo json_encode($response);


         }else if( $_SESSION['idioma']   == 'en' ){

           $response = array(
             'status'=>'1',
             'titulo'=>'Completed.',
             'mensaje'=>'Action completed.'
           );

           echo json_encode($response);

         }
       }else{
         // echo '<script>window.location="'.base_url().'index.php/tour_operadora/configurar_tour/'.$playa.'?e=2#cupo'.'"</script>';
         //  var_dump($insert);
         if( $_SESSION['idioma']   == 'es' ){

           $response = array(
             'status'=>'1',
             'titulo'=>'No Completado.',
             'mensaje'=>'Acción no completada.'
           );

           echo json_encode($response);


         }else if( $_SESSION['idioma']   == 'en' ){

           $response = array(
             'status'=>'1',
             'titulo'=>'Incomplete.',
             'mensaje'=>'Action incomplete.'
           );

           echo json_encode($response);

         }
        }



}





  } else{

            echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';

        }
}


    public function configurar_tour($id){
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu


        $data['']='';
        	if(isset($this->session->userdata['logged_in'])){
       $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
       $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }



            if($data['rol_user'][0]->rol == 2 || $data['rol_user'][0]->rol == 1 ){
             $data['have_operadora'] = ($this->tour_model->check_operadora($this->session->userdata['logged_in']['id']));
             $data['datos_operadora'] = ($this->tour_model->datos_operadora($this->session->userdata['logged_in']['id']));
             $data['playas'] = $this->provincias_model->todas_las_playas();
             $data['tour'] = $this->tour_model->tours_por_id($id);
             $data['actividades'] = $this->tour_model->todas_los_tipos_de_tour();
             $data['cupos'] = $this->tour_model->cupos_tour($data['tour'][0]->idtour);
             $data['precios'] = $this->tour_model->precios_tour($data['tour'][0]->idtour);
             $data['galeria'] = $this->tour_model->galeria_tour($id);

             $this->load->view('configurar_tour.php',$data);

         }else{

            echo '<script>window.location="'.base_url().''.'"</script>';

         }


            } else{

            echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';

        }

    }

    public function agregar_cupo(){

        $data['']='';
      if(isset($this->session->userdata['logged_in'])){
       $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
       $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }


            if($data['rol_user'][0]->rol == 1 || $data['rol_user'][0]->rol == 2){

              $date=date_create($_POST['fecha']);





              $data = array(

                'id_tour'=>$_POST['id'],
                'fecha'=>date_format($date,"Y-m-d"),
                'hora'=>$_POST['time'],
                'cupos'=>$_POST['cupos']
              );



                $insert = $this->tour_model->agregar_cupo($data);

                $idcupo = $this->tour_model->cupo_max($_POST['id']);

                if($insert){
                $response = array(
                  'satus'=>'1',
                  'idcupo'=>$idcupo[0],
                  'row'=>$data
                );
                }else{
                  $response = array(
                    'satus'=>'2',

                    'row'=>''
                  );
                }

                echo json_encode($response);

         }else{
            echo '<script>window.location="'.base_url().''.'"</script>';
         }
            } else{
            echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
        }
    }

    public function agregar_precio(){

        $data['']='';
      if(isset($this->session->userdata['logged_in'])){
       $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
       $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }

            if($data['rol_user'][0]->rol == 1 || $data['rol_user'][0]->rol == 2){
              $data = array(
                'idtour'=>$_POST['id'],
                'tipo_cliente'=>$_POST['cliente'],
                'descuento'=>$_POST['descuento'],
                'estado'=>$_POST['estado'],
              );


              $data2 = array(
                'idtour'=>$_POST['id'],
                'tipo_cliente'=>$_POST['cliente'],
                'descuento'=>$_POST['descuento'],
                'estado'=>$_POST['estado'],
                'precio'=>$_POST['precio'],
                'idcupo'=>$_POST['idcupo'],
              );

                if ($_POST['cliente'] == 1) {

                  $id_tour = $_POST['id'];
                  $data3 = array(
                    'porcetaje_descuento'=>$_POST['descuento']
                  );

                  $update= $this->tour_model->actualizar_info_tour($id_tour,$data3);
                }








               $insert = $this->tour_model->agregar_precio($data);



               $idprecio = $this->tour_model->precio_max($_POST['id']);


                  if($insert){
                  $response = array(
                    'status'=>'1',
                    'row'=>$data2,
                    'idprecio'=>$idprecio[0]
                  );
                  }else{
                    $response = array(
                      'status'=>'2',
                      'row'=>'',
                      'idpreio'=>$idprecio
                    );
                  }
         }else{
            echo '<script>window.location="'.base_url().''.'"</script>';
         }
            } else{
            echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';
        }

          echo json_encode($response);
    }

}
