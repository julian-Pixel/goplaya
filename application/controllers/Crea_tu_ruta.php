<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crea_tu_ruta extends CI_Controller {

    
    public function __construct() {
        parent::__construct();


        $this->load->model('login_model');
        $this->load->model('provincias_model');
        $this->load->model('search_model');
        $this->load->model('menu_model');
        $this->load->model('email_model');
    }

	
    
    
	public function index()
	{
          if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
        //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
      $data['rutas_predisenadas'] = $this->provincias_model->rutas_predisenadas();
        
    //Fin Datos Generales
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        $data['caracteristicas'] = $this->provincias_model->todas_las_caracteristicas_simples();
        if(isset($this->session->userdata['logged_in'])){
                $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
                $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
                
                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){
                    
                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];
                   
                }else{
                    
                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
                
               
            }
        
        
        
        if(isset($_GET['lat'])){
          $latitud = $_GET['lat'];
    }
    
    if(isset($_GET['lng'])){
         $longitud = $_GET['lng'];
    } 
    
     if(isset($_GET['range'])){
        $radio = $_GET['range'];
    }
    
       
  if(isset($_GET['lat'])){
       
      
        
     if(isset($_GET['filtros'])){
         
         //Variables principales
        
        $filtros ='';
 //Asignar valores a variables principales
        
        $categorias = $_GET['filtros'];
        
        foreach($categorias as $cat){
            
           $filtros.= 'caracteristicas_playas.id_caracteristica = '.$cat.' OR ';
        }
        
        
//Variables limpias
         
         
          $filtros =  substr($filtros,0,-4); 
        $where = " ( ".$filtros." ) ";
            
      //var_dump($where);
         $data2 = array(
             'where'=> $where,
            'radio'=>$radio,
            'latitud'=>$latitud,
            'longitud'=>$longitud
        );
         
     }else{
         
         $data2 = array(
            'radio'=>$radio,
            'latitud'=>$latitud,
            'longitud'=>$longitud
        );
         
     }     
       
 $data['playas'] = $this->search_model->buscar_playas_ctr($data2);
      
}else{
         
      
         
     }
        

    //Ejecutamos la funcion buscar
        
      
       
    /*if(sizeof($data2['playas'])>1){
        $this->load->view('resultados_busqueda_ctr',$data2);
        
    }else{
        echo'<div class="nofound">';
        echo '<h3><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></h3>';
        echo '<p>No se encontraron playas a: '.$radio.' Kms, intenta con una distancia mayor y con diferentes filtros</p>';
        echo'</div>';
    }*/
    
        
        
        
        $this->load->view('crea_tu_ruta',$data);   
	}
    
  
    
}
