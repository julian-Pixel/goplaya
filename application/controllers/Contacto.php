<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {

	   public function __construct() {
        parent::__construct();


        $this->load->model('login_model');
        $this->load->model('provincias_model');
        $this->load->model('pagina_model');
        $this->load->model('slider_model');
        $this->load->model('modulos_model');
        $this->load->model('menu_model');
        $this->load->model('publicidad_model');
        $this->load->model('email_model');
    }

    
	public function index()
	{
        
        
        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
       
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        //Lugares
                $data['provincias'] = $this->provincias_model->todas_las_provincias();
                $data['regiones'] = $this->provincias_model->todas_las_regiones();
                $data['playas'] = $this->provincias_model->todas_las_playas();
                $data['caracteristicas'] = $this->provincias_model->todas_las_caracteristicas_simples();
        // Fin lugares
        
         //Articulos
            $data['articulos'] = $this->pagina_model->articulos_home();
        //Fin articulos
        //Slides
                
                $data['slides'] = $this->slider_model->slides();
            
            //Fin slides
        
        
    //Secciones home_template
        $data['secciones'] = $this->modulos_model->modulos_home();
        
     //Fin secciones home   
        
    //crea tu ruta
        

     $data['crea_tu_ruta'] = $this->pagina_model->contenido_pagina('crea-tu-ruta');    
        
    //Fin crea tu ruta   
        
        
        
    //ADS
        
        $data['banners_superior'] = $this->publicidad_model->banners_id('1');   
        $data['banners_medio'] = $this->publicidad_model->banners_id('2');   
        $data['banners_final'] = $this->publicidad_model->banners_id('3');   
        $data['banners_buscador'] = $this->publicidad_model->banners_id('4');   
        
    //FIN ADS
        
    //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales
        
        	if(isset($this->session->userdata['logged_in'])){
                $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
                $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
                
                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){
                    
                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];
                   
                }else{
                    
                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
                
               
            }
           
           
		$this->load->view('contacto',$data);
	}
    
    public function enviar(){
        
        $to = 'dev@pixelcr.com';
        $asunto = $_POST['asunto'];
        $mensaje['mensaje'] = 'Nuevo contacto de: '.$_POST['nombre'].'<br> Mensaje:'.$_POST['mensaje'];
        $plantilla = 'contacto';
        
        $email = $this->email_model->enviar_email($to,$asunto,$mensaje,$plantilla);
        
        if($email){
               echo '<script>window.location="'.base_url().'contacto/?e=1"</script>';  
            }else{
                echo '<script>window.location="'.base_url().'contacto/?e=2"</script>';  
            }
       
    }
    
    
    public function suscribirse(){
        
     
        $data = array(
            'nombre' => $_POST['nombre'],
            'email' => $_POST['email']
        );
       
        
        $insert = $this->email_model->suscripcion($data);
        
        if($insert){
               echo '<script>window.location="'.base_url().'?e=1"</script>';  
            }else{
                echo '<script>window.location="'.base_url().'?e=2"</script>';  
            }
       
    }
    
}
