<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {


    public function __construct() {
        parent::__construct();


        $this->load->model('login_model');
        $this->load->model('provincias_model');
        $this->load->model('seguridad_model');
        $this->load->model('email_model');
        $this->load->model('menu_model');
    }




	public function index(){
           //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales

        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
          if(isset($this->session->userdata['logged_in'])){

             //check administrador

            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            //fin check adminsitrador
                //Datos Usuario
             $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));




                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
              //Fin datos Usuario

            //Menus
              $data['menus'] = $this->menu_model->menus();
            //Fin menus

            	$this->load->view('ad_menu.php',$data);

    }else{


        }

	}


    public function editar_menu($id){
           //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales

        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
           if(isset($this->session->userdata['logged_in'])){

             //check administrador

            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            //fin check adminsitrador
                //Datos Usuario
             $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));




                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
              //Fin datos Usuario

            //Menus
              $data['menus'] = $this->menu_model->menu_items($id);
            //Fin menus
                $data['idmenu'] = $id;
            	$this->load->view('ad_menuitem.php',$data);

    }else{


        }
    }

    public function eliminar_item($id){

        if(isset($this->session->userdata['logged_in'])){

             //check administrador

            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            //fin check adminsitrador
                //Datos Usuario
             $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));




                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
              //Fin datos Usuario


            $delete = $this->menu_model->eliminar_item($id);

            if($delete){

                echo '<script>window.location="'.base_url().'menu?e=1"</script>';

            }else{
                 echo '<script>window.location="'.base_url().'menu?e=2"</script>';
            }


    }else{


        }


    }


     public function agregar_item(){

        if(isset($this->session->userdata['logged_in'])){

             //check administrador

            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            //fin check adminsitrador
                //Datos Usuario
             $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));




                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
              //Fin datos Usuario


          $data = array(

            'detalle'=>$_POST['detalle_es'],
            'detalle_en'=>$_POST['detalle_en'],
            'enlace'=>$_POST['enlace_es'],
            'enlace_en'=>$_POST['enlace_en'],
            'idmenu'=>$_POST['id']

          );

       $insert = $this->menu_model->insert_item($data);
            if($insert){

                echo '<script>window.location="'.base_url().'menu/editar_menu/'.$_POST['id'].'?e=1"</script>';

            }else{
                echo '<script>window.location="'.base_url().'menu/editar_menu/'.$_POST['id'].'?e=2"</script>';
            }

    }else{


        }


    }


    public function editar_item(){

        if(isset($this->session->userdata['logged_in'])){

             //check administrador

            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            //fin check adminsitrador
                //Datos Usuario
             $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));




                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
              //Fin datos Usuario


          $data = array(

            'detalle'=>$_POST['detalle'],
            'detalle_en'=>$_POST['detalle_en'],
            'enlace'=>$_POST['enlace_es'],
            'enlace_en'=>$_POST['enlace_en'],

          );

       $insert = $this->menu_model->update_item($_POST['id'],$data);
            if($insert){

                echo '<script>window.location="'.base_url().'menu/editar_menu/'.$_POST['idmenu'].'?e=1"</script>';

            }else{
                echo '<script>window.location="'.base_url().'menu/editar_menu/'.$_POST['idmenu'].'?e=2"</script>';
            }

    }else{


        }


    }

    function ordenar_item(){
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
       if(isset($this->session->userdata['logged_in'])){

         $items = $_POST['item'];
        $orden = 0;

           foreach($items as $item){

               $data=array(
               'id'=> $item,
                'orden'=>$orden
               );

               $insert = $this->menu_model->ordenar_items($data);
               if($insert){
                   echo 'item actualizado';
               }else{

                   'problema';
               }

              $orden ++;
           }


        }else{

        }

}





}
