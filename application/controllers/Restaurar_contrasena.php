<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurar_contrasena extends CI_Controller {

    
    public function __construct() {
        parent::__construct();


        $this->load->model('login_model');
        $this->load->model('provincias_model');
        $this->load->model('seguridad_model');
        $this->load->model('email_model');
        $this->load->model('menu_model');
    }

	
    
    
	public function index(){
          //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        
       		$this->load->view('generar_contrasena',$data);
		
	}
    
    public function generar_codigo(){
          //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        
        $fecha = date('Y-m-d h:m:s');
        $email = $_POST['email'];
        $codigo = $this->seguridad_model->generar_codigo();
        $fecha_vencimiento = $this->seguridad_model->fecha_vencimiento($fecha);
       
        $check_usuario = $this->seguridad_model->check_usuario($email);
        
        if($check_usuario){
            $usuario = $this->seguridad_model->usuario($email);
            $data = array(
                'idusuario'=>$usuario[0]->idusuario,
                'codigo'=>$codigo,
                'fecha_maxima' =>$fecha_vencimiento,
                'estado' =>'0'
            );   
        //var_dump($data);
            
            $insert = $this->seguridad_model->insertar_codigo($data);
            
            if($insert){
                
                $mensaje = array(
                    'codigo'=>$codigo,
                    'usuario'=> $usuario[0]->nombre.' '.$usuario[0]->apellidos
                );
                  $email = $this->email_model->enviar_email('valverdeagustn@gmail.com','Restaurar contraseña',$mensaje,'restaurar_contrasena');
                
                if($email){
                     echo '<script>window.location="'.base_url().'?e=1&me=1'.'"</script>';
                    
                }else{
                     echo '<script>window.location="'.base_url().'?e=2'.'"</script>';
                }
                
                
                
            }else{
                echo '<script>window.location="'.base_url().'?e=2'.'"</script>';
            }
            
        }else{
            
           echo '<script>window.location="'.base_url().'?e=2'.'"</script>';
            
            
        }
        
	}
    
    public function cambiar(){
          //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
         $data=0;
       		$this->load->view('cambiar_contrasena',$data);
    }
    
    
    public function cambiar_contrasena(){
          //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        
        //var_dump($_POST);
        $fecha_actual = date('Y-m-d h:m:s');

        $codigo = $this->seguridad_model->datos_codigo($_POST['codigo']);

        $verificar = $this->seguridad_model->verificar_codigo($_POST['codigo'],$codigo[0]->fecha_maxima,$fecha_actual);
        var_dump( $verificar);
        if($verificar){
            
            $data = array(
                'contrasena'=>md5($_POST['nc'])
            
            );
            
            $update = $this->seguridad_model->actualizar_contrasena($codigo[0]->idusuario,$data);
            
            var_dump($update);
            
            if($update){
                
                /* $mensaje = array(
                    'usuario'=> $usuario[0]->nombre.' '.$usuario[0]->apellidos
                );
                
                $email = $this->email_model->enviar_email('valverdeagustn@gmail.com','Restaurar contraseña',$mensaje,'contrasena_cambiada');
                
                if($email){
                     echo '<script>window.location="'.base_url().'?e=1&me=1'.'"</script>';
                    
                }else{
                     echo '<script>window.location="'.base_url().'?e=2'.'"</script>';
                }*/
                
                
                echo '<script>window.location="'.base_url().'?e=1'.'"</script>';
                
            }else{
                echo '<script>window.location="'.base_url().'?e=2'.'"</script>';
            }
        }else{
             echo '<script>window.location="'.base_url().'?e=2&ucne=1'.'"</script>';
        }
     
    }
   
    
}
