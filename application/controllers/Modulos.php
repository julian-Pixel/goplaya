<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class modulos extends CI_Controller {

	  
    public function __construct() {
        parent::__construct();
        $this->load->model('pagina_model');
        $this->load->model('login_model');
        $this->load->model('modulos_model');
        $this->load->model('menu_model');
        $this->load->model('email_model');
    }

    
	public function index()
	{
        //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales
        
        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
            if(isset($this->session->userdata['logged_in'])){
            
             //check administrador
            
            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            //fin check adminsitrador
                //Datos Usuario
             $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
            
             $data['secciones'] = $this->modulos_model->modulos_home();
            
                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){
                    
                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];
                   
                }else{
                    
                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
              //Fin datos Usuario  
             $data['secciones'] = $this->modulos_model->modulos_disponibles();
            
             $data['modulos'] = $this->modulos_model->modulos_no_instalados();
                
                $disponibles= array();
                $secciones = array();
                
                foreach( $data['secciones'] as $sec){
                    
                    $secciones[]= $sec->modulo;
                }
                
                $modulos = array();
                
                foreach( $data['modulos'] as $mod){
                    
                    $modulos[]= $mod;
                }
               
                
                foreach ($modulos as $value1) {
    $encontrado=false;
    foreach ($secciones as $value2) {
        if ($value1 == $value2){
            $encontrado=true;
            $break;
        }
    }
    if ($encontrado == false){
           $disponibles[] = $value1;
    }
}
             
                $data['disponibles'] = $disponibles;
              
      
            	$this->load->view('administrador_modulos',$data);
            
    }else{
            
            
        }
        
        
        
	}
    
   
    
    function ordenar_modulos(){
        //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales
        
        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
       if(isset($this->session->userdata['logged_in'])){
           
         $items = $_POST['item'];
        $orden = 0;
           
           foreach($items as $item){
               
               $data=array(
               'id'=> $item,
                'orden'=>$orden
               );
               
               $insert = $this->modulos_model->ordenar_modulo($data);
               if($insert){
                   echo 'red actualizada';
               }else{
                   
                   'problema';
               }
               
              $orden ++; 
           }
           
        
    }else{
        
    }     
        
    }  
    
    
    
    public function activar_modulo(){
        //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales
        
        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
        
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        if(isset($this->session->userdata['logged_in'])){
            
            $data = array(
                'modulo'=>$_GET['m'],           
            );
          
            $insert = $this->modulos_model->activar_modulo($data);
            
            if($insert){
                
                echo '<script>window.location="'.base_url().'modulos/?e=1"</script>';
                
            }else{
                 echo '<script>window.location="'.base_url().'modulos/?e=2"</script>';
            }
            
            
        }else{
            
            
        }
 
    }
    
    
     public function agregar_home(){
         //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales
        
        if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data['admin'] = $admin;
        }
        
        //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu
        if(isset($this->session->userdata['logged_in'])){
            
            $data = array(
                'idmodulo'=>$_POST['seccion'],           
            );
          
            $insert = $this->modulos_model->agregar_home($data);
            
            if($insert){
                
                echo '<script>window.location="'.base_url().'pagina/editar_home?e=1"</script>';
                
            }else{
                 echo '<script>window.location="'.base_url().'pagina/editar_home?e=2"</script>';
            }
            
            
        }else{
            
            
        }
 
    }
}
