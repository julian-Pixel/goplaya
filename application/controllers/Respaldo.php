<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Respaldo extends CI_Controller {


    public function __construct() {
        parent::__construct();


        $this->load->model('pagina_model');
        $this->load->model('login_model');
        $this->load->model('slider_model');
        $this->load->model('menu_model');
    }


	public function index(){
    //Menu
            $data['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu

        if(isset($this->session->userdata['logged_in'])){

             //check administrador

            $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
            //fin check adminsitrador
            //Datos Usuario
             $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
            $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));

                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){

                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];

                }else{

                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
              //Fin datos Usuario



            	       $this->load->view('ad_slider',$data);

    }else{


        }
    }
   function backup($fileName='db_backup.sql'){

    // Load the DB utility class
    $this->load->dbutil();

    // Backup your entire database and assign it to a variable
    $backup =& $this->dbutil->backup();

    // Load the file helper and write the file to your server
    $this->load->helper('file');
    write_file(FCPATH.'/downloads/'.$fileName, $backup);

    // Load the download helper and send the file to your desktop
    $this->load->helper('download');
    force_download($fileName, $backup);
}



}
