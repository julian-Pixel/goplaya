<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_controller extends CI_Controller {

	
      public function __construct() {
        parent::__construct();


        $this->load->model('search_model');
        $this->load->model('provincias_model');
        $this->load->model('login_model');
        $this->load->model('menu_model');
        $this->load->model('email_model');
          

    }

    
    
	public function index()
	{
		
	}
    
    public function generar_playas(){
        //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales
        
        $s = $_POST['term'];
        
        $playas = $this->search_model->generar_playas($s);
        
        if($playas){
             echo json_encode($playas);
        }else{
           
$string = '
[{"idplaya":"1","nombre":"No se encontraron resultados","provincia":""}]
';
            
        $obj = json_decode($string);
echo json_encode($obj);
        }
        
       
        
    }
    
    
public function buscar(){
    
    
    if(isset($_GET['latitud'])){
          $latitud = $_GET['latitud'];
    }else{
          $latitud = '9.935607';
    }
    
    if(isset($_GET['longitud'])){
         $longitud = $_GET['longitud'];
    }else{
         $longitud = '-84.1833856';
    }
      
    
     if(isset($_GET['radio'])){
        $radio = $_GET['radio'];
    }else{
         $radio ='10000';
    }
    
    if(isset($_GET['filtros']) and isset($_GET['busqueda'])){
       
        //Variables principales
        
        $filtros ='';
        $ubicaciones ='';
        $playa_ptrn ='';
       
        
         //Asignar valores a variables principales
        
        $categorias = $_GET['filtros'];
        
        foreach($categorias as $cat){
            
           $filtros.= 'caracteristicas_playas.id_caracteristica = '.$cat.' OR ';
        }
        
        
      $lugares = $_GET['busqueda'];
        
       foreach($lugares as $lugar){
            
           $ubicaciones.= "(playas.nombre LIKE '%".$lugar."%' or provincias.nombre LIKE '%".$lugar."%' or regiones.nombre LIKE '%".$lugar."%') or " ;
        } 
        
        
        //Variables limpias
        
      
        
        $filtros =  substr($filtros,0,-4); 
        $ubicaciones =  substr($ubicaciones,0,-4); 
          
        /*$playa_ptrn = "playas.nombre LIKE '%".$_GET['busqueda']."%' or provincias.nombre LIKE '%".$_GET['busqueda']."%' or regiones.nombre LIKE '%".$_GET['busqueda']."%'";*/
        $playa_ptrn = $ubicaciones;
   
        
        if($_GET['busqueda']=='todas'){
          $where = " ( ".$filtros." ) ";
            
        }else{
           $where = "(".$playa_ptrn.") AND ( ".$filtros." ) ";
        }
        
      
        
        $data = array(
            'where'=>$where,
            'radio'=>$radio,
            'latitud'=>$latitud,
            'longitud'=>$longitud
        );
        
    

        
       $data2['playas'] = $this->search_model->buscar_playas($data); 
    
       
    }else{
        
        
    }
     
    
    //Datos para la vista
      $data2['latitud']=$latitud;
     $data2['longitud']= $longitud;
    
    //Lugares
                $data2['provincias'] = $this->provincias_model->todas_las_provincias();
                $data2['regiones'] = $this->provincias_model->todas_las_regiones();
              
                $data2['caracteristicas'] = $this->provincias_model->todas_las_caracteristicas_simples();
                $caracteristicas_padre = $this->provincias_model->caracteristicas_padre();
    
            // Fin lugares
        	if(isset($this->session->userdata['logged_in'])){
       $data2['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
       $data2['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
                
                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){
                    
                    $data2['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];
                   
                }else{
                    
                     $data2['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
                
               
            }
           
    
        $fp = array();
      foreach($caracteristicas_padre as $cp){

        $ch = $this->provincias_model->todas_las_caracteristicas_por_padre($cp->id);  
                              
          foreach($ch as $c){
              
              
               if( $_SESSION['idioma']   == 'es' ){ 
                      $stack[] =  $c->detalle;
                }else if( $_SESSION['idioma']   == 'en' ){
                   
                      $stack[] =  $c->detalle_en;
                }
                                
              //$stack[] =  $c->detalle;
              $id_c[] = $c->idactividad;
          }
             
          
          
               if( $_SESSION['idioma']   == 'es' ){ 
                     $fp[$cp->detalle] = array($id_c,$stack);  
                }else if( $_SESSION['idioma']   == 'en' ){
                   
                    $fp[$cp->detalle_en] = array($id_c,$stack);  
                }
          
          
        //$fp[$cp->detalle] = array($id_c,$stack);  
          
          unset($stack);
          $stack = array();
          unset($id_c);
          $id_c = array();
    
      
      
      }
    
    
    $data2['filtros_avanzados'] = $fp;
              
     //Menu
            $data2['menu_home'] = $this->menu_model->menu_items('1');
        //Fin menu 
    //Vista
    
    //Ejecutamos la funcion buscar
        //Datos Generales
      $data2['datos_generales'] = $this->email_model->datos_generales();
      if(isset($this->session->userdata['logged_in'])){
              $admin = $this->login_model->admin($this->session->userdata['logged_in']['id']);
    $data2['admin'] = $admin;
        }
    $this->load->view('resultados_busqueda',$data2);
      
        
    }
    
    
    
    public function crea_tu_ruta(){

        //Datos Generales
      $data['datos_generales'] = $this->email_model->datos_generales();
    //Fin Datos Generales
        
  
    if(isset($_POST['lat'])){
          $latitud = $_POST['lat'];
    }else{
          $latitud = '9.87945';
    }
    
    if(isset($_POST['lng'])){
         $longitud = $_POST['lng'];
    }else{
         $longitud = '-85.5184';
    }
      
    
     if(isset($_POST['range'])){
        $radio = $_POST['range'];
    }else{
         $radio ='10000';
    }
    
       
     if(isset($_POST['filtros'])){
       
        //Variables principales
        
        $filtros ='';
 //Asignar valores a variables principales
        
        $categorias = $_POST['filtros'];
        
        foreach($categorias as $cat){
            
           $filtros.= 'caracteristicas_playas.id_caracteristica = '.$cat.' OR ';
        }
        
        
//Variables limpias
        
        
        $filtros =  substr($filtros,0,-4); 
        $where = " ( ".$filtros." ) ";
            
      //var_dump($where);
         $data = array(
             'where'=> $where,
            'radio'=>$radio,
            'latitud'=>$latitud,
            'longitud'=>$longitud
        );

}else{
         
         
        $data = array(
            'radio'=>$radio,
            'latitud'=>$latitud,
            'longitud'=>$longitud
        ); 
         
     }
        

    //Ejecutamos la funcion buscar
        
       $data2['playas'] = $this->search_model->buscar_playas_ctr($data); 
    if(sizeof($data2['playas'])>1){
        $this->load->view('resultados_busqueda_ctr',$data2);
        
    }else{
        echo'<div class="nofound">';
        echo '<h3><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></h3>';
        echo '<p>No se encontraron playas a: '.$radio.' Kms, intenta con una distancia mayor y con diferentes filtros</p>';
        echo'</div>';
    }
    
     
      
        
    }
    
    
}
