<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservaciones extends CI_Controller {

	  
    public function __construct() {
        parent::__construct();


        $this->load->model('login_model');
        $this->load->model('provincias_model');
        $this->load->model('tour_model');
    }

    
	public function index()
	{
        
     
        
        $data['']='';
        	if(isset($this->session->userdata['logged_in'])){
       $data['info_user'] = ($this->login_model->info_usuario($this->session->userdata['logged_in']['id']));
       $data['rol_user'] = ($this->login_model->mi_rol($this->session->userdata['logged_in']['id']));
                
                if(isset ($this->session->userdata['logged_in']['fbp']['data']['url'])){
                    
                    $data['foto_perfil']= $this->session->userdata['logged_in']['fbp']['data']['url'];
                   
                }else{
                    
                     $data['foto_perfil']= 'https://s-media-cache-ak0.pinimg.com/originals/b1/bb/ec/b1bbec499a0d66e5403480e8cda1bcbe.png';
                }
                
                
                
                       if($data['rol_user'][0]->rol == 2){
             $data['have_operadora'] = ($this->tour_model->check_operadora($this->session->userdata['logged_in']['id']));
             $data['datos_operadora'] = ($this->tour_model->datos_operadora($this->session->userdata['logged_in']['id']));
             $this->load->view('reservaciones.php',$data);
             
         }else{
             
            echo '<script>window.location="'.base_url().''.'"</script>'; 
             
         } 
                
               
            } else{
            
            echo '<script>window.location="'.base_url().'index.php/dashboard/ingresar'.'"</script>';

        }
        
   
       
		
	}
    
    
    
    
}
