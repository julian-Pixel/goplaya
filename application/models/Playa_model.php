<?php

Class Playa_model extends CI_Model {

public function informacion_general($id) {
    $condition = "idplaya =". "'" . $id . "'";
    $this->db->select('*');
    $this->db->from('playas');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();
}


    public function informacion_general2($id) {
    $condition = "slug = ". "'" . $id . "'";
    $this->db->select('*');
    $this->db->from('playas');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();
}


public function get_all() {
$this->db->select('*');
$this->db->from('playas');
$query = $this->db->get();
return $query->result();
}


public function portada($id) {
    $condition = "tipo = 1 and idplaya=". "'" . $id . "'";
    $this->db->select('*');
    $this->db->from('galeria_playas');
    $this->db->where($condition);
    $query = $this->db->get();
    $this->db->limit(1);
    return $query->result();
}

public function galeria($id) {
    $condition = "idplaya =". "'" . $id . "'";
    $this->db->select('*');
    $this->db->from('galeria_playas');
    $this->db->where($condition);
    $this->db->order_by('orden asc');
    $query = $this->db->get();
    return $query->result();
}


public function ads($id) {
$condition = "idplaya =". "'" . $id . "'";
$this->db->select('*');
$this->db->from('ads');
$this->db->where($condition);
$this->db->order_by('orden asc');
$query = $this->db->get();
return $query->result();
}



public function estrellas($id) {
    $condition = "id_playa =". "'" . $id . "'";
    $this->db->select('sum(calificacion) as total, count(*) as registros');
    $this->db->from('calificaciones_playas');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();
}

 public function actividades_playa($id) {
    $condition = "id_playa =". "'" . $id . "'";
    $this->db->select('actividades.detalle, actividades.icono');
    $this->db->from('caracteristicas_playas');
    $this->db->join('actividades','actividades.idactividad = caracteristicas_playas.id_caracteristica');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();
}





 public function agregar_voto($data){

    $this->db->insert('calificaciones_playas', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
        return false;
    }

}


  public function ordenar_items($data = array()){
    $orden = array(
    'orden'=> $data['orden']
    );

    $this->db->where('id', $data['id']);
    $this->db->update('galeria_playas', $orden);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else{
        return false;

    }
}


}

?>
