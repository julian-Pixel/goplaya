<?php

Class Opiniones_model extends CI_Model {


  //Seleccionar todas las opiniones
  public function todas_las_opiniones(){

      $this->db->select('opiniones.id,
                         opiniones.nombre,
                         opiniones.correo,
                         opiniones.opinion,
                         opiniones.id_tour,
                         tours.nombre as nombre_tour,
                         opiniones.habilitado,
                         opiniones.fecha');
      $this->db->from('opiniones');
      $this->db->join('tours','tours.idtour = opiniones.id_tour');
      $query = $this->db->get();
      return $query->result();
  }

  //Seleccionar todas las opiniones con condición
  public function todas_las_opiniones_where($id){
      $condition = "id_tour =" ."'". $id . "'";
      $this->db->select('opiniones.id,
                         opiniones.nombre,
                         opiniones.correo,
                         opiniones.opinion,
                         opiniones.id_tour,
                         tours.nombre as nombre_tour,
                         opiniones.habilitado,
                         opiniones.fecha');
      $this->db->from('opiniones');
      $this->db->join('tours','tours.idtour = opiniones.id_tour');
      $this->db->where($condition);
      $query = $this->db->get();

      return $query->result();
  }

  //Agregar una opinion
  public function agregar_opinion($data){

      $this->db->insert('opiniones', $data);
      if ($this->db->affected_rows() > 0) {
          return true;
      }else{
          return false;
      }
  }

  //Editar el estado de un tour
  public function actualizar_opinion($id,$data){
  $this->db->where('id', $id);
  $this->db->update('opiniones', $data);
    if ($this->db->affected_rows() > 0) {
          return true;
      }else {
      return false;
      }
  }

}

?>
