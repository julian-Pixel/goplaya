<?php

Class Login_model extends CI_Model {

// Insert registration data in database
public function registration_insert($data) {

// Query to check whether username already exist or not
$condition = "email =" . "'" . $data['email'] . "'";
$this->db->select('*');
$this->db->from('usuarios');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();
if ($query->num_rows() == 0) {

// Query to insert data in database
$this->db->insert('usuarios', $data);
if ($this->db->affected_rows() > 0) {
return true;
}
} else {
return false;
}
}

// Read data using username and password
public function login($data) {

$condition = "email =" . "'" . $data['username'] . "' AND " . "contrasena =" . "'" . $data['password'] . "' AND estado = 1";
$this->db->select('*');
$this->db->from('usuarios');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return true;
} else {
return false;
}
}

// Read data from database to show data in admin page
public function read_user_information($username) {

$condition = "email =" . "'" . $username . "'";
$this->db->select('*');
$this->db->from('usuarios');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return $query->result();
} else {
return false;
}
}
    
public function mi_rol($id){
    $condition = "usuarios.idusuario =" . "'" . $id . "'";
    $this->db->select('usuarios.rol, roles_usuarios.nombre');
    $this->db->from('usuarios');
    $this->db->join('roles_usuarios','roles_usuarios.idrol_usuario = usuarios.rol');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();
    
}
    
    
public function info_usuario($id) {

$condition = "idusuario =" . "'" . $id . "'";
$this->db->select('*');
$this->db->from('usuarios');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return $query->result();
} else {
return false;
}
}    
    
    
   public function facebook_login($data) {

// Query to check whether username already exist or not
$condition = "email =" . "'" . $data['email'] . "'";
$this->db->select('*');
$this->db->from('usuarios');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();
if ($query->num_rows() == 0) {
//var_dump($data);
$this->db->insert('usuarios', $data);
if ($this->db->affected_rows() > 0) {
return true;
}
} else {
    
return false;
    
}
} 
    
    
 public function admin($id) {

$condition = "idusuario =" . "'" . $id . "'";
$this->db->select('*');
$this->db->from('administradores');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return true;
} else {
return false;
}
}  
    
    
    
public function update_info($id,$data){ 
$this->db->where('idusuario', $id);
$this->db->update('usuarios', $data); 
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }   
    
}  
    
public function update_pass($id,$data){ 
$this->db->where('idusuario', $id);
$this->db->update('usuarios', $data); 
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }   
    
}    
    

}

?>