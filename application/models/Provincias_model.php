<?php

Class Provincias_model extends CI_Model {

// Insert registration data in database
public function registration_insert($data) {

// Query to check whether username already exist or not
$condition = "email =" . "'" . $data['user_name'] . "'";$condition = "email =" . "'" . $data['user_name'] . "'";
$this->db->select('*');
$this->db->from('usuarios');
$this->db->where($condition);$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();
if ($query->num_rows() == 0) {

// Query to insert data in database
$this->db->insert('user_login', $data);
if ($this->db->affected_rows() > 0) {
return true;
}
} else {
return false;
}
}


// Read data from database to show data in admin page
public function todas_las_provincias() {

    $this->db->select('*');
    $this->db->from('provincias');
    $query = $this->db->get();
    return $query->result();

}

public function info_region($region) {
    $condition = 'id_region ='.'"'.$region.'"';
    $this->db->select('*');
    $this->db->from('regiones');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

}

public function comporobar_permalink($slug) {
    $condition = 'slug ='.'"'.$slug.'"';
    $this->db->select('*');
    $this->db->from('playas');
    $this->db->where($condition);
    $this->db->limit(1);
    $query = $this->db->get();

    if ($query->num_rows() == 1) {
    return true;
    } else {
    return false;
    }

}


public function insertar_provincia($data){

    $this->db->insert('provincias', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function insertar_item_galeria($data){

    $this->db->insert('galeria_playas', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}





public function eliminar_provincia($id){

     $this->db->where('idprovincia',$id);
  $this->db->delete('provincias');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}



public function eliminar_item_galeria($id){

     $this->db->where('id',$id);
  $this->db->delete('galeria_playas');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}


public function eliminar_cupo_tour($id){

     $this->db->where('id',$id);
  $this->db->delete('cupo_tours');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}
public function eliminar_precio_tour($id){

     $this->db->where('id',$id);
  $this->db->delete('precios_tours');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}
public function eliminar_item_galeria_tour($id){

     $this->db->where('id',$id);
  $this->db->delete('galeria_tours');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}
/// ADS

public function insertar_item_ads($data){




$this->db->insert('ads', $data);

if ($this->db->affected_rows() > 0) {
return true;
}else {
return false;
}

}


public function eliminar_item_ads($id){

$this->db->where('id',$id);
$this->db->delete('ads');

if ($this->db->affected_rows() > 0) {
return true;
}else {
return false;
}

}

public function ads_playa($id){

$condition = "idplaya =" . "'" . $id . "'";
$this->db->select('*');
$this->db->from('ads');
$this->db->where($condition);
$query = $this->db->get();
return $query->result();


}


/// Fin de ADS


    public function delete_ruta($id){

     $this->db->where('id',$id);
  $this->db->delete('puntos_ruta');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

 public function eliminar_item_caracteristica($id){

     $this->db->where('idcaracteristica_playa',$id);
  $this->db->delete('caracteristicas_playas');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}



public function todas_las_regiones() {

    $this->db->select('*');
    $this->db->from('regiones');
    $query = $this->db->get();
    return $query->result();

}


public function region_por_id($id) {
    $condition = 'id_region = '.'"'.$id.'"';
    $this->db->select('*');
    $this->db->from('regiones');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

}

public function provincia_por_id($id) {
    $condition = 'idprovincia = '.'"'.$id.'"';
    $this->db->select('*');
    $this->db->from('provincias');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

}


public function caracteristicas_padre() {
  $condition = "oculto = 0";
    $this->db->select('*');
    $this->db->from('actividades_padre');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

}
      public function caracteristicas_padre_all2() {
  $this->db->select('*');
    $this->db->from('actividades_padre');
    $query = $this->db->get();
    return $query->result();
      }
public function caracteristicas_padre_all() {
    $this->db->select('*');
    $this->db->from('actividades_padre');
    $query = $this->db->get();
    return $query->result();

}

public function todas_las_caracteristicas() {

    $this->db->select('actividades.idactividad, actividades.detalle , actividades.icono, actividades_padre.detalle as padre,actividades.simple');
    $this->db->from('actividades');
    $this->db->join('actividades_padre','actividades_padre.id = actividades.actividad_padre');
    $this->db->order_by('actividades_padre.detalle asc');
    $query = $this->db->get();
    return $query->result();

}



    public function rutas_predisenadas() {

    $this->db->select('*');
    $this->db->from('puntos_ruta');
    $query = $this->db->get();
    return $query->result();

}


    public function ruta_predisenada($id) {

    $condition = 'id ='.'"'.$id.'"';
    $this->db->select('*');
    $this->db->from('puntos_ruta');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

    }




public function todas_las_caracteristicas_simples() {

    $condition = "actividades.simple = 1";
    $this->db->select('actividades.idactividad, actividades.detalle,actividades.detalle_en, actividades_padre.detalle as padre,actividades.simple');
    $this->db->from('actividades');
    $this->db->join('actividades_padre','actividades_padre.id = actividades.actividad_padre');
    $this->db->where($condition);
    $this->db->order_by('actividades_padre.detalle asc');
    $query = $this->db->get();
    return $query->result();

}


    public function todas_las_caracteristicas_por_padre($id) {

    $condition = "actividades.actividad_padre = ".$id;
    $this->db->select('actividades.idactividad, actividades.detalle, actividades.detalle_en, actividades_padre.detalle as padre,actividades.simple');
    $this->db->from('actividades');
    $this->db->join('actividades_padre','actividades_padre.id = actividades.actividad_padre');
    $this->db->where($condition);
    $this->db->order_by('actividades_padre.detalle asc');
    $query = $this->db->get();
    return $query->result();

}


    public function caracteristica_por_id($id) {
    $condition = 'idactividad='.'"'.$id.'"';
    $this->db->select('*');
    $this->db->from('actividades');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

}

       public function caracteristica_padre_por_id($id) {
    $condition = 'id='.'"'.$id.'"';
    $this->db->select('*');
    $this->db->from('actividades_padre');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

}

public function insertar_region($data){

    $this->db->insert('regiones', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

    public function insert_ruta($data){

    $this->db->insert('puntos_ruta', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}
 public function insertar_playa($data){

    $this->db->insert('playas', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function insertar_caracteristica($data){

    $this->db->insert('actividades', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}
    public function insertar_caracteristica_padre($data){

    $this->db->insert('actividades_padre', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function eliminar_region($id){

     $this->db->where('id_region',$id);
  $this->db->delete('regiones');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}



public function todas_las_playas() {
    $this->db->select('playas.nombre,playas.idplaya,playas.latitud,playas.longitud, provincias.nombre as provincia, regiones.nombre as region');
    $this->db->from('playas');
    $this->db->join('provincias','provincias.idprovincia = playas.idprovincia');
    $this->db->join('regiones','regiones.id_region = playas.idregion');
    $query = $this->db->get();
    return $query->result();

}



public function eliminar_playa($id){
    $this->db->where('idplaya',$id);
    $this->db->delete('playas');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}
    public function eliminar_caracteristica($id){
    $this->db->where('idactividad',$id);
    $this->db->delete('actividades');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}
    public function eliminar_caracteristica_padre($id){
    $this->db->where('id',$id);
    $this->db->delete('actividades_padre');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}



public function playa_por_id($id){

    $condition = "idplaya =" . "'" . $id . "'";
    $this->db->select('*');
    $this->db->from('playas');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();


    }

    public function actividades_por_playa($id){

    $condition = "caracteristicas_playas.id_playa =" . "'" . $id . "'";
    $this->db->select('caracteristicas_playas.idcaracteristica_playa,caracteristicas_playas.id_playa, caracteristicas_playas.id_caracteristica,actividades.icono, actividades.idactividad ,actividades.detalle, actividades.detalle_en');
    $this->db->from('caracteristicas_playas');
    $this->db->join('actividades','actividades.idactividad = caracteristicas_playas.id_caracteristica');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();


    }
 public function descp_carac($caracteristica,$playa){
    $condition = "idplaya =" . "'" . $playa . "' AND " . "idcaracteristica =". "'" .$caracteristica."'";
    $this->db->select('*');
    $this->db->from('detalles_caracteristicas');
    $this->db->where($condition);
    $this->db->limit(1);
    $query = $this->db->get();

    if ($query->num_rows() == 1) {
        return true;
    } else {
        return false;
    }
 }

public function descp_carac2($caracteristica,$playa){
    $condition = "idplaya =" . "'" . $playa . "' AND " . "idcaracteristica =". "'" .$caracteristica."'";
    $this->db->select('*');
    $this->db->from('detalles_caracteristicas');
    $this->db->where($condition);
    $this->db->limit(1);
    $query = $this->db->get();

   return $query->result();
 }


     public function descp_carac_add($data){

    $this->db->insert('detalles_caracteristicas', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

    }

    public function descp_carac_update($playa,$carac,$data){

        $condition ="idplaya = "."'".$playa."' AND idcaracteristica = "."'".$carac."'";

     $this->db->where($condition);
        $this->db->update('detalles_caracteristicas', $data);


        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
}

      public function descp_carac_delete($playa,$carac){

        $condition ="idplaya = "."'".$playa."' AND idcaracteristica = "."'".$carac."'";

        $this->db->where($condition);
        $this->db->delete('detalles_caracteristicas');

        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
}


public function c_datos($playa){

    $condition = "idplaya =" . "'" . $playa . "'";
    $this->db->select('idcaracteristica');
    $this->db->from('detalles_caracteristicas');
    $this->db->where($condition);
    $query = $this->db->get();

   return $query->result();
 }

public function galeria_playa($id){

    $condition = "idplaya =" . "'" . $id . "'";
    $this->db->select('*');
    $this->db->from('galeria_playas');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();


    }

public function actualizar_playa($id,$data){
$this->db->where('idplaya', $id);
$this->db->update('playas', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function update_ruta($id,$data){
$this->db->where('id', $id);
$this->db->update('puntos_ruta', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function update_caption($id,$data){
$this->db->where('id', $id);

$this->db->update('galeria_playas', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function update_position( $id, $data ){

	$this->db->where('id', $id);
	$this->db->update('ads', $data);


	if ($this->db->affected_rows() > 0) {

		return true;

	}else {

		return false;

	}

}


public function actualizar_categoria($id,$data){
$this->db->where('idactividad', $id);
$this->db->update('actividades', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}


public function actualizar_categoriapad($id,$data){
$this->db->where('id', $id);
$this->db->update('actividades_padre', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function actualizar_region($id,$data){
$this->db->where('id_region', $id);
$this->db->update('regiones', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}
public function actualizar_prov($id,$data){
$this->db->where('idprovincia', $id);
$this->db->update('provincias', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

  public function update_playa_image($data = array()){
 $update_data = array('imagen_destacada'=>$data['foto']);
   $this->db->where('idplaya', $data['id']);
    $this->db->update('playas', $update_data);

    return true;
 }

    public function agregar_item_actividad($data){
         $this->db->insert('caracteristicas_playas', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

    }


}

?>
