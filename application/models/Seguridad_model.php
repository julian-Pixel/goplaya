<?php

Class Seguridad_model extends CI_Model {

public function generar_codigo() {
    $characters = 'abcdefghijklmnopqrstuvwxyz0123456789$%#ABCEDFGHIJKLMNOPQRSTUVWXYZ';
    $random_string_length = 10;
    $string = '';
    $max = strlen($characters) - 1;
    for ($i = 0; $i < $random_string_length; $i++) {
      $string .= $characters[mt_rand(0, $max)];
    }
    return $string; 
}
 
  public function fecha_vencimiento($fecha){
    $nuevafecha = strtotime ( '+60 minute' , strtotime ( $fecha ) ) ;
    $nuevafecha = date ( 'Y-m-d h:m:s' , $nuevafecha );
    return  $nuevafecha;
  } 
    
    public function usuario($email){
        
    $condition = "email= " . "'" . $email . "'";
    $this->db->select('*');
    $this->db->from('usuarios');
    $this->db->where($condition);
        
    $query = $this->db->get();   
    return $query->result();  
    }
    
    
    public function check_usuario($email){
        
        $condition = "email= " . "'" . $email . "'";
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    
    public function insertar_codigo($data){
        
        $this->db->insert('codigos_seguridad', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }
    
    public function actualizar_contrasena($id,$data){
        $this->db->where('idusuario', $id);
        $this->db->update('usuarios', $data); 
        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }
    
    public function datos_codigo($codigo){
        
          $condition = "codigo= " . "'" . $codigo . "'";
        $this->db->select('*');
        $this->db->from('codigos_seguridad');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();  
        } else {
            return false;
        }
        
        
    }
    public function verificar_codigo($codigo, $fa, $fc){
        
        $condition = "codigo= " . "'" . $codigo . "' AND estado = 0 and fecha_maxima BETWEEN "."'" . $fc . "' AND "."'" . $fa . "'";
        $this->db->select('*');
        $this->db->from('codigos_seguridad');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        
        //var_dump($query->result());
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
        
    }

}

?>