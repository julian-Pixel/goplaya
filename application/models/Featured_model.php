<?php

Class Featured_model extends CI_Model {



// Read data from database to show data in admin page
public function fetured() {

    $this->db->select('*');
    $this->db->from('featured');
    $this->db->limit('6');
    $this->db->order_by('orden ASC');
    $query = $this->db->get();
    return $query->result();

}

public function fetured_by_id($id) {
$condition = 'id = '.'"'.$id.'"';
    $this->db->select('*');
    $this->db->from('featured');
    $this->db->where($condition);
    $this->db->limit('1');
    $this->db->order_by('orden ASC');
    $query = $this->db->get();
    return $query->result();

}

public function delmes() {

    $this->db->select('playas_del_mes.id,playas_del_mes.zona_es,playas.*');
    $this->db->from('playas_del_mes');
      $this->db->join('playas','playas.idplaya = playas_del_mes.id_playa');
    $this->db->limit('12');
    $query = $this->db->get();
    return $query->result();

}
public function populares() {

    $this->db->select('playas_populares.id,playas_populares.zona_es,playas.*');
    $this->db->from('playas_populares');
      $this->db->join('playas','playas.idplaya = playas_populares.id_playa');
    $this->db->limit('12');
    $query = $this->db->get();
    return $query->result();

}


public function insertar_item_galeria($data){

    $this->db->insert('galeria_playas', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}





public function eliminar_playa_mes($id){

     $this->db->where('id',$id);
  $this->db->delete('playas_del_mes');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}



public function eliminar_playa_popular($id){

     $this->db->where('id',$id);
  $this->db->delete('playas_populares');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}




public function eliminar_item_galeria($id){

     $this->db->where('id',$id);
  $this->db->delete('galeria_playas');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function add_playa_mes($data){

$this->db->insert('playas_del_mes', $data);

if ($this->db->affected_rows() > 0) {
return true;
}else {
return false;
}

}

public function add_playa_popular($data){

$this->db->insert('playas_populares', $data);

if ($this->db->affected_rows() > 0) {
return true;
}else {
return false;
}

}

public function eliminar_item_ads($id){

$this->db->where('id',$id);
$this->db->delete('ads');

if ($this->db->affected_rows() > 0) {
return true;
}else {
return false;
}

}

public function ads_playa($id){

$condition = "idplaya =" . "'" . $id . "'";
$this->db->select('*');
$this->db->from('ads');
$this->db->where($condition);
$query = $this->db->get();
return $query->result();


}


/// Fin de ADS


    public function delete_ruta($id){

     $this->db->where('id',$id);
  $this->db->delete('puntos_ruta');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

 public function eliminar_item_caracteristica($id){

     $this->db->where('idcaracteristica_playa',$id);
  $this->db->delete('caracteristicas_playas');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}



public function todas_las_regiones() {

    $this->db->select('*');
    $this->db->from('regiones');
    $query = $this->db->get();
    return $query->result();

}


public function region_por_id($id) {
    $condition = 'id_region = '.'"'.$id.'"';
    $this->db->select('*');
    $this->db->from('regiones');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

}

public function provincia_por_id($id) {
    $condition = 'idprovincia = '.'"'.$id.'"';
    $this->db->select('*');
    $this->db->from('provincias');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

}


public function caracteristicas_padre() {
  $condition = "oculto = 0";
    $this->db->select('*');
    $this->db->from('actividades_padre');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

}
      public function caracteristicas_padre_all2() {
  $this->db->select('*');
    $this->db->from('actividades_padre');
    $query = $this->db->get();
    return $query->result();
      }
public function caracteristicas_padre_all() {
    $this->db->select('*');
    $this->db->from('actividades_padre');
    $query = $this->db->get();
    return $query->result();

}

public function todas_las_caracteristicas() {

    $this->db->select('actividades.idactividad, actividades.detalle , actividades.icono, actividades_padre.detalle as padre,actividades.simple');
    $this->db->from('actividades');
    $this->db->join('actividades_padre','actividades_padre.id = actividades.actividad_padre');
    $this->db->order_by('actividades_padre.detalle asc');
    $query = $this->db->get();
    return $query->result();

}



    public function rutas_predisenadas() {

    $this->db->select('*');
    $this->db->from('puntos_ruta');
    $query = $this->db->get();
    return $query->result();

}


    public function ruta_predisenada($id) {

    $condition = 'id ='.'"'.$id.'"';
    $this->db->select('*');
    $this->db->from('puntos_ruta');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

    }




public function todas_las_caracteristicas_simples() {

    $condition = "actividades.simple = 1";
    $this->db->select('actividades.idactividad, actividades.detalle,actividades.detalle_en, actividades_padre.detalle as padre,actividades.simple');
    $this->db->from('actividades');
    $this->db->join('actividades_padre','actividades_padre.id = actividades.actividad_padre');
    $this->db->where($condition);
    $this->db->order_by('actividades_padre.detalle asc');
    $query = $this->db->get();
    return $query->result();

}


    public function todas_las_caracteristicas_por_padre($id) {

    $condition = "actividades.actividad_padre = ".$id;
    $this->db->select('actividades.idactividad, actividades.detalle, actividades.detalle_en, actividades_padre.detalle as padre,actividades.simple');
    $this->db->from('actividades');
    $this->db->join('actividades_padre','actividades_padre.id = actividades.actividad_padre');
    $this->db->where($condition);
    $this->db->order_by('actividades_padre.detalle asc');
    $query = $this->db->get();
    return $query->result();

}


    public function caracteristica_por_id($id) {
    $condition = 'idactividad='.'"'.$id.'"';
    $this->db->select('*');
    $this->db->from('actividades');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

}

       public function caracteristica_padre_por_id($id) {
    $condition = 'id='.'"'.$id.'"';
    $this->db->select('*');
    $this->db->from('actividades_padre');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();

}

public function insertar_region($data){

    $this->db->insert('regiones', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

    public function insert_ruta($data){

    $this->db->insert('puntos_ruta', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}
 public function insertar_playa($data){

    $this->db->insert('playas', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function insertar_caracteristica($data){

    $this->db->insert('actividades', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}
    public function insertar_caracteristica_padre($data){

    $this->db->insert('actividades_padre', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function eliminar_region($id){

     $this->db->where('id_region',$id);
  $this->db->delete('regiones');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}



public function todas_las_playas() {
    $this->db->select('playas.nombre,playas.idplaya,playas.latitud,playas.longitud, provincias.nombre as provincia, regiones.nombre as region');
    $this->db->from('playas');
    $this->db->join('provincias','provincias.idprovincia = playas.idprovincia');
    $this->db->join('regiones','regiones.id_region = playas.idregion');
    $query = $this->db->get();
    return $query->result();

}



public function eliminar_playa($id){
    $this->db->where('idplaya',$id);
    $this->db->delete('playas');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}
    public function eliminar_caracteristica($id){
    $this->db->where('idactividad',$id);
    $this->db->delete('actividades');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}
    public function eliminar_caracteristica_padre($id){
    $this->db->where('id',$id);
    $this->db->delete('actividades_padre');

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }


}



public function playa_por_id($id){

    $condition = "idplaya =" . "'" . $id . "'";
    $this->db->select('*');
    $this->db->from('playas');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();


    }

    public function actividades_por_playa($id){

    $condition = "caracteristicas_playas.id_playa =" . "'" . $id . "'";
    $this->db->select('caracteristicas_playas.idcaracteristica_playa,caracteristicas_playas.id_playa, caracteristicas_playas.id_caracteristica,actividades.icono, actividades.idactividad ,actividades.detalle, actividades.detalle_en');
    $this->db->from('caracteristicas_playas');
    $this->db->join('actividades','actividades.idactividad = caracteristicas_playas.id_caracteristica');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();


    }
 public function descp_carac($caracteristica,$playa){
    $condition = "idplaya =" . "'" . $playa . "' AND " . "idcaracteristica =". "'" .$caracteristica."'";
    $this->db->select('*');
    $this->db->from('detalles_caracteristicas');
    $this->db->where($condition);
    $this->db->limit(1);
    $query = $this->db->get();

    if ($query->num_rows() == 1) {
        return true;
    } else {
        return false;
    }
 }

public function descp_carac2($caracteristica,$playa){
    $condition = "idplaya =" . "'" . $playa . "' AND " . "idcaracteristica =". "'" .$caracteristica."'";
    $this->db->select('*');
    $this->db->from('detalles_caracteristicas');
    $this->db->where($condition);
    $this->db->limit(1);
    $query = $this->db->get();

   return $query->result();
 }


     public function descp_carac_add($data){

    $this->db->insert('detalles_caracteristicas', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

    }

    public function descp_carac_update($playa,$carac,$data){

        $condition ="idplaya = "."'".$playa."' AND idcaracteristica = "."'".$carac."'";

     $this->db->where($condition);
        $this->db->update('detalles_caracteristicas', $data);


        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
}

      public function descp_carac_delete($playa,$carac){

        $condition ="idplaya = "."'".$playa."' AND idcaracteristica = "."'".$carac."'";

        $this->db->where($condition);
        $this->db->delete('detalles_caracteristicas');

        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
}


public function c_datos($playa){

    $condition = "idplaya =" . "'" . $playa . "'";
    $this->db->select('idcaracteristica');
    $this->db->from('detalles_caracteristicas');
    $this->db->where($condition);
    $query = $this->db->get();

   return $query->result();
 }

public function galeria_playa($id){

    $condition = "idplaya =" . "'" . $id . "'";
    $this->db->select('*');
    $this->db->from('galeria_playas');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();


    }

public function update_featured($id,$data){
$this->db->where('id', $id);
$this->db->update('featured', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function update_ruta($id,$data){
$this->db->where('id', $id);
$this->db->update('puntos_ruta', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function update_caption($id,$data){
$this->db->where('id', $id);

$this->db->update('galeria_playas', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function update_position( $id, $data ){

	$this->db->where('id', $id);
	$this->db->update('ads', $data);


	if ($this->db->affected_rows() > 0) {

		return true;

	}else {

		return false;

	}

}


public function actualizar_categoria($id,$data){
$this->db->where('idactividad', $id);
$this->db->update('actividades', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}


public function actualizar_categoriapad($id,$data){
$this->db->where('id', $id);
$this->db->update('actividades_padre', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function actualizar_region($id,$data){
$this->db->where('id_region', $id);
$this->db->update('regiones', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}
public function actualizar_prov($id,$data){
$this->db->where('idprovincia', $id);
$this->db->update('provincias', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

  public function update_playa_image($data = array()){
 $update_data = array('imagen_destacada'=>$data['foto']);
   $this->db->where('idplaya', $data['id']);
    $this->db->update('playas', $update_data);

    return true;
 }

    public function agregar_item_actividad($data){
         $this->db->insert('caracteristicas_playas', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

    }


}

?>
