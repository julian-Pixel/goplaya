<?php

Class Tour_model extends CI_Model {


    public function configurar_operadora($data){

        $this->db->insert('tour_operadoras', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        }
    }

    public function check_operadora($id){

    $condition = "id_usuario =" . "'" . $id . "'";
    $this->db->select('*');
    $this->db->from('tour_operadoras');
    $this->db->where($condition);
    $this->db->limit(1);
    $query = $this->db->get();

    if ($query->num_rows() == 1) {
        return true;
    } else {
        return false;
    }


    }


    public function todas_las_operadoras(){
        $condition = "estado = 1";
        $this->db->select('*');
        $this->db->from('tour_operadoras');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();
    }

    public function todas_las_tour_operadoras(){

        $this->db->select('*');
        $this->db->from('tour_operadoras');
        $query = $this->db->get();
        return $query->result();
    }

    public function tour_operadora_id($id) {
          $condition = "idtour_operadora =" . "'" . $id . "'";
          $this->db->select('*');
          $this->db->from('tour_operadoras');
          $this->db->where($condition);
          $query = $this->db->get();
          return $query->result();
      }




    public function generar_playas($s) {

        $condition = "(provincias.nombre LIKE " . "'%" . $s . "%') OR (playas.nombre LIKE ". "'%" . $s . "%')";
        $this->db->select('playas.idplaya,playas.nombre,playas.slug,playas.descripcion,playas.latitud,playas.longitud, provincias.nombre as provincia');
        $this->db->from('tours');
        $this->db->join('playas','tours.id_playa = playas.idplaya');
        $this->db->join('provincias','provincias.idprovincia = playas.idprovincia');
        $this->db->where($condition);

        $query = $this->db->get();


            return $query->result();

    }


    public function generar_playas_all() {


        $this->db->select('playas.idplaya,playas.nombre,playas.slug,playas.descripcion,playas.latitud,playas.longitud, provincias.nombre as provincia');
        $this->db->from('tours');
        $this->db->join('playas','tours.id_playa = playas.idplaya');
        $this->db->join('provincias','provincias.idprovincia = playas.idprovincia');


        $query = $this->db->get();


            return $query->result();

    }

    public function todas_los_tipos_de_tour() {

        $this->db->select('*');
        $this->db->from('tipotours');
        $query = $this->db->get();
        return $query->result();

    }


    public function todas_las_dudas() {

        $this->db->select('*');
        $this->db->from('dudas');
        $query = $this->db->get();
        return $query->result();

    }

    public function datos_operadora($id){

        $condition = "id_usuario =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('tour_operadoras');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }


    }


    public function comentarios($id){

        $condition = "id_tour =" ."'". $id . "'";
        $this->db->select('*');
        $this->db->from('comentarios');
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }


    }


    public function agregar_tour($data){

        $this->db->insert('tours', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        }
    }

    public function insertar_tipo_tour($data){
        $this->db->insert('tipotours', $data);
        if ($this->db->affected_rows() > 0) {
           return true;
        }else {
           return false;
        }
    }

    public function editar_tipo_tour($id,$data){

        $this->db->where('idactividad', $id);
        $this->db->update('tipotours', $data);
          if ($this->db->affected_rows() > 0) {
                return true;
            }else {
            return false;
            }
    }


    public function editar_dudas_tour($id,$data){

        $this->db->where('id', $id);
        $this->db->update('dudas', $data);
          if ($this->db->affected_rows() > 0) {
                return true;
            }else {
            return false;
            }
    }


    public function editar_iconos_tour($id,$data){

        $this->db->where('id', $id);
        $this->db->update('iconos_tours', $data);
          if ($this->db->affected_rows() > 0) {
                return true;
            }else {
            return false;
            }
    }


    public function editar_como_subir_video($id,$data){

        $this->db->where('id', $id);
        $this->db->update('como_subir_tour', $data);
          if ($this->db->affected_rows() > 0) {
                return true;
            }else {
            return false;
            }
    }


    public function insertar_dudas_tour($data){
        $this->db->insert('dudas', $data);
        if ($this->db->affected_rows() > 0) {
           return true;
        }else {
           return false;
        }
    }


    public function eliminar_dudas($id){

        $this->db->where('id', $id);
        $this->db->delete('dudas');
       if ($this->db->affected_rows() > 0) {
                return true;
            } else {
                return false;
            }

    }


    public function todos_los_videos(){
        $this->db->select('*');
        $this->db->from('como_subir_tour');
        $query = $this->db->get();
        return $query->result();
    }

    public function todos_los_iconos(){
        $this->db->select('*');
        $this->db->from('iconos_tours');
        $query = $this->db->get();
        return $query->result();
    }

    public function insertar_icono_tour($data){
        $this->db->insert('iconos_tours', $data);
        if ($this->db->affected_rows() > 0) {
           return true;
        }else {
           return false;
        }
    }

    public function eliminar_icono_tour($id){

        $this->db->where('id', $id);
        $this->db->delete('iconos_tours');
       if ($this->db->affected_rows() > 0) {
                return true;
            } else {
                return false;
            }

    }



    public function tipos_de_tour_id($id) {
          $condition = "idactividad =" . "'" . $id . "'";
          $this->db->select('*');
          $this->db->from('tipotours');
          $this->db->where($condition);
          $query = $this->db->get();
          return $query->result();
      }


      public function dudas_de_tour_id($id) {
            $condition = "id =" . "'" . $id . "'";
            $this->db->select('*');
            $this->db->from('dudas');
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }



        public function iconos_de_tour_id($id) {
              $condition = "id =" . "'" . $id . "'";
              $this->db->select('*');
              $this->db->from('iconos_tours');
              $this->db->where($condition);
              $query = $this->db->get();
              return $query->result();
          }




          public function como_subir_videos_tours_id($id) {
                $condition = "id =" . "'" . $id . "'";
                $this->db->select('*');
                $this->db->from('como_subir_tour');
                $this->db->where($condition);
                $query = $this->db->get();
                return $query->result();
            }




    public function touroperadora_insert($data){

        $this->db->insert('tour_operadoras', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        }
    }

    public function mis_tours($id){
        $condition = "id_usuario =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('tours');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();
    }
    public function reservaciones_tour($id){
        $condition = "tours.id_usuario =" . "'" . $id . "'";
        $this->db->select('tours.nombre as tour_nombre ,reservaciones_tour.*');
        $this->db->from('reservaciones_tour');
        $this->db->join('tours','tours.idtour = reservaciones_tour.id_tour');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();
    }
    public function reservaciones($id){
        $condition = "reservaciones_tour.estado = 0 AND reservaciones_tour.comision = 0 AND tours.id_usuario ="  . "'" . $id . "'" ;
        $this->db->select('tours.nombre as tour_nombre ,reservaciones_tour.*,tour_operadoras.codigo as codt, tour_operadoras.nombre as nomt');
        $this->db->from('reservaciones_tour');
        $this->db->join('tours','tours.idtour = reservaciones_tour.id_tour');
        $this->db->join('tour_operadoras','tours.id_usuario = tour_operadoras.id_usuario');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();
    }
    public function reservaciones_admin($id){
        $condition = "reservaciones_tour.estado = 0 AND reservaciones_tour.comision = 0";
        $this->db->select('tours.nombre as tour_nombre ,reservaciones_tour.*,tour_operadoras.codigo as codt, tour_operadoras.nombre as nomt');
        $this->db->from('reservaciones_tour');
        $this->db->join('tours','tours.idtour = reservaciones_tour.id_tour');
        $this->db->join('tour_operadoras','tours.id_usuario = tour_operadoras.id_usuario');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();
    }
    public function reservacionesyap($id){
         $condition = "reservaciones_tour.estado = 1 AND reservaciones_tour.comision = 1 AND tour_operadoras.id_usuario"  . "'" . $id . "'" ;
        $this->db->select('tours.nombre as tour_nombre ,reservaciones_tour.*,tour_operadoras.codigo as codt, tour_operadoras.nombre as nomt');
        $this->db->from('reservaciones_tour');
        $this->db->join('tours','tours.idtour = reservaciones_tour.id_tour');
        $this->db->join('tour_operadoras','tour_operadoras.id_usuario = tours.id_usuario');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();
    }
    public function reservacionesyap_admin($id){
         $condition = "reservaciones_tour.estado = 1 AND reservaciones_tour.comision = 1";
        $this->db->select('tours.nombre as tour_nombre ,reservaciones_tour.*,tour_operadoras.codigo as codt, tour_operadoras.nombre as nomt');
        $this->db->from('reservaciones_tour');
        $this->db->join('tours','tours.idtour = reservaciones_tour.id_tour');
        $this->db->join('tour_operadoras','tour_operadoras.id_usuario = tours.id_usuario');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();
    }

    public function cupos_tour($id){
        $condition = "id_tour =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('cupo_tours');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();
    }

    public function cupo_max($id){
        $condition = "id_tour =" . "'" . $id . "'";
        $this->db->select('max(id) as id');
        $this->db->from('cupo_tours');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();
    }


    public function precio_max($id){
        $condition = "idtour =" . "'" . $id . "'";
        $this->db->select('max(id) as id');
        $this->db->from('precios_tours');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();
    }



    public function tours_por_id($id){
        $condition = "idtour =" . "'" . $id . "'";
        $this->db->select('tours.*,tour_operadoras.nombre as nombre_to');
        $this->db->from('tours');
        $this->db->where($condition);
        $this->db->join('tour_operadoras','tours.id_usuario = tour_operadoras.id_usuario');
        $query = $this->db->get();
        return $query->result();
    }

    public function precios_tour($id){
        $condition = "idtour =" . "'" . $id . "' and estado = 1";
        $this->db->select('*');
        $this->db->from('precios_tours');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();
    }

    public function tours_disponibles(){
        $condition = "estado = '1'";
        $this->db->select('tours.*,provincias.nombre as provincia');
        $this->db->from('tours');
        $this->db->where($condition);
        $this->db->order_by('porcetaje_descuento desc');
        $this->db->join('playas','tours.id_playa = playas.idplaya');
        $this->db->join('provincias','playas.idprovincia = provincias.idprovincia');
        $query = $this->db->get();
        return $query->result();

    }

    public function tours_disponibles_top(){
        $condition = "estado = '1'";
        $this->db->select('tours.*,provincias.nombre as provincia');
        $this->db->from('tours');
        $this->db->where($condition);
        $this->db->order_by('porcetaje_descuento desc');
          $this->db->join('playas','tours.id_playa = playas.idplaya');
          $this->db->join('provincias','playas.idprovincia = provincias.idprovincia');
        $this->db->limit(4);
        $query = $this->db->get();
        return $query->result();

    }

    public function tours_disponibles_destacados(){
        $condition = "tours.estado = '1' and tours.destacado = 1";
        $this->db->select('tours.*,provincias.idprovincia as id_provincia, provincias.nombre as nombre_provincia');
        $this->db->from('tours');
        $this->db->where($condition);
        $this->db->join('playas','tours.id_playa = playas.idplaya');
        $this->db->join('provincias','playas.idprovincia = provincias.idprovincia');
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result();

    }


    public function tours_disponibles_destacados_where($s) {

        $condition = "(provincias.nombre LIKE " . "'%" . $s . "%') OR (tours.nombre LIKE ". "'%" . $s . "%')";
        $this->db->select('tours.*,provincias.idprovincia as id_provincia, provincias.nombre as nombre_provincia');
        $this->db->from('tours');
        $this->db->where($condition);
        $this->db->join('playas','tours.id_playa = playas.idplaya');
        $this->db->join('provincias','playas.idprovincia = provincias.idprovincia');
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result();

    }


    public function tours_disponibles_por_playa($id_playa){
        $condition = "id_playa =" . "'" . $id_playa . "' and estado = 1";
        $this->db->select('*');
        $this->db->from('tours');
        $this->db->where($condition);
        $this->db->order_by('porcetaje_descuento desc');
        $query = $this->db->get();
        return $query->result();

    }

    public function tours_all(){
        $this->db->select('*');
        $this->db->from('tours');
        $query = $this->db->get();
        return $query->result();
    }


    public function tours_all_join(){
        $this->db->select('tour_operadoras.nombre as nombre_touroperadora,
                           tour_operadoras.codigo,
                           tours.idtour,
                           tours.nombre as nombre_tour,
                           tours.estado,
                           tours.habilitado');
        $this->db->from('tour_operadoras');
        $this->db->join('tours','tours.id_usuario = tour_operadoras.id_usuario');
        $query = $this->db->get();
        return $query->result();
    }

    public function tours_disponibles_tipo($cat){
        $condition = "estado = '1' AND(cate1 = ".$cat. " OR cate2 = ".$cat. " )";
        $this->db->select('*');
        $this->db->from('tours');
        $this->db->where($condition);
        $this->db->order_by('porcetaje_descuento desc');
        $query = $this->db->get();
        return $query->result();

    }

    public function tours_disponibles_like($s){


      $condition = "(provincias.nombre LIKE " . "'%" . $s . "%') OR (playas.nombre LIKE ". "'%" . $s . "%')";

        $this->db->select('tours.*');
        $this->db->from('tours');
        $this->db->where($condition);
        $this->db->join('playas','tours.id_playa = playas.idplaya');
        $this->db->join('provincias','provincias.idprovincia = playas.idprovincia');
        $this->db->order_by('tours.porcetaje_descuento desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function insertar_item_galeria($data){

        $this->db->insert('galeria_tours', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
        return false;
        }

    }

    public function galeria_tour($id){

        $condition = "idtour =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('galeria_tours');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();


        }



    public function verificar_tour($tour,$date){
        $condition = "fecha = '".$date."' AND id_tour=".$tour;
        $this->db->select('*');
        $this->db->from('cupo_tours');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();

    }

    public function verificar_horas_tour($tour,$date){
        $condition = "fecha = '".$date." 'AND cupos > 0 AND id_tour=".$tour;
        $this->db->select('*');
        $this->db->from('cupo_tours');
        $this->db->where($condition);
        $this->db->order_by('hora ASC');
        $query = $this->db->get();
        return $query->result();

    }

    public function verificar_cupo_horas_tour($tour,$date,$hora){
        $condition = "fecha = '".$date." 'AND hora='".$hora."'  AND id_tour=".$tour;
        $this->db->select('*');
        $this->db->from('cupo_tours');
        $this->db->where($condition);
        $this->db->order_by('hora ASC');
        $query = $this->db->get();
        return $query->result();

    }

    public function ver_reserva($id){
        $condition = "idreservacion = '".$id."'";
        $this->db->select('*');
        $this->db->from('reservaciones_tour');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();

    }
    public function fechas_disponibles_tour($tour){
        $condition = "id_tour = '".$tour."'";
        $this->db->select('*');
        $this->db->from('cupo_tours');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();

    }

    public function reservar($data){
      $this->db->insert('reservaciones_tour', $data);
      if ($this->db->affected_rows() > 0) {
         return true;
      }else {
         return false;
      }

    }
public function agregar_cupo($data){
      $this->db->insert('cupo_tours', $data);
      if ($this->db->affected_rows() > 0) {
         return true;
      }else {
         return false;
      }

    }

    public function agregar_precio($data){
          $this->db->insert('precios_tours', $data);
          if ($this->db->affected_rows() > 0) {
             return true;
          }else {
             return false;
          }

        }


public function actualizar_info_tour($id,$data){
$this->db->where('idtour', $id);
$this->db->update('tours', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function touroperadora_update($data, $id){

  $this->db->where('idtour_operadora', $id);
  $this->db->update('tour_operadoras', $data);
    if ($this->db->affected_rows() > 0) {
          return true;
      }else {
      return false;
      }
}

public function comision($id,$data){
$this->db->where('id', $id);
$this->db->update('reservaciones_tour', $data);
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

public function update_cantidad_tour($tour,$data ){

  $this->db->where('id', $tour);
  $this->db->update('cupo_tours', $data);
  return true;
}

public function update_tour_image($data = array()){
  $update_data = array('imagen'=>$data['foto']);
  $this->db->where('idtour', $data['id']);
  $this->db->update('tours', $update_data);
  return true;
 }


 public function update_caption_tour($id,$data){
 $this->db->where('id', $id);

 $this->db->update('galeria_tours', $data);
   if ($this->db->affected_rows() > 0) {
         return true;
     }else {
     return false;
     }

 }





}

?>
