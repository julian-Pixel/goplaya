<?php

Class Publicidad_tours_model extends CI_Model {

  public function insertar_item($data){
      $this->db->insert('ads_tours', $data);
      if ($this->db->affected_rows() > 0) {
         return true;
      }else {
         return false;
      }
  }


  public function banners(){
      $this->db->select('*');
      $this->db->from('ads_tours');
      $query = $this->db->get();
      return $query->result();
  }

  public function banners_posicion($id){
  $condition = 'posicion = '.'"'.$id.'"';
  $this->db->select('*');
  $this->db->from('ads_tours');
  $this->db->where($condition);
  $query = $this->db->get();
  return $query->result();
  }

  public function eliminar_slide($id){

      $this->db->where('id', $id);
      $this->db->delete('ads_tours');
     if ($this->db->affected_rows() > 0) {
              return true;
          } else {
              return false;
          }

  }

}

?>
