<?php

Class Slider_model extends CI_Model {

 public function insertar_slide($data){
     $this->db->insert('slider', $data);
     if ($this->db->affected_rows() > 0) {
        return true;
     }else {
        return false;
     }  
 }  
    
public function slides(){
    $this->db->select('*');
    $this->db->from('slider');
    $this->db->order_by('orden ASC');
    $query = $this->db->get();
    return $query->result();
}    
    
public function ordenar_slides($data = array()){    
    $orden = array(
    'orden'=> $data['orden'] 
    );
    
    $this->db->where('id', $data['id']);
    $this->db->update('slider', $orden);
    
    if ($this->db->affected_rows() > 0) {
        return true;
    }else{
        return false;   

    }   
}    

    
        
public function eliminar_slide($id){
    
    $this->db->where('id', $id);
    $this->db->delete('slider'); 
   if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }  
    
}
    
    

}

?>