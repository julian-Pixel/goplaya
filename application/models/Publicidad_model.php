 <?php

Class Publicidad_model extends CI_Model {

 public function insertar_item($data){
     $this->db->insert('ads', $data);
     if ($this->db->affected_rows() > 0) {
        return true;
     }else {
        return false;
     }
 }

public function banners(){
    $this->db->select('*');
    $this->db->from('ads');
    $query = $this->db->get();
    return $query->result();
}


    public function banners_id($id){
    $condition = 'idplaya = '.'"'.$id.'"';
    $this->db->select('*');
    $this->db->from('ads');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();
}

public function banners_posicion($id){
$condition = 'posicion = '.'"'.$id.'"';
$this->db->select('*');
$this->db->from('ads');
$this->db->where($condition);
$query = $this->db->get();
return $query->result();
}

public function ordenar_slides($data = array()){
    $orden = array(
    'orden'=> $data['orden']
    );

    $this->db->where('id', $data['id']);
    $this->db->update('slider', $orden);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else{
        return false;

    }
}



public function eliminar_slide($id){

    $this->db->where('id', $id);
    $this->db->delete('ads');
   if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }

}



}

?>
