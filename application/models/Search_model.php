<?php

Class Search_model extends CI_Model {

public function generar_playas($s) {

    $condition = "(provincias.nombre LIKE " . "'%" . $s . "%') OR (playas.nombre LIKE ". "'%" . $s . "%')";
    $this->db->select('playas.idplaya,playas.nombre,playas.slug,playas.descripcion,playas.latitud,playas.longitud, provincias.nombre as provincia');
    $this->db->from('playas');
    $this->db->join('provincias','provincias.idprovincia = playas.idprovincia');
    $this->db->where($condition);
    
    $query = $this->db->get();

   
        return $query->result();
   
}
    

public function buscar_playas($data){
    
  
    $condition = $data['where'];
    
    $this->db->select(' playas.idplaya,playas.slug,playas.latitud, playas.longitud,playas.descripcion, playas.nombre,provincias.nombre as provincia, regiones.nombre as region, playas.imagen_destacada, (6371 * ACOS( 
                                SIN(RADIANS(playas.latitud)) * SIN(RADIANS('.$data['latitud'].')) 
                                + COS(RADIANS(playas.longitud - '.$data['longitud'].')) * COS(RADIANS(playas.latitud)) 
                                * COS(RADIANS('.$data['latitud'].'))
                                )
    ) AS distance');
    $this->db->from('playas');
    $this->db->join('provincias','provincias.idprovincia = playas.idprovincia');
    $this->db->join('regiones','regiones.id_region = playas.idregion');
    $this->db->join('caracteristicas_playas','caracteristicas_playas.id_playa = playas.idplaya');
    $this->db->where($condition);
    $this->db->order_by('playas.nombre asc');
    $this->db->group_by('playas.idplaya');
    $this->db->having('distance < '.$data['radio']); 
    
    $query = $this->db->get();
    return $query->result();

    
    
    
}    
    
    
    public function buscar_playas_ctr($data){
    
  

        
        if(isset($data['where'])){
         $condition = $data['where'];  
        }
    
    $this->db->select(' playas.idplaya,playas.latitud, playas.longitud,playas.descripcion, playas.nombre,provincias.nombre as provincia, regiones.nombre as region, playas.imagen_destacada, (6371 * ACOS( 
                                SIN(RADIANS(playas.latitud)) * SIN(RADIANS('.$data['latitud'].')) 
                                + COS(RADIANS(playas.longitud - '.$data['longitud'].')) * COS(RADIANS(playas.latitud)) 
                                * COS(RADIANS('.$data['latitud'].'))
                                )
                   ) AS distance');
    
    
    $this->db->from('playas');
    $this->db->join('provincias','provincias.idprovincia = playas.idprovincia');
    $this->db->join('regiones','regiones.id_region = playas.idregion');
    $this->db->join('caracteristicas_playas','caracteristicas_playas.id_playa = playas.idplaya');
        if(isset($data['where'])){
        $this->db->where($condition); 
        }
    
    $this->db->group_by('playas.idplaya');
    $this->db->having('distance < '.$data['radio']); 
    
    $query = $this->db->get();
    return $query->result();

    
    
    
}  
    

}

?>