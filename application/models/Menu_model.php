<?php

Class Menu_model extends CI_Model {


    public function menus(){
        
    $this->db->select('*');
    $this->db->from('menus');
    $query = $this->db->get();   
    return $query->result();  
    }
    
    public function menu_items($id){
    $condition = 'idmenu = '."'".$id."'";
    $this->db->select('*');
    $this->db->from('items_menu');
    $this->db->where($condition);
    $this->db->order_by('orden ASC');
    $query = $this->db->get();   
    return $query->result();  
    }
    
    public function eliminar_item($id){
    
    $this->db->where('id', $id);
    $this->db->delete('items_menu'); 
   if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }  
    
}
    public function insert_item($data){
        
         $this->db->insert('items_menu', $data);
    
    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }
        
        
    }
    
    public function ordenar_items($data = array()){    
    $orden = array(
    'orden'=> $data['orden'] 
    );
    
    $this->db->where('id', $data['id']);
    $this->db->update('items_menu', $orden);
    
    if ($this->db->affected_rows() > 0) {
        return true;
    }else{
        return false;   

    }   
}   
    
   public function update_item($id,$data = array()){    
   
    
    $this->db->where('id', $id);
    $this->db->update('items_menu', $data);
    
    if ($this->db->affected_rows() > 0) {
        return true;
    }else{
        return false;   

    }   
}   
    
    
    

}

?>