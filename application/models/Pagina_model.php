<?php

Class Pagina_model extends CI_Model {
    
    public function plantillas(){
        
        $plantillas = array();
           $directorio = opendir("./application/views/plantillas"); //ruta actual
while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
{
    
    
    if (is_dir($archivo))//verificamos si es o no un directorio
    {
        
    }
    else
    {
        
       $archivo = explode(".php", $archivo);
           $plantillas[]=$archivo[0];
        
    }
}

     return  $plantillas ;   
        
        
    }
    
    
 public function crear_pagina($data){
     
     $this->db->insert('paginas', $data);
    
    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }
     
     
 }
    
    
public function editar_pagina($id,$data){
    
  
$this->db->where('id', $id);
$this->db->update('paginas', $data); 
  if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }   
    
} 
 
  public function datos_pagina($slug){
        $condition = "slug =" . "'" . $slug . "'";
        $this->db->select('*');
        $this->db->from('paginas');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();     
 }  
    
public function todas_las_paginas(){
        $this->db->select('*');
        $this->db->from('paginas');
        $query = $this->db->get();
        return $query->result();     
 }  
    
    
public function insertar_banner($id,$data){
        
    $this->db->where('id', $id);
    $this->db->update('paginas', $data); 
    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
        return false;
    }   
}  
    
 public function articulos_home(){
      $condition = "categoria = 1 and mostrar_home = 1";
        $this->db->select('*');
        $this->db->from('paginas');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();         
 }
    
    
public function contenido_pagina($slug){   
    $condition = "slug = "."'".$slug."'";
    $this->db->select('*');
    $this->db->from('paginas');
    $this->db->where($condition);
    $query = $this->db->get();
    return $query->result();     
}


}

?>