<?php

Class Usuarios_model extends CI_Model {

    
    public function usuarios(){
        
    $this->db->select('usuarios.idusuario, usuarios.nombre, usuarios.apellidos, usuarios.email,usuarios.estado,usuarios.fecha_registro as fecha,roles_usuarios.nombre as rol ');
    $this->db->from('usuarios');
    $this->db->join('roles_usuarios','roles_usuarios.idrol_usuario = usuarios.rol');
    $query = $this->db->get();
    return $query->result();  
    }
    
     
    public function usuarios2(){
        
    $this->db->select('usuarios.idusuario, usuarios.nombre, usuarios.apellidos, usuarios.email,usuarios.estado,usuarios.fecha_registro as fecha,roles_usuarios.nombre as rol ');
    $this->db->from('administradores');
    $this->db->join('usuarios','usuarios.idusuario = administradores.idusuario');
    $this->db->join('roles_usuarios','roles_usuarios.idrol_usuario = usuarios.rol');
    $query = $this->db->get();
    return $query->result();  
    }
    
    
    public function desbloquear_usuario($id,$data){
        $this->db->where('idusuario', $id);
        $this->db->update('usuarios', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
    }   
        
    }
    
    public function bloquear_usuario($id,$data){
        $this->db->where('idusuario', $id);
        $this->db->update('usuarios', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }   
        
    }
    
     public function usuarios_disponibles(){
        $condition = "administradores.idusuario is null";
        $this->db->select('usuarios.*');
        $this->db->from('usuarios');
        $this->db->join('administradores','usuarios.idusuario = administradores.idusuario','left');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->result();  
            
    }
    
    public function agregar_admin($data){
        $this->db->insert('administradores', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
        
    }
    
    public function eliminar_admin($id){
        $this->db->where('idusuario',$id);
        $this->db->delete('administradores');
    
        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }  
    } 
    
    
}

?>