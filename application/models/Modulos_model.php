<?php

Class Modulos_model extends CI_Model {

 public function insertar_slide($data){
     $this->db->insert('slider', $data);
     if ($this->db->affected_rows() > 0) {
        return true;
     }else {
        return false;
     }  
 }  
    
 
    public function modulos_no_instalados(){
        
        $plantillas = array();
           $directorio = opendir("./application/views/modulos"); //ruta actual
while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
{
    
    
    if (is_dir($archivo))//verificamos si es o no un directorio
    {
        
    }
    else
    {
        
       $archivo = explode(".php", $archivo);
           $plantillas[]=$archivo[0];
        
    }
}

     return  $plantillas ;   
        
        
    }
    
    
    
public function modulos_disponibles(){
    $this->db->select('*');
    $this->db->from('modulos');
    $query = $this->db->get();
    return $query->result();
}    
    
    
public function  modulos_home(){
    $this->db->select('pagina_home.id,pagina_home.orden,modulos.modulo');
    $this->db->from('pagina_home');
    $this->db->join('modulos','modulos.id = pagina_home.id');
    $this->db->order_by('pagina_home.orden asc');
    $query = $this->db->get();
    return $query->result();
}    
    
public function ordenar_modulo($data = array()){    
    $orden = array(
    'orden'=> $data['orden'] 
    );
    
    $this->db->where('id', $data['id']);
    $this->db->update('pagina_home', $orden);
    
    if ($this->db->affected_rows() > 0) {
        return true;
    }else{
        return false;   

    }   
}    

    
        
public function eliminar_modulo($id){
    
    $this->db->where('id', $id);
    $this->db->delete('slider'); 
   if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }  
    
}
    
    
    
     public function activar_modulo($data){
     $this->db->insert('modulos', $data);
     if ($this->db->affected_rows() > 0) {
        return true;
     }else {
        return false;
     }  
 }  
      public function agregar_home($data){
     $this->db->insert('pagina_home', $data);
     if ($this->db->affected_rows() > 0) {
        return true;
     }else {
        return false;
     }  
 }   
    
  public function modulos_sin_indexar(){
      $this->db->select('modulos.id, modulos.modulo');
      $this->db->from('modulos');
      $this->db->join('pagina_home', 'pagina_home.idmodulo = modulos.id', 'left');
      $this->db->where('pagina_home.idmodulo IS NULL');
      $query = $this->db->get();
      return $query->result();
  }  
    

}

?>