<?php

Class Email_model extends CI_Model {

    public function enviar_email($to,$asunto,$mensaje,$plantilla){

         $this->db->select('*');
    $this->db->from('ajustes_sitio');
    $this->db->where('id = 1');
    $query = $this->db->get();
    $smtp =  $query->result();




        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $smtp[0]->smtp_host,
            'smtp_port' => $smtp[0]->smtp_port,
            'smtp_user' => $smtp[0]->smtp_user,
            'smtp_pass' => $smtp[0]->smtp_pass,
            'smtp_timeout' => '4',
            'mailtype'  => 'html',
            'charset'   => 'UTF-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from($smtp[0]->email_contacto, 'GO PLAYA');

        $this->email->to($smtp[0]->email_contacto);
        $this->email->subject($asunto);

        $body = $this->load->view('emails/'.$plantilla,$mensaje,TRUE);
        $this->email->message($body);
        if($this->email->send())
        {
            return true;
        }else{
            return show_error($this->email->print_debugger());

        }
    }


     public function enviar_email2($to,$asunto,$mensaje,$plantilla){

         $this->db->select('*');
        $this->db->from('ajustes_sitio');
        $this->db->where('id = 1');
        $query = $this->db->get();
        $smtp =  $query->result();

      // echo "email model";
      //
      //   var_dump($mensaje);

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $smtp[0]->smtp_host,
            'smtp_port' => $smtp[0]->smtp_port,
            'smtp_user' => $smtp[0]->smtp_user,
            'smtp_pass' => $smtp[0]->smtp_pass,
            'smtp_timeout' => '4',
            'mailtype'  => 'html',
            'charset'   => 'UTF-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from($smtp[0]->smtp_user, 'GO PLAYA');

        $this->email->to($to);
        $this->email->subject($asunto);

        $body = $this->load->view('emails/'.$plantilla,$mensaje,TRUE);
        $this->email->message($body);
        if($this->email->send())
        {
            return true;
        }else{
            return show_error($this->email->print_debugger());

        }
    }

   public function suscripcion($data){

    $this->db->insert('suscriptores', $data);

    if ($this->db->affected_rows() > 0) {
        return true;
    }else {
    return false;
    }

}

    public function suscriptores() {
        $this->db->select('*');
        $this->db->from('suscriptores');
        $query = $this->db->get();
        return $query->result();

}

    public function datos_generales(){
        $this->db->select('*');
        $this->db->from('ajustes_sitio');
        $this->db->where('id = 1');
        $query = $this->db->get();
        return $query->result();
    }

    public function agg($data){
        $this->db->where('id = 1');
        $this->db->update('ajustes_sitio', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }




}

?>
