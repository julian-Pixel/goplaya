<?php

//Perfil

$lang['global.mi_perfil'] = 'Mi perfil';

//Side Bar
$lang['global.bienvenido'] = 'Bienvenido';
$lang['global.configuracion'] = 'Configuración';
$lang['global.proximos_viajes'] = 'Mis próximos viajes';
$lang['global.mis_rutas'] = 'Mis rutas';
$lang['global.administracion'] = 'Adminitración';
$lang['global.salir'] = 'Salir';
$lang['global.ingresar'] ='Ingresar';
$lang['global.bienvenido'] = 'Bienvenido';


$lang['global.btn_detectar_ubicacion'] = 'Detectar ubicación';
$lang['global.nombre'] = 'Nombre';
$lang['global.apellidos'] = 'Apellidos';
$lang['global.email'] = 'Email';
$lang['global.asunto'] = 'Asunto';
$lang['global.mensaje'] = 'Mensaje';
$lang['global.enviar'] = 'Enviar';
$lang['global.votos'] = 'Votos';
$lang['global.ver_mas'] = 'Ver más';
$lang['global.btn_siguiente']='Siguiente';
$lang['global.btn_anterior']='Anterior';
$lang['global.filtros']='Escoja el tipo de playa que desea visitar';
$lang['global.todos']='Todos';
$lang['global.radio']='Radio (en km) que desea recorrer (desde la playa que escogió)';
$lang['global.btn_actualizar']='Actualizar';
$lang['global.apellidos']='Apellidos';
$lang['global.telefono']='Teléfono';
$lang['global.ajustes']='AJUSTES DE MI CUENTA';
$lang['global.ajustes_generales']='Ajustes generales';
$lang['global.caracteristicas']='Características';
$lang['global.email']='Email';
$lang['global.contrasena']='Contraseña';
$lang['global.ingresar']='Ingresar';
$lang['global.ingresar_facebook']='Ingresar con  Facebook';
$lang['global.registrarse']='Registrarse';
$lang['global.ruta_enviada']="GOPlaya ha enviado su ruta de playas a su correo electrónico";


$lang['global.titulo'] = 'Costa Rica';
$lang['global.subtitulo'] = '¿Qué tipo de playa te gustaría explorar?';
$lang['global.placeholder'] = 'Playa o provincia';
$lang['global.btn_filtro'] = '¿QUÉ TIPO DE PLAYA BUSCAS?';
$lang['global.btn_buscar'] = '<span>Buscar</span> playas';
$lang['global.frase_final'] = 'Más de 200 playas <br> Más de 2000 hoteles';
$lang['global.foto_por'] = 'Fotografía:';

$lang['global.suscripcion_titulo'] = '<span>Suscríbete</span> para recibir promociones!';

$lang['global.titulo_tours'] ='Nuestros Tours';
$lang['global.subtitulo_tours'] ='¿Qué experiencia estás buscando?';
$lang['global.btn_tipotours'] ='Qué tipo de tour buscas?';
$lang['global.su_numero_id'] ='Su número de identificación es:';
$lang['global.ingrese_al_sistema'] ='Ingrese al sistema y suba su tour';
$lang['global.lbl1'] ='Goplaya, ganador del premio internacional Pasaporte Abierto en la categoría innovación';
$lang['global.lbl2'] ='Afiliado a la Cámara Nacional de Turismo (Canatur)';
$lang['global.lbl3'] ='Miembro de la Organización Mundial de Periodismo Turístico';
$lang['global.gracias'] ='Gracias por Registrarse';

$lang['global.tab1'] ='Datos generales<br /><small>Detalles del dueño del tour operador</small>';
$lang['global.tab2'] ='Tour operdora<br /><small>Detalles de la compañía tour operadora</small>';
$lang['global.tab3'] ='Recepción de pagos <br /><small>Detalles para la transferencia del dinero al tour operador</small>';

$lang['global.marca'] ='Nombre de la empresa (marca)';
$lang['global.razon'] ='Razón social:';
$lang['global.website'] ='Dirección del sitio web de la empresa';
$lang['global.direccion_empresa'] ='Dirección de su empresa:';
$lang['global.ciudad'] ='Ciudad:';
$lang['global.provincia'] ='Provincia';
$lang['global.organizacion'] ='¿Está afiliado a una organización turística?';
$lang['global.numero_afiliado'] ='Número de afiliado a dicha cámara:';

$lang['global.propietario'] ='Nombre del propietario de la cuenta bancaria:';
$lang['global.cedula'] ='Cédula del propietario de la cuenta bancaria:';
$lang['global.cuenta'] ='Cuenta bancaria:';
$lang['global.cuenta_normal'] ='Número de Cuenta:';
$lang['global.cuenta_sinpe'] ='Número de SINPE:';
$lang['global.elija_uno'] ='Elija una opción';


$lang['global.siguiente']='Siguiente';
$lang['global.anterior']='Anterior';
$lang['global.lbl6'] ='*Cuenta debe ser en dólares <br>
*Comisión GOPlaya del 20% <br>
*Para los tours operadores afiliados a las siguientes Cámaras y organizaciones de turismo, la Comisión de GOPlaya será del 15% <br>
*La comisión bancaria se divide entre GOPlaya y el tour operador.';


//HOME
$lang['global.step-intro'] ='<h4>Bienvenido a GO Playa</h4> <p>Para facilitarte la búsqueda, te daremos un rápido recorrido.</p>';
$lang['global.step-1h'] ='Elija el tipo de playa que busca.<br>Ejemplo: Arena blanca. Luego de elegir, toque el botón “Buscar playas” para que salgan los resultados de búsqueda.';
$lang['global.step-2h'] ='Puede buscar de dos maneras: A. Coloque el nombre de la playa y selecciónela, así irá directo a ella. El sistema le sugerirá playas con cada letra que ponga. B. Combine: Coloque el nombre completo de la provincia en la barra de búsqueda y elija el tipo de playa que busca (como en el paso 2). ';
$lang['global.step-3h'] ="Toque el botón “Buscar playas” o presione enter cuando decida combinar. Si busca la playa por su nombre en la barra de búsqueda, solo selecciónela.";
//Buscador
$lang['global.step-1b'] ='<h4>Bienvenido al Buscador de playas</h4><p>Para facilitarte la búsqueda, te daremos un rápido recorrido.</p>';
$lang['global.step-2b'] ='Escoja la provincia que desea visitar. Puede elegir más de una provincia al mismo tiempo.';
$lang['global.step-3b'] ='Escoja el tipo de playa que le gustaría visitar. Ejemplo: Arena blanca.';
$lang['global.step-4b'] ='Elija el radio de búsqueda desde el punto rojo señalado el mapa, o desde el lugar donde se encuentra según el GPS de su celular.';
$lang['global.step-5b'] ='Puede elegir más características en búsqueda avanzada. Es opcional.';
$lang['global.step-6b'] ='Siempre que va realizar una búsqueda, debe darle click a este botón';
//Crea tu ruta
$lang['global.step-1c'] ='<h4>Bienvenido a Crea tu Ruta</h4> <p>Para facilitarte la búsqueda, te daremos un rápido recorrido.</p>';
$lang['global.step-2c'] ='Coloque la playa en donde desea iniciar su ruta. Ejemplo: Tamarindo. Al elegirla, el punto rojo en el mapa se localizará sobre la playa.';
$lang['global.step-3c'] ='Elija las características de las playas que desea visitar en su ruta.';

$lang['global.step-4c'] ='Elija los Km que está dispuesto a recorrer desde la playa que escogió. ';
$lang['global.step-5c'] ='Presione “Buscar” para que aparezcan las playas que puede agregar a su ruta. Cada playa trae el botón verde de “Agregar a mi ruta”, presione click en la playa que desee y revise el mapa. ';
$lang['global.step-6c'] ='Importante: Cada vez que toque el botón verde de “Agregar a mi ruta”, acerque el mapa para que vea con claridad la ruta que está creando.';


 //Home
$lang['global.titulo_bp'] = '<span>#</span>CompartimosCostaRica';
$lang['global.subtitulo_bp'] = 'Suba su foto aquí con el hashtag #CompartimosCostaRica';
$lang['global.btn_bp'] = 'Buscador de playas';

 //Page

$lang['global.opciones'] = '<span>Seleccione</span> el tipo de  playa que le gustaría visitar';
$lang['global.opciones_playas'] ='Opciones de playas';
$lang['global.definir_radio'] ='Definir radio de la búsqueda';
$lang['global.zonas'] = 'Zonas';
$lang['global.mostrar_filtros'] = 'Búsqueda avanzada';
$lang['global.buscar_playas'] = 'Buscar playas';
$lang['global.todas'] = 'Todas';
$lang['global.busqueda_avanzada'] = '<span>Búsqueda</span> avanzada de playas';
$lang['global.resultados_busqueda'] = '<span>Resultados</span> de búsqueda';

$lang['global.titulo_ic']='<span>Información</span> de contacto';

$lang['global.titulo_ctr'] = '<span>Crea tu</span> Ruta';

 //Page

$lang['global.punto_partida']='Elija la playa desde donde desea iniciar la ruta';
$lang['global.placeholder-ctr']='Elige tú punto de partida';
$lang['global.waze']='Ir con waze';
$lang['global.gmap']='Ir con google maps';
$lang['global.ruta_predisenada']='<span>Rutas</span> recomendadas';
$lang['global.lblAdd']='Agregar a mi ruta';
$lang['global.lblRemove']='Eliminar de mi ruta';
$lang['global.mensaje_ruta']='Envía tu ruta a tu correo o a la cuenta de un amigo';
