$(document).ready(function() {


  $('#reviews').click(function(){
       $(this).val(this.checked ? 1 : 0);
       console.log($(this).val());
   });


  var base_url = 'http://queleparece.com/goplaya/';
  var host = 'http://queleparece.com/goplaya/';

  $(document).on('click', '.anchor', function(event) {
    event.preventDefault();

    $('html, body').animate({
      scrollTop: $($.attr(this, 'href')).offset().top
    }, 700);
  });

  $('.auto').on('ifChecked', function(event){
var f = $(this).val(); // alert value


        window.location.href = base_url+"index.php/tours?filtros%5B%5D="+f+"&busqueda%5B%5D=";

  });


    var proxy = 'https://cors-anywhere.herokuapp.com/';
    // var proxy = '';
    $.ajax({
      dataType: 'JSON',
      jsonpCallback: 'callbackFnc',
      type: 'GET',
      async: false,
      crossDomain: true,

      url: proxy + "https://ubicaciones.paginasweb.cr/provincias.json",
      beforeSend: function(request) {
        request.setRequestHeader("Authorization", "Negotiate");
      },
      data: {},
      success: function(data) {
        var html = "<select>";
        for (key in data) {
          html += "<option value='" + key + "'>" + data[key] +
            "</option>";
        }
        html += "</select";
        $('#provincias').html(html);
      }
    });
    $.ajax({
      dataType: 'JSON',
      jsonpCallback: 'callbackFnc',
      type: 'GET',
      async: false,
      crossDomain: true,

      url: proxy +
        "https://ubicaciones.paginasweb.cr/provincia/1/cantones.json",
      beforeSend: function(request) {
        request.setRequestHeader("Authorization", "Negotiate");
      },
      data: {},
      success: function(data) {
        var html = "<select>";
        for (key in data) {
          html += "<option value='" + data[key] + "'>" + data[key] +
            "</option>";
        }
        html += "</select";
        $('#cantones').html(html);
      }
    });
    $('#provincias').on('change', function() {
      var canton = this.value;
      $.ajax({
        dataType: 'JSON',
        jsonpCallback: 'callbackFnc',
        type: 'GET',
        async: false,
        crossDomain: true,

        url: proxy + "https://ubicaciones.paginasweb.cr/provincia/" +
          canton +
          "/cantones.json",
        beforeSend: function(request) {
          request.setRequestHeader("Authorization", "Negotiate");
        },
        data: {},
        success: function(data) {
          var html = "<select>";
          for (key in data) {
            html += "<option value='" + data[key] + "'>" + data[key] +
              "</option>";
          }
          html += "</select";
          $('#cantones').html(html);
        }
      });
    });


  $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function() {
    $(this).toggleClass('open');
  });
  $(".stars > i").hover(function() {
    //alert('si');
    $(this).prevAll().addClass('fa-star').removeClass('fa-star-o');
    $(this).addClass('fa-star').removeClass('fa-star-o');
    $(this).nextAll().addClass('fa-star-o').removeClass('fa-star');
  });
  $(".range").ionRangeSlider({
    hide_min_max: true,
    keyboard: true,
    min: 1,
    max: 500,
    from: 350,
    to: 2,
    type: 'single',
    step: 1,
    postfix: " Km",
    grid: true
  });
  $('#meter').entropizer({
    target: '#pwd',
    update: function(data, ui) {
      //   console.log(data.percent);
      ui.bar.css({
        'background-color': data.color,
        'width': data.percent + '%'
      });
      if (data.percent > 63) {
        $('.cc').fadeIn();
      } else {
        $('.cc').fadeOut();
      }
    }
  });
  $('#pwd').keyup(function() {
    if ($('#pwd').val() === $('#rpwd').val()) {
      $('.status').html('');
    } else {
      $('.status').html('Contraseñas no coinciden');
    }
  });
  $('#rpwd').keyup(function() {
    if ($('#pwd').val() === $('#rpwd').val()) {
      $('.status').html('');
    } else {
      $('.status').html('Contraseñas no coinciden');
    }
  });

  $('.open-mm').click(function() {
    $('.main-menu').slideToggle(150, "linear");
  });


  $('.ecf').click(function() {

    var id = $(this).attr("data-id");
    var pos = $(this).attr("data-position");
    $('#elementid').val(id);
    $('#elementid2').val(id);
    $('#upselectposition').val(pos);
    console.log(pos);
  });


  $('#upcaption2').click(function() {
    var datos = $('#upcaption').serialize();
    $('#rbctr').html(
      '<img class="loader-center" src="' + host +
      '/theme/img/sp.svg">'
    );

    $.ajax({
      url: host + '/index.php/provincias/update_caption', //This is the current doc
      type: "POST",
      data: (datos),
      success: function(data) {
        console.log(data)
        $('#rbctr').html(data).fadeIn();
        alert('Se ha cambiado el caption correctamente!');
        $('#efModal').modal('toggle');


      }
    });
  });

  $('#upposition').click(function() {


    var datos = $('#upcaption3').serialize();



    $('#rbctr').html(
      '<img class="loader-center" src="' + host +
      '/theme/img/sp.svg">'
    );


    $.ajax({
      url: host + '/index.php/provincias/update_position', //This is the current doc
      type: "POST",
      data: (datos),
      success: function(data) {

        console.log(datos);

        $('#rbctr').html(data).fadeIn();
        alert('Se ha cambiado el caption correctamente !');
        // $('#efModal').modal('toggle');
        $('#positionModal').modal('hide');


      }
    });
  });



  $('.date-range').daterangepicker({
    "autoApply": true,
    locale: {
      format: 'YYYY/MM/DD'
    },
  }, function(start, end, label) {
    console.log(
      "New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')"
    );
  });
  $('[data-gostep]').click(function() {
    var siguiente = $(this).attr("data-gostep");
    $('[data-step]').slideUp();
    $('[data-step=' + siguiente + ']').slideDown();
  });
  /*var folder = "'+ host+'/archivos/";
  $.ajax({
    url: folder,
    success: function(data) {
      $(data).find("a").attr("href", function(i, val) {
        if (val.match(/\.(jpe?g|png|gif)$/)) {
          $("#medios").append(
            "<li class='col-xs-6 col-md-3'><a target='_blank' href=''+ host+'/dashboard/eliminar_fichero/" +
            val +
            "'>Eliminar</a><img class='img-responsive' src='" +
            folder + val + "'></li>");
        }
      });
    }
  });*/
  $(function() {
    $(".sortable").sortable({
      update: function(event, ui) {
        $('.save_ajax').fadeIn();
        var postData = $(this).sortable('serialize');
        $.ajax({
          type: "POST",
          url: host + '/slider/ordenar_slides',
          data: postData,
          success: function(response) {
            if (response) {
              $('.save_ajax').fadeOut();
            } else {
              //show error
            }
          }
        });
      }
    });
    $(".sortable").disableSelection();
  });
  $(function() {
    $(".sortable2").sortable({
      update: function(event, ui) {
        $('.save_ajax').fadeIn();
        var postData = $(this).sortable('serialize');
        $.ajax({
          type: "POST",
          url: '' + host + '/modulos/ordenar_modulos',
          data: postData,
          success: function(response) {
            if (response) {
              $('.save_ajax').fadeOut();
            } else {
              //show error
            }
          }
        });
      }
    });
    $(".sortable2").disableSelection();
  });
  $(function() {
    $(".sortable3").sortable({
      update: function(event, ui) {
        $('.save_ajax').fadeIn();
        var postData = $(this).sortable('serialize');
        $.ajax({
          type: "POST",
          url: '' + host + '/menu/ordenar_item',
          data: postData,
          success: function(response) {
            if (response) {
              console.log(response);
              $('.save_ajax').fadeOut();
            } else {
              //show error
            }
          }
        });
      }
    });
    $(".sortable3").disableSelection();
  });



  $(function() {
    $(".sortable4").sortable({
      update: function(event, ui) {
        $('.save_ajax').fadeIn();
        var postData = $(this).sortable('serialize');
        $.ajax({
          type: "POST",
          url: '' + host + '/playa/ordenar_item',
          data: postData,
          success: function(response) {
            if (response) {
              console.log(response);
              $('.save_ajax').fadeOut();
            } else {
              //show error
            }
          }
        });
      }
    });
    $(".sortable4").disableSelection();
  });

  $('#permalink').slugger({
    source: '#titulo'
  });

  $('#permalink').keyup(function() {
    var s = $('#permalink').val();
    $.ajax({
      url: base_url + 'provincias/comprobar_permalink',
      type: "POST",
      data: ({
        slug: s
      }),
      success: function(data) {
        console.log(data);
        if (data == '1') {

          $('#permalink').css('border-color', '#cc2323');


        } else {
          $('#permalink').css('border-color',
            'rgba(128, 128, 128, 0.33)');
        }

      }
    });



  });


  $('#myGrid').gridEditor({
    source_textarea: '#test',
    content_types: ['ckeditor'],
    col_classes: [{
      label: 'Sin padding',
      cssClass: 'sin-padding'
    }, {
      label: 'Banner full width',
      cssClass: 'banner-full-width'
    }, {
      label: 'Padding 10px',
      cssClass: 'padding-10'
    }, {
      label: 'Padding 20px',
      cssClass: 'padding-20'
    }, {
      label: 'Padding 30px',
      cssClass: 'padding-30'
    }, {
      label: 'Padding 40px',
      cssClass: 'padding-40'
    }, {
      label: 'Padding 50px',
      cssClass: 'padding-50'
    }, {
      label: 'Hide mobile',
      cssClass: 'hide-mobile '
    }],
    row_classes: [{
      label: 'Contenedor',
      cssClass: 'container'
    }, {
      label: 'Hide mobile',
      cssClass: 'hide-mobile '
    }],
  });


  $('#myGrid2').gridEditor({
    source_textarea: '#test2',
    content_types: ['ckeditor'],
    col_classes: [{
      label: 'Sin padding',
      cssClass: 'sin-padding'
    }, {
      label: 'Banner full width',
      cssClass: 'banner-full-width'
    }, {
      label: 'Padding 10px',
      cssClass: 'padding-10'
    }, {
      label: 'Padding 20px',
      cssClass: 'padding-20'
    }, {
      label: 'Padding 30px',
      cssClass: 'padding-30'
    }, {
      label: 'Padding 40px',
      cssClass: 'padding-40'
    }, {
      label: 'Padding 50px',
      cssClass: 'padding-50'
    }, {
      label: 'Hide mobile',
      cssClass: 'hide-mobile '
    }],
    row_classes: [{
      label: 'Contenedor',
      cssClass: 'container'
    }, {
      label: 'Hide mobile',
      cssClass: 'hide-mobile '
    }],
  });



  $('input[type=radio][name=home]').change(function() {
    if (this.value == '0') {
      $('#plantilla_select').fadeOut();
    } else if (this.value == '1') {
      $('#plantilla_select').fadeIn();
    }
  });
  $('#salvar').click(function() {
    $("#salvar").html(
      'Guardando <img src="' + host + '/theme/img/ring_blue.svg">'
    );



    var html = $('#myGrid').gridEditor('getHtml');
    console.log(html);
    if ($("#test").html(html)) {
      setTimeout(function() {
        $("#salvar").html('Guardado!');
        setTimeout(function() {
          $("#salvar").html(
            '<i class="fa fa-floppy-o" aria-hidden="true"></i> Actualizar cambios'
          );
        }, 2000);
      }, 3000);
    } else {
      console.log('error');
    }


    var html = $('#myGrid2').gridEditor('getHtml');
    console.log(html);
    if ($("#test2").html(html)) {
      setTimeout(function() {
        $("#salvar").html('Guardado!');
        setTimeout(function() {
          $("#salvar").html(
            '<i class="fa fa-floppy-o" aria-hidden="true"></i> Actualizar cambios'
          );
        }, 2000);
      }, 3000);
    } else {
      console.log('error');
    }



  });
  $('.swipebox').swipebox();
  $('.tipo_item').on('change', function() {
    if (this.value == 1) {
      $('.video-yotube').fadeOut();
      $('.galeria-foto').fadeIn();
    }
    if (this.value == 2) {
      $('.galeria-foto').fadeOut();
      $('.video-yotube').fadeIn();
    }
  })


  $('.filtros-btn').click(function() {
    $('.sub-list').slideToggle();
  });

  $('.add_route').click(function() {

    if (getCookie('anuncio_crearuta') == '1') {



    } else {

      setCookie('anuncio_crearuta', '1', 1);
      $('.anuncio-patrocinado').slideToggle();

      setTimeout(function() {
        $('.anuncio-patrocinado').slideToggle();
      }, 5000);
    }

  });
  $('.cap').click(function() {

    $('.anuncio-patrocinado').slideToggle();


  });



  $('.alerta').click(function() {
    $(this).fadeOut();
  });
  $('.icheck').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });
  $(".checkAll").click(function() {
    var target = $(this).attr("data-target");
    if (!$(target).is('checked')) {
      $(target).prop('checked', true);
      var parent = $(target).parent();
      if ($(parent).hasClass("checked")) {
        parent.removeClass('checked');
        $(this).hide().html(
          '<i class="fa fa-check" aria-hidden="true"></i>').fadeIn();
        $(target).prop('checked', false);
      } else {
        parent.addClass('checked');
        $(this).hide().html(
          '<i class="fa fa-times" aria-hidden="true"></i>').fadeIn();
      }
    } else {
      $(target).prop('checked', false);
      console.log(parent);
    }
  });
  try {


    var editor = new Simditor({
      textarea: $('.editor'),
      upload: false,
      toolbar: [
          'title', 'bold', 'italic', 'underline', 'strikethrough',
          'fontScale', 'color', 'ol', 'ul', 'image', 'blockquote',
          'table', 'link', 'hr', 'indent', 'outdent', 'alignment',
        ]
        //optional options
    });

    var editor = new Simditor({
      textarea: $('.editor2'),
      upload: false,
      toolbar: [
          'title', 'bold', 'italic', 'underline', 'strikethrough',
          'fontScale', 'color', 'ol', 'ul', 'image', 'blockquote',
          'table', 'link', 'hr', 'indent', 'outdent', 'alignment',
        ]
        //optional options
    });


    var editor = new Simditor({
      textarea: $('.editor3'),
      upload: false,
      toolbar: [
          'title', 'bold', 'italic', 'underline', 'strikethrough',
          'fontScale', 'color', 'ol', 'ul', 'image', 'blockquote',
          'table', 'link', 'hr', 'indent', 'outdent', 'alignment',
        ]
        //optional options
    });

    var editor = new Simditor({
      textarea: $('.editor4'),
      upload: false,
      toolbar: [
          'title', 'bold', 'italic', 'underline', 'strikethrough',
          'fontScale', 'color', 'ol', 'ul', 'image', 'blockquote',
          'table', 'link', 'hr', 'indent', 'outdent', 'alignment',
        ]
        //optional options
    });


    var editor = new Simditor({
      textarea: $('.editor5'),
      upload: false,
      toolbar: [
          'title', 'bold', 'italic', 'underline', 'strikethrough',
          'fontScale', 'color', 'ol', 'ul', 'image', 'blockquote',
          'table', 'link', 'hr', 'indent', 'outdent', 'alignment',
        ]
        //optional options
    });


  } catch (err) {}


  try {


    var $wrapper = $('.iconos');

    $wrapper.find('li').sort(function(a, b) {
        return +a.dataset.order - +b.dataset.order;
      })
      .appendTo($wrapper);

    var $wrapper2 = $('#rbctr');

    $wrapper2.find('li').sort(function(a, b) {
        return +b.dataset.order - +a.dataset.order;
      })
      .appendTo($wrapper2);


    console.log($("#rbctr li:nth-child(2)"));

    $("#rbctr li:nth-child(1)").attr("id", "scroll-here");

    $('html, body').animate({
      scrollTop: $("#scroll-here").offset().top
    }, 2000);



  } catch (err) {}


  $uploadCrop = $('#upload-demo').croppie({
    enableExif: true,
    viewport: {
      width: 360,
      height: 360,
      type: 'square'
    },
    boundary: {
      width: 400,
      height: 400
    }
  });

  $uploadCrop2 = $('#upload-demo2').croppie({
    enableExif: true,
    viewport: {
      width: 700,
      height: 400,
      type: 'square'
    },
    boundary: {
      width: 950,
      height: 550
    }
  });



  $('#upload').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
      $uploadCrop.croppie('bind', {
        url: e.target.result
      }).then(function() {
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
  });


  $('#upload2').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
      $uploadCrop2.croppie('bind', {
        url: e.target.result
      }).then(function() {
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
  });
  $('#mfa').on('click', function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass('active');
      $(this).hide().html(
        '<i class="fa fa-check" aria-hidden="true"></i>').fadeIn();
    } else {
      $(this).addClass('active');
      $(this).hide().html(
        '<i class="fa fa-times-circle" aria-hidden="true"></i>').fadeIn();
    }
    $('.filterbg').slideToggle(600, "linear", function() {});
  });


  $("#btndc").on('click', function() {

    var tipo = $("#frmdc").attr("data-action");
    var datos = $('#frmdc').serialize();
    if (tipo == 'insert') {

      $.ajax({
        url: base_url + 'provincias/descp_carac_add',
        type: "POST",
        data: (datos),
        success: function(data) {
          if (data == '1') {

            $('#edicaModal').modal('toggle');
          } else {
            console.log(data);
          }



        }
      });



    } else {
      $.ajax({
        url: base_url + 'provincias/descp_carac_update',
        type: "POST",
        data: (datos),
        success: function(data) {
          if (data == '1') {
            console.log(data);
            $('#edicaModal').modal('toggle');
          } else {
            console.log(data);
          }



        }
      });
    }
  });


  $("#btndeletec").on('click', function() {

    var datos = $('#frmdc').serialize();

    $.ajax({
      url: base_url + 'provincias/descp_carac_delete',
      type: "POST",
      data: (datos),
      success: function(data) {
        if (data == '1') {

          $('#edicaModal').modal('toggle');
        } else {
          console.log(data);
        }



      }
    });



  });



  $('.upload-result').on('click', function(ev) {
    $('#vpp').html(
      '<img src="http://comolove.com/lens-2/theme/img/ring.svg">');
    var cu = $('#photoid').val();
    $uploadCrop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(resp) {
      $.ajax({
        url: host + '/index.php/dashboard/update_imagen_playa',
        type: "POST",
        data: {
          image: resp,
          xc: cu
        },
        success: function(data) {
          console.log(data);
          $('#vpp').html('<p>Imagen actualizada!</p>');
          document.getElementById("upload").value = "";
          location.reload();
        }
      });
    });
  });

  $('.upload-result-tour').on('click', function(ev) {
    $('#vpp').html(
      '<img src="http://comolove.com/lens-2/theme/img/ring.svg">');
    var cu = $('#photoid').val();
    $uploadCrop2.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(resp) {
      $.ajax({
        url: host + '/index.php/tours/update_imagen_tour',
        type: "POST",
        data: {
          image: resp,
          xc: cu
        },
        success: function(data) {
          console.log(data);
          $('#vpp').html('<p>Imagen actualizada!</p>');
          document.getElementById("upload").value = "";
          location.reload();
        }
      });
    });
  });



  $('#e2_element').fontIconPicker({
    theme: 'fip-darkgrey',
    attributeName: 'data-icomoon',
  });
  $(document).ready(function() {
    $('.table').DataTable({
      dom: 'Bfrtip',
      buttons: [{
        extend: 'copyHtml5',
        text: '<i class="fa fa-files-o"></i>',
        titleAttr: 'Copy'
      }, {
        extend: 'excelHtml5',
        text: '<i class="fa fa-file-excel-o"></i>',
        titleAttr: 'Excel'
      }, {
        extend: 'csvHtml5',
        text: '<i class="fa fa-file-text-o"></i>',
        titleAttr: 'CSV'
      }, {
        extend: 'pdfHtml5',
        text: '<i class="fa fa-file-pdf-o"></i>',
        titleAttr: 'PDF'
      }, {
        extend: 'print',
        text: '<i class="fa fa-print"></i>',
        titleAttr: 'PRINT'
      }]
    });
    $("[data-open='modal']").click(function() {
      var modal = $(this).attr("data-target");
      var effect = 'slide';
      var options = {
        direction: 'down'
      };
      var duration = 500;
      $(modal).toggle(effect, options, duration);
      $('body').css('overflow', 'hidden');
      $('.modal-bg').fadeIn();
    });
    $("#btnmi").click(function() {
      var effect = 'slide';
      var options = {
        direction: 'right'
      };
      var duration = 500;
      $('#right-panel').toggle(effect, options, duration);
    });
    $("#btnwaze").click(function() {
      var effect = 'slide';
      var options = {
        direction: 'right'
      };
      var duration = 500;
      $('.wazepanel').toggle(effect, options, duration);
    });
    $("#btngmap").click(function() {
      var effect = 'slide';
      var options = {
        direction: 'right'
      };
      var duration = 500;
      $('.gmpanel').toggle(effect, options, duration);
    });
    $("#mf").click(function() {
      var effect = 'slide';
      var options = {
        direction: 'right'
      };
      var duration = 500;
      $('.pom2').fadeOut();
      $('.pom1').fadeIn();
    });
    $("#mm").click(function() {
      var effect = 'slide';
      var options = {
        direction: 'right'
      };
      var duration = 500;
      $('.pom1').fadeOut();
      $('.pom2').fadeIn();
      initMap();
    });


    $('#bpc').click(function() {
      var datos = $('#frmfiltros').serialize();
      $('#rbctr').html(
        '<img class="loader-center" src="' + host +
        '/theme/img/sp.svg">'
      );
      $.ajax({
        url: host + '/index.php/search_controller/crea_tu_ruta', //This is the current doc
        type: "POST",
        data: (datos),
        success: function(data) {
          setTimeout(function() {
            $('#rbctr').html(data).fadeIn();
          }, 2000);
        }
      });
    });


    $('#ruta_emai_btn').click(function() {

      var punto_inicial = $('#playas-generator2').val();
      var lat_lng_pi = $('#lat').val() + ',' + $('#lng').val();

      var datos2 =
        '<a style="color:white;"  target="_blank" href="https://maps.google.com/maps?ll=' +
        lat_lng_pi + '&q=' + lat_lng_pi +
        '&hl=en&t=h&z=18"> Google maps: ' + punto_inicial +
        "</a><br>";

      $("#waypoints option:selected").each(function(i) {
        // console.log($(this).text() + " : " + $(this).val());
        datos2 +=
          '<a style="color:white;"  target="_blank" href="https://maps.google.com/maps?ll=' +
          $(this).val() + '&q=' + $(this).val() +
          '&hl=en&t=h&z=18"> Google maps: ' + $(this).text() +
          "</a><br>";

      });


      $('#enviar_ruta_email').val(datos2);

      console.log(punto_inicial + ':' + lat_lng_pi);

      $('#ruta_email').val(datos2);

      var datos = $('#enviar_ruta_email').serialize();

      $.ajax({
        url: '' + host + '/dashboard/enviar_ruta', //This is the current doc
        type: "POST",
        data: datos,
        success: function(data) {
          console.log(data);
          alert($('#text-success').val());
          $('#correoModal').modal('toggle');
        }
      });

    });



    $('.modal-bg').click(function() {
      var effect = 'slide';
      var options = {
        direction: 'down'
      };
      var duration = 500;
      $('.setup-modal').toggle(effect, options, duration);
      $('body').css('overflow', 'inherit');
      $('.modal-bg').fadeOut();
    });
    $('.dropdown').click(function() {
      $(this).children('.sub-menu-admin').slideDown(200);
    }, function() {
      $(this).children('.sub-menu-admin').slideUp(200);
    });

    $.stellar({
      horizontalScrolling: false,
      responsive: true
    });
    $('.tours-carousel').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3
    });
    $(function() {
      $("#playas-generator").autocomplete({


        select: function(event, ui) {

          window.location = base_url + 'playa/detalle/' + ui.item
            .slug;

        },


        source: function(request, response) {
          $.ajax({
            url: host +
              '/index.php/Search_controller/generar_playas/',
            type: "POST",
            dataType: "json",
            data: {
              term: request.term
            },
            success: function(data) {
              console.log(data);
              response($.map(data, function(item) {
                return {
                  label: item.nombre + ', ' +
                    item.provincia,
                  value: item.provincia,
                  slug: item.slug
                };
              }))
            }
          });
        }
      });
    });

    $('#playas-generatortours').click(function() {
       $('.playas-destacadas').show();

    });

    // $('.hero_tours').focusout(function() {
    //    $('.playas-destacadas').hide();
    //
    // });

    $(document).mouseup(function (e){

    	var container = $("#playas-generatortours");

    	if (!container.is(e.target) && container.has(e.target).length === 0){

    		$('.playas-destacadas').fadeOut(300);


      }
    });



    $('#playas-generatortours').keyup(function() {
       $('.playas-destacadas').fadeOut(300);
    });


    $(function() {



      $("#playas-generatortours").on('click', function () {

        var destacados = 'samara';



        $.ajax({
            url: host +
              '/index.php/tours/generar_playas_all/',
            type: "POST",
            data: {
                term: destacados,
            },
            dataType : 'json',

            success: function(data) {
              console.log(data);


              $(".tours").html('');

             $.each(data, function(i){
                $(".tours").append("<div class='col-xs-12 col-md-4 ft' >" +
                                   "<a class='tours-destacados' href='#' onclick={crearfiltro('"+ data[i].provincia +"')}> " + data[i].provincia + "</a>" +
                                   "<br>" +
                                   "</div>");
            });



            }
        });




      });



    });




    $(function() {


      $("#playas-generatortours").autocomplete({


        select: function(event, ui) {

          window.location = base_url + 'tours?busqueda=' + ui
            .item.playa;

        },


        source: function(request, response) {
          $.ajax({
            url: host +
              '/index.php/tours/generar_playas/',
            type: "POST",
            dataType: "json",
            data: {
              term: request.term
            },
            success: function(data) {
              console.log(data);
              response($.map(data, function(item) {
                return {
                  label: item.nombre + ', ' +
                    item.provincia,
                  value: item.provincia,
                  slug: item.slug,
                  playa: item.nombre,
                };
              }))
            }
          });
        }
      });
    });







    //mi perfil menu
    $('.btn_perfil').click(function() {
      var effect = 'slide';
      var options = {
        direction: 'right'
      };
      var duration = 500;
      $('.mp_submenu').toggle(effect, options, duration);
    });
    $('.step-arrows li a').click(function() {
      var href = $(this).attr('href');
      var current = $(this).closest('div').closest('div').attr('id');
      $('#' + current).fadeOut(500);
      $(href).fadeIn(500);
      $(this)
    });
    $('.setup-btn').click(function() {
      var effect = 'slide';
      var options = {
        direction: 'down'
      };
      var duration = 500;
      $('.setup-modal').toggle(effect, options, duration);
      $('body').css('overflow', 'hidden');
      $('.modal-bg').fadeIn();
    });
    $('.close-setm').click(function() {
      var effect = 'slide';
      var options = {
        direction: 'down'
      };
      var duration = 500;
      $('.setup-modal').toggle(effect, options, duration);
      $('body').css('overflow', 'inherit');
      $('.modal-bg').fadeOut();
    });
    //Fin mi perfil menu
    if (!$('.notificacion').is(':visible')) {
      setTimeout(function() {
        $('.notificacion').show(500);
      }, 900);
    }
    $('.notificacion').click(function() {
      $(this).fadeOut(600);
    });
    //Mascaras inputs
    $('.numero').mask('#');
    $('.numero2').mask(
      'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ', {
        translation: {
          'Z': {
            pattern: /[0-9.-]/,
            optional: true
          }
        }
      });
    //Fin Mascaras inputs
    $('#loginbtn').click(function() {
      $('#loginbtn').html(
        'Iniciando sesión <img src="theme/img/ring_white.svg">');
      $.ajaxSetup({
        cache: true
      });
      $.getScript('//connect.facebook.net/en_US/sdk.js', function() {
        FB.init({
          appId: '368856163481838',
          version: 'v2.7' // or v2.1, v2.2, v2.3, ...
        });
        FB.getLoginStatus(function(response) {
          if (response.status === 'connected') {
            console.log(response.authResponse['userID']);
            FB.api('/me', {
              locale: 'en_US',
              fields: 'name, email,picture.width(800).height(800)'
            }, function(response) {
              console.log(response.email);
              $.ajax({
                url: '' + host +
                  '/index.php/dashboard/facebook_login', //This is the current doc
                type: "POST",
                data: ({
                  email: response.email,
                  nombre: response.name,
                  picture: response.picture
                }),
                success: function(data) {
                  location.reload();
                }
              });
            });
          } else {
            FB.login(function(response) {
              if (response.authResponse) {
                FB.api('/me', {
                  locale: 'en_US',
                  fields: 'name, email,picture.width(800).height(800)'
                }, function(response) {
                  console.log(response.email);
                  $.ajax({
                    url: host +
                      '/index.php/dashboard/facebook_login', //This is the current doc
                    type: "POST",
                    data: ({
                      email: response.email,
                      nombre: response.name,
                      picture: response.picture
                    }),
                    success: function(data) {
                      location.reload();
                    }
                  });
                });
              } else {
                $('#loginbtn').html(
                  '<i class="fa fa-facebook"></i> Se canceló el inicio de sesión'
                );
              }
            });
            //console.log(opener.document);
          }
        });
      });
    });
  });
  $(".login-action").click(function() {
    $(".modal-overlay").toggle("puff");
  });
  $(".close-modal").click(function() {
    $(".modal-overlay").toggle("puff");
  });
  if ($('input[type=radio][name=home]:checked').val() == '0') {
    $('#plantilla_select').hide();
  } else if (this.value == '1') {
    $('#plantilla_select').fadeIn();
  }
  (function($) {
    //creamos la fecha actual
    var date = new Date();
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth() + 1).toString().length == 1 ? "0" + (date.getMonth() +
      1).toString() : (date.getMonth() + 1).toString();
    var dd = (date.getDate()).toString().length == 1 ? "0" + (date.getDate())
      .toString() : (date.getDate()).toString();
    //establecemos los valores del calendario
    var options = {
      events_source: '<?php echo base_url() ?>index.php/dashboard/getAll',
      view: 'month',
      language: 'es-ES',
      tmpl_path: '' + host + '/tmpls/',
      tmpl_cache: false,
      day: yyyy + "-" + mm + "-" + dd,
      time_start: '10:00',
      time_end: '20:00',
      time_split: '30',
      width: '100%',
      onAfterEventsLoad: function(events) {
        if (!events) {
          return;
        }
        var list = $('#eventlist');
        list.html('');
        $.each(events, function(key, val) {
          console.log(val.precio);
          $(document.createElement('li')).html('<a href="' + val.url +
            '">' + val.title + '</a>').appendTo(list);
        });
      },
      onAfterViewLoad: function(view) {
        $('.page-header h3').text(this.getTitle());
        $('.btn-group button').removeClass('active');
        $('button[data-calendar-view="' + view + '"]').addClass(
          'active');
      },
      classes: {
        months: {
          general: 'label'
        }
      }
    };
    var calendar = $('#calendar').calendar(options);
    $('.btn-group button[data-calendar-nav]').each(function() {
      var $this = $(this);
      $this.click(function() {
        calendar.navigate($this.data('calendar-nav'));
      });
    });
    $('.btn-group button[data-calendar-view]').each(function() {
      var $this = $(this);
      $this.click(function() {
        calendar.view($this.data('calendar-view'));
      });
    });
    $('#first_day').change(function() {
      var value = $(this).val();
      value = value.length ? parseInt(value) : null;
      calendar.setOptions({
        first_day: value
      });
      calendar.view();
    });
    $('#events-in-modal').change(function() {
      var val = $(this).is(':checked') ? $(this).val() : null;
      calendar.setOptions({
        modal: true,
        modal_type: 'iframe'
      });
    });
  }(jQuery));
});


function vote(num, slug) {


  $.ajax({
    type: "POST",
    url: host + '/dashboard/votar',
    data: {
      voto: num,
      playa: slug
    },
    success: function(response) {
      if (response) {

        console.log(response);
        location.reload();

      } else {
        //show error
      }
    }
  });



}


function setCookie(nombre, valor, caducidad) {
  //Si no tenemos caducidad para la cookie, la definimos a 31 dias
  if (!caducidad)
    caducidad = 31

  var expireDate = new Date() //coge la fecha actual
  expireDate.setDate(expireDate.getDate() + caducidad);

  //crea la cookie: incluye el nombre, la caducidad y la ruta donde esta guardada
  //cada valor esta separado por ; y un espacio
  document.cookie = nombre + "=" + escape(valor) + "; expires=" + expireDate.toGMTString() +
    "; path=/";
}


function getCookie(nombre) {
  /*
   * document.cookie
   * Contiene todas las cookies que estan al alcance de la paginas web en el formato:
   * nombreCookie1=valor1; nombreCookie2=valor2
   *
   * document.cookie.length
   * Contiene la longitud de la suma de todas las cookies
   */
  if (document.cookie.length > 0) {
    /*
     * indexOf(caracter,desde) Devuelve la primera posicion que el caracter aparece
     * devuelve -1 si no encuentra el caracter
     */
    start = document.cookie.indexOf(nombre + "=");
    if (start != -1) {
      //El inicio de la cookie, el nombre de la cookie mas les simbolo '='
      start = start + nombre.length + 1;
      //Buscamos el final de la cookie (es el simbolo ';')
      end = document.cookie.indexOf(";", start);
      //Si no encontramos el simbolo del final ';', el final sera el final de la cookie.
      if (end == -1)
        end = document.cookie.length;
      //Devolvemos el contenido de la cookie.
      //substring(start,end) devuelve la cadena entre el valor mas bajo y
      //el mas alto, indiferentemente de la posicion.
      return unescape(document.cookie.substring(start, end));
    }
  }
  //no hemos encontrado la cookie
  return "";
}

$(window).load(function() {
  $('.loader').fadeOut();
});



function help() {
  var intro = introJs();
  intro.setOptions({
    showProgress: true
  });
  intro.start();

}

function crearfiltro(provincia){
var base_url = 'http://queleparece.com/goplaya/';
    console.log('filtro');
    console.log(provincia);

    window.location = base_url + 'tours?busqueda=' + provincia;
}



$("#review").change(function(e){
    $(this).val( $(this).checked ? true : false);
    // Below this line I write to span in order to show changes
    console.log($(this).val());
})
