$(document).ready(function() {
  //Inicio

var base_url_reservas = "http://dev.goplaya.cr";

  //calcular precios
  $(":input[name='adultos']").bind('keyup mouseup change', function() {
    console.log("suma de adultos");

    var precio_adultos = $(".precio_adultos").text();
    var precio = parseFloat(precio_adultos.replace("$", ""));
    console.log("precio");
    console.log(precio);
    var new_val = precio * parseFloat($(this).val());
    console.log("new_val");
    console.log(new_val);
    $("#subtotal_adultos").text(new_val);
    $("#numpersonastour").text(sumPersonas());
    sumPersonas();
    console.log("sumPersonas");
    console.log(sumPersonas());
  });

  $(":input[name='estudiantes']").bind('keyup mouseup change', function() {
    var precio_estudiantes = $(".precio_estudiantes").text();
    var precio = parseFloat(precio_estudiantes.replace("$", ""));
    var new_val = precio * parseFloat($(this).val());
    $("#subtotal_estudiantes").text(new_val);
    $("#numpersonastour").text(sumPersonas());
    sumPersonas();

  });

  $(":input[name='ninos']").bind('keyup mouseup change', function() {
    var precio_ninos = $(".precio_ninos").text();
    var precio = parseFloat(precio_ninos.replace("$", ""));
    var new_val = precio * parseFloat($(this).val());
    $("#subtotal_ninos").text(new_val);
    $("#numpersonastour").text(sumPersonas());
    sumPersonas();
  });

  $(":input[name='menores']").bind('keyup mouseup change', function() {
    var precio_menores = $(".precio_menores").text();
    var precio = parseFloat(precio_menores.replace("$", ""));
    var new_val = precio * parseFloat($(this).val());
    $("#subtotal_menores").text(new_val);
    sumPersonas();

  });


  function sumPersonas (){
      var menores =          parseInt($(":input[name='menores']").val())  || 0  ;
      var ninos =          parseInt($(":input[name='ninos']").val())   || 0 ;
      var estudiantes =          parseInt($(":input[name='estudiantes']").val()) || 0  ;
      var adultos    =      parseInt($(":input[name='adultos']").val()) || 0 ;

    var suma = menores + ninos + estudiantes + adultos;


    $("#numpersonastour").text(suma);
  }











  $('select').on('change', function() {

    var tourp = $("#tour").val();
    var fechap = $("#fecha-elegida").val();

    $.ajax({
      type: "POST",
      url:  "http://dev.goplaya.cr/tours/verificar_cupo_horas_tour",
      data: {
        tour: tourp,
        fecha: fechap,
        hora: this.value,
      },
      dataType: "json",
      success: function(data) {


        $("#cupo").css("display", "block");
        $("#cupo").append(data[0].cupos);

      }
    });

  });
  //Fin
});

function reservar() {
  var status = false;
  var go = false;

  var personas = 0;
  $(".cantidad_personas").each(function(index) {

console.log("se hizo un click");

    personas = personas + parseInt($(this).val());

  });
  // console.log("cantidad personas:" + personas);
  if ($("#fecha-elegida").val() == "") {
    // console.log("falta fecha");
    status = false;
    $(".calendar").addClass('error');
      $(".error-label").remove();
    $(".msj-alert").append(
      "<p class='error-label'>Ingrese una fecha para el tour</p>");


  } else if ($("#time").val() == 0) {
    // console.log("falta hora");
    status = false;
    $(".calendar").addClass('error');
      $(".error-label").remove();
    $(".msj-alert").append(
      "<p class='error-label'>Ingrese una hora</p>");


  }
   else {
    status = true;
    $(".calendar").removeClass('error');
    $(".error-label").remove();
  }

  if (status == true) {

    if ($("#time").val() == "0") {
      $("#time").addClass('error');
        $(".error-label").remove();
      $(".calendar").append(
        "<p class='error-label'>Ingrese una hora para el tour</p>");
      status = false;
    } else {
      $("#time").removeClass('error');
      $(".error-label").remove();
      status = true;
    }

    if (status == true) {


      if (personas > 0) {
        go = true;
        $(".cantidad_personas").removeClass('error');
        $(".error-label").remove();
      } else {
        go = false;
        $(".cantidad_personas").addClass('error');


        $(".msj-alert").append(
          "<p class='error-label'>Ingrese la cantidad de personas</p>");

      }



    } else {
      status = true;
    }

    // console.log("estado:" + go);

    if (go == true) {
      data = $("#form-reserva").serialize();
      // console.log("datos:" + data);
      $.ajax({
        type: "POST",
        url:  "http://dev.goplaya.cr/tours/verificar_tour",
        data: data,
        dataType: "json",
        success: function(data) {

          // console.log("respuesta verificar tour:" + data.response);
          if (data.response == 1) {


            // console.log('ir a reservacion');

            window.location.href = 'http://dev.goplaya.cr/tours/reservar_tour?id=' + btoa($(
                "#tour").val()) + '&a=' + btoa($("input[name='adultos']")
                .val()) +
              '&n=' + btoa($("input[name='ninos']").val()) + '&e=' + btoa(
                $(
                  "input[name='estudiantes']").val()) + '&m=' + btoa($(
                "input[name='menores']").val()) + "&fecha=" + btoa($(
                "#fecha-elegida").val()) + "&time=" + btoa($(
                "#time").val())

          } else {

            $.alert({
                theme: 'modern',
                icon: 'fa fa-warning',
                title: 'Error!',
                content: data.code,
              }

            );

          }
        }
      });

    }

  }

}


function horas_disponibles_tour(tourp, fechap) {



  // console.log('prueba');

  jQuery('html, body').animate({
        scrollTop: jQuery("#horario-tour").offset().top

    }, 2000);


  $.ajax({
    type: "POST",
    url: "http://dev.goplaya.cr/tours/verificar_horas_tour",
    data: {
      tour: tourp,
      fecha: fechap
    },
    dataType: "json",
    success: function(data) {
      var htime = moment().format("YYYY-MM-DD HH:mm");


      $('#time').empty();

      $("#time").append('<option value="0">---</option>');

      $.each(data, function(index) {
        // console.log(data[index].hora);
        var fech_tour = data[index].fecha + " " + data[index].hora;


        isAfter = moment(fech_tour).isAfter(htime);


        if (!isAfter) {

          $("#time").append('<option value="" disabled>' +
            data[index].hora + '</option>');

        } else {
          $("#time").append('<option value="' + data[index].hora +
            '">' +
            data[index].hora + '</option>');
        }



      });

    }
  });

}

$(document).ready(function() {
  jQuery(
    '<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>'
  ).insertAfter('.quantity input');
  jQuery('.quantity').each(function() {
    var spinner = jQuery(this),
      input = spinner.find('input[type="number"]'),
      btnUp = spinner.find('.quantity-up'),
      btnDown = spinner.find('.quantity-down'),
      min = input.attr('min'),
      max = input.attr('max');

    btnUp.click(function() {
      var oldValue = parseFloat(input.val());
      if (oldValue >= max) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue + 1;
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });

    btnDown.click(function() {
      var oldValue = parseFloat(input.val());


      if (oldValue <= 1) {
        // console.log('deshabilitar');
        oldValue = 1;
      }

      if (oldValue <= min) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue - 1;
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });

  });

  $('.timepicker').wickedpicker();

});


// ---------------------------------


$(".quantity-up").on('click', function() {
  // var precio = parseFloat($(".precio_estudiantes").text());
  // var new_val = precio * parseFloat($(this).val());
  // $("#subtotal_estudiantes").text(new_val);
  // console.log("atv");
});
