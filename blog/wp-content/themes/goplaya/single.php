<?php get_header()?>

<div class="container">
<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>

    <?php the_content(); ?>

<?php endwhile; ?>

<?php endif; ?>
</div>
<div class="container-fluid">
<?php get_footer()?>