<?php
/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter( 'ot_theme_mode', '__return_true' );

/**
 * Required: include OptionTree.
 */
require( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );


// Registro del menú de WordPress

add_theme_support( 'nav-menus' );

if ( function_exists( 'register_nav_menus' ) ) {
    register_nav_menus(
        array(
          'main' => 'Main',
          'ingles' => 'Ingles'
        )
    );
}


 //  Main Sidebar
     if(function_exists('register_sidebar'))
          register_sidebar(array(
          'name' => 'Main Sidebar',
           'before_widget' => '<hr>',
            'after_widget' => '',
          'before_title' => '<h3>',
          'after_title' => '</h3>',
     ));


//Habilitar thumbnails
add_theme_support('post-thumbnails');


add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}



function exclude_widget_categories($args){
  $exclude = "7"; // The IDs of the excluding categories
  $args["exclude"] = $exclude;
  return $args;
}
add_filter("widget_categories_args","exclude_widget_categories");

function exclude_post_categories($excl='', $spacer=' '){
   $categories = get_the_category($post->ID);
      if(!empty($categories)){
        $exclude=$excl;
        $exclude = explode(",", $exclude);
        $thecount = count(get_the_category()) - count($exclude);
        foreach ($categories as $cat) {
            $html = '';
            if(!in_array($cat->cat_ID, $exclude)) {
                $html .= '<a class="category" href="' . get_category_link($cat->cat_ID) . '" ';
                $html .= 'title="' . $cat->cat_name . '">' . $cat->cat_name . '</a>';
                if($thecount>1){
                    $html .= $spacer;
                }
            $thecount--;
            echo $html;
            }
          }
      }
}






?>
