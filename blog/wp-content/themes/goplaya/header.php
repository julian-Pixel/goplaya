<?php

if ( function_exists( 'ot_get_option' ) ) {
  $logotipo = ot_get_option( 'logotipo' );
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>
        <?php echo $blog_title = get_bloginfo( 'name' ); ?>

    </title>
    
    
    <link rel="apple-touch-icon" sizes="57x57" href="https://goplaya.cr/theme/img/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://goplaya.cr/theme/img/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://goplaya.cr/theme/img/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://goplaya.cr/theme/img/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://goplaya.cr/theme/img/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://goplaya.cr/theme/img/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://goplaya.cr/theme/img/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://goplaya.cr/theme/img/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://goplaya.cr/theme/img/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="https://goplaya.cr/theme/img/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://goplaya.cr/theme/img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="https://goplaya.cr/theme/img/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://goplaya.cr/theme/img/favicons/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="https://goplaya.cr/theme/img/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    
    
    <link rel="stylesheet" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
     <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700|Roboto" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/flexslider.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/custom.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/mobile.css" rel="stylesheet">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>

<body>
    <div class="container-fluid">
    
    <div class="row header">
        
        
        <div class="logo"><a href="https://goplaya.cr/"><img src="<?php echo $logotipo; ?>" class="img-responsive" alt=""></a></div>
           <?php wp_nav_menu( array('menu' => 'Main', 'container' => 'nav' )); ?>
    </div>
    
     <div class="mobile-menu">
                                <div id="nav-icon2" class="open-mm">
  <span></span>
  <span></span>
  <span></span>
  <span></span>
  <span></span>
  <span></span>
</div>
        
</div>
</div>  
