<?php get_header()?>

<div class="flexslider">
  <ul class="slides">
   
   
   
   
   <?php
   $args = array('cat' => 7);
   $category_posts = new WP_Query($args);
 
   if($category_posts->have_posts()) : 
      while($category_posts->have_posts()) : 
         $category_posts->the_post();
?>
      
      
      
        <li>
      <img src="<?php echo get_the_post_thumbnail_url(); ?>" />
      
      <div class="caption">
          
          <h4> <?php exclude_post_categories('7', ', '); ?></h4>
          <h2><?php the_title(); ?></a></h2>
          <p>25 enero 2017</p>
          <a href="<?php the_permalink(); ?>" class="btn-default-gp btnf2">Ver más</a>
          
      </div>
    </li>
       
<?php
      endwhile;
   else: 
?>
      Vaya, no hay entradas.
<?php
   endif;
?>
   
   
  </ul>
</div>

<div class="container-fluid">
<center>
    <h2><span>Go Playa</span> Blog</h2>
</center>



<div class="row">
   
   <!--Contendio-->
   <div class="col-xs-12 col-md-9">
    
    <div class="row is-flex">
    
    <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
    
     <div class="col-xs-12 col-md-6 item">
         
         <img class="img-responsive" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
         <div class="info">
         <?php exclude_post_categories('7', ', '); ?>
         <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
          <?php the_excerpt(); ?>
         </div>
         
     </div>
<?php endwhile; else: ?>
    <p><?php _e('No hay entradas .'); ?></p>
  <?php endif; ?>
  
   </div> 
   </div>
    <!--Fin Contendio-->
    
    
     <!--Sidebar-->
<div class="col-xs-12 col-md-3">
    <?php get_sidebar(); ?>
</div>
  <!--Fin Sidebar-->
    
    
</div>

    
   <div class="row">
       
       
       
   </div>
     

 

 

 

<?php get_footer()?>