<div class="search_form_wrap">
	<form name="search_form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" class="search_form">
		<input class="search-field" type="text" name="s" placeholder="<?php esc_html_e('<!--:en-->Type your search<!--:--><!--:es-->Digite su búsqueda<!--:-->', 'newsider'); ?>" value="" />
		<input class="search-submit" type="submit" value="" />
	</form>
	<i class="fa fa-search"></i>
</div>
<i class="search_form_close"><?php echo esc_html__('<!--:en-->Close<!--:--><!--:es-->Cerrar<!--:-->','newsider'); ?></i>