<?php
/**
 * The main template file
 */

get_header();

$newsider_sidebar_layout = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options('blog_layout') : 'right-sidebar';
if( $newsider_sidebar_layout == 'left-sidebar' ) {
	$newsider_sidebar_class = 'pull-left ';
	$newsider_blog_list_wrap_class = 'left_sidebar ';
	$newsider_blog_list_class = 'col-md-8 pull-right';
} elseif( $newsider_sidebar_layout == 'right-sidebar' ) {
	$newsider_sidebar_class = 'pull-right';
	$newsider_blog_list_wrap_class = 'right_sidebar ';
	$newsider_blog_list_class = 'col-md-8 pull-left ';
} else {
	$newsider_sidebar_class = $newsider_blog_list_class = '';
	$newsider_blog_list_wrap_class = 'no_sidebar ';
}
?>
		
		<div id="blog_list" class="container default mt0 <?php echo esc_attr( $newsider_blog_list_wrap_class ); ?>">
			<div class="row">
			
			<?php
			if( $newsider_sidebar_layout != 'no-sidebar' ) {
			echo '
				<div class="blog_list_wrap ' . esc_attr( $newsider_blog_list_class ) . '">
					<div class="row">
				';
			}
			?>

						<?php

							while (have_posts()) {
								
								the_post();
								
								get_template_part('templates/blog/loop');
								
							}

						?>
					
			<?php
			if( $newsider_sidebar_layout != 'no-sidebar' ) {
			echo '
					</div>';
					
					echo newsider_pagination( $pages = '' );
					
				echo '
				</div>
				';
			}
			?>
				
				<?php if( $newsider_sidebar_layout != 'no-sidebar' ) { ?>
					<div class="col-md-4 blogsidebar_wrap <?php echo esc_attr( $newsider_sidebar_class ); ?>">
						<?php get_sidebar(); ?>
					</div>
				<?php } ?>
				
				<?php if( $newsider_sidebar_layout == 'no-sidebar' ) { ?>
					<?php echo newsider_pagination( $pages = '' ); ?>
				<?php } ?>
				
			</div>
			
		</div>
       
        
<?php get_footer(); ?>