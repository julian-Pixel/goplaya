<?php

/* Load frontend scripts and styles */
add_action( 'wp_enqueue_scripts', 'newsider_load_scripts' );

/**
 * Load scripts and styles on frontend
 */

function newsider_load_scripts() {
	newsider_load_css();
	newsider_load_js();
}

/**
 * Load frontend css files
 */

function newsider_load_css() {
	
	global $post;
	
	$newsider_postid = ( isset( $post->ID ) ? get_the_ID() : NULL );
	$blogsingle_gallery_style = get_post_meta($newsider_postid, 'newsider_blogsingle_gallery_style', true);

	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.4', 'all' );
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.6.3', 'all' );
	wp_enqueue_style('newsider-owlcarousel', get_template_directory_uri() . '/css/custom-owlcarousel.css');
	wp_enqueue_style('justified-gallery', get_template_directory_uri() . '/css/justifiedGallery.min.css', array(), '3.6.3');
	wp_enqueue_style('swipebox', get_template_directory_uri() . '/css/swipebox.min.css', array(), '1.3.0');
	wp_enqueue_style('newsider-theme', get_template_directory_uri() . '/css/theme-style.css');
	wp_add_inline_style('newsider-theme', newsider_generate_dynamic_css() );
	wp_enqueue_style('newsider-responsive', get_template_directory_uri() . '/css/responsive.css');
	wp_enqueue_style('newsider-default', get_stylesheet_uri());
	if( ! class_exists('ReduxFrameworkPlugin' ) ) {
		wp_enqueue_style('newsider-google-fonts', newsider_google_fonts_url(), array(), '1.0.0' );
	}

}


/**
 * Load frontend js files
 */

function newsider_load_js() {

	global $post;
	
	$newsider_postid = ( isset( $post->ID ) ? get_the_ID() : NULL );
	$blogsingle_gallery_style = get_post_meta($newsider_postid, 'newsider_blogsingle_gallery_style', true);

	wp_enqueue_script('jquery');
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.3.4', true);
	if ( newsider_options('theia_sticky_sidebar') != 0) {
		wp_enqueue_script('newsider-stickysidebar', get_template_directory_uri() . '/js/custom-stickysidebar.js', array(), false, true);
	}
	wp_enqueue_script('newsider-owlcarousel', get_template_directory_uri() . '/js/custom-owlcarousel.js', array(), false, true);
	if( get_page_template_slug() == "page-comingsoon.php" ) {
		wp_enqueue_script('newsider-downCount', get_template_directory_uri() . '/js/custom-downCount.js', array(), false, true);
	}
	wp_enqueue_script('newsider-jflickrfeed', get_template_directory_uri() . '/js/custom-jflickrfeed.js', array(), false, true);
	wp_enqueue_script('newsider-parallax', get_template_directory_uri() . '/js/custom-parallax.js', array(), '', true);
	wp_enqueue_script('justified-gallery', get_template_directory_uri() . '/js/jquery.justifiedGallery.min.js', 'jquery', '3.6.3', true);
	wp_enqueue_script('swipebox', get_template_directory_uri() . '/js/jquery.swipebox.min.js', 'jquery', '1.4.4', true);
	wp_enqueue_script('lazyload', get_template_directory_uri() . '/js/jquery.lazyload.min.js', 'jquery', '1.9.7', true);
	wp_enqueue_script('newsider-script', get_template_directory_uri() . '/js/theme-script.js', array(), '', true);
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
	//	Coming Soon Page
	if( get_page_template_slug() == "page-comingsoon.php" ) {
		
		$newsider_coming_soon_years = get_post_meta( $post->ID, 'newsider_coming_soon_years', true );
		$newsider_coming_soon_months = get_post_meta( $post->ID, 'newsider_coming_soon_months', true );
		$newsider_coming_soon_days = get_post_meta( $post->ID, 'newsider_coming_soon_days', true );
		
		wp_add_inline_script( 'newsider-script', '
			
			jQuery(document).ready(function() {
				jQuery(".countdown").downCount({
					date: "'. esc_html( $newsider_coming_soon_months ) .'/'. esc_html( $newsider_coming_soon_days ) .'/'. esc_html( $newsider_coming_soon_years ) .' 12:00:00"
				});
			});
		
		' );
		
	}
}


/* Custom js */
function newsider_custom_js() {
	
	$newsider_custom_js = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options('custom-js') : '';
	
	if( !empty( $newsider_custom_js ) ) {
		echo $newsider_custom_js;
	}
}
add_action('wp_footer', 'newsider_custom_js', 100);


/* Load backend scripts and styles */
add_action( 'admin_enqueue_scripts', 'newsider_load_admin_scripts' );

/**
 * Load scripts and styles on backend
 */

function newsider_load_admin_scripts() {
	newsider_load_admin_css();
	newsider_load_admin_js();
}

/**
 * Load backend css files
 */

function newsider_load_admin_css() {

	wp_enqueue_style('newsider-admin', get_template_directory_uri() . '/framework/assets/css/custom-admin.css' );
	wp_enqueue_style('newsider-redux', get_template_directory_uri() . '/framework/assets/css/custom-redux-styles.css' );
	wp_enqueue_style('newsider-admin-google-fonts', newsider_admin_google_fonts_url(), array(), '1.0.0' );

}


/**
 * Load backend js files
 */

function newsider_load_admin_js() {

	wp_enqueue_script( 'custom-admin-js', get_template_directory_uri() . '/framework/assets/js/admin.js', array('jquery'), false, true);
}


/**
 * Register Google Fonts url
 */

function newsider_google_fonts_url(){
	
	$google_font_url = add_query_arg( 'family', urlencode( 'Rubik:300,400,500,700&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );
    
	return $google_font_url;
}


//	Admin Styles
function newsider_admin_google_fonts_url(){
	
	$admin_google_font_url = add_query_arg( 'family', urlencode( 'Lato:300,400,700&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );
    
	return $admin_google_font_url;
}
?>