<?php

/**
 * Widget Name: List Categories with Thumbnails
 */

class newsider_widget_cat_thumbnails extends WP_Widget {

    function newsider_widget_cat_thumbnails() {
        parent::__construct( false, esc_html__('Evatheme Categories with Thumbnails','newsider') );
    }
 
	function widget($args, $instance) {
		extract( $args );
		$title 		= apply_filters('widget_title', $instance['title']);
		$number 	= $instance['number'];

		$args = array(
			'number' 	=> $number,
		);
		
		$cats = get_categories($args);
		?>
			<?php echo $before_widget; ?>
			
			<?php if ( $title ) { echo $before_title . $title . $after_title; } ?>
				
			<ul>
				<?php foreach( $cats as $cat ) {
	
					if( $cat->slug == 'uncategorized' ) {
						continue;
					}
					
					echo '<li>';
				
						$query = new WP_Query( array(
							'posts_per_page' => 1,
							'cat' => $cat->term_id,
							'meta_query' => array(
								array(
									'key' => '_thumbnail_id',
								),
							),
						) );
						
						if( $query->have_posts() ) {
							$post_id = $query->posts[0]->ID;
							$img_url = wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
							$img = '<img src="' . newsider_aq_resize( $img_url, 80, 50, true, true, true) . '" alt="' . get_the_title() . '" width="80" height="50" />';
						} else {
							$img = '';
						}
						
						$link = get_category_link( $cat->term_id );
						$count = $cat->category_count;
						
						echo '
							<a href="' . esc_url( $link ) . '" class="cat_block ' . esc_attr( $cat->slug ) . '">
								' . $img . '
								<h6>' . esc_attr( $cat->slug ) . '</h6>
								<span>' . esc_html( $count ) . ' ' . esc_html__('Posts','newsider') . '</span>
							</a>
						';
				
					echo '</li>';
					
				} ?>
			</ul>
			
			<?php echo $after_widget; ?>
		<?php
	}
 
	/** @see WP_Widget::update -- do not rename this */
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = strip_tags($new_instance['number']);
		
		return $instance;
	}

    function form($instance) {
 
        $title 		= esc_attr($instance['title']);
        $number		= esc_attr($instance['number']);
        
        ?>
		
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:','newsider'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
		<p>
			<label for="<?php echo $this->get_field_id('number'); ?>"><?php esc_html_e('Number of categories to display','newsider'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" />
        </p>

        <?php
    }
 
 
} // end class newsider_widget_cat_thumbnails
add_action('widgets_init', create_function('', 'return register_widget("newsider_widget_cat_thumbnails");'));
?>