<?php

/**
 * Register sidebars
 */
function newsider_widgets_init() {
	
	//	Blog Sidebar
	register_sidebar( array(
		'name'          	=> esc_html__( 'Blog Sidebar', 'newsider' ),
		'id'            	=> 'blog-sidebar',
		'before_widget' 	=> '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  	=> '</aside>',
		'before_title'  	=> '<h4 class="widget-title"><span>',
		'after_title'   	=> '</span></h4>',
	) );
	
	//	Single Post Sidebar
	register_sidebar( array(
		'name'          	=> esc_html__( 'Single Post Sidebar', 'newsider' ),
		'id'            	=> 'singlepost-sidebar',
		'before_widget' 	=> '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  	=> '</aside>',
		'before_title'  	=> '<h4 class="widget-title"><span>',
		'after_title'   	=> '</span></h4>',
	) );
	
	//	Prefooter Area
	$newsider_widgets_grid = newsider_options('prefooter_col') ? newsider_options('prefooter_col') : '4-4-4';
	$i = 1;
	foreach (explode('-', $newsider_widgets_grid) as $newsider_widgets_g) {
		register_sidebar(array(
			'name' 				=> esc_html__('Prefooter Area ', 'newsider') . $i,
			'id' 				=> "footer-area-$i",
			'description' 		=> esc_html__('The prefooter area widgets', 'newsider'),
			'before_widget' 	=> '<aside id="%1$s" class="widget %2$s">',
			'after_widget' 		=> '</aside>',
			'before_title' 		=> '<h4 class="widget-title">',
			'after_title'   	=> '</h4>',
		));
		$i++;
	}
	
	//	Unlimited Sidebars
	$newsider_options_unlimited_sidebar = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options('unlimited_sidebar') : '';
	
	if ( !empty( $newsider_options_unlimited_sidebar ) ) {	
		if( sizeof( $newsider_options_unlimited_sidebar > 0 ) ) {
			foreach( $newsider_options_unlimited_sidebar as $newsider_sidebar ) {
				if ( !empty( $newsider_sidebar ) ) {
					register_sidebar( array(
						'name' 				=> $newsider_sidebar,
						'id' 				=> newsider_generateSlug($newsider_sidebar, 45),
						'before_widget' 	=> '<aside id="%1$s" class="widget %2$s">',
						'after_widget' 		=> '</aside>',
						'before_title' 		=> '<h4 class="widget-title">',
						'after_title'   	=> '</h4>',
					));
				}
			}
		}
	}
	
}
add_action( 'widgets_init', 'newsider_widgets_init' );


/**
 * Unlimited Sidebars
 */

function newsider_generateSlug($phrase, $maxLength) {
    $result = strtolower($phrase);
    $result = preg_replace("/[^a-z0-9\s-]/", "", $result);
    $result = trim(preg_replace("/[\s-]+/", " ", $result));
    $result = trim(substr($result, 0, $maxLength));
    $result = preg_replace("/\s/", "-", $result);
    return $result;
}


/**
 * Include widgets
 */
require_once( get_template_directory() . '/framework/widgets/widget-flickr.php' );
require_once( get_template_directory() . '/framework/widgets/widget-posts.php' );
require_once( get_template_directory() . '/framework/widgets/widget-social.php' );
require_once( get_template_directory() . '/framework/widgets/widget-cat.php' );
require_once( get_template_directory() . '/framework/widgets/widget-instagram.php' );