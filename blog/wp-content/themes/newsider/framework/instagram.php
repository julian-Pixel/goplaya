<?php

/**
 * Footer Instagram
 */

global $post;

$newsider_footer_instagram_username = strtolower( newsider_options('footer_instagram_username') );
$newsider_footer_instagram_url = 'http://instagram.com/' . $newsider_footer_instagram_username;
$limit = '6';
$size = 'small';
$target = '_blank';

if( $newsider_footer_instagram_username !== '' ) {
	$media_array = newsider_footer_scrape_instagram($newsider_footer_instagram_username, $limit);
	if (is_wp_error($media_array)) {
		echo balanceTags($media_array->get_error_message());
	} else {
		
		// filter for images only?
		if ($newsider_footer_instagram_imgs_only = apply_filters('newsider_images_only', FALSE)){
			$media_array = array_filter($media_array, array($post->ID, 'newsider_footer_instagram_imgs_only'));
		}
		
		// filters for custom classes
		$ulclass = esc_attr(apply_filters('newsider_list_class', 'clearfix instagram-pics instagram-size-' . $size)); ?>
		<div id="instagram_wrap">
			<a class="footer_instagram_user_btn" href="<?php echo esc_url( $newsider_footer_instagram_url ); ?>"><i class="fa fa-instagram"></i><?php echo esc_html( $newsider_footer_instagram_username ); ?></a>
			<ul class="<?php echo esc_attr($ulclass); ?>"><?php
				foreach($media_array as $item){
					echo '<li><a href="' . esc_url($item['link']) . '" target="' . esc_attr($target) . '" title="" ><img src="' . esc_url($item[$size]) . '"  alt="' . esc_attr($item['description']) . '" title="' . esc_attr($item['description']) . '"  /></a><div class="instagram_meta"><span><i class="fa fa-heart"></i>' . esc_html($item['likes']) . '</span><span><i class="fa fa-comment"></i>' . esc_html($item['comments']) . '</span></div></li>';
				} ?>
			</ul>
		</div>
		<?php
	}
}

// based on https://gist.github.com/cosmocatalano/4544576
function newsider_footer_scrape_instagram($newsider_footer_instagram_username, $slice = 9) {

	$newsider_footer_instagram_username = strtolower($newsider_footer_instagram_username);
	$newsider_footer_instagram_username = str_replace('@', '', $newsider_footer_instagram_username);

	if (false === ( $instagram = get_transient('instagram-media-5-' . sanitize_title_with_dashes($newsider_footer_instagram_username)) )) {

		$remote = wp_remote_get('http://instagram.com/' . trim($newsider_footer_instagram_username));

		if (is_wp_error($remote))
			return new WP_Error('site_down', esc_html__('Unable to communicate with Instagram.', 'newsider'));

		if (200 != wp_remote_retrieve_response_code($remote))
			return new WP_Error('invalid_response', esc_html__('Instagram did not return a 200.', 'newsider'));

		$shards = explode('window._sharedData = ', $remote['body']);
		$insta_json = explode(';</script>', $shards[1]);
		$insta_array = json_decode($insta_json[0], TRUE);

		if (!$insta_array)
			return new WP_Error('bad_json', esc_html__('Instagram has returned invalid data.', 'newsider'));

		if (isset($insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'])) {
			$images = $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
		} else {
			return new WP_Error('bad_json_2', esc_html__('Instagram has returned invalid data.', 'newsider'));
		}

		if (!is_array($images))
			return new WP_Error('bad_array', esc_html__('Instagram has returned invalid data.', 'newsider'));

		$instagram = array();

		foreach ($images as $image) {
			$image['thumbnail_src'] = preg_replace("/^https:/i", "", $image['thumbnail_src']);
			$image['thumbnail'] = str_replace('s640x640', 's160x160', $image['thumbnail_src']);
			$image['small'] = str_replace('s640x640', 's320x320', $image['thumbnail_src']);
			$image['large'] = $image['thumbnail_src'];
			$image['display_src'] = preg_replace("/^https:/i", "", $image['display_src']);

			if ($image['is_video'] == true) {
				$type = 'video';
			} else {
				$type = 'image';
			}

			$caption = esc_html__('Instagram Image', 'newsider');
			if (!empty($image['caption'])) {
				$caption = $image['caption'];
			}

			$instagram[] = array(
				'description' => $caption,
				'link' => '//instagram.com/p/' . $image['code'],
				'time' => $image['date'],
				'comments' => $image['comments']['count'],
				'likes' => $image['likes']['count'],
				'thumbnail' => $image['thumbnail'],
				'small' => $image['small'],
				'large' => $image['large'],
				'original' => $image['display_src'],
				'type' => $type
			);
		}

		// do not set an empty transient - should help catch private or empty accounts
		if (!empty($instagram)) {
			$instagram = serialize($instagram);
			set_transient('instagram-media-5-' . sanitize_title_with_dashes($newsider_footer_instagram_username), $instagram, apply_filters('newsider_instagram_cache_time', HOUR_IN_SECONDS * 2));
		}
	}

	if (!empty($instagram)) {
		$instagram = unserialize($instagram);
		return array_slice($instagram, 0, $slice);
	} else {
		return new WP_Error('no_images', esc_html__('Instagram did not return any images.', 'newsider'));
	}
}

function newsider_footer_instagram_imgs_only($media_item) {
	if ($media_item['type'] == 'image'){
		return true;
	}
	return false;
}