<?php
/**
 * Custom functions and definitions
 */


//	Add specific CSS class by filter
add_filter( 'body_class', 'newsider_body_class' );
function newsider_body_class( $classes ) {

	global $post;

	$newsider_postId = ( isset( $post->ID ) ? get_the_ID() : NULL );

	//	If Site Boxed
	$newsider_theme_layout = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options('theme_layout') : 'full-width';
	if ($newsider_theme_layout == 'boxed') {
		$classes[] = 'boxed';
	} else {
		$classes[] = 'full-width';
	}

	$newsider_header_type = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options('header_type') : 'type4';
	$classes[] = 'header_' . $newsider_header_type;

	return $classes;
}


//	Theme Options
function newsider_options($index1, $index2 = false) {
    $options = get_option("newsider_option");
    if ($index2) {
        $output = isset($options[$index1][$index2]) ? $options[$index1][$index2] : false;
        return $output;
    }
    $output = isset($options[$index1]) ? $options[$index1] : false;
    return $output;
}


//	Theme Preloader
function newsider_preloader() {

	$newsider_preloader = ( newsider_options('preloader') != '' ) ? newsider_options('preloader') : 1;

	if( $newsider_preloader != 0 ) {
		echo '<div id="loader" class="loading"><span></span></div>';
	}
}


/**
 * Theme Logo
 */
if ( ! function_exists( 'newsider_logo' ) ) {
	function newsider_logo() {

		if(qtranxf_getLanguage() == "es") {
					$lm = '?idioma=es';
		}

		if(qtranxf_getLanguage() == "en") {
					$lm = '?idioma=en';
		}

		if ( ! class_exists( 'ReduxFrameworkPlugin' ) ) {
			$newsider_logo_src 		= get_template_directory_uri() . '/images/logo.png';
			$newsider_logo_retina_src 	= '';
		} else {
			$newsider_logo_src 		= newsider_options( 'header-logo','url' );
			$newsider_logo_retina_src 	= newsider_options( 'header-logo-retina','url' );
		}

		echo '<div class="cstheme-logo">';
			if ( $newsider_logo_src != '' || $newsider_logo_retina_src != '' ) {
				echo '<a class="logo" href="https://goplaya.cr'.$lm.'">';
					if( $newsider_logo_retina_src != '' ) {
						echo '<img class="logo-img retina" src="' . esc_url( $newsider_logo_retina_src ) . '" alt="' . get_bloginfo( 'name' ) . '" />';
					} else {
						echo '<img class="logo-img" src="' . esc_url( $newsider_logo_src ) . '" alt="' . get_bloginfo( 'name' ) . '" />';
					}
				echo '</a>';
			} else {
				echo '<h1 class="site-name">';
					echo '<a class="logo" href="https://goplaya.cr/">';
						bloginfo('name');
					echo '</a>';
				echo '</h1>';
			}
		echo '</div>';
	}
}


/**
 *	Theme Favicon
 */
function newsider_favicon() {

	if ( ! function_exists( 'wp_site_icon' ) || ! has_site_icon() ) {
		if ( ! class_exists( 'ReduxFrameworkPlugin' ) && newsider_options('favicon', 'url') == '' ) {
			echo '<link rel="shortcut icon" href="'. get_template_directory_uri() . '/images/favicon.ico" />';
		} else {
			echo '<link rel="shortcut icon" href="' . esc_url( newsider_options('favicon', 'url') ) . '"/>';
		}

		echo newsider_options('apple_icons_57x57', 'url') != '' ? ('<link rel="apple-touch-icon" href="' . esc_url( newsider_options('apple_icons_57x57', 'url') ) . '"/>') : '';
        echo newsider_options('apple_icons_72x72','url') != '' ? ('<link rel="apple-touch-icon" sizes="114x114" href="' . esc_url( newsider_options('apple_icons_72x72','url') ) . '"/>') : '';
        echo newsider_options('apple_icons_114x114','url') != '' ? ('<link rel="apple-touch-icon" sizes="72x72" href="' . esc_url( newsider_options('apple_icons_114x114','url') ) . '"/>') : '';
	}

}


/**
 * Generate dynamic css
 */

if ( !function_exists( 'newsider_generate_dynamic_css' ) ):
	function newsider_generate_dynamic_css() {
		ob_start();
		get_template_part( 'css/dynamic-css' );
		$output = ob_get_contents();
		ob_end_clean();
		return newsider_compress_css_code( $output );
	}
endif;


/**
 * Compress CSS Code
 */

if ( !function_exists( 'newsider_compress_css_code' ) ) :
	function newsider_compress_css_code( $code ) {

		// Remove Comments
		$code = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $code );

		// Remove tabs, spaces, newlines, etc.
		$code = str_replace( array( "\r\n", "\r", "\n", "\t", '  ', '    ', '    ' ), '', $code );

		return $code;
	}
endif;


/**
 * Social Links
 */
if ( ! function_exists( 'newsider_social_links' ) ) {

	function newsider_social_links() {

		$output = '';
		$social_links = array();

		if( newsider_options('facebook_link') ) {
			$social_links['facebook'] = newsider_options('facebook_link');
		}

		if( newsider_options('twitter_link') ) {
			$social_links['twitter'] = newsider_options('twitter_link');
		}

		if( newsider_options('linkedin_link') ) {
			$social_links['linkedin'] = newsider_options('linkedin_link');
		}

		if( newsider_options('pinterest_link') ) {
			$social_links['pinterest'] = newsider_options('pinterest_link');
		}

		if( newsider_options('googleplus_link') ) {
			$social_links['google-plus'] = newsider_options('googleplus_link');
		}

		if( newsider_options('youtube_link') ) {
			$social_links['youtube'] = newsider_options('youtube_link');
		}

		if( newsider_options('rss_link') ) {
			$social_links['rss'] = newsider_options('rss_link');
		}

		if( newsider_options('tumblr_link') ) {
			$social_links['tumblr'] = newsider_options('tumblr_link');
		}

		if( newsider_options('reddit_link') ) {
			$social_links['reddit'] = newsider_options('reddit_link');
		}

		if( newsider_options('dribbble_link') ) {
			$social_links['dribbble'] = newsider_options('dribbble_link');
		}

		if( newsider_options('digg_link') ) {
			$social_links['digg'] = newsider_options('digg_link');
		}

		if( newsider_options('flickr_link') ) {
			$social_links['flickr'] = newsider_options('flickr_link');
		}

		if( newsider_options('instagram_link') ) {
			$social_links['instagram'] = newsider_options('instagram_link');
		}

		if( newsider_options('vimeo_link') ) {
			$social_links['vimeo'] = newsider_options('vimeo_link');
		}

		if( newsider_options('skype_link') ) {
			$social_links['skype'] = newsider_options('skype_link');
		}

		if( newsider_options('yahoo_link') ) {
			$social_links['yahoo'] = newsider_options('yahoo_link');
		}

		$icon_class = '';
		$social_link = '';

		if( isset( $social_links ) && is_array( $social_links ) ) {
			foreach( $social_links as $icon => $link ) {
				$icon_class = $icon;

				$icon = 'fa fa-' . $icon;

				$social_link .= '<a class="' . esc_attr( $icon_class ) . '" href="' . esc_url( $link ) . '" target="_blank"><i class="' . esc_attr( $icon ) . '"></i><i class="' . esc_attr( $icon ) . '"></i></a>';
			}
		}

		if( isset( $social_link ) && $social_link != '' ) {
			$output .= '<div class="social_links">';
				$output .= $social_link;
			$output .= '</div>';
		}

		return $output;
	}
}


/**
 * Post excerpt
 */
if (!function_exists('newsider_smarty_modifier_truncate')) {
    function newsider_smarty_modifier_truncate($string, $length = 80, $etc = '... ',
		$break_words = false)
    {
        if ($length == 0)
            return '';

        if (mb_strlen($string, 'utf8') > $length) {
            $length -= mb_strlen($etc, 'utf8');
            if (!$break_words) {
                $string = preg_replace('/\s+\S+\s*$/su', '', mb_substr($string, 0, $length + 1, 'utf8'));
            }
            return mb_substr($string, 0, $length, 'utf8') . $etc;
        } else {
            return $string;
        }
    }
}


/**
 * Single Post Comments List
 */

if (!function_exists('newsider_comment')) {
    function newsider_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment; ?>

		<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
			<div id="comment-<?php comment_ID(); ?>" class="comment-body">
				<div class="comment-meta clearfix">
					<span class="comment-avatar">
						<?php echo get_avatar($comment, $size = '50'); ?>
					</span>
					<h6 class="comment_author"><?php comment_author(); ?><?php edit_comment_link( esc_html__( '<!--:en-->Edit<!--:--><!--:es-->Editar<!--:-->', 'newsider'),' ','' ) ?></h6>
					<span class="comment-date"><?php comment_date('M j, Y'); ?></span>
					<?php comment_reply_link(array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => esc_attr__('<!--:en-->Replay<!--:--><!--:es-->Repetir<!--:-->', 'newsider') ) ) ) ?>
				</div>
				<div class="comment-content">
					<div class="comment-text clearfix">
						<?php comment_text() ?>
						<?php if ( $comment->comment_approved == '0' ) : ?>
							<em><?php  esc_html__("<!--:en-->Your comment is awaiting moderation.<!--:--><!--:es-->Su comentario está en espera de moderación.<!--:-->");?></em>
							<br>
						<?php endif; ?>
					</div>
				</div>
			</div>
	<?php
	}
}

function newsider_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}
add_filter( 'comment_form_fields', 'newsider_comment_field_to_bottom' );


//	Pagination
if (!function_exists('newsider_pagination')) {
    function newsider_pagination($type = "") {

		global $wp_query;

		if ($type == "query2") {

			$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

			echo '<div class="eva-pagination">';

				$pagination = paginate_links( array(
					'base' 			=> @add_query_arg('page','%#%'),
					'format' 		=> '',
					'total' 		=> $wp_query->max_num_pages,
					'current' 		=> $current,
					'show_all' 		=> false,
					'type' 			=> '',
					'prev_next' 	=> true,
					'prev_text' 	=> esc_html__("<!--:en-->Previous<!--:--><!--:es-->Anterior<!--:-->"),
					'next_text' 	=> esc_html__("<!--:en-->Next<!--:--><!--:es-->Siguiente<!--:-->"), //esc_html__('Next', 'newsider'),
					'before_page_number' => '',
				) );

				echo $pagination;

			echo '</div>';

		} else {

			$pages = $wp_query->max_num_pages;
			if (empty($pages)) {
				$pages = 1;
			}
			if ( get_query_var('paged') ) {
				$paged = get_query_var('paged');
			} elseif ( get_query_var('page') ) {
				$paged = get_query_var('page');
			} else {
				$paged = 1;
			}

			if (1 != $pages) {
				$big = 9999; // need an unlikely integer

				echo '<div class="eva-pagination">';

				$pagination = paginate_links(
						array(
							'base' 			=> str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
							'format' 		=> '',
							'current' 		=> max(1, $paged),
							'total' 		=> $pages,
							'type' 			=> '',
							'prev_next' 	=> true,
							'prev_text' 	=> esc_html__("<!--:en-->Previous<!--:--><!--:es-->Anterior<!--:-->"), //esc_html__('Previous', 'newsider'),
							'next_text' 	=> esc_html__("<!--:en-->Next<!--:--><!--:es-->Siguiente<!--:-->"), //esc_html__('Next', 'newsider'),
							'before_page_number' => '',
						)
					);

					echo $pagination;

				echo '</div>';
			}
		}
    }
}


/**
 *	Load More button
 */

if (!function_exists('newsider_infinite_scroll')) {
	function newsider_infinite_scroll() {

		global $paged, $wp_query;

        $range = 3;

        if (empty($paged)) {
            $paged = (get_query_var('page')) ? get_query_var('page') : 1;
        }

		$pages = intval($wp_query->max_num_pages);

		if (empty($pages)) {
			$pages = 1;
		}

		if (1 != $pages) {
			echo '<div class="eva-infinite-scroll text-center" data-has-next="' . ( $paged === $pages ? 'false' : 'true' ) . '">';
				echo '<a class="btn-infinite-scroll no-more hide" href="#">' . esc_html__("<!--:en-->No more<!--:--><!--:es-->No más<!--:-->") . '</a>';
				echo '<a class="btn-infinite-scroll loading" href="#"><i class="fa fa-rotate-right fa-spin"></i>' . esc_html__("<!--:en-->Loading<!--:--><!--:es-->Cargando<!--:-->") . '</a>';
				echo '<a class="btn-infinite-scroll next" href="' . esc_url( get_pagenum_link($paged + 1) ) . '"><i class="fa fa-rotate-right"></i>' . esc_html__("<!--:en-->Load More<!--:--><!--:es-->Cargar más<!--:-->") . '</a>';
			echo '</div>';
		}
	}
}


//	Posts per author posts page
add_filter( 'pre_option_posts_per_page', 'newsider_author_posts_per_page' );
function newsider_author_posts_per_page( $posts_per_page ) {
	global $wp_query;
	if ( is_author() ) {
		return 9;
	}

	if ( is_search() ) {
		return 10;
	}

	return $posts_per_page;
}


//	Add OpenGraph Meta
function newsider_add_opengraph() {
    global $post; // Ensures we can use post variables outside the loop
    if(!$post) return;

    // Start with some values that don't change.
    echo "<meta property='og:site_name' content='". get_bloginfo('name') ."'/>"; // Sets the site name to the one in your WordPress settings
    echo "<meta property='og:url' content='" . get_permalink() . "'/>"; // Gets the permalink to the post/page

    if (is_singular()) { // If we are on a blog post/page
        echo "<meta property='og:title' content='" . get_the_title() . "'/>"; // Gets the page title
        echo "<meta property='og:type' content='article'/>"; // Sets the content type to be article.
    } elseif(is_front_page() or is_home()) { // If it is the front page or home page
        echo "<meta property='og:title' content='" . get_bloginfo("name") . "'/>"; // Get the site title
        echo "<meta property='og:type' content='website'/>"; // Sets the content type to be website.
    }

    if(has_post_thumbnail( $post->ID )) { // If the post has a featured image.
        $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
        echo "<meta property='og:image' content='" . esc_attr( $thumbnail[0] ) . "'/>"; // If it has a featured image, then display this for Facebook
    }

}
add_action( 'wp_head', 'newsider_add_opengraph', 5 );


//	Display navigation to next/previous single post.
if ( ! function_exists( 'newsider_post_navigation' ) ) {
	function newsider_post_navigation() {

		$newsider_prev_post = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$newsider_next_post = get_adjacent_post( false, '', false );

		if ( ! $newsider_next_post && ! $newsider_prev_post ) {
			return;
		}

		echo '<div class="single_post_nav clearfix">';

			if( $newsider_prev_post ) {
				$url = get_permalink($newsider_prev_post->ID);
				$image_id = get_post_thumbnail_id($newsider_prev_post->ID);
				$image_link = wp_get_attachment_image_src($image_id,'full');
				$image = newsider_aq_resize( $image_link[0], 60, 60, true, true, true);
				$title = get_the_title( $newsider_prev_post->ID );

				echo '
					<div class="single_post_nav_div pull-left '; if( empty( $image_link ) ){ echo 'no_thumbnail'; } echo '">
						<div class="previous_post">
							<a class="prev_post_arrow" href="' . esc_url( $url ) . '" title="' . esc_html( $title ) . '"><i class="fa fa-angle-left"></i>' .esc_html__('<!--:en-->Previous post<!--:--><!--:es-->Artículo anterior<!--:-->', 'newsider') . '</a>
							<a class="prev_post_thumb" href="' . esc_url( $url ) . '" title="' . esc_html( $title ) . '">';
								if( !empty( $image_link ) ){
									echo '<img src="' . $image . '" alt="' . esc_html( $title ) . '" />';
								}
								echo '<h6>' . $title . '</h6>
							</a>
						</div>
					</div>
				';
			}

			if( $newsider_next_post ) {
				$url = get_permalink($newsider_next_post->ID);
				$image_id = get_post_thumbnail_id($newsider_next_post->ID);
				$image_link = wp_get_attachment_image_src($image_id,'full');
				$image = newsider_aq_resize( $image_link[0], 60, 60, true, true, true);
				$title = get_the_title( $newsider_next_post->ID );

				echo '
					<div class="single_post_nav_div pull-right '; if( empty( $image_link ) ){ echo 'no_thumbnail'; } echo '">
						<div class="next_post">
							<a class="next_post_arrow" href="' . esc_url( $url ) . '" title="' . esc_html( $title ) . '">' . esc_html__("<!--:en-->Next post<!--:--><!--:es-->Sigiente Artículo <!--:-->") . '<i class="fa fa-angle-right"></i></a>
							<a class="next_post_thumb" href="' . esc_url( $url ) . '" title="' . esc_html( $title ) . '">';
								if( !empty( $image_link ) ){
									echo '<img src="' . $image . '" alt="' . esc_html( $title ) . '" />';
								}
								echo '<h6>' . $title . '</h6>
							</a>
						</div>
					</div>
				';
			}

		echo '</div>';

	}
}


//	Widget Title empty if no Title
add_filter( 'widget_title', 'newsider_remove_widget_title' );
function newsider_remove_widget_title( $widget_title ) {
	if ( substr ( $widget_title, 0, 1 ) == '!' )
		return;
	else
		return ( $widget_title );
}


//	Custom Post Formats
function rename_post_formats( $safe_text ) {
    if ( $safe_text == 'Aside' )
        return 'Twitter';

	if ( $safe_text == 'Status' )
        return 'Instagram';

    return $safe_text;
}
add_filter( 'esc_html', 'rename_post_formats' );

//rename Aside, Status in posts list table
function live_rename_formats() {
    global $current_screen;

    if ( $current_screen->id == 'edit-post' ) { ?>
        <script type="text/javascript">
        jQuery('document').ready(function() {

            jQuery("span.post-state-format").each(function() {
                if ( jQuery(this).text() == "Aside" )
                    jQuery(this).text("Twitter");

				if ( jQuery(this).text() == "Status" )
                    jQuery(this).text("Instagram");
            });

        });
        </script>
<?php }
}
add_action('admin_head', 'live_rename_formats');
