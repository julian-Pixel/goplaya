window.jQuery = window.$ = jQuery;

jQuery(document).ready(function($) {
	"use strict";
	
	/* Page template */
    var selector = "#page_template";
    jQuery('#cstheme_pagebuilder').fadeOut();
    jQuery(selector).bind('change', function(){
        var template = jQuery(this).val();
        if(template != 'default'){
            jQuery('#cstheme_pagebuilder').fadeOut();
        } else {
          jQuery('#cstheme_pagebuilder').fadeIn();
        }
    });    
    jQuery(selector).change();
	
	/* Categories Custom Color */
	jQuery( '.newsider-color-field' ).wpColorPicker();
	
});