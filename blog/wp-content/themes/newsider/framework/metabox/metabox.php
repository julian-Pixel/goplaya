<?php
/**
 * Adds custom metaboxes to the WordPress
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_action('admin_print_scripts', 'newsider_metabox_postsettings_admin_scripts');
if ( !function_exists('newsider_metabox_postsettings_admin_scripts')) {
    function newsider_metabox_postsettings_admin_scripts(){
        global $post, $pagenow;

        if (current_user_can('edit_posts') && ($pagenow == 'post-new.php' || $pagenow == 'post.php')) {
            if( isset($post) ) {
                wp_localize_script( 'jquery', 'script_data', array(
                    'post_id' => $post->ID,
                    'nonce' => wp_create_nonce( 'newsider-ajax' ),
                    'image_ids' => get_post_meta( $post->ID, 'gallery_image_ids', true ),
                    'label_create' => esc_html__("Create Featured Gallery", "newsider"),
                    'label_edit' => esc_html__("Edit Featured Gallery", "newsider"),
                    'label_save' => esc_html__("Save Featured Gallery", "newsider"),
                    'label_saving' => esc_html__("Saving...", "newsider")
                ));
            }
        }
    }
}

// The Metabox class
class newsider_Post_Metaboxes {
	private $post_types;
	
	/**
	 * Register this class with the WordPress API
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		// Post types to add the metabox to
		$this->post_types = apply_filters( 'newsider_main_metaboxes_post_types', array(
			'post' 			=> 'post',
			'page' 			=> 'page',
			'portfolio' 	=> 'portfolio',
		) );

		// Add metabox to corresponding post types
		foreach( $this->post_types as $key => $val ) {
			add_action( 'add_meta_boxes_'. $val, array( $this, 'post_meta' ), 11 );
		}

		// Save meta
		add_action( 'save_post', array( $this, 'save_meta_data' ) );

		// Load scripts for the metabox
		add_action( 'admin_enqueue_scripts', array( $this, 'newsider_metabox_admin_enqueue_scripts' ) );

		// Load custom css for metabox
		add_action( 'admin_print_styles-post.php', array( $this, 'newsider_metaboxes_css' ) );
		add_action( 'admin_print_styles-post-new.php', array( $this, 'newsider_metaboxes_css' ) );

		// Load custom js for metabox
		add_action( 'admin_footer-post.php', array( $this, 'newsider_metaboxes_js' ) );
		add_action( 'admin_footer-post-new.php', array( $this, 'newsider_metaboxes_js' ) );

	}

	/**
	 * The function responsible for creating the actual meta box.
	 *
	 * @since 1.0.0
	 */
	public function post_meta( $post ) {
		$obj = get_post_type_object( $post->post_type );
		add_meta_box(
			'newsider-metabox',
			$obj->labels->singular_name . ' ' . esc_html__( 'Settings', 'newsider' ),
			array( $this, 'display_meta_box' ),
			$post->post_type,
			'normal',
			'high'
		);
	}

	/**
	 * Enqueue scripts and styles needed for the metaboxes
	 *
	 * @since 1.0.0
	 */
	public static function newsider_metabox_admin_enqueue_scripts() {
		wp_enqueue_media();
		wp_enqueue_script( 'jquery' );
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker' );
	}

	/**
	 * Renders the content of the meta box.
	 *
	 * @since 1.0.0
	 */
	public function display_meta_box( $post ) {

		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'newsider_metabox', 'newsider_metabox_nonce' );

		// Get current post data
		$post_id   = $post->ID;
		$post_type = get_post_type();

		// Get tabs
		$tabs = $this->tabs_array();

		// Make sure tabs aren't empty
		if ( empty( $tabs ) ) {
			echo '<p>' . esc_html__('Hey your settings are empty, something is going on please contact your webmaster', 'newsider') . '</p>';
			return;
		}

		// Store tabs that should display on this specific page in an array for use later
		$active_tabs = array();
		foreach ( $tabs as $tab ) {
			$tab_post_type = isset( $tab['post_type'] ) ? $tab['post_type'] : '';
			if ( ! $tab_post_type ) {
				$display_tab = true;
			} elseif ( in_array( $post_type, $tab_post_type ) ) {
				$display_tab = true;
			} else {
				$display_tab = false;
			}
			if ( $display_tab ) {
				$active_tabs[] = $tab;
			}
		} ?>

		<ul class="wp-tab-bar">
			<?php
			// Output tab links
			$newsider_count = '';
			foreach ( $active_tabs as $tab ) {
				$newsider_count++;
				$li_class = ( '1' == $newsider_count ) ? 'wp-tab-active ' : '';
				$li_class .= 'metabox-tab' . $newsider_count;
				// Define tab title
				$tab_title = $tab['title'] ? $tab['title'] : esc_html__( 'Other', 'newsider' ); ?>
				<li class="<?php echo $li_class; ?>">
					<a href="javascript:;" data-tab="#newsider-mb-tab-<?php echo $newsider_count; ?>"><?php echo $tab_title; ?></a>
				</li>
			<?php } ?>
		</ul>

		<?php
		// Output tab sections
		$newsider_count = '';
		foreach ( $active_tabs as $tab ) {
			$newsider_count++; ?>
			<div id="newsider-mb-tab-<?php echo $newsider_count; ?>" class="wp-tab-panel clr">
				<table class="form-table">
					<?php
					// Loop through sections and store meta output
					foreach ( $tab['settings'] as $setting ) {

						// Vars
						$meta_id     = $setting['id'];
						$title       = $setting['title'];
						$hidden      = isset ( $setting['hidden'] ) ? $setting['hidden'] : false;
						$type        = isset ( $setting['type'] ) ? $setting['type'] : 'text';
						$default     = isset ( $setting['default'] ) ? $setting['default'] : '';
						$description = isset ( $setting['description'] ) ? $setting['description'] : '';
						$meta_value  = get_post_meta( $post_id, $meta_id, true );
						$meta_value  = $meta_value ? $meta_value : $default; ?>

						<tr<?php if ( $hidden ) echo ' style="display:none;"'; ?> id="<?php echo $meta_id; ?>_tr">
							<th>
								<label for="newsider_main_layout"><strong><?php echo $title; ?></strong></label>
								<?php
								// Display field description
								if ( $description ) { ?>
									<p class="newsider-mb-description"><?php echo $description; ?></p>
								<?php } ?>
							</th>

							<?php
							// Text Field
							if ( 'text' == $type ) { ?>

								<td><input name="<?php echo $meta_id; ?>" type="text" value="<?php echo $meta_value; ?>"></td>

							<?php }

							// Number Field
							if ( 'number' == $type ) { ?>

								<td><input name="<?php echo $meta_id; ?>" type="number" value="<?php echo $meta_value; ?>"></td>

							<?php }

							// HTML Text
							if ( 'text_html' == $type ) { ?>

								<td><input name="<?php echo $meta_id; ?>" type="text" value="<?php echo esc_html( $meta_value ); ?>"></td>

							<?php }

							// Link field
							elseif ( 'link' == $type ) { ?>

								<td><input name="<?php echo $meta_id; ?>" type="text" value="<?php echo esc_url( $meta_value ); ?>"></td>

							<?php }

							// Textarea Field
							elseif ( 'textarea' == $type ) {
								$rows = isset ( $setting['rows'] ) ? $setting['rows'] : '4';?>

								<td>
									<textarea rows="<?php echo $rows; ?>" cols="1" name="<?php echo $meta_id; ?>" type="text" class="newsider-mb-textarea"><?php echo $meta_value; ?></textarea>
								</td>

							<?php }

							// Code Field
							elseif ( 'code' == $type ) { ?>

								<td>
									<textarea rows="1" cols="1" name="<?php echo $meta_id; ?>" type="text" class="newsider-mb-textarea-code"><?php echo $meta_value; ?></textarea>
								</td>

							<?php }

							// Checkbox
							elseif ( 'checkbox' == $type ) {
								
								$meta_value = ( 'on' == $meta_value ) ? false : true; ?>
								<td><input name="<?php echo $meta_id; ?>" type="checkbox" <?php checked( $meta_value, true, true ); ?>></td>

							<?php }

							// Select
							elseif ( 'select' == $type ) {

								$options = isset ( $setting['options'] ) ? $setting['options'] : '';
								if ( ! empty( $options ) ) { ?>
									<td><select id="<?php echo $meta_id; ?>" name="<?php echo $meta_id; ?>">
									<?php foreach ( $options as $option_value => $option_name ) { ?>
										<option value="<?php echo $option_value; ?>" <?php selected( $meta_value, $option_value, true ); ?>><?php echo $option_name; ?></option>
									<?php } ?>
									</select></td>
								<?php }

							}

							// Color
							elseif ( 'color' == $type ) { ?>

								<td><input name="<?php echo $meta_id; ?>" type="text" value="<?php echo $meta_value; ?>" class="newsider-mb-color-field"></td>

							<?php }
							
							//	Layout
							elseif ( 'layout' == $type ) { ?>
					
								<td class="metabox_type_layout">
									<?php  foreach ($setting['options'] as $val => $option) {
										echo '<a href="#" data-value="' . $val . '" ' . ( $val == $meta_value ? ' class="active"' : '' ) . '><img src="'.esc_url($option['img']).'">'.esc_html($option['title']).'</a>';
									} ?>
									<input name="<?php echo $meta_id; ?>" type="hidden" value="<?php echo $meta_value; ?>" />
								</td>

							<?php }
							
							// Media
							elseif ( 'media' == $type ) {

								// Validate data if array - old Redux cleanup
								if ( is_array( $meta_value ) ) {
									if ( ! empty( $meta_value['url'] ) ) {
										$meta_value = $meta_value['url'];
									} else {
										$meta_value = '';
									}
								} ?>
								<td>
									<div class="uploader">
									<input type="text" name="<?php echo $meta_id; ?>" value="<?php echo $meta_value; ?>">
									<input class="newsider-mb-uploader button-secondary" name="<?php echo $meta_id; ?>" type="button" value="<?php esc_html_e( 'Upload', 'newsider' ); ?>" />
									<?php if ( $meta_value ) {
											if ( is_numeric( $meta_value ) ) {
												$meta_value = wp_get_attachment_image_src( $meta_value, 'full' );
												$meta_value = $meta_value[0];
											} ?>
										<div class="newsider-mb-thumb" style="padding-top:10px;"><img src="<?php echo $meta_value; ?>" height="40" width="" style="height:40px;width:auto;max-width:100%;" /></div>
									<?php } ?>
									</div>
								</td>

							<?php }
							
							//	Gallery
							elseif ( 'gallery' == $type ) {

								$gallery_thumbs = '';
								$button_text = ($meta_value) ? esc_html__('Edit Gallery', 'newsider') : esc_html__('Upload Images', 'newsider');
								
								// Validate data if array - old Redux cleanup
								if ( is_array( $meta_value ) ) {
									if ( ! empty( $meta_value['url'] ) ) {
										$meta_value = $meta_value['url'];
									} else {
										$meta_value = '';
									}
								}
								
								if( $meta_value ) {
									$thumbs = explode(',', $meta_value);
									foreach( $thumbs as $thumb ) {
										$gallery_thumbs .= '<li style="display:inline-block; vertica-align:top; margin-right:6px;">' . wp_get_attachment_image( $thumb, array(32,32) ) . '</li>';
									}
								} ?>
								
								<td>
									<input type="button" class="button" id="gallery_images_upload" value="<?php echo $button_text; ?>" />
									<input type="hidden" name="<?php echo $meta_id; ?>" id="gallery_image_ids" value="<?php echo $meta_value; ?>" />
									<ul class="gallery-thumbs" style="font-size:0;margin-top:6px;"><?php echo $gallery_thumbs;?></ul>
								</td>

							<?php }

							// Editor
							elseif ( 'editor' == $type ) {
								$teeny= isset( $setting['teeny'] ) ? $setting['teeny'] : false;
								$rows = isset( $setting['rows'] ) ? $setting['rows'] : '10';
								$media_buttons= isset( $setting['media_buttons'] ) ? $setting['media_buttons'] : true; ?>
								<td><?php wp_editor( $meta_value, $meta_id, array(
									'textarea_name' => $meta_id,
									'teeny' => $teeny,
									'textarea_rows' => $rows,
									'media_buttons' => $media_buttons,
								) ); ?></td>
							<?php } ?>
						</tr>

					<?php } ?>
				</table>
			</div>
		<?php } ?>

		<div class="newsider-mb-reset">
			<a class="button button-secondary newsider-reset-btn"><?php esc_html_e( 'Reset Settings', 'newsider' ); ?></a>
			<div class="newsider-reset-checkbox"><input type="checkbox" name="newsider_metabox_reset"> <?php esc_html_e( 'Are you sure? Check this box, then update your post to reset all settings.', 'newsider' ); ?></div>
		</div>

		<div class="clear"></div>

	<?php }

	/**
	 * Save metabox data
	 *
	 * @since 1.0.0
	 */
	public function save_meta_data( $post_id ) {

		/*
		 * We need to verify this came from our screen and with proper authorization,
		 * because the save_post action can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['newsider_metabox_nonce'] ) ) {
			return;
		}

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $_POST['newsider_metabox_nonce'], 'newsider_metabox' ) ) {
			return;
		}

		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		// Check the user's permissions.
		if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return;
			}

		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return;
			}
		}

		/* OK, it's safe for us to save the data now. Now we can loop through fields */

		// Check reset field
		$reset = isset( $_POST['newsider_metabox_reset'] ) ? $_POST['newsider_metabox_reset'] : '';

		// Set settings array
		$tabs = $this->tabs_array();
		$settings = array();
		foreach( $tabs as $tab ) {
			foreach ( $tab['settings'] as $setting ) {
				$settings[] = $setting;
			}
		}

		// Loop through settings and validate
		foreach ( $settings as $setting ) {

			// Vars
			$value = '';
			$id    = $setting['id'];
			$type  = isset ( $setting['type'] ) ? $setting['type'] : 'text';

			// Make sure field exists and if so validate the data
			if ( isset( $_POST[$id] ) ) {

				// Validate text
				if ( 'text' == $type ) {
					$value = sanitize_text_field( $_POST[$id] );
				}

				// Validate textarea
				if ( 'textarea' == $type ) {
					$value = esc_html( $_POST[$id] );
				}

				// Links
				elseif ( 'link' == $type ) {
					$value = esc_url( $_POST[$id] );
				}

				// Validate select
				elseif ( 'select' == $type ) {
					if ( 'default' == $_POST[$id] ) {
						$value = '';
					} else {
						$value = $_POST[$id];
					}
				}

				// Validate media
				if ( 'media' == $type ) {

					// Sanitize
					$value = $_POST[$id];

					// Move old newsider_post_self_hosted_shortcode_redux to newsider_post_self_hosted_media
					if ( 'newsider_post_self_hosted_media' == $id && empty( $_POST[$id] ) && $old = get_post_meta( $post_id, 'newsider_post_self_hosted_shortcode_redux', true ) ) {
						$value = $old;
						delete_post_meta( $post_id, 'newsider_post_self_hosted_shortcode_redux' );
					}

				}

				// All else
				else {
					$value = $_POST[$id];
				}

				// Update meta if value exists
				if ( $value && 'on' != $reset ) {
					update_post_meta( $post_id, $id, $value );
				}

				// Otherwise cleanup stuff
				else {
					delete_post_meta( $post_id, $id );
				}
			}

		}

	}

	/**
	 * Helpers
	 *
	 * @since 1.0.0
	 */
	public static function helpers( $return = NULl ) {


		// Return array of WP menus
		if ( 'menus' == $return ) {
			$menus = array( esc_html__( 'Default', 'newsider' ) );
			$get_menus = get_terms( 'nav_menu', array( 'hide_empty' => true ) );
			foreach ( $get_menus as $menu) {
				$menus[$menu->term_id] = $menu->name;
			}
			return $menus;
		}

		// Widgets
		elseif ( 'widget_areas' == $return ) {
			global $wp_registered_sidebars;
			$get_widget_areas = $wp_registered_sidebars;
			if ( ! empty( $get_widget_areas ) ) {
				foreach ( $get_widget_areas as $widget_area ) {
					$name = isset ( $widget_area['name'] ) ? $widget_area['name'] : '';
					$id = isset ( $widget_area['id'] ) ? $widget_area['id'] : '';
					if ( $name && $id ) {
						$widgets_areas[$id] = $name;
					}
				}
			}
			return $widgets_areas;
		}
		
		//	Portfolio Categories
		elseif ( 'portfolio_category_array' == $return ) {
			$portfolio_category = get_terms('portfolio_category');
			$portfolio_category_array[0] = esc_html__('All categories','newsider');
			if(isset($portfolio_category)) {
				foreach($portfolio_category as $portfolio_categories) {
					$portfolio_category_array[$portfolio_categories->term_id] = $portfolio_categories->name;
				}
			}
			return $portfolio_category_array;
		}
	}

	/**
	 * Settings Array
	 *
	 * @since 1.0.0
	 */
	public function tabs_array() {

		// Prefix
		$prefix = 'newsider_';
		
		$theme_uri = get_template_directory_uri();
		
		// Define variable
		$array = array();
		
		// Title Tab
		if( get_page_template_slug() != "page-comingsoon.php" ) {
			$array['pagetitle'] = array(
				'title' => esc_html__( 'Page Title', 'newsider' ),
				'post_type' => array( 'page' ),
				'settings' => array(
					'pagetitle' => array(
						'title' 			=> esc_html__( 'Title', 'newsider' ),
						'description' 		=> esc_html__( 'Enable or disable title on this page or post.', 'newsider' ),
						'id' 				=> $prefix . 'pagetitle',
						'type' 				=> 'select',
						'options' 			=> array(
													'' => esc_html__( 'Default', 'newsider' ),
													'show' => esc_html__( 'Show', 'newsider' ),
													'hide' => esc_html__( 'Hide', 'newsider' ),
												),
						'default' 			=> '',
					),
					'pagetitle_text' => array(
						'title' 			=> esc_html__( 'Page Title', 'newsider' ),
						'description' 		=> esc_html__( 'Please enter the page title.', 'newsider' ),
						'type' 				=> 'text',
						'id' 				=> $prefix . 'pagetitle_text',
					),
					'pagetitle_height' => array(
						'title' 			=> esc_html__( 'Height', 'newsider' ),
						'description' 		=> esc_html__( 'Enter the height for the page title.', 'newsider' ),
						'type' 				=> 'text',
						'id' 				=> $prefix . 'pagetitle_height',
						'default' 			=> '',
					),
					'pagetitle_bg_color' => array(
						'title' 			=> esc_html__( 'Page Title Background Color', 'newsider' ),
						'description' 		=> esc_html__( 'Select a color.', 'newsider' ),
						'id' 				=> $prefix .'pagetitle_bg_color',
						'type' 				=> 'color',
						'default' 			=> '',
					),
					'pagetitle_bg_image' => array(
						'title' 			=> esc_html__( 'Page Title Background Image', 'newsider'),
						'description' 		=> esc_html__( 'Select a custom header image for your main title.', 'newsider' ),
						'id' 				=> $prefix . 'pagetitle_bg_image',
						'type' 				=> 'media',
					),
					'pagetitle_bg_repeat' => array(
						'title' 			=> esc_html__( 'Page Title Background Repeat', 'newsider' ),
						'type' 				=> 'select',
						'id' 				=> $prefix . 'pagetitle_bg_repeat',
						'options' 			=> array(	
													'repeat'		=> esc_html__( 'Repeat', 'newsider' ),
													'repeat-x'		=> esc_html__( 'Repeat-x', 'newsider' ),
													'repeat-y'		=> esc_html__( 'Repeat-y', 'newsider' ),
													'no-repeat' 	=> esc_html__( 'No Repeat',  'newsider' )
												),
						'default' 			=> 'no-repeat',
					),
					'pagetitle_bg_attachment' => array(
						'title' 			=> esc_html__( 'Page Title Background Attachment', 'newsider' ),
						'type' 				=> 'select',
						'id' 				=> $prefix . 'pagetitle_bg_attachment',
						'options' 			=> array(	
													'scroll'	=> esc_html__( 'Scroll', 'newsider' ),
													'fixed'		=> esc_html__( 'Fixed', 'newsider' )
												),
						'default' 			=> 'scroll',
					),
					'pagetitle_bg_position' => array(
						'title' 			=> esc_html__( 'Page Title Background Position', 'newsider' ),
						'type' 				=> 'select',
						'id' 				=> $prefix . 'pagetitle_bg_position',
						'options' 			=> array(	
													'left top' 		=> esc_html__( 'Left Top', 'newsider' ),
													'left center' 	=> esc_html__( 'Left Center', 'newsider' ),
													'left bottom' 	=> esc_html__( 'Left Bottom', 'newsider' ),
													'center top' 	=> esc_html__( 'Center Top', 'newsider' ),
													'center center' => esc_html__( 'Center Center', 'newsider' ),
													'center bottom' => esc_html__( 'Center Bottom', 'newsider' ),
													'right top' 	=> esc_html__( 'Right Top', 'newsider' ),
													'right center' 	=> esc_html__( 'Right Center', 'newsider' ),
													'right bottom' 	=> esc_html__( 'Right Bottom', 'newsider' )
												),
						'default' 			=> 'center center',
					),
					'pagetitle_bg_full' => array(
						'title' 			=> esc_html__( 'Page Title Background Size', 'newsider' ),
						'type' 				=> 'select',
						'id' 				=> $prefix . 'pagetitle_bg_full',
						'options' 			=> array(	
													'inherit' 		=> esc_html__( 'Inherit', 'newsider' ),
													'cover' 		=> esc_html__( 'Cover', 'newsider' )
												),
						'default' 			=> 'cover',
					),
					'pagetitle_bg_image_parallax' => array(
						'title' 			=> esc_html__( 'Parallax Effect', 'newsider' ),
						'description' 		=> esc_html__( 'Enable this to the parallax effect for background image.', 'newsider' ),
						'type' 				=> 'select',
						'id' 				=> $prefix . 'pagetitle_bg_image_parallax',
						'options' 			=> array(	
													''		 		=> esc_html__( 'Default', 'newsider' ),
													'disable' 		=> esc_html__( 'Disable', 'newsider' ),
													'enable' 		=> esc_html__( 'Enable', 'newsider' )
												),
						'default' 			=> '',
					),
					'pagetitle_text_color' => array(
						'title' 			=> esc_html__( 'Page Title Text Color', 'newsider' ),
						'description' 		=> esc_html__( 'Select a text color.', 'newsider' ),
						'id' 				=> $prefix .'pagetitle_text_color',
						'type' 				=> 'color',
						'default' 			=> '',
					),
					
					'breadcrumbs' => array(
						'title' => esc_html__( 'Breadcrumbs', 'newsider' ),
						'description' => esc_html__( 'Enable or disable breadcrumbs on this page or post.', 'newsider' ),
						'id' => $prefix . 'breadcrumbs',
						'type' => 'select',
						'options' => array(
							'' => esc_html__( 'Default', 'newsider' ),
							'show' => esc_html__( 'Show', 'newsider' ),
							'hide' => esc_html__( 'Hide', 'newsider' ),
						),
						'default' => '',
					),
				),
			);
		
		
			// Footer tab
			$array['footer'] = array(
				'title' => esc_html__( 'Footer', 'newsider' ),
				'post_type' => array( 'page' ),
				'settings' => array(
					'footer_instagram' => array(
						'title' => esc_html__( 'Instagram', 'newsider' ),
						'description' => esc_html__( 'Show or hide Instagram', 'newsider' ),
						'id' => $prefix . 'footer_instagram',
						'type' => 'select',
						'options' => array(
							'show' => esc_html__( 'Show', 'newsider' ),
							'hide' => esc_html__( 'Hide', 'newsider' ),
						),
						'default' => 'show',
					),
				),
			);
			
		}
		

		// Post tab
		$newsider_blogsingle_layout = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options('blogsingle_layout') : 'right-sidebar';
		
		$array['media'] = array(
			'title' 		=> esc_html__( 'Post', 'newsider' ),
			'post_type' 	=> array( 'post' ),
			'settings' 		=> array(
				'post_metro' => array(
					'title' => esc_html__( 'Masonry Item Sizing', 'newsider' ),
					'description' => esc_html__( 'This will only be used if you choose to display your Blog Posts in the "Metro Style" in element settings', 'newsider' ),
					'id' => $prefix . 'post_metro',
					'type' => 'select',
					'options' => array(
						'' => esc_html__( 'Default', 'newsider' ),
						'width2' => esc_html__( 'Double Width', 'newsider' ),
						'height2' => esc_html__( 'Double Height', 'newsider' ),
						'wh2' => esc_html__( 'Double Width and Height', 'newsider' ),
					),
					'default' => '',
				),
				
				//	Single Post Layout
				'blogsingle_layout' => array(
					'title' 			=> esc_html__( 'Single Post Sidebar', 'newsider' ),
					'description' 		=> esc_html__( 'Select the position of the sidebar for this post', 'newsider' ),
					'id' 				=> $prefix . 'blogsingle_layout',
					'type' 				=> 'layout',
					'options' 			=> array(
						'right-sidebar' => array(
							'title' => esc_html__('Right Sidebar', 'newsider'),
							'img' => $theme_uri . '/framework/assets/images/single_rightsidebar.jpg'
						),
						'left-sidebar' => array(
							'title' => esc_html__('Left Sidebar', 'newsider'),
							'img' => $theme_uri . '/framework/assets/images/single_leftsidebar.jpg'
						),
						'no-sidebar' => array(
							'title' => esc_html__('Fullwidth Content', 'newsider'),
							'img' => $theme_uri . '/framework/assets/images/single_nosidebar.jpg'
						),
					),
					'default' => $newsider_blogsingle_layout,
				),
				'blogsingle_featured_img' => array(
					'title' 			=> esc_html__( 'Single Post Layout', 'newsider' ),
					'description' 		=> esc_html__( 'Select the layout of the elements or styles for a single post page', 'newsider' ),
					'id' 				=> $prefix . 'blogsingle_featured_img',
					'type' => 'layout',
					'options' => array(
						'featured_img_side' => array(
							'title' => esc_html__('Featured area small', 'newsider'),
							'img' => $theme_uri . '/framework/assets/images/single_media_small.jpg'
						),
						'featured_img_fullwidth' => array(
							'title' => esc_html__('Featured area large', 'newsider'),
							'img' => $theme_uri . '/framework/assets/images/single_media_large.jpg'
						),
						'featured_img_wide' => array(
							'title' => esc_html__('Featured area full', 'newsider'),
							'img' => $theme_uri . '/framework/assets/images/single_media_wide.jpg'
						),
					),
					'default' => 'featured_img_side',
				),
				
				'post_quote_text' => array(
					'title' 			=> esc_html__( 'Post Format Quote', 'newsider' ),
					'description' 		=> esc_html__( 'Write your quote in this field. Will show only for Quote Post Format.', 'newsider' ),
					'id' 				=> $prefix . 'post_quote_text',
					'type' 				=> 'textarea',
					'rows' 				=> '2',
				),
				'post_quote_author' => array(
					'title' 			=> esc_html__( 'Post Format Quote Author', 'newsider' ),
					'description' 		=> esc_html__( 'Write your quote author in this field. Will show only for Quote Post Format.', 'newsider' ),
					'id' 				=> $prefix . 'post_quote_author',
					'type' 				=> 'text',
				),
				'post_link_url' => array(
					'title' 			=> esc_html__( 'Post Format Link', 'newsider' ),
					'description' 		=> esc_html__( 'Write your link in this field. Will show only for Link Post Format.', 'newsider' ),
					'id' 				=> $prefix . 'post_link_url',
					'type' 				=> 'text',
				),
				'post_gallery' => array(
					'title' 			=> esc_html__( 'Post Format Gallery', 'newsider' ),
					'description' 		=> esc_html__( 'Select the images that should be upload to this gallery. Will show only for Gallery Post Format.', 'newsider' ),
					'id' 				=> 'gallery_image_ids',
					'type' 				=> 'gallery',
				),
				'blogsingle_gallery_style' => array(
					'title' 			=> esc_html__( 'Masonry Item Sizing', 'newsider' ),
					'description' 		=> esc_html__( 'This will only be used if you choose to display your Blog Posts in the "Metro Style" in element settings', 'newsider' ),
					'id' 				=> $prefix . 'blogsingle_gallery_style',
					'type' 				=> 'select',
					'options' 			=> array(
						'slider' 		=> esc_html__( 'Slider', 'newsider' ),
						'grid' 			=> esc_html__( 'Grid', 'newsider' ),
						'justified' 	=> esc_html__( 'Justified', 'newsider' ),
					),
					'default' 			=> '',
				),
				'post_embed' => array(
					'title' 			=> esc_html__( 'Post Format Video, Audio, Maps Embed Code', 'newsider' ),
					'description' 		=> esc_html__( 'Insert Youtube, Vimeo, SoundCloud, Map embed code. Will show only for Video Post Format.', 'newsider' ),
					'id' 				=> $prefix . 'post_embed',
					'type' 				=> 'textarea',
					'rows' 				=> '2',
				),
				
				//	Twitter post format
				'post_twitter_username' => array(
					'title' 			=> esc_html__( 'Post Format Twitter Username', 'newsider' ),
					'description' 		=> esc_html__( 'Only if you select Twitter post format', 'newsider' ),
					'id' 				=> $prefix . 'post_twitter_username',
					'type' 				=> 'text',
				),
				'post_twitter_consumer_key' => array(
					'title' 			=> esc_html__( 'Twitter Consumer key:', 'newsider' ),
					'id' 				=> $prefix . 'post_twitter_consumer_key',
					'type' 				=> 'text',
				),
				'post_twitter_consumer_secret' => array(
					'title' 			=> esc_html__( 'Twitter Consumer secret:', 'newsider' ),
					'id' 				=> $prefix . 'post_twitter_consumer_secret',
					'type' 				=> 'text',
				),
				'post_twitter_access_token' => array(
					'title' 			=> esc_html__( 'Twitter Access token:', 'newsider' ),
					'id' 				=> $prefix . 'post_twitter_access_token',
					'type' 				=> 'text',
				),
				'post_twitter_access_token_secret' => array(
					'title' 			=> esc_html__( 'Twitter Access token secret:', 'newsider' ),
					'id' 				=> $prefix . 'post_twitter_access_token_secret',
					'type' 				=> 'text',
				),
				'post_twitter_count' => array(
					'title' 			=> esc_html__( 'Twitter count:', 'newsider' ),
					'id' 				=> $prefix . 'post_twitter_count',
					'type' 				=> 'text',
				),
				
				//	Instagram post format
				'post_instagram_username' => array(
					'title' 			=> esc_html__( 'Post Format Instagram Username', 'newsider' ),
					'description' 		=> esc_html__( 'Only if you select Instagram post format', 'newsider' ),
					'id' 				=> $prefix . 'post_instagram_username',
					'type' 				=> 'text',
				),
			),
		);
		
		
		//	Page Coming Soon
		if (get_page_template_slug() == "page-comingsoon.php") {

			$newsider_coming_soon_years = array('2016'=>'2016','2017'=>'2017','2018'=>'2018','2019'=>'2019','2020'=>'2020');
			$newsider_coming_soon_months = array(
				'01' => '01', '02' => '02', '03' => '03', '04' => '04',
				'05' => '05', '06' => '06', '07' => '07', '08' => '08',
				'09' => '09', '10' => '10', '11' => '11', '12' => '12'
			);
			$newsider_coming_soon_days = array(
				'01' => '01','02' => '02','03' => '03','04' => '04','05' => '05',
				'06' => '06','07' => '07','08' => '08','09' => '09','10' => '10',
				'11' => '11','12' => '12','13' => '13','14' => '14','15' => '15',
				'16' => '16','17' => '17','18' => '18','19' => '19','20' => '20',
				'21' => '21','22' => '22','23' => '23','24' => '24','25' => '25',
				'26' => '26','27' => '27','28' => '28','29' => '29','30' => '30','31' => '31',
			);
			
			$array['coming_soon'] = array(
				'title' => esc_html__( 'Coming Soon', 'newsider' ),
				'post_type' => array( 'page' ),
				'settings' => array(
					'coming_soon_years' => array(
						'title' 			=> esc_html__( 'Years', 'newsider' ),
						'description' 		=> '',
						'id' 				=> $prefix . 'coming_soon_years',
						'type' 				=> 'select',
						'options' 			=> $newsider_coming_soon_years,
						'default' 			=> '2020',
					),
					'coming_soon_months' => array(
						'title' 			=> esc_html__( 'Months', 'newsider' ),
						'description' 		=> '',
						'id' 				=> $prefix . 'coming_soon_months',
						'type' 				=> 'select',
						'options' 			=> $newsider_coming_soon_months,
						'default' 			=> '01',
					),
					'coming_soon_days' => array(
						'title' 			=> esc_html__( 'Days', 'newsider' ),
						'description' 		=> '',
						'id' 				=> $prefix . 'coming_soon_days',
						'type' 				=> 'select',
						'options' 			=> $newsider_coming_soon_days,
						'default' 			=> '01',
					),
					'coming_soon_title' => array(
						'title' 			=> esc_html__( 'Title', 'newsider' ),
						'description' 		=> '',
						'id' 				=> $prefix . 'coming_soon_title',
						'type' 				=> 'text',
						'default' 			=> 'Coming Soon',
					),
					'coming_soon_subtitle' => array(
						'title' 			=> esc_html__( 'Subtitle', 'newsider' ),
						'description' 		=> '',
						'id' 				=> $prefix . 'coming_soon_subtitle',
						'type' 				=> 'text',
						'default' 			=> 'The site is under construction',
					),
					'coming_soon_subscribe' => array(
						'title' 			=> esc_html__( 'Subscribe Form', 'newsider' ),
						'description' 		=> '',
						'id' 				=> $prefix . 'coming_soon_subscribe',
						'type' 				=> 'select',
						'options' 			=> array(
													'show' => esc_html__( 'Show', 'newsider' ),
													'hide' => esc_html__( 'Hide', 'newsider' ),
												),
						'default' 			=> '',
					),
					'coming_soon_social' => array(
						'title' 			=> esc_html__( 'Social Icons', 'newsider' ),
						'description' 		=> '',
						'id' 				=> $prefix . 'coming_soon_social',
						'type' 				=> 'select',
						'options' 			=> array(
													'show' => esc_html__( 'Show', 'newsider' ),
													'hide' => esc_html__( 'Hide', 'newsider' ),
												),
						'default' 			=> '',
					),
					'comingsoon_bg_color' => array(
						'title' 			=> esc_html__( 'Background Color', 'newsider' ),
						'description' 		=> '',
						'id' 				=> $prefix .'comingsoon_bg_color',
						'type' 				=> 'color',
						'default' 			=> '#4c4e50',
					),
					'comingsoon_bg_image' => array(
						'title' 			=> esc_html__( 'Background Image', 'newsider'),
						'description' 		=> '',
						'id' 				=> $prefix . 'comingsoon_bg_image',
						'type' 				=> 'media',
					),
				),
			);
		}


		// Apply filter & return settings array
		return apply_filters( 'newsider_metabox_array', $array );
	}

	/**
	 * Adds custom CSS for the metaboxes inline instead of loading another stylesheet
	 *
	 * @see assets/metabox.css
	 * @since 1.0.0
	 */
	public static function newsider_metaboxes_css() { ?>

		<style type="text/css">
			#newsider-metabox .wp-tab-panel{display:none;}#newsider-metabox .wp-tab-panel#newsider-mb-tab-1{display:block;}#newsider-metabox .wp-tab-panel{max-height:none !important;}#newsider-metabox ul.wp-tab-bar{-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;}#newsider-metabox ul.wp-tab-bar{padding-top:5px;}#newsider-metabox ul.wp-tab-bar:after{content:"";display:block;height:0;clear:both;visibility:hidden;zoom:1;}#newsider-metabox ul.wp-tab-bar li{padding:5px 12px;font-size:14px;}#newsider-metabox ul.wp-tab-bar li a:focus{box-shadow:none;}#newsider-metabox .inside .form-table tr{border-top:1px solid #dfdfdf;}#newsider-metabox .inside .form-table tr:first-child{border:none;}#newsider-metabox .inside .form-table th{width:240px;padding:10px 30px 10px 0;}#newsider-metabox .inside .form-table td{padding:10px 0;}#newsider-metabox .inside .form-table label{display:block;}#newsider-metabox .inside .form-table th label span{margin-right:7px;}#newsider-metabox .newsider-mb-uploader{margin-left:5px;}#newsider-metabox .inside .form-table th p.newsider-mb-description{font-size:12px;font-weight:normal;margin:0;padding:0;padding-top:4px;}#newsider-metabox .inside .form-table input[type="text"],#newsider-metabox .inside .form-table input[type="number"],#newsider-metabox .inside .form-table .newsider-mb-textarea-code{width:40%;}#newsider-metabox .inside .form-table textarea{width:100%}#newsider-metabox .inside .form-table select{min-width:40%;}#newsider-metabox .newsider-mb-reset{margin-top:7px;}#newsider-metabox .newsider-mb-reset .newsider-reset-btn{display:block;float:left;}#newsider-metabox .newsider-mb-reset .newsider-reset-checkbox{float:left;display:none;margin-left:10px;padding-top:5px;}.metabox_type_layout{padding-bottom:40px!important;}.metabox_type_layout a{position:relative;display:inline-block;vertical-align:top;height:140px;width:140px;margin:0 10px 10px 0;text-decoration:none;color:inherit;}.metabox_type_layout a:hover,.metabox_type_layout a:focus{color:#7b52ab;}.metabox_type_layout a img{margin-bottom:5px;max-width:100%;}.metabox_type_layout a:after{content:"";position:absolute;font-family:dashicons;top:0;left:0;width:28px;height:28px;text-align:center;line-height:30px;font-size:20px;color:#fff;}.metabox_type_layout a:before{content:'';position:absolute;left:0;top:0;width:100%;height:100%;}.metabox_type_layout a.active:before{border:3px solid #7b52ab;background-color:rgba(123,82,171,0.15);}.metabox_type_layout a.active:after{content:"\f147";background-color:#7b52ab;}.metabox_type_layout:before,.metabox_type_layout:after{display:table;content:"";line-height:0;}.metabox_type_layout:after{clear:both;}
		</style>

	<?php

	}

	/**
	 * Adds custom js for the metaboxes inline instead of loading another js file
	 *
	 * @see assets/metabox.js
	 * @since 1.0.0
	 */
	public static function newsider_metaboxes_js() { ?>

		<script type="text/javascript">
			!function(e){"use strict";e(document).on("ready",function(){e("div#newsider-metabox ul.wp-tab-bar a").click(function(){var t=e("#newsider-metabox ul.wp-tab-bar li"),a=e(this).data("tab"),o=e("#newsider-metabox div.wp-tab-panel");return e(t).removeClass("wp-tab-active"),e(o).hide(),e(a).show(),e(this).parent("li").addClass("wp-tab-active"),!1}),e("div#newsider-metabox .newsider-mb-color-field").wpColorPicker();var t=!0,a=wp.media.editor.send.attachment;e("div#newsider-metabox .newsider-mb-uploader").click(function(){var o=(wp.media.editor.send.attachment,e(this)),i=o.prev();return wp.media.editor.send.attachment=function(o,r){return t?void e(i).val(r.id):a.apply(this,[o,r])},wp.media.editor.open(o),!1}),e("div#newsider-metabox .add_media").on("click",function(){t=!1}),e("div#newsider-metabox div.newsider-mb-reset a.newsider-reset-btn").click(function(){var t=e("div.newsider-mb-reset div.newsider-reset-checkbox"),a=t.is(":visible")?"<?php echo esc_html__( 'Reset Settings', 'newsider' ); ?>":"<?php echo esc_html__(  'Cancel Reset', 'newsider' ); ?>";e(this).text(a),e("div.newsider-mb-reset div.newsider-reset-checkbox input").attr("checked",!1),t.toggle()});})}(jQuery);
			
			/* Gallery */
			jQuery(function(){
				var frame;
				var images = script_data.image_ids;
				var selection = loadImages(images);

				jQuery('#gallery_images_upload').on('click', function(e) {
					e.preventDefault();

					// Set options for 1st frame render
					var options = {
						title: script_data.label_create,
						state: 'gallery-edit',
						frame: 'post',
						selection: selection
					};

					// Check if frame or gallery already exist
					if( frame || selection ) {
						options['title'] = script_data.label_edit;
					}

					frame = wp.media(options).open();

					// Tweak views
					frame.menu.get('view').unset('cancel');
					frame.menu.get('view').unset('separateCancel');
					frame.menu.get('view').get('gallery-edit').el.innerHTML = script_data.label_edit;
					frame.content.get('view').sidebar.unset('gallery'); // Hide Gallery Settings in sidebar

					// When we are editing a gallery
					overrideGalleryInsert();
					frame.on( 'toolbar:render:gallery-edit', function() {
						overrideGalleryInsert();
					});

					frame.on( 'content:render:browse', function( browser ) {
						if ( !browser ) return;
						// Hide Gallery Setting in sidebar
						browser.sidebar.on('ready', function(){
							browser.sidebar.unset('gallery');
						});
						// Hide filter/search as they don't work 
						browser.toolbar.on('ready', function(){ 
							if(browser.toolbar.controller._state == 'gallery-library'){ 
								browser.toolbar.$el.hide(); 
							} 
						}); 
					});

					// All images removed
					frame.state().get('library').on( 'remove', function() {
						var models = frame.state().get('library');
						if(models.length == 0){
							selection = false;
							jQuery.post(ajaxurl, { 
								ids: '',
								action: 'newsider_save_images',
								post_id: script_data.post_id,
								nonce: script_data.nonce 
							});
						}
					});

					// Override insert button
					function overrideGalleryInsert() {
						frame.toolbar.get('view').set({
							insert: {
								style: 'primary',
								text: script_data.label_save,

								click: function() {                                            
									var models = frame.state().get('library'),
										ids = '';

									models.each( function( attachment ) {
										ids += attachment.id + ','
									});

									this.el.innerHTML = script_data.label_saving;
									
									jQuery.ajax({
										type: 'POST',
										url: ajaxurl,
										data: { 
											ids: ids, 
											action: 'newsider_save_images', 
											post_id: script_data.post_id, 
											nonce: script_data.nonce 
										},
										success: function(){
											selection = loadImages(ids);
											jQuery('#gallery_image_ids').val( ids );
											frame.close();
										},
										dataType: 'html'
									}).done( function( data ) {
										jQuery('.gallery-thumbs').html( data );
									}); 
								}
							}
						});
					}
				});

				// Load images
				function loadImages(images) {
					if( images ){
						var shortcode = new wp.shortcode({
							tag:    'gallery',
							attrs:   { ids: images },
							type:   'single'
						});

						var attachments = wp.media.gallery.attachments( shortcode );

						var selection = new wp.media.model.Selection( attachments.models, {
							props:    attachments.props.toJSON(),
							multiple: true
						});

						selection.gallery = attachments.gallery;

						// Fetch the query's attachments, and then break ties from the
						// query to allow for sorting.
						selection.more().done( function() {
							// Break ties with the query.
							selection.props.set({ query: false });
							selection.unmirror();
							selection.props.unset('orderby');
						});

						return selection;
					}
					return false;
				}
				
				/* Type Layout */
				if (jQuery('.metabox_type_layout').size() > 0) {
					jQuery("body").on( "click",".metabox_type_layout>a",function(e){e.preventDefault();
						var $c=$(this);
						$c.addClass('active').siblings('.active').removeClass('active');
						$c.siblings('input').val($c.data('value')).trigger('change');
					});
				}
				
			});
			
		</script>

	<?php }

}
$newsider_post_metaboxes = new newsider_Post_Metaboxes();