<?php
/**
 * The template for displaying search results pages.
 */

get_header(); ?>

		<div id="search-result-list">
			<div class="container">
				<div class="row">

					<?php

						if (have_posts ()) {
							
							while (have_posts ()) : the_post();
								
								$newsider_post_excerpt = (newsider_smarty_modifier_truncate(get_the_excerpt(), 120));
							?>

								<article id="post-<?php the_ID(); ?>" <?php post_class( 'col-md-12' ); ?>>
									<div class="post-content-wrapper clearfix">
										<div class="post-descr-wrap text-center clearfix">
											<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('<!--:en-->Permalink to<!--:--><!--:es-->Enlace para<!--:--> %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
											<div class="post-meta-date"><?php the_time('j F Y') ?></div>
											<div class="post-content clearfix">
												<p><?php echo $newsider_post_excerpt; ?></p>
											</div>
										</div>
									</div>
								</article>

							<?php endwhile; ?>
							
							<?php wp_reset_postdata(); ?>
							
						<?php

						} else {

						?>
							<div id="error404-container">
								<h4 class="error404"><?php esc_html_e('<!--:en-->Sorry, but nothing matched your search criteria. Please try again with some different keywords.<!--:--><!--:es-->Lo siento, pero nada coincidía con tus criterios de búsqueda. Por favor, inténtelo de nuevo con algunas palabras clave diferentes.<!--:-->', 'newsider');?></h4>
							</div>
							
						<?php }
					
					?>
	
				</div>
				
				<?php newsider_pagination(); ?>
				
			</div>
		</div>

<?php get_footer(); ?>