# WordPress Blank Pot
# Copyright (C) 2014 ...
# This file is distributed under the GNU General Public License v2 or later.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: "
"WordPress Blank Pot "
"v1.0.0\n"
"POT-Creation-Date: "
"2017-03-08 02:09+0200\n"
"PO-Revision-Date: \n"
"Last-Translator: Your "
"Name <you@example.com>\n"
"Language-Team: Your Team "
"<translations@example."
"com>\n"
"Report-Msgid-Bugs-To: "
"Translator Name "
"<translations@example."
"com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/"
"plain; charset=UTF-8\n"
"Content-Transfer-"
"Encoding: 8bit\n"
"Plural-Forms: "
"nplurals=2; plural=n != "
"1;\n"
"X-Textdomain-Support: "
"yesX-Generator: Poedit "
"1.6.4\n"
"X-Poedit-SourceCharset: "
"UTF-8\n"
"X-Poedit-KeywordsList: "
"__;_e;esc_html_e;"
"esc_html_x:1,2c;"
"esc_html__;esc_attr_e;"
"esc_attr_x:1,2c;"
"esc_attr__;_ex:1,2c;"
"_nx:4c,1,2;"
"_nx_noop:4c,1,2;_x:1,2c;"
"_n:1,2;_n_noop:1,2;"
"__ngettext:1,2;"
"__ngettext_noop:1,2;_c,"
"_nc:4c,1,2\n"
"X-Poedit-Basepath: ..\n"
"Language: en_US\n"
"X-Generator: Poedit "
"1.8.12\n"
"X-Poedit-"
"SearchPath-0: .\n"

#: 404.php:10
msgid "404"
msgstr ""

#: 404.php:11
msgid ""
"Oops...Page not found!"
msgstr ""

#: 404.php:13
msgid "back to home page"
msgstr ""

#: comments.php:9
msgid ""
"This post is password "
"protected. Enter the "
"password to view "
"comments."
msgstr ""

#: comments.php:19
msgid "Comments"
msgstr ""

#: comments.php:52
msgid ""
"Comments are closed."
msgstr ""

#: comments.php:67
msgid "Name *"
msgstr ""

#: comments.php:68
msgid "E-mail *"
msgstr ""

#: comments.php:74
msgid "Leave A Comment"
msgstr ""

#: comments.php:75
msgid "Comment *"
msgstr ""

#: comments.php:77
msgid "Post comment"
msgstr ""

#: footer.php:54
msgid ""
"Copyright © 2017 "
"Avantura Theme"
msgstr ""

#: framework/breadcrumbs.php:12
msgid "Home"
msgstr ""

#: framework/breadcrumbs.php:13
#, php-format
msgid ""
"Archive by Category \"%s"
"\""
msgstr ""

#: framework/breadcrumbs.php:14
#, php-format
msgid ""
"Search Results for \"%s"
"\" Query"
msgstr ""

#: framework/breadcrumbs.php:15
#, php-format
msgid "Posts Tagged \"%s\""
msgstr ""

#: framework/breadcrumbs.php:16
#, php-format
msgid ""
"Articles Posted by %s"
msgstr ""

#: framework/breadcrumbs.php:17
#: templates/page_title.php:42
msgid "Error 404"
msgstr ""

#: framework/breadcrumbs.php:18
#, php-format
msgid "Page %s"
msgstr ""

#: framework/breadcrumbs.php:19
#, php-format
msgid "Comment Page %s"
msgstr ""

#: framework/category-color/category_color.php:24
#: framework/category-color/category_color.php:37
#: framework/category-color/category_color.php:60
msgid "Color"
msgstr ""

#: framework/megamenu.php:171
#, php-format
msgid "%s (Invalid)"
msgstr ""

#: framework/megamenu.php:175
#, php-format
msgid "%s (Pending)"
msgstr ""

#: framework/megamenu.php:188
msgid "sub item"
msgstr ""

#: framework/megamenu.php:203
msgid "Move up"
msgstr ""

#: framework/megamenu.php:216
msgid "Move down"
msgstr ""

#: framework/megamenu.php:218
#: framework/megamenu.php:220
msgid "Edit Menu Item"
msgstr ""

#: framework/megamenu.php:229
msgid "URL"
msgstr ""

#: framework/megamenu.php:236
msgid "Navigation Label"
msgstr ""

#: framework/megamenu.php:242
msgid "Title Attribute"
msgstr ""

#: framework/megamenu.php:248
msgid "Mega Menu"
msgstr ""

#: framework/megamenu.php:254
msgid ""
"Make this item Mega Menu?"
msgstr ""

#: framework/megamenu.php:261
msgid ""
"Open link in a new "
"window/tab"
msgstr ""

#: framework/megamenu.php:266
msgid ""
"CSS Classes (optional)"
msgstr ""

#: framework/megamenu.php:272
msgid ""
"Link Relationship (XFN)"
msgstr ""

#: framework/megamenu.php:278
msgid "Description"
msgstr ""

#: framework/megamenu.php:280
msgid ""
"The description will be "
"displayed in the menu if "
"the current theme "
"supports it."
msgstr ""

#: framework/megamenu.php:286
msgid "Move"
msgstr ""

#: framework/megamenu.php:287
msgid "Up one"
msgstr ""

#: framework/megamenu.php:288
msgid "Down one"
msgstr ""

#: framework/megamenu.php:291
msgid "To the top"
msgstr ""

#: framework/megamenu.php:298
#, php-format
msgid "Original: %s"
msgstr ""

#: framework/megamenu.php:311
msgid "Remove"
msgstr ""

#: framework/megamenu.php:312
msgid "Cancel"
msgstr ""

#: framework/megamenu.php:525
msgid "Add a menu"
msgstr ""

#: framework/metabox/metabox.php:22
msgid ""
"Create Featured Gallery"
msgstr ""

#: framework/metabox/metabox.php:23
msgid ""
"Edit Featured Gallery"
msgstr ""

#: framework/metabox/metabox.php:24
msgid ""
"Save Featured Gallery"
msgstr ""

#: framework/metabox/metabox.php:25
msgid "Saving..."
msgstr ""

#: framework/metabox/metabox.php:80
msgid "Settings"
msgstr ""

#: framework/metabox/metabox.php:119
msgid ""
"Hey your settings are "
"empty, something is "
"going on please contact "
"your webmaster"
msgstr ""

#: framework/metabox/metabox.php:148
msgid "Other"
msgstr ""

#: framework/metabox/metabox.php:277
msgid "Upload"
msgstr ""

#: framework/metabox/metabox.php:294
msgid "Edit Gallery"
msgstr ""

#: framework/metabox/metabox.php:294
msgid "Upload Images"
msgstr ""

#: framework/metabox/metabox.php:340
#: framework/metabox/metabox.php:983
msgid "Reset Settings"
msgstr ""

#: framework/metabox/metabox.php:341
msgid ""
"Are you sure? Check this "
"box, then update your "
"post to reset all "
"settings."
msgstr ""

#: framework/metabox/metabox.php:482
#: framework/metabox/metabox.php:544
#: framework/metabox/metabox.php:631
#: framework/metabox/metabox.php:651
#: framework/metabox/metabox.php:693
#: framework/metabox/metabox.php:764
msgid "Default"
msgstr ""

#: framework/metabox/metabox.php:509
msgid "All categories"
msgstr ""

#: framework/metabox/metabox.php:535
#: framework/metabox/metabox.php:551
msgid "Page Title"
msgstr ""

#: framework/metabox/metabox.php:539
#: framework/metabox/metabox.php:901
#: framework/widgets/widget-flickr.php:61
#: framework/widgets/widget-instagram.php:62
#: framework/widgets/widget-posts.php:226
msgid "Title"
msgstr ""

#: framework/metabox/metabox.php:540
msgid ""
"Enable or disable title "
"on this page or post."
msgstr ""

#: framework/metabox/metabox.php:545
#: framework/metabox/metabox.php:652
#: framework/metabox/metabox.php:673
#: framework/metabox/metabox.php:920
#: framework/metabox/metabox.php:931
msgid "Show"
msgstr ""

#: framework/metabox/metabox.php:546
#: framework/metabox/metabox.php:653
#: framework/metabox/metabox.php:674
#: framework/metabox/metabox.php:921
#: framework/metabox/metabox.php:932
msgid "Hide"
msgstr ""

#: framework/metabox/metabox.php:552
msgid ""
"Please enter the page "
"title."
msgstr ""

#: framework/metabox/metabox.php:557
msgid "Height"
msgstr ""

#: framework/metabox/metabox.php:558
msgid ""
"Enter the height for the "
"page title."
msgstr ""

#: framework/metabox/metabox.php:564
msgid ""
"Page Title Background "
"Color"
msgstr ""

#: framework/metabox/metabox.php:565
msgid "Select a color."
msgstr ""

#: framework/metabox/metabox.php:571
msgid ""
"Page Title Background "
"Image"
msgstr ""

#: framework/metabox/metabox.php:572
msgid ""
"Select a custom header "
"image for your main "
"title."
msgstr ""

#: framework/metabox/metabox.php:577
msgid ""
"Page Title Background "
"Repeat"
msgstr ""

#: framework/metabox/metabox.php:581
msgid "Repeat"
msgstr ""

#: framework/metabox/metabox.php:582
msgid "Repeat-x"
msgstr ""

#: framework/metabox/metabox.php:583
msgid "Repeat-y"
msgstr ""

#: framework/metabox/metabox.php:584
msgid "No Repeat"
msgstr ""

#: framework/metabox/metabox.php:589
msgid ""
"Page Title Background "
"Attachment"
msgstr ""

#: framework/metabox/metabox.php:593
msgid "Scroll"
msgstr ""

#: framework/metabox/metabox.php:594
msgid "Fixed"
msgstr ""

#: framework/metabox/metabox.php:599
msgid ""
"Page Title Background "
"Position"
msgstr ""

#: framework/metabox/metabox.php:603
msgid "Left Top"
msgstr ""

#: framework/metabox/metabox.php:604
msgid "Left Center"
msgstr ""

#: framework/metabox/metabox.php:605
msgid "Left Bottom"
msgstr ""

#: framework/metabox/metabox.php:606
msgid "Center Top"
msgstr ""

#: framework/metabox/metabox.php:607
msgid "Center Center"
msgstr ""

#: framework/metabox/metabox.php:608
msgid "Center Bottom"
msgstr ""

#: framework/metabox/metabox.php:609
msgid "Right Top"
msgstr ""

#: framework/metabox/metabox.php:610
msgid "Right Center"
msgstr ""

#: framework/metabox/metabox.php:611
msgid "Right Bottom"
msgstr ""

#: framework/metabox/metabox.php:616
msgid ""
"Page Title Background "
"Size"
msgstr ""

#: framework/metabox/metabox.php:620
msgid "Inherit"
msgstr ""

#: framework/metabox/metabox.php:621
msgid "Cover"
msgstr ""

#: framework/metabox/metabox.php:626
msgid "Parallax Effect"
msgstr ""

#: framework/metabox/metabox.php:627
msgid ""
"Enable this to the "
"parallax effect for "
"background image."
msgstr ""

#: framework/metabox/metabox.php:632
msgid "Disable"
msgstr ""

#: framework/metabox/metabox.php:633
msgid "Enable"
msgstr ""

#: framework/metabox/metabox.php:638
msgid ""
"Page Title Text Color"
msgstr ""

#: framework/metabox/metabox.php:639
msgid ""
"Select a text color."
msgstr ""

#: framework/metabox/metabox.php:646
msgid "Breadcrumbs"
msgstr ""

#: framework/metabox/metabox.php:647
msgid ""
"Enable or disable "
"breadcrumbs on this page "
"or post."
msgstr ""

#: framework/metabox/metabox.php:664
msgid "Footer"
msgstr ""

#: framework/metabox/metabox.php:668
#: framework/theme_functions.php:543
#: framework/widgets/widget-instagram.php:56
msgid "Instagram"
msgstr ""

#: framework/metabox/metabox.php:669
msgid ""
"Show or hide Instagram"
msgstr ""

#: framework/metabox/metabox.php:684
msgid "Post"
msgstr ""

#: framework/metabox/metabox.php:688
#: framework/metabox/metabox.php:759
msgid "Masonry Item Sizing"
msgstr ""

#: framework/metabox/metabox.php:689
msgid ""
"This will only be used "
"if you choose to display "
"your Blog Posts in the "
"\"Metro Style\" in "
"element settings"
msgstr ""

#: framework/metabox/metabox.php:694
#: framework/metabox/metabox.php:765
msgid "Double Width"
msgstr ""

#: framework/metabox/metabox.php:695
#: framework/metabox/metabox.php:766
msgid "Double Height"
msgstr ""

#: framework/metabox/metabox.php:696
#: framework/metabox/metabox.php:767
msgid ""
"Double Width and Height"
msgstr ""

#: framework/metabox/metabox.php:701
msgid "Quote"
msgstr ""

#: framework/metabox/metabox.php:702
msgid ""
"Write your quote in this "
"field. Will show only "
"for Quote Post Format."
msgstr ""

#: framework/metabox/metabox.php:708
msgid "Quote Author"
msgstr ""

#: framework/metabox/metabox.php:709
msgid ""
"Write your quote author "
"in this field. Will show "
"only for Quote Post "
"Format."
msgstr ""

#: framework/metabox/metabox.php:714
#: framework/metabox/metabox.php:825
msgid "Link"
msgstr ""

#: framework/metabox/metabox.php:715
msgid ""
"Write your link in this "
"field. Will show only "
"for Link Post Format."
msgstr ""

#: framework/metabox/metabox.php:720
#: framework/metabox/metabox.php:844
msgid "Gallery"
msgstr ""

#: framework/metabox/metabox.php:721
#: framework/metabox/metabox.php:845
msgid ""
"Select the images that "
"should be upload to this "
"gallery. Will show only "
"for Gallery Post Format."
msgstr ""

#: framework/metabox/metabox.php:726
msgid "Video Embed Code"
msgstr ""

#: framework/metabox/metabox.php:727
msgid ""
"Insert Youtube or Vimeo "
"embed code. Videos will "
"show only for Video Post "
"Format."
msgstr ""

#: framework/metabox/metabox.php:733
msgid "Audio Embed Code"
msgstr ""

#: framework/metabox/metabox.php:734
msgid ""
"Insert audio embed code. "
"Audios will show only "
"for Audio Post Format."
msgstr ""

#: framework/metabox/metabox.php:752
msgid ""
"Select a background "
"color."
msgstr ""

#: framework/metabox/metabox.php:753
msgid ""
"Select the background "
"color for portfolio item "
"from the style \"BG "
"Image on hover\" in Page "
"Attributes -> Templates"
msgstr ""

#: framework/metabox/metabox.php:760
msgid ""
"This will only be used "
"if you choose to display "
"your Portfolio in the "
"\"Portfolio Metro\" in "
"Page Attributes -> "
"Templates"
msgstr ""

#: framework/metabox/metabox.php:772
msgid "Layout"
msgstr ""

#: framework/metabox/metabox.php:773
msgid ""
"Select page layout for "
"single portfolio"
msgstr ""

#: framework/metabox/metabox.php:777
msgid ""
"Full screen (Featured "
"Image or Slider on the "
"full screen)"
msgstr ""

#: framework/metabox/metabox.php:778
msgid ""
"Half width (Description "
"right, featured images "
"or Slider left)"
msgstr ""

#: framework/metabox/metabox.php:783
#: single-portfolio.php:79
msgid "Client"
msgstr ""

#: framework/metabox/metabox.php:789
msgid ""
"Additional Field Title"
msgstr ""

#: framework/metabox/metabox.php:795
msgid "Additional Field"
msgstr ""

#: framework/metabox/metabox.php:801
msgid ""
"Additional Field Title 2"
msgstr ""

#: framework/metabox/metabox.php:807
msgid "Additional Field 2"
msgstr ""

#: framework/metabox/metabox.php:813
msgid ""
"Additional Field Title 3"
msgstr ""

#: framework/metabox/metabox.php:819
msgid "Additional Field 3"
msgstr ""

#: framework/metabox/metabox.php:831
msgid "Link Name"
msgstr ""

#: framework/metabox/metabox.php:837
msgid "Embed Code"
msgstr ""

#: framework/metabox/metabox.php:838
msgid ""
"Insert your embed/iframe "
"code."
msgstr ""

#: framework/metabox/metabox.php:873
msgid "Coming Soon"
msgstr ""

#: framework/metabox/metabox.php:877
msgid "Years"
msgstr ""

#: framework/metabox/metabox.php:885
msgid "Months"
msgstr ""

#: framework/metabox/metabox.php:893
#: page-comingsoon.php:51
msgid "Days"
msgstr ""

#: framework/metabox/metabox.php:908
msgid "Subtitle"
msgstr ""

#: framework/metabox/metabox.php:915
msgid "Subscribe Form"
msgstr ""

#: framework/metabox/metabox.php:926
msgid "Social Icons"
msgstr ""

#: framework/metabox/metabox.php:937
msgid "Background Color"
msgstr ""

#: framework/metabox/metabox.php:944
msgid "Background Image"
msgstr ""

#: framework/metabox/metabox.php:983
msgid "Cancel Reset"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:334
msgid ""
"Install Required Plugins"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:335
msgid "Install Plugins"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:337
#, php-format
msgid ""
"Installing Plugin: %s"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:339
#, php-format
msgid "Updating Plugin: %s"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:340
msgid ""
"Something went wrong "
"with the plugin API."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:343
#, php-format
msgid ""
"This theme requires the "
"following plugin: %1$s."
msgid_plural ""
"This theme requires the "
"following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:349
#, php-format
msgid ""
"This theme recommends "
"the following plugin: "
"%1$s."
msgid_plural ""
"This theme recommends "
"the following plugins: "
"%1$s."
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:355
#, php-format
msgid ""
"The following plugin "
"needs to be updated to "
"its latest version to "
"ensure maximum "
"compatibility with this "
"theme: %1$s."
msgid_plural ""
"The following plugins "
"need to be updated to "
"their latest version to "
"ensure maximum "
"compatibility with this "
"theme: %1$s."
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:361
#, php-format
msgid ""
"There is an update "
"available for: %1$s."
msgid_plural ""
"There are updates "
"available for the "
"following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:367
#, php-format
msgid ""
"The following required "
"plugin is currently "
"inactive: %1$s."
msgid_plural ""
"The following required "
"plugins are currently "
"inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:373
#, php-format
msgid ""
"The following "
"recommended plugin is "
"currently inactive: %1$s."
msgid_plural ""
"The following "
"recommended plugins are "
"currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:378
msgid ""
"Begin installing plugin"
msgid_plural ""
"Begin installing plugins"
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:383
msgid ""
"Begin updating plugin"
msgid_plural ""
"Begin updating plugins"
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:388
msgid ""
"Begin activating plugin"
msgid_plural ""
"Begin activating plugins"
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:392
msgid ""
"Return to Required "
"Plugins Installer"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:393
#: framework/plugins/class-tgm-plugin-activation.php:912
#: framework/plugins/class-tgm-plugin-activation.php:2618
#: framework/plugins/class-tgm-plugin-activation.php:3665
msgid ""
"Return to the Dashboard"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:394
#: framework/plugins/class-tgm-plugin-activation.php:3244
msgid ""
"Plugin activated "
"successfully."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:395
#: framework/plugins/class-tgm-plugin-activation.php:3037
msgid ""
"The following plugin was "
"activated successfully:"
msgid_plural ""
"The following plugins "
"were activated "
"successfully:"
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:397
#, php-format
msgid ""
"No action taken. Plugin "
"%1$s was already active."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:399
#, php-format
msgid ""
"Plugin not activated. A "
"higher version of %s is "
"needed for this theme. "
"Please update the plugin."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:401
#, php-format
msgid ""
"All plugins installed "
"and activated "
"successfully. %1$s"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:402
msgid "Dismiss this notice"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:403
msgid ""
"There are one or more "
"required or recommended "
"plugins to install, "
"update or activate."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:404
msgid ""
"Please contact the "
"administrator of this "
"site for help."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:607
msgid ""
"This plugin needs to be "
"updated to be compatible "
"with your theme."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:608
msgid "Update Required"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:1019
msgid ""
"The remote plugin "
"package does not contain "
"a folder with the "
"desired slug and "
"renaming did not work."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:1019
#: framework/plugins/class-tgm-plugin-activation.php:1022
msgid ""
"Please contact the "
"plugin provider and ask "
"them to package their "
"plugin according to the "
"WordPress guidelines."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:1022
msgid ""
"The remote plugin "
"package consists of more "
"than one file, but the "
"files are not packaged "
"in a folder."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:1206
#: framework/plugins/class-tgm-plugin-activation.php:3033
msgctxt ""
"plugin A *and* plugin B"
msgid "and"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2067
#, php-format
msgid "TGMPA v%s"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2358
msgid "Required"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2361
msgid "Recommended"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2377
msgid ""
"WordPress Repository"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2380
msgid "External Source"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2383
msgid "Pre-Packaged"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2400
msgid "Not Installed"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2404
msgid ""
"Installed But Not "
"Activated"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2406
msgid "Active"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2412
msgid ""
"Required Update not "
"Available"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2415
msgid "Requires Update"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2418
msgid "Update recommended"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2427
#, php-format
msgctxt ""
"Install/Update Status"
msgid "%1$s, %2$s"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2473
#, php-format
msgctxt "plugins"
msgid ""
"All <span class=\"count"
"\">(%s)</span>"
msgid_plural ""
"All <span class=\"count"
"\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:2477
#, php-format
msgid ""
"To Install <span class="
"\"count\">(%s)</span>"
msgid_plural ""
"To Install <span class="
"\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:2481
#, php-format
msgid ""
"Update Available <span "
"class=\"count\">(%s)</"
"span>"
msgid_plural ""
"Update Available <span "
"class=\"count\">(%s)</"
"span>"
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:2485
#, php-format
msgid ""
"To Activate <span class="
"\"count\">(%s)</span>"
msgid_plural ""
"To Activate <span class="
"\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: framework/plugins/class-tgm-plugin-activation.php:2567
msgctxt ""
"as in: \"version nr "
"unknown\""
msgid "unknown"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2575
msgid "Installed version:"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2583
msgid ""
"Minimum required version:"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2595
msgid "Available version:"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2618
msgid ""
"No plugins to install, "
"update or activate."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2632
msgid "Plugin"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2633
msgid "Source"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2634
msgid "Type"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2638
msgid "Version"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2639
msgid "Status"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2688
#, php-format
msgid "Install %2$s"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2693
#, php-format
msgid "Update %2$s"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2699
#, php-format
msgid "Activate %2$s"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2769
msgid ""
"Upgrade message from the "
"plugin author:"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2802
msgid "Install"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2808
msgid "Update"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2811
msgid "Activate"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2842
msgid ""
"No plugins were selected "
"to be installed. No "
"action taken."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2844
msgid ""
"No plugins were selected "
"to be updated. No action "
"taken."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2885
msgid ""
"No plugins are available "
"to be installed at this "
"time."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2887
msgid ""
"No plugins are available "
"to be updated at this "
"time."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:2993
msgid ""
"No plugins were selected "
"to be activated. No "
"action taken."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3019
msgid ""
"No plugins are available "
"to be activated at this "
"time."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3243
msgid ""
"Plugin activation failed."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3583
#, php-format
msgid ""
"Updating Plugin %1$s "
"(%2$d/%3$d)"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3586
#, php-format
msgid ""
"An error occurred while "
"installing %1$s: <strong>"
"%2$s</strong>."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3588
#, php-format
msgid ""
"The installation of %1$s "
"failed."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3592
msgid ""
"The installation and "
"activation process is "
"starting. This process "
"may take a while on some "
"hosts, so please be "
"patient."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3594
#, php-format
msgid ""
"%1$s installed and "
"activated successfully."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3594
#: framework/plugins/class-tgm-plugin-activation.php:3602
msgid "Show Details"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3594
#: framework/plugins/class-tgm-plugin-activation.php:3602
msgid "Hide Details"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3595
msgid ""
"All installations and "
"activations have been "
"completed."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3597
#, php-format
msgid ""
"Installing and "
"Activating Plugin %1$s "
"(%2$d/%3$d)"
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3600
msgid ""
"The installation process "
"is starting. This "
"process may take a while "
"on some hosts, so please "
"be patient."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3602
#, php-format
msgid ""
"%1$s installed "
"successfully."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3603
msgid ""
"All installations have "
"been completed."
msgstr ""

#: framework/plugins/class-tgm-plugin-activation.php:3605
#, php-format
msgid ""
"Installing Plugin %1$s "
"(%2$d/%3$d)"
msgstr ""

#: framework/plugins/install-plugin.php:63
msgid ""
"Evatheme Page Builder"
msgstr ""

#: framework/plugins/install-plugin.php:73
msgid ""
"Evatheme Redux Framework"
msgstr ""

#: framework/plugins/install-plugin.php:83
msgid "Contact Form 7"
msgstr ""

#: framework/plugins/install-plugin.php:92
msgid ""
"MailChimp for WordPress"
msgstr ""

#: framework/plugins/install-plugin.php:101
msgid ""
"AccessPress Social "
"Counter"
msgstr ""

#: framework/plugins/install-plugin.php:110
msgid "Instagram Feed"
msgstr ""

#: framework/theme_functions.php:244
msgid "Edit"
msgstr ""

#: framework/theme_functions.php:246
msgid "Reply"
msgstr ""

#: framework/theme_functions.php:252
msgid ""
"Your comment is awaiting "
"moderation."
msgstr ""

#: framework/theme_functions.php:291
#: framework/theme_functions.php:327
msgid "Previous"
msgstr ""

#: framework/theme_functions.php:292
#: framework/theme_functions.php:328
msgid "Next"
msgstr ""

#: framework/theme_functions.php:365
msgid "No more"
msgstr ""

#: framework/theme_functions.php:366
msgid "Loading"
msgstr ""

#: framework/theme_functions.php:367
msgid "Load More"
msgstr ""

#: framework/theme_functions.php:471
msgid "Previous post"
msgstr ""

#: framework/theme_functions.php:493
msgid "Next post"
msgstr ""

#: framework/theme_functions.php:540
#: framework/woo_functions.php:151
#: templates/blog/sharebox.php:18
#: templates/portfolio/sharebox.php:5
msgid "Facebook"
msgstr ""

#: framework/theme_functions.php:541
#: framework/woo_functions.php:153
#: templates/blog/sharebox.php:22
#: templates/portfolio/sharebox.php:6
msgid "Twitter"
msgstr ""

#: framework/theme_functions.php:542
msgid "Google Plus"
msgstr ""

#: framework/theme_functions.php:544
msgid "Linked In"
msgstr ""

#: framework/theme_functions.php:545
msgid "YouTube"
msgstr ""

#: framework/theme_functions.php:546
msgid "Vimeo"
msgstr ""

#: framework/theme_functions.php:547
msgid "Tumblr"
msgstr ""

#: framework/theme_functions.php:548
msgid "Skype"
msgstr ""

#: framework/theme_functions.php:549
msgid "Vkontakte"
msgstr ""

#: framework/theme_functions.php:550
msgid "Pinterest"
msgstr ""

#: framework/theme_functions.php:551
msgid "Wordpress"
msgstr ""

#: framework/theme_functions.php:552
msgid "Dropbox"
msgstr ""

#: framework/theme_functions.php:553
msgid "RSS"
msgstr ""

#: framework/widgets/widget-cat.php:10
msgid ""
"Evatheme Categories with "
"Thumbnails"
msgstr ""

#: framework/widgets/widget-cat.php:65
msgid "Posts"
msgstr ""

#: framework/widgets/widget-cat.php:95
#: framework/widgets/widget-posts.php:191
#: framework/widgets/widget-social.php:97
msgid "Title:"
msgstr ""

#: framework/widgets/widget-cat.php:99
msgid ""
"Number of categories to "
"display"
msgstr ""

#: framework/widgets/widget-flickr.php:10
msgid "Evatheme Flickr"
msgstr ""

#: framework/widgets/widget-flickr.php:66
msgid "Flickr ID"
msgstr ""

#: framework/widgets/widget-flickr.php:71
msgid "Number of images"
msgstr ""

#: framework/widgets/widget-instagram.php:17
msgid "Evatheme Instagram"
msgstr ""

#: framework/widgets/widget-instagram.php:17
msgid ""
"Displays your latest "
"Instagram photos"
msgstr ""

#: framework/widgets/widget-instagram.php:63
msgid "Username"
msgstr ""

#: framework/widgets/widget-instagram.php:64
msgid "Number of photos"
msgstr ""

#: framework/widgets/widget-instagram.php:65
msgid "Photo size"
msgstr ""

#: framework/widgets/widget-instagram.php:67
msgid "Thumbnail"
msgstr ""

#: framework/widgets/widget-instagram.php:68
msgid "Small"
msgstr ""

#: framework/widgets/widget-instagram.php:69
msgid "Large"
msgstr ""

#: framework/widgets/widget-instagram.php:70
msgid "Original"
msgstr ""

#: framework/widgets/widget-instagram.php:73
msgid "Open links in"
msgstr ""

#: framework/widgets/widget-instagram.php:75
msgid "New window (_blank)"
msgstr ""

#: framework/widgets/widget-instagram.php:76
msgid ""
"Current window (_self)"
msgstr ""

#: framework/widgets/widget-instagram.php:104
msgid ""
"Unable to communicate "
"with Instagram."
msgstr ""

#: framework/widgets/widget-instagram.php:107
msgid ""
"Instagram did not return "
"a 200."
msgstr ""

#: framework/widgets/widget-instagram.php:114
#: framework/widgets/widget-instagram.php:119
#: framework/widgets/widget-instagram.php:123
msgid ""
"Instagram has returned "
"invalid data."
msgstr ""

#: framework/widgets/widget-instagram.php:140
msgid "Instagram Image"
msgstr ""

#: framework/widgets/widget-instagram.php:170
msgid ""
"Instagram did not return "
"any images."
msgstr ""

#: framework/widgets/widget-posts.php:10
msgid ""
"Evatheme Latest Posts"
msgstr ""

#: framework/widgets/widget-posts.php:195
msgid "View Type:"
msgstr ""

#: framework/widgets/widget-posts.php:198
msgid "Grid"
msgstr ""

#: framework/widgets/widget-posts.php:199
msgid "Two Columns"
msgstr ""

#: framework/widgets/widget-posts.php:200
msgid "Big first post"
msgstr ""

#: framework/widgets/widget-posts.php:201
msgid "Carousel"
msgstr ""

#: framework/widgets/widget-posts.php:206
msgid "Category:"
msgstr ""

#: framework/widgets/widget-posts.php:220
msgid "Order by:"
msgstr ""

#: framework/widgets/widget-posts.php:223
msgid "Random"
msgstr ""

#: framework/widgets/widget-posts.php:224
#: single-portfolio.php:86
msgid "Date"
msgstr ""

#: framework/widgets/widget-posts.php:225
msgid "Comment count"
msgstr ""

#: framework/widgets/widget-posts.php:231
msgid "Number:"
msgstr ""

#: framework/widgets/widget-posts.php:245
msgid "All"
msgstr ""

#: framework/widgets/widgets.php:10
msgid "Blog Sidebar"
msgstr ""

#: framework/widgets/widgets.php:20
msgid "Single Post Sidebar"
msgstr ""

#: framework/widgets/widgets.php:33
msgid "Prefooter Area "
msgstr ""

#: framework/widgets/widgets.php:35
msgid ""
"The prefooter area "
"widgets"
msgstr ""

#: framework/widgets/widgets.php:65
msgid "Shop Sidebar"
msgstr ""

#: framework/woo_functions.php:85
msgid "Sale"
msgstr ""

#: framework/woo_functions.php:147
msgid "Share"
msgstr ""

#: framework/woo_functions.php:155
#: templates/blog/sharebox.php:39
#: templates/portfolio/sharebox.php:7
msgid "Google+"
msgstr ""

#: functions.php:37
msgid "Primary Menu"
msgstr ""

#: functions.php:38
msgid "Top Menu"
msgstr ""

#: page-authorslist.php:46
msgid "articles"
msgstr ""

#: page-comingsoon.php:55
msgid "Hours"
msgstr ""

#: page-comingsoon.php:59
msgid "Minutes"
msgstr ""

#: page-comingsoon.php:63
msgid "Seconds"
msgstr ""

#: page.php:33
#: single-portfolio.php:70
#: single.php:67
msgid "Read more!"
msgstr ""

#: page.php:35
#: single-portfolio.php:71
#: single.php:68
msgid "Pages"
msgstr ""

#: search.php:24
#: templates/blog/loop-left-image.php:43
#: templates/blog/loop.php:32
#: templates/blog/post-format/post-image.php:13
#: templates/blog/post-format/post-link.php:8
#: templates/blog/post-format/post.php:13
#, php-format
msgid "Permalink to %s"
msgstr ""

#: search.php:43
msgid ""
"Sorry, but nothing "
"matched your search "
"criteria. Please try "
"again with some "
"different keywords."
msgstr ""

#: searchform.php:3
msgid "Type your search"
msgstr ""

#: single-portfolio.php:93
msgid "Category"
msgstr ""

#: single-portfolio.php:101
msgid "Tags"
msgstr ""

#: single-portfolio.php:157
msgid "previous"
msgstr ""

#: single-portfolio.php:162
msgid "next"
msgstr ""

#: single.php:76
msgid "Tags:"
msgstr ""

#: templates/blog/authorinfo.php:13
msgid "Author say"
msgstr ""

#: templates/blog/loop-left-image.php:47
msgid "Read More"
msgstr ""

#: templates/blog/sharebox.php:14
#: templates/portfolio/sharebox.php:2
msgid "Share:"
msgstr ""

#: templates/blog/sharebox.php:27
msgid "Linkedin Share"
msgstr ""

#: templates/blog/sharebox.php:31
msgid "Reddit"
msgstr ""

#: templates/blog/sharebox.php:35
msgid "Digg"
msgstr ""

#: templates/page_title.php:23
msgid "Recent Posts"
msgstr ""

#: templates/page_title.php:30
msgid ""
"Category Archive for :"
msgstr ""

#: templates/page_title.php:32
msgid "Posts Tagged"
msgstr ""

#: templates/page_title.php:34
msgid "search results for"
msgstr ""

#: templates/page_title.php:36
msgid "Archive for"
msgstr ""

#: templates/page_title.php:40
msgid "Articles posted by"
msgstr ""

#: templates/page_title.php:44
msgid "Blog Archives"
msgstr ""
