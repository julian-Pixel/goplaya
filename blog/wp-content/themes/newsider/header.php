<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <?php newsider_favicon(); ?>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	
	<?php newsider_preloader(); ?>
	
	<div id="page-wrap">
		
		<?php $newsider_header_type = newsider_options('header_type'); ?>
		
		<header class="<?php echo esc_attr( $newsider_header_type ); ?> clearfix">
			
			<?php get_template_part( 'templates/header/header_layout' ); ?>
				
			<?php get_template_part( 'templates/header/header-mobile' ); ?>
			
		</header>
		
		<?php if( !is_404() ) {
			get_template_part( 'templates/page_title' ); 
		} ?>
		
		<div id="page-content">