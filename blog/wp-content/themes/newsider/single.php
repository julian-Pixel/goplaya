<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header();

global $post;

$newsider_postid 		= get_the_ID();
$newsider_pf 			= get_post_format();
$featured_image_url 	= wp_get_attachment_url(get_post_thumbnail_id());
$featured_image_url_bg 	= newsider_aq_resize( $featured_image_url, 1920, 650, true, true, true );

$newsider_blogsingle_class = $newsider_blogsingle_wrap_class = '';

$newsider_blogsingle_wrap_class .= 'format-' . esc_attr( $newsider_pf ) . ' ';

$newsider_blogsingle_layout = get_post_meta($newsider_postid, 'newsider_blogsingle_layout', true);
if( $newsider_blogsingle_layout == 'left-sidebar' ) {
	$newsider_sidebar_class = 'pull-left ';
	$newsider_blogsingle_class .= 'col-md-8 pull-right ';
} elseif( $newsider_blogsingle_layout == 'right-sidebar' ) {
	$newsider_sidebar_class = 'pull-right ';
	$newsider_blogsingle_class .= 'col-md-8 pull-left ';
} else {
	$newsider_sidebar_class = '';
	$newsider_blogsingle_class .= 'col-md-12 ';
}
$newsider_blogsingle_wrap_class .= $newsider_blogsingle_layout . ' ';

$category = get_the_category();
$newsider_blogsingle_wrap_class .= 'category-' . $category[0]->slug . ' ';

$newsider_single_post_navigation = '';
if( class_exists( 'ReduxFrameworkPlugin' ) && ( newsider_options('single_post_navigation') != 0 ) ) {
	$newsider_single_post_navigation = 'show';
} else if( ! class_exists( 'ReduxFrameworkPlugin' ) ){
	$newsider_single_post_navigation = 'show';
}

$newsider_single_post_author = '';
if( class_exists( 'ReduxFrameworkPlugin' ) && ( newsider_options('single_post_author') != 0 ) ) {
	$newsider_single_post_author = 'show';
} else if( ! class_exists( 'ReduxFrameworkPlugin' ) ){
	$newsider_single_post_author = 'show';
}

$newsider_single_post_related_posts = '';
if( class_exists( 'ReduxFrameworkPlugin' ) && ( newsider_options('single_post_related_posts') != 0 ) ) {
	$newsider_single_post_related_posts = 'show';
} else if( ! class_exists( 'ReduxFrameworkPlugin' ) ){
	$newsider_single_post_related_posts = 'show';
}

$newsider_date_format = get_option( 'date_format' );

$newsider_blogsingle_featured_img = get_post_meta($newsider_postid, 'newsider_blogsingle_featured_img', true);
?>

		<div id="blog-single-wrap" class="<?php echo esc_attr( $newsider_blogsingle_wrap_class ); ?> clearfix">
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
				<?php if( has_post_thumbnail( $post->ID ) && $newsider_blogsingle_featured_img == 'featured_img_wide' ) { ?>
					
					<div class="featured_img_wide_wrap mb30">
						<div class="featured_img_bg" 
							<?php if( ! empty( $featured_image_url ) ) { ?>
								style="background-image:url(<?php echo $featured_image_url_bg ?>);"
							<?php } ?>
						></div>
						<div class="container featured_img_wide_descr">
							<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
							<div class="single_post_title">
								<h1><?php the_title(); ?></h1>
							</div>
							<div class="single_post_meta clearfix">
								<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
								<span class="post_meta_date"><?php the_time( esc_html( $newsider_date_format ) ); ?></span>
								<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
							</div>
						</div>
					</div>
					
				<?php } else if( $newsider_blogsingle_featured_img == 'featured_img_fullwidth' ) { ?>
					
					<div class="container">
						<div class="post_format_content mb30 text-center">
							<?php if( $newsider_pf == '' || $newsider_pf == 'image' || $newsider_pf == 'gallery' || $newsider_pf == 'video' ) { ?>
								<?php get_template_part( 'templates/blog/post-format/post', $newsider_pf ); ?>
							<?php } else { ?>
								<?php get_template_part( 'templates/blog/post-format/post', 'image' ); ?>
							<?php } ?>
						</div>
					</div>
					
				<?php } ?>
                
                <?php
						/*==========================================================
						/// ANUNCIOS
						----------------------------------------------------------*/
						
						/// ESPANIOL 
						  if(qtranxf_getLanguage() == "es") { ?>
							<?php
								// BANNER PUBLICIDAD TOP
								$id_post = get_the_ID($query->ID);
							
								if( have_rows('banners') ): ?>
								
								<div class="banner-top">
								<?php 
									// loop through the rows of data
									while ( have_rows('banners') ) : the_row(); ?>
								
										<?php 
										$posicion = get_sub_field('posicion', $id_post);
										
										if( $posicion == 1 ):
										
										$image = get_sub_field('imagen', $id_post);
										$enlace = get_sub_field('enlace', $id_post);
										?>
										
										<div class="publicidad">
											<a href="<?php echo $enlace  ?>" target="_blank">
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											</a>
										 </div>
								<?php
										endif;	
									endwhile; ?>
									</div>
								<?php endif;
							
							?>
						<?php } ?>									 
							 
						 <?php
						 /// INGLES 
						  if(qtranxf_getLanguage() == "en") { ?>
							<?php
								// BANNER PUBLICIDAD TOP
								$id_post = get_the_ID($query->ID);
							
								if( have_rows('banners_en') ): ?>
								
								<div class="banner-top">
								<?php 
									// loop through the rows of data
									while ( have_rows('banners_en') ) : the_row(); ?>
								
										<?php 
										$posicion = get_sub_field('posicion_en', $id_post);
										
										if( $posicion == 1 ):
										
										$image = get_sub_field('imagen_en', $id_post);
										$enlace = get_sub_field('enlace_en', $id_post);
										?>
										
										<div class="publicidad">
											<a href="<?php echo $enlace  ?>" target="_blank">
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											</a>
										 </div>
								<?php
										endif;	
									endwhile; ?>
									</div>
								<?php endif;
							
							?>
							
						<?php } ?>
  
				
				<div class="container">
					<div class="row">
						<div class="single-post-content clearfix <?php echo esc_attr( $newsider_blogsingle_class ); ?>">
							<?php if( $newsider_blogsingle_featured_img == 'featured_img_wide' && ( $newsider_pf != 'image' && $newsider_pf != '' ) ) { ?>
								<div class="post_format_content mb30 text-center">
									<?php get_template_part( 'templates/blog/post-format/post', $newsider_pf ); ?>
								</div>
							<?php } else if( $newsider_blogsingle_featured_img == 'featured_img_fullwidth' ) { ?>
								
                                <?php if(!has_post_thumbnail( $post->ID )): ?>
                                
                                <div class="single_post_meta clearfix">
									<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
									<span class="post_meta_date"><?php the_time( esc_html( $newsider_date_format ) ); ?></span>
									<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
								</div>
								<div class="single_post_title">
									<h1><?php the_title(); ?></h1>
								</div>
                                
                                <?php endif; ?>
                                
								<?php if ( $newsider_pf != '' && $newsider_pf != 'image' && $newsider_pf != 'gallery' && $newsider_pf != 'video' ) { ?>
									<div class="post_format_content mb30 text-center">
										<?php get_template_part( 'templates/blog/post-format/post', $newsider_pf ); ?>
									</div>
								<?php } ?>
							<?php } else { ?>
							
								<div class="post_format_content mb30 text-center">
									<?php get_template_part( 'templates/blog/post-format/post', $newsider_pf ); ?>
								</div>
                                
                                <?php if(!has_post_thumbnail( $post->ID )): ?>
								<div class="single_post_meta clearfix">
									<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
									<span class="post_meta_date"><?php the_time( esc_html( $newsider_date_format ) ); ?></span>
									<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
								</div>
								<div class="single_post_title">
									<h1><?php the_title(); ?></h1>
								</div>
                                
                                <?php endif; ?>
							<?php } ?>
                            
                            <div class="row">
								<?php if( has_tag() ) { ?>
                                    <div class="single_post_meta_tags">
                                        <span><?php _e("<!--:en-->Tags<!--:--><!--:es-->Etiquetas<!--:-->");?></span>
                                        <?php the_tags('',' ', ''); ?>
                                    </div>
                                <?php } ?>
                            </div>
					
							<div class="single-post-content-wrap">
								<?php
									the_content(esc_html__('Read more!', 'newsider'));
									wp_link_pages(array('before' => '<div class="page-link">' . esc_html__('Pages', 'newsider') . ': ', 'after' => '</div>'));
								?>
							</div>
                            
                           
						<?php 
                        /*======================================================
                        // VIDEOS
                        ------------------------------------------------------*/
                        
                        /// ESPANIOL 
                          if(qtranxf_getLanguage() == "es") { ?>
                            <?php
                            $id_post = get_the_ID($query->ID);
                           
                                if( have_rows('videos_del_articulo') ): ?>
                                
                                <div class="videos">
                                <h1><span>Videos</span> Relacionados</h1>
                    
                                <?php 
                                    // loop through the rows of data
                                    while ( have_rows('videos_del_articulo') ) : the_row(); ?>
                                
                                    <div class="col-md-6 video">
                                        <?php 
                                        $url_video = get_sub_field('url_del_video');
                                        
                                        $id_video = substr($url_video, 17, 50);
                                        ?>
                                        
                                        <iframe width="620" height="415"
                                        src="https://www.youtube.com/embed/<?php echo $id_video ?>">
                                        </iframe>
                                    </div>
                                <?php	
                                    endwhile; ?>
                                    </div>
                                <?php endif;
                            
                            ?>
                        <?php } ?>									 
                             
                         <?php
                         /// INGLES 
                          if(qtranxf_getLanguage() == "en") { ?>
                            <?php
                            $id_post = get_the_ID($query->ID);
                       
                            if( have_rows('videos_del_articulo_en') ): ?>
                            
                            <div class="videos">
                            <h1><span>Related</span> Videos</h1>
                    
                            <?php
                                // loop through the rows of data
                                while ( have_rows('videos_del_articulo_en') ) : the_row(); ?>
                            
                                <div class="col-md-6 video">
                                    <?php 
                                    $url_video = get_sub_field('url_del_video_en');
                                    
                                    $id_video = substr($url_video, 17, 50);
                                    ?>
                                    
                                    <iframe width="620" height="415"
                                    src="https://www.youtube.com/embed/<?php echo $id_video ?>">
                                    </iframe>
                                </div>
                            <?php	
                                endwhile; ?>
                            </div>
                            <?php endif;
                            
                            ?>
                            
                        <?php } ?>
								
                            
						<?php
						/*==========================================================
						/// ANUNCIOS
						----------------------------------------------------------*/
						
						/// ESPANIOL 
						  if(qtranxf_getLanguage() == "es") { ?>
							<?php
								// BANNER PUBLICIDAD TOP
								$id_post = get_the_ID($query->ID);
							
								if( have_rows('banners') ): ?>
								
								<div class="banner-medio">
								<?php 
									// loop through the rows of data
									while ( have_rows('banners') ) : the_row(); ?>
								
										<?php 
										$posicion = get_sub_field('posicion', $id_post);
										
										if( $posicion == 2 ):
										
										$image = get_sub_field('imagen', $id_post);
										$enlace = get_sub_field('enlace', $id_post);
										?>
										
										<div class="publicidad">
											<a href="<?php echo $enlace  ?>" target="_blank">
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											</a>
										 </div>
								<?php
										endif;	
									endwhile; ?>
									</div>
								<?php endif;
							
							?>
						<?php } ?>									 
							 
						 <?php
						 /// INGLES 
						  if(qtranxf_getLanguage() == "en") { ?>
							<?php
								// BANNER PUBLICIDAD TOP
								$id_post = get_the_ID($query->ID);
							
								if( have_rows('banners_en') ): ?>
								
								<div class="banner-medio">
								<?php 
									// loop through the rows of data
									while ( have_rows('banners_en') ) : the_row(); ?>
								
										<?php 
										$posicion = get_sub_field('posicion_en', $id_post);
										
										if( $posicion == 2 ):
										
										$image = get_sub_field('imagen_en', $id_post);
										$enlace = get_sub_field('enlace_en', $id_post);
										?>
										
										<div class="publicidad">
											<a href="<?php echo $enlace  ?>" target="_blank">
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											</a>
										 </div>
								<?php
										endif;	
									endwhile; ?>
									</div>
								<?php endif;
							
							?>
							
						<?php } ?>
							
                            
							<div class="posts_nav_link"><?php posts_nav_link(); ?></div>
							
							<div class="row">
								<div class="col-sm-6">
									<div class="post_share_wrap single_content">
										<?php get_template_part( 'templates/blog/sharebox' ); ?>
									</div>
								</div>
							</div>
						
							<?php if( $newsider_single_post_navigation == 'show' ) {
								
								newsider_post_navigation();
								
							} ?>
							
							<?php if( $newsider_single_post_author == 'show' ) {
								get_template_part( 'templates/blog/authorinfo' );
							} ?>
							
							<?php if( $newsider_single_post_related_posts == 'show' ) {
								get_template_part('templates/blog/related-posts');
							} ?>
				
							<?php 
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;
							?>
							
						</div>
                        
                        
						
						<?php if( $newsider_blogsingle_layout != 'no-sidebar' ) { ?>
						
							<div id="blog_sidebar" class="col-md-4 blogsidebar_wrap <?php echo esc_attr( $newsider_sidebar_class ); ?>">
								<?php if ( is_active_sidebar('singlepost-sidebar') ) {
									dynamic_sidebar('singlepost-sidebar');
								} else {
									dynamic_sidebar('blog-sidebar');
								} ?>
							</div>
						
						<?php } ?>
						
					</div>
				</div>
				
			<?php endwhile; endif; ?>
            
            
            <?php
						/*==========================================================
						/// ANUNCIOS
						----------------------------------------------------------*/
						
						/// ESPANIOL 
						  if(qtranxf_getLanguage() == "es") { ?>
							<?php
								// BANNER PUBLICIDAD TOP
								$id_post = get_the_ID($query->ID);
							
								if( have_rows('banners') ): ?>
								
								<div class="banner-abajo">
								<?php 
									// loop through the rows of data
									while ( have_rows('banners') ) : the_row(); ?>
								
										<?php 
										$posicion = get_sub_field('posicion', $id_post);
										
										if( $posicion == 3 ):
										
										$image = get_sub_field('imagen', $id_post);
										$enlace = get_sub_field('enlace', $id_post);
										?>
										
										<div class="publicidad">
											<a href="<?php echo $enlace  ?>" target="_blank">
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											</a>
										 </div>
								<?php
										endif;	
									endwhile; ?>
									</div>
								<?php endif;
							
							?>
						<?php } ?>									 
							 
						 <?php
						 /// INGLES 
						  if(qtranxf_getLanguage() == "en") { ?>
							<?php
								// BANNER PUBLICIDAD TOP
								$id_post = get_the_ID($query->ID);
							
								if( have_rows('banners_en') ): ?>
								
								<div class="banner-abajo">
								<?php 
									// loop through the rows of data
									while ( have_rows('banners_en') ) : the_row(); ?>
								
										<?php 
										$posicion = get_sub_field('posicion_en', $id_post);
										
										if( $posicion == 3 ):
										
										$image = get_sub_field('imagen_en', $id_post);
										$enlace = get_sub_field('enlace_en', $id_post);
										?>
										
										<div class="publicidad">
											<a href="<?php echo $enlace  ?>" target="_blank">
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											</a>
										 </div>
								<?php
										endif;	
									endwhile; ?>
									</div>
								<?php endif;
							
							?>
							
						<?php } ?>
           
			
		</div>

<?php get_footer(); ?>