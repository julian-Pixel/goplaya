<?php

global $post;

$newsider_postId = ( isset( $post->ID ) ? get_the_ID() : NULL );

$newsider_body_font_family 				= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'body-font', 'font-family' ) : 'Roboto Condensed';
$newsider_body_font_color 				= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'body-font', 'color' ) : '#222222';
$newsider_theme_color 					= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'theme_color' ) : '#3fc6cb';

// Header Primary Menu
$newsider_menu_primary_font_family 		= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'menu_primary', 'font-family' ) : 'Roboto Condensed';
$newsider_menu_primary_transform 		= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'menu_primary', 'text-transform' ) : 'uppercase';
$newsider_menu_primary_weight 			= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'menu_primary', 'font-weight' ) : '400';
$newsider_menu_primary_size 			= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'menu_primary', 'font-size' ) : '12px';
$newsider_menu_primary_spacing 			= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'menu_primary', 'letter-spacing' ) : '2px';

$newsider_submenu_primary_font_family 	= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'submenu_primary', 'font-family' ) : 'Roboto Condensed';
$newsider_submenu_primary_transform 	= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'submenu_primary', 'text-transform' ) : 'uppercase';
$newsider_submenu_primary_weight 		= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'submenu_primary', 'font-weight' ) : '400';
$newsider_submenu_primary_size 			= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'submenu_primary', 'font-size' ) : '11px';
$newsider_submenu_primary_spacing 		= class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'submenu_primary', 'letter-spacing' ) : '1px';

/* Custom CSS from Theme Options */
$newsider_options_custom_css = newsider_options('custom_css');

//	Theme Layout
$newsider_theme_bg_image = $newsider_theme_bg_image_repeat = $newsider_theme_bg_color = $newsider_theme_bg_attachment = $newsider_theme_bg_position = $newsider_theme_bg_cover = $newsider_theme_bg_styles = '';
if( class_exists( 'ReduxFrameworkPlugin' ) && newsider_options('theme_bg_image') != '' ) {
	if( newsider_options('theme_bg_image','background-image') != '' ) {
		$newsider_theme_bg_image = newsider_options('theme_bg_image','background-image');
	}
	if( newsider_options('theme_bg_image','background-repeat') != '' ) {
		$newsider_theme_bg_image_repeat = newsider_options('theme_bg_image','background-repeat');
	}
	if( newsider_options('theme_bg_image','background-attachment') != '' ) {
		$newsider_theme_bg_attachment = newsider_options('theme_bg_image','background-attachment');
	}
	if( newsider_options('theme_bg_image','background-position') != '' ) {
		$newsider_theme_bg_position = newsider_options('theme_bg_image','background-position');
	}
	if( newsider_options('theme_bg_image','background-size') != '' ) {
		$newsider_theme_bg_cover = newsider_options('theme_bg_image','background-size');
	}
}
if( class_exists( 'ReduxFrameworkPlugin' ) && newsider_options('theme_bg_image','background-color') != '' ) {
	$newsider_theme_bg_color = newsider_options('theme_bg_image','background-color');
}

if( $newsider_theme_bg_image != '' ) {
	$newsider_theme_bg_styles .= 'background-image: url('. $newsider_theme_bg_image .');';
	if( $newsider_theme_bg_image_repeat != '' ) {
		$newsider_theme_bg_styles .= 'background-repeat: '. $newsider_theme_bg_image_repeat .';';
	}
	if( $newsider_theme_bg_attachment != '' ) {
		$newsider_theme_bg_styles .= 'background-attachment: '. $newsider_theme_bg_attachment .';';
	}
	if( $newsider_theme_bg_position != '' ) {
		$newsider_theme_bg_styles .= 'background-position: '. $newsider_theme_bg_position .';';
	}
	if( $newsider_theme_bg_cover != '' ) {
		$newsider_theme_bg_styles .= 'background-size: '. $newsider_theme_bg_cover .';';
		$newsider_theme_bg_styles .= '-moz-background-size: '. $newsider_theme_bg_cover .';';
		$newsider_theme_bg_styles .= '-webkit-background-size: '. $newsider_theme_bg_cover .';';
		$newsider_theme_bg_styles .= '-o-background-size: '. $newsider_theme_bg_cover .';';
		$newsider_theme_bg_styles .= '-ms-background-size: '. $newsider_theme_bg_cover .';';
	}
}

if( $newsider_theme_bg_color != '' ) {
	$newsider_theme_bg_styles .= 'background-color: '. $newsider_theme_bg_color .';';
}

// Page Title Background Stylings
$newsider_pagetitle_bg_styles = $newsider_pagetitle_bg_image = $newsider_pagetitle_bg_image_repeat = $newsider_pagetitle_bg_color = $newsider_pagetitle_bg_attachment = $newsider_pagetitle_bg_position = $newsider_pagetitle_bg_cover = $newsider_pagetitle_text_color = '';
$newsider_pagetitle_bg_image = get_post_meta( $newsider_postId, 'newsider_pagetitle_bg_image', true );

if ( $newsider_pagetitle_bg_image ) {
	if ( is_numeric( $newsider_pagetitle_bg_image ) ) {
		$newsider_pagetitle_bg_image = wp_get_attachment_image_src( $newsider_pagetitle_bg_image, 'full' );
		$newsider_pagetitle_bg_image = $newsider_pagetitle_bg_image[0];
	}
}

$newsider_pagetitle_bg_image_repeat 	= get_post_meta( $newsider_postId, 'newsider_pagetitle_bg_repeat', true );
$newsider_pagetitle_bg_color 			= get_post_meta( $newsider_postId, 'newsider_pagetitle_bg_color', true );
$newsider_pagetitle_bg_attachment 		= get_post_meta( $newsider_postId, 'newsider_pagetitle_bg_attachment', true );
$newsider_pagetitle_bg_position 		= get_post_meta( $newsider_postId, 'newsider_pagetitle_bg_position', true );
$newsider_pagetitle_bg_cover 			= get_post_meta( $newsider_postId, 'newsider_pagetitle_bg_full', true );
$newsider_pagetitle_text_color 			= get_post_meta( $newsider_postId, 'newsider_pagetitle_text_color', true );

$newsider_theme_pagetitle_bg_image = $newsider_theme_pagetitle_bg_image_repeat = $newsider_theme_pagetitle_bg_color = $newsider_theme_pagetitle_bg_attachment = $newsider_theme_pagetitle_bg_position = $newsider_theme_pagetitle_bg_cover = '';
$newsider_theme_pagetitle_text_color = '#222222';
if( newsider_options('pagetitle_bg_image') ) {
	if( newsider_options('pagetitle_bg_image', 'background-image') != '' ) {
		$newsider_theme_pagetitle_bg_image = newsider_options('pagetitle_bg_image', 'background-image');
	}
	if( newsider_options('pagetitle_bg_image', 'background-repeat') != '' ) {
		$newsider_theme_pagetitle_bg_image_repeat = newsider_options('pagetitle_bg_image', 'background-repeat');
	}
	if( newsider_options('pagetitle_bg_image', 'background-attachment') != '' ) {
		$newsider_theme_pagetitle_bg_attachment = newsider_options('pagetitle_bg_image', 'background-attachment');
	}
	if( newsider_options('pagetitle_bg_image', 'background-position') != '' ) {
		$newsider_theme_pagetitle_bg_position = newsider_options('pagetitle_bg_image', 'background-position');
	}
	if( newsider_options('pagetitle_bg_image', 'background-size') != '' ) {
		$newsider_theme_pagetitle_bg_cover = newsider_options('pagetitle_bg_image', 'background-size');
	}
	if( newsider_options('pagetitle_text_color') != '' ) {
		$newsider_theme_pagetitle_text_color = newsider_options('pagetitle_text_color');
	}
}
if( newsider_options('pagetitle_bg_image', 'background-color') != '' ) {
	$newsider_theme_pagetitle_bg_color = newsider_options('pagetitle_bg_image', 'background-color');
}

//	If Blog
if ( ( is_home() ) && newsider_options('blog_pagetitle_bg_image') ) {
	if( newsider_options('blog_pagetitle_bg_image', 'background-image') != '' ) {
		$newsider_theme_pagetitle_bg_image = newsider_options('blog_pagetitle_bg_image', 'background-image');
	}
	if( newsider_options('blog_pagetitle_bg_image', 'background-repeat') != '' ) {
		$newsider_theme_pagetitle_bg_image_repeat = newsider_options('blog_pagetitle_bg_image', 'background-repeat');
	}
	if( newsider_options('blog_pagetitle_bg_image', 'background-attachment') != '' ) {
		$newsider_theme_pagetitle_bg_attachment = newsider_options('blog_pagetitle_bg_image', 'background-attachment');
	}
	if( newsider_options('blog_pagetitle_bg_image', 'background-position') != '' ) {
		$newsider_theme_pagetitle_bg_position = newsider_options('blog_pagetitle_bg_image', 'background-position');
	}
	if( newsider_options('blog_pagetitle_bg_image', 'background-size') != '' ) {
		$newsider_theme_pagetitle_bg_cover = newsider_options('blog_pagetitle_bg_image', 'background-size');
	}
	if( newsider_options('blog_pagetitle_text_color') != '' ) {
		$newsider_theme_pagetitle_text_color = newsider_options('blog_pagetitle_text_color');
	}
}
if( ( is_home() ) && newsider_options('blog_pagetitle_bg_image', 'background-color') != '' ) {
	$newsider_theme_pagetitle_bg_color = newsider_options('blog_pagetitle_bg_image', 'background-color');
}

$newsider_pagetitle_bg_image 			= ! empty( $newsider_pagetitle_bg_image ) ? $newsider_pagetitle_bg_image : $newsider_theme_pagetitle_bg_image;
$newsider_pagetitle_bg_color 			= ! empty( $newsider_pagetitle_bg_color ) ? $newsider_pagetitle_bg_color : $newsider_theme_pagetitle_bg_color;
$newsider_pagetitle_bg_image_repeat 	= ! empty( $newsider_pagetitle_bg_image_repeat ) ? $newsider_pagetitle_bg_image_repeat : $newsider_theme_pagetitle_bg_image_repeat;
$newsider_pagetitle_bg_attachment 		= ! empty( $newsider_pagetitle_bg_attachment ) ? $newsider_pagetitle_bg_attachment : $newsider_theme_pagetitle_bg_attachment;
$newsider_pagetitle_bg_position 		= ! empty( $newsider_pagetitle_bg_position ) ? $newsider_pagetitle_bg_position : $newsider_theme_pagetitle_bg_position;
$newsider_pagetitle_bg_cover 			= ! empty( $newsider_pagetitle_bg_cover ) ? $newsider_pagetitle_bg_cover : $newsider_theme_pagetitle_bg_cover;
$newsider_pagetitle_text_color 			= ! empty( $newsider_pagetitle_text_color ) ? $newsider_pagetitle_text_color : $newsider_theme_pagetitle_text_color;

if( $newsider_pagetitle_bg_image != '' ) {
	$newsider_pagetitle_bg_styles .= 'background-image: url('. $newsider_pagetitle_bg_image .');';
	if( $newsider_pagetitle_bg_image_repeat != '' ) {
		$newsider_pagetitle_bg_styles .= 'background-repeat: '. $newsider_pagetitle_bg_image_repeat .';';
	}
	if( $newsider_pagetitle_bg_attachment != '' ) {
		$newsider_pagetitle_bg_styles .= 'background-attachment: '. $newsider_pagetitle_bg_attachment .';';
	}
	if( $newsider_pagetitle_bg_position != '' ) {
		$newsider_pagetitle_bg_styles .= 'background-position: '. $newsider_pagetitle_bg_position .';';
	}
	if( $newsider_pagetitle_bg_cover != '' ) {
		$newsider_pagetitle_bg_styles .= 'background-size: '. $newsider_pagetitle_bg_cover .';';
		$newsider_pagetitle_bg_styles .= '-moz-background-size: '. $newsider_pagetitle_bg_cover .';';
		$newsider_pagetitle_bg_styles .= '-webkit-background-size: '. $newsider_pagetitle_bg_cover .';';
		$newsider_pagetitle_bg_styles .= '-o-background-size: '. $newsider_pagetitle_bg_cover .';';
		$newsider_pagetitle_bg_styles .= '-ms-background-size: '. $newsider_pagetitle_bg_cover .';';
	}
}

if( $newsider_pagetitle_bg_color != '' ) {
	$newsider_pagetitle_bg_styles .= 'background-color: '. $newsider_pagetitle_bg_color .';';
}

if( $newsider_pagetitle_text_color != '' ) {
	$newsider_pagetitle_bg_styles .= 'color: '. $newsider_pagetitle_text_color .';';
}

if ( is_singular('post') ) {
	$newsider_featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
	$newsider_pagetitle_bg_styles .= 'background-image: url('. $newsider_featured_image_url .');';
}

/* Page Title Height */
$newsider_options_pagetitle_height = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options('pagetitle_height') : '150px';
$newsider_pagetitle_height = ( get_post_meta( $newsider_postId, 'newsider_pagetitle_height', true ) ? get_post_meta( $newsider_postId, 'newsider_pagetitle_height', true ) : $newsider_options_pagetitle_height );
if ( is_home() ) {
	$newsider_pagetitle_height = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options('blog_pagetitle_height') : '150px';
} else if ( is_singular('post') ) {
	$newsider_pagetitle_height = '';
} else if ( is_category() || is_tag() || is_search() || is_day() || is_month() || is_year() ) {
	$newsider_pagetitle_height = '150px';
}
if ( $newsider_pagetitle_height != '' ) {
	$newsider_pagetitle_bg_styles .= 'height: '. $newsider_pagetitle_height .';';
}

	
$newsider_cat_styles .= '
	article.post .post_category_label,
	article.post .post_category_label a,
	.evapb-bloglist.left_image .post_meta span.post_meta_category a,
	#blog_list.default article.post .post_title a:hover,
	#related_posts_list article.post .post_title a:hover,
	.top_slide_wrap.style3 .post .post_meta_category,
	.top_slide_wrap.style3 .post .post_meta_category a,
	.post .post_title a:hover,
	.evapb-bloglist.bg_image .post .post_descr_wrap .post_title a:hover,
	.evapb_blog_carousel.fisrt_big_img .big_post.post .post_title a:hover,
	.recent_posts_list li .recent_post_meta_category a,
	.recent_posts_list li .recent_post_title a:hover,
	.footer_posts_carousel_title a:hover,
	.widget_categories a:hover,
	.widget_categories li .val,
	.top_slider .post_meta_category a,
	.top_slider .post_title a:hover,
	.evapb_blog_carousel.small_carousel .post_content_wrapper:hover .post_title a,
	.evapb-categories a:hover h6,
	.evapb-bloglist.card_clean .post.format-status .post_title a:hover,
	.evapb-bloglist.top_img.style2 .format-link .post_title a:hover,
	.evapb-bloglist.top_img.style2 .format-quote .post_title a:hover,
	#blog-single-wrap .featured_img_wide_descr .post_category_label a,
	#blog-single-wrap .single_post_meta span a{
		color: ' . $newsider_theme_color . ';
	}
	.post .post_category_label span,
	.post .post_category_label span:before,
	.post .post_category_label span:after,
	.evapb-categories a:after,
	.widget_newsider_cat_thumbnails li a:before,
	.post span.post_meta_date:after,
	.recent_posts_list li .post_category_label span,
	.recent_posts_list li .post_category_label span:before,
	.recent_posts_list li .post_category_label span:after,
	.top_slider .post_category_label span,
	.top_slider .post_category_label span:before,
	.top_slider .post_category_label span:after,
	#blog_list.default .post_meta .post_meta_date:after,
	#related_posts_list .post_meta .post_meta_date:after,
	.evapb-bloglist.left_image .post_meta .post_meta_date:after,
	.top_slider.style1 .post_meta span.post_meta_date:after,
	.top_slider.style2 .post_meta span.post_meta_date:after,
	.top_slider.style2 .post_meta span.post_meta_author:after,
	.top_slider.style4 .post_meta span.post_meta_date:after,
	.top_slider.style5 .post_meta span.post_meta_date:after,
	.top_slider.style5 .post_meta span.post_meta_author:after,
	.top_slider.style6 .post_meta span.post_meta_date:after,
	.top_slider.style6 .post_meta span.post_meta_author:after,
	.evapb-bloglist.big_bg_images .post_meta span.post_meta_date:after,
	.evapb-bloglist.card_clean .post_meta_category,
	.evapb-bloglist.card_clean .post .post_meta_category,
	#blog-single-wrap .featured_img_wide_descr .post_category_label span,
	#blog-single-wrap .featured_img_wide_descr .post_category_label span:before,
	#blog-single-wrap .featured_img_wide_descr .post_category_label span:after,
	#blog-single-wrap .single_post_meta .post_category_label span,
	#blog-single-wrap .single_post_meta .post_category_label span:before,
	#blog-single-wrap .single_post_meta .post_category_label span:after,
	#blog-single-wrap .single_post_meta .post_meta_date:after,
	#blog-single-wrap .single_post_meta .post_meta_author:after{
		background-color: ' . $newsider_theme_color . ';
	}
	.evapb_blog_carousel.small_carousel .post_content_wrapper:hover .post_descr_wrap{
		border-color: ' . $newsider_theme_color . ';
	}
';


//	MailChimp Plugin
$newsider_mc4wp_styles = '';
if ( class_exists( 'ReduxFrameworkPlugin' ) && function_exists( 'mc4wp' ) ) {
	
	$newsider_mc4wp_styles_bg = $newsider_mc4wp_widget_bg = $newsider_mc4wp_widget_bg_repeat = $newsider_mc4wp_widget_bg_attachment = $newsider_mc4wp_widget_bg_position = $newsider_mc4wp_widget_bg_cover = $newsider_mc4wp_widget_text_color = $newsider_mc4wp_widget_bg_color = $newsider_mc4wp_widget_text_color_set = '';
	if( newsider_options('mc4wp_widget_bg') ) {
		if( newsider_options('mc4wp_widget_bg','background-image') != '' ) {
			$newsider_mc4wp_widget_bg = newsider_options('mc4wp_widget_bg','background-image');
		}
		if( newsider_options('mc4wp_widget_bg','background-repeat') != '' ) {
			$newsider_mc4wp_widget_bg_repeat = newsider_options('mc4wp_widget_bg','background-repeat');
		}
		if( newsider_options('mc4wp_widget_bg','background-attachment') != '' ) {
			$newsider_mc4wp_widget_bg_attachment = newsider_options('mc4wp_widget_bg','background-attachment');
		}
		if( newsider_options('mc4wp_widget_bg','background-position') != '' ) {
			$newsider_mc4wp_widget_bg_position = newsider_options('mc4wp_widget_bg','background-position');
		}
		if( newsider_options('mc4wp_widget_bg','background-size') != '' ) {
			$newsider_mc4wp_widget_bg_cover = newsider_options('mc4wp_widget_bg','background-size');
		}
		if( newsider_options('shop_pagetitle_text_color') != '' ) {
			$newsider_mc4wp_widget_text_color = newsider_options('mc4wp_widget_text_color');
		}
	}
	if( newsider_options('mc4wp_widget_bg','background-color') != '' ) {
		$newsider_mc4wp_widget_bg_color = newsider_options('mc4wp_widget_bg','background-color');
	}

	if( $newsider_mc4wp_widget_bg != '' ) {
		$newsider_mc4wp_styles_bg .= 'background-image: url('. $newsider_mc4wp_widget_bg .');';
		if( $newsider_mc4wp_widget_bg_repeat != '' ) {
			$newsider_mc4wp_styles_bg .= 'background-repeat: '. $newsider_mc4wp_widget_bg_repeat .';';
		}
		if( $newsider_mc4wp_widget_bg_attachment != '' ) {
			$newsider_mc4wp_styles_bg .= 'background-attachment: '. $newsider_mc4wp_widget_bg_attachment .';';
		}
		if( $newsider_mc4wp_widget_bg_position != '' ) {
			$newsider_mc4wp_styles_bg .= 'background-position: '. $newsider_mc4wp_widget_bg_position .';';
		}
		if( $newsider_mc4wp_widget_bg_cover != '' ) {
			$newsider_mc4wp_styles_bg .= 'background-size: '. $newsider_mc4wp_widget_bg_cover .';';
			$newsider_mc4wp_styles_bg .= '-moz-background-size: '. $newsider_mc4wp_widget_bg_cover .';';
			$newsider_mc4wp_styles_bg .= '-webkit-background-size: '. $newsider_mc4wp_widget_bg_cover .';';
			$newsider_mc4wp_styles_bg .= '-o-background-size: '. $newsider_mc4wp_widget_bg_cover .';';
			$newsider_mc4wp_styles_bg .= '-ms-background-size: '. $newsider_mc4wp_widget_bg_cover .';';
		}
	}

	if( $newsider_mc4wp_widget_bg_color != '' ) {
		$newsider_mc4wp_styles_bg .= 'background-color: '. $newsider_mc4wp_widget_bg_color .';';
	}
	
	if( $newsider_mc4wp_widget_text_color != '' ) {
		$newsider_mc4wp_widget_text_color_set .= 'color: '. $newsider_mc4wp_widget_text_color .';';
	}
	
	if( $newsider_mc4wp_styles_bg ) {
		$newsider_mc4wp_styles .= '
			.widget .mc4wp-form{
				' . $newsider_mc4wp_styles_bg . '
			}
			.widget .mc4wp-form h6,
			.widget .mc4wp-form span,
			.widget .mc4wp-form p i,
			.widget .mc4wp-form input{
				' . $newsider_mc4wp_widget_text_color_set . ';
			}
		';
	}
}
?>
	
/* Typography */
blockquote:before{ background-color: <?php echo $newsider_theme_color; ?> ; }
textarea, input, .contentarea form.wpcf7-form textarea, .contentarea form.wpcf7-form input{ font-family: <?php echo $newsider_body_font_family; ?> , Arial, Helvetica, sans-serif !important; }
.contentarea ol > li:before, .single-post-content ol > li:before{ color: <?php echo $newsider_theme_color; ?> ; }
input[type='text']:focus, input[type='email']:focus, input[type='url']:focus, input[type='password']:focus, input[type='search']:focus, textarea:focus{ border-color: <?php echo $newsider_theme_color; ?> !important; }

<?php echo $newsider_cat_styles; ?>

<?php if( $newsider_theme_bg_styles ) {
	echo '
		body.boxed { ' . $newsider_theme_bg_styles . ' }
	';
}

$newsider_theme_boxed_margin = newsider_options('theme_boxed_margin');
if( class_exists( 'ReduxFrameworkPlugin' ) && $newsider_theme_boxed_margin != '' ) {
	echo '
		body.boxed{
			padding-top: ' . $newsider_theme_boxed_margin . 'px;
			padding-bottom: ' . $newsider_theme_boxed_margin . 'px;
		}
	';
} ?>

/* Menu */
.header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li > a{
	font-family: <?php echo $newsider_menu_primary_font_family; ?> ;
	text-transform: <?php echo $newsider_menu_primary_transform; ?> ;
	font-weight: <?php echo $newsider_menu_primary_weight; ?> ;
	font-size: <?php echo $newsider_menu_primary_size; ?> ;
	letter-spacing: <?php echo $newsider_menu_primary_spacing; ?> ;
}
.header_wrap.desktop .menu-primary-menu-container-wrap .sub-menu li a{
	font-family: <?php echo $newsider_submenu_primary_font_family; ?> ;
	text-transform: <?php echo $newsider_submenu_primary_transform; ?> ;
	font-weight: <?php echo $newsider_submenu_primary_weight; ?> ;
	font-size: <?php echo $newsider_submenu_primary_size; ?> ;
	letter-spacing: <?php echo $newsider_submenu_primary_spacing; ?> ;
}

.header_wrap.desktop .menu-primary-menu-container-wrap li:hover a,
.header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li:hover > a,
.header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current_page_item > a,
.header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-item > a,
.header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-parent > a,
.header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-ancestor > a,
body.header_type2 .header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li:hover a,
body.header_type2 .header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current_page_item > a,
body.header_type2 .header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-item > a,
body.header_type2 .header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-parent > a,
body.header_type2 .header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-ancestor > a,
body.header_type4 .header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li:hover a,
body.header_type4 .header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current_page_item > a,
body.header_type4 .header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-item > a,
body.header_type4 .header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-parent > a,
body.header_type4 .header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-ancestor > a,
.header_wrap.desktop .menu-primary-menu-container-wrap ul li.menu-item-has-children:after{
	color: <?php echo $newsider_theme_color; ?> ;
}
.header_wrap.desktop .menu-primary-menu-container-wrap li:hover:after,
.header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li:hover:after,
.header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current_page_item:after,
.header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-item:after,
.header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-parent:after,
.header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-ancestor:after{
	color: <?php echo $newsider_theme_color; ?> ;
}
.header_wrap.desktop .menu-primary-menu-container-wrap .sub-menu li a:after{
	background-color: <?php echo $newsider_theme_color; ?> ;
}
.header_wrap.desktop .menu-primary-menu-container-wrap ul.nav-menu > li a:before,
.header_wrap.desktop .menu-primary-menu-container-wrap .sub-menu li a:after{
	background-color: <?php echo $newsider_theme_color; ?> ;
}
.header_wrap .menu_btn:hover b{
	color: <?php echo $newsider_theme_color; ?> ;
}
.header_wrap .menu_btn:hover span,
body.menu_active .header_wrap .menu_btn:hover span{
	background-color: <?php echo $newsider_theme_color; ?> ;
}
.header_search .search_form{
	border-bottom: 1px solid <?php echo $newsider_theme_color; ?> ;
}
.header_search i.fa-search:hover,
body.header_type2 .header_search i.fa-search:hover,
body.header_type3 .header_search i.fa-search:hover,
body.header_type4 .header_search i.fa-search:hover{
	color: <?php echo $newsider_theme_color; ?> ;
}
.cstheme_mega_menu_wrap .category-children-item .category_post_title a:hover{
	color: <?php echo $newsider_theme_color; ?> !important;
}


/* Mobile Menu */
.header_wrap.mobile .menu-primary-menu-container-wrap li a:hover,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu > li.current_page_item > a,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-item > a,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-parent > a,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-ancestor > a,
.header_wrap.mobile .menu-primary-menu-container-wrap li a:hover:after,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu > li > a:hover:after,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu > li.current_page_item > a:after,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-item > a:after,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-parent > a:after,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu > li.current-menu-ancestor > a:after,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu > li > a:hover:after,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu ul.sub-menu > li.current_page_item > a:after,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu ul.sub-menu > li.current-menu-item > a:after,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu ul.sub-menu > li.current-menu-parent > a:after,
.header_wrap.mobile .menu-primary-menu-container-wrap ul.nav-menu ul.sub-menu > li.current-menu-ancestor > a:after,
.header_wrap.mobile .menu-primary-menu-container-wrap ul ul.sub-menu li a:hover,
.header_wrap.mobile .menu-primary-menu-container-wrap ul ul.sub-menu li.current_page_item a,
.header_wrap.mobile .menu-primary-menu-container-wrap ul ul.sub-menu li a:hover,
.header_wrap.mobile .menu-primary-menu-container-wrap ul ul.sub-menu li.current_page_item > a,
.header_wrap.mobile .menu-primary-menu-container-wrap ul ul.sub-menu li.current-menu-item > a,
.header_wrap.mobile .menu-primary-menu-container-wrap ul ul.sub-menu li.current-menu-parent > a,
.header_wrap.mobile .menu-primary-menu-container-wrap ul ul.sub-menu li.current-menu-ancestor > a{
	color: <?php echo $newsider_theme_color; ?> !important;
}


/* Shortcodes */
.btn:hover,
.btn:focus,
.btn-default.active,
.btn-default.focus,
.btn-default:active,
.btn-default:focus,
.btn-default:hover,
.btn-primary,
.btn-primary.active,
.btn-primary.focus,
.btn-primary:active,
.btn-primary:focus{
	background-color: <?php echo $newsider_theme_color; ?> ;
}

/* Custom Colors */
a:hover, a:focus, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover{ color: <?php echo $newsider_theme_color; ?> ; }
.single-post-content-wrap > p > a, .contentarea > p > a{
	text-decoration:underline;
	color: <?php echo $newsider_theme_color; ?> ;
}
::selection{ background: <?php echo $newsider_theme_color; ?> ; color:#fff; }
::-moz-selection{ background: <?php echo $newsider_theme_color; ?> ; color:#fff; }
.theme_color{ color: <?php echo $newsider_theme_color; ?> ; }
.bg_primary{ background-color: <?php echo $newsider_theme_color; ?> ; }
button, input[type='button'], input[type='reset'], input[type='submit'], input[type='button'], input[type='reset']{ font-family: <?php echo $newsider_body_font_family; ?> , Arial, Helvetica, sans-serif; }
button, input[type='button'], input[type='reset'], input[type='submit'], button:hover, input[type='button']:hover, input[type='reset']:hover, input[type='submit']:hover, input[type='button']:hover, input[type='reset']:hover{ background-color: <?php echo $newsider_theme_color; ?> ; }
.widget .mc4wp-form input[type='submit'],
.evapb-column .mc4wp-form input[type='submit'],
.contentarea form.wpcf7-form input[type='submit']{
	background-color: <?php echo $newsider_theme_color; ?> ;
}
.cstheme_widget_last_tweets .twitter-text a{ color: <?php echo $newsider_theme_color; ?> ; }
.cstheme_widget_sociallinks .social_link:hover span,
.cstheme_widget_sociallinks .social_link i,
.recent_posts_list.carousel .recent_post_title a:hover{
	color: <?php echo $newsider_theme_color; ?> ;
}
body.header_type3 .header_menu_wrap .social_links a i:last-child{
	color: <?php echo $newsider_theme_color; ?> !important;
}
.eva-pagination .page-numbers:hover{
	border-color: <?php echo $newsider_theme_color; ?> ;
}
.eva-pagination .page-numbers.current{
	border-color: <?php echo $newsider_theme_color; ?> ;
	background-color: <?php echo $newsider_theme_color; ?> ;
}
.eva-infinite-scroll a:hover{
	background-color: <?php echo $newsider_theme_color; ?> ;
}
.single_post_meta_tags a:hover, .tagcloud a:hover{ border-color: <?php echo $newsider_theme_color; ?> ; color: <?php echo $newsider_theme_color; ?> ; }
.single_post_meta_tags a:hover:before{ border-color: <?php echo $newsider_theme_color; ?> ; }
.post_format_content .post-link:before, .post_format_content .post-quote:before{ background-color: <?php echo $newsider_theme_color; ?> ; }
#categories_list .item a{ background-color: <?php echo $newsider_theme_color; ?> ; }
#categories_list .item a:hover span{ color: <?php echo $newsider_theme_color; ?> ; }
#related_posts_list.owl-carousel .post-title:hover a{ color: <?php echo $newsider_theme_color; ?> ; }
#posts_carousel .owl-carousel .posts_carousel_title:hover a{ color: <?php echo $newsider_theme_color; ?> ; }
#posts_carousel .with_title .owl-controls .owl-nav > div{ border-color: <?php echo $newsider_theme_color; ?> ; color: <?php echo $newsider_theme_color; ?> ; }
#related_posts_list .owl-carousel .post-title a:hover{ color: <?php echo $newsider_theme_color; ?> ; }
.categories_carousel .item:hover .overlay_border:before, .categories_carousel .item:hover .overlay_border:after{ border-color: <?php echo $newsider_theme_color; ?> ; }
.widget_meta li a:hover, .widget_archive li a:hover, .widget_categories li a:hover{ color: <?php echo $newsider_theme_color; ?> ; }
#fixed_social_links a:hover span,
#fixed_social_links a i,
#right_fixed_panel a:hover span,
#right_fixed_panel a i,
.recent_posts_list.carousel .recent_post_title a:hover,
#search-result-list .post-title a:hover{
	color: <?php echo $newsider_theme_color; ?> ;
}
#categories_list .item a .cat_title:after{ background-color: <?php echo $newsider_theme_color; ?> ; }
footer form.mc4wp-form input[type='submit']{ background-color: <?php echo $newsider_theme_color; ?> ; }
.page_featured_image:before,
.page_featured_image:after{
	background-color: <?php echo $newsider_theme_color; ?> ;
}
.header_search_icon:hover i{ color: <?php echo $newsider_theme_color; ?> ; }
#loader span{
	border-top-color: <?php echo $newsider_theme_color; ?> ;
	border-bottom-color: <?php echo $newsider_theme_color; ?> ;
}
.widget_text .info_block{
	border-left-color: <?php echo $newsider_theme_color; ?> ;
}
.widget .apsc-icons-wrapper .apsc-each-profile,
.widget .apsc-icons-wrapper .apsc-count{
	font-family: <?php echo $newsider_body_font_family; ?> , Arial, Helvetica, sans-serif !important;
}


/* Slider */
.owl-controls .owl-dot:hover,
.owl-controls .owl-dot.active{
	background-color: <?php echo $newsider_theme_color; ?> ;
}


/* Page Title */
#pagetitle{
	<?php echo $newsider_pagetitle_bg_styles; ?>
}
#pagetitle h1,
.breadcrumbs_wrap span,
.breadcrumbs_wrap a{
	color: <?php echo $newsider_pagetitle_text_color; ?> ;
}


/* Widgets */
aside h4.widget-title:before,
.widget_meta li a:hover,
.widget_archive li a:hover,
.widget_categories li a:hover,
.newsider_widget_sociallinks .social_links_wrap a:hover i,
.recent_posts_list.grid .recent_post_title:hover a,
.recent_posts_list.big_first_post .recent_post_title a:hover,
.widget_recent_entries li a:hover,
.widget_pages li a:hover,
.widget_nav_menu li a:hover,
.widget_rss .rsswidget:hover{
	color: <?php echo $newsider_theme_color; ?> ;
}
.tagcloud a:hover{
	color: <?php echo $newsider_theme_color; ?> !important;
}


/* Blog */
.single_sharebox_wrap .cstheme_likes:hover i,
.single_sharebox_wrap .cstheme_likes.already_liked i{
	color: <?php echo $newsider_theme_color; ?> ;
	border-color: <?php echo $newsider_theme_color; ?> ;
}
.widget_categories li .val,
.eva-pagination .page-numbers span{ color: <?php echo $newsider_theme_color; ?> ; }
.single_post_meta_category:before{ background-color: <?php echo $newsider_theme_color; ?> ; }
.single_sharebox_wrap a:hover,
.evapb-bloglist.bg_image .post_descr_wrap .post_title a:hover,
.evapb-bloglist.card .format-quote .post_title a:hover,
.evapb-bloglist.card .format-link .post_title a:hover,
.evapb-bloglist.top_img_masonry .format-quote .post_title a:hover,
.evapb-bloglist.top_img_masonry .format-link .post_title a:hover,
.evapb-bloglist.big_bg_images .post_meta a:hover,
.evapb-bloglist.big_bg_images .post_title a:hover,
.evapb_blog_carousel.listing_hover .active .post_title a,
.evapb_blog_carousel.small_carousel .post_content_wrapper:hover .post_title a,
.single_post_nav .prev_post_thumb h6:hover,
.single_post_nav .next_post_thumb h6:hover,
.single_post_nav .prev_post_arrow:hover i,
.single_post_nav .next_post_arrow:hover i,
.single_post_meta span.post_meta_category,
.evapb-bloglist.card_clean .post.format-standard .post_meta_author:hover b,
.evapb-bloglist.card_clean .post.format-image .post_meta_author:hover b,
.evapb-bloglist.top_img.style2 .format-link .pf_link_url:hover{
	color: <?php echo $newsider_theme_color; ?> ;
}
#blog-single-wrap.format-quote h2.single-post-title:after,
#blog-single-wrap.format-link h2.single-post-title:after,
h6.comment_author:after,
.evapb-bloglist.left_image .post_meta .post_meta_date:after,
#related_posts_list .post_meta .post_meta_date:after,
#blog_list.default .post_meta .post_meta_date:after,
.single_post_meta .post_meta_date:after,
#related_posts_list .posts_carousel_wrap:before,
#commentform .form-submit input[type='submit'],
#author-info:before,
.top_slider.style1 .post_meta span.post_meta_date:after,
.top_slider.style2 .post_meta span.post_meta_date:after,
.top_slider.style2 .post_meta span.post_meta_author:after,
.top_slide_wrap.style3 .post_meta span.post_meta_date:after,
.top_slider.style4 .post_meta span.post_meta_date:after,
.top_slider.style5 .post_meta span.post_meta_date:after,
.top_slider.style5 .post_meta span.post_meta_author:after,
.evapb-bloglist.card .format-quote .quote_author_name:before,
.evapb-bloglist.card .format-link .pf_link_url:before,
.evapb-bloglist.top_img_masonry .format-quote .quote_author_name:before,
.evapb-bloglist.top_img_masonry .format-link .pf_link_url:before,
.evapb-bloglist.big_bg_images .post_meta span.post_meta_date:after,
.evapb-bloglist.big_bg_images .post_content_wrapper .btn_more:hover,
.breadcrumbs_wrap span.sep:after,
#blog_list.default article.sticky .post_content_wrapper:before,
.evapb-bloglist.card_clean .post.format-quote .post_content_wrapper,
.evapb-bloglist.top_img.style2 .format-quote .quote_author_name:before,
.evapb-bloglist.top_img.style2 .format-link .pf_link_url:before{
	background-color: <?php echo $newsider_theme_color; ?> ;
}
.evapb-bloglist.big_bg_images .btn_more,
.evapb-bloglist.big_bg_images .post_content_wrapper .btn_more:hover,
.evapb_blog_carousel.listing_hover .active .post_content_wrapper,
.evapb_blog_carousel.small_carousel .post_content_wrapper:hover .post_descr_wrap,
.evapb-bloglist.card .format-quote .post_content_wrapper:hover,
.evapb-bloglist.card .format-link .post_content_wrapper:hover{
	border-color: <?php echo $newsider_theme_color; ?> ;
}


/* Evatheme Page Builder */
.cstheme-accordion .active .accordion-heading .accordion-toggle{
	border-bottom-color: <?php echo $newsider_theme_color; ?> ;
}
.cstheme-accordion .accordion-toggle:hover i,
.cstheme-accordion .active .accordion-toggle i,
.cstheme-progress.light .bar_percent,
.cstheme-testimonials .testimonials_author a:hover,
.cstheme-accordion .active .accordion-heading .accordion-toggle,
.cstheme-counter .counters-box .stat_count{
	color: <?php echo $newsider_theme_color; ?> ;
}
.cstheme-pricing .pricing-feature li:before,
.cstheme-testimonials .testimonial-item:before,
.evapb_top_slider .flex-control-paging li a:hover,
.evapb_top_slider .flex-control-paging li a.flex-active,
.cstheme-pricing .pricing-box:before{
	background-color: <?php echo $newsider_theme_color; ?> ;
}


/* Mailchimp Widget */
<?php echo $newsider_mc4wp_styles; ?>

<?php echo $newsider_options_custom_css; ?>