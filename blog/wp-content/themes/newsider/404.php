<?php
/**
 * The template for displaying 404 pages (not found)
 */

get_header(); ?>
	
	<div id="error404_container" class="text-center">
		<div class="container">
			<h1><?php echo esc_html__('404', 'newsider'); ?></h1>
			<h2><?php echo esc_html__('Oops...Page not found!', 'newsider'); ?></h2>
			<div class="clearfix"></div>
			<a class="btnback theme_color" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_html_e('back to home page', 'newsider'); ?></a>
		</div>
	</div>

<?php get_footer(); ?>
