<?php
/**
 * The sidebar containing the main widget area
 */

$newsider_sidebar = 'blog-sidebar';

		echo '<div id="blog_sidebar">';
			
			if ( is_active_sidebar( $newsider_sidebar ) ) {
				dynamic_sidebar( $newsider_sidebar );
			}
			
		echo '</div>';