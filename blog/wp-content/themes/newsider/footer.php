<?php
/**
 * The template for displaying the footer
 */

global $post;

$newsider_postId = ( isset( $post->ID ) ? get_the_ID() : NULL );

$newsider_footer_instagram = get_post_meta( $newsider_postId, 'newsider_footer_instagram', true );
$newsider_footer_posts_carousel = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'footer_posts_carousel' ) : '0';
$newsider_prefooter = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'prefooter' ) : 'hide';
?>

		</div><!-- //page-content -->
		
		<?php if( newsider_options('footer_instagram_username') != '' && $newsider_footer_instagram != 'hide' ){ ?>
			<?php get_template_part( 'framework/instagram' ); ?>
		<?php } ?>
		
		<footer>
			<div class="container">
				<?php if( $newsider_footer_posts_carousel != '0' ) {
					get_template_part('templates/blog/footer-posts-carousel');
				} ?>
				
				<!-- Prefooter Area -->
				<?php if( $newsider_prefooter == 'show' ) { ?>
					
					<div id="prefooter_area">
						<div class="row">
						
							<?php
								$newsider_widgets_grid = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'prefooter_col' ) : '4-4-4';
								$i = 1;
								foreach (explode('-', $newsider_widgets_grid) as $newsider_widgets_g) {
									echo '<div class="col-md-' . esc_attr( $newsider_widgets_g ) . ' col-' . esc_attr( $i ) . '">';
										dynamic_sidebar("footer-area-$i");
									echo '</div>';
									$i++;
								}
							?>
							
						</div>
					</div>
					
				<?php } ?>
				
				<?php if( !class_exists( 'ReduxFrameworkPlugin' ) ){ ?>
					<div class="copyright_block ReduxFrameworkDisable"><?php echo esc_html__('Copyright © 2017 newsider Theme', 'newsider'); ?></div>
				<?php } ?>
				
			</div>
		</footer>
	</div><!-- //Page Wrap -->

<?php wp_footer(); ?>

<script>
	jQuery(document).ready(function(){
		
		jQuery('.qtranxs_language_chooser a').append('<i class="fa fa-chevron-down" aria-hidden="true"></i>');
		
	});
</script>

</body>
</html>