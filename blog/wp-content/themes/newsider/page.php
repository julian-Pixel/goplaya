<?php
/**
 * The template for displaying pages
 */

get_header();

global $post;

$newsider_postId = ( isset( $post->ID ) ? get_the_ID() : NULL );

$newsider_sidebar_layout = get_post_meta( $newsider_postId, 'newsider_page_sidebar', true );
if( $newsider_sidebar_layout == 'left-sidebar' ) {
	$newsider_sidebar_class = 'pull-left';
	$newsider_page_content_class = 'col-md-9 pull-right ';
} elseif( $newsider_sidebar_layout == 'right-sidebar' ) {
	$newsider_sidebar_class = 'pull-right ';
	$newsider_page_content_class = 'col-md-9 pull-left ';
} else {
	$newsider_sidebar_class = '';
	$newsider_page_content_class = 'col-md-12 ';
}
?>
		
		<div id="default_page" class="<?php echo esc_attr( $newsider_sidebar_layout ); ?>">
			<div class="container">
				<div class="row">
					<div class="<?php echo $newsider_page_content_class ?>">
						<div class="contentarea clearfix">
					
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							
								<?php the_content(esc_html__('Read more!', 'newsider')); ?>
								
								<?php wp_link_pages(array('before' => '<div class="page-link">' . esc_html__('Pages', 'newsider') . ': ', 'after' => '</div>')); ?>
								
								<?php 
									if ( comments_open() || get_comments_number() ) :
										comments_template();
									endif;
								?>
						
							<?php endwhile; ?>
							
							<?php endif; ?>
					
						</div>
					</div>
					
                    
					<?php 
						if( $newsider_sidebar_layout == 'left-sidebar' || $newsider_sidebar_layout == 'right-sidebar' ) {
							echo '<div class="col-md-3 ' . $newsider_sidebar_class . '">';
								dynamic_sidebar('pagedefault-sidebar');
							echo '</div>';
						}
					?>
					
				</div>
               
			</div>
		</div>

<?php get_footer(); ?>

<script>
	jQuery(document).ready(function(){
		<?php if(qtranxf_getLanguage() == "es") { ?>
			jQuery('.banner-top-home').show();
			jQuery('.banner-medio-home').show();
			jQuery('.banner-bottom-home').show();
		<?php } ?>
		
		<?php if(qtranxf_getLanguage() == "en") { ?>
			jQuery('.banner-top-home-en').show();
			jQuery('.banner-medio-home-en').show();
			jQuery('.banner-bottom-home-en').show();
		<?php } ?>
		
	});
</script>