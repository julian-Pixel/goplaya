<?php

$newsider_header_search = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'header_search' ) : 1;
?>

		<div class="header_wrap mobile">
			<div class="header_mobile_wrap clearfix">
				<?php newsider_logo(); ?>
				<div class="header_right_part text-right">

					<?php if( $newsider_header_search != 0 ) { ?>
						<div class="header_search pull-right">
							<?php get_search_form(true) ?>
						</div>
					<?php } ?>

                    <?php echo my_qtranxf_generateLanguageSelectCode()?>

                    <a class="mobile_menu_btn" href="javascript:void(0)">
						<span class="menu_line1"></span><span class="menu_line2"></span><span class="menu_line3"></span>
					</a>
				</div>
			</div>
			<div class="menu-primary-menu-container-wrap">

				<?php

				if(qtranxf_getLanguage() == "es") {
							$lm = 'primary';
				}

				if(qtranxf_getLanguage() == "en") {
							$lm = 'ingles';
				}
 ?>
				<?php wp_nav_menu( array( 'menu_class' => 'nav-menu', 'theme_location' => $lm ) ); ?>
			</div>
		</div>
