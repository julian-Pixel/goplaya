<?php

$newsider_header_type = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'header_type' ) : 'type3';
$newsider_header_adv = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'header_adv' ) : '';
$newsider_header_social_icons = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'header_social_icons' ) : 0;
$newsider_header_search = class_exists( 'ReduxFrameworkPlugin' ) ? newsider_options( 'header_search' ) : 1;

$newsider_header_has_adv = '';
if ( !empty( $newsider_header_adv ) ) {
	$newsider_header_has_adv = 'header_has_adv';
}
?>

		<?php /* if ( $newsider_header_type == 'type2' ) { ?>

			<div class="header_wrap desktop">
				<div class="container">
					<div class="header_logo_wrap <?php echo esc_attr( $newsider_header_has_adv ); ?>">
						<?php newsider_logo(); ?>
						<div class="header_adv text-right">
							<?php echo newsider_options( 'header_adv' ); ?>
						</div>
					</div>
				</div>
				<div class="header_menu_container">
					<div class="header_menu_wrap">
						<div class="container">
							<div class="menu-primary-menu-container-wrap">
								<?php wp_nav_menu( array( 'menu_class' => 'nav-menu clearfix', 'theme_location' => 'primary', 'depth' => 3, 'container' => false, 'fallback_cb' => 'newsider_Walker_Nav_Menu_Edit_Custom::fallback', 'walker' => new newsider_MegaMenu_Walker ) ); ?>
							</div>
							<?php if( $newsider_header_search != 0 ) { ?>
								<div class="header_search pull-right">
									<?php get_search_form(true) ?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>

		<?php } elseif ( $newsider_header_type == 'type3' ) { ?>

			<div class="header_wrap desktop">
				<div class="container">
					<div class="header_logo_wrap <?php echo esc_attr( $newsider_header_has_adv ); ?>">
						<?php newsider_logo(); ?>
						<div class="menu-primary-menu-container-wrap">
								<?php wp_nav_menu( array( 'menu_class' => 'nav-menu clearfix', 'theme_location' => 'primary', 'depth' => 3, 'container' => false, 'fallback_cb' => 'newsider_Walker_Nav_Menu_Edit_Custom::fallback', 'walker' => new newsider_MegaMenu_Walker ) ); ?>
							</div>
					</div>
				</div>
				<div class="header_menu_container">
					<div class="header_menu_wrap">
						<div class="container">

							<?php if( $newsider_header_search != 0 ) { ?>
								<div class="header_search pull-right">
									<?php get_search_form(true) ?>
								</div>
							<?php } ?>
							<?php if( $newsider_header_social_icons != 0 ) { ?>
								<div class="social_links_wrap pull-right">
									<?php echo newsider_social_links(); ?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>

		<?php } elseif ( $newsider_header_type == 'type4' ) { ?>

			<div class="header_wrap desktop">
				<div class="header_menu_container">
					<div class="header_menu_wrap">
						<div class="container">
							<div class="menu-primary-menu-container-wrap">
								<?php wp_nav_menu( array( 'menu_class' => 'nav-menu clearfix', 'theme_location' => 'primary', 'depth' => 3, 'container' => false, 'fallback_cb' => 'newsider_Walker_Nav_Menu_Edit_Custom::fallback', 'walker' => new newsider_MegaMenu_Walker ) ); ?>
							</div>
							<?php if( $newsider_header_search != 0 ) { ?>
								<div class="header_search pull-right">
									<?php get_search_form(true) ?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="header_logo_wrap <?php echo esc_attr( $newsider_header_has_adv ); ?>">
						<?php newsider_logo(); ?>
						<div class="header_adv text-right">
							<?php echo newsider_options( 'header_adv' ); ?>
						</div>
					</div>
				</div>
			</div>



		<?php } else { /*/?>

			<div class="header_wrap desktop">

            <div class="container-fluid">
					<div class="header_logo_wrap <?php echo esc_attr( $newsider_header_has_adv ); ?>">
						<?php newsider_logo(); ?>
						<div class="header_adv text-right">
							<?php echo newsider_options( 'header_adv' ); ?>
						</div>
					</div>

                    <div class="main-menu">

						<?php



					if(qtranxf_getLanguage() == "es") {
								$lm = 'primary';
					}

					if(qtranxf_getLanguage() == "en") {
								$lm = 'ingles';
					}


						wp_nav_menu( array( 'menu_class' => 'nav-menu clearfix', 'theme_location' => $lm , 'depth' => 3, 'container' => false, 'fallback_cb' => 'newsider_Walker_Nav_Menu_Edit_Custom::fallback', 'walker' => new newsider_MegaMenu_Walker ) );



						?>
                    </div>

                    <?php if( $newsider_header_search != 0 ) { ?>
                        <div class="header_search pull-right">
                            <?php get_search_form(true) ?>
                        </div>
                    <?php } ?>

                    <?php echo my_qtranxf_generateLanguageSelectCode()?>

				</div>
				<!--div class="header_tagline_wrap">
					<div class="container">
						<?php /*
							if ( has_nav_menu( 'topmenu' ) ) {
								wp_nav_menu( array( 'menu_class' => 'topmenu-menu pull-left', 'theme_location' => 'topmenu', 'depth' => -1, 'container' => false, 'fallback_cb' => false ) );
							}
						?>
						<?php if( $newsider_header_social_icons != 0 ) { ?>
							<div class="social_links_wrap pull-right">
								<?php echo newsider_social_links(); ?>
							</div>
						<?php } */?>
					</div>
				</div-->
				<div class="header_menu_container">
					<div class="header_menu_wrap">
						<div class="container">


						</div>
					</div>
				</div>

			</div>

		<?php // } ?>
