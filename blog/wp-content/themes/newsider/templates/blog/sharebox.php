<?php

$newsider_single_post_sharebox_facebook = newsider_options('single_post_sharebox_facebook') ? newsider_options('single_post_sharebox_facebook') : '1';
$newsider_single_post_sharebox_twitter = newsider_options('single_post_sharebox_twitter') ? newsider_options('single_post_sharebox_twitter') : '1';
$newsider_single_post_sharebox_google = newsider_options('single_post_sharebox_google') ? newsider_options('single_post_sharebox_google') : '1';
$newsider_single_post_sharebox_pinterest = newsider_options('single_post_sharebox_pinterest') ? newsider_options('single_post_sharebox_pinterest') : '1';
?>

<div class="sharebox clearfix">
	<h5><?php _e("<!--:en-->Share<!--:--><!--:es-->Compartir<!--:-->");?></h5>
	<div class="sharebox_links text-right">
		<?php if( $newsider_single_post_sharebox_facebook != '0' ) { ?>
			<a class="social_link facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php echo str_replace(' ', '+', the_title('', '', false)); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-facebook"></i></a>
		<?php } ?>
		<?php if( $newsider_single_post_sharebox_twitter != '0' ) { ?>
			<a class="social_link twitter" href="https://twitter.com/intent/tweet?text=<?php echo htmlspecialchars(urlencode(html_entity_decode( get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8'); ?>&url=<?php echo urlencode( esc_url( get_permalink() ) ); ?>&via=<?php echo urlencode( get_bloginfo( 'name' ) ); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-twitter"></i></a>
		<?php } ?>
		<?php if( $newsider_single_post_sharebox_google != '0' ) { ?>
			<a class="social_link google-plus" href="http://plus.google.com/share?url=<?php the_permalink() ?>&amp;title=<?php echo str_replace(' ', '+', the_title('', '', false)); ?>" title="<?php esc_html_e( 'Google+', 'newsider') ?>" onclick="javascript:window.open(this.href,'','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-google-plus"></i></a>
		<?php } ?>
		<?php if( $newsider_single_post_sharebox_pinterest != '0' ) { ?>
			<?php $newsider_featured_image_url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID())); ?>
			<a class="social_link pinterest" href="http://pinterest.com/pin/create/button/?url=<?php echo get_permalink(); ?>&media=<?php echo (strlen($newsider_featured_image_url[0]) > 0) ? $newsider_featured_image_url[0] : get_option( 'newsider_logo' ); ?>" onclick="javascript:window.open(this.href,'','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-pinterest-p"></i></a>
		<?php } ?>
	</div>
</div>