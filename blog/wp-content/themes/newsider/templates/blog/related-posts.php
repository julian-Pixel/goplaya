<?php

global $post;

if( class_exists( 'ReduxFrameworkPlugin' ) && ( newsider_options('single_post_related_posts_title') != '' ) ) {
	$newsider_single_post_related_posts_title = newsider_options('single_post_related_posts_title');
	//$newsider_single_post_related_posts_title = _e("<!--:en-->Related Posts<!--:--><!--:es-->Artículos Relacionados<!--:-->");
} else if( ! class_exists( 'ReduxFrameworkPlugin' ) ){
	$newsider_single_post_related_posts_title = _e("<!--:en-->Related Posts<!--:--><!--:es-->Artículos Relacionados<!--:-->");
}
?>
	
	<?php

		$categories = get_the_category($post->ID);
		
		if ($categories) {

			$category_ids = array();

			foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
			
			$args = array(
				'category__in' 		=> $category_ids,
				'post__not_in' 		=> array($post->ID),
				'posts_per_page' 	=> 2,
				'ignore_sticky_posts' => 1,
				'orderby' 			=> 'rand'
			);
			
			$my_query = new WP_Query($args);
	
			if( $my_query->have_posts() ) {
	
				echo '<div id="related_posts_list">
					<h2>' . esc_html( $newsider_single_post_related_posts_title ) . '</h2>
						<div class="row">';
					
							while ($my_query->have_posts()) : $my_query->the_post();
					
								get_template_part('templates/blog/loop');
								
							endwhile;
							
							wp_reset_postdata();
					
						echo '</div>
					</div>';
			}
		}
	?>