<?php
/**
 * The blog post content
 */

global $post;

$newsider_featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
$newsider_featured_image = '<img class="lazy" data-original="' . newsider_aq_resize( $newsider_featured_image_url, 440, 440, true, true, true) . '" width="440" height="440" alt="' . get_the_title() . '" />';
$newsider_post_excerpt = (newsider_smarty_modifier_truncate(get_the_excerpt(), 150));
$newsider_date_format = get_option( 'date_format' );
?>
		
		<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-12'); ?>>
			<div class="post_content_wrapper">
				<div class="row">
				<?php if( has_post_thumbnail( $post->ID ) ) { ?>
					<div class="col-sm-6 post_fetured_img_wrap">
						<div class="post_format_content">
							<?php echo '<a href="' . get_permalink() . '">' . $newsider_featured_image . '</a>'; ?>
							<div class="post_share_wrap">
								<i class="post_share_btn"></i>
								<?php get_template_part( 'templates/blog/sharebox' ); ?>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
				<?php } else { ?>
					<div class="col-sm-12">
				<?php } ?>
						<div class="post_descr_wrap clearfix">
							<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
							<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
							<div class="post_meta clearfix">
								<span class="post_meta_date"><?php the_time( esc_html( $newsider_date_format ) ); ?></span>
								<span class="post_metaauthor"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
							</div>
							<div class="post-content clearfix">
								<?php echo $newsider_post_excerpt; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>