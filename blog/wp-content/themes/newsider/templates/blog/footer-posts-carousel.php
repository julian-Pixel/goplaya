<?php

global $post;

$newsider_footer_posts_carousel_title = newsider_options( 'footer_posts_carousel_title' );
$newsider_footer_posts_carousel_categories = newsider_options( 'footer_posts_carousel_categories' );
$newsider_date_format = get_option( 'date_format' );
?>

<div id="footer_posts_carousel">
	
	<?php if( !empty( $newsider_footer_posts_carousel_title ) ) { ?>
		<h5><?php echo esc_html( $newsider_footer_posts_carousel_title ); ?></h5>
	<?php } ?>
	
	<?php

		$args = array(
			'post_type' 	=> 'post',
			'cat' 			=> $newsider_footer_posts_carousel_categories,
			'orderby' 		=> 'date',
			'post_status' 	=> 'publish',
		);
		
	?>
	
	<div class="footer_posts_carousel_list owl-carousel clearfix">

		<?php
			
			$my_query = new WP_Query($args);
			
			if( $my_query->have_posts() ) {
				
				while ($my_query->have_posts()) : $my_query->the_post();
				
					$featured_image_url = wp_get_attachment_url(get_post_thumbnail_id());
					$featured_image = '<img src="' . newsider_aq_resize( $featured_image_url, 270, 130, true, true, true ) . '" alt="' . get_the_title() . '" width="270" height="130" />';
					
					$categories = get_the_category($post->ID);
					$categories_slug = $categories[0]->slug;
					?>
				
						<div class="item">
							<div class="footer_posts_carousel_item category-<?php echo $categories_slug; ?>">
								<?php if( !empty( $featured_image_url ) ) { ?>
									<a class="post_format_content" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php echo $featured_image ?></a>
								<?php } ?>
								<div class="footer_posts_carousel_descr">
									<h5 class="footer_posts_carousel_title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
									<span class="footer_posts_carousel_date"><?php the_time( esc_html( $newsider_date_format ) ); ?></span>
								</div>
							</div>
						</div>
						
					<?php
	
				endwhile;
				
				wp_reset_postdata();
				
			}
		?>
		
	</div>
</div>