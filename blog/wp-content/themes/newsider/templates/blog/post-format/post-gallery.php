<?php

global $post;

$newsider_postid = get_the_ID();

$blogsingle_gallery_style = get_post_meta($newsider_postid, 'newsider_blogsingle_gallery_style', true);

if ( is_single() && $blogsingle_gallery_style == 'slider' ) {
	$newsider_width = 1000;
	$newsider_height = 700;
} else {
	$newsider_width = 770;
	$newsider_height = 410;
}

$gallery_image_ids = get_post_meta($post->ID, 'gallery_image_ids', true);

if (!empty($gallery_image_ids)) {
	$my_posts_image_gallery = get_post_meta($newsider_postid, 'gallery_image_ids', true);
} else {
	// Backwards compat
	$attachment_ids = get_posts('post_parent=' . $newsider_postid . '&numberposts=-1&post_type=attachment&orderby=menu_order&order=ASC&post_mime_type=image&fields=ids');
	$attachment_ids = array_diff($attachment_ids, array(get_post_thumbnail_id()));
	$my_posts_image_gallery = implode(',', $attachment_ids);
}

$attachments = array_filter(explode(',', $my_posts_image_gallery));

foreach( $attachments as $imageID ) {
  $attachment_title = get_the_title($imageID);
}


    if ($attachments) {
		if ( $blogsingle_gallery_style == 'grid' ) {
			
			echo '<div class="post-slider grid clearfix">';
				foreach ($attachments as $attachment) {
					$attachment_title = get_the_title($attachment);
					$newsider_featured_image_url = wp_get_attachment_url($attachment);
					$newsider_featured_image_bg = newsider_aq_resize( $newsider_featured_image_url, $newsider_width, $newsider_height, true, true, true );
					$newsider_featured_image = '<img class="lazy" data-original="' . $newsider_featured_image_url . '" alt="' . $attachment_title . '" />';
					?>
					
					<a class="swipebox" href="<?php echo $newsider_featured_image_url; ?>" title="<?php echo $attachment_title; ?>">
						<?php echo $newsider_featured_image; ?>
						<span class="gallery_zoom"></span>
						<span class="caption"><?php echo $attachment_title; ?></span>
					</a>

				<?php  }
			echo '</div>';
			
		} elseif ( $blogsingle_gallery_style == 'justified' ) {
			
			echo '<div class="post-slider justified clearfix">';
				foreach ($attachments as $attachment) {
					$attachment_title = get_the_title($attachment);
					$newsider_featured_image_url = wp_get_attachment_url($attachment);
					$newsider_featured_image = '<img src="' . $newsider_featured_image_url . '" width="440" height="440" alt="' . $attachment_title . '" />';
					?>
					
					<a class="swipebox" href="<?php echo $newsider_featured_image_url; ?>" title="<?php echo $attachment_title; ?>">
						<?php echo $newsider_featured_image; ?>
						<span class="gallery_zoom"></span>
					</a>

				<?php  }
			echo '</div>';
			
		} else {
			
			echo '<div class="post-slider owl-carousel clearfix">';
				foreach ($attachments as $attachment) {
					$newsider_featured_image_url = wp_get_attachment_url($attachment);
					$newsider_featured_image_bg = newsider_aq_resize( $newsider_featured_image_url, $newsider_width, $newsider_height, true, true, true );
					$newsider_featured_image = '<img src="' . newsider_aq_resize( $newsider_featured_image_url, $newsider_width, $newsider_height, true, true, true ) . '" alt="" />';
					$attachment_title = get_the_title($attachment);
					?>
					
					<div class="item">
						<a class="swipebox" href="<?php echo $newsider_featured_image_url; ?>" title="<?php echo $attachment_title; ?>">
							<?php echo $newsider_featured_image; ?>
							<span class="gallery_zoom"></span>
							<span class="caption"><?php echo $attachment_title; ?></span>
						</a>
					</div>

				<?php  }
			echo '</div>';
			
		}
    }
?>