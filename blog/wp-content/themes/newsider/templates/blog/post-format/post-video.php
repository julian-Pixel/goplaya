<?php

global $post, $newsider_featured_image_url, $posts_style;

$newsider_width = 400;
$newsider_height = 400;
if( $posts_style == 'big_top_img' || is_single() ){
	$newsider_width = 770;
	$newsider_height = 410;
}
$newsider_post_embed = get_post_meta($post->ID, 'newsider_post_embed', true);
$newsider_featured_image_bg = newsider_aq_resize( $newsider_featured_image_url, $newsider_width, $newsider_height, true, true, true );
$is_youtube = substr_count($newsider_post_embed, "youtu");
$is_vimeo = substr_count($newsider_post_embed, "vimeo");

?>
	
	<?php if ( is_single() ) { ?>
		<div class="pf_video_wrap">
			<?php echo apply_filters( "the_content", htmlspecialchars_decode( $newsider_post_embed ) ) ?>
		</div>
	<?php } else { ?>	
		<?php if ( $is_vimeo > 0 || $is_youtube > 0 ) { ?>
			<div class="pf_video_wrap video_player" style="background-image:url(<?php echo $newsider_featured_image_bg ?>);">
				<div class="post_share_wrap">
					<i class="post_share_btn"></i>
					<?php get_template_part( 'templates/blog/sharebox' ); ?>
				</div>
				<a class="pf_video_play" href="javascript:void(0)"></a>
		<?php } else { ?>
			<div class="pf_video_wrap">
		<?php } ?>
				<?php echo apply_filters( "the_content", htmlspecialchars_decode( $newsider_post_embed ) ) ?>
			</div>
	<?php } ?>