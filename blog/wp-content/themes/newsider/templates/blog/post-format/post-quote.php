<?php 

$pf_quote_text = get_post_meta( $post->ID, 'newsider_post_quote_text', true );
$pf_quote_author = get_post_meta( $post->ID, 'newsider_post_quote_author', true );
?>

	<?php if( !empty($pf_quote_text) ){ ?>
		<div class="pf_quote_wrap">
			<i class="pf_quote"></i>
			<h2 class="pf_quote_title"><?php echo esc_html( $pf_quote_text ); ?></h2>
			<p class="pf_quote_author">
				<?php
					if ( !empty( $pf_quote_author ) ) {
						echo esc_attr( $pf_quote_author );
					} else {
						echo get_the_author_meta('display_name');
					}
				?>
			</p>
		</div>
	<?php } ?>