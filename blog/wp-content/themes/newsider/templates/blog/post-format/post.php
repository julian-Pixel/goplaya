<?php 

$newsider_featured_image_url = wp_get_attachment_url(get_post_thumbnail_id());
$newsider_featured_image = '<img class="lazy" data-original="' . $newsider_featured_image_url . '" width="770" height="410" alt="' . get_the_title() . '" />';
?>
	
	<?php if( !empty( $newsider_featured_image_url ) ) { ?>

		<div class="post-image">
			
			<?php if ( !is_single() ) { ?>
				
				<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
	
			<?php } ?>
		
				<?php echo $newsider_featured_image; ?>
			
			<?php if ( !is_single() ) { ?>
				
				</a>
			
			<?php } ?>
			
		</div>

	<?php } ?>