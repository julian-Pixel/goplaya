<?php

$pf_link_url = get_post_meta( $post->ID, 'newsider_post_link_url', true );
?>
	
	<?php if(!empty($pf_link_url)) { ?>
		<div class="pf_link_wrap">
			<i class="pf_link"></i>
			<a class="pf_link_url" href="<?php echo esc_url( $pf_link_url ); ?>"  target="_blank" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>"><?php echo esc_url( $pf_link_url ); ?></a>
		</div>
	<?php } ?>