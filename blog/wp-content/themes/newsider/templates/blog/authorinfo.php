<?php

	$newsider_authordesc = get_the_author_meta( 'description' );

	if ( ! empty ( $newsider_authordesc ) ) {
?>
	
	<div id="author-info">
		<div class="author-image">
			<a class="author-avatar" href="<?php echo esc_url( get_author_posts_url(get_the_author_meta( 'ID' )) ); ?>"><?php echo get_avatar( get_the_author_meta('user_email'), '100', '' ); ?></a>
		</div>
		<div class="author_name_wrap">
			<h4 class="author_name"><a href="<?php echo esc_url( get_author_posts_url(get_the_author_meta( 'ID' )) ); ?>"><?php echo esc_html__('Author say','newsider'); ?></a></h4>
			<?php if ( function_exists( 'evapb_author_social_networks' ) ) {
				echo evapb_author_social_networks();
			} ?>
		</div>
		<div class="author-bio">
			<?php echo the_author_meta('description'); ?>
		</div>
	</div>
	
<?php } ?>