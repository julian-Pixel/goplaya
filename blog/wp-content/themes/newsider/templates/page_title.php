<?php
/**
 * Page Title
 */

	global $post, $wp_query;
	
	$newsider_postId = ( isset( $post->ID ) ? get_the_ID() : NULL );
	
	$newsider_options_pagetitle = newsider_options('pagetitle') ? newsider_options('pagetitle') : 'show';
	$newsider_pagetitle = ( get_post_meta( $newsider_postId, 'newsider_pagetitle', true ) ? get_post_meta( $newsider_postId, 'newsider_pagetitle', true ) : $newsider_options_pagetitle );
	if ( is_home() || is_category() || is_tag() || is_search() || is_day() || is_month() || is_year() ) {
		$newsider_pagetitle = newsider_options('blog_pagetitle') ? newsider_options('blog_pagetitle') : 'show';
	} else if ( is_singular('post') ) {
		$newsider_pagetitle = 'hide';
	}
	
	$newsider_pagetitle_text		= get_post_meta( $newsider_postId, 'newsider_pagetitle_text', true );
	$newsider_blog_pagetitle_text	= newsider_options('blog_pagetitle_text') ? newsider_options('blog_pagetitle_text') : esc_html__('<!--:en-->Recent Posts<!--:--><!--:es-->Artículos Recientes<!--:-->', 'newsider');
	
	if ( $newsider_pagetitle_text != '' ) {
		$newsider_pagetitle_text = '<h1>' . esc_attr( $newsider_pagetitle_text ) . '</h1>';
	} else if (is_home()) {
		$newsider_pagetitle_text = '<h1>' . esc_attr( $newsider_blog_pagetitle_text ) . '</h1>';
	} else if (is_category()) {
		$newsider_pagetitle_text = '<h1>' . esc_attr__( '<!--:en-->Category<!--:--><!--:es-->Categoría<!--:-->:', 'newsider' ) . ' \'' . single_cat_title( '', false ) . '\'</h1>';
	} else if( is_tag() ) {
		$newsider_pagetitle_text = '<h1>' . esc_attr__( '<!--:en-->Tagged<!--:--><!--:es-->Etiquetado<!--:-->:', 'newsider' ) . ' \'' . single_tag_title( '', false ) . '\'</h1>';
	} else if( is_search() ) {
		$newsider_pagetitle_text = '<h1>' . $wp_query->found_posts . ' ' . esc_attr__( '<!--:en-->search results for<!--:--><!--:es-->resultados de búsqueda para<!--:-->', 'newsider' ) . ': \'' . esc_attr( get_search_query() ) . '\'</h1>';
	} else if ( is_day() || is_month() || is_year() ) {
		$newsider_pagetitle_text = '<h1>' . esc_attr__( '<!--:en-->Archive for<!--:--><!--:es-->Artículos para<!--:-->', 'newsider' ) . ' ' . get_the_time( get_option( 'date_format' ) ) . '</span></h1>';
	} else if (is_author()) {
		global $author;
        $userdata = get_userdata($author);
		$newsider_pagetitle_text = '<h1><span>' . esc_html__('<!--:en-->Articles posted by<!--:--><!--:es-->Artículos publicados por<!--:-->', 'newsider') . '</span>' . $userdata->display_name . '</h1>';
	} else if (is_404()) {
		$newsider_pagetitle_text = '<h1>' . esc_attr__( 'Error 404', 'newsider' ) . '</h1>';
	} else if (isset($_GET['paged']) && !empty($_GET['paged'])) {
		$newsider_pagetitle_text = '<h1>' . esc_attr__( '<!--:en-->Blog Archives<!--:--><!--:es-->Artículos del Blog<!--:-->', 'newsider' ) . '</h1>';
	} else {
		$newsider_pagetitle_text = '<h1>' . get_the_title() . '</h1>';
	}
	
	/* Parallax Effect */
	$newsider_pagetitle_bg_image_parallax = $newsider_pagetitle_bg_image_parallax_theme = $newsider_pagetitle_bg_image_parallax_page = $newsider_pagetitle_class = '';
	$newsider_pagetitle_bg_image_parallax_theme = get_post_meta( $newsider_postId, 'newsider_pagetitle_bg_image_parallax', true );
	if( newsider_options('pagetitle_bg_image_parallax') ) {
		$newsider_pagetitle_bg_image_parallax_page 	= newsider_options('pagetitle_bg_image_parallax');
	}
	$newsider_pagetitle_bg_image_parallax = ! empty( $newsider_pagetitle_bg_image_parallax_theme ) ? $newsider_pagetitle_bg_image_parallax_theme : $newsider_pagetitle_bg_image_parallax_page;
	if ( $newsider_pagetitle_bg_image_parallax == 'enable' ) {
		$newsider_pagetitle_class .= ' cstheme_parallax';
	}
	
	/* Breadcrumbs */
	$newsider_options_breadcrumbs = newsider_options('breadcrumbs') ? newsider_options('breadcrumbs') : 'show';
	if( is_search() ) {
		$newsider_options_breadcrumbs = 'hide';
	}
	$newsider_breadcrumbs = ( get_post_meta( $newsider_postId, 'newsider_breadcrumbs', true ) ? get_post_meta( $newsider_postId, 'newsider_breadcrumbs', true ) : $newsider_options_breadcrumbs );
	
?>
		
		<?php if ( $newsider_pagetitle == 'show' ) { ?>
			<div id="pagetitle" class="<?php echo esc_attr( $newsider_pagetitle_class ); ?>">
				<div class="pagetitle_content">
					<div class="container">
						<div class="row">
							<?php
								echo '<div class="col-md-8">' . $newsider_pagetitle_text . '</div>';
							
								if ( $newsider_breadcrumbs != 'hide' ) {
									echo '<div class="col-md-4">';
										newsider_breadcrumbs();
									echo '</div>';
								}
							?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		
		