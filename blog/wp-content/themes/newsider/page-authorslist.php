<?php
/*
 *	Template Name: Page - Authors List
 */

get_header(); ?>
		
		<div id="authors_list_page">
			<div class="container">
				<div class="row">

					<?php

						global $wpdb;
						
						$query = "SELECT ID, user_nicename from $wpdb->users ORDER BY user_nicename";
						$author_ids = $wpdb->get_results($query);

						foreach($author_ids as $author) :

							$curauth = get_userdata($author->ID);

							if($curauth->user_level > 0 || $curauth->user_login == 'admin') :

								$user_link = get_author_posts_url($curauth->ID);
								$avatar = 'identicon';
								$newsider_user_post_count = count_user_posts( $curauth->ID, $post_type = 'post' );
								$newsider_author_facebook = function_exists( 'evapb_author_contact_methods' ) ? get_user_meta( $curauth->ID, 'evapb_author_facebook', true ) : '';
								$newsider_author_googleplus = function_exists( 'evapb_author_contact_methods' ) ? get_user_meta( $curauth->ID, 'evapb_author_googleplus', true ) : '';
								$newsider_author_twitter = function_exists( 'evapb_author_contact_methods' ) ? get_user_meta( $curauth->ID, 'evapb_author_twitter', true ) : '';
								$newsider_author_instagram = function_exists( 'evapb_author_contact_methods' ) ? get_user_meta( $curauth->ID, 'evapb_author_instagram', true ) : '';
								$newsider_author_vimeo = function_exists( 'evapb_author_contact_methods' ) ? get_user_meta( $curauth->ID, 'evapb_author_vimeo', true ) : '';
								$newsider_author_vkontakte = function_exists( 'evapb_author_contact_methods' ) ? get_user_meta( $curauth->ID, 'evapb_author_vkontakte', true ) : '';
								$newsider_author_youtube = function_exists( 'evapb_author_contact_methods' ) ? get_user_meta( $curauth->ID, 'evapb_author_youtube', true ) : '';
								$newsider_author_linkedin = function_exists( 'evapb_author_contact_methods' ) ? get_user_meta( $curauth->ID, 'evapb_author_linkedin', true ) : '';
								$newsider_author_pinterest = function_exists( 'evapb_author_contact_methods' ) ? get_user_meta( $curauth->ID, 'evapb_author_pinterest', true ) : '';
								$newsider_author_wordpress = function_exists( 'evapb_author_contact_methods' ) ? get_user_meta( $curauth->ID, 'evapb_author_wordpress', true ) : '';
								$newsider_author_dropbox = function_exists( 'evapb_author_contact_methods' ) ? get_user_meta( $curauth->ID, 'evapb_author_dropbox', true ) : '';
								$newsider_author_rss = function_exists( 'evapb_author_contact_methods' ) ? get_user_meta( $curauth->ID, 'evapb_author_rss', true ) : '';
								
								?>
								
								<div class="col-md-3">
									<div class="author_item text-center mb60">
										<a class="author_posts_avatar" href="<?php echo esc_url( $user_link ); ?>" title="Articles by <?php echo esc_attr( $curauth->display_name ); ?>"><?php echo get_avatar($curauth->user_email, '250', $avatar); ?></a>
										<div class="author_posts_count"><?php echo $newsider_user_post_count . ' ' . esc_html__('articles','newsider') ?></div>
										<h5 class="author_posts_name"><a href="<?php echo esc_url( $user_link ); ?>" title="Articles by <?php echo esc_attr( $curauth->display_name ); ?>"><?php echo esc_attr( $curauth->display_name ); ?></a></h5>
										<div class="social_links">
											<?php
												if ( isset( $newsider_author_facebook ) && $newsider_author_facebook != '' ) {
													echo '<a class="social_link facebook" href="' . esc_url($newsider_author_facebook) . '" target="_blank"><i class="fa fa-facebook"></i><i class="fa fa-facebook"></i></a>';
												}

												if ( isset( $newsider_author_googleplus ) && $newsider_author_googleplus != '' ) {
													echo '<a class="social_link google-plus" href="' . esc_url( $newsider_author_googleplus ) . '" target="_blank"><i class="fa fa-google-plus"></i><i class="fa fa-google-plus"></i></a>';
												}

												if ( isset( $newsider_author_twitter ) && $newsider_author_twitter != '' ) {
													echo '<a class="social_link twitter" href="' . esc_url( $newsider_author_twitter ) . '" target="_blank"><i class="fa fa-twitter"></i><i class="fa fa-twitter"></i></a>';
												}
												
												if ( isset( $newsider_author_instagram ) && $newsider_author_instagram != '' ) {
													echo '<a class="social_link instagram" href="' . esc_url( $newsider_author_instagram ) . '" target="_blank"><i class="fa fa-instagram"></i><i class="fa fa-instagram"></i></a>';
												}
												
												if ( isset( $newsider_author_vimeo ) && $newsider_author_vimeo != '' ) {
													echo '<a class="social_link vimeo" href="' . esc_url( $newsider_author_vimeo ) . '" target="_blank"><i class="fa fa-vimeo"></i><i class="fa fa-vimeo"></i></a>';
												}
												
												if ( isset( $newsider_author_vkontakte ) && $newsider_author_vkontakte != '' ) {
													echo '<a class="social_link vk" href="' . esc_url( $newsider_author_vkontakte ) . '" target="_blank"><i class="fa fa-vk"></i><i class="fa fa-vk"></i></a>';
												}
												
												if ( isset( $newsider_author_youtube ) && $newsider_author_youtube != '' ) {
													echo '<a class="social_link youtube" href="' . esc_url( $newsider_author_youtube ) . '" target="_blank"><i class="fa fa-youtube"></i><i class="fa fa-youtube"></i></a>';
												}
												
												if ( isset( $newsider_author_linkedin ) && $newsider_author_linkedin != '' ) {
													echo '<a class="social_link linkedin" href="' . esc_url( $newsider_author_linkedin ) . '" target="_blank"><i class="fa fa-linkedin"></i><i class="fa fa-linkedin"></i></a>';
												}
												
												if ( isset( $newsider_author_pinterest ) && $newsider_author_pinterest != '' ) {
													echo '<a class="social_link pinterest" href="' . esc_url( $newsider_author_pinterest ) . '" target="_blank"><i class="fa fa-pinterest-p"></i><i class="fa fa-pinterest-p"></i></a>';
												}
												
												if ( isset( $newsider_author_wordpress ) && $newsider_author_wordpress != '' ) {
													echo '<a class="social_link wordpress" href="' . esc_url( $newsider_author_wordpress ) . '" target="_blank"><i class="fa fa-wordpress"></i><i class="fa fa-wordpress"></i></a>';
												}
												
												if ( isset( $newsider_author_dropbox ) && $newsider_author_dropbox != '' ) {
													echo '<a class="social_link dropbox" href="' . esc_url( $newsider_author_dropbox ) . '" target="_blank"><i class="fa fa-dropbox"></i><i class="fa fa-dropbox"></i></a>';
												}
												
												if ( isset( $newsider_author_rss ) && $newsider_author_rss != '' ) {
													echo '<a class="social_link rss" href="' . esc_url( $newsider_author_rss ) . '" target="_blank"><i class="fa fa-rss"></i><i class="fa fa-rss"></i></a>';
												}
												
											?>
										</div>
									</div>
								</div>
						
							<?php endif; ?>
						
					<?php endforeach; ?>
					
				</div>
			</div>
		</div>

<?php get_footer(); ?>