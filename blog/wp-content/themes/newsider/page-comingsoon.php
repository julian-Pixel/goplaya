<?php
/**
 * Template Name: Page - Coming Soon
 */

get_header('comingsoon');

global $post;

$newsider_coming_soon_title 		= get_post_meta( $post->ID, 'newsider_coming_soon_title', true );
$newsider_coming_soon_subtitle 		= get_post_meta( $post->ID, 'newsider_coming_soon_subtitle', true );
$newsider_coming_soon_social 		= get_post_meta( $post->ID, 'newsider_coming_soon_social', true );
$newsider_coming_soon_subscribe 	= get_post_meta( $post->ID, 'newsider_coming_soon_subscribe', true );


/* Coming Soon Styles */
$newsider_comingsoon_bg_styles = '';
$newsider_comingsoon_bg_image = get_post_meta( $post->ID, 'newsider_comingsoon_bg_image', true );
$newsider_comingsoon_bg_color = get_post_meta( $post->ID, 'newsider_comingsoon_bg_color', true );

if ( $newsider_comingsoon_bg_image ) {
	if ( is_numeric( $newsider_comingsoon_bg_image ) ) {
		$newsider_comingsoon_bg_image = wp_get_attachment_image_src( $newsider_comingsoon_bg_image, 'full' );
		$newsider_comingsoon_bg_image = $newsider_comingsoon_bg_image[0];
	}
}
	
if( $newsider_comingsoon_bg_image != '' ) {
	$newsider_comingsoon_bg_styles .= 'background-image: url('. $newsider_comingsoon_bg_image .');';
}
if( $newsider_comingsoon_bg_color != '' ) {
	$newsider_comingsoon_bg_styles .= 'background-color: '. $newsider_comingsoon_bg_color .';';
}
?>
		
		<div class="coming_soon_wrapper <?php echo esc_attr( $newsider_coming_soon_months ); ?>" style="<?php echo esc_attr( $newsider_comingsoon_bg_styles ); ?>">
			<div class="coming_soon_container">
				<div class="coming_soon_content">
					<div class="coming_soon_content_wrap">
						<?php if ( !empty( $newsider_coming_soon_title ) ) { ?>
							<h1><?php echo esc_attr( $newsider_coming_soon_title ); ?></h1>
						<?php } ?>
						<?php if ( !empty( $newsider_coming_soon_subtitle ) ) { ?>
							<p class="coming_descr"><?php echo esc_attr( $newsider_coming_soon_subtitle ); ?></p>
						<?php } ?>
						
						<!-- COUNTDOWN -->
						<ul class="countdown">
							<li>
								<span class="days">00</span>
								<p class="days_ref"><?php echo esc_attr__('Days', 'newsider'); ?></p>
							</li>
							<li>
								<span class="hours">00</span>
								<p class="hours_ref"><?php echo esc_attr__('Hours', 'newsider'); ?></p>
							</li>
							<li>
								<span class="minutes">00</span>
								<p class="minutes_ref"><?php echo esc_attr__('Minutes', 'newsider'); ?></p>
							</li>
							<li>
								<span class="seconds">00</span>
								<p class="seconds_ref"><?php echo esc_attr__('Seconds', 'newsider'); ?></p>
							</li>
						</ul><!-- //COUNTDOWN -->
					
					</div>
				</div>

				<?php if ( $newsider_coming_soon_social != 'hide') { ?>
					<div class="coming-soon-social-links">
						<?php echo newsider_social_links(); ?>
					</div>
				<?php } ?>
			</div>
		</div>

<?php get_footer('comingsoon');