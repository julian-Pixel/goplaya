<div id="comments">
	
	<?php
	
		if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
			die ('Please do not load this page directly. Thanks!');
	
		if ( post_password_required() ) { ?>
			<?php echo '<div class="comments_pass_note">' . esc_html__('<!--:en-->This post is password protected. Enter the password to view comments<!--:--><!--:es-->Este post está protegido con contraseña. Ingrese la contraseña para ver los comentarios<!--:-->.', 'newsider') . '</div></div>'; ?>
		<?php
			return;
		}
	?>
	
	<?php if ( have_comments() ) : ?>
	
		<div class="commentlist_wrap">
			<h2 class="comments_title">
				<?php echo esc_html__('<!--:en-->Comments<!--:--><!--:es-->Comentarios<!--:-->', 'newsider'); ?>
				<span class="theme_color">
					<?php printf( number_format_i18n(get_comments_number())	); ?>
				</span>
			</h2>
		
			<?php if( previous_comments_link() || next_comments_link() ) : ?>
				<div class="comments_navigation clearfix text-center mb30">
					<div class="next-comments pull-left"><?php previous_comments_link(); ?></div>
					<div class="prev-comments pull-right"><?php next_comments_link(); ?></div>
				</div>
			<?php endif; ?>
		
			<ol class="commentlist clearfix">
				<?php wp_list_comments(array('callback' => 'newsider_comment' )); ?>
			</ol>
			
			<?php if( previous_comments_link() || next_comments_link() ) : ?>
				<div class="comments_navigation clearfix text-center mb30">
					<div class="next-comments pull-left"><?php previous_comments_link(); ?></div>
					<?php paginate_comments_links(); ?> 
					<div class="prev-comments pull-right"><?php next_comments_link(); ?></div>
				</div>
			<?php endif; ?>
				
		</div>
		
	<?php else : // this is displayed if there are no comments so far ?>
	
		<?php if ( comments_open() ) : ?>
			<!-- If comments are open, but there are no comments. -->
	
		 <?php else : // comments are closed ?>
			<p class="hidden"><?php esc_html_e('<!--:en-->Comments are closed.<!--:--><!--:es-->Los comentarios están cerrados.<!--:-->', 'newsider'); ?></p>
	
		<?php endif; ?>
		
	<?php endif; ?>

	<?php if ( comments_open() ) : ?>

		<?php
		
			$req = get_option( 'require_name_email' );
			$aria_req = ( $req ? " aria-required='true'" : '' );

			//Custom Fields
			$fields =  array(
				'author'=> '<div id="respond-inputs" class="clearfix"><div class="comment-form-author"><input id="author" name="author" type="text" placeholder="' . esc_html__('<!--:en-->Name *<!--:--><!--:es-->Nombre *<!--:-->', 'newsider') . '" value="" size="30"' . $aria_req . ' /></div>',
				'email' => '<div class="comment-form-email"><input id="email" name="email" type="text" placeholder="' . esc_html__('<!--:en-->Email *<!--:--><!--:es-->Correo *<!--:-->', 'newsider') . '" value="" size="30"'. $aria_req .' /></div></div>',
			);

			//Comment Form Args
			$comments_args = array(
				'fields' => $fields,
				'title_reply' => esc_html__('<!--:en-->Leave A Comment<!--:--><!--:es-->Deja un comentario<!--:-->', 'newsider'),
				'comment_field' => '<div id="respond-textarea"><textarea id="comment" name="comment" placeholder="'. esc_html__('<!--:en-->Comment *<!--:--><!--:es-->Comentario *<!--:-->','newsider') .'" aria-required="true" cols="58" rows="10" tabindex="4"></textarea></div>',
				'comment_notes_after' => '',
				'label_submit' => esc_html__('<!--:en-->Post comment<!--:--><!--:es-->Publicar comentario<!--:-->','newsider')
			);
			
			// Show Comment Form
			comment_form($comments_args); 
		?>


	<?php endif; // if you delete this the sky will fall on your head ?>

</div>