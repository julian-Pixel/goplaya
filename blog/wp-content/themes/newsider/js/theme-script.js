window.jQuery = window.$ = jQuery;


//	Header Sticky
function newsider_headerSticky() {
	"use strict";
	
	if ( jQuery('body.header_type2').size() > 0 ) {
		var beforeMenu_h = jQuery('.header_wrap.desktop > div').height();
	} else if ( jQuery('body.header_type3').size() > 0 ) {
		var beforeMenu_h = jQuery('.header_wrap.desktop > div').height();
	} else {
		var beforeMenu_h = jQuery('.header_tagline_wrap').height();
	}

	if ( ( jQuery(window).scrollTop() > beforeMenu_h ) && ( jQuery(window).width() > 767 ) && ( !jQuery('body').hasClass('error404') ) ) {
		jQuery('body').addClass('header-fixed');
	} else {
		jQuery('body').removeClass('header-fixed');
	}
	
}


//	LazyLoad
function newsider_lazyload() {
    "use strict";
	
	jQuery("img.lazy").lazyload({
		effect : "fadeIn"
	});
}


//	Menu Height
function newsider_MenuHeight() {
    "use strict";
	
	jQuery('.header_menu_container').css({'height': jQuery('.header_menu_wrap').height() + 'px'});
}


//	Video Iframe Size
function newsider_video_size() {
	"use strict";
	jQuery('.post-video, .video_player, .pf_video_wrap').each(function(){	
		jQuery(this).find('iframe').css({'height': jQuery(this).width()*9/16 + 'px'});
	});
}


//	Page min height
function newsider_pageMinHeight() {
	"use strict";

	var windowH = jQuery(window).height();
	var headerH = jQuery('header').height();
	if (jQuery('#pagetitle').size() > 0) {
		if (jQuery('#pagetitle.breadcrumbs_show').size() > 0) {
			var pagetitleH = jQuery('#pagetitle').height();
			headerH = 0;
		} else {
			var pagetitleH = jQuery('#pagetitle').height() + 40;
			headerH = jQuery('header').height();
		}
	} else {
		var pagetitleH = 0;
	}
	if (jQuery('footer').size() > 0) {
		var footerH = jQuery('footer').height();
	} else {
		var footerH = 0;
	}
	var pagecontentH = windowH - headerH - pagetitleH - footerH;
	
	
	jQuery('#page-content').css({
		'min-height': pagecontentH + 'px'
	});

	
	//	Single Post
	if (jQuery('body.single-post #pagetitle').size() > 0) {
		jQuery('body.single-post #pagetitle').css({
			'height' : jQuery(window).height() + 'px',
			'padding-top' : '0',
			'padding-bottom' : '0',
		});
	}
	
}
	

//	Page 404 height
function newsider_page404_h() {
	"use strict";
	if (jQuery('body.error404').size() > 0) {
		var windowH = jQuery(window).height(),
			headerH = jQuery('header').height(),
			footerH = jQuery('footer').height(),
			page404ContainerH = jQuery('#error404_container .container').height(),
			page404H = windowH - headerH - footerH;
		
		jQuery('#page-content').css({
			'min-height': 'auto'
		});
		jQuery('#error404_container').css({
			'min-height': page404H + 'px',
			'padding-top': ( page404H - page404ContainerH ) / 2 + 'px',
		});
	}
}


//	Top Search Form width
function newsider_topSearchFormW() {
	"use strict";
	if (jQuery('.header_search .search_form').size() > 0) {
		if( jQuery(window).width() > 767 ){
			var containerW = jQuery('header .container').width();
		} else {
			var containerW = jQuery('header').width() - 30;
		}
		
		jQuery('.header_search .search_form').css({
			'width': containerW + 'px',
		});
	}
}


jQuery(document).ready(function($) {
	"use strict";
	
	// Empty href
	jQuery('.nav-menu a[href*="#"]').each(function() {
		jQuery(this).attr("href", "javascript:void(0);");
	});
	
	newsider_MenuHeight();
	
	newsider_lazyload();
	
	//	Header Search focus
	if (jQuery('header .search_form_wrap').size() > 0) {
		var $search_btn = $( '.header_search .fa' ),
			$search_form = $( '.header_search' );

		$search_btn.on( 'click', function () {
			$('body').toggleClass( 'header_searchform_focus' );
			$('.header_search input.search-field').focus();
		} );

		$( document ).on( 'click', function ( e ) {
			if ( $( e.target ).closest( $search_btn ).length == 0
				&& $( e.target ).closest( 'input.search-field' ).length == 0
				&& $('body').hasClass( 'header_searchform_focus' ) ) {
				$('body').removeClass( 'header_searchform_focus' );
			}
		} );
	}
	
	//	Video Iframe Size
	newsider_video_size();
	
	//	Mega menu (with tabs)
	jQuery('.menu-item-mega-parent').each(function(){
		jQuery('.category-children > div:not(:first-child)', this).addClass('tab-hidden');
		jQuery('.cstheme_mega_menu li:first-child', this).addClass('nav-active');
		jQuery('.cstheme_mega_menu li', this).hover(function() {
			var tabId = jQuery(this).attr('id');
			jQuery(this).closest('.cstheme_mega_menu').find('li').removeClass('nav-active');
			jQuery(this).addClass('nav-active');
			jQuery(this).closest('.cstheme_mega_menu_wrap').find('.category-children-wrap').addClass('tab-hidden');
			jQuery(this).closest('.cstheme_mega_menu_wrap').find('.category-children-wrap.'+tabId).removeClass('tab-hidden');
		}); 
		
		var highestTab = jQuery('.tabs-content-wrapper', this).outerHeight(); 
		jQuery('.cstheme_mega_menu', this).css( 'min-height', highestTab-40 );
		
	});
	
	//	Mega Menu Posts Carousel
	if( jQuery('.cstheme_mega_menu_wrap').size() > 0 ) {

		jQuery(".cstheme_mega_menu_wrap .owl-carousel").owlCarousel({
			margin: 25,
			dots: false,
			nav: true,
			navText: [
				"",
				"",
			],
			loop: false,
			autoplay: true,
			autoplaySpeed: 1000,
			autoplayTimeout: 3000,
			navSpeed: 1000,
			autoplayHoverPause: true,
			responsive: {
				0: {items: 1},
				481: {items: 2},
				769: {items: 3},
				1025: {items: 4},
				1921: {items: 4},
			},
			thumbs: false,
			thumbImage: false,
		});
	}
	
	//	Contact Form 7
	if (jQuery('.contentarea .wpcf7').size() > 0) {
		jQuery('.contentarea .wpcf7 span').each(function(){
			jQuery('.wpcf7-form-control').blur(function() {
				jQuery('.wpcf7-form-control').parent().parent().removeClass("focus");
			}).focus(function() {
				jQuery(this).parent().parent().addClass("focus")
			});
		});
	}
	
	//	Tooltips Bootstrap
	if (jQuery('[data-toggle="tooltip"]').size() > 0) {
		jQuery('[data-toggle="tooltip"]').tooltip();
	}
	
	
	//	List widgets
	jQuery('.widget_archive ul li, .widget_categories ul li').each(function(){
		var str = jQuery(this).html();
		str = str.replace('(', '<span class="val">');
		str = str.replace(')', '</span>');

		jQuery(this).html(str);
	});
	
	
	//	Mobile Menu Function
	if (jQuery('header .header_wrap.mobile').size() > 0) {
		jQuery('.mobile_menu_btn').on('click', function () {
			jQuery('header .header_wrap.mobile .menu-primary-menu-container-wrap').slideToggle();
			jQuery('header .header_wrap.mobile').toggleClass('active_mobile_menu');
		});
		
		jQuery('header .header_wrap.mobile ul.sub-menu').hide();
		jQuery('header .header_wrap.mobile .menu-item-has-children').children('a').on('click', function(event) {
			event.preventDefault();
			jQuery(this).toggleClass('submenu_open');
			jQuery(this).next('ul').slideToggle(300);
		});
	}
	
	
	//	Menu Button
	if (jQuery('.header_wrap .menu_btn').size() > 0) {
		jQuery('.header_wrap .menu_btn').on("click", function() {
			jQuery('body').toggleClass('menu_active');
		});
	}
	
	
	//	Single Post Avatar
	if (jQuery('body.single-post .author-image img').size() > 0) {
		var authorAvatar = jQuery('body.single-post .author-image').html();

		jQuery('body.single-post #pagetitle span.post-meta-author a').before(authorAvatar);
	}
	
	
	//	Single Post Comment focus
	if (jQuery('#respond').size() > 0) {
		jQuery('#commentform input, #commentform textarea').each(function(){
			jQuery(this).blur(function() {
				jQuery(this).parent().removeClass("focus");
			}).focus(function() {
				jQuery(this).parent().addClass("focus")
			});
		});
	}
	
	
	if( jQuery('.footer_posts_carousel_list.owl-carousel').size() > 0 ) {
		jQuery('.footer_posts_carousel_list.owl-carousel').owlCarousel({
			margin: 30,
			dots: false,
			nav: true,
			navText: [
				"",
				"",
			],
			loop: false,
			autoplay: true,
			autoplaySpeed: 1000,
			autoplayTimeout: 3000,
			navSpeed: 1000,
			autoplayHoverPause: true,
			responsive: {
				0: {items: 1},
				481: {items: 2},
				769: {items: 3},
				1025: {items: 4},
				1921: {items: 5},
			},
			thumbs: false,
			thumbImage: false,
		});
	}
	
});


jQuery(window).load(function(){
	"use strict";
	
	//	Preloader
	if( jQuery('#loader').size() > 0 ) {
		jQuery("#loader").removeClass('loading').delay(1000).addClass('loaded');
	}
	
	setTimeout("newsider_pageMinHeight()", 100);
	
	if (jQuery(".recent_posts_list.carousel").size() > 0) {
		jQuery(".recent_posts_list.carousel").owlCarousel({
			margin: 0,
			nav: true,
			dots: false,
			navText: [
				"",
				"",
			],
			loop: true,
			autoplay: true,
			autoplaySpeed: 1000,
			autoplayTimeout: 3000,
			navSpeed: 1000,
			autoplayHoverPause: true,
			items: 1,
			thumbs: false
		});
	}
	
	if (jQuery('#blog-single-wrap .post-slider.owl-carousel').size() > 0) {
		jQuery('#blog-single-wrap .post-slider.owl-carousel').owlCarousel({
			items: 1,
			margin: 0,
			dots: false,
			nav: true,
			navText: [
				"",
				""
			],
			loop: true,
			autoplay: true,
			autoplaySpeed: 1000,
			autoplayTimeout: 5000,
			navSpeed: 1000,
			autoplayHoverPause: true,
			thumbs: false
		});
	}
	
	if (jQuery('.pf_slider_wrap .post-slider.owl-carousel').size() > 0) {
		jQuery('.pf_slider_wrap .post-slider.owl-carousel').owlCarousel({
			items: 1,
			margin: 0,
			dots: false,
			nav: true,
			navText: [
				"",
				""
			],
			loop: true,
			autoplay: true,
			autoplaySpeed: 1000,
			autoplayTimeout: 5000,
			navSpeed: 1000,
			autoplayHoverPause: true,
			thumbs: false
		});
	}
	
	if (jQuery('.post-slider.justified').size() > 0) {
		jQuery('.post-slider.justified').justifiedGallery({
			border:0,
			margins:10,
			lastRow:"justify",
			rowHeight:200,
			maxRowHeight:300,
			captions:!0,
			cssAnimation:!0,
			captionSettings:{
				animationDuration:100,
				visibleOpacity:1,
				nonVisibleOpacity:0
			}
		});
	}
	
	//	SwipeBox popup
	if( jQuery('.swipebox').size() > 0 ) {
		jQuery( '.swipebox' ).swipebox();
	}
	
	newsider_page404_h();
	
	newsider_topSearchFormW();
	
	newsider_lazyload();
	
});

jQuery(window).resize(function(){
	"use strict";
	
	newsider_pageMinHeight();
	
	newsider_MenuHeight();
	
	//	Video Iframe Size
	newsider_video_size();
	
	newsider_page404_h();
	
	newsider_topSearchFormW();
	
	//	Header Sticky
	newsider_headerSticky();
	
});

jQuery(window).scroll(function(){
	"use strict";

	//	Header Sticky
	newsider_headerSticky();

});