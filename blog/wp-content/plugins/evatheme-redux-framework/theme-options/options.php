<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "newsider_option";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'newsider_option/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();

    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => $theme->get( 'Name' ) . esc_html__( ' Options', 'evatheme-redux-framework' ),
        'page_title'           => $theme->get( 'Name' ) . esc_html__( ' Options', 'evatheme-redux-framework' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => plugin_dir_url( __FILE__ ) . 'img/emblem.png',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );


    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://plus.google.com/share?url=http://www.evatheme.ru',
        'title' => 'Visit us on Google Plus',
        'icon'  => 'el el-googleplus'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/evatheme',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'https://twitter.com/EVATHEME',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.behance.net/evickagency',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-behance'
    );
	
	// Add content after the form.
	$args['footer_text'] = __( '<p style="margin-top:20px;"><a href="http://www.evatheme.com/themes/" target="_blank" style="font-size:14px; color:#777; text-decoration:none; margin-right:10px;">More Themes</a><a href="http://forum.evatheme.com/" target="_blank"  style="font-size:14px; color:#777; text-decoration:none; margin-right:10px;">Support Forum</a><a href="http://newsider.evatheme.com/docs/" target="_blank" style="font-size:14px; color:#777; text-decoration:none; margin-right:10px;">Documentation</a>
	</p>', 'evatheme-redux-framework' );
	
	
    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_html__( 'Theme Information 1', 'evatheme-redux-framework' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'evatheme-redux-framework' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_html__( 'Theme Information 2', 'evatheme-redux-framework' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'evatheme-redux-framework' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = esc_html__( '<p>This is the sidebar content, HTML is allowed.</p>', 'evatheme-redux-framework' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

	/* General */
	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__( 'General', 'evatheme-redux-framework' ),
		'heading'		=> esc_html__( 'General Settings', 'evatheme-redux-framework'),
		'id'			=> 'general',
		'customizer_width' => '400px',
        'icon'			=> 'el el-cogs',
		'fields' 		=> array(
			array(
				'id'		=> 'section-favicon-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Favicon', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
				'id'		=> 'favicon',
				'type'		=> 'media',
				'title'		=> esc_html__( 'Favicon', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Please insert your favicon 16x16px or 32x32px icon.', 'evatheme-redux-framework' ) . '<br>' . esc_html__( 'Please note that if you"ve already uploaded the Site Icon in the Theme Customizer (Appearance -> Customize), the settings from the theme options panel will be ignored.', 'evatheme-redux-framework' ),
				'compiler'	=> 'true',
				'desc'		=> esc_html__( 'Upload your Favicon.', 'evatheme-redux-framework' ),
				'default'	=> array(
									'url' => plugin_dir_url( __FILE__ ) . 'img/favicon.ico',
								),
			),
			array(
				'id'		=> 'favicon-retina',
				'type'		=> 'switch',
				'title'		=> esc_html__( 'Retina Favicon', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Check this option if you want to have a Retina Favicon.', 'evatheme-redux-framework' ),
				'default'	=> '0'
			),
			array(
				'id'		=> 'apple_icons_57x57',
				'type'		=> 'media',
				'required'	=> array( 'favicon-retina', '=', '1' ),
				'title'		=> esc_html__( 'Apple touch icon (57px)', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Icon must be 57x57px. Please note that if you"ve already uploaded the Site Icon in the Theme Customizer (Appearance -> Customize), the settings from the theme options panel will be ignored.', 'evatheme-redux-framework' ),
				'compiler'	=> 'true',
				'desc'		=> esc_html__( 'Upload your Favicon.', 'evatheme-redux-framework' ),
				'default'	=> array('url' => ''),
			),
			array(
				'id'		=> 'apple_icons_72x72',
				'type'		=> 'media',
				'required'	=> array( 'favicon-retina', '=', '1' ),
				'title'		=> esc_html__( 'Apple touch icon (72px)', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Icon must be 72x72px. Please note that if you"ve already uploaded the Site Icon in the Theme Customizer (Appearance -> Customize), the settings from the theme options panel will be ignored.', 'evatheme-redux-framework' ),
				'compiler'	=> 'true',
				'desc'		=> esc_html__( 'Upload your Favicon.', 'evatheme-redux-framework' ),
				'default'	=> array('url' => ''),
			),
			array(
				'id'		=> 'apple_icons_114x114',
				'type'		=> 'media',
				'required'	=> array( 'favicon-retina', '=', '1' ),
				'title'		=> esc_html__( 'Apple touch icon (114px)', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Icon must be 114x114px. Please note that if you"ve already uploaded the Site Icon in the Theme Customizer (Appearance -> Customize), the settings from the theme options panel will be ignored.', 'evatheme-redux-framework' ),
				'compiler'	=> 'true',
				'desc'		=> esc_html__( 'Upload your Favicon.', 'evatheme-redux-framework' ),
				'default'	=> array('url' => ''),
			),
			array(
				'id'		=> 'section-favicon-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
			
			array(
				'id'		=> 'section-preloader-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Preloader Settings', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
				'id'		=> 'preloader',
				'type'		=> 'switch',
				'title'		=> esc_html__( 'Preloader', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Enable a preloader screen on first page load. It displays a running line until the browser fetch the whole web content and will fade out the moment the page has been completely cached.', 'evatheme-redux-framework' ),
				'default'	=> '0'
			),
			array(
				'id'		=> 'section-preloader-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
			
			array(
				'id'		=> 'section-theme_layout-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Theme Layout', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
				'id'		=> 'theme_layout',
				'type'		=> 'select',
				'title'		=> esc_html__( 'Theme Layout', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Select the layout for you site.', 'evatheme-redux-framework' ),
				'options' 	=> array(
								'full-width' 	=> esc_html__( 'Full-Width', 'evatheme-redux-framework' ),
								'boxed' 		=> esc_html__( 'Boxed', 'evatheme-redux-framework' ),
							),
				'default'	=> 'full-width'
			),
			array(
				'id'        => 'theme_bg_image',
				'type'      => 'background',
				'title'     => esc_html__('Theme Background Color / Image', 'evatheme-redux-framework'),
				'subtitle'  => esc_html__('Select the for theme background color or image.', 'evatheme-redux-framework'),
				'required' => array('theme_layout', 'equals', array('boxed')),
				'default'   => array(
									'background-color' => '#696969',
								),
				'transparent' => false,
			),
			array(
				'id'		=> 'theme_boxed_margin',
				'type'		=> 'slider',
				'title'		=> esc_html__( 'Indentation Site', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Select margins for the entire site from above and below', 'evatheme-redux-framework' ),
				'required' => array('theme_layout', 'equals', array('boxed')),
				'default'	=> 0,
				'min'		=> 0,
				'step'		=> 10,
				'max'		=> 200,
				'display_value' => 'text'
			),
			array(
				'id'		=> 'section-theme_layout-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
			
			array(
				'id'		=> 'section-preloader-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Preloader Settings', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
				'id'		=> 'preloader',
				'type'		=> 'switch',
				'title'		=> esc_html__( 'Preloader', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Enable a preloader screen on first page load. It displays a running line until the browser fetch the whole web content and will fade out the moment the page has been completely cached.', 'evatheme-redux-framework' ),
				'default'	=> '0'
			),
			array(
				'id'		=> 'section-preloader-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
			
			array(
				'id'		=> 'section-codefield-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Custom Code Fields', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
				'id'		=> 'custom_css',
				'type'		=> 'ace_editor',
				'title'		=> esc_html__( 'CSS Code', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Paste your custom CSS code here.', 'evatheme-redux-framework' ) . '<br>' . esc_html__( 'Very helpful if you add a custom class in a shortcode in order to customize a specific element.', 'evatheme-redux-framework' ),
				'mode'		=> 'css',
				'theme'		=> 'chrome',
				'desc'		=> '',
				'default'	=> ''
			),
			array(
				'id'		=> 'custom-js',
				'type'		=> 'ace_editor',
				'title'		=> esc_html__( 'JS Code', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Paste your custom JS code here.', 'evatheme-redux-framework' ),
				'mode'		=> 'javascript',
				'theme'		=> 'monokai',
				'desc'		=> '',
				'default'	=> ''
			),
			array(
				'id'		=> 'section-codefield-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
		)
	) );
	
	
	//	Typography
    Redux::setSection( $opt_name, array(
        'title'			=> esc_html__( 'Typography', 'evatheme-redux-framework' ),
		'heading'		=> esc_html__('Typography Settings', 'evatheme-redux-framework'),
        'id'			=> 'typography',
        'icon'			=> 'el el-font',
        'fields'		=> array(
            array(
				'id'		=> 'section-main-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Main font', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
                'id'		=> 'body-font',
                'type'		=> 'typography',
                'title'		=> esc_html__( 'Body Font', 'evatheme-redux-framework' ),
                'subtitle'	=> esc_html__( 'Specify the body font properties.', 'evatheme-redux-framework' ),
                'google'	=> true,
				'preview'	=> false,
				'output'	=> array( 'body' ),
                'compiler'	=> array( 'body' ),
                'default'	=> array(
                    'font-family'	=> 'Roboto Condensed',
					'font-style'	=> '300',
					'line-height'	=> '28px',
                    'font-size'		=> '16px',
                    'color'			=> '#222222',
                ),
            ),
			array(
				'id'		=> 'section-main-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
			array(
				'id'		=> 'section-heading-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Heading font', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
            array(
                'id'		=> 'h1-font',
                'type'		=> 'typography',
                'title'		=> esc_html__( 'Heading 1', 'evatheme-redux-framework' ),
                'google'	=> true,
				'text-transform' => true,
                'letter-spacing' => true,
                'preview'	=> false,
                'all_styles' => true,
                'output'	=> array( 'h1' ),
                'compiler'	=> array( 'h1' ),
                'default'	=> array(
                    'font-family'	=> 'Roboto Condensed',
					'text-transform' => 'none',
					'font-style'	=> '300',
					'line-height'	=> '58px',
					'font-size'		=> '46px',
					'color'			=> '#222222',
					'letter-spacing' => '1px'
                ),
            ),
			array(
                'id'		=> 'h2-font',
                'type'		=> 'typography',
                'title'		=> esc_html__( 'Heading 2', 'evatheme-redux-framework' ),
                'google'	=> true,
				'text-transform' => true,
                'letter-spacing' => true,
                'preview'	=> false,
                'all_styles' => true,
                'output'	=> array( 'h2' ),
                'compiler'	=> array( 'h2' ),
                'default'	=> array(
                    'font-family' 	=> 'Roboto Condensed',
					'text-transform' => 'none',
					'font-style' 	=> '300',
					'line-height' 	=> '52px',
					'font-size' 	=> '40px',
					'color' 		=> '#222222',
					'letter-spacing' => '1px'
                ),
            ),
			array(
                'id' 		=> 'h3-font',
                'type' 		=> 'typography',
                'title' 	=> esc_html__( 'Heading 3', 'evatheme-redux-framework' ),
                'google' 	=> true,
				'text-transform' => true,
                'letter-spacing' => true,
                'preview' 	=> false,
                'all_styles' => true,
                'output' 	=> array( 'h3' ),
                'compiler' 	=> array( 'h3' ),
                'default' 	=> array(
                    'font-family' 	=> 'Roboto Condensed',
					'text-transform' => 'none',
					'font-style' 	=> '300',
					'line-height' 	=> '46px',
					'font-size' 	=> '34px',
					'color' 		=> '#222222',
					'letter-spacing' => '1px'
                ),
            ),
			array(
                'id' 		=> 'h4-font',
                'type' 		=> 'typography',
                'title' 	=> esc_html__( 'Heading 4', 'evatheme-redux-framework' ),
                'google' 	=> true,
				'text-transform' => true,
                'letter-spacing' => true,
                'preview' 	=> false,
                'all_styles' => true,
                'output' 	=> array( 'h4' ),
                'compiler' 	=> array( 'h4' ),
                'default' 	=> array(
                    'font-family' 	=> 'Roboto Condensed',
					'text-transform' => 'none',
					'font-style' 	=> '300',
					'line-height' 	=> '40px',
					'font-size' 	=> '28px',
					'color' 		=> '#222222',
					'letter-spacing' => '1px'
                ),
            ),
			array(
                'id' 		=> 'h5-font',
                'type' 		=> 'typography',
                'title' 	=> esc_html__( 'Heading 5', 'evatheme-redux-framework' ),
                'google' 	=> true,
				'text-transform' => true,
                'letter-spacing' => true,
                'preview' 	=> false,
                'all_styles' => true,
                'output' 	=> array( 'h5' ),
                'compiler' 	=> array( 'h5' ),
                'default' 	=> array(
                    'font-family' 	=> 'Roboto Condensed',
					'text-transform' => 'none',
					'font-style' 	=> '300',
					'line-height' 	=> '34px',
					'font-size' 	=> '22px',
					'color' 		=> '#222222',
					'letter-spacing' => '1px'
                ),
            ),
			array(
                'id' 		=> 'h6-font',
                'type' 		=> 'typography',
                'title' 	=> esc_html__( 'Heading 6', 'evatheme-redux-framework' ),
                'google' 	=> true,
				'text-transform' => true,
                'letter-spacing' => true,
                'preview' 	=> false,
                'all_styles' => true,
                'output' 	=> array( 'h6' ),
                'compiler' 	=> array( 'h6' ),
                'default' 	=> array(
                    'font-family' 	=> 'Roboto Condensed',
					'text-transform' => 'none',
					'font-style' 	=> '300',
					'line-height' 	=> '28px',
					'font-size' 	=> '16px',
					'color' 		=> '#222222',
					'letter-spacing' => '1px'
                ),
            ),
			array(
				'id'		=> 'section-heading-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
        )
    ) );
	
	
	// Color Selection
    Redux::setSection( $opt_name, array(
        'title'			=> esc_html__( 'Colors', 'evatheme-redux-framework' ),
		'heading'		=> esc_html__('Colors Settings', 'evatheme-redux-framework'),
        'id'			=> 'color',
		'icon'			=> 'el el-brush',
        'fields'		=> array(
            array(
				'id' 		=> 'section-theme-color-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Theme Color', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			
			array(
                'id'		=> 'theme_color',
                'type'		=> 'color',
				'transparent' => false,
                'output'	=> array( '.theme_color' ),
                'title'		=> esc_html__( 'Default Theme Color', 'evatheme-redux-framework' ),
                'subtitle'	=> esc_html__( 'Pick a color for the theme (default: #3fc6cb).', 'evatheme-redux-framework' ),
                'default'	=> '#3fc6cb',
            ),
			array(
				'id'		=> 'section-theme-color-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
        ),
    ) );
	
	
	// Header
    Redux::setSection( $opt_name, array(
		'title'			=> esc_html__( 'Header', 'evatheme-redux-framework' ),
		'heading'		=> esc_html__( 'Header Settings', 'evatheme-redux-framework' ),
		'id'			=> 'header',
		'icon'			=> 'dashicons dashicons-align-center',
		'fields'		=> array(
			array(
				'id'		=> 'section-header_type-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Header Type', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
				'id'		=> 'header_type',
				'type' 		=> 'image_select',
				'full_width'=> true,
				'title' 	=> esc_html__( 'Header Type', 'evatheme-redux-framework' ),
				'options' 	=> array(
					'type1' 		=> array( 'alt' => esc_html__( 'Default Header', 'evatheme-redux-framework' ), 'img' => plugin_dir_url( __FILE__ ) . 'img/header_type1.jpg' ),
					'type2' 		=> array( 'alt' => esc_html__( 'Header Left Logo', 'evatheme-redux-framework' ), 'img' => plugin_dir_url( __FILE__ ) . 'img/header_type2.jpg' ),
					'type3' 		=> array( 'alt' => esc_html__( 'Header Center Logo', 'evatheme-redux-framework' ), 'img' => plugin_dir_url( __FILE__ ) . 'img/header_type3.jpg' ),
					'type4' 		=> array( 'alt' => esc_html__( 'Header Top Menu Center Logo', 'evatheme-redux-framework' ), 'img' => plugin_dir_url( __FILE__ ) . 'img/header_type4.jpg' ),
				),
				'default' 	=> 'type4'
			),
			array(
				'id'		=> 'section-header_type-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
			
			array(
				'id'		=> 'section-logo-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Theme Logo', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
				'id'		=> 'header-logo',
				'type'		=> 'media',
				'title'		=> esc_html__( 'Upload Standard Logo', 'evatheme-redux-framework' ),
				'compiler'	=> 'true',
				'desc'		=> esc_html__( 'Upload your header logo.', 'evatheme-redux-framework' ),
				'default'	=> array( 'url' => plugin_dir_url( __FILE__ ) . 'img/logo.png' ),
			),
			array(
				'id'		=> 'header-logo-retina',
				'type'		=> 'media',
				'title'		=> esc_html__( 'Retina Logo', 'evatheme-redux-framework' ),
				'compiler'	=> 'true',
				'desc'		=> esc_html__( 'Upload your header retina logo.', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Note: You retina logo must be larger than 2x. Example: Main logo 120x200 then Retina must be 240x400.', 'evatheme-redux-framework' ),
				'default'	=> array('url' => ''),
			),
			array(
				'id'		=> 'section-logo-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
			
			array(
				'id'		=> 'section-header_icons-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Header Icons', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
				'id'		=> 'header_search',
				'type'		=> 'switch',
				'title'		=> esc_html__( 'Header Search', 'evatheme-redux-framework' ),
				'default'	=> '1'
			),
			array(
				'id'		=> 'header_social_icons',
				'type'		=> 'switch',
				'title'		=> esc_html__( 'Header Social Icons', 'evatheme-redux-framework' ),
				'default'	=> '0'
			),
			array(
				'id'		=> 'section-header_icons-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
			
			array(
				'id'		=> 'section-header_adv-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Advertising from Header', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
				'id' 		=> 'header_adv',
				'type' 		=> 'textarea',
				'title' 	=> esc_html__('Field for adv', 'evatheme-redux-framework'), 
				'subtitle' 	=> esc_html__('Enter your adv html code in this field. your adv will be displayed next to the logo', 'evatheme-redux-framework'),
				'validate' 	=> 'html_custom',
				'default' 	=> '<a href="http://www.evatheme.com/themes/" target="_blank"><img src="http://newsider.evatheme.com/demo/wp-content/uploads/2017/01/ad1.jpg" alt="Newsider Adv" /></a>',
				
			),
			array(
				'id'		=> 'section-header_ad-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
		)
	) );
	
	
	// Header Menu Settings
    Redux::setSection( $opt_name, array(
		'title'			=> esc_html__( 'Menu Settings', 'evatheme-redux-framework' ),
		'heading'		=> esc_html__( 'Menu Settings', 'evatheme-redux-framework' ),
		'id'			=> 'header_menu',
		'subsection'	=> true,
		'fields'		=> array(
			array(
				'id'     	=> 'section-header_menu_primary-start',
				'type'   	=> 'section',
				'title' 	=> '',
				'indent' 	=> true,
			),
			array(
                'id'		=> 'menu_primary',
                'type'		=> 'typography',
                'title'		=> esc_html__( 'Primary Menu main item', 'evatheme-redux-framework' ),
                'google'	=> true,
				'text-align' => false,
				'text-transform' => true,
				'line-height'	=> false,
				'color' 	=> false,
                'letter-spacing' => true,
                'preview'	=> false,
                'all_styles' => false,
                'default'	=> array(
					'font-family'	=> 'Roboto Condensed',
					'text-transform' => 'uppercase',
					'font-style'	=> '400',
					'font-size'		=> '12px',
					'letter-spacing' => '2px'
                ),
            ),
			array(
                'id'		=> 'submenu_primary',
                'type'		=> 'typography',
                'title'		=> esc_html__( 'Primary Submenu item', 'evatheme-redux-framework' ),
                'google'	=> true,
				'text-align' => false,
				'text-transform' => true,
				'line-height'	=> false,
				'color' 	=> false,
                'letter-spacing' => true,
                'preview'	=> false,
                'all_styles' => false,
                'default'	=> array(
					'font-family'	=> 'Roboto Condensed',
					'text-transform' => 'uppercase',
					'font-style'	=> '400',
					'font-size'		=> '11px',
					'letter-spacing' => '1px'
                ),
            ),
			array(
				'id'     => 'section-header_menu_primary-end',
				'type'   => 'section',
				'indent' => false,
			),
		)
	) );
	
	
	// Page Title
    Redux::setSection( $opt_name, array(
		'title'			=> esc_html__( 'Page Title', 'evatheme-redux-framework' ),
		'heading'		=> '',
		'id'			=> 'pagetitle',
		'icon'			=> 'dashicons dashicons-editor-insertmore',
		'fields'		=> array(
			array(
				'id'     	=> 'section-pagetitle-start',
				'type'   	=> 'section',
				'title' 	=> esc_html__( 'Page Title Styles', 'evatheme-redux-framework' ),
				'indent' 	=> true,
			),
			array(
				'id'        => 'pagetitle',
				'type'      => 'select',
				'title'     => esc_html__('Show Page Title?', 'evatheme-redux-framework'),
				'subtitle'  => esc_html__('Turn on to show the page title.', 'evatheme-redux-framework'),
				'options' => array(
					'show' => esc_html__( 'Show', 'evatheme-redux-framework' ),
					'hide' => esc_html__( 'Hide', 'evatheme-redux-framework' ),
				),
				'default' => 'show',
			),
			array(
				'id'		=> 'pagetitle_height',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Page Title Height', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Enter the height for the page title.', 'evatheme-redux-framework' ),
				'required' 	=> array('pagetitle', 'equals', array('show')),
				'default'	=> '120px'
			),
			array(
				'id'        => 'pagetitle_bg_image',
				'type'      => 'background',
				'title'     => esc_html__('Page Title Background Color / Image', 'evatheme-redux-framework'),
				'subtitle'  => esc_html__('Select the Title background color or image.', 'evatheme-redux-framework'),
				'required' 	=> array('pagetitle', 'equals', array('show')),
				'default'   => array(
									'background-color' => '#f5f5f5',
								),
				'transparent' => false,
			),
			array(
				'id'        => 'pagetitle_bg_image_parallax',
				'type'      => 'select',
				'title'     => esc_html__('Parallax Effect', 'evatheme-redux-framework'),
				'subtitle'  => esc_html__('Enable this to the parallax effect for background image.', 'evatheme-redux-framework'),
				'options' => array(
					'disable' => esc_html__( 'Disable', 'evatheme-redux-framework' ),
					'enable' => esc_html__( 'Enable', 'evatheme-redux-framework' ),
				),
				'default' => 'disable',
			),
			array(
				'id'        => 'pagetitle_text_color',
				'type'      => 'color',
				'title'     => esc_html__('Title Text Color', 'evatheme-redux-framework'),
				'subtitle'  => esc_html__('Title text color (default: #222222).', 'evatheme-redux-framework'),
				'required' => array('pagetitle', 'equals', array('show')),
				'default'   => '#222222',
				'transparent' => false,
			),
			array(
				'id'     => 'section-pagetitle-end',
				'type'   => 'section',
				'indent' => false,
			),
			
			array(
				'id'     	=> 'section-breadcrumbs-start',
				'type'   	=> 'section',
				'title' 	=> esc_html__( 'Breadcrumbs Styles', 'newsider' ),
				'indent' 	=> true,
			),
			array(
				'id'        => 'breadcrumbs',
				'type'      => 'select',
				'title'     => esc_html__('Show breadcrumbs?', 'newsider'),
				'subtitle'  => esc_html__('Turn on to show the breadcrumbs in the title.', 'newsider'),
				'options' => array(
					'show' => esc_html__( 'Show', 'newsider' ),
					'hide' => esc_html__( 'Hide', 'newsider' ),
				),
				'default' => 'show',
			),
			array(
				'id'     => 'section-breadcrumbs-end',
				'type'   => 'section',
				'indent' => false,
			),
		)
	) );
	
	
	// Blog
    Redux::setSection( $opt_name, array(
		'title'			=> esc_html__( 'Blog', 'evatheme-redux-framework' ),
		'heading'		=> esc_html__( 'Blog Settings', 'evatheme-redux-framework' ),
		'id'			=> 'blog',
		'icon'			=> 'dashicons dashicons-welcome-write-blog',
		'fields'		=> array(
			array(
				'id'     	=> 'section-blog_layout-start',
				'type'   	=> 'section',
				'title' 	=> esc_html__( 'Blog Layout', 'evatheme-redux-framework' ),
				'indent' 	=> true,
			),
			array(
				'id'		=> 'blog_layout',
				'type' 		=> 'image_select',
				'title' 	=> esc_html__( 'Blog Layout', 'evatheme-redux-framework' ),
				'options' 	=> array(
					'no-sidebar' 		=> array('alt' => esc_html__('No Sidebar', 'evatheme-redux-framework'), 'img' => plugin_dir_url( __FILE__ ) . 'img/no-sidebar.png'),
					'right-sidebar' 	=> array('alt' => esc_html__('Right Sidebar', 'evatheme-redux-framework'), 'img' => plugin_dir_url( __FILE__ ) . 'img/right-sidebar.png'),
					'left-sidebar' 		=> array('alt' => esc_html__('Left Sidebar', 'evatheme-redux-framework'), 'img' => plugin_dir_url( __FILE__ ) . 'img/left-sidebar.png'),
				),
				'default' 	=> 'right-sidebar'
			),
			array(
				'id'     => 'section-blog_layout-end',
				'type'   => 'section',
				'indent' => false,
			),
			
			array(
				'id'     	=> 'section-blogtitle-start',
				'type'   	=> 'section',
				'title' 	=> esc_html__( 'Blog Title Styles', 'evatheme-redux-framework' ),
				'indent' 	=> true,
			),
			array(
				'id'        => 'blog_pagetitle',
				'type'      => 'select',
				'title'     => esc_html__('Show Page Title?', 'evatheme-redux-framework'),
				'subtitle'  => esc_html__('Turn on to show the page title.', 'evatheme-redux-framework'),
				'options' => array(
					'show' => esc_html__( 'Show', 'evatheme-redux-framework' ),
					'hide' => esc_html__( 'Hide', 'evatheme-redux-framework' ),
				),
				'default' => 'show',
			),
			array(
				'id'		=> 'blog_pagetitle_height',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Blog Title Height', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Enter the height for the blog title.', 'evatheme-redux-framework' ),
				'required' => array('blog_pagetitle', 'equals', array('show')),
				'default'	=> '200px'
			),
			array(
				'id'        => 'blog_pagetitle_bg_image',
				'type'      => 'background',
				'title'     => esc_html__('Page Title Background Color / Image', 'evatheme-redux-framework'),
				'subtitle'  => esc_html__('Select the Title background color or image.', 'evatheme-redux-framework'),
				'required' => array('blog_pagetitle', 'equals', array('show')),
				'default'   => array(
									'background-color' => '#e8e8e8',
								),
				'transparent' => false,
			),
			array(
				'id'        => 'blog_pagetitle_text_color',
				'type'      => 'color',
				'title'     => esc_html__('Title Text Color', 'evatheme-redux-framework'),
				'subtitle'  => esc_html__('Title text color (default: #222222).', 'evatheme-redux-framework'),
				'required' => array('blog_pagetitle', 'equals', array('show')),
				'default'   => '#222222',
				'transparent' => false,
			),
			array(
				'id'		=> 'blog_pagetitle_text',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Blog Title', 'evatheme-redux-framework' ),
				'required' => array('blog_pagetitle', 'equals', array('show')),
				'default'	=> 'Blog Classic'
			),
			array(
				'id'     => 'section-blogtitle-end',
				'type'   => 'section',
				'indent' => false,
			),

		)
	) );
	
	
	// Single Post
    Redux::setSection( $opt_name, array(
		'title'			=> esc_html__( 'Single Post', 'evatheme-redux-framework' ),
		'heading'		=> esc_html__( 'Single Post Settings', 'evatheme-redux-framework' ),
		'id'			=> 'single_post',
		'subsection'	=> true,
		'fields'		=> array(
			array(
				'id'     	=> 'section-blogsingle-start',
				'type'   	=> 'section',
				'title' 	=> esc_html__( 'Single Post Title Styles', 'evatheme-redux-framework' ),
				'indent' 	=> true,
			),
			array(
				'id'		=> 'blogsingle_layout',
				'type' 		=> 'image_select',
				'title' 	=> esc_html__( 'Single Post Layout', 'evatheme-redux-framework' ),
				'options' 	=> array(
					'no-sidebar' 		=> array('alt' => esc_html__('No Sidebar', 'evatheme-redux-framework'), 'img' => plugin_dir_url( __FILE__ ) . 'img/no-sidebar.png'),
					'right-sidebar' 	=> array('alt' => esc_html__('Right Sidebar', 'evatheme-redux-framework'), 'img' => plugin_dir_url( __FILE__ ) . 'img/right-sidebar.png'),
					'left-sidebar' 		=> array('alt' => esc_html__('Left Sidebar', 'evatheme-redux-framework'), 'img' => plugin_dir_url( __FILE__ ) . 'img/left-sidebar.png'),
				),
				'default' 	=> 'right-sidebar'
			),
			array(
				'id'     => 'section-blogsingle-end',
				'type'   => 'section',
				'indent' => false,
			),
			array(
				'id'     	=> 'section-blogsingle-sharebox-start',
				'type'   	=> 'section',
				'title' 	=> esc_html__( 'Social sharing box icons', 'evatheme-redux-framework' ),
				'indent' 	=> true,
			),
			array(
				'id' => 'single_post_sharebox',
				'type' => 'switch',
				'title' => esc_html__( 'Sharebox', 'evatheme-redux-framework' ),
				'subtitle' => esc_html__( 'Enable social share-box on single post view?', 'evatheme-redux-framework' ),
				'default' => '1'
			),
			array(
				'id' => 'single_post_sharebox_facebook',
				'type' => 'switch',
				'title' => esc_html__( 'Facebook', 'evatheme-redux-framework' ),
				'required' => array( 'single_post_sharebox', '=', '1' ),
				'subtitle' => esc_html__( 'Check to enable Facebook in social sharing box', 'evatheme-redux-framework' ),
				'default' => '1'
			),
			array(
				'id' => 'single_post_sharebox_twitter',
				'type' => 'switch',
				'title' => esc_html__( 'Twitter', 'evatheme-redux-framework' ),
				'required' => array( 'single_post_sharebox', '=', '1' ),
				'subtitle' => esc_html__( 'Check to enable Twitter in social sharing box', 'evatheme-redux-framework' ),
				'default' => '1'
			),
			array(
				'id' => 'single_post_sharebox_google',
				'type' => 'switch',
				'title' => esc_html__( 'Google plus', 'evatheme-redux-framework' ),
				'required' => array( 'single_post_sharebox', '=', '1' ),
				'subtitle' => esc_html__( 'Check to enable Google plus in social sharing box', 'evatheme-redux-framework' ),
				'default' => '1'
			),
			array(
				'id' => 'single_post_sharebox_pinterest',
				'type' => 'switch',
				'title' => esc_html__( 'Pinterest', 'evatheme-redux-framework' ),
				'required' => array( 'single_post_sharebox', '=', '1' ),
				'subtitle' => esc_html__( 'Check to enable Pinterest in social sharing box', 'evatheme-redux-framework' ),
				'default' => '1'
			),
			array(
				'id'     => 'section-blogsingle-sharebox-end',
				'type'   => 'section',
				'indent' => false,
			),
			array(
				'id'     	=> 'section-blogsingle-elements-start',
				'type'   	=> 'section',
				'title' 	=> esc_html__( 'Single Post Elements', 'evatheme-redux-framework' ),
				'indent' 	=> true,
			),
			array(
				'id' => 'single_post_author',
				'type' => 'switch',
				'title' => esc_html__( 'Author Box', 'evatheme-redux-framework' ),
				'subtitle' => esc_html__( 'Enable Author Box on single post view?', 'evatheme-redux-framework' ),
				'default' => '1'
			),
			array(
				'id' => 'single_post_navigation',
				'type' => 'switch',
				'title' => esc_html__( 'Post Navigation', 'evatheme-redux-framework' ),
				'subtitle' => esc_html__( 'Enable Navigation on single post view?', 'evatheme-redux-framework' ),
				'default' => '1'
			),
			array(
				'id' => 'single_post_related_posts',
				'type' => 'switch',
				'title' => esc_html__( 'Related Posts', 'evatheme-redux-framework' ),
				'subtitle' => esc_html__( 'Enable Related Posts on single post view?', 'evatheme-redux-framework' ),
				'default' => '1'
			),
			array(
				'id' => 'single_post_related_posts_title',
				'type' => 'text',
				'title' => esc_html__( 'Related Posts Title', 'evatheme-redux-framework' ),
				'required' => array( 'single_post_related_posts', '=', '1' ),
				'default' => 'Related Posts'
			),
			array(
				'id'     => 'section-blogsingle-elements-end',
				'type'   => 'section',
				'indent' => false,
			),
		)
	) );
	
	
	//	Sidebars
	$fields = get_option('newsider_option');
	if (is_array($fields) && array_key_exists ('unlimited_sidebar', $fields)) {
		$options = $fields['unlimited_sidebar'];
		$sidebars =  array();
		if ($options != null) {
			foreach($options as $sidebar) {
				$sidebars[$sidebar] = $sidebar;
			}
		} else {
			$sidebars = null;
		}
	} else {
		$sidebars = null;
	}
		
	Redux::setSection( $opt_name, array(
		'title'			=> esc_html__( 'SideBar', 'evatheme-redux-framework' ),
		'id'			=> 'unlimited-sideBar',
		'customizer_width' => '400px',
        'icon'			=> 'dashicons dashicons-align-left',
		'fields'		=> array(
			array(
				'id'		=> 'sidebars-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Sidebar Generator', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
				'id'		=> 'info_sidebars',
				'type'		=> 'info',
				'title'		=> esc_html__( 'In this option panel, you can create and add unlimited number of sidebar.', 'evatheme-redux-framework' ) . '<br>' . esc_html__( 'Give a name to your sidebar, add it, then save changes.', 'evatheme-redux-framework' ) . '<br>' . esc_html__( 'Your new created sidebar will appear on the widget page', 'evatheme-redux-framework' ) . ' (<a href="'. admin_url( "widgets.php", "http" ) . '">' . esc_html__( 'widget pages', 'evatheme-redux-framework' ) . '</a>) ' . esc_html__( 'Manage it by adding widget as usual.', 'evatheme-redux-framework' ) . '<br>' . esc_html__( 'To set the sidebar, you will see on post, portfolio and page a custom meta box named Sidebar. Select the name of the desired sidebar that you want to display on the current post/page.', 'evatheme-redux-framework' ),
				'icon'		=> 'el-icon-info-sign',
			),
			array(
				'id'		=> 'sidebars-end',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Sidebar Generator', 'evatheme-redux-framework' ),
				'indent'	=> false,
			),
			array(
				'id'		=> 'sidebars2-start',
				'type'		=> 'section',
				'indent'	=> true,
			),
			array(
				'id'		=> 'unlimited_sidebar',
				'type'		=> 'multi_text',
				'title'		=> esc_html__( 'Sidebar generator', 'evatheme-redux-framework' ),
				'validate'	=> 'no_html',
				'subtitle'	=> esc_html__( 'Enter a name ', 'evatheme-redux-framework' ) . '<strong>' . esc_html__( '(Without special characters)', 'evatheme-redux-framework' ) . '</strong>' . esc_html__( ' for the sidebar, then click on ', 'evatheme-redux-framework' ) . '<i>' . esc_html__( '"add more"', 'evatheme-redux-framework' ) . '</i>' . esc_html__( ' to add a new sidebar.', 'evatheme-redux-framework' ),
				'default'	=> '',
			),
			array(
				'id'		=> 'sidebars-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
			
			array(
				'id'		=> 'section-theia_sticky_sidebar-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Sidebar Settings', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
				'id'		=> 'theia_sticky_sidebar',
				'type'		=> 'switch',
				'title'		=> esc_html__( 'Theia Sticky Sidebar', 'evatheme-redux-framework' ),
				'subtitle'	=> esc_html__( 'Enable this function for Fixed Sidebar for pages, posts, shop.', 'evatheme-redux-framework' ),
				'default'	=> '1'
			),
			array(
				'id'		=> 'section-theia_sticky_sidebar-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
			
			array(
				'id'		=> 'section-mc4wp-start',
				'type'		=> 'section',
				'title'		=> esc_html__( 'Mailchimp Widget Settings', 'evatheme-redux-framework' ),
				'indent'	=> true,
			),
			array(
				'id'        => 'mc4wp_widget_bg',
				'type'      => 'background',
				'title'     => esc_html__('Shop Page Title Background Color / Image', 'newsider'),
				'subtitle'  => esc_html__('Select the for shop page title background color or image.', 'newsider'),
				'default'   => array(
								'background-color' => '#f5f5f5',
							),
				'transparent' => false,
			),
			array(
				'id'        => 'mc4wp_widget_text_color',
				'type'      => 'color',
				'title'     => esc_html__('Title Text Color', 'newsider'),
				'subtitle'  => esc_html__('Title text color (default: #222222).', 'newsider'),
				'default'   => '#222222',
				'transparent' => false,
			),
			array(
				'id'		=> 'section-mc4wp-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
		)
	) );
	
	
	// Footer
    Redux::setSection( $opt_name, array(
		'title'			=> esc_html__( 'Footer', 'evatheme-redux-framework' ),
		'heading'		=> esc_html__( 'Footer Settings', 'evatheme-redux-framework' ),
		'id'			=> 'footer',
		'icon'			=> 'dashicons dashicons-align-center',
		'fields'		=> array(
			array(
				'id'     	=> 'section-footer_instagram-start',
				'type'   	=> 'section',
				'indent' 	=> true,
			),
			array(
				'id' => 'footer_instagram_username',
				'type' => 'text',
				'title' => esc_html__( 'Instagram username', 'evatheme-redux-framework' ),
				'default' => 'evathemefoto'
			),
			array(
				'id'     => 'section-footer_instagram-end',
				'type'   => 'section',
				'indent' => false,
			),
			
			array(
				'id'     	=> 'section-footer_posts_carousel-start',
				'type'   	=> 'section',
				'title' 	=> esc_html__( 'Posts Carousel', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Settings for the carousel with the posts at the footer', 'evatheme-redux-framework' ),
				'indent' 	=> true,
			),
			array(
				'id'		=> 'footer_posts_carousel',
				'type'		=> 'switch',
				'title'		=> esc_html__( 'Enabled Posts Carousel', 'evatheme-redux-framework' ),
				'default'	=> '1'
			),
			array(
				'id' => 'footer_posts_carousel_title',
				'type' => 'text',
				'title' => esc_html__( 'Carousel Title', 'evatheme-redux-framework' ),
				'required'	=> array( 'footer_posts_carousel', '=', '1' ),
				'default' => 'Do not miss'
			),
			array(
				'id'        => 'footer_posts_carousel_categories',
				'type'      => 'select',
				'multi' 	=> true,
				'title'     => esc_html__('Select a category', 'evatheme-redux-framework'),
				'required'	=> array( 'footer_posts_carousel', '=', '1' ),
				'data' => 'terms',
				'args' => array('taxonomies' => 'category', 'args' => array('hide_empty' => 0)),
				'default' => '0'
			),
			array(
				'id'     => 'section-footer_posts_carousel-end',
				'type'   => 'section',
				'indent' => false,
			),
			
			array(
				'id'     	=> 'section-prefooter-start',
				'type'   	=> 'section',
				'title' 	=> esc_html__( 'Prefooter Styles', 'evatheme-redux-framework' ),
				'indent' 	=> true,
			),
			array(
				'id' => 'prefooter',
				'type' => 'select',
				'title' => esc_html__( 'Prefooter Area', 'evatheme-redux-framework' ),
				'subtitle' => esc_html__( 'Check this option if you want show prefooter area with widgets', 'evatheme-redux-framework' ),
				'options' => array(
					'show' => esc_html__( 'Show', 'evatheme-redux-framework' ),
					'hide' => esc_html__( 'Hide', 'evatheme-redux-framework' ),
				),
				'default' => 'hide',
			),
			array(
				'id' => 'prefooter_col',
				'type' => 'image_select',
				'title' => esc_html__( 'Prefooter Columns', 'evatheme-redux-framework' ),
				'subtitle' => esc_html__( 'Choose the number of desired column in the prefooter widget area.', 'evatheme-redux-framework' ),
				'required' => array('prefooter', 'equals', array('show')),
				'options' => array(
					'12' => array('alt' => '1 Columns', 'img' => plugin_dir_url( __FILE__ ) . 'img/1col.png'),
					'6-6' => array('alt' => '2 Columns', 'img' => plugin_dir_url( __FILE__ ) . 'img/2col.png'),
					'4-4-4' => array('alt' => '3 Columns', 'img' => plugin_dir_url( __FILE__ ) . 'img/3col.png'),
					'3-3-3-3' => array('alt' => '4 Columns', 'img' => plugin_dir_url( __FILE__ ) . 'img/4col.png')
				),
				'default' => '4-4-4'
			),
			array(
				'id'     => 'section-prefooter-end',
				'type'   => 'section',
				'indent' => false,
			),
			
		)
	) );
	
	
	// Social Icons
    Redux::setSection( $opt_name, array(
		'title'			=> esc_html__( 'Social Icons', 'evatheme-redux-framework' ),
		'heading'		=> esc_html__( 'Social Icons Settings', 'evatheme-redux-framework' ),
		'id'			=> 'social-icons',
		'icon'			=> 'dashicons dashicons-share',
		'fields'		=> array(
			array(
				'id'		=> 'section-social-icons-start',
				'type'		=> 'section',
				'indent'	=> true,
			),
			array(
				'id'		=> 'facebook_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Facebook', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Facebook icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> "http://www.facebook.com/evatheme"
			),
			array(
				'id'		=> 'twitter_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Twitter', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Twitter icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> "http://twitter.com/EVATHEME"
			),
			array(
				'id'		=> 'linkedin_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'LinkedIn', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for LinkedIn icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'pinterest_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Pinterest', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Pinterest icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'googleplus_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Google Plus+', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Google Plus+ icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> "https://plus.google.com/u/0/share?url=http://www.evatheme.com"
			),
			array(
				'id'		=> 'youtube_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Youtube', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Youtube icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'rss_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'RSS', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for RSS icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'tumblr_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Tumblr', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Tumblr icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'reddit_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Reddit', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Reddit icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'dribbble_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Dribbble', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Dribbble icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'digg_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Digg', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Digg icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'flickr_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Flickr', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Flickr icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'instagram_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Instagram', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Instagram icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'vimeo_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Vimeo', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Vimeo icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'skype_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Skype', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Skype icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'yahoo_link',
				'type'     	=> 'text',
				'title' 	=> esc_html__( 'Yahoo', 'evatheme-redux-framework' ),
				'subtitle' 	=> esc_html__( 'Enter the link for Yahoo icon to appear. To remove it, just leave it blank.', 'evatheme-redux-framework' ),
				'default' 	=> ""
			),
			array(
				'id'		=> 'section-social-icons-end',
				'type'		=> 'section',
				'indent'	=> false,
			),
		)
	) );

	
	Redux::setSection( $opt_name, array(
		'title'			=> '',
		'type'			=> 'divide',
	) );
	
	
	function encodeURIComponent($str) {
		$revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
		return strtr(rawurlencode($str), $revert);
	}
	$theme_doc = '<iframe src="http://newsider.evatheme.com/docs/" style="width:90%;height:800px"></iframe>';
	
	Redux::setSection( $opt_name, array(
		'icon'			=> 'dashicons dashicons-book-alt',
		'title'			=> esc_html__( 'Documentation', 'evatheme-redux-framework' ),
		'heading'		=> esc_html__( 'Newsider Documentation', 'evatheme-redux-framework' ), 
		'fields'		=> array(
			array(
				'id'		=> '1000',
				'type'		=> 'raw',
				'content'	=> $theme_doc,
			)
		),
	) );
	
    
    /*
     * <--- END SECTIONS
     */


    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => esc_html__( 'Section via hook', 'evatheme-redux-framework' ),
                'desc'   => esc_html__( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'evatheme-redux-framework' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

