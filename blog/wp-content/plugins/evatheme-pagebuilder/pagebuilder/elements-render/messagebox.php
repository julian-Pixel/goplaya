<?php
/**
 *	Messagebox Shortcode
 */

function cstheme_messagebox($atts, $content) {
    $output  = cstheme_item($atts,"cstheme-messagebox");
    $output .= do_shortcode($content);
    $output .= '</div>';
    return $output;
}
add_shortcode('cs_message_box', 'cstheme_messagebox');


function cstheme_messagebox_item($atts, $content) {
    $atts = shortcode_atts( array(
        'size'  => 'col-md-3',
        'class' => '',
        'title' => '',
        'type' => 'default'
    ), $atts );
	
	
    $class = '';
	
    $output  = '<div class="alert alert-'. $atts['type'] . $class .'"><button type="button" class="close" data-dismiss="alert">&times;</button>';
        $output .= do_shortcode($content);
    $output .= '</div>';

    return $output;
}
add_shortcode('cs_message_box_item', 'cstheme_messagebox_item');