<?php
/**
 *	Button Shortcode
 */

function cstheme_button($atts, $content) {
    
    $link = !empty($atts['link']) ? $atts['link'] : '#';
    $target = !empty($atts['target']) ? ($atts['target']) : '_blank';
    $color = '';

    if ( !empty($atts['color'])) {
        $color .= 'background-color:' . $atts['color'];
        if ($atts['color'] == '#fff' || $atts['color'] == '#ffffff' || $atts['color'] == 'white')
            $whitecolor = ' white-button';
    }
    return '<a href="' . $link . '" target="' . $target . '" class="btn' . $whitecolor . '"' . $color . '>' . $content . '<span></span></a>';
}

add_shortcode('cs_button', 'cstheme_button');
