<?php
/**
 *	Testimonials Shortcode
 */

function cstheme_testimonials($atts, $content) {
    $atts = shortcode_atts( array(
        'size'  			=> 'col-md-4',
        'class' 			=> '',
		'margin_bottom'		=> '',
        'title' 			=> '',
		'title_size' 		=> '',
		'title_aligment' 	=> '',
    ), $atts );
	
    
    $output  = cstheme_item($atts);
        $output .= '<div class="cstheme-testimonials">';
			$output .= do_shortcode($content);
        $output .= '</div>';
    $output .= '</div>';

    return $output;
}
add_shortcode('cs_testimonials', 'cstheme_testimonials');


/**
 *	Testimonials item
 */

function cstheme_testimonials_item($atts, $content) {
    $atts = shortcode_atts( array(
        "name" 		=> "",
        "thumb" 	=> "",
        "url_name" 	=> "",
        "url" 		=> "",
    ), $atts );
    
	
	$output = '<div class="testimonial-item">';
		$output .= '<div class="testimonial_heading">';
			if(!empty($atts['thumb'])) {
				$output .= '<img src="' . esc_url( $atts['thumb'] ) . '" alt="' . $atts['name'] . '" />';
			}
			$output .= '<h5 class="testimonials_author">';
				if( !empty( $atts['url'] ) ) {
					$output .= '<a href="' . esc_url( $atts['url'] ) . '" target="_blank">';
				}
						$output .= esc_html( $atts['name'] );
				if( !empty( $atts['url'] ) ) {
					$output .= '</a>';
				}
				if(!empty($atts['url_name'])) {
					$output .= ', ';
				}
			$output .= '</h5>';
			if(!empty($atts['url_name'])) {
				$output .= '<a class="testimonials_author_link theme_color" href="' . esc_url( $atts['url'] ? $atts['url'] : '#' ) . '" target="_blank">' . esc_html( $atts['url_name'] ) . '</a>';
			}
		$output .= '</div>';
		$output .= '<div class="testimonial-content">';
			$output .= $content;
		$output .= '</div>';
	$output .= '</div>';

    return $output;
}
add_shortcode('cs_testimonials_item', 'cstheme_testimonials_item');