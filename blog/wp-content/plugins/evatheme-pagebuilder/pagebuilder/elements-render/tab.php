<?php
/**
 *	Tab container
 */

function cstheme_tab($atts, $content) {
    
	$position = (!empty($atts['position']) || $atts['position'] != 'top') ? (' tabs-' . $atts['position']) : '';
    
	$output =  cstheme_item($atts, 'cstheme-tab tabbable tab-init' . $position);
		$output .= do_shortcode($content);
		$output .= '<ul class="nav nav-tabs"></ul>';
		$output .= '<div class="tab-content"></div>';
	$output .= '</div>';
    
	return $output;
}

add_shortcode('cs_tab', 'cstheme_tab');


/**
 *	Tab Item
 */

function cstheme_tab_item($atts, $content) {
    
	global $cs_isshortcode;
	
    $cs_isshortcode = 'true';
    
	$atts = shortcode_atts(array(
        'title' => '',
	), $atts);
	
	
	$output = '<li>';
		$output .= '<a href="javascript:void(0);">';
			if (!empty($atts['title'])) {
				$output .= $atts['title'];
			}
		$output .= '</a>';
    $output .= '</li>';
    $output .= '<div class="tab-pane">';
		$output .= apply_filters("the_content", $content);
    $output .= '</div>';
    
	$cs_isshortcode = 'false';
    
	return $output;
}

add_shortcode('cs_tab_item', 'cstheme_tab_item');
