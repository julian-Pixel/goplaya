<?php
/**
 *	Service Shortcode
 */

if (!function_exists('shortcode_cs_service')) {
    function shortcode_cs_service($atts, $content) {
        $atts = shortcode_atts( array(
            'size'  			=> 'col-md-3',
            'class' 			=> '',
            'title' 			=> '',
			'title_size' 		=> '',
			'title_aligment' 	=> '',
			'margin_bottom'		=> '',
            "service_style" 	=> "style1",
            "thumb_type" 		=> "fi",
            "service_thumb_width" => "20",
            "service_thumb" 	=> "",
            "add_thumb" 		=> "Upload",
            // Font icon
            "fi" 				=> "",
            "fi_size" 			=> "30",
            "fi_padding" 		=> "20",
            "fi_color" 			=> "#aaaaaa",
            "fi_bg_color" 		=> "",
            "fi_border_color" 	=> "#aaaaaa",
            "fi_rounded" 		=> "6",
            "fi_rounded_size" 	=> "3",
            "fi_icon" 			=> "fa-glass",
            // -----------------------
            "service_title" 	=> "Your Service Title",
            "more_text" 		=> "Read More",
            "more_url" 			=> "",
            "more_target" 		=> "_blank"
        ), $atts );
        
		
        $thumb = $class = '';
        $thumbType = isset($atts['thumb_type']) ? $atts['thumb_type'] : 'fi';
        $class .= $atts['service_style'] . ' ';

        if ($thumbType === 'image') {
            $thumb = isset($atts['service_thumb']) ? '<img title="' . $atts['service_title'] . '" width="' . $atts['service_thumb_width'] . '" src="' . $atts['service_thumb'] . '" />' : '';
			$class .= 'image ';
        } elseif ($thumbType === 'fi') {
            $thumb = do_shortcode('[cs_fonticon size="cstheme-shortcode" fi_size="' . $atts['fi_size'] . '" fi_padding="' . $atts['fi_padding'] . '" fi_color="' . $atts['fi_color'] . '" fi_bg_color="' . $atts['fi_bg_color'] . '" fi_border_color="' . $atts['fi_border_color'] . '" fi_rounded="' . $atts['fi_rounded'] . '" fi_rounded_size="' . $atts['fi_rounded_size'] . '" fi_icon="' . $atts['fi_icon'] . '"]');
			$class .= 'fonticon ';
        } else {
			$class .= 'no_icon ';
		}
        
        $output  = cstheme_item($atts,'','');
			$output .= '<div class="cstheme-service-box ' . $class . '">';
				if ( $thumbType != 'no_icon' ) {
					$output .= '<div class="cs-service-icon">' . $thumb . '</div>';
				}
				$output .= '<h4>' . $atts['service_title'] . '</h4>';
				if ( $thumbType === 'no_icon' ) {
					$output .= '<span class="cs-service-sep bg_primary"></span>';
				}
				if ( do_shortcode( $content ) != '' ) {
					$output .= '<p>' . do_shortcode($content) . '</p>';
				}
			$output .= "</div>";
        $output .= "</div>";
		
        return $output;
    }

}
add_shortcode('cs_service', 'shortcode_cs_service');