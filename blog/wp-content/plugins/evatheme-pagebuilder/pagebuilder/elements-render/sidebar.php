<?php
/**
 *	Sidebar Shortcode
 */

if (!function_exists('shortcode_cs_sidebar')) {
    function shortcode_cs_sidebar($atts, $content) {
        
		$output  = cstheme_item($atts);
            
			ob_start();
            
				echo '<div class="sidebar">';
                    if (!dynamic_sidebar($atts['name'])) {
                        print 'There is no widget. You should add your widgets into <strong>';
                        print $atts['name'];
                        print '</strong> sidebar area on <strong>Appearance => Widgets</strong> of your dashboard. <br/><br/>';
                    }
                echo '</div>';
			
			$output .= ob_get_clean();
			
        $output .= '</div>';
        
		return $output;
    }
}
add_shortcode('cs_sidebar', 'shortcode_cs_sidebar');