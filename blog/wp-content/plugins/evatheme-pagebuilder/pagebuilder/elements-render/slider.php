<?php
/**
 *	Slider Shortcode
 */

function cstheme_slider($atts, $content) {
	
	global $post, $cs_isshortcode;
    $cs_isshortcode='true';
    
	$atts = shortcode_atts(array(
        'size'  			=> 'col-md-3',
        'class' 			=> '',
        'title' 			=> '',
		'title_size' 		=> '',
		'title_aligment' 	=> '',
		'margin_bottom'		=> '',
		'style'				=> 'style1',
        'slides_count'    	=> '4',
        'slider_columns'    => 'col3',
		'orderby' 			=> '',
        'category_ids'  	=> '',
    ), $atts);
    
	
	wp_enqueue_script('flexslider', CSTHEME_PLUGIN_URL . 'assets/js/jquery.flexslider-min.js', array(), '2.6.3', true);
	wp_enqueue_style('newsider-flexslider', CSTHEME_PLUGIN_URL . 'assets/css/cstheme-flexslider.css');
	
	
	$output = cstheme_item( $atts, 'evapb_top_slider' );
		
		$top_slider_style = $atts['style'];
		$top_slides_count = $atts['slides_count'];
		$top_slider_columns = $atts['slider_columns'];
		if( $top_slider_style == 'style3' ){
			$top_slides_count = '3';
		}
		$evapb_date_format = get_option( 'date_format' );
		
		$query = Array(
			'post_type' 		=> 'post',
			'posts_per_page' 	=> $top_slides_count,
			'post_status' 		=> 'publish',
			'ignore_sticky_posts' => 1
		);
		
		$cats = empty($atts['category_ids']) ? false : explode(",", $atts['category_ids']);
		if ($cats) {
			$query['tax_query'] = Array(Array(
					'taxonomy' 	=> 'category',
					'terms' 	=> $cats,
					'field' 	=> 'slug'
				)
			);
		}
		if (!empty($atts['orderby'])) {
            switch ($atts['orderby']) {
                case "date_asc":
                    $query['orderby'] = 'date';
                    $query['order'] = 'ASC';
                    break;
                case "title_asc":
                    $query['orderby'] = 'title';
                    $query['order'] = 'ASC';
                    break;
                case "title_desc":
                    $query['orderby'] = 'title';
                    $query['order'] = 'DESC';
                    break;
                case "random":
                    $query['orderby'] = 'rand';
                    break;
				case "comment_count":
                    $query['orderby'] = 'comment_count';
                    break;
            }
        }
		
		$top_slider_id = uniqid('top_slider_id');
		
		ob_start();
		query_posts($query);
		
		if (have_posts()) {
			
			if( $top_slider_style == 'style2' ){
			
				echo '<div class="top_slider_wrap style2 clearfix">';
					
					echo '<div class="top_slider_preloader"><div class="top_slider_preloader_in"></div></div>';
					
					echo '<div class="top_slider flexslider style2 ' . $top_slider_id . '">';
						echo '<ul class="slides">';
						
							while (have_posts()) { the_post();
								
								$categories = get_the_category($post->ID);
								$categories_slug = $categories[0]->slug;
								$newsider_featured_image_url = newsider_aq_resize( wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ), 990, 550, true, true, true );
								?>
								
								<li>
									<div class="top_slider_slide category-<?php echo $categories_slug; ?>">
										<div class="featured_img_bg" 
											<?php if( ! empty ( $newsider_featured_image_url ) ) { ?>
												style="background-image: url(<?php echo esc_url( $newsider_featured_image_url ); ?>);"
											<?php } ?>
										></div>
										<div class="post_share_wrap">
											<i class="post_share_btn"></i>
											<?php get_template_part( 'templates/blog/sharebox' ); ?>
										</div>
										<div class="top_slider_descr">
											<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
											<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
											<div class="post_meta">
												<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
												<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
												<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
											</div>
										</div>
									</div>
								</li>
								
								<?php

							}
							
						echo '</ul>';
					echo '</div>';
					
					echo '
						<div id="top_slider_thumb" class="flexslider thumbnails style2">
							<ul class="slides">
					';
							
								while (have_posts()) { the_post();
									
									$newsider_featured_image_url2 = newsider_aq_resize( wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ), 990, 550, true, true, true );
									
									echo '<li>
											<div class="top_slider_thumb_img">';
										if( ! empty ( $newsider_featured_image_url2 ) ) {
											echo '<img src="' . newsider_aq_resize( $newsider_featured_image_url2, 160, 80, true, true, true) . '" alt="' . get_the_title() . '" />';
										}
									echo '</div>
										<h2>' . get_the_title() . '</h2>
									</li>';
									
								}
					
				echo '
							</ul>
						</div>
					</div>
				';
				
			} else if( $top_slider_style == 'style3' ){
				
				$i = 1;
				
				echo '<div class="top_slide_wrap clearfix style3">';
				
					while (have_posts()) { the_post();
						
						$newsider_top_slider_class = '';
						
						if ( $i == 1 ) {
							$newsider_featured_image_url = newsider_aq_resize( wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ), 690, 650, true, true, true );
							$newsider_top_slider_class = 'big_post';
						} else {
							$newsider_featured_image_url = newsider_aq_resize( wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ), 470, 320, true, true, true );
							$newsider_top_slider_class = 'small_post';
						}
							
						?>
						
							<div <?php post_class( $newsider_top_slider_class ); ?>>
								<div class="featured_img_bg" 
									<?php if( ! empty( $newsider_featured_image_url ) ) { ?>
										style="background-image:url(<?php echo $newsider_featured_image_url ?>);"
									<?php } ?>
								></div>
								<div class="post_share_wrap">
									<i class="post_share_btn"></i>
									<?php get_template_part( 'templates/blog/sharebox' ); ?>
								</div>
								<div class="top_slider_descr">
									<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
									<div class="clearfix">
										<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
										<div class="post_meta">
											<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
										</div>
									</div>
								</div>
							</div>
							
						<?php
			
						$i++;

					}
					
				echo '</div>';
				
			} else if( $top_slider_style == 'style4' ){
					
				echo '<div class="top_slider flexslider style4 ' . $top_slider_id . '">';
				
					echo '<div class="top_slider_preloader"><div class="top_slider_preloader_in"></div></div>';
				
					echo '<ul class="slides">';
					
						while (have_posts()) { the_post();
							
							$categories = get_the_category($post->ID);
							$categories_slug = $categories[0]->slug;
							$newsider_featured_image_url = newsider_aq_resize( wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ), 990, 550, true, true, true );
							?>
							
							<li>
								<div class="top_slider_slide category-<?php echo $categories_slug; ?>">
									<div class="featured_img_bg" 
										<?php if( ! empty ( $newsider_featured_image_url ) ) { ?>
											style="background-image: url(<?php echo esc_url( $newsider_featured_image_url ); ?>);"
										<?php } ?>
									></div>
									<div class="top_slider_descr">
										<div class="post_meta">
											<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
											<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
											<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
										</div>
										<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
										<div class="post_meta">
											<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
										</div>
									</div>
								</div>
							</li>
							
							<?php

						}
						
					echo '</ul>';
				echo '</div>';
				
			} else if( $top_slider_style == 'style5' ){
					
				echo '<div class="top_slider flexslider style5 ' . $top_slider_id . '">';
					
					echo '<div class="top_slider_preloader"><div class="top_slider_preloader_in"></div></div>';
					
					echo '<ul class="slides">';
					
						while (have_posts()) { the_post();
							
							$categories = get_the_category($post->ID);
							$categories_slug = $categories[0]->slug;
							$newsider_featured_image_url = newsider_aq_resize( wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ), 990, 550, true, true, true );
							?>
							
							<li>
								<div class="top_slider_slide category-<?php echo $categories_slug; ?>">
									<div class="featured_img_bg" 
										<?php if( ! empty ( $newsider_featured_image_url ) ) { ?>
											style="background-image: url(<?php echo esc_url( $newsider_featured_image_url ); ?>);"
										<?php } ?>
									></div>
									<div class="container">
										<div class="post_share_wrap">
											<i class="post_share_btn"></i>
											<?php get_template_part( 'templates/blog/sharebox' ); ?>
										</div>
										<div class="top_slider_descr">
											<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
											<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
											<div class="post_meta">
												<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
												<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
												<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
											</div>
										</div>
									</div>
								</div>
							</li>
							
							<?php

						}
						
					echo '</ul>
				</div>';
				
			} else if( $top_slider_style == 'style6' ){
					
				echo '<div class="top_slider style6 ' . $top_slider_columns . ' ' . $top_slider_id . '">';
					
					echo '<div class="top_slider_preloader"><div class="top_slider_preloader_in"></div></div>';
					
					echo '<div class="owl-carousel">';
					
						while (have_posts()) { the_post();
							
							$categories = get_the_category($post->ID);
							$categories_slug = $categories[0]->slug;
							$newsider_featured_image_url = newsider_aq_resize( wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ), 480, 660, true, true, true );
							?>
							
							<div class="item">
								<div class="top_slider_slide category-<?php echo $categories_slug; ?>">
									<div class="featured_img_bg" 
										<?php if( ! empty ( $newsider_featured_image_url ) ) { ?>
											style="background-image: url(<?php echo esc_url( $newsider_featured_image_url ); ?>);"
										<?php } ?>
									></div>
									<div class="post_share_wrap">
										<i class="post_share_btn"></i>
										<?php get_template_part( 'templates/blog/sharebox' ); ?>
									</div>
									<div class="top_slider_descr">
										<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
										<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
										<div class="post_meta">
											<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
											<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
											<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
										</div>
									</div>
								</div>
							</div>
							
							<?php

						}
						
					echo '</div>
				</div>';
				
			} else {
				
				echo '<div class="top_slider flexslider style1 ' . $top_slider_id . '">';
					
					echo '<div class="top_slider_preloader"><div class="top_slider_preloader_in"></div></div>';
					
					echo '<ul class="slides">';
					
						while (have_posts()) { the_post();
							
							$categories = get_the_category($post->ID);
							$categories_slug = $categories[0]->slug;
							$newsider_featured_image_url = newsider_aq_resize( wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ), 1170, 550, true, true, true );
							?>
							
							<li>
								<div class="top_slider_slide category-<?php echo $categories_slug; ?>">
									<div class="featured_img_bg" 
										<?php if( ! empty ( $newsider_featured_image_url ) ) { ?>
											style="background-image: url(<?php echo esc_url( $newsider_featured_image_url ); ?>);"
										<?php } ?>
									></div>
									<div class="post_share_wrap">
										<i class="post_share_btn"></i>
										<?php get_template_part( 'templates/blog/sharebox' ); ?>
									</div>
									<div class="top_slider_descr">
										<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
										<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
										<div class="post_meta">
											<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
											<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
										</div>
									</div>
								</div>
							</li>
							
							<?php

						}
						
					echo '</ul>';
				echo '</div>';
				
				echo '
					<div id="top_slider_thumb" class="flexslider thumbnails style1">
						<ul class="slides">
				';
						
							while (have_posts()) { the_post();
								
								echo '<li>
									<h2>' . get_the_title() . '</h2>
								</li>';
								
							}
				
				echo '
						</ul>
					</div>
				';
				
			}
			
		}
		
		wp_reset_query();
		$output .= ob_get_clean();
		
    $output .= '</div>';
    
	$cs_isshortcode = 'false';
	
	return $output;
	
}
add_shortcode('cs_slider', 'cstheme_slider');