<?php
/**
 *	Column Shortcode
 */

function cstheme_column($atts, $content){
    
	global $cs_isshortcode;
    $cs_isshortcode='true';
    
	$output  = cstheme_item($atts, 'evapb-column');
        $output .= apply_filters("the_content", $content);
    $output .= '</div>';
    
	$cs_isshortcode='false';
    
	return $output;
	
}
add_shortcode('cs_column', 'cstheme_column');