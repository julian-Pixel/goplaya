<?php
/**
 *	Pricing Table Shortcode
 */

function cstheme_pricing($atts, $content) {
    global $parentAtts;
    
	$atts = shortcode_atts( array(
        'size'  			=> 'col-md-3',
        'class' 			=> '',
        'title' 			=> '',
		'title_size' 		=> '',
		'title_aligment' 	=> '',
		'margin_bottom'		=> '',
        'symbol' 			=> '',
        'column_count' 		=> '3'
    ), $atts );
	
	
    $parentAtts = $atts;
    
    $output  = cstheme_item($atts,'cstheme-pricing');
		$output .= '<div class="cstheme-pricing-list row">';
			$output .= do_shortcode($content);
        $output .= '</div>';
    $output .= '</div>';
    return $output;
}
add_shortcode('cs_pricing', 'cstheme_pricing');


function cstheme_pricing_item($atts, $content) {
    global $parentAtts;
    
	$atts = shortcode_atts( array(
		'title' 			=> '',
		'sub_title' 		=> '',
		'featured' 			=> 'false',
		'price' 			=> '',
		'time' 				=> '',
		'price_feature1' 	=> '',
		'price_feature2' 	=> '',
		'price_feature3' 	=> '',
		'price_feature4' 	=> '',
		'buttontext' 		=> '',
		'buttonlink' 		=> ''
    ), $atts );
	
	
    $class = '';
	
    if($atts['featured'] == 'true') {
        $class.=' featured';
    }
    
    $symbol = !empty($parentAtts['symbol']) ? $parentAtts['symbol'] : '$';
    switch($parentAtts['column_count']){
        case '2' :{ $class .=' col-md-6'; break; }
        case '3' :{ $class .=' col-md-4'; break; }
        case '4' :{ $class .=' col-md-3'; break; }
    }
    
	$output = '<div class="pricing-column mb50 ' . $class . '">';
        $output .= '<div class="pricing-box">';
            $output .= '<div class="pricing-header">';
                $output .= '<h3>'. $atts['title'] .'</h3>';
                $output .= '<p>'. $atts['sub_title'] .'</p>';
            $output .= '</div>';
            $output .= '<div class="pricing-top">';
                $output .= '<h4 class="theme_color"><span>'. $symbol .'</span> '. $atts['price'] .'</h4><h5 class="theme_color">/ '. $atts['time'] .'</h5>';
            $output .= '</div>';
            $output .= '<ul class="pricing-feature">';
                $output .= '<li>'. $atts['price_feature1'] .'</li>';
                $output .= '<li>'. $atts['price_feature2'] .'</li>';
                $output .= '<li>'. $atts['price_feature3'] .'</li>';
                $output .= '<li>'. $atts['price_feature4'] .'</li>';
            $output .= '</ul>';
            if ( !empty( $atts['buttontext'] ) ) {
				$output .= '<div class="pricing-footer">';
					$output .= '<a class="price_btn bg_primary" href="'. esc_url( $atts['buttonlink'] ) .'">'. $atts['buttontext'] .'</a>';
				$output .= '</div>';
            }
        $output .= '</div>';
    $output .= '</div>';
	
    return $output;
}
add_shortcode('cs_pricing_item', 'cstheme_pricing_item');