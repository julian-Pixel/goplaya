<?php
/**
 *	Progress Shortcode
 */

function cstheme_progress($atts, $content) {
    $output  = cstheme_item($atts);
        $output .= do_shortcode($content);
    $output .= '</div>';
    
    return $output;
}
add_shortcode('cs_progress', 'cstheme_progress');


/**
 *	Progress Shortcode
 */

function cstheme_progress_item($atts, $content) {
    $atts = shortcode_atts( array(
		'title' 	=> 'Progress',
		'type' 		=> 'light',
		'percent' 	=> '50',
		'color' 	=> ''
    ), $atts );
	
	
    $class = '';
    $class .= $atts['type'] . ' ';

    if ($atts['type'] == 'massive') {
		
		$output = '<div class="cstheme-progress ' . $class . '">';
			$output .= '<h5 class="progress-title bg_primary">'. $atts['title'] .'</h5>';
			$output .= '<div class="bar-container" data-width="'. $atts['percent'] .'">';
				$output .= '<div class="bar" data-percent="'. $atts['percent'] .' %" style="background-color:'. $atts['color'] .'"></div>';
			$output .= '</div>';
		$output .= '</div>';
		
	} else {
		
		$output = '<div class="cstheme-progress ' . $class . '">';
			$output .= '<h5 class="progress-title">'. $atts['title'] .'</h5>';
			$output .= '<div class="bar-container" data-width="'. $atts['percent'] .'">';
				$output .= '<div class="bar" style="background-color:'. $atts['color'] .'"></div>';
				$output .= '<span class="bar_percent">' . $atts['percent'] . ' %</span>';
			$output .= '</div>';
		$output .= '</div>';
		
	}
	
    return $output;
}
add_shortcode('cs_progress_item', 'cstheme_progress_item');