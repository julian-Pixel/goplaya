<?php
/**
 *	Blog Shortcode
 */

function cstheme_blog($atts, $content) {
	
	global $post, $newsider_post_first_big, $cs_isshortcode, $posts_style, $evapb_date_format;
    $cs_isshortcode='true';
    
	$atts = shortcode_atts(array(
        'size'  				=> 'col-md-3',
        'class' 				=> '',
        'title' 				=> '',
		'title_size' 			=> '',
		'title_aligment' 		=> '',
		'margin_bottom'			=> '',
		'posts_style'			=> '',
		'post_first_big'		=> 'false',
		'post_listing'			=> 'grid',
		'posts_top_img_style'	=> 'style1',
        'post_count'    		=> get_option('posts_per_page'),
		'post_per_line' 		=> '3',
		'orderby' 				=> '',
		'post_featured_img' 	=> 'show',
		'post_excerpt_count' 	=> '100',
        'category_ids'  		=> '',
		'pagination' 			=> 'true',
		'pagination_alignment' 	=> 'left',
    ), $atts);

    
	$bloglist_unique_id = uniqid('bloglist_id');
	
	$posts_style 				= $atts['posts_style'];
	$post_listing 				= $atts['post_listing'];
	$posts_top_img_style 		= $atts['posts_top_img_style'];
	$newsider_post_first_big 	= $atts['post_first_big'];
	$evapb_post_per_line 		= $atts['post_per_line'];
	$evapb_pagination_alignment = $atts['pagination_alignment'];
	
	$evapb_date_format = get_option( 'date_format' );
	
	$posts_top_img_style_class = '';
	if( $posts_style == 'top_img' ) {
		$posts_top_img_style_class = $posts_top_img_style;
	}
	$bloglist_class = '';
	$bloglist_class .= ' ' . $posts_style . ' ' . $posts_top_img_style_class;
	if( $posts_style != 'big_top_img' ){
		$bloglist_class .= ' ' . $post_listing;
		$bloglist_class .= ' columns' . $evapb_post_per_line;
	}
	
	$output = cstheme_item( $atts, 'evapb-bloglist ' . $bloglist_class . ' ' . $bloglist_unique_id . '' );
    
		if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
		
		$query = Array(
			'post_type' 		=> 'post',
			'posts_per_page' 	=> $atts['post_count'],
			'paged' 			=> $paged,
			'post_status' 		=> 'publish',
			'ignore_sticky_posts' => 1
		);
		$cats = empty($atts['category_ids']) ? false : explode(",", $atts['category_ids']);
		if ($cats) {
			$query['tax_query'] = Array(Array(
					'taxonomy' 	=> 'category',
					'terms' 	=> $cats,
					'field' 	=> 'slug'
				)
			);
		}
		if (!empty($atts['orderby'])) {
            switch ($atts['orderby']) {
                case "date_asc":
                    $query['orderby'] = 'date';
                    $query['order'] = 'ASC';
                    break;
                case "title_asc":
                    $query['orderby'] = 'title';
                    $query['order'] = 'ASC';
                    break;
                case "title_desc":
                    $query['orderby'] = 'title';
                    $query['order'] = 'DESC';
                    break;
                case "random":
                    $query['orderby'] = 'rand';
                    break;
				case "comment_count":
                    $query['orderby'] = 'comment_count';
                    break;
            }
        }
		
		$evapb_blog_class = '';
		
		if( $posts_style == 'left_image' || $post_listing == 'carousel' || $posts_style == 'big_bg_images' || $posts_style == 'big_top_img' ) {
			$evapb_blog_class .= ' col-md-12';
		} else {
			if( $evapb_post_per_line == '2' ) {
				$evapb_blog_class .= ' col-md-6';
			} elseif( $evapb_post_per_line == '3' ) {
				$evapb_blog_class .= ' col-md-4';
			} elseif( $evapb_post_per_line == '4' ) {
				$evapb_blog_class .= ' col-md-3';
			} elseif( $evapb_post_per_line == '5' ) {
				$evapb_blog_class .= ' col-md-3 col-md-2_5';
			} else {
				$evapb_blog_class .= ' col-md-4';
			}
		}
		if ( $post_listing == 'carousel' ) {
			$evapb_blog_class .= ' item';
		}
		
			$evapb_blog_class .= ' order' . $atts['orderby'] . ' ';
		
		
		$i = 1;
		
		ob_start();
		query_posts($query);
		if (have_posts()) {
			
			if ( $post_listing != 'carousel' ) {
				if ( $posts_style != 'big_top_img' ) {
					echo '<div class="row"><div class="isotope_container_wrap"><div class="isotope-container">';
				} else {
					echo '<div class="row">';
				}
			} else {
				echo '<div class="posts_carousel_list owl-carousel clearfix">';
			}
			
				while (have_posts()) { the_post();
					
					global $i, $newsider_featured_image_url, $newsider_post_excerpt, $newsider_pf;
					
					$newsider_pf = get_post_format();
					
					$evapb_big_post = '';
					if( $posts_style == 'left_image' ) {
						if( $i == 0 && $newsider_post_first_big == 'true' ) {
							$evapb_big_post .= ' big_post';
						}
					}
					
					if( $posts_style == 'big_bg_images' ) {
						if ( ($i % 3) == 0 ) {
							$evapb_big_post .= ' big_post';
						}
					}
					
					$newsider_featured_image_url 	= wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
					$post_excerpt_count 			= $atts['post_excerpt_count'];
					$newsider_post_excerpt 			= (newsider_smarty_modifier_truncate(get_the_excerpt(), $post_excerpt_count));
					$evapb_post_metro_class = '';
					if( $posts_style == 'metro' ){
						$newsider_post_metro = get_post_meta( $post->ID, 'newsider_post_metro', true );
						if( isset( $newsider_post_metro ) && $newsider_post_metro != '' ) {
							$evapb_post_metro_class = ' sizing_' . $newsider_post_metro;
						}
					}
					
					if( !empty( $newsider_featured_image_url ) ){
						$evapb_blog_class .= ' has_featured_image';
					} else {
						$evapb_blog_class .= ' no_featured_image';
					}
					
					?>
					
					<article <?php post_class( esc_html( $evapb_blog_class . ' ' . $evapb_big_post . ' ' . $evapb_post_metro_class . ' ' . $i ) ); ?>>
						
						<?php call_user_func('evapb_posts_style_' . $posts_style, $atts); ?>
						
					</article>
					
					<?php
					
					$i++;
				}
				$i = 0;
			if ( $post_listing != 'carousel' ) {
				if ( $posts_style != 'big_top_img' ) {
					echo '</div></div></div>';
				} else {
					echo '</div>';
				}
			} else {
				echo '</div>';
			}

			if ( $post_listing != 'carousel' ) {
				echo '<div class="text-' . $evapb_pagination_alignment . '">';
					if ($atts['pagination'] == 'true') {
						$output .= newsider_pagination();
					} elseif ($atts['pagination'] == 'infiniti') {
						$output .= newsider_infinite_scroll('query2');
					}
				echo '</div>';
			}
		}
		
		wp_reset_query();
		$output .= ob_get_clean();
		
    $output .= '</div>';
	
	if ( $post_listing == 'carousel' ) {
		$output .= '
			<script type="text/javascript">
				jQuery(window).load(function() {
					"use strict";
					
					jQuery(".evapb-bloglist.' . $bloglist_unique_id . ' .posts_carousel_list.owl-carousel").owlCarousel({
						margin: 30,
						dots: false,
						nav: true,
						navText: [
							"",
							"",
						],
						loop: false,
						autoplay: true,
						autoplaySpeed: 1000,
						autoplayTimeout: 3000,
						navSpeed: 1000,
						autoplayHoverPause: true,
						responsive: {
							0: {items: 1},
							481: {items: 2},
							769: {items: 3},
							1024: {items: 3},
							1920: {items: ' . $evapb_post_per_line . '},
						},
						thumbs: false,
						thumbImage: false,
					});
				});
			</script>
		';
	}
    
	$cs_isshortcode = 'false';
	
	return $output;
	
}
add_shortcode('cs_blog', 'cstheme_blog');


//	Left Image Style
if ( !function_exists('evapb_posts_style_left_image')) {
	function evapb_posts_style_left_image($atts) {
		
		global $post, $newsider_pf, $i, $newsider_featured_image_url, $evapb_date_format, $newsider_post_excerpt;
		
		$newsider_post_first_big = $atts['post_first_big'];
		$newsider_post_featured_img = $atts['post_featured_img'];
		$newsider_featured_image = '<img src="' . newsider_aq_resize( $newsider_featured_image_url, 440, 440, true, true, true) . '" alt="' . get_the_title() . '" width="440" height="440" />';
		
		?>
			
			<?php if ( $i == 0 && $newsider_post_first_big == 'true' ) { ?>

				<div class="post_content_wrapper clearfix">
					<?php if( !empty( $newsider_featured_image_url ) && $newsider_post_featured_img != 'hide' ) { ?>
						<div class="post_format_content">
							<?php echo '<a href="' . get_permalink() . '">';
								echo '<img src="' . newsider_aq_resize( $newsider_featured_image_url, 1170, 620, true, true, true) . '" alt="' . get_the_title() . '" width="1170" height="620" />';
								if ( $newsider_pf == 'video' ){
									echo '<span class="post_format_label video"></span>';
								}
							echo '</a>'; ?>
							<div class="post_share_wrap">
								<i class="post_share_btn"></i>
								<?php get_template_part( 'templates/blog/sharebox' ); ?>
							</div>
						</div>
					<?php } ?>
					<div class="post_descr_wrap clearfix">
						<div class="post_meta clearfix">
							<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
							<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
							<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
						</div>
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						<?php if( !empty( $newsider_post_excerpt ) ){ ?>
							<div class="post-content">
								<?php echo $newsider_post_excerpt; ?>
							</div>
						<?php } ?>
					</div>
				</div>
				
			<?php } else { ?>
			
				<div class="post_content_wrapper clearfix">
					<div class="row">
					<?php if( !empty( $newsider_featured_image_url ) && $newsider_post_featured_img != 'hide' ) { ?>
						<div class="col-sm-6 post_fetured_img_wrap">
							<div class="post_format_content">
								<?php echo '<a href="' . get_permalink() . '">';
									echo $newsider_featured_image;
									if ( $newsider_pf == 'video' ){
										echo '<span class="post_format_label video"></span>';
									}
								echo '</a>'; ?>
								<div class="post_share_wrap">
									<i class="post_share_btn"></i>
									<?php get_template_part( 'templates/blog/sharebox' ); ?>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
					<?php } else { ?>
						<div class="col-sm-12">
					<?php } ?>
							<div class="post_descr_wrap clearfix">
								<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
								<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
								<div class="post_meta clearfix">
									<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
									<span class="post_metaauthor"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
								</div>
								<?php if( !empty( $newsider_post_excerpt ) ){ ?>
								<div class="post-content">
									<?php echo $newsider_post_excerpt; ?>
								</div>
							<?php } ?>
							</div>
						</div>
					</div>
				</div>
			
			<?php } ?>
			
		<?php
	}
}


//	Background Image Style
if ( !function_exists('evapb_posts_style_bg_image')) {
	function evapb_posts_style_bg_image($atts) {
		
		global $post, $newsider_featured_image_url, $newsider_pf, $evapb_date_format;
		
		$newsider_post_quote_text = get_post_meta( $post->ID, 'newsider_post_quote_text', true );
		$newsider_post_quote_author = get_post_meta( $post->ID, 'newsider_post_quote_author', true );
		$newsider_post_link_url = get_post_meta( $post->ID, 'newsider_post_link_url', true );
		
		?>
			
			<div class="post_content_wrapper">
				<div class="featured_img_bg" 
					<?php if( ! empty( $newsider_featured_image_url ) ) { ?>
						style="background-image:url(<?php echo $newsider_featured_image_url ?>);"
					<?php } ?>
				></div>
				<a class="pf_label" href="<?php the_permalink(); ?>"></a>
				<div class="post_descr_wrap">
					<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
					<h2 class="post_title">
						<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
							<?php the_title(); ?>
						</a>
					</h2>
					<div class="post_meta clearfix">
						<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
						<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
					</div>
				</div>
			</div>
			
		<?php
	}
}


//	Card Style
if ( !function_exists('evapb_posts_style_card')) {
	function evapb_posts_style_card($atts) {
		
		global $post, $newsider_featured_image_url, $newsider_pf, $evapb_date_format;
		
		$evapb_post_per_line = $atts['post_per_line'];
		
		if( $evapb_post_per_line == '3' ){
			$newsider_width = 370;
			$newsider_height = 500;
		} else {
			$newsider_width = 570;
			$newsider_height = 770;
		}
		
		$newsider_post_quote_text = get_post_meta( $post->ID, 'newsider_post_quote_text', true );
		$newsider_post_quote_author = get_post_meta( $post->ID, 'newsider_post_quote_author', true );
		$newsider_post_link_url = get_post_meta( $post->ID, 'newsider_post_link_url', true );
		$newsider_featured_image_bg = newsider_aq_resize( $newsider_featured_image_url, $newsider_width, $newsider_height, true, true, true);
		
		?>
			
			<div class="post_content_wrapper clearfix mb30 <?php if( empty ( $newsider_featured_image_url ) ) { echo 'no_featured_img'; } ?>">
				
				<?php if($newsider_pf == 'quote') { ?>

					<div class="post_descr_wrap">
						<i class="post_format_quote_label"></i>
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
							<?php if (!empty($newsider_post_quote_text)) {
								echo esc_attr( $newsider_post_quote_text );
							} else {
								the_title();
							} ?>
						</a></h2>
						<p class="quote_author_name">
							<?php
								if ( !empty( $newsider_post_quote_author ) ) {
									echo esc_attr( $newsider_post_quote_author );
								} else {
									echo get_the_author_meta('display_name');
								}
							?>
						</p>
					</div>
					
				<?php } elseif ($newsider_pf == 'link') { ?>
					
					<div class="post_descr_wrap">
						<i class="post_format_link_label"></i>
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						<a class="pf_link_url" href="<?php
							if ( !empty( $newsider_post_link_url ) ) {
								echo esc_url( $newsider_post_link_url );
							} else {
								echo the_permalink();
							}
							?>" target="_blank"><?php
								if ( !empty( $newsider_post_link_url ) ) {
									echo esc_attr( $newsider_post_link_url );
								} else {
									echo get_the_author_meta('display_name');
								}
						?></a>
					</div>
					
				<?php } else { ?>
					
					<div class="featured_img_bg" 
						<?php if( ! empty ( $newsider_featured_image_url ) ) { ?>
							style="background-image: url(<?php echo esc_url( $newsider_featured_image_bg ); ?>);"
						<?php } ?>
					>
						<?php if ( $newsider_pf == 'video' ){
							echo '<span class="post_format_label video"></span>';
						} ?>
					</div>
					<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
					<div class="post_descr_wrap text-center">
						<div class="post_meta clearfix">
							<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
							<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
						</div>
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						<a class="btn_more" href="<?php the_permalink(); ?>"><?php echo esc_html__('Read More','evatheme-pagebuilder'); ?></a>
					</div>
					
				<?php } ?>
					
			</div>
			
		<?php
	}
}


//	Top Image Masonry
if ( !function_exists('evapb_posts_style_top_img')) {
	function evapb_posts_style_top_img($atts) {
		
		global $post, $newsider_featured_image_url, $newsider_post_excerpt, $newsider_pf, $evapb_date_format;
		
		$evapb_post_per_line 		= $atts['post_per_line'];
		$posts_top_img_style 		= $atts['posts_top_img_style'];
		
		if( $posts_top_img_style == 'style2' || $posts_top_img_style == 'style4' ) {
			if( $evapb_post_per_line == '2' ){
				$newsider_width = 570;
				$newsider_height = '';
			} else {
				$newsider_width = 370;
				$newsider_height = '';
			}
		} elseif( $posts_top_img_style == 'style3' ) {
			if( $evapb_post_per_line == '2' ){
				$newsider_width = 570;
				$newsider_height = 680;
			} else {
				$newsider_width = 370;
				$newsider_height = 440;
			}
		} else {
			if( $evapb_post_per_line == '2' ){
				$newsider_width = 570;
				$newsider_height = 370;
			} else {
				$newsider_width = 370;
				$newsider_height = 240;
			}
		}
		
		$newsider_post_quote_text = get_post_meta( $post->ID, 'newsider_post_quote_text', true );
		$newsider_post_quote_author = get_post_meta( $post->ID, 'newsider_post_quote_author', true );
		$newsider_post_link_url = get_post_meta( $post->ID, 'newsider_post_link_url', true );
		$newsider_post_featured_img = $atts['post_featured_img'];
		$newsider_featured_image = '<img src="' . newsider_aq_resize( $newsider_featured_image_url, $newsider_width, $newsider_height, true, true, true) . '" alt="' . get_the_title() . '" width="570" height="370" />';
		
		?>
			
			<div class="post_content_wrapper clearfix mb30">
				
				<?php if( $posts_top_img_style == 'style2' ) { ?>
				
					<?php if($newsider_pf == 'quote') { ?>

						<div class="post_descr_wrap">
							<i class="post_format_quote_label"></i>
							<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
								<?php if (!empty($newsider_post_quote_text)) {
									echo esc_attr( $newsider_post_quote_text );
								} else {
									the_title();
								} ?>
							</a></h2>
							<p class="quote_author_name">
								<?php
									if ( !empty( $newsider_post_quote_author ) ) {
										echo esc_attr( $newsider_post_quote_author );
									} else {
										echo get_the_author_meta('display_name');
									}
								?>
							</p>
						</div>
						
					<?php } elseif ($newsider_pf == 'link') { ?>
						
						<div class="post_descr_wrap">
							<i class="post_format_link_label"></i>
							<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
							<a class="pf_link_url" href="<?php
								if ( !empty( $newsider_post_link_url ) ) {
									echo esc_url( $newsider_post_link_url );
								} else {
									echo the_permalink();
								}
								?>" target="_blank"><?php
									if ( !empty( $newsider_post_link_url ) ) {
										echo esc_attr( $newsider_post_link_url );
									} else {
										echo get_the_author_meta('display_name');
									}
							?></a>
						</div>
						
					<?php } else { ?>
						
						<?php if( !empty( $newsider_featured_image_url ) && $newsider_post_featured_img != 'hide' ) { ?>
							<div class="post_format_content mb25">
								<?php echo '<a href="' . get_permalink() . '">';
									echo $newsider_featured_image;
									if ( $newsider_pf == 'video' ){
										echo '<span class="post_format_label video"></span>';
									}
								echo '</a>'; ?>
							</div>
						<?php } ?>
						<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
						<div class="post_descr_wrap">
							<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
							<?php if( !empty( $newsider_post_excerpt ) ){ ?>
								<div class="post-content mb20">
									<?php echo $newsider_post_excerpt; ?>
								</div>
							<?php } ?>
							<div class="post_meta clearfix">
								<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
								<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
							</div>
						</div>
						
					<?php } ?>
				
				<?php } else if( $posts_top_img_style == 'style3' ) { ?>
				
					<!-- if Top Image style 3 -->
					
					<?php if( !empty( $newsider_featured_image_url ) && $newsider_post_featured_img != 'hide' ) { ?>
						<div class="post_format_content">
							<?php echo '<a href="' . get_permalink() . '">';
								echo $newsider_featured_image;
								if( $newsider_pf == 'video' ) {
									echo '<span class="post_format_label video"></span>';
								}
							echo '</a>'; ?>
							<div class="post_share_wrap">
								<i class="post_share_btn"></i>
								<?php get_template_part( 'templates/blog/sharebox' ); ?>
							</div>
						</div>
					<?php } ?>
					<div class="post_descr_wrap">
						<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						<?php if( !empty( $newsider_post_excerpt ) ){ ?>
							<div class="post-content mb20">
								<?php echo $newsider_post_excerpt; ?>
							</div>
						<?php } ?>
						<div class="post_meta clearfix">
							<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
							<span class="post-meta-comments pull-right"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
						</div>
					</div>
				
				<?php } else if( $posts_top_img_style == 'style4' ) { ?>
				
					<!-- if Top Image style 4 -->
					
					<?php if( !empty( $newsider_featured_image_url ) && $newsider_post_featured_img != 'hide' ) { ?>
						<div class="post_format_content">
							<?php echo '<a href="' . get_permalink() . '">' . $newsider_featured_image . '</a>'; ?>
							<div class="post_meta_category_wrap"><span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span></div>
						</div>
					<?php } ?>
					<div class="post_descr_wrap">
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						<?php if( !empty( $newsider_post_excerpt ) ){ ?>
							<div class="post-content mb25">
								<?php echo $newsider_post_excerpt; ?>
							</div>
						<?php } ?>
						<div class="post_meta clearfix">
							<a class="btn_more" href="<?php the_permalink(); ?>"><?php echo esc_html__('Read More','evatheme-pagebuilder'); ?></a>
							<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
							<span class="post_meta_date pull-right"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
						</div>
					</div>
				
				<?php } else { ?>
					
					<!-- if Top Image style 1 -->
					
					<?php if( !empty( $newsider_featured_image_url ) && $newsider_post_featured_img != 'hide' ) { ?>
						<div class="post_format_content mb25">
							<?php echo '<a href="' . get_permalink() . '">';
								echo $newsider_featured_image;
								if ( $newsider_pf == 'video' ){
									echo '<span class="post_format_label video"></span>';
								}
							echo '</a>'; ?>
						</div>
					<?php } ?>
					<div class="post_descr_wrap">
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						<?php if( !empty( $newsider_post_excerpt ) ){ ?>
							<div class="post-content mb20">
								<?php echo $newsider_post_excerpt; ?>
							</div>
						<?php } ?>
						<div class="post_meta clearfix">
							<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
							<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
							<span class="post-meta-comments pull-right"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
						</div>
					</div>
					
				<?php } ?>
				
			</div>
			
		<?php
	}
}


//	Big Background Images
if ( !function_exists('evapb_posts_style_big_bg_images')) {
	function evapb_posts_style_big_bg_images($atts) {
		
		global $i, $newsider_featured_image_url, $newsider_post_excerpt, $evapb_date_format;

		?>
		
			<div class="post_content_wrapper big_first_post text-center clearfix">
				<div class="featured_img_bg" 
					<?php if( ! empty ( $newsider_featured_image_url ) ) { ?>
						style="background-image: url(<?php echo esc_url( $newsider_featured_image_url ); ?>);"
					<?php } ?>
				></div>
				<div class="post_share_wrap">
					<i class="post_share_btn"></i>
					<?php get_template_part( 'templates/blog/sharebox' ); ?>
				</div>
				<div class="post_descr_wrap">
					<div class="post_meta clearfix">
						<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
						<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
					</div>
					<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<?php if ( ($i % 3) == 0 ) { ?>
						<div class="post-content">
							<?php echo $newsider_post_excerpt; ?>
						</div>
					<?php } ?>
				</div>
				<a class="btn_more" href="<?php the_permalink(); ?>"><?php echo esc_html__('Read More','evatheme-pagebuilder'); ?></a>
			</div>
	
		<?php

	}
}


//	Metro Style
if ( !function_exists('evapb_posts_style_metro')) {
	function evapb_posts_style_metro($atts) {
		
		global $post, $newsider_featured_image_url, $evapb_date_format;
		
		$evapb_post_per_line 	= $atts['post_per_line'];
		$newsider_post_metro 	= get_post_meta( $post->ID, 'newsider_post_metro', true );
		
		if( $evapb_post_per_line == '5' ) {
			if( $newsider_post_metro == 'width2' ){
				$newsider_width = 950;
				$newsider_height = 500;
			} else if( $newsider_post_metro == 'height2' ){
				$newsider_width = 500;
				$newsider_height = 950;
			} else if( $newsider_post_metro == 'wh2' ){
				$newsider_width = 950;
				$newsider_height = 950;
			} else {
				$newsider_width = 500;
				$newsider_height = 500;
			}
		} elseif ($evapb_post_per_line == '4') {
			if( $newsider_post_metro == 'width2' ){
				$newsider_width = 950;
				$newsider_height = 500;
			} else if( $newsider_post_metro == 'height2' ){
				$newsider_width = 500;
				$newsider_height = 950;
			} else if( $newsider_post_metro == 'wh2' ){
				$newsider_width = 950;
				$newsider_height = 950;
			} else {
				$newsider_width = 500;
				$newsider_height = 500;
			}
		} elseif ($evapb_post_per_line == '3') {
			if( $newsider_post_metro == 'width2' ){
				$newsider_width = 1200;
				$newsider_height = 650;
			} else if( $newsider_post_metro == 'height2' ){
				$newsider_width = 650;
				$newsider_height = 1200;
			} else if( $newsider_post_metro == 'wh2' ){
				$newsider_width = 1200;
				$newsider_height = 1200;
			} else {
				$newsider_width = 650;
				$newsider_height = 650;
			}
		} elseif ($evapb_post_per_line == '2') {
			if( $newsider_post_metro == 'width2' ){
				$newsider_width = '';
				$newsider_height = 950;
			} else if( $newsider_post_metro == 'height2' ){
				$newsider_width = 950;
				$newsider_height = '';
			} else if( $newsider_post_metro == 'wh2' ){
				$newsider_width = 1700;
				$newsider_height = 1700;
			} else {
				$newsider_width = 950;
				$newsider_height = 950;
			}
		} else {
			$newsider_width = '';
			$newsider_height = '';
		}
		
		$newsider_featured_image_bg = newsider_aq_resize( $newsider_featured_image_url, $newsider_width, $newsider_height, true, true, true );
		?>
		 
			<div class="post_content_wrapper">
				<?php if( !empty( $newsider_featured_image_url ) ) { ?>
					<div class="featured_img_bg" style="background-image:url(<?php echo esc_url( $newsider_featured_image_bg ); ?>);"></div>
				<?php } ?>
				<div class="post_share_wrap">
					<i class="post_share_btn"></i>
					<?php get_template_part( 'templates/blog/sharebox' ); ?>
				</div>
				<div class="post_descr_wrap">
					<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<div class="clearfix">
						<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
						<div class="post_meta">
							<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
						</div>
					</div>
				</div>
			</div>
			
		<?php
	}
}

//	Grid Classic
if ( !function_exists('evapb_posts_style_card_clean')) {
	function evapb_posts_style_card_clean($atts) {
		
		global $post, $newsider_featured_image_url, $newsider_pf, $authordata, $evapb_date_format;
		
		$newsider_featured_image_bg = newsider_aq_resize( $newsider_featured_image_url, 400, 400, true, true, true );
		$newsider_post_quote_text = get_post_meta( $post->ID, 'newsider_post_quote_text', true );
		$newsider_post_quote_author = get_post_meta( $post->ID, 'newsider_post_quote_author', true );
		$newsider_post_link_url = get_post_meta( $post->ID, 'newsider_post_link_url', true );
		$author_roles = $authordata->roles;
		$author_role = array_shift($author_roles);
		
		?>
			
			<div class="post_content_wrapper">
			
				<?php if($newsider_pf == 'quote') { ?>
					
					<i class="pf_quote"></i>
					<div class="post_descr_wrap text-center">
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
							<?php if (!empty($newsider_post_quote_text)) {
								echo esc_attr( $newsider_post_quote_text );
							} else {
								the_title();
							} ?>
						</a></h2>
					</div>
					<p class="pf_quote_author">
						<?php
							if ( !empty( $newsider_post_quote_author ) ) {
								echo esc_attr( $newsider_post_quote_author );
							} else {
								echo get_the_author_meta('display_name');
							}
						?>
					</p>
					
				<?php } elseif ($newsider_pf == 'link') { ?>
					
					<i class="pf_link"></i>
					<div class="post_descr_wrap">
						<span class="post_meta_date"><?php the_date('F j, Y'); ?></span>
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					</div>
					<a class="pf_link_url" href="<?php
						if ( !empty( $newsider_post_link_url ) ) {
							echo esc_url( $newsider_post_link_url );
						} else {
							echo the_permalink();
						}
						?>" target="_blank"><?php
							if ( !empty( $newsider_post_link_url ) ) {
								echo esc_attr( $newsider_post_link_url );
							} else {
								echo get_the_author_meta('display_name');
							}
					?></a>
					
				<?php } elseif ($newsider_pf == 'aside') { ?>
					
					<?php include_once( CSTHEME_PLUGIN_PATH . 'inc/templates/blog/post-format/post-twitter.php' ); ?>
					
				<?php } elseif ($newsider_pf == 'status') { ?>
					
					<?php
						
						$newsider_post_instagram_username = strtolower( get_post_meta($post->ID, 'newsider_post_instagram_username', true) );
						$newsider_post_instagram_url = 'http://instagram.com/' . $newsider_post_instagram_username;
					?>
					
					<div class="post_descr_wrap">
						<a class="pf__instagram_label" href="<?php echo $newsider_post_instagram_url; ?>" target="_blank"><i class="fa fa-instagram"></i></a>
						<h2 class="post_title">
							<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
								<?php the_title(); ?>
							</a>
						</h2>
					</div>
					
					<?php include_once( CSTHEME_PLUGIN_PATH . 'inc/templates/blog/post-format/post-instagram.php' ); ?>
					
				<?php } elseif ($newsider_pf == 'gallery') { ?>
					
					<div class="pf_slider_wrap">
						<?php include_once( CSTHEME_PLUGIN_PATH . 'inc/templates/blog/post-format/post-gallery.php' ); ?>
						<div class="post_share_wrap">
							<i class="post_share_btn"></i>
							<?php get_template_part( 'templates/blog/sharebox' ); ?>
						</div>
					</div>
					
					<div class="post_descr_wrap">
						<a class="post_meta_author" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>">
							<?php echo get_avatar( get_the_author_meta('user_email'), '40', '' ) ?>
							<b><?php echo get_the_author_meta('display_name'); ?></b>
							<span><?php echo $author_role; ?></span>
						</a>
						<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
						<h2 class="post_title">
							<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
								<?php the_title(); ?>
							</a>
						</h2>
						<div class="post_meta">
							<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
							<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
						</div>
					</div>
					
				<?php } elseif ($newsider_pf == 'video') { ?>
					
					<?php get_template_part( 'templates/blog/post-format/post', $newsider_pf ); ?>
					
					<div class="post_descr_wrap">
						<a class="post_meta_author" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>">
							<?php echo get_avatar( get_the_author_meta('user_email'), '40', '' ) ?>
							<b><?php echo get_the_author_meta('display_name'); ?></b>
							<span><?php echo $author_role; ?></span>
						</a>
						<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
						<h2 class="post_title">
							<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
								<?php the_title(); ?>
							</a>
						</h2>
						<div class="post_meta">
							<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
							<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
						</div>
					</div>
					
				<?php } else { ?>
					
					<?php if( ! empty( $newsider_featured_image_url ) ) { ?>
						<div class="featured_img_bg" style="background-image:url(<?php echo $newsider_featured_image_bg ?>);">
							<a class="post_permalink" href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>"></a>
							<div class="post_share_wrap">
								<i class="post_share_btn"></i>
								<?php get_template_part( 'templates/blog/sharebox' ); ?>
							</div>
						</div>
					<?php } ?>
					
					<div class="post_descr_wrap">
						<a class="post_meta_author" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>">
							<?php echo get_avatar( get_the_author_meta('user_email'), '40', '' ) ?>
							<b><?php echo get_the_author_meta('display_name'); ?></b>
							<span><?php echo $author_role; ?></span>
						</a>
						<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
						<h2 class="post_title">
							<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
								<?php the_title(); ?>
							</a>
						</h2>
						<div class="post_meta">
							<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
							<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
						</div>
					</div>
					
				<?php } ?>
			
			</div>
			
		<?php
	}
}


//	Grid Classic
if ( !function_exists('evapb_posts_style_big_top_img')) {
	function evapb_posts_style_big_top_img($atts) {
		
		global $post, $newsider_featured_image_url, $newsider_pf, $newsider_post_excerpt, $authordata, $evapb_date_format;
		
		$newsider_post_quote_text = get_post_meta( $post->ID, 'newsider_post_quote_text', true );
		$newsider_post_quote_author = get_post_meta( $post->ID, 'newsider_post_quote_author', true );
		$newsider_post_link_url = get_post_meta( $post->ID, 'newsider_post_link_url', true );
		$author_roles = $authordata->roles;
		$author_role = array_shift($author_roles);
		
		?>
			
			<div class="post_content_wrapper">
			
				<?php if($newsider_pf == 'quote') { ?>
					
					<i class="pf_quote"></i>
					<div class="post_descr_wrap text-center">
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
							<?php if (!empty($newsider_post_quote_text)) {
								echo esc_attr( $newsider_post_quote_text );
							} else {
								the_title();
							} ?>
						</a></h2>
					</div>
					<p class="pf_quote_author">
						<?php
							if ( !empty( $newsider_post_quote_author ) ) {
								echo esc_attr( $newsider_post_quote_author );
							} else {
								echo get_the_author_meta('display_name');
							}
						?>
					</p>
					
				<?php } elseif ($newsider_pf == 'link') { ?>
					
					<i class="pf_link"></i>
					<div class="post_descr_wrap">
						<span class="post_meta_date"><?php the_date('F j, Y'); ?></span>
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'newsider'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					</div>
					<a class="pf_link_url" href="<?php
						if ( !empty( $newsider_post_link_url ) ) {
							echo esc_url( $newsider_post_link_url );
						} else {
							echo the_permalink();
						}
						?>" target="_blank"><?php
							if ( !empty( $newsider_post_link_url ) ) {
								echo esc_attr( $newsider_post_link_url );
							} else {
								echo get_the_author_meta('display_name');
							}
					?></a>
					
				<?php } elseif ( $newsider_pf == 'aside' || $newsider_pf == 'status') { ?>
					
					<?php get_template_part( 'templates/blog/post-format/post', $newsider_pf ); ?>
					
				<?php } elseif ($newsider_pf == 'gallery') { ?>
					
					<div class="pf_slider_wrap mb20">
						<?php get_template_part ( 'templates/blog/post-format/post-gallery' ); ?>
						<div class="post_share_wrap">
							<i class="post_share_btn"></i>
							<?php get_template_part( 'templates/blog/sharebox' ); ?>
						</div>
					</div>
					
					<div class="post_descr_wrap clearfix">
						<div class="post_meta clearfix">
							<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
							<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
							<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
						</div>
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						<?php if( !empty( $newsider_post_excerpt ) ){ ?>
							<div class="post-content">
								<?php echo $newsider_post_excerpt; ?>
							</div>
						<?php } ?>
					</div>
					
				<?php } elseif ($newsider_pf == 'video') { ?>
					
					<?php get_template_part( 'templates/blog/post-format/post', $newsider_pf ); ?>
					
					<div class="post_descr_wrap clearfix">
						<div class="post_meta clearfix">
							<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
							<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
							<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
						</div>
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						<?php if( !empty( $newsider_post_excerpt ) ){ ?>
							<div class="post-content">
								<?php echo $newsider_post_excerpt; ?>
							</div>
						<?php } ?>
					</div>
					
				<?php } else { ?>
					
					<?php if( ! empty( $newsider_featured_image_url ) ) { ?>
						<div class="post_format_content">
							<?php echo '<a href="' . get_permalink() . '"><img src="' . newsider_aq_resize( $newsider_featured_image_url, 770, 410, true, true, true) . '" alt="' . get_the_title() . '" width="1170" height="620" /></a>'; ?>
							<div class="post_share_wrap">
								<i class="post_share_btn"></i>
								<?php get_template_part( 'templates/blog/sharebox' ); ?>
							</div>
						</div>
					<?php } ?>
					
					<div class="post_descr_wrap clearfix">
						<div class="post_meta clearfix">
							<span class="post_meta_category post_category_label"><span></span><?php the_category(', '); ?></span>
							<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
							<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
						</div>
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						<?php if( !empty( $newsider_post_excerpt ) ){ ?>
							<div class="post-content">
								<?php echo $newsider_post_excerpt; ?>
							</div>
						<?php } ?>
					</div>
					
				<?php } ?>
			
			</div>
			
		<?php
	}
}