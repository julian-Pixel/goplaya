<?php
/**
 *	Before After Shortcode
 */

function cstheme_before_after($atts, $content) {
    wp_enqueue_script('cstheme-event-move',  CSTHEME_PLUGIN_URL .'assets/js/jquery.event.move.js', false, false, true);
    wp_enqueue_script('cstheme-twentytwenty',  CSTHEME_PLUGIN_URL . 'assets/js/jquery.twentytwenty.js', false, false, true);
    wp_enqueue_style('cstheme-twentytwenty', CSTHEME_PLUGIN_URL . 'assets/css/twentytwenty.css');

    if(empty($atts['before'])){$atts['before']=CSTHEME_PLUGIN_URL .'assets/images/no-image.png';}
    if(empty($atts['after'] )){$atts['after'] =CSTHEME_PLUGIN_URL .'assets/images/no-image.png';}
    $output = cstheme_item($atts,'cstheme-before-after-container');
        $output .= '<div class="cstheme-before-after">';
            $output .= '<img src="'.$atts['before'].'" title="'.__('Before','cstheme').'" />';
            $output .= '<img src="'.$atts['after'] .'" title="'.__('After','cstheme') .'" />';
        $output .= '</div>';
    $output .= '</div>';

    return $output;
}
add_shortcode('cs_before_after', 'cstheme_before_after');