<?php
/**
 *	Accordion Shortcode
 */

function cstheme_accordion($atts, $content) {
    $output = cstheme_item($atts,'cstheme-accordion');
        $output .= do_shortcode($content);
    $output .= '</div>';
    return $output;
}
add_shortcode('cs_accordion', 'cstheme_accordion');


function cstheme_accordion_item($atts, $content) {
    $atts = shortcode_atts( array(
		'title' 	=> esc_html__('Accordion', 'evatheme-pagebuilder'),
		'expand' 	=> 'false',
    ), $atts );
	
	
	wp_enqueue_script('evapagebuilder_accordion', CSTHEME_PLUGIN_URL . 'assets/js/jquery.accordionpro.js', array('jquery'), false, true);
	
    $class = $active = '';
	
    if($atts['expand'] == 'true'){
        $active = ' active';
    }
	
    $output = '<div class="accordion-group' . $class . ' ' . $active . '">';
        $output .= '<div class="accordion-heading">';
            $output .= '<a class="accordion-toggle" data-toggle="collapse" data-parent="" href="javascript:void(0);">';
                $output .= $atts['title'];
                $output .= '<i class="fa fa-caret-right"></i>';
            $output .= '</a>';
        $output .= '</div>';
        $output .= '<div class="accordion-body collapse">';
            $output .= '<div class="accordion-inner">';
                $output .= apply_filters('the_excerpt', $content);
            $output .= '</div>';
        $output .= '</div>';
    $output .= '</div>';

    return $output;
}
add_shortcode('cs_accordion_item', 'cstheme_accordion_item');