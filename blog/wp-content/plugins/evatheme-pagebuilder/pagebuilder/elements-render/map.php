<?php
/**
 *	Map Shortcode
 */

function cstheme_map($atts, $content) {
    $atts = shortcode_atts(array(
        'size'  			=> 'col-md-12',
        'class' 			=> '',
        'title' 			=> '',
		'title_size' 		=> '',
		'title_aligment' 	=> '',
		'margin_bottom'		=> '',
        'type'  			=> 'boxed'
    ), $atts);
	
	
    $output  = cstheme_item($atts);
        $output .= '<div class="cstheme-map'.($atts['type']==='full'?' cstheme-full-element':'').'">';
            $output .= $content;
        $output .= '</div>';
    $output .= '</div>';
    return $output;
}
add_shortcode('cs_map', 'cstheme_map');