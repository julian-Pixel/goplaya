<?php
/**
 *	Team container
 */

function cstheme_team($atts, $content) {
    $atts = shortcode_atts( array(
        'size'  			=> 'col-md-3',
        'class' 			=> '',
        'title' 			=> '',
		'title_size' 		=> '',
		'title_aligment' 	=> '',
		'margin_bottom'		=> '',
        "team_title"    	=> "Team Title",
        "position" 			=> '',
        "image"    			=> '',
        "facebook" 			=> '#',
        "google"   			=> '#',
        "twitter" 			=> '#',
        "pinterest"			=> '#',
        "youtube"			=> '#',
        "instagram"			=> '#',
        "flickr"			=> '#',
        "dribbble"			=> '#',
        "skype"				=> '#',
    ), $atts );
	
	
    $output  = cstheme_item($atts,'cstheme-team');
		$output .='<div class="cstheme_member_wrapper">';
			if( !empty( $atts['image'] ) ) {
				$output .= '<div class="member-image">';
					$output .= '<img src="' . $atts['image'] . '" alt="' . $atts['team_title'] . '" />';
				$output .='</div>';
			}
			$output .='<div class="cstheme_member_descr">';
				$output .= '<div class="member-title">';
					if (!empty($atts['position'])){
						$output .= '<div class="member-pos theme_color">' . $atts['position'] . '</div>';
					}
					if (!empty($atts['team_title'])){
						$output .= '<h2>' . $atts['team_title'] . '</h2>';
					}
				$output .= '</div>';
				if ( !empty($atts['facebook']) || !empty($atts['google']) || !empty($atts['twitter']) || !empty($atts['pinterest']) || !empty($atts['youtube']) || !empty($atts['instagram']) || !empty($atts['flickr']) || !empty($atts['dribbble']) ) {
					$output .='<div class="social_links">';
						if (!empty($atts['facebook'])){
							$output .= '<a href="' . esc_url($atts['facebook']) . '" target="_blank" class="facebook"><i class="fa fa-facebook"></i><i class="fa fa-facebook"></i></a>';
						}
						if (!empty($atts['google'])){
							$output .= '<a href="' . esc_url($atts['google']) . '" target="_blank" class="gplus"><i class="fa fa-google-plus"></i><i class="fa fa-google-plus"></i></a>';
						}
						if (!empty($atts['twitter'])){
							$output .= '<a href="' . esc_url($atts['twitter']) . '" target="_blank" class="twitter"><i class="fa fa-twitter"></i><i class="fa fa-twitter"></i></a>';
						}
						if (!empty($atts['pinterest'])){
							$output .= '<a href="' . esc_url($atts['pinterest']) . '" target="_blank" class="pinterest"><i class="fa fa-pinterest-p"></i><i class="fa fa-pinterest-p"></i></a>';
						}
						if (!empty($atts['youtube'])){
							$output .= '<a href="' . esc_url($atts['youtube']) . '" target="_blank" class="youtube"><i class="fa fa-youtube"></i><i class="fa fa-youtube"></i></a>';
						}
						if (!empty($atts['instagram'])){
							$output .= '<a href="' . esc_url($atts['instagram']) . '" target="_blank" class="instagram"><i class="fa fa-instagram"></i><i class="fa fa-instagram"></i></a>';
						}
						if (!empty($atts['flickr'])){
							$output .= '<a href="' . esc_url($atts['flickr']) . '" target="_blank" class="flickr"><i class="fa fa-flickr"></i><i class="fa fa-flickr"></i></a>';
						}
						if (!empty($atts['dribbble'])){
							$output .= '<a href="' . esc_url($atts['dribbble']) . '" target="_blank" class="dribbble"><i class="fa fa-dribbble"></i><i class="fa fa-dribbble"></i></a>';
						}
					$output .= '</div>';
				}
			$output .= '</div>';
		$output .= '</div>';
    $output .= '</div>';
	
    return $output;
}
add_shortcode('cs_team', 'cstheme_team');