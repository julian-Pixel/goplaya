<?php
/**
 *	Categories Shortcode
 */

function cstheme_categories($atts, $content) {
	
	global $cs_isshortcode;
    $cs_isshortcode='true';
    
	$atts = shortcode_atts(array(
        'size'  			=> 'col-md-3',
        'class' 			=> '',
        'title' 			=> '',
		'title_size' 		=> '',
		'title_aligment' 	=> '',
		'margin_bottom'		=> '',
		'style'				=> 'grid',
        'count' 			=> '3',
    ), $atts);
    
    
	$categorieslist_unique_id = uniqid('categorieslist_id');
	
	$style 	= $atts['style'];
	$count 	= $atts['count'];
	
	$output = cstheme_item( $atts, 'evapb-categories ' . $categorieslist_unique_id . ' ' . $style );
    
		$cats = get_categories( array(
			'orderby' 		=> 'name',
			'order' 		=> 'ASC',
			'hide_empty' 	=> true
		) );
		
		if( $style != 'carousel' ){
			$output .= '<div class="categories_list"><div class="row">';
		} else {
			$output .= '<div class="categories_list owl-carousel">';
		}
		
			foreach( $cats as $cat ) {
				
				if( $cat->slug == 'uncategorized' ) {
					continue;
				}
				
				if( $style != 'carousel' ){
					$output .= '<div class="col-md-4 col-sm-6">';
				} else {
					$output .= '<div class="item">';
				}
			
					$query = new WP_Query( array(
						'posts_per_page' => $count,
						'cat' => $cat->term_id,
						'meta_query' => array(
							array(
								'key' => '_thumbnail_id',
							),
						),
					) );
					
					if( $query->have_posts() ) {
						$post_id = $query->posts[0]->ID;
						$img_url = wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
						$img = '<img src="' . newsider_aq_resize( $img_url, 170, 110, true, true, true) . '" alt="' . get_the_title() . '" width="170" height="110" />';
					} else {
						$img = '';
					}
					
					$link = get_category_link( $cat->term_id );
					$count = $cat->category_count;
					
					$output .= '
						<a href="' . esc_url( $link ) . '" class="cat_block ' . esc_html( $cat->slug ) . '">
							' . $img . '
							<h6>' . esc_attr( $cat->slug ) . '</h6>
							<span>' . esc_html( $count ) . ' ' . esc_html__('Posts','evatheme-pagebuilder') . '</span>
						</a>
					';
			
				$output .= '</div>';
	
			}
		
		if( $style != 'carousel' ){
			$output .= '</div></div>';
		} else {
			$output .= '</div>';
		}
		
    $output .= '</div>';
    
	$cs_isshortcode = 'false';
	
	return $output;
	
}
add_shortcode('cs_categories', 'cstheme_categories');