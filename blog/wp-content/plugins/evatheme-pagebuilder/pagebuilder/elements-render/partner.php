<?php
/**
 *	Partner Shortcode
 */

function cstheme_carousel_partner($atts, $content) {
    global $parentAtts;
	
	$atts = shortcode_atts( array(
        'size'  			=> 'col-md-3',
        'class' 			=> '',
        'title' 			=> '',
		'title_size' 		=> '',
		'title_aligment' 	=> '',
		'partners_per_line' => '6',
		'margin_bottom'		=> ''
    ), $atts );

	
	$parentAtts = $atts;
	
    $output  = cstheme_item($atts,"carousel-container");
        $output .= '<div class="cstheme-partners-list">';
			$output .= do_shortcode($content);
        $output .= '</div>';
    $output .= '</div>';
    return $output;
}
add_shortcode('cs_partner', 'cstheme_carousel_partner');


function cstheme_partner_item($atts, $content) {
    global $parentAtts;
	
	$atts = shortcode_atts( array(
        'title'		=> '',
        'link' 		=> '',
        'thumb' 	=> '',
    ), $atts );

	
	$partner_items_count = $parentAtts['partners_per_line'];
	$partner_item_width = 100/$partner_items_count;
	
    $output = '<div class="partner-item" style="width:'. $partner_item_width. '%">';
        if ($atts['link'] != '') { $output .= '<a href="' . $atts['link'] . '" title="'.$atts['title'].'">'; }
            $output .= '<img alt="'. $atts['title'] .'" src="'. $atts['thumb'] .'" data-toggle="tooltip" data-original-title="'. $atts['title'] .'" />';
        if ($atts['link'] != '') {$output .= '</a>';}
    $output .= '</div>';
    return $output;
}
add_shortcode('cs_partner_item', 'cstheme_partner_item');