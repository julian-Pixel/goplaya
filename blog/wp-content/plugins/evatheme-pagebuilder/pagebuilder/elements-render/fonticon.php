<?php
/**
 *	Font Icon Shortcode
 */

function shortcode_cs_fonticon($atts, $content) {
    $atts['fi_size'] = str_replace('px', '', $atts['fi_size']);
    $atts['fi_padding'] = str_replace('px', '', $atts['fi_padding']);
    $atts['fi_rounded'] = str_replace('px', '', $atts['fi_rounded']);
    $atts['fi_rounded_size'] = str_replace('px', '', $atts['fi_rounded_size']);
    $style = 'text-align:center;';
    $style .='font-size:' . $atts['fi_size'] . 'px;';
    $style .='width:' . ($atts['fi_size']+2) . 'px;';
    $style .='line-height:' . ($atts['fi_size']+2) . 'px;';
    $style .='padding:' . $atts['fi_padding'] . 'px;';
    $style .='color:' . $atts['fi_color'] . ';';
    $style .='background-color:' . $atts['fi_bg_color'] . ';';
    $style .='border-color:' . $atts['fi_border_color'] . ';';
    $style .='border-width:' . $atts['fi_rounded'] . 'px;';
    $style .='border-radius:' . $atts['fi_rounded_size'] . 'px;';
    $style .='-webkit-border-radius:' . $atts['fi_rounded_size'] . 'px;';
    $style .='-moz-border-radius:' . $atts['fi_rounded_size'] . 'px;';
    $output = '<i class="cs-font-icon ' . $atts['fi_icon'] . '" style="border-style: solid;font-style:normal;' . $style . '"></i>';
    return $output;
}
add_shortcode('cs_fonticon', 'shortcode_cs_fonticon');