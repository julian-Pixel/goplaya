<?php
/**
 *	Layout Shortcode
 */

function cstheme_layout($atts, $content) {
    if(!empty($atts['bg_type'])) {
        if($atts['bg_type']=='video') {
            add_action('wp_footer', 'cstheme_jplayer_script');
        }
    }
    $output = '<div class="' . $atts['size'] . ' ' . $atts['layout_custom_class'] . '">' . do_shortcode($content) . '</div>';
    return $output;
}
add_shortcode('cs_layout', 'cstheme_layout');