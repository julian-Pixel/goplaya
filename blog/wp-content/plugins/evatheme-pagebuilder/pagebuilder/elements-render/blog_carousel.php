<?php
/**
 *	Blog Carousel Shortcode
 */

function cstheme_blog_carousel($atts, $content) {
	
	global $post, $blog_carousel_i, $cs_isshortcode, $evapb_date_format;
    
	$cs_isshortcode='true';
    
	$atts = shortcode_atts(array(
        'size'  				=> 'col-md-3',
        'class' 				=> '',
		'title' 				=> '',
		'title_size' 			=> '',
		'title_aligment' 		=> '',
		'margin_bottom'			=> '',
		'posts_style'			=> '',
        'post_count'    		=> '3',
		'orderby' 				=> '',
        'category_ids'  		=> '',
    ), $atts);

	$evapb_date_format = get_option( 'date_format' );
	$evapb_posts_style = $atts['posts_style'];

	$blog_carousel_class = $blog_carousel_list_class = $evapb_post_class = '';
	$blog_carousel_class .= $evapb_posts_style;
	
	$output = cstheme_item( $atts, 'evapb_blog_carousel ' . $blog_carousel_class );
    
		if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
		
		$query = Array(
			'post_type' 		=> 'post',
			'posts_per_page' 	=> $atts['post_count'],
			'post_status' 		=> 'publish',
			'ignore_sticky_posts' => 1
		);
		$cats = empty($atts['category_ids']) ? false : explode(",", $atts['category_ids']);
		if ($cats) {
			$query['tax_query'] = Array(Array(
					'taxonomy' 	=> 'category',
					'terms' 	=> $cats,
					'field' 	=> 'slug'
				)
			);
		}
		if (!empty($atts['orderby'])) {
            switch ($atts['orderby']) {
                case "date_asc":
                    $query['orderby'] = 'date';
                    $query['order'] = 'ASC';
                    break;
                case "title_asc":
                    $query['orderby'] = 'title';
                    $query['order'] = 'ASC';
                    break;
                case "title_desc":
                    $query['orderby'] = 'title';
                    $query['order'] = 'DESC';
                    break;
                case "random":
                    $query['orderby'] = 'rand';
                    break;
				case "comment_count":
                    $query['orderby'] = 'comment_count';
                    break;
            }
        }
		
		if ( $evapb_posts_style != 'listing_hover' ) {
			$blog_carousel_list_class .= 'owl-carousel';
		}
		
		$wp_query2 = new WP_Query();
		$wp_query2->query($query);
		$blog_carousel_i = 1;
		$blog_carousel_all = 1;
		
		ob_start();
		query_posts($query);
		if (have_posts()) {
			
			echo '<div class="posts_carousel_list clearfix ' . $blog_carousel_list_class . '">';
			
				while (have_posts()) { the_post();
					
					global $evapb_featured_image_url;
					
					$evapb_big_post = '';
					if( $evapb_posts_style == 'fisrt_big_img' ) {
						if( ( $blog_carousel_i == 1 ) || ( ($blog_carousel_i % 5) == 1 ) ) {
							$evapb_big_post .= ' big_post';
						} else {
							$evapb_big_post .= ' small_post';
						}
					}
					
					if( $evapb_posts_style == 'fisrt_big_img' && ( $blog_carousel_i == 1 ) ) {
						echo '<div class="item">';
					}
					
					$evapb_featured_image_url 	= wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
					?>
					
						<article <?php post_class( esc_html( $evapb_post_class . ' ' . $evapb_big_post ) ); ?>>
							
							<?php call_user_func('evapb_posts_style_' . $evapb_posts_style, $atts); ?>
							
						</article>
		
					<?php
					
					if ($blog_carousel_i >= 5 && $evapb_posts_style == 'fisrt_big_img' ) {
                        echo '</div>';
                        $blog_carousel_i = 0;
                    }
                    if ($blog_carousel_i !== 0 && $blog_carousel_all == $wp_query2->post_count && $evapb_posts_style == 'fisrt_big_img' ) {
                        echo '</div>';
                    }
                    $blog_carousel_i++;
                    $blog_carousel_all++;
				}
				
			echo '</div>';

		}
		
		wp_reset_query();
		
		$output .= ob_get_clean();
		
    $output .= '</div>';
    
	$cs_isshortcode = 'false';
	
	return $output;
	
}
add_shortcode('cs_blog_carousel', 'cstheme_blog_carousel');


//	Listing on hover
if ( !function_exists('evapb_posts_style_listing_hover')) {
	function evapb_posts_style_listing_hover($atts) {
		
		global $post, $evapb_featured_image_url, $newsider_post_excerpt, $evapb_date_format;
		
		$evapb_featured_image = '<img src="' . newsider_aq_resize( $evapb_featured_image_url, 340, 470, true, true, true) . '" alt="' . get_the_title() . '" width="340" height="470" />';
		
		?>

			<div class="post_content_wrapper clearfix">
				<div class="post_format_content">
					<?php echo '<a href="' . get_permalink() . '">' . $evapb_featured_image . '</a>'; ?>
				</div>
				<div class="post_descr_wrap clearfix">
					<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<div class="post_meta clearfix">
						<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
						<span class="post-meta-comments"><i></i><?php echo get_comments_number(get_the_ID()); ?></span>
					</div>
				</div>
			</div>
			
		<?php
	}
}


//	First Big Images
if ( !function_exists('evapb_posts_style_fisrt_big_img')) {
	function evapb_posts_style_fisrt_big_img($atts) {
		
		global $post, $blog_carousel_i, $evapb_featured_image_url, $evapb_date_format;
		
		$evapb_featured_image = '<img src="' . newsider_aq_resize( $evapb_featured_image_url, 120, 90, true, true, true) . '" alt="' . get_the_title() . '" width="120" height="90" />';
		$evapb_featured_image_bg = newsider_aq_resize( $evapb_featured_image_url, 450, 550, true, true, true);
		
		?>

			<?php if( ( $blog_carousel_i == 1 ) || ( ($blog_carousel_i % 5) == 1 ) ) { ?>

				<div class="post_content_wrapper">
					<div class="featured_img_bg" 
						<?php if( ! empty( $evapb_featured_image_url ) ) { ?>
							style="background-image:url(<?php echo $evapb_featured_image_bg ?>);"
						<?php } ?>
					></div>
					<div class="post_share_wrap">
						<i class="post_share_btn"></i>
						<?php get_template_part( 'templates/blog/sharebox' ); ?>
					</div>
					<div class="post_descr_wrap clearfix">
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						<div class="post_meta clearfix">
							<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
							<span class="post_meta_author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></span>
						</div>
					</div>
				</div>
				
			<?php } else { ?>
			
				<div class="post_content_wrapper clearfix <?php if( empty( $evapb_featured_image_url ) ){ echo 'no_featured_img'; } ?>">
					<?php if( !empty( $evapb_featured_image_url ) ){ ?>
						<div class="post_format_content">
							<?php echo '<a href="' . get_permalink() . '">' . $evapb_featured_image . '</a>'; ?>
						</div>
					<?php } ?>
					<div class="post_descr_wrap clearfix">
						<span class="post_meta_date"><?php the_time( esc_html( $evapb_date_format ) ); ?></span>
						<h2 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					</div>
				</div>
			
			<?php } ?>
			
		<?php
	}
}


//	Small carousel
if ( !function_exists('evapb_posts_style_small_carousel')) {
	function evapb_posts_style_small_carousel($atts) {
		
		global $post, $evapb_featured_image_url, $newsider_post_excerpt;
		
		$evapb_featured_image = '<img src="' . newsider_aq_resize( $evapb_featured_image_url, 240, 125, true, true, true) . '" alt="' . get_the_title() . '" width="240" height="125" />';
		
		?>

			<div class="post_content_wrapper">
				<div class="post_format_content mb25">
					<?php echo '<a href="' . get_permalink() . '">' . $evapb_featured_image . '</a>'; ?>
				</div>
				<div class="post_descr_wrap">
					<h5 class="post_title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'evatheme-pagebuilder'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h5>
				</div>
			</div>
			
		<?php
	}
}