<?php
/**
 *	Image Slider Shortcode
 */

if (!function_exists('shortcode_cs_image_slider')) {
    function shortcode_cs_image_slider($atts, $content) {
        $atts = shortcode_atts( array(
            'size'  			=> 'col-md-3',
            'class' 			=> '',
			'title' 			=> '',
			'title_size' 		=> '',
			'title_aligment' 	=> '',
			'margin_bottom'		=> '',
            'gallery_list' 		=> '',
            'type' 				=> 'col3',
            'height' 			=> '',
            'padding' 			=> '0px'
        ), $atts );
		
		wp_enqueue_script('evapb-isotope', CSTHEME_PLUGIN_URL . 'assets/js/cstheme-isotope.js', array(), false, true);
		wp_enqueue_script('swipebox', CSTHEME_PLUGIN_URL . 'assets/js/jquery.swipebox.min.js', array(), '1.4.4 ', true);
		wp_enqueue_style('evapb-swipebox', CSTHEME_PLUGIN_URL . 'assets/css/cstheme-swipebox.css' );
		
		$evapb_gallery_type = $atts['type'];
			
            $output  = cstheme_item($atts,'cstheme-gallery-container clearfix');
			
            $margin = $padding = $evapb_gallery_class = '';
			
			if( $evapb_gallery_type == 'col2' ) {
				$evapb_gallery_class = 'col-sm-6';
			} elseif( $evapb_gallery_type == 'col3' ) {
				$evapb_gallery_class = 'col-sm-4';
			} elseif( $evapb_gallery_type == 'col4' ) {
				$evapb_gallery_class = 'col-sm-3';
			} elseif( $evapb_gallery_type == 'col5' ) {
				$evapb_gallery_class = 'col-sm-3 col-sm-2_5';
			} else {
				$evapb_gallery_class = 'col-sm-2';
			}
			
            if( !empty( $atts['padding'] ) ) {
                
				$margin = ' style="margin: -' . esc_attr( $atts['padding'] ) . ';"';
                $padding = ' style="padding:' . esc_attr( $atts['padding'] ) . '"';
            }
                $output .= '<div class="cstheme-gallery clearfix ' . $evapb_gallery_type . '"' . $margin . '>';
                    
					$images = empty($atts['gallery_list']) ? false : explode(",", $atts['gallery_list']);
					
                    if( $images ){
                        foreach ( $images as $id ) {
                            if( !empty( $id ) ){
									
									
								$output .= '<div class="gallery_item ' . $evapb_gallery_class . '" ' . $padding . '>';
									$output .= '<a class="swipebox" href="' . wp_get_attachment_url($id) . '" title="' . get_the_title($id) . '">';
										$output .= '<img src="' . wp_get_attachment_url($id) . '" alt="' . get_the_title() . '" />';
										$output .= '<span class="gallery_zoom"></span>';
									$output .= '</a>';
								$output .= '</div>';
									
                            }
                        }    
                    }
                $output .= '</div>';
            $output .= '</div>';
		
		return $output;
		
    }
}
add_shortcode('cs_image_slider', 'shortcode_cs_image_slider');