<?php
/**
 *	Core Content Shortcode
 */

function cstheme_content($atts, $content) {
    global $cs_isshortcode;
    $cs_isshortcode='true';
    
	$atts = shortcode_atts(array(
        'size'  			=> 'col-md-12',
        'class' 			=> '',
        'title' 			=> '',
		'title_size' 		=> '',
		'title_aligment' 	=> '',
		'margin_bottom'		=> ''
    ), $atts);
	
	
    $output  = cstheme_item($atts);
        $output .= '<div class="entry-content">';
            $output .= apply_filters("the_content", get_the_content());
        $output .= '</div>';
    $output .= '</div>';
    $cs_isshortcode='false';
    return $output;
}
add_shortcode('cs_content', 'cstheme_content');