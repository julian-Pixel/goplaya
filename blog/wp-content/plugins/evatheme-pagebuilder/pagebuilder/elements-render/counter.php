<?php
/**
 *	Counter Shortcode
 */

function cstheme_counter($atts, $content) {
	$atts = shortcode_atts( array(
        'size'  			=> 'col-md-3',
        'title' 			=> '',
		'title_size' 		=> '',
		'title_aligment' 	=> '',
		'margin_bottom'		=> '',
        'counter_title' 	=> 'Our Customers',
        'thumb_type' 		=> 'fi',
        // Font Icon
        "fi" 				=> "",
        "fi_size" 			=> "30",
        "fi_padding" 		=> "20",
        "fi_color" 			=> "#aaaaaa",
        "fi_bg_color" 		=> "",
        "fi_border_color" 	=> "#aaaaaa",
        "fi_rounded" 		=> "6",
        "fi_rounded_size" 	=> "3",
        "fi_icon" 			=> "fa-glass",
        // -----------------------
        'image' 			=> '',
        'counter_type' 		=> '',
        'count' 			=> '596',
    ), $atts);
	
	
    $atts2 = $atts;

    $output = cstheme_item($atts2, "cstheme-counter");
        
		$class = $thumb = '';
		
		if ($atts['thumb_type'] === 'fi') {
            $thumb = do_shortcode('[cs_fonticon size="cstheme-shortcode" fi_size="' . $atts['fi_size'] . '" fi_padding="' . $atts['fi_padding'] . '" fi_color="' . $atts['fi_color'] . '" fi_bg_color="' . $atts['fi_bg_color'] . '" fi_border_color="' . $atts['fi_border_color'] . '" fi_rounded="' . $atts['fi_rounded'] . '" fi_rounded_size="' . $atts['fi_rounded_size'] . '" fi_icon="' . $atts['fi_icon'] . '"]');
        } elseif ($atts['thumb_type'] === 'image') {
            $thumb = '<img src="' . $atts['image'] . '" />';
        }

		$count = $atts['count'];
		$class .= $atts['counter_type'];

        if ( $atts['counter_type'] == 'style2' ) {
		
			$output .= '<div class="counters-box ' . $class . '">';
				$output .= '<div class="counter_heading">';
					if( !empty( $thumb ) ) {
						$output .= $thumb;
					}
					$output .= '<span class="stat_count" data-count="' . $count . '">0</span>';
				$output .= '</div>';
				if ($atts['thumb_type'] == 'empty') {
					$output .= '<span class="sep bg_primary"></span>';
				}
				$output .= '<p class="counter_title">' . $atts['counter_title'] . '</p>';
			$output .= '</div>';
		
		} else {
			
			$output .= '<div class="counters-box ' . $class . '">';
				if( !empty( $thumb ) ) {
					$output .= $thumb;
				}
				$output .= '<p class="counter_title">' . $atts['counter_title'] . '</p>';
				if ($atts['thumb_type'] == 'empty') {
					$output .= '<span class="sep bg_primary"></span>';
				}
				$output .= '<span class="stat_count" data-count="' . $count . '">0</span>';
			$output .= '</div>';
			
		}

    $output .= '</div>';
    return $output;
}
add_shortcode('cs_counters', 'cstheme_counter');