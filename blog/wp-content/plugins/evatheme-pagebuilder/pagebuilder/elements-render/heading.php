<?php
/**
 *	Item Title Shortcode
 */

function cstheme_heading($atts, $content) {
    $atts = shortcode_atts(array(
        'size' 				=> 'col-md-3',
        'class' 			=> '',
		'margin_bottom'		=> '',
        "text" 				=> '',
        "description" 		=> '',
        "weight" 			=> '400',
        "style" 			=> 'normal',
        "position" 			=> 'center'
    ), $atts);
	
	
    $output = cstheme_item($atts, 'cstheme-heading text-' . $atts['position']);
		$output .= '<h5 class="heading-title" style="font-weight:' . $atts['weight'] . '; font-style:' . $atts['style'] . '">' . $atts['text'] . '<span class="title-seperator"></span></h5>';
		if(!empty($content)) {$output .= '<p>' . do_shortcode( $content ) . '</p>';}
    $output .= '</div>';
    
	return $output;
}

add_shortcode('cs_heading', 'cstheme_heading');
