<?php
/**
 *	Divider Shortcode
 */

function cstheme_divider($atts, $content) {
   $atts = shortcode_atts(array(
        'size' 				=> 'col-md-12',
        'class' 			=> '',
		'margin_bottom'		=> '',
        'icon' 				=> '',
        'type' 				=> '',
        'color' 			=> '',
        'text' 				=> '',
        'position' 			=> 'center',
        'height' 			=> '0'
    ), $atts);
	
	
    $class = ' cs-divider';
    
	if ($atts['type'] == 'space') {
        $class .= '-space';
    } else {
        $style = 'background-color:' . $atts['color'] . '; height: ' . $atts['height'] . ';';
    }
    
    $output  = cstheme_item($atts, $class);
    $output  .= '<div style="' . $style . '">';
    $output .= '</div></div>';
    
	return $output;
}

add_shortcode('cs_divider', 'cstheme_divider');
