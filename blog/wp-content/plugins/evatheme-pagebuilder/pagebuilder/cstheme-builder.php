<?php

add_action('admin_print_scripts', 'cstheme_builder_admin_scripts');
if (!function_exists('cstheme_builder_admin_scripts')) {
    function cstheme_builder_admin_scripts() {
        global $post,$pagenow;
        $pID = '';
        if(!empty($post->ID)){ $pID=$post->ID; }
        if (current_user_can('edit_posts') && ($pagenow == 'post-new.php' || $pagenow == 'post.php')) {
            if( isset($post) ) {
                wp_localize_script( 'jquery', 'cstheme_script_data', array(
                    'home_uri' => home_url(),
                    'post_id' => $post->ID,
                    'nonce' => wp_create_nonce( 'cstheme-ajax' ),
                    'image_ids' => get_post_meta( $post->ID, 'gallery_image_ids', true ),
                    'label_create' => esc_html__("Create Featured Gallery", "evatheme-pagebuilder"),
                    'label_edit' => esc_html__("Edit Featured Gallery", "evatheme-pagebuilder"),
                    'label_save' => esc_html__("Save Featured Gallery", "evatheme-pagebuilder"),
                    'label_saving' => esc_html__("Saving...", "evatheme-pagebuilder")
                ));
				wp_register_script('cstheme-easy-pie-chart', CSTHEME_PLUGIN_URL . 'assets/js/jquery.easy-pie-chart.js');
                wp_register_script('cstheme-colorpicker', CSTHEME_PLUGIN_URL . 'assets/js/admin-colorpicker.js');
                wp_register_script('cstheme-builder', CSTHEME_PLUGIN_URL . 'assets/js/admin-cstheme-builder.js');

                wp_enqueue_script( 'jquery-ui-dialog' );
				wp_enqueue_script('cstheme-easy-pie-chart');
                wp_enqueue_script('cstheme-colorpicker');
                wp_enqueue_script('cstheme-builder');
				
            }
        }
    }
}


add_action('admin_print_styles', 'cstheme_builder_admin_styles');
if (!function_exists('cstheme_builder_admin_styles')) {
    function cstheme_builder_admin_styles() {
        global $pagenow;
        if (current_user_can('edit_posts') && ($pagenow == 'post-new.php' || $pagenow == 'post.php')) {
            
            wp_register_style('cstheme-font-awesome', CSTHEME_PLUGIN_URL . 'assets/css/font-awesome.min.css');
            wp_register_style('cstheme-colorpicker', CSTHEME_PLUGIN_URL . 'assets/css/admin-colorpicker.css');
            wp_register_style('cstheme-builder', CSTHEME_PLUGIN_URL . 'assets/css/admin-cstheme-builder.css');
            
            wp_enqueue_style('cstheme-font-awesome');
            wp_enqueue_style('cstheme-colorpicker');
            wp_enqueue_style('cstheme-builder');

        }
    }
}


//====== Back-End Includes  ======//

add_action('admin_init', 'cstheme_elements_include');

function cstheme_elements_include(){
    global $cstheme_elementfiles, $cstheme_elements;
    $cstheme_elements = array(); 
    
    require_once (CSTHEME_PLUGIN_PATH . "pagebuilder/elements-option/element_globals.php");
    foreach($cstheme_elementfiles as $file){
        require_once (CSTHEME_PLUGIN_PATH . "pagebuilder/elements-option/$file.php");
    }
    require_once CSTHEME_PLUGIN_PATH . 'pagebuilder/cstheme-shortcode.php';
    
	add_meta_box('cstheme_pagebuilder', esc_html__('Evatheme Page Builder', 'evatheme-pagebuilder'), 'cstheme_pagebuilder_box', 'page', 'normal', 'high');
    
    add_action('wp_ajax_template_add', 'pbTemplateAdd');
    add_action('wp_ajax_template_get', 'pbTemplateGet');
    add_action('wp_ajax_template_remove', 'pbTemplateRemove');
    add_action('wp_ajax_get_circlechart', 'pbGetCircleChart');
    add_action('wp_ajax_get_fonticon', 'pbGetFonticon');
}

function cstheme_pagebuilder_box(){
        global $post, $cstheme_elements,$cstheme_elements_list;
        $items     = '';
        $cstheme_elements_list = '';
        foreach ($cstheme_elements as $element_slug => $element_array) {
            if(empty($element_array['only']) || $element_array['only']==='builder'){
                $items               .= csthemeGetItem($element_slug);
                $cstheme_elements_list .= csthemeGetItem($element_slug,array(),false);
            }
        }        
        $_pb_content_area = '';
        $_pb_content   = get_post_meta($post->ID, CSTHEME_PAGEBUILDER, true);
        $_pb_rows = json_decode(rawUrlDecode($_pb_content), true);
        
        if(!empty($_pb_rows)&&is_array($_pb_rows)){
            foreach($_pb_rows as $_pb_row){
                $_pb_content_area .= csthemeGetRow($_pb_row);
            }
        }else{
            $_pb_content_area .= csthemeGetRow(array('default_row'=>'true'));
        }

        $puilderLogo  = '<div class="pb-logo"></div>';
        
        
        $layoutAddButtons  = '<div class="pb-add-layout-conteiner">';
            $layoutAddButtons .= '<a href="#" class="pb-add-layout" data-possition="bottom">' . esc_html__('Add Container', 'evatheme-pagebuilder') . '</a>';
            $layoutAddButtons .= '<div class="data hidden">'.csthemeGetRow().'</div>';
            $layoutAddButtons .= '<div class="loader">Loading...</div>';
        $layoutAddButtons .= '</div>';
        
        $templates  = '<div class="cs-template-container">';
            $templates .= '<div id="template-save" class="dropdown" tabindex="1">';
                $templates .= '<div class="template">Templates</div>';
                $templates .= '<ul class="dropdown template-container">';
                    $templates .= '<li class="template-item"><a class="template-add">Save this to Template</a></li>';
                    $templates_array = get_option('cs_pb_cstheme_templates',array());
                    if ($templates_array !== false) {
                        foreach ($templates_array as $templates_name => $templates_content) {
                            $templates .= '<li class="template-item"><a class="template-name">' . $templates_name . '</a><span class="template-delete"></span></li>';
                        }
                    }
                $templates .= '</ul>';
            $templates .= '</div>';
        $templates .= '</div>';

        $pbAdditionalTools = $layoutAddButtons.$templates.$puilderLogo;
        
        wp_nonce_field(plugin_basename(__FILE__), 'myplugin_noncename');
        
        echo '<div class="pagebuilder-container">
                    <textarea id="pb_content"   name="pb_content"   class="hidden">' . $_pb_content   . '</textarea>
                    <ul id="size-list" class="hidden">
                        <li data-class="col-md-3" data-text="1 / 4" class="min"></li>
                        <li data-class="col-md-4" data-text="1 / 3"></li>
                        <li data-class="col-md-6" data-text="1 / 2"></li>
                        <li data-class="col-md-8" data-text="2 / 3"></li>
                        <li data-class="col-md-9" data-text="3 / 4"></li>
                        <li data-class="col-md-12" data-text="1 / 1" class="max"></li>
                    </ul>
                    <ul id="custom-content-list" class="hidden" ></ul>
                    <div id="items-list" class="hidden">'.$items.'</div>
                    ' . $pbAdditionalTools . '
                    <div id="pagebuilder-area" class="clearfix">'.$_pb_content_area.'</div>
            </div>';
}





function getItemField($itemSlug, $itemArray) {
        $title = isset($itemArray['title']) ? $itemArray['title'] : '';
        $type = isset($itemArray['type']) ? $itemArray['type'] : '';
        $default = isset($itemArray['default']) ? $itemArray['default'] : '';
        $desc = isset($itemArray['desc']) ? $itemArray['desc'] : '';
        $holder = isset($itemArray['holder']) ? $itemArray['holder'] : '';
        $selector = isset($itemArray['selector']) ? $itemArray['selector'] : '';
        $save_to = isset($itemArray['save_to']) ? $itemArray['save_to'] : '';
        $tinyMCE = isset($itemArray['tinyMCE']) ? $itemArray['tinyMCE'] : '';
        $class = 'field'; ?>
        <div class="field-item<?php echo $type === 'hidden' ? ' hidden' : ''; echo' type-' . $type; echo $tinyMCE?' editor':''; echo " ".$itemSlug; ?>"><?php
            if($type!='container'){
                echo '<div class="field-title">'.$title.'</div>';
                $default = rawUrlDecode($default);
            } ?>
            <div class="field-data"><?php
                switch ($type) {
                    case 'cc' : { ?>
                        <div class="button show-cc-modal"><?php _e('Edit Chart','cstheme'); ?></div>
                        <div class="cc-viewer" style="padding: 20px 0;"></div><?php
                        break;
                    }
                    case 'fi' : { ?>
                        <div class="button show-fi-modal"><?php _e('Edit Icon','cstheme'); ?></div>
                        <div class="fi-viewer"></div><?php
                        break;
                    }
                    case 'gallery':
                    case 'hidden':
                    case 'button':
                    case 'text' : {
                        $typeOrg=$type;
                        if($type==='gallery'){$type='hidden';} ?>
                        <input    data-name="<?php echo $itemSlug; ?>" data-type="<?php echo $type; ?>" data-type-org="<?php echo $typeOrg; ?>" class="<?php echo $class; ?>" value="<?php echo htmlspecialchars($default); ?>" placeholder="<?php echo $holder; ?>" data-selector="<?php echo $selector; ?>" data-save-to="<?php echo $save_to; ?>" type="<?php echo $type; ?>" /><?php
                        if (!empty($itemArray['data'])) {
                            global $cstheme_elements;
                            echo '<div class="data hidden">';
                            $tmpItem = $itemArray['data']['item'];
                            $tmpSettings = $itemArray['data']['settings'];
                            getItemField($tmpSettings, $cstheme_elements[$tmpItem]['settings'][$tmpSettings]);
                            echo '</div>';
                        }
                        if($typeOrg==='gallery'){
                            echo '<ul class="gallery-images">';
                                $images = empty($default) ? false : explode(",", $default);
                                if($images){foreach ($images as $id) {if(!empty($id)){ ?><li><img width="100" height="auto" src="<?php echo wp_get_attachment_url($id); ?>"></li><?php }}}
                            echo '</ul>';
                        }
                        $type=$typeOrg;
                        break;
                    }
                    case 'color': { ?>
                        <div style="background-color: <?php echo empty($default)?'':$default; ?>;" class="color-info"></div>
                        <input    data-name="<?php echo $itemSlug; ?>" data-type="<?php echo $type; ?>" class="<?php echo $class; ?>" value="<?php echo empty($default)?'':$default; ?>" placeholder="<?php echo $holder; ?>" type="text" /><?php
                        break;
                    }
                    case 'checkbox': { ?>
                        <input    data-name="<?php echo $itemSlug; ?>" data-type="<?php echo $type; ?>" class="<?php echo $class; ?> hidden" value="<?php echo $default; ?>" placeholder="<?php echo $holder; ?>" type="checkbox" <?php echo $default==='true'?'checked':''; ?> />
                        <div class="checkbox-text clearfix"><div class="checkbox-true"><?php esc_html_e('ON','evatheme-pagebuilder'); ?></div><div class="checkbox-false"><?php esc_html_e('OFF','evatheme-pagebuilder'); ?></div></div><?php
                        break;
                    }
                    case 'textArea': { ?>
                        <textarea data-name="<?php echo $itemSlug; ?>" data-type="<?php echo $type; ?>" class="<?php echo $class; ?>" placeholder="<?php echo $holder; ?>" data-tinyMCE="<?php echo $tinyMCE; ?>" ><?php echo $default; ?></textarea><?php
                        break;
                    }
                    case 'category':
                    case 'select': { ?>
                        <select   data-name="<?php echo $itemSlug; ?>" data-type="<?php echo $type; ?>" class="<?php echo $class; ?>"><?php
                            $hide = isset($itemArray['hide']) ? $itemArray['hide'] : '';
                            foreach ($itemArray['options'] as $val => $text) {
                                echo '<option value="' . $val . '"' . ($default === strval($val) ? ' selected="selected"' : '') . ' hide="' . (isset($hide[$val]) ? $hide[$val] : '') . '">' . $text . '</option>';
                            } ?>
                        </select>
                        <?php
                        if($type === 'category'){
                            echo '<div class="category-list-container"></div>';
                        }
                        break;
                    }
                    case 'container': {
                        $title_as = isset($itemArray['title_as']) ? $itemArray['title_as'] : '';
                        $add_button = isset($itemArray['add_button']) ? $itemArray['add_button'] : '';
                        $container_type = isset($itemArray['container_type']) ? $itemArray['container_type'] : ''; ?>
                        <div data-name="<?php echo $itemSlug; ?>" data-type="<?php echo $type; ?>" data-container-type="<?php echo $container_type; ?>" class="<?php echo $class; ?> container" placeholder="<?php echo $holder; ?>" data-title-as="<?php echo $title_as; ?>" data-add-button="<?php echo $add_button; ?>" ><?php
                            if(!empty($default)) {
                                foreach ($default as $data) { ?>
                                    <div class="container-item<?php echo $container_type==='image_slider'?' expanded':''; ?>">
                                        <div class="list clearfix">
                                            <div class="name"><?php if(isset($data[$title_as]['default'])){echo $data[$title_as]['default'];} ?></div>
                                            <div class="actions">
                                                <a href="#" class="action-edit" title="Edit">e</a>
                                                <a href="#" class="action-duplicate" title="Duplicate"></a>
                                                <a href="#" class="action-delete"  title="Delete"></a>
                                            </div>
                                        </div>
                                        <div class="content"><?php
                                            if($container_type==='image_slider'){
                                                echo '<img class="image-src" src="'.rawUrlDecode($data[$title_as]['default']).'" />';
                                            }
                                            $ccPrint=$fiPrint=true;
                                            foreach ($data as $slug => $setting) {
                                                //Font icon
                                                if(isset($setting['need_fi'])&&$setting['need_fi']==='true'&&$fiPrint){
                                                    echo getItemField('fi', array("type"=>"fi","title"=>"Add Icon"));
                                                }
                                                if($slug==='fi'){$fiPrint=false;}
                                                //Circle Chart
                                                if(isset($setting['need_cc'])&&$setting['need_cc']==='true'&&$ccPrint){
                                                    echo getItemField('cc', array("type"=>"cc","title"=>"Add Chart"));
                                                }
                                                if($slug==='cc'){$ccPrint=false;}
                                                echo getItemField($slug, $setting);
                                            } ?>
                                        </div>
                                    </div><?php
                                }
                            }?>
                        </div><?php
                        break;
                    }
                } ?>
            </div><?php
            if($type!='container'){ echo '<div class="field-desc">'.$desc.'</div>';} ?>
        </div><?php
}


    
    

function csthemeGetItem($itemSlug, $itemNewData = array(),$all=true) {
        global $cstheme_elements, $cstheme_global_options;
        
        $itemArray = $cstheme_elements[$itemSlug];
        $itemArray['size'] = empty($itemNewData['size']) ? $itemArray['size'] : $itemNewData['size'];
        ob_start(); ?>
        <div class="action-container item <?php echo $itemArray['size']; ?>" data-slug="<?php echo $itemSlug; ?>"<?php if(isset($itemArray['min-size'])){echo ' data-min="'.$itemArray['min-size'].'"';} ?> data-help="<?php echo isset($itemArray['help'])?$itemArray['help']:''; ?>">
            <div class="thumb"><?php echo $itemArray['name']; ?></div><?php
            if($all){ ?>
                <div class="list clearfix">
                    <div class="size-sizer-container">
                        <div class="size need-convert"><?php echo $itemArray['size']; ?></div>
                        <div class="sizer"><a class="down" href="#" title="Decrease Size"></a><a class="up" href="#" title="Increase Size"></a></div>
                    </div>
                    <div class="name"><?php echo $itemArray['name']; ?></div>
                    <div class="actions">
                        <a href="#" class="action-edit" title="Edit"></a>
                        <a href="#" class="action-duplicate" title="Duplicate"></a>
                        <a href="#" class="action-delete" title="Delete"></a>
                    </div>
                </div>
                <div class="data">
                    <div class="general-field-container clearfix"><?php
                        foreach ($cstheme_global_options['element'] as $pbHeadSettingSlug => $pbHeadSetting) {
                            $pbHeadSetting['default'] = isset($itemNewData[$pbHeadSettingSlug]) ? $itemNewData[$pbHeadSettingSlug] : (!empty($pbHeadSetting['default'])?$pbHeadSetting['default']:'');
                            echo getItemField($pbHeadSettingSlug, $pbHeadSetting);
                        } ?>
                    </div>
                    <div class="custom-field-container"><?php
                        foreach ($itemArray['settings'] as $pbItemSettingSlug => $pbItemSetting) {
                            if ($pbItemSetting['type'] === 'container' && isset($itemNewData['settings'][$pbItemSettingSlug])) {
                                $templateContainerItem = $pbItemSetting['default'][0];
                                foreach ($itemNewData['settings'][$pbItemSettingSlug] as $index => $containerItemNewData) {
                                    foreach ($containerItemNewData as $containerItemNewFieldSlug => $containerItemNewFieldValue) {
                                        $templateContainerItem[$containerItemNewFieldSlug]['default'] = $containerItemNewFieldValue;
                                        $itemNewData['settings'][$pbItemSettingSlug][$index][$containerItemNewFieldSlug] = $templateContainerItem[$containerItemNewFieldSlug];
                                    }
                                }
                            }
                            $pbItemSetting['default'] = isset($itemNewData['settings'][$pbItemSettingSlug]) ? $itemNewData['settings'][$pbItemSettingSlug] : (isset($pbItemSetting['default'])?$pbItemSetting['default']:"");
                            echo getItemField($pbItemSettingSlug, $pbItemSetting);
                        } ?>
                    </div>
					<div class="general-field-container clearfix"><?php
                        foreach ($cstheme_global_options['element_settings_bottom'] as $pbHeadSettingSlug => $pbHeadSetting) {
                            $pbHeadSetting['default'] = isset($itemNewData[$pbHeadSettingSlug]) ? $itemNewData[$pbHeadSettingSlug] : (!empty($pbHeadSetting['default'])?$pbHeadSetting['default']:'');
                            echo getItemField($pbHeadSettingSlug, $pbHeadSetting);
                        } ?>
                    </div>
                </div><?php
            } ?>
        </div><?php
        $output = ob_get_clean();
        return $output;
}




function csthemeGetRowTools($rowNewData=array()) {
    global $cstheme_elements, $cstheme_elements_list,$cstheme_global_options;
    $rowNewData['row_layout'] = isset($rowNewData['row_layout'])?$rowNewData['row_layout']:'full';
    $output ='<div class="list clearfix">';
        $output.='<div class="add-element">';
            $output.='<a href="#" class="elements-dropdown clearfix">' . esc_html__('Add Element', 'evatheme-pagebuilder') . '</a>';
        $output.='</div>';
        $output.='<div class="change-layout">';
            $output.='<a href="#" class="sidebar left-sidebar tooltip' .($rowNewData['row_layout']==='left' ?' active':'').'" data-value="4,8,0"  data-input="left" ><span>Left Sidebar</span></a>';
            $output.='<a href="#" class="sidebar full tooltip'         .($rowNewData['row_layout']==='full' ?' active':'').'" data-value="0,12,0" data-input="full" ><span>Full Width</span></a>';
            $output.='<a href="#" class="sidebar right-sidebar tooltip'.($rowNewData['row_layout']==='right'?' active':'').'" data-value="0,8,4"  data-input="right"><span>Right Sidebar</span></a>';
            # $output.='<a href="#" class="sidebar half tooltip'         .($rowNewData['row_layout']==='half' ?' active':'').'" data-value="0,6,6"  data-input="half" ><span>Half</span></a>';
        $output.='</div>';
        $output.='<div class="name">' . esc_html__('Container Settings', 'evatheme-pagebuilder') . '</div>';
        $output.='<div class="actions">';
            $output.='<a href="#" class="action-expand tooltip"><span>' . esc_html__('Curtail', 'evatheme-pagebuilder') . '</span></a>';
			$output.='<a href="#" class="action-edit tooltip"><span>' . esc_html__('Edit Settings', 'evatheme-pagebuilder') . '</span></a>';
            $output.='<a href="#" class="action-duplicate tooltip"><span>' . esc_html__('Duplicate', 'evatheme-pagebuilder') . '</span></a>';
            $output.='<a href="#" class="action-delete tooltip"><span>' . esc_html__('Delete', 'evatheme-pagebuilder') . '</span></a>';
        $output.='</div>';
        $output.='<div class="pagebuilder-elements-container" class="clearfix">' . $cstheme_elements_list . '</div>';
    $output.='</div>';
    $output.='<div class="data">';
        $output.='<div class="custom-field-container">';
            foreach ($cstheme_global_options['row'] as $pbRowSettingSlug => $pbRowSetting){
                $pbRowSetting['default'] = isset($rowNewData[$pbRowSettingSlug]) ? $rowNewData[$pbRowSettingSlug] : (isset($pbRowSetting['default'])?$pbRowSetting['default']:'');
                ob_start();
                getItemField($pbRowSettingSlug, $pbRowSetting);
                $output.=ob_get_clean();
            }
        $output.='</div>';
    $output.='</div>';
    return $output;
}




function csthemeGetLayout($_pb_layout) {
    $output  = '<div class="action-container clearfix builder-area '.$_pb_layout['size'].'">';
        if(isset($_pb_layout['items'])&&is_array($_pb_layout['items'])){
            foreach ($_pb_layout['items'] as $item_array) {
                $output .= csthemeGetItem($item_array['slug'], $item_array);
            }
        }
    $output .= '</div>';
    return $output;
}




function csthemeGetRow($_pb_row=array() ) {
    $output  = '<div class="row-container action-container clearfix '.(isset($_pb_row['default_row'])&&$_pb_row['default_row']==='true'?'default':'additional').'-row">';
        $output .= csthemeGetRowTools($_pb_row); 
        if(isset($_pb_row['layouts'])&&is_array($_pb_row['layouts'])){
            foreach($_pb_row['layouts'] as $_pb_layout){
                $output .= csthemeGetLayout($_pb_layout);
            }
        }else{
            foreach(array('col-md-0','col-md-12','col-md-0')as$size){
                $output .= csthemeGetLayout(array('size'=>$size));
            }
        }
    $output .= '</div>';
    return $output;
}




// Save fields data

add_action('save_post', 'cstheme_pagebuilder_save');

function cstheme_pagebuilder_save($post_id) {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post_id;
        if (isset($_GET['post_type']) && 'page' == $_GET['post_type']) {
            if (!current_user_can('edit_page', $post_id))
                return $post_id;
        } else {
            if (!current_user_can('edit_post', $post_id))
                return $post_id;
        }
        if (isset($_POST['pb_content'])) {
            update_post_meta($post_id, CSTHEME_PAGEBUILDER, $_POST['pb_content']);
            $_pb_row_array = json_decode(rawUrlDecode(str_replace("\'","'",$_POST['pb_content'])), true);
            update_post_meta($post_id, CSTHEME_SHORTCODE,cstheme_page_builder($_pb_row_array));
        }
}



// Template Ajax Action


function pbTemplateAdd() {
    
    if (isset($_REQUEST['template_name']) && isset($_REQUEST['template_content'])) {
        $response = '';
        $templates_array = get_option('cs_pb_'.strtolower(THEMENAME).'_templates',array());
        if (isset($templates_array[$_REQUEST['template_name']])) {
            $response .= '<div class="error">' . esc_html__('Template name is allready exist. Please insert the template name and try again', 'evatheme-pagebuilder') . '</div>';
        } else {
            $upRes = update_option('cs_pb_'.strtolower(THEMENAME).'_templates_'.$_REQUEST['template_name'], $_REQUEST['template_content']);
            if($upRes){
                $templates_array[$_REQUEST['template_name']] = 'true';
                $upRes=update_option('cs_pb_'.strtolower(THEMENAME).'_templates', $templates_array);
            }
            if($upRes){
                $response .= '<div class="succes">' . esc_html__('Template added', 'evatheme-pagebuilder') . '</div>';
            }else{
                $response .= '<div class="error">' . esc_html__('Not Saved !!!', 'evatheme-pagebuilder') . '</div>';
            }
        }
        die('<div class="response">' . $response . '</div>');
    }
}



function pbTemplateGet() {
    if (isset($_REQUEST['template_name'])) {
        $response = '';
        $templates_array = get_option('cs_pb_'.strtolower(THEMENAME).'_templates',array());
        $currTemplate    = get_option('cs_pb_'.strtolower(THEMENAME).'_templates_'.$_REQUEST['template_name'],false);
        if (isset($templates_array[$_REQUEST['template_name']])&&$currTemplate!==false) {
            $currTemplateArea = '';
            $currTemplateRows = json_decode(rawUrlDecode($currTemplate), true);
            if(!empty($currTemplateRows)&&is_array($currTemplateRows)){
                foreach($currTemplateRows as $currTemplateRow){
                    $currTemplateArea .= csthemeGetRow($currTemplateRow);
                }
            }else{
                $currTemplateArea .= csthemeGetRow(array('default_row'=>'true'));
            }
            $response .= '<div class="data">';
                $response .= '<div class="content">'. $currTemplateArea . '</div>';
            $response .= '</div>';
        } else {
            $response .= '<div class="error">' . esc_html__('Template name not exsist', 'evatheme-pagebuilder') . '</div>';
        }
        die('<div class="response">' . $response . '</div>');
    }
}



function pbTemplateRemove() {
    if (isset($_REQUEST['template_name'])) {
        $response = '';
        $templates_array = get_option('cs_pb_'.strtolower(THEMENAME).'_templates',array());
        if (isset($templates_array[$_REQUEST['template_name']])) {
            unset($templates_array[$_REQUEST['template_name']]);
            update_option('cs_pb_'.strtolower(THEMENAME).'_templates', $templates_array);
            update_option('cs_pb_'.strtolower(THEMENAME).'_templates_'.$_REQUEST['template_name'], '');
        } else {
            $response .= '<div class="error">' . esc_html__('Template name not exsist', 'evatheme-pagebuilder') . '</div>';
        }
        die('<div class="response">' . $response . '</div>');
    }
}


function pbGetFonticon() {
    require_once (CSTHEME_PLUGIN_PATH . "pagebuilder/font-icon.php");
    die();
}



function pbGetCircleChart() {
    require_once (CSTHEME_PLUGIN_PATH . "pagebuilder/circle-chart.php");
    die();
}