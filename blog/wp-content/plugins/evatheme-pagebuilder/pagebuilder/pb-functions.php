<?php
/**
 * CStheme Page Builder functions and definitions
 */


add_filter('body_class', 'cstheme_class');
function cstheme_class($classes) {
    global $post;
	
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
    if (is_page() && is_plugin_active( 'cstheme-pagebuilder/cs_builder.php' ) && get_post_meta($post->ID, 'cstheme_metabox_shortcode', true)) {
        $classes[] = 'cstheme-pagebuilder';
    }
    return $classes;
}

 
// CS Theme Item
function cstheme_item($atts,$class='',$margin_bottom='',$title_size='',$title_aligment=''){
    if ( !empty( $atts['layout_size'] ) && $atts['layout_size'] === 'col-md-4' ) {
        $atts['size'] = 'col-md-12';
    }
    if( !empty( $atts['size'] ) ){
        $class .= ' '.$atts['size'];
    }
    if( !empty( $atts['class'] ) ){
        $class .= ' '.$atts['class'];
    }
	if( !empty( $atts['margin_bottom'] ) ){
        $margin_bottom .= $atts['margin_bottom'];
    }
	if( !empty( $atts['title_size'] ) ){
		$title_size .= $atts['title_size'];
	}
	if( !empty( $atts['title_aligment'] ) ){
		$title_aligment .= $atts['title_aligment'];
	}
	
    $output = '<div class="cs-element '. $class .'" style="margin-bottom:'. $margin_bottom .';">';
    if( !empty( $atts['title'] ) ){
		$output .= '<div class="cstheme-title mb30" style="text-align:'. $title_aligment .';"><'. $title_size .'>' . rawUrlDecode($atts['title']) . '</'. $title_size .'></div>';
    }
    return $output;
}


//	Post Author Social Icons
function evapb_add_remove_contactmethods( $contactmethods ) {
	$contacts = evapb_author_contact_methods();
	
	foreach($contacts as $k=>$v) {
		$contactmethods[$k] = $v;
	}

    // Remove Contact Methods
    unset($contactmethods['aim']);
    unset($contactmethods['yim']);
    unset($contactmethods['jabber']);

    return $contactmethods;
}
add_filter('user_contactmethods','evapb_add_remove_contactmethods',10,1);

function evapb_author_contact_methods() {
	$contactmethods = array();
    $contactmethods['evapb_author_facebook'] = esc_html__('Facebook','evapb');
	$contactmethods['evapb_author_twitter'] = esc_html__('Twitter','evapb');
    $contactmethods['evapb_author_googleplus'] = esc_html__('Google Plus','evapb');
	$contactmethods['evapb_author_instagram'] = esc_html__('Instagram','evapb');
    $contactmethods['evapb_author_linkedin'] = esc_html__('Linked In','evapb');
    $contactmethods['evapb_author_youtube'] = esc_html__('YouTube','evapb');
    $contactmethods['evapb_author_vimeo'] = esc_html__('Vimeo','evapb');
    $contactmethods['evapb_author_tumblr'] = esc_html__('Tumblr','evapb');
    $contactmethods['evapb_author_skype'] = esc_html__('Skype','evapb');
    $contactmethods['evapb_author_vkontakte'] = esc_html__('Vkontakte','evapb');
    $contactmethods['evapb_author_pinterest'] = esc_html__('Pinterest','evapb');
    $contactmethods['evapb_author_wordpress'] = esc_html__('Wordpress','evapb');
    $contactmethods['evapb_author_dropbox'] = esc_html__('Dropbox','evapb');
    $contactmethods['evapb_author_rss'] = esc_html__('RSS','evapb');
	
	return $contactmethods;
}

function evapb_author_social_networks() {
	
	$options = evapb_author_contact_methods();
	
	$social_icons = array(
        "evapb_author_facebook" => "fa fa-facebook",
        "evapb_author_googleplus" => "fa fa-google-plus",
        "evapb_author_twitter" => "fa fa-twitter",
        "evapb_author_instagram" => "fa fa-instagram",
        "evapb_author_vimeo" => "fa fa-vimeo",
        "evapb_author_vkontakte" => "fa fa-vk",
        "evapb_author_youtube" => "fa fa-youtube",
        "evapb_author_linkedin" => "fa fa-linkedin",
        "evapb_author_pinterest" => "fa fa-pinterest-p",
        "evapb_author_wordpress" => "fa fa-wordpress",
        "evapb_author_dropbox" => "fa fa-dropbox",
        "evapb_author_rss" => "fa fa-rss",
    );
	
	ob_start();
	
	echo '<div class="author_socicons">';
	
	foreach($social_icons as $option=>$class) {
		$title = $options[$option];
		$link = get_the_author_meta($option);
		
		if (empty($link)) {
			continue;
		}
		
		echo '
			<a href="' . esc_url( $link ) . '" title="' . esc_attr( $title ) . '">
				<i class="' . esc_attr( $class ) . '"></i>
			</a>
		';
	}
	
	echo '</div>';
	
	return ob_get_clean();
}