<?php
if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
     return;
if ( get_user_option('rich_editing') == 'true') {
     add_filter('mce_external_plugins', 'cstheme_tinymce_external');
     add_filter('mce_buttons', 'cstheme_tinymce_button');
}
   
function cstheme_tinymce_external($plugin_array) {
   $plugin_array['csshortcodegenerator'] = CSTHEME_PLUGIN_URL .'assets/js/admin-cstheme-shortcode.js';
   return $plugin_array;
}
function cstheme_tinymce_button($buttons) {
   array_push($buttons, "|", "csshortcodegenerator");
   return $buttons;
}
    
function cstheme_refresh_mce($ver) {
    $ver += 3;
    return $ver;
}
add_filter( 'tiny_mce_version', 'cstheme_refresh_mce');

//====== START - Functions ======
if (!function_exists('cstheme_shortcode_admin_html')){
    function cstheme_shortcode_admin_html(){
        global $cstheme_elements; ?>
        <div id="cs-shortcode-template" style="display: none;">
            <div class="general-field-container">
                <div class="field-item clearfix type-select">
                    <div class="field-data">
                        <select id="style_shortcode" data-type="select" class="field">
                            <option value="none"><?php _e('Select Shortcode','cstheme'); ?></option><?php
                            if(!empty($cstheme_elements)){
                                foreach ($cstheme_elements as $pbItemSlug => $pbItemArray) {
                                    if(empty($pbItemArray['only']) || $pbItemArray['only']==='shortcode'){
                                        echo '<option value="' . $pbItemSlug . '" >' . $pbItemArray['name'] . '</option>';
                                    }
                                }
                            } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="custom-field-container"></div>
        </div><?php
    }
}
add_action( 'admin_head', 'cstheme_shortcode_admin_html', 1 );

// Shortcode Builder
if (!function_exists('cstheme_shortcode_modal')) {
    function cstheme_shortcode_modal() {
        if (!empty($_REQUEST['shortcode_name'])) {            
            die(csthemeGetItem($_REQUEST['shortcode_name']));
        }
    }
} add_action('wp_ajax_cstheme_shortcode_modal', 'cstheme_shortcode_modal');


if (!function_exists('cstheme_shortcode_print')) {
    function cstheme_shortcode_print(){
        if (!empty($_REQUEST['data'])) {
            $item_array = json_decode(rawUrlDecode(str_replace("\'","'",$_REQUEST['data'])), true);
            die(cstheme_builder_item($item_array));
        }
        die('<div class="error">Empty Request</div>');
    }
} add_action('wp_ajax_cstheme_shortcode_print', 'cstheme_shortcode_print');

if (!function_exists('rowStart')) {
    function rowStart($colCounter,$size){
        if($colCounter===0||$colCounter===12||$colCounter+$size>12 ){return array($size,'true');}
        return array($colCounter+$size,'false');
    }
}
if (!function_exists('cstheme_builder_item')) {
    function cstheme_builder_item($item_array) {
        global $cstheme_elements,$cs_layoutSize;
        ob_start();
        $itemSlug = $item_array['slug'];
        $itemSettingsArray = $item_array['settings'];
        $defaultItem=$cstheme_elements[$itemSlug];
        $defaultItemSettingsArray=$defaultItem['settings'];
        $itemClass = !empty($item_array['custom_class']) ? $item_array['custom_class'] : '';
        $itemMargBottom = !empty($item_array['element_margin_bottom']) ? $item_array['element_margin_bottom'] : '';
        $onlyBuilderAtts='';
        if($item_array['size']==='shortcode-size'){
            $onlyBuilderAtts .= '  size="cstheme-shortcode"';
        }else{
            $onlyBuilderAtts .= ' title="'.$item_array['item_title'].'" title_aligment="'.$item_array['item_title_aligment'].'" title_size="'.$item_array['item_title_size'].'" size="'.$item_array['size'].'" class="'.str_replace('"','&quot;',rawUrlDecode($itemClass)).'" layout_size="'.$cs_layoutSize.'" row_type="'.(isset($item_array['row-type'])?$item_array['row-type']:'row').'" margin_bottom="'.str_replace('"','&quot;',rawUrlDecode($itemMargBottom)).'"';
        }
        $content_slug=  empty($defaultItem['content'])?'':$defaultItem['content'];
        echo '[cs_'.$itemSlug.$onlyBuilderAtts;
            foreach($defaultItemSettingsArray as $settings_slug=>$default_settings_array){
                if($content_slug!==$settings_slug&&$default_settings_array['type']!='category'&&$default_settings_array['type']!='button'&&$default_settings_array['type']!='fi'&&$default_settings_array['type']!='cc'){
                    $settings_val=isset($itemSettingsArray[$settings_slug])?$itemSettingsArray[$settings_slug]:(isset($default_settings_array['default'])?$default_settings_array['default']:'');
                    echo ' '.$settings_slug.'="'.str_replace('"','&quot;',rawUrlDecode($settings_val)).'"';
                }
            }
        echo ']';
        if($content_slug){
            $settings_val='';
            if($defaultItemSettingsArray[$content_slug]['type']==='container'&&isset($defaultItemSettingsArray[$content_slug]['default'][0])){
                $defaultContainarItem=$defaultItemSettingsArray[$content_slug]['default'][0];
                $containarItemArray =$itemSettingsArray[$content_slug];
                foreach($containarItemArray as $index=>$containarItem){
                    $containarItemContent='';
                    $settings_val .= '[cs_'.$itemSlug.'_item';
                    foreach($containarItem as $slug=>$value){
                        if($defaultContainarItem[$slug]['type']!='category'&&$defaultContainarItem[$slug]['type']!='button'&&$defaultContainarItem[$slug]['type']!='fi'&&$default_settings_array['type']!='cc'){
                            if($defaultContainarItem[$slug]['type']==='textArea'){
                                $containarItemContent=rawUrlDecode($value);
                            }else{
                                $settings_val .= ' '.$slug.'="'.str_replace('"','&quot;',rawUrlDecode($value)).'"';
                            }
                        }
                    }
                    $settings_val .= ']';
                    if(!empty($containarItemContent)){
                        $settings_val .= $containarItemContent.'[/cs_'.$itemSlug.'_item]';
                    }
                }
            }else{
                $settings_val=isset($itemSettingsArray[$content_slug])?$itemSettingsArray[$content_slug]:$defaultItemSettingsArray[$content_slug]['default'];
                $settings_val=rawUrlDecode($settings_val);
            }
            echo $settings_val.'[/cs_'.$itemSlug.']';
        }
        $output = ob_get_clean();
        return $output;
    }
}
if (!function_exists('cstheme_page_builder')) {
    function cstheme_page_builder($_pb_row_array) {
        
		global $post, $cs_startPrinted, $cstheme_elements;
        
		$endPrint=false;
        
		ob_start();
		
        if(empty($_pb_row_array)){
            return false;
        } else {
            $layoutsEcho = '';
			
			foreach($_pb_row_array as $_pb_row){
                $bg_type = $evapb_row_styles = '';
                $bgStyle = isset($_pb_row['background_style'])?$_pb_row['background_style']:'scroll';
                switch ($bgStyle) {
                    case 'scroll':
                        $bgClass = ' bg-scroll';
                        break;
                    case 'fixed':
                        $bgClass = ' bg-fixed';
                        break;
                    case 'parallax':
                        $bg_type = "parallax";
                        $bgClass = ' cstheme_parallax';
                        break;
                    default:
                        $bgClass = ' bg-pattern';
                        break;
                }
                
                $rowCustomClass = !empty($_pb_row['row_custom_class'])?(' '.$_pb_row['row_custom_class']):'';
                $rowCustomId = !empty($_pb_row['row_custom_id'])?(' id="'.$_pb_row['row_custom_id'].'"'):'';
                $evapb_container_size = isset( $_pb_row['container_size'] ) ? $_pb_row['container_size'] : 'boxed';
                $class = $bgClass.$rowCustomClass;
                $_pb_row['background_color']=isset($_pb_row['background_color'])?str_replace(' ','',$_pb_row['background_color']):'';
                $style = empty($_pb_row['background_color'])?'':('background-color:'.$_pb_row['background_color'].';');
                $style .= empty($_pb_row['background_image'])?'':('background-image:url('.$_pb_row['background_image'].');');
                $padding_top = $padding_right = $padding_bottom = $padding_left = '';
                if( isset($_pb_row['padding_top']) && $_pb_row['padding_top'] != '') {
                    $padding_top = $_pb_row['padding_top'];
					$evapb_row_styles .= ' padding-top: ' . esc_attr( $padding_top ) . ';';
                }
                if(isset($_pb_row['padding_right']) && $_pb_row['padding_right'] != '') {
                    $padding_right = $_pb_row['padding_right'];
					$evapb_row_styles .= ' padding-right: ' . esc_attr( $padding_right ) . ';';
                }
				if(isset($_pb_row['padding_bottom']) && $_pb_row['padding_bottom'] != '') {
                    $padding_bottom = $_pb_row['padding_bottom'];
					$evapb_row_styles .= ' padding-bottom: ' . esc_attr( $padding_bottom ) . ';';
                }
				if(isset($_pb_row['padding_left']) && $_pb_row['padding_left'] != '') {
                    $padding_left = $_pb_row['padding_left'];
					$evapb_row_styles .= ' padding-left: ' . esc_attr( $padding_left ) . ';';
                }
				
				if( $evapb_container_size == 'full_width' ) {
					$class .= ' pl40 pr40 ';
				}
				
				$class .= ' ' . $evapb_container_size;
                
                $layoutsEcho .= '<div' . $rowCustomId . ' class="row-container ' . $class . '" style="' . $style . '">';
					if( $evapb_container_size != 'full_width' ) {
						$layoutsEcho .= '<div class="container">';
					}
						$layoutsEcho .= '<div class="row" style="' . $evapb_row_styles . '">';

							foreach($_pb_row['layouts'] as $_pb_layout){
								if($_pb_layout['size']!=='col-md-0'){
									global $cs_layoutSize;
									$cs_layoutSize = $_pb_layout['size'];
									$layoutsEcho .= '[cs_layout bg_type="'.$bg_type.'" size="'.$_pb_layout['size'].'" layout_custom_class="'.(isset($_pb_layout['layout_custom_class'])?$_pb_layout['layout_custom_class']:'').'"]';
										$cs_startPrinted=false;    
										$colCounter=0;
										$start='true';
										foreach ($_pb_layout['items'] as $item_array){
											list($colCounter,$start)=rowStart($colCounter,$_pb_layout['size']==='col-md-3'?12:intval(str_replace('col-md-','',$item_array['size'])));
											$endPrint=true;
											$rowClass = $item_array['row-type'] = !empty($cstheme_elements[$item_array['slug']]['row-type'])?$cstheme_elements[$item_array['slug']]['row-type']:'row';
											if($start === "true") {
												if($cs_startPrinted){$layoutsEcho .= '</div>';}
												$cs_startPrinted=true;
												$layoutsEcho .= '<div class="'.$rowClass.'">';
											}
											$layoutsEcho .= cstheme_builder_item($item_array);
										}
										if($cs_startPrinted){$layoutsEcho.='</div>';}
									$layoutsEcho .= '[/cs_layout]';
								}
							}
				
						$layoutsEcho .= '</div>';
					if( $evapb_container_size != 'full_width' ) {
						$layoutsEcho .= '</div>';
					}
				$layoutsEcho .= '</div>';
            
			}
            if($endPrint){
                echo $layoutsEcho;
            }else{
                return false;
            }
        }
        $output = ob_get_clean();
        return $output;
    }
}