<?php

global $cstheme_elements, $cstheme_element_options;

$cstheme_elements['slider'] = array(
    'name' => esc_html__('Blog Posts Slider', 'evatheme-pagebuilder'),
    'size' => 'col-md-12',
    'min-size' => 'col-md-12',
    'only' => 'builder',
    'settings' => array(
        'style' => array(
            'title' => esc_html__('Top Slider Style', 'evatheme-pagebuilder'),
			'desc' => esc_html__('select a style to display Slider Posts', 'evatheme-pagebuilder'),
            'type' => 'select',
			'options' => array(
							'style1' => esc_html__('Style 1 (thumbnails bottom)', 'evatheme-pagebuilder'),
							'style2' => esc_html__('Style 2 (thumbnails left)', 'evatheme-pagebuilder'),
							'style3' => esc_html__('Style 3 (three posts)', 'evatheme-pagebuilder'),
							'style4' => esc_html__('Style 4 (narrow. with pagination)', 'evatheme-pagebuilder'),
							'style5' => esc_html__('Style 5 (full width)', 'evatheme-pagebuilder'),
							'style6' => esc_html__('Style 6 (grid)', 'evatheme-pagebuilder'),
						),
            'default' => 'style1',
			'hide' => array(
							'style1' => 'slider_columns',
							'style2' => 'slider_columns',
							'style3' => 'slides_count,slider_columns',
							'style4' => 'slider_columns',
							'style5' => 'slider_columns',
						),
        ),
		'category' => array(
            'title' => esc_html__('Blog category', 'evatheme-pagebuilder'),
			'desc' => esc_html__('If you want to display Specify category then choose.', 'evatheme-pagebuilder'),
            'type' => 'category',
            'options' => $cstheme_element_options['cat']['post'],
            'default' => '0',
        ),
        'category_ids' => array(
            'type' => 'hidden',
            'selector' => 'category',
            'default' => '',
        ),
        'slides_count' => array(
            'title' => esc_html__('Slides Count', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Insert how many slides will displayed in Top Slider', 'evatheme-pagebuilder'),
            'type' => 'text',
            'holder' => '4',
            'default' => '4',
        ),
		'slider_columns' => array(
            'title' => esc_html__('Slider Columns', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Select how many columns will displayed in Top Slider', 'evatheme-pagebuilder'),
            'type' => 'select',
			'options' => array(
							'col3' => esc_html__('3 Columns', 'evatheme-pagebuilder'),
							'col4' => esc_html__('4 Columns', 'evatheme-pagebuilder'),
							'col5' => esc_html__('5 Columns', 'evatheme-pagebuilder'),
						),
            'default' => 'col3',
        ),
		'orderby' => array(
            'title' => esc_html__('Order', 'evatheme-pagebuilder'),
            'type' => 'select',
            'options' => $cstheme_element_options['orderby'],
            'default' => 'date_desc',
        ),
    ),
);