<?php
global $cstheme_elements,$cstheme_element_options;
$cstheme_elements['blog_carousel'] = array(
    'name' => esc_html__('Blog Carousel', 'evatheme-pagebuilder'),
    'size' => 'col-md-12',
    'min-size' => 'col-md-12',
    'only' => 'builder',
    'settings' => array(
        'posts_style' => array(
            'title' => esc_html__('Blog Posts Style', 'evatheme-pagebuilder'),
			'desc' => esc_html__('select a style to display blog posts', 'evatheme-pagebuilder'),
            'type' => 'select',
			'options' => array(
							'listing_hover' 	=> esc_html__('Listing on hover', 'evatheme-pagebuilder'),
							'fisrt_big_img' 	=> esc_html__('First Big Images', 'evatheme-pagebuilder'),
							'small_carousel' 	=> esc_html__('Small carousel', 'evatheme-pagebuilder'),
						),
            'default' => 'listing_hover',
        ),
		'category' => array(
            'title' => esc_html__('Blog category', 'evatheme-pagebuilder'),
			'desc' => esc_html__('If you want to display Specify category then choose.', 'evatheme-pagebuilder'),
            'type' => 'category',
            'options' => $cstheme_element_options['cat']['post'],
            'default' => '0',
        ),
        'category_ids' => array(
            'type' => 'hidden',
            'selector' => 'category',
            'default' => '',
        ),
        'post_count' => array(
            'title' => esc_html__('Post Count', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Insert how many posts will displayed in blog.', 'evatheme-pagebuilder'),
            'type' => 'text',
            'holder' => '6',
            'default' => '6',
        ),
		'orderby' => array(
            'title' => esc_html__('Order', 'evatheme-pagebuilder'),
            'type' => 'select',
            'options' => $cstheme_element_options['orderby'],
            'default' => 'date_desc',
        ),
    ),
);