<?php
global $cstheme_elements;
$cstheme_elements['tab'] = array(
    'name' 		=> esc_html__('Tab', 'evatheme-pagebuilder'),
    'size' 		=> 'col-md-6',
    'content' 	=> 'items',
    'settings' 	=> array(
        'add_item' 	=> array(
            'title' 	=> '',
			'desc' 		=> '',
            'type' 		=> 'button',
            'data' 		=> array('item' => 'tab', 'settings' => 'items'),
            'default' 	=> esc_html__('Add Tab', 'evatheme-pagebuilder'),
        ),
        'position' 	=> array(
            'title' 	=> esc_html__('Choose Tab position', 'evatheme-pagebuilder'),
            'type' 		=> 'select',
            'options' 	=> array(
								'top' => esc_html__('Top', 'evatheme-pagebuilder'),
								'left' => esc_html__('Left', 'evatheme-pagebuilder'),
							),
            'default' 	=> 'top',
            'desc' 		=> esc_html__('Tab Titles can be displayed top, left position.', 'evatheme-pagebuilder'),
        ),
        'items' 	=> array(
            'title' 	=> esc_html__('Items', 'evatheme-pagebuilder'),
			'desc' 		=> '',
            'type' 		=> 'container',
            'container_type' => 'toggle',
            'title_as' 	=> 'title',
            'add_button' => 'add_item',
            'default' 	=> array(
                array(
                    'title' 	=> array(
                        'title' 	=> '',
                        'type' 		=> 'text',
                        'default' 	=> 'Tab Title',
                    ),
                    'content' 	=> array(
                        'title' 	=> '',
                        'type' 		=> 'textArea',
                        'tinyMCE' 	=> 'true',
                        'default' 	=> 'Tab Content',
                    ),
                )
            ),
        ),
    ),
);