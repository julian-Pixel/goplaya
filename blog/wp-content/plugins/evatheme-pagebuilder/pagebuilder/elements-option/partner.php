<?php
global $cstheme_elements, $cstheme_element_options;
$cstheme_elements["partner"] = array(
    "name" => "Partners",
    "size" => "col-md-12",
    "content" => "items",
    "settings" => array(
        "partners_per_line" => array(
            "title" => "Items per line",
            "type" => "text",
            "holder" => "6",
            "default" => "6",
            "desc" => "Input the number of Partner logos to be displayed in one line.",
        ),
		"add_item" => array(
            "title" => "",
            "type" => "button",
            "data" => array("item" => "partner", "settings" => "items"),
            "default" => "Add Partner",
        ),
        "items" => array(
            "title" => "Items",
            "type" => "container",
            "container_type" => "toggle",
            "title_as" => "title",
            "add_button" => "add_item",
            "default" => array(
                array(
                    "title" => array(
                        "type" => "text",
                        "default" => "",
                        "desc" => "Partner Title",
                    ),
                    "link" => array(
                        "title" => "Partner Link to URL",
                        "type" => "text",
                        "default" => "",
                        "desc" => "Partner Link to URL",
                    ),
                    "thumb" => array(
                        "title" => "Thumbnail Image",
                        "type" => "text",
                    ),
                    "add_thumb" => array(
                        "type" => "button",
                        "save_to" => "thumb",
                        "default" => "Upload",
                    ),
                )
            ),
        ),
    ),
);