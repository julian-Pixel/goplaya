<?php
global $cstheme_elements, $cstheme_element_options;
$cstheme_elements['button'] = array(
    'name' => esc_html__('Button', 'evatheme-pagebuilder'),
    'size' => 'col-md-4',
    'only' => 'shortcode', //builder
    'content' => 'text',
    'settings' => array(
        'text' => array(
            'title' => esc_html__('Button Text', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Insert your button text', 'evatheme-pagebuilder'),
            'type' => 'text',
            'default' => 'Button',
        ),
        'color' => array(
            'title' => esc_html__('Button Color', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Choose color', 'evatheme-pagebuilder'),
            'type' => 'color',
        ),
        'link' => array(
            'title' => esc_html__('Button Link', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Insert button link URL', 'evatheme-pagebuilder'),
            'type' => 'text',
        ),
        'target' => array(
            'title' => esc_html__('Button Target', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Blank will open new window, self will current window', 'evatheme-pagebuilder'),
            'type' => 'select',
            'options' => $cstheme_element_options['target'],
            'default' => '_blank',
        ),
    ),
);