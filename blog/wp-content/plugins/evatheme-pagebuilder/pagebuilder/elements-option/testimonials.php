<?php
global $cstheme_elements, $cstheme_element_options;
$cstheme_elements['testimonials'] = array(
    'name' => esc_html__('Testimonials', 'evatheme-pagebuilder'),
    'size' => 'col-md-4',
    'content' => 'items',
    'settings' => array(
        'add_item' => array(
            'title' => '',
            'type' => 'button',
            'data' => array('item' => 'testimonials', 'settings' => 'items'),
            'default' => 'Add Testimonial',
        ),
        'items' => array(
            'title' => esc_html__('Items', 'evatheme-pagebuilder'),
            'type' => 'container',
            'container_type' => 'toggle',
            'title_as' => 'name',
            'add_button' => 'add_item',
            'default' => array(
                array(
                    'name' => array(
                        'title' => esc_html__('Testimonial Author Name', 'evatheme-pagebuilder'),
                        'type' => 'text',
                    ),
                    'thumb' => array(
                        'title' => esc_html__('Author Photo', 'evatheme-pagebuilder'),
                        'type' => 'text',
                    ),
                    'add_thumb' => array(
                        'type' => 'button',
                        'save_to' => 'thumb',
                        'default' => 'Upload',
                    ),
                    'url_name' => array(
                        'title' => esc_html__('Name Link', 'evatheme-pagebuilder'),
                        'type' => 'text',
                    ),
					'url' => array(
                        'title' => esc_html__('Link URL', 'evatheme-pagebuilder'),
                        'type' => 'text',
                    ),
                    'content' => array(
                        'title' => esc_html__('Testimonial', 'evatheme-pagebuilder'),
                        'type' => 'textArea',
                    ),
                )
            ),
        ),
    ),
);
