<?php
global $cstheme_elements, $cstheme_element_options;
$cstheme_elements["sidebar"] = array(
    "name" => esc_html__('Sidebar','evatheme-pagebuilder'),
    "size" => "col-md-3",
    "only" => "builder",
    "settings" => array(
        "name" => array(
            "title" => "",
			"desc" => esc_html__('Please create your own Sidebar in Theme Options Sidebar Creater then Choose here.','evatheme-pagebuilder'),
            "type" => "select",
            "options" => $cstheme_element_options['sidebars'],
            "default" => "Default sidebar",
        ),
    ),
);