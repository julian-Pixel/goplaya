<?php
global $cstheme_elements;
$cstheme_elements['before_after'] = array(
    'name' => esc_html__('Before After','evatheme-pagebuilder'),
    'size' => 'col-md-4',
    'settings' => array(
        'before' => array(
            'title' => esc_html__('Before Image','evatheme-pagebuilder'),
            'desc' => esc_html('This element will helps you compare the two images between. Please insert the before image here.','evatheme-pagebuilder'),
			'type' => 'text',
            'holder' => '',
            'default' => '',
        ),
        'add_before' => array(
            'title' => '',
            'type' => 'button',
            'save_to' => 'before',
            'default' => esc_html__('Upload','evatheme-pagebuilder'),
            'desc' => '',
        ),
        'after' => array(
            'title' => esc_html__('After Image','evatheme-pagebuilder'),
			'desc' => esc_html__('Please insert the after image here.','evatheme-pagebuilder'),
            'type' => 'text',
            'holder' => '',
            'default' => '',
        ),
        'add_after' => array(
            'title' => '',
            'type' => 'button',
            'save_to' => 'after',
            'default' => esc_html__('Upload','evatheme-pagebuilder'),
            'desc' => '',
        ),
    ),
);