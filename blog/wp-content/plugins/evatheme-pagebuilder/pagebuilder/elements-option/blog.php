<?php
global $cstheme_elements,$cstheme_element_options;
$cstheme_elements['blog'] = array(
    'name' => esc_html__('Blog', 'evatheme-pagebuilder'),
    'size' => 'col-md-12',
    'min-size' => 'col-md-12',
    'only' => 'builder',
    'settings' => array(
        'posts_style' => array(
            'title' => esc_html__('Blog Posts Style', 'evatheme-pagebuilder'),
			'desc' => esc_html__('select a style to display blog posts', 'evatheme-pagebuilder'),
            'type' => 'select',
			'options' => array(
							'left_image' 		=> esc_html__('Left Image', 'evatheme-pagebuilder'),
							'bg_image' 			=> esc_html__('Background Image', 'evatheme-pagebuilder'),
							'card' 				=> esc_html__('Card Style', 'evatheme-pagebuilder'),
							'top_img' 			=> esc_html__('Top Image', 'evatheme-pagebuilder'),
							'big_top_img' 		=> esc_html__('Top Big Image', 'evatheme-pagebuilder'),
							'big_bg_images' 	=> esc_html__('Big Background Images', 'evatheme-pagebuilder'),
							'metro' 			=> esc_html__('Metro Style', 'evatheme-pagebuilder'),
							'card_clean' 		=> esc_html__('Card Clean Style', 'evatheme-pagebuilder'),
						),
            'default' => 'left_image',
			'hide' => array(
							'left_image' 		=> 'post_per_line,posts_top_img_style,post_listing',
							'bg_image' 			=> 'post_first_big,post_featured_img,post_excerpt_count,posts_top_img_style',
							'card' 				=> 'post_first_big,post_featured_img,post_listing,post_excerpt_count,posts_top_img_style',
							'top_img' 			=> 'post_first_big,post_listing',
							'big_top_img' 		=> 'post_first_big,post_featured_img,post_listing,post_per_line,posts_top_img_style',
							'big_bg_images' 	=> 'post_first_big,post_featured_img,post_listing,post_per_line,posts_top_img_style',
							'metro' 			=> 'post_first_big,post_featured_img,post_listing,posts_top_img_style,post_excerpt_count',
							'card_clean' 		=> 'post_first_big,post_featured_img,post_listing,post_per_line,posts_top_img_style,post_excerpt_count',
						),
        ),
		'posts_top_img_style' => array(
            'title' => esc_html__('Top Image substyles', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Select a style for the posts with top featured Image', 'evatheme-pagebuilder'),
            'type' => 'select',
			'options' => array(
							'style1' => esc_html__('Style 1 (grid)', 'evatheme-pagebuilder'),
							'style2' => esc_html__('Style 2 (masonry)', 'evatheme-pagebuilder'),
							'style3' => esc_html__('Style 3 (card with borders)', 'evatheme-pagebuilder'),
							'style4' => esc_html__('Style 4 (card with background)', 'evatheme-pagebuilder'),
						),
            'default' => 'style1',
        ),
		'post_first_big' => array(
            'title' => esc_html__('Big First Post', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Select this item to the first post was a big', 'evatheme-pagebuilder'),
            'type' => 'checkbox',
			'default' => 'false',
        ),
		'category' => array(
            'title' => esc_html__('Blog category', 'evatheme-pagebuilder'),
			'desc' => esc_html__('If you want to display Specify category then choose.', 'evatheme-pagebuilder'),
            'type' => 'category',
            'options' => $cstheme_element_options['cat']['post'],
            'default' => '0',
        ),
        'category_ids' => array(
            'type' => 'hidden',
            'selector' => 'category',
            'default' => '',
        ),
		'post_listing' => array(
            'title' => esc_html__('Post Listing', 'evatheme-pagebuilder'),
			'desc' => esc_html__('choose posts displaying style', 'evatheme-pagebuilder'),
            'type' => 'select',
			'options' => array(
							'grid' => esc_html__('Grid', 'evatheme-pagebuilder'),
							'masonry' => esc_html__('Masonry', 'evatheme-pagebuilder'),
							'carousel' => esc_html__('Carousel', 'evatheme-pagebuilder'),
						),
            'default' => 'grid',
			'hide' => array(
							'carousel' => 'pagination',
						),
        ),
        'post_count' => array(
            'title' => esc_html__('Post Count', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Insert how many posts will displayed in blog.', 'evatheme-pagebuilder'),
            'type' => 'text',
            'holder' => '6',
            'default' => '6',
        ),
		'post_per_line' => array(
            'title' => esc_html__('Post per Line', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Insert how many posts will displayed per line.', 'evatheme-pagebuilder'),
            'type' => 'select',
			'options' => array(
							'2' => esc_html__('2 Columns', 'evatheme-pagebuilder'),
							'3' => esc_html__('3 Columns', 'evatheme-pagebuilder'),
							'4' => esc_html__('4 Columns', 'evatheme-pagebuilder'),
							'5' => esc_html__('5 Columns', 'evatheme-pagebuilder'),
						),
            'default' => '3',
        ),
		'orderby' => array(
            'title' => esc_html__('Order', 'evatheme-pagebuilder'),
            'type' => 'select',
            'options' => $cstheme_element_options['orderby'],
            'default' => 'date_desc',
        ),
		'post_featured_img' => array(
            'title' => esc_html__('Featured Image', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Show/Hide Featured image for these posts', 'evatheme-pagebuilder'),
            'type' => 'select',
			'options' => array(
							'show' => esc_html__('Show', 'evatheme-pagebuilder'),
							'hide' => esc_html__('Hide', 'evatheme-pagebuilder'),
						),
            'default' => 'show',
        ),
		'post_excerpt_count' => array(
            'title' => esc_html__('Post Excerpt', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Enter the number of characters for a excerpt of the post', 'evatheme-pagebuilder'),
            'type' => 'text',
            'holder' => '6',
            'default' => '100',
        ),
		'pagination' => array(
            'title' => 'Pagination',
            'type' => 'select',
            'options' => array(
							'true' => esc_html__('Simple', 'evatheme-pagebuilder'),
							'infiniti' => esc_html__('Infiniti', 'evatheme-pagebuilder'),
							'false' => esc_html__('None', 'evatheme-pagebuilder'),
						),
            'default' => 'true',
			'hide' => array(
							'false' 		=> 'pagination_alignment',
						),
        ),
		'pagination_alignment' => array(
            'title' => 'Pagination alignment',
            'type' => 'select',
            'options' => array(
							'left' => esc_html__('left', 'evatheme-pagebuilder'),
							'center' => esc_html__('center', 'evatheme-pagebuilder'),
						),
            'default' => 'left',
        ),
    ),
);