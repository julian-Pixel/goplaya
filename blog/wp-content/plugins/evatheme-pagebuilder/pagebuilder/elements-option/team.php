<?php
global $cstheme_elements, $cstheme_element_options;
$cstheme_elements['team'] = array(
    'name' => esc_html__('Team', 'evatheme-pagebuilder'),
    'size' => 'col-md-4',
    'content' => 'content',
    'only' => 'builder',
    'settings' => array(
		'team_title' => array(
			'desc' => esc_html__('Insert Name', 'evatheme-pagebuilder'),
			'type' => 'text',
			'default' => '',
		),
		'position' => array(
			'desc' => esc_html__('Insert Position', 'evatheme-pagebuilder'),
			'type' => 'text',
			'default' => '',
		),
		'image' => array(
			'desc' => esc_html__('Upload Image', 'evatheme-pagebuilder'),
			'type' => 'text',
			'default' => '',
		),
		'add_thumb' => array(
			'title' => '',
			'type' => 'button',
			'save_to' => 'image',
			'default' => 'Upload',
		),
		'facebook' => array(
			'desc' => esc_html__('Facebook Link URL (optional)', 'evatheme-pagebuilder'),
			'type' => 'text',
			'default' => '',
		),
		'google' => array(
			'desc' => esc_html__('Google Plus Link URL (optional)', 'evatheme-pagebuilder'),
			'type' => 'text',
			'default' => '',
		),
		'twitter' => array(
			'desc' => esc_html__('Twitter Link URL (optional)', 'evatheme-pagebuilder'),
			'type' => 'text',
			'default' => '',
		),
		'pinterest' => array(
			'desc' => esc_html__('Pinterest Link URL (optional)', 'evatheme-pagebuilder'),
			'type' => 'text',
			'default' => '',
		),
		'youtube' => array(
			'desc' => esc_html__('Youtube Link URL (optional)', 'evatheme-pagebuilder'),
			'type' => 'text',
			'default' => '',
		),
		'instagram' => array(
			'desc' => esc_html__('Instagram Link URL (optional)', 'evatheme-pagebuilder'),
			'type' => 'text',
			'default' => '',
		),
		'flickr' => array(
			'desc' => esc_html__('Flickr Link URL (optional)', 'evatheme-pagebuilder'),
			'type' => 'text',
			'default' => '',
		),
		'dribbble' => array(
			'desc' => esc_html__('Dribbble Link URL (optional)', 'evatheme-pagebuilder'),
			'type' => 'text',
			'default' => '',
		),
    ),
);