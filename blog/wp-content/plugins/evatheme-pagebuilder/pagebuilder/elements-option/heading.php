<?php
global $cstheme_elements;
$cstheme_elements['heading'] = array(
    'name' => esc_html__('Heading', 'evatheme-pagebuilder'),
    'size' => 'col-md-12',
    'content' => 'description',
    'settings' => array(
        'text' => array(
            'title' => esc_html__('Title text', 'evatheme-pagebuilder'),
            'type' => 'text',
            'default' => 'Your Title',
        ),
        'weight' => array(
            'title' => esc_html__('Choose Font Weight', 'evatheme-pagebuilder'),
            'type' => 'select',
            'options' => array('100'=>'100','200'=>'200','300'=>'300','400'=>'400','500'=>'500','600'=>'600','700'=>'700','800'=>'800','900'=>'900'),
            'default' => '400',
        ),
        'style' => array(
            'title' => esc_html__('Choose Font Style', 'evatheme-pagebuilder'),
            'type' => 'select',
            'options' => array(
							'normal' => esc_html__('normal', 'evatheme-pagebuilder'),
							'italic' => esc_html__('italic', 'evatheme-pagebuilder'),
						),
            'default' => 'normal',
        ),
        'position' => array(
            'title' => esc_html__('Choose Title Position', 'evatheme-pagebuilder'),
            'type' => 'select',
            'options' => array(
							'left' => esc_html__('left', 'evatheme-pagebuilder'),
							'center' => esc_html__('center', 'evatheme-pagebuilder'),
							'right' => esc_html__('right', 'evatheme-pagebuilder'),
						),
            'default' => 'center',
        ),
        'description' => array(
            'title' => esc_html__('Title Description', 'evatheme-pagebuilder'),
            'type' => 'textArea',
            'default' => '',
        ),
    ),
);