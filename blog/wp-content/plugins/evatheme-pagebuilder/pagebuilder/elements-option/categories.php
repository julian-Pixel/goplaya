<?php
global $cstheme_elements,$cstheme_element_options;
$cstheme_elements['categories'] = array(
    'name' => esc_html__('Blog Categories', 'evatheme-pagebuilder'),
    'size' => 'col-md-12',
    'min-size' => 'col-md-12',
    'only' => 'builder',
    'settings' => array(
        'style' => array(
            'title' => esc_html__('categories Posts Style', 'evatheme-pagebuilder'),
			'desc' => esc_html__('select a style to display categories posts', 'evatheme-pagebuilder'),
            'type' => 'select',
			'options' => array(
							'grid' 		=> esc_html__('Grid', 'evatheme-pagebuilder'),
							'carousel' 	=> esc_html__('Carousel', 'evatheme-pagebuilder'),
						),
            'default' => 'grid',
        ),
        'count' => array(
            'title' => esc_html__('Post Count', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Insert how many posts will displayed in categories.', 'evatheme-pagebuilder'),
            'type' => 'text',
            'holder' => '6',
            'default' => '6',
        ),
    ),
);