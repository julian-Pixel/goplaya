<?php
global $cstheme_elements, $cstheme_element_options;
$cstheme_elements['pricing'] = array(
    'name' => esc_html__('Pricing Table', 'evatheme-pagebuilder'),
    'size' => 'col-md-12',
    'min-size' => 'col-md-12',
    'content' => 'columns',
    'settings' => array(
        'column_count' => array(
            'title' => esc_html__('Column count', 'evatheme-pagebuilder'),
			'desc' => '',
            'type' => 'select',
            'options' => array( '2' => '2', '3' => '3', '4' => '4' ),
            'default' => 'pricing-four',
        ),
        'symbol' => array(
			'title' => esc_html__('Symbol', 'evatheme-pagebuilder'),
			'type' => 'text',
			'default' => '$',
        ),
        'add_column' => array(
            'title' => '',
            'type' => 'button',
            'data' => array('item' => 'pricing', 'settings' => 'columns'),
            'default' => 'Add Column',
        ),                    
        'columns' => array(
            'title' => esc_html__('Columns', 'evatheme-pagebuilder'),
            'type' => 'container',
            'container_type' => 'toggle',
            'title_as' => 'title',
            'add_button' => 'add_column',
            'default' => array(
                array(
                    'title' => array(
                        'title' => esc_html__('Title', 'evatheme-pagebuilder'),
                        'type' => 'text',
                        'default' => 'Pricing Title',
                    ),
                    'sub_title' => array(
                        'title' => esc_html__('Sub Title', 'evatheme-pagebuilder'),
                        'type' => 'text',
                        'default' => 'Pricing table description',
                    ),
                    'featured' => array(
                        'title' => esc_html__('Featured ?', 'evatheme-pagebuilder'),
                        'type' => 'checkbox',
                        'default' => 'false',
                    ),
                    'price' => array(
                        'title' => esc_html__('Price ?', 'evatheme-pagebuilder'),
						'desc' => esc_html__('Please insert with $ symbol.', 'evatheme-pagebuilder'),
                        'type' => 'text',
                        'default' => '25',
                    ),
                    'time' => array(
                        'title' => esc_html__('Price Time ?', 'evatheme-pagebuilder'),
						'desc' => esc_html__('Price time option. Example: Month, Day, Year etc.', 'evatheme-pagebuilder'),
                        'type' => 'text',
                        'default' => 'month',
                    ),
					'price_feature1' => array(
                        'title' => esc_html__('Feature', 'evatheme-pagebuilder'),
                        'type' => 'text',
                        'default' => '100 Mb Disk space',
                    ),
					'price_feature2' => array(
                        'title' => esc_html__('Feature', 'evatheme-pagebuilder'),
                        'type' => 'text',
                        'default' => '50 Gb Data transfer',
                    ),
					'price_feature3' => array(
                        'title' => esc_html__('Feature', 'evatheme-pagebuilder'),
                        'type' => 'text',
                        'default' => 'Business Admin Panel',
                    ),
					'price_feature4' => array(
                        'title' => esc_html__('Feature', 'evatheme-pagebuilder'),
                        'type' => 'text',
                        'default' => 'Standard Support',
                    ),
                    'buttontext' => array(
                        'title' => esc_html__('Button Text', 'evatheme-pagebuilder'),
                        'type' => 'text',
                        'default' => 'Sign Up',
                        'holder' => 'Sign Up',
                    ),
                    'buttonlink' => array(
                        'title' => esc_html__('Button Link', 'evatheme-pagebuilder'),
                        'type' => 'text',
                        'default' => 'http://evatheme.com/',
                        'holder' => 'http://evatheme.com/',
                    ),
                )
            ),
        ),
    ),
);