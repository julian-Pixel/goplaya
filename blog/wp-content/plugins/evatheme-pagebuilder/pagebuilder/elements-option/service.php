<?php
global $cstheme_elements, $cstheme_element_options;
$cstheme_elements['service'] = array(
    'name' => esc_html__('Service', 'evatheme-pagebuilder'),
    'size' => 'col-md-4',
    'content' => 'content',
    'settings' => array(
        'service_style' => array(
            'title' => '',
            'type' => 'select',
            'options' => array(
							'style1' => esc_html__('Style 1 (Centered)', 'evatheme-pagebuilder'),
							'style2' => esc_html__('Style 2 (Icon Left)', 'evatheme-pagebuilder'),
						),
            'default' => 'style1',
        ),
        'thumb_type' => array(
            'title' => esc_html__('Choose Thumb type', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Chosen Post type will be displayed.', 'evatheme-pagebuilder'),
            'type' => 'select',
            'options' => array(
							'image' => esc_html__('Image', 'evatheme-pagebuilder'),
							'fi' => esc_html__('Font Icon', 'evatheme-pagebuilder'),
							'no_icon' => esc_html__('Without Icon', 'evatheme-pagebuilder'),
						),
            'hide' => array(
							'image' => 'fi',
							'fi' => 'service_thumb,add_thumb,service_thumb_height,service_thumb_width',
							'no_icon' => 'fi,service_thumb,add_thumb,service_thumb_height,service_thumb_width',
						),
            'default' => 'fi',
        ),
        'service_thumb_width' => array(
            'title' => esc_html__('Service Thumbnail Image Width', 'evatheme-pagebuilder'),
			'desc' => esc_html__('The preivew image width.', 'evatheme-pagebuilder'),
            'type' => 'text',
            'holder' => '',
            'default' => '50',
        ),
        'service_thumb' => array(
            'title' => esc_html__('Service Thumbnail Image', 'evatheme-pagebuilder'),
			'desc' => esc_html__('The preivew image.', 'evatheme-pagebuilder'),
            'type' => 'text',
            'holder' => '',
            'default' => '',
        ),
        'add_thumb' => array(
            'title' => '',
            'type' => 'button',
            'save_to' => 'service_thumb',
            'default' => 'Upload',
            'desc' => '',
        ),
		
        // Font Icon
        'fi' => array(
            'title' => esc_html__('Add Icon', 'evatheme-pagebuilder'),
            'type' => 'fi',
            'holder' => '',
            'default' => '',
            'desc' => '',
        ),
        'fi_size' => array(
            'need_fi' => 'true',
            'title' => esc_html__('Size', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Size.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '28',
        ),
        'fi_padding' => array(
            'title' => esc_html__('Padding', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Padding.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '25',
        ),
        'fi_color' => array(
            'title' => esc_html__('Color', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Color.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '#fff',
        ),
        'fi_bg_color' => array(
            'title' => esc_html__('Background Color', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Background Color.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '#7b52ab',
        ),
        'fi_border_color' => array(
            'title' => esc_html__('Border Color', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Border Color.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '',
        ),
        'fi_rounded' => array(
            'title' => esc_html__('Border Size', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Border Size.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '0',
        ),
        'fi_rounded_size' => array(
            'title' => esc_html__('Border Rounded Size', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Border Rounded Size.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '50',
        ),
        'fi_icon' => array(
            'title' => esc_html__('Icon', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Icon.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => 'fa-cloud',
        ),
		
        // -----------------------
        'service_title' => array(
            'title' => '',
            'type' => 'text',
            'holder' => 'Your Service Title',
            'default' => 'Your Service Title',
        ),
        'content' => array(
            'title' => '',
            'type' => 'textArea',
            'holder' => 'Content',
            'default' => 'Content',
        ),
    ),
);