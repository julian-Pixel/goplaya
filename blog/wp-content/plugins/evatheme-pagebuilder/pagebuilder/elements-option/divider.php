<?php

global $cstheme_elements;
$cstheme_elements['divider'] = array(
    'name' 		=> esc_html__('Divider', 'evatheme-pagebuilder'),
    'size' 		=> 'col-md-12',
    'settings' 	=> array(
        'type' 		=> array(
            'title' 	=> esc_html__('Choose Divider Type', 'evatheme-pagebuilder'),
            'type' 		=> 'select',
            'options' 	=> array(
								'line' => esc_html__('Line', 'evatheme-pagebuilder'),
								'space' => esc_html__('Space', 'evatheme-pagebuilder'),
							),
			'hide' => array(
							'space' => 'color',
						),
            'default' 	=> 'line',
        ),
		'color' 	=> array(
            'title' 	=> esc_html__('Background Color', 'evatheme-pagebuilder'),
            'type' 		=> 'color',
            'default' 	=> '#25262c',
        ),
        'height' 	=> array(
            'title' 	=> esc_html__('Insert Height of Divider', 'evatheme-pagebuilder'),
            'type' 		=> 'text',
            'default' 	=> '1px',
        ),
    ),
);
