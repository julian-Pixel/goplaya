<?php
global $cstheme_elements, $cstheme_element_options;
$cstheme_elements['counters'] = array(
    'name' => esc_html__('Counters', 'evatheme-pagebuilder'),
    'size' => 'col-md-3',
	'content' => 'items',
    'settings' => array(
        'counter_type' => array(
            'title' => esc_html__('Choose Counter type', 'evatheme-pagebuilder'),
			'desc' => esc_html__('FL means Icons Floated Left.', 'evatheme-pagebuilder'),
            'type' => 'select',
            'options' => array(
							'style1' => esc_html__('Icon + Title + Counter', 'evatheme-pagebuilder'),
							'style2' => esc_html__('Icon(FL) + Counter + Title', 'evatheme-pagebuilder'),
						),
            'default' => 'style_2',
        ),
        'thumb_type' => array(
            'title' => esc_html__('Choose Thumb type', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Chosen type will be displayed.', 'evatheme-pagebuilder'),
            'type' => 'select',
            'options' => array(
							'image' => esc_html__('Image', 'evatheme-pagebuilder'),
							'fi' => esc_html__('Font Icon', 'evatheme-pagebuilder'),
							'empty' => esc_html__('Without Icon', 'evatheme-pagebuilder'),
						),
            'hide' => array('image' => 'fi', 'fi' => 'image,add_image', 'empty' => 'fi,image,add_image'),
            'default' => 'fi',
        ),
		
        // Image
        'image' => array(
            'title' => esc_html__('Thumbnail Image', 'evatheme-pagebuilder'),
			'desc' => esc_html__('The preivew image.', 'evatheme-pagebuilder'),
            'type' => 'text',
            'holder' => '',
            'default' => '',
        ),
        'add_image' => array(
            'title' => '',
			'desc' => '',
            'type' => 'button',
            'save_to' => 'image',
            'default' => 'Upload',
        ),
		
        // Font Icon
        'fi' => array(
            'title' => esc_html__('Add Icon', 'evatheme-pagebuilder'),
			'desc' => '',
            'type' => 'fi',
            'holder' => '',
            'default' => '',
        ),
        'fi_size' => array(
            'need_fi' => 'true',
            'title' => esc_html__('Size', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Size.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '25',
        ),
        'fi_padding' => array(
            'title' => esc_html__('Padding', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Padding.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '20',
        ),
        'fi_color' => array(
            'title' => esc_html__('Color', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Color.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '#7b52ab',
        ),
        'fi_bg_color' => array(
            'title' => esc_html__('Background Color', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Background Color.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '',
        ),
        'fi_border_color' => array(
            'title' => esc_html__('Border Color', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Border Color.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '#7b52ab',
        ),
        'fi_rounded' => array(
            'title' => esc_html__('Border Size', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Border Size.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '2',
        ),
        'fi_rounded_size' => array(
            'title' => esc_html__('Border Rounded Size', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Border Rounded Size.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => '3',
        ),
        'fi_icon' => array(
            'title' => esc_html__('Icon', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Icon.', 'evatheme-pagebuilder'),
            'type' => 'hidden',
            'holder' => '',
            'default' => 'icon-fire',
        ),
        // -----------------------
        'counter_title' => array(
            'title' => '',
			'desc' => esc_html__('Insert your Counter', 'evatheme-pagebuilder'),
            'type' => 'text',
            'default' => 'Our Customers',
        ),
        'count' => array(
            'title' => '',
			'desc' => esc_html__('Count Number. Note: Only Digit', 'evatheme-pagebuilder'),
            'type' => 'text',
            'default' => '1500',
        ),
    ),
);
