<?php
global $cstheme_elements, $cstheme_element_options;
$cstheme_elements['accordion'] = array(
    'name' => esc_html__('Accordion', 'evatheme-pagebuilder'),
    'size' => 'col-md-4',
    'content' => 'items',
    'settings' => array(
        'add_item' => array(
            'title' => '',
			'desc' => '',
            'type' => 'button',
            'data' => array('item' => 'accordion', 'settings' => 'items'),
            'default' => esc_html__('Add Accordion', 'evatheme-pagebuilder'),
        ),
        'items' => array(
            'title' => esc_html__('Items', 'evatheme-pagebuilder'),
            'type' => 'container',
            'container_type' => 'toggle',
            'title_as' => 'title',
            'add_button' => 'add_item',
            'default' => array(
                array(
                    'title' => array(
                        'title' => esc_html__('Accordion Title', 'evatheme-pagebuilder'),
                        'type' => 'text',
                        'default' => 'Who are we?',
                    ),
                    'expand' => array(
                        'title' => esc_html__('Expand?', 'evatheme-pagebuilder'),
                        'type' => 'checkbox',
                        'default' => 'false',
                    ),
                    'content' => array(
                        'type' => 'textArea',
                        'tinyMCE' => 'true',
                        'default' => 'Accordion Content',
                    ),
                )
            ),
        ),
    ),
);