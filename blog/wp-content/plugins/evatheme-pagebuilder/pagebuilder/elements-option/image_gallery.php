<?php
global $cstheme_elements, $cstheme_element_options;
$cstheme_elements['image_slider'] = array(
    'name'    => esc_html__('Image Gallery', 'evatheme-pagebuilder'),
    'size'    => 'col-md-4',
    'settings' => array(
        'type' => array(
            'title' => '',
            'type' => 'select',
            'options' => array(
							'col2' => esc_html__('2 Columns', 'evatheme-pagebuilder'),
							'col3' => esc_html__('3 Columns', 'evatheme-pagebuilder'),
							'col4' => esc_html__('4 Columns', 'evatheme-pagebuilder'),
							'col5' => esc_html__('5 Columns', 'evatheme-pagebuilder'),
							'col6' => esc_html__('6 Columns', 'evatheme-pagebuilder'),
						),
            'default' => 'col3',
			'hide' => array(
							'slide' => 'padding',
						),
        ),
        'padding' => array(
            'title' => '',
			'desc' => esc_html__('Padding for your Images', 'evatheme-pagebuilder'),
            'type' => 'text',
            'default' => '0px',
        ),
        'add_gallery' => array(
            'title' => '',
            'type' => 'button',
            'save_to' => 'gallery_list',
            'default' => esc_html__('Edit Gallery', 'evatheme-pagebuilder'),
        ),
        'gallery_list' => array(
            'title' => '',
            'type' => 'gallery',
            'holder' => '',
            'default' => '',
        ),
    ),
);