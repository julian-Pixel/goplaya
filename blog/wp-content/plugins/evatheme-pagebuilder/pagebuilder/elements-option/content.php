<?php
global $cstheme_elements;
$cstheme_elements["content"] = array(
    "name" => "Content",
    "size" => "col-md-12",
    "only" => "builder",
    "settings" => array(
        "description" => array(
            "title" => "This element displays content of your Core Editor. If you want to use additional Editor Please use the Column Element on our Page builder",
            "type" => "button",
            "data" => array(),
            "default" => "",
            "desc" => "",
        ),
    ),
);