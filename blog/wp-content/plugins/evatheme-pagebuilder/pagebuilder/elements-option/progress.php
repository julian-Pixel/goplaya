<?php
global $cstheme_elements, $cstheme_element_options;
$cstheme_elements['progress'] = array(
    'name' => esc_html__('Progress Bar', 'evatheme-pagebuilder'),
    'size' => 'col-md-4',
    'content' => 'items',
    'settings' => array(
        'add_item' => array(
            'title' => '',
            'type' => 'button',
            'data' => array('item' => 'progress', 'settings' => 'items'),
            'default' => 'Add Progress Bar',
        ),
        'items' => array(
            'title' => esc_html__('Items', 'evatheme-pagebuilder'),
			'desc' => esc_html__('Items', 'evatheme-pagebuilder'),
            'type' => 'container',
            'container_type' => 'toggle',
            'title_as' => 'title',
            'add_button' => 'add_item',
            'default' => array(
                array(
                    'title' => array(
                        'title' => '',
						'desc' => '',
                        'type' => 'text',
                        'holder' => 'Progress title',
                        'default' => 'Progress title',
                    ),
                    'percent' => array(
                        'title' => esc_html__('Progress Percent', 'evatheme-pagebuilder'),
						'desc' => esc_html__('integer value in 0-100.', 'evatheme-pagebuilder'),
                        'type' => 'text',
                        'default' => '50',
                    ),
                    'type' => array(
                        'title' => esc_html__('Choose Type', 'evatheme-pagebuilder'),
						'desc' => esc_html__('Choose Type', 'evatheme-pagebuilder'),
                        'type' => 'select',
                        'options' => array(
										'light' => esc_html__('Light', 'evatheme-pagebuilder'),
										'massive' => esc_html__('Massive', 'evatheme-pagebuilder'),
									),
                    ),
                    'color' => array(
                        'title' => esc_html__('Color of Progress', 'evatheme-pagebuilder'),
                        'type' => 'color',
                        'holder' => 'Color of Progress',
                        'default' => '#d5552b',
                    ),
                )
            ),
        ),
    ),
);