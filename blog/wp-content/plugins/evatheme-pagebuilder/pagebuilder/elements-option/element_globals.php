<?php
global $cstheme_element_options, $cstheme_global_options;

$cstheme_element_options['orderby'] = array(
    'date_desc' 	=> esc_html__('Date Desc', 'evatheme-pagebuilder'),
    'date_asc' 		=> esc_html__('Date Asc', 'evatheme-pagebuilder'),
    'title_desc' 	=> esc_html__('Title Desc', 'evatheme-pagebuilder'),
    'title_asc' 	=> esc_html__('Title Asc', 'evatheme-pagebuilder'),
    'random' 		=> esc_html__('Random', 'evatheme-pagebuilder'),
    'comment_count' => esc_html__('Comment Count', 'evatheme-pagebuilder'),
);

$cstheme_element_options['heading_alignment'] = array(
    'left' 		=> esc_html__('left', 'evatheme-pagebuilder'),
    'center' 	=> esc_html__('center', 'evatheme-pagebuilder'),
    'right' 	=> esc_html__('right', 'evatheme-pagebuilder'),
);

$cstheme_element_options['heading_size'] = array(
    'h1' => 'h1',
    'h2' => 'h2',
    'h3' => 'h3',
    'h4' => 'h4',
    'h5' => 'h5',
    'h6' => 'h6'
);

$cstheme_element_options['target'] = array(
	'_blank' 	=> esc_html__('Blank', 'evatheme-pagebuilder'),
	'_self' 	=> esc_html__('Self', 'evatheme-pagebuilder'),
);

$cstheme_element_options['yesno'] = array(
	'true' => 'Yes',
	'false' => 'No',
);

$cstheme_element_options['bgposition'] = array(
    'left top' 			=> esc_html__('left top', 'evatheme-pagebuilder'),
    'left center' 		=> esc_html__('left center', 'evatheme-pagebuilder'),
    'left bottom' 		=> esc_html__('left bottom', 'evatheme-pagebuilder'),
    'right top' 		=> esc_html__('right top', 'evatheme-pagebuilder'),
    'right center' 		=> esc_html__('right center', 'evatheme-pagebuilder'),
    'right bottom' 		=> esc_html__('right bottom', 'evatheme-pagebuilder'),
    'center top' 		=> esc_html__('center top', 'evatheme-pagebuilder'),
    'center center' 	=> esc_html__('center center', 'evatheme-pagebuilder'),
    'center bottom' 	=> esc_html__('center bottom', 'evatheme-pagebuilder'),
);


$arraySidebar = array("blog-sidebar" => "Default sidebar");
$sidebars = newsider_options('unlimited_sidebar');
if (!empty($sidebars)) {
    foreach ($sidebars as $sidebar) {
        $arraySidebar[$sidebar] = $sidebar;
    }
}
$cstheme_element_options['sidebars'] = $arraySidebar;


$ignoredPostType=array('page','attachment','revision','nav_menu_item');
$arrayPostType = array();
$arrayPostTypeHide = array();
$postTypeList = get_post_types(array(),'objects');
foreach($postTypeList as $slug => $typeOptions){
    if(!in_array($slug, $ignoredPostType)){
        $arrayPostType[$slug] = $typeOptions->labels->menu_name;
        //---------hide----------
        $tmpArr=array();
        foreach($postTypeList as $slugSub => $typeOptionsSub){
            if(!in_array($slugSub, $ignoredPostType)&&$slug!==$slugSub){$tmpArr[]='cat_'.$slugSub;}
        }
        $arrayPostTypeHide[$slug]=implode(",", $tmpArr);
    }
}

$categories=array();
foreach($arrayPostType as $slug => $name){
    $categories[$slug]=array();
    $taxonomyNames = get_object_taxonomies($slug);
    if(isset($taxonomyNames[0])){
        $taxonomyCategories = get_terms($taxonomyNames[0], 'hide_empty=0');
        $categories[$slug]["0"] = "Select Category";
        if(!empty($taxonomyCategories)) {
            foreach ($taxonomyCategories as $taxonomyCategorie) {
                $categories[$slug][$taxonomyCategorie->slug] = $taxonomyCategorie->name;
            }
        }
    }
}
$cstheme_element_options['cat'] = $categories;



//	All Type Settings
$cstheme_global_options['element'] = array(
    'item_title' 	=> array(
        'title' 	=> esc_html__('Element Title', 'evatheme-pagebuilder'),
        'type' 		=> 'text',
    ),
	'item_title_aligment' => array(
        'title' 	=> esc_html__('Alignment', 'evatheme-pagebuilder'),
        'type' 		=> 'select',
        'options' 	=> $cstheme_element_options['heading_alignment'],
        'default' 	=> 'left',
    ),
	'item_title_size' => array(
        'title' 	=> esc_html__('Size', 'evatheme-pagebuilder'),
        'type' 		=> 'select',
        'options' 	=> $cstheme_element_options['heading_size'],
        'default' 	=> 'h5',
    ),
);

$cstheme_global_options['element_settings_bottom'] = array(
    'custom_class' 	=> array(
        'title' 	=> esc_html__('Custom Class', 'evatheme-pagebuilder'),
		'desc'		=> esc_html__('You can use the custom CSS class for this module.', 'evatheme-pagebuilder'),
        'type' 		=> 'text',
    ),
    'element_margin_bottom' => array(
        'title' 	=> esc_html__('Margin bottom', 'evatheme-pagebuilder'),
		'desc' 		=> '',
        'type' 		=> 'text',
        'holder' 	=> '',
		'default' 	=> '60px',
    ),
);

$cstheme_global_options['row'] = array(
    'row_custom_class' => array(
        'title' 	=> esc_html__('Custom Class', 'evatheme-pagebuilder'),
		'desc' 		=> '',
        'type' 		=> 'text',
        'holder' 	=> '',
        'default' 	=> '',
    ),
    'row_custom_id' => array(
        'title' 	=> esc_html__('Custom ID', 'evatheme-pagebuilder'),
		'desc' 		=> esc_html__('ID will help you build One Page, Example: if you inserted about here then use the Menu section to yourwebsite.com/#about etc', 'evatheme-pagebuilder'),
        'type' 		=> 'text',
        'holder' 	=> '',
        'default' 	=> '',
    ),
	'container_size' => array(
        'title' 	=> esc_html__('Container Size', 'evatheme-pagebuilder'),
		'desc' 		=> esc_html__('Select size options for container', 'evatheme-pagebuilder'),
        'type' 		=> 'select',
        'options' 	=> array(
							'boxed' => esc_html__('Boxed Content', 'evatheme-pagebuilder'),
							'full_width' => esc_html__('Full Width Content', 'evatheme-pagebuilder'),
							'full_width_content_boxed' => esc_html__('Full Width BG Content Boxed', 'evatheme-pagebuilder'),
						),
        'default' 	=> '',
    ),
    'background_color' => array(
        'title' 	=> esc_html__('Background color', 'evatheme-pagebuilder'),
		'desc' 		=> esc_html__('Choose background color.', 'evatheme-pagebuilder'),
        'type' 		=> 'color',
        'holder' 	=> '',
        'default' 	=> '',
    ),
    'background_image' => array(
        'title' 	=> esc_html__('Background image', 'evatheme-pagebuilder'),
		'desc' 		=> esc_html__('Upload background image.', 'evatheme-pagebuilder'),
        'type' 		=> 'text',
        'holder' 	=> '',
        'default' 	=> '',
    ),
    'add_background_image' => array(
        'title' 	=> '',
		'desc' 		=> '',
        'type' 		=> 'button',
        'save_to' 	=> 'background_image',
        'default' 	=> 'Upload',
    ),
    'background_style' => array(
        'title' 	=> esc_html__('Background image style', 'evatheme-pagebuilder'),
		'desc' 		=> '',
        'type' 		=> 'select',
        'options' 	=> array(
							'scroll' => esc_html__('Scroll', 'evatheme-pagebuilder'),
							'fixed' => esc_html__('Fixed', 'evatheme-pagebuilder'),
							'parallax' => esc_html__('Parallax', 'evatheme-pagebuilder'),
							'pattern' => esc_html__('Pattern', 'evatheme-pagebuilder'),
						),
        'default' 	=> '',
    ),
    'default_row' => array(
        'title' 	=> '',
		'desc' 		=> '',
        'type' 		=> 'hidden',
        'holder' 	=> '',
        'default' 	=> 'true',
    ),
    'row_layout' => array(
        'title' 	=> '',
		'desc' 		=> '',
        'type' 		=> 'hidden',
        'holder' 	=> '',
        'default' 	=> 'full',
    ),
    'padding_top' => array(
        'title' 	=> esc_html__('Container Padding Top', 'evatheme-pagebuilder'),
		'desc' 		=> esc_html__('Controlling Container top margin value. Example 60px', 'evatheme-pagebuilder'),
        'type' 		=> 'text',
        'holder' 	=> '',
        'default' 	=> '',
    ),
	'padding_right' => array(
        'title' 	=> esc_html__('Container Padding Right', 'evatheme-pagebuilder'),
		'desc' 		=> esc_html__('Controlling Container right margin value. Example 60px', 'evatheme-pagebuilder'),
        'type' 		=> 'text',
        'holder' 	=> '',
        'default' 	=> '',
    ),
    'padding_bottom' => array(
        'title' 	=> esc_html__('Container Padding Bottom', 'evatheme-pagebuilder'),
		'desc' 		=> esc_html__('Controlling Container bottom margin value. Example 60px', 'evatheme-pagebuilder'),
        'type' 		=> 'text',
        'holder' 	=> '',
        'default' 	=> '',
    ),
	'padding_left' => array(
        'title' 	=> esc_html__('Container Padding Left', 'evatheme-pagebuilder'),
		'desc' 		=> esc_html__('Controlling Container left margin value. Example 60px', 'evatheme-pagebuilder'),
        'type' 		=> 'text',
        'holder' 	=> '',
        'default' 	=> '',
    ),
);