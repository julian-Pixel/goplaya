<?php
global $cstheme_elements;
$cstheme_elements["fonticon"] = array(
    "name" => "Font Icon",
    "only" => "shortcode",
    "size" => "col-md-4",
    "settings" => array(
        "fi" => array(
            "title" => "Add Icon",
            "type" => "fi",
            "holder" => "",
            "default" => "",
            "desc" => "",
        ),
        "fi_size" => array(
            "need_fi" => "true",
            "title" => "Size",
            "type" => "hidden",
            "holder" => "",
            "default" => "28",
            "desc" => "Size.",
        ),
        "fi_padding" => array(
            "title" => "Padding",
            "type" => "hidden",
            "holder" => "",
            "default" => "25",
            "desc" => "Padding.",
        ),
        "fi_color" => array(
            "title" => "Color",
            "type" => "hidden",
            "holder" => "",
            "default" => "#fff",
            "desc" => "Color.",
        ),
        "fi_bg_color" => array(
            "title" => "Background Color",
            "type" => "hidden",
            "holder" => "",
            "default" => "#7b52ab",
            "desc" => "Background Color.",
        ),
        "fi_border_color" => array(
            "title" => "Border Color",
            "type" => "hidden",
            "holder" => "",
            "default" => "",
            "desc" => "Border Color.",
        ),
        "fi_rounded" => array(
            "title" => "Border Size",
            "type" => "hidden",
            "holder" => "",
            "default" => "0",
            "desc" => "Border Size.",
        ),
        "fi_rounded_size" => array(
            "title" => "Border Rounded Size",
            "type" => "hidden",
            "holder" => "",
            "default" => "50",
            "desc" => "Border Rounded Size.",
        ),
        "fi_icon" => array(
            "title" => "Icon",
            "type" => "hidden",
            "holder" => "",
            "default" => "fa-cloud",
            "desc" => "Icon.",
        ),
    ),
);