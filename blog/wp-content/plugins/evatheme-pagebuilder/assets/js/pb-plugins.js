/**
 * CStheme Page Builder scripts
 */


window.jQuery = window.$ = jQuery;


function evapb_fullwidth() {
	"use strict";
	
	if( ( jQuery('.container .row-container.full_width').size() > 0 ) || ( jQuery('.container .row-container.full_width_content_boxed').size() > 0 ) ) {
		var default_pageW = jQuery('#page-wrap').width(),
			row_containerW = jQuery('.container').width(),
			marginLeft = ( default_pageW - row_containerW ) / 2;
		
		jQuery('.container .row-container.full_width, .container .row-container.full_width_content_boxed').css({
			'margin-left': '-' + marginLeft + 'px',
			'width': default_pageW + 'px'
		});
		
		evapb_post_card_clean_Height();
	}
}


function evapb_post_card_clean_Height() {
	"use strict";
	
	if( jQuery('.evapb-bloglist.card_clean .post').size() > 0 ) {
		if ( jQuery('.row-container').hasClass('boxed') ) {
			var postW = jQuery('.evapb-bloglist.card_clean').width() / 3;
			
			jQuery('.evapb-bloglist.card_clean .post .post_content_wrapper').css({
				'height': postW - 20 + 'px'
			});
		} else {
			var postW = jQuery('.evapb-bloglist.card_clean').width() / 4;
			
			jQuery('.evapb-bloglist.card_clean .post .post_content_wrapper').css({
				'height': postW - 20 + 'px'
			});
		}
	}
}


//	Blog Post Format Twitter
function evapb_pf_twitter_owlcarousel() {
	"use strict";
	
	if (jQuery('.post_format_twitter').size() > 0) {
		jQuery(".post_format_twitter").owlCarousel({
			margin: 0,
			nav: false,
			dots: true,
			navText: [
				"",
				"",
			],
			loop: true,
			autoplay: true,
			autoplaySpeed: 1000,
			autoplayTimeout: 3000,
			navSpeed: 1000,
			autoplayHoverPause: true,
			items: 1,
			thumbs: false,
			animateIn: "fadeIn",
			animateOut: "fadeOut",
		});
	}
}


//	Blog Posts Metro Style size
function evapb_BlogMetroHeight() {
	if (jQuery('.evapb-bloglist.metro').size() > 0) {
		jQuery('.evapb-bloglist.metro article.post').each(function(){
			var postW = jQuery(this).find('.post_content_wrapper').width();
			if (jQuery(this).hasClass('sizing_width2')) {
				jQuery(this).find('.post_content_wrapper').css('height', Math.round( postW / 2 ) + 'px');
			} else if (jQuery(this).hasClass('sizing_height2')) {
				jQuery(this).find('.post_content_wrapper').css('height', Math.round( postW * 2 ) + 'px');
			} else {
				jQuery(this).find('.post_content_wrapper').css('height', Math.round( postW ) + 'px');
			}
		});
	}
}

jQuery(document).ready(function() {
	"use strict";
	
	evapb_fullwidth();
	
	evapb_BlogMetroHeight();
	
	evapb_post_card_clean_Height();
	
	//	Progress Bar
	if(jQuery(window).width() > 768) {
		jQuery('.cstheme-progress .bar-container').waypoint(function() {
			var newwidth = jQuery(this).data('width') + '%';
			jQuery(this).find('.bar').animate({width: newwidth}, {
				duration:1500,
				step: function(now) {
					jQuery(this).find('span').show().html(Math.floor(now) + '%');
				}
			});
		}, {
			triggerOnce: true,
			offset: '70%'
		});
	} else {
		jQuery('.cstheme-progress .bar-container').each(function(){
            var newwidth = jQuery(this).data('width') + '%';
			jQuery(this).find('.bar').animate({width: newwidth}, {
				duration:1500
			});

        });
    }
	
	
	//	Counters
	jQuery('.cstheme-counter .stat_count').waypoint(function() {
		var counter = jQuery(this).attr('data-count');
		jQuery(this).countTo({
			from: 0,
			to: counter,
			speed: 5500,
			refreshInterval: 20,
		});
	}, {
		triggerOnce: true,
		offset: '200%'
	});
	
	
	//	Post Carousel List
	if( jQuery('.evapb_blog_carousel.small_carousel .posts_carousel_list').size() > 0 ) {
		jQuery('.posts_carousel_list').each(function(){
			var $carouselPostsCount = jQuery(this).find('article.post');
			if($carouselPostsCount.length < 6){
				jQuery(this).addClass('lessthan5');
			}
		});
	}
	
	//	sidebar add wrap class
	if( jQuery('#sidebar').size() > 0 ) {
		jQuery('#sidebar').each(function(){
			jQuery(this).parent().parent().parent('.col-md-4').addClass('sidebar_wrap');
			jQuery(this).parent('.row-container').find('.sidebar_wrap').prev('.col-md-8').addClass('content_sidebar');
		});
	}
	
	
	//	Blog Posts listing on hover
	if( jQuery('.evapb_blog_carousel.listing_hover').size() > 0 ) {
		jQuery('.evapb_blog_carousel.listing_hover .posts_carousel_list').each(function(){
			var $cFeatPost=$(this);
			var $cFeatPostItems=$cFeatPost.children('article.post');
			var $auto=true;
			var $time=0;
			var $timeInt=1000;
			var $timeMax=3000;
			$cFeatPost.find('.post').first().addClass('active');
			$cFeatPostItems.each(function(){
				var $cFeatPostItem=$(this);
				$cFeatPostItem.hover(function(){
					$cFeatPostItem.addClass('active').siblings('article.post').removeClass('active');
					$auto=false;
				},function(){
					$time=0;
					$auto=true;
				});
			});
			if($cFeatPostItems.length>1){
				setInterval(function(){
				   if($auto&&$time>$timeMax){
					   $time=0;
					   var $activeItem=$cFeatPost.children('article.post.active');
					   var $nextItem=$activeItem.next('article').hasClass('post')?$activeItem.next('article.post'):$cFeatPostItems.eq(0);
					   $nextItem.addClass('active');
					   $activeItem.removeClass('active');
				   }else{
					   $time+=$timeInt;
				   }
				},$timeInt);
			}
		});
	}
	
	if( jQuery('.video_player').size() > 0 ) {
		jQuery('.video_player .pf_video_play').on('click', function(){
			jQuery(this).parent('.video_player').addClass('show_video');
			jQuery(this).parent('.video_player').find('iframe')[0].src += "&autoplay=1";
		});
	}
	
	evapb_pf_twitter_owlcarousel();
	
});


jQuery(window).load(function() {
    "use strict";
	
	//	Top Slider preloader
	setTimeout(function () {
		jQuery('.top_slider_preloader').fadeOut();
	}, 1000);
	
	evapb_BlogMetroHeight();
	
    jQuery('.cstheme-gallery').each(function(i){
        var $currentIsotopeContainer = $('.cstheme-gallery');
		
		// Isotop
        $currentIsotopeContainer.isotope({
            itemSelector: '.gallery_item',
			masonry: {columnWidth: 1}
        });
		
        // Resize
        jQuery(window).resize(function(){
            $currentIsotopeContainer.isotope('reLayout');
        });
	});
	
	//	Twitter Widget
	setTimeout(function () {
		jQuery(".evapb_widget_last_tweets .carousel").owlCarousel({
			margin: 0,
			nav: true,
			dots: false,
			navText: [
				"",
				"",
			],
			loop: true,
			autoplay: true,
			autoplaySpeed: 1000,
			autoplayTimeout: 3000,
			navSpeed: 1000,
			autoplayHoverPause: true,
			items: 1,
			thumbs: false
		});
	}, 300);
	
	
	//	Top Slider style 1
	if( jQuery('.evapb_top_slider .style1').size() > 0 ) {

		jQuery("#top_slider_thumb").flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: true,
			slideshowSpeed: 2000,
			animationSpeed: 800,
			pauseOnHover: false,
			prevText: "<i class='fa fa-angle-left'></i>",
			nextText: "<i class='fa fa-angle-right'></i>",
			itemWidth: 292,
			itemMargin: 0,
			asNavFor: ".top_slider.flexslider"
		});
		 
		jQuery(".top_slider.flexslider").flexslider({
			animation: "fade",
			controlNav: false,
			directionNav: false,
			animationLoop: false,
			slideshow: true,
			prevText: "<span></span>",
			nextText: "<span></span>",
			sync: "#top_slider_thumb",
		});
	
	}
	
	//	Top Slider style 2
	if( jQuery('.evapb_top_slider .style2').size() > 0 ) {

		jQuery("#top_slider_thumb").flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			direction: 'vertical',
			pauseOnHover: false,
			directionNav: false,
			prevText: "<i class='fa fa-angle-left'></i>",
			nextText: "<i class='fa fa-angle-right'></i>",
			itemWidth: 160,
			itemMargin: 0,
			asNavFor: ".top_slider.flexslider"
		});
	
		jQuery(".top_slider.flexslider").flexslider({
			animation: "fade",
			controlNav: false,
			directionNav: true,
			animationLoop: false,
			slideshow: false,
			prevText: "<i class='fa fa-angle-left'></i>",
			nextText: "<i class='fa fa-angle-right'></i>",
			sync: "#top_slider_thumb",
		});
	
	}
	
	//	Top Slider style 4
	if( jQuery('.evapb_top_slider .style4').size() > 0 ) {
	
		jQuery(".top_slider.flexslider").flexslider({
			animation: "fade",
			controlNav: true,
			directionNav: false,
			animationLoop: true,
			slideshow: true,
			slideshowSpeed: 3000,
			animationSpeed: 800,
			prevText: "<i class='fa fa-angle-left'></i>",
			nextText: "<i class='fa fa-angle-right'></i>",
		});
	
	}
	
	//	Top Slider style 5
	if( jQuery('.evapb_top_slider .style5').size() > 0 ) {
	
		jQuery(".top_slider.flexslider").flexslider({
			animation: "fade",
			controlNav: false,
			directionNav: true,
			animationLoop: false,
			slideshow: false,
			prevText: "<i class='fa fa-angle-left'></i>",
			nextText: "<i class='fa fa-angle-right'></i>",
		});
	
	}
	
	//	Top Slider style 6
	if( jQuery('.evapb_top_slider .style6').size() > 0 ) {
		
		if( jQuery('.top_slider.style6').hasClass('col3') ) {
			var $top_slider_columns = 3;
		} else if( jQuery('.top_slider.style6').hasClass('col4') ) {
			var $top_slider_columns = 4;
		} else {
			var $top_slider_columns = 5;
		}
		
		jQuery(".top_slider.style6 .owl-carousel").owlCarousel({
			margin: 0,
			dots: false,
			nav: true,
			navText: [
				"",
				"",
			],
			loop: true,
			autoplay: false,
			autoplaySpeed: 1000,
			autoplayTimeout: 3000,
			navSpeed: 1000,
			autoplayHoverPause: true,
			responsive: {
				0: {items: 1},
				481: {items: 2},
				769: {items: 3},
				1024: {items: 3},
				1920: {items: $top_slider_columns},
			},
			thumbs: false,
			thumbImage: false,
		});
	
	}
	
	//	Blog Carousel first image big
	if( jQuery('.evapb_blog_carousel.fisrt_big_img').size() > 0 ) {
		jQuery(".evapb_blog_carousel.fisrt_big_img .posts_carousel_list.owl-carousel").owlCarousel({
			margin: 0,
			dots: false,
			nav: true,
			navText: [
				"",
				"",
			],
			loop: false,
			autoplay: false,
			navSpeed: 1000,
			animateIn: "fadeIn",
			animateOut: "fadeOut",
			items: 1,
			thumbs: false,
			thumbImage: false,
		});
	}
	
	//	Blog Carousel small carousel
	if( jQuery('.evapb_blog_carousel.small_carousel').size() > 0 ) {
		jQuery(".evapb_blog_carousel.small_carousel .posts_carousel_list.owl-carousel").owlCarousel({
			margin: 0,
			dots: true,
			nav: false,
			navText: [
				"",
				"",
			],
			loop: false,
			autoplay: false,
			autoplaySpeed: 1000,
			autoplayTimeout: 3000,
			navSpeed: 1000,
			autoplayHoverPause: true,
			responsive: {
				0: {items: 1},
				481: {items: 2},
				769: {items: 3},
				1024: {items: 4},
				1920: {items: 5},
			},
			thumbs: false,
			thumbImage: false,
		});
	}
	
	
	//	Blog Categories Carousel
	if( jQuery('.evapb-categories').size() > 0 ) {
		jQuery('.evapb-categories .categories_list.owl-carousel').owlCarousel({
			margin: 30,
			dots: false,
			nav: true,
			navText: [
				"",
				"",
			],
			loop: false,
			autoplay: true,
			autoplaySpeed: 1000,
			autoplayTimeout: 3000,
			navSpeed: 1000,
			autoplayHoverPause: true,
			responsive: {
				0: {items: 1},
				481: {items: 2},
				769: {items: 2},
				1024: {items: 3},
				1920: {items: 3},
			},
			thumbs: false,
			thumbImage: false,
		});
	}
	
});

jQuery(window).resize(function(){
	"use strict";
	
	evapb_fullwidth();
	
	evapb_BlogMetroHeight();
	
	evapb_post_card_clean_Height();
	
	evapb_pf_twitter_owlcarousel();
	
});


//	Fun Facts
(function(e){e.fn.countTo=function(t){t=e.extend({},e.fn.countTo.defaults,t||{});var n=Math.ceil(t.speed/t.refreshInterval),r=(t.to-t.from)/n;return e(this).each(function(){function a(){o+=r;s++;e(i).html(o.toFixed(t.decimals));if(typeof t.onUpdate=="function"){t.onUpdate.call(i,o)}if(s>=n){clearInterval(u);o=t.to;if(typeof t.onComplete=="function"){t.onComplete.call(i,o)}}}var i=this,s=0,o=t.from,u=setInterval(a,t.refreshInterval)})};e.fn.countTo.defaults={from:0,to:100,speed:1e3,refreshInterval:100,decimals:0,onUpdate:null,onComplete:null}})(jQuery)