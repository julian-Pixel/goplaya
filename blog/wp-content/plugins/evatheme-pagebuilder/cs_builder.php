<?php
/*
Plugin Name: 	Evatheme Page Builder
Plugin URI: 	http://www.evatheme.com/
Description: 	Evatheme Page Builder is a powerful WordPress plugin that allows you to create the unlimited number of custom page layouts in WordPress themes. This special drag and drop plugin will save your time when building the pages.
Version: 		1.0
Author: 		Evatheme Team
Author URI: 	http://www.evatheme.com/
*/


global $cs_isshortcode;
$cs_isshortcode='false';

// Load from theme
define('CSTHEME_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('CSTHEME_PLUGIN_URL', plugin_dir_url(__FILE__));
define('CSTHEME_SHORTCODE'  , 'cstheme_metabox_shortcode');
define('CSTHEME_PAGEBUILDER', 'cstheme_metabox_pagebuilder');


//	Load files
include_once(CSTHEME_PLUGIN_PATH . "pagebuilder/pb-functions.php");

//	Widgets
include_once( CSTHEME_PLUGIN_PATH . 'inc/widgets/widget-twitter.php' );


//	CStheme Page Builder Elements
global $cstheme_elementfiles;
$cstheme_elementfiles = array(
	'accordion',
	'before_after',
	'blog',
	'blog_carousel',
	'button',
	'categories',
	'column',
	'content',
	'counter',
	'divider',
	'fonticon',
	'heading',
	'image_gallery',
	'map',
	'messagebox',
	'partner',
	'pricing_table',
	'progress',
	'service',
	'sidebar',
	'slider',
	'tab',
	'team',
	'testimonials',
);


if(!is_admin()){
	require_once CSTHEME_PLUGIN_PATH .'pagebuilder/elements-render/layout.php';
	foreach($cstheme_elementfiles as $file){
		require_once (CSTHEME_PLUGIN_PATH ."pagebuilder/elements-render/$file.php");
	}
	
	
	add_filter('the_content', 'cstheme_page_content');
	function cstheme_page_content($content){
		
		global $post, $cs_isshortcode;
		
		$cstheme_content = ( $cs_isshortcode === 'true' || !isset( $post->ID ) ) ? false : get_post_meta($post->ID, CSTHEME_SHORTCODE, true);
		if( $cstheme_content && !post_password_required() ){
			return do_shortcode( $cstheme_content );
		}
		return $content;
	}
	
	// Enqueue styles.
	add_action('wp_enqueue_scripts', 'cstheme_builder_front_styles');
	if (!function_exists('cstheme_builder_front_styles')) {
		function cstheme_builder_front_styles() {

			//	styles
			wp_enqueue_style('cstheme-font-awesome', CSTHEME_PLUGIN_URL . 'assets/css/font-awesome.min.css');
			wp_enqueue_style('cstheme-pb-elements', CSTHEME_PLUGIN_URL . 'assets/css/pb-elements.css');
	
			//	scripts
			wp_enqueue_script('cstheme-waypoints', CSTHEME_PLUGIN_URL . 'assets/js/jquery.waypoints.min.js', array(), false, true);
			wp_enqueue_script('evapb-isotope', CSTHEME_PLUGIN_URL . 'assets/js/cstheme-isotope.js', array(), false, true);
			wp_enqueue_script('cstheme-pb-plugins', CSTHEME_PLUGIN_URL . 'assets/js/pb-plugins.js', array(), false, true);
			
		}
	}
	
} else {     
	//====== Back-End ======//
	require_once CSTHEME_PLUGIN_PATH .'pagebuilder/cstheme-builder.php';
}

?>